package ArtificialIntelligence
{
	public class AITile
	{
		// basic info
		public var state:int = 0;
		public var team:int;
		public var location:int;
		public var activeCapturePoint:Boolean = false;
		public var playerOwner:int;
		
		// adjacencies
		public var adjacentLeftTileLoc:int;
		public var adjacentRightTileLoc:int;
		public var adjacentTopTileLoc:int;
		public var adjacentBottomTileLoc:int;
		public var top:Boolean = false;
		public var right:Boolean = false;
		public var bottom:Boolean = false;
		public var left:Boolean = false;
		
		// changing state variables
		public var hasAlreadySpilledOver:Boolean = false;
		public var originalTileState:int;
		public var hasTraced:Boolean = false;
		public var hasCounted:Boolean = false;
		public var hasCountedThrees:Boolean = false;
		public var hasCountedTwos:Boolean = false;
		public var hasCountedTwosForUpgrade:Boolean = false;
		
		// weight used for tile selection
		public var weight:Array = [];
		
		public function AITile(inLocation:int):void
		{
			location = inLocation;
			for (var i:int = 0; i < 4; i++)
				weight.push(0);
		}
		
		public function changeState():Boolean
		{
			var stateChange:Boolean = false;
			
			//if (AI.debugCalc)
				//trace("in change state");
			
			if (AI.currentPlayer != null)
			{
				//trace("changing state of tile");
				
				hasAlreadySpilledOver = true;
				
				//If the player has selected a level 0 Tile
				if (AI.selectedTileLevel == 0)
				{
					//If the player is stacking the same level on itself
					if ((state == 1) && (team == AI.currentPlayer.playerTeam))
					{
						//trace("changed state to 2");
						
						state = 2;
						stateChange = true;
					}
						//If the Tile has not been Taken
					else if (state == 0)
					{
						//trace("changed state to 1");
						
						state = 1;
						stateChange = true;
					}
				}
					//If the player has selected a level 1 Tile
				else if (AI.selectedTileLevel == 1)
				{
					//trace("I have a lvl 1 tile");
					//trace("CurrentTile State:" + state);
					
					//If the player is stacking the same level on itself
					if ((state == 2) && (team == AI.currentPlayer.playerTeam))
					{
						state = 3;
						stateChange = true;
					}
						//Overwrite a lesser color
					else if (state == 1)
					{
						state = 2;
						stateChange = true;
					}
					else if (state == 0 || state == 5)
					{
						//trace("changed to state 2");
						state = 2;
						stateChange = true;
					}
				}
					//If the player has selected a level 2 Tile
				else if (AI.selectedTileLevel == 2)
				{
					//if (AI.debugCalc)
						//trace("tile level = 2 --- state = " + state);
					//trace("I Have a lvl 2 tile");
					//trace("CurrentTile State:" + state);
					
					if (state == 6)
					{
						state = 3;
						stateChange = true;
					}
					else if (state == 5)
					{
						state = 3;
						stateChange = true;
					}
					else if ((state == 3) && (team == AI.currentPlayer.playerTeam))
					{
						state = 4;
						stateChange = true;
					}
					else if (state == 2)
					{
						state = 3;
						stateChange = true;
					}
					else if (state == 1)
					{
						state = 3;
						stateChange = true;
					}
					else if (state == 0)
					{
						state = 3;
						stateChange = true;
					}
				}
				else if (AI.selectedTileLevel == 3)
				{
					//trace("Using A Virus");
					
					if ((state == 3) && (team != AI.currentPlayer.playerTeam))
					{
						state = 0;
						originalTileState = 3;
						stateChange = true;
					}
					if ((state == 2) && (team != AI.currentPlayer.playerTeam))
					{
						state = 0;
						originalTileState = 2;
						stateChange = true;
					}
					if ((state == 1) && (team != AI.currentPlayer.playerTeam))
					{
						//trace("On a level 1 tile");
						state = 0;
						originalTileState = 1;
						stateChange = true;
					}
				}
			}
			
			if (stateChange)
			{
				team = AI.currentPlayer.playerTeam;
				
				//if (AI.debugCalc)
					//trace("set state to " + state + " and team to " + team + " and spilling over");
				
				spillOver();
			}
			else
			{
				return false;
			}
			return true;
		}
		
		public function cascade(OriginatorTileState:int, OriginatorTilesRelativePos:int, tileTeam:int, originatorOriginalTileState:int):void
		{
			//trace("Cascade");
			originalTileState = originatorOriginalTileState;
			
			//if (AI.debugCalc)
				//trace("cascading for loc " + location + " original tile state = " + originalTileState + " OriginatorTileState = " + OriginatorTileState);
			
			//trace("Have you spilled over?" + hasAlreadySpilledOver);
			
			if (!hasAlreadySpilledOver)
			{
				hasAlreadySpilledOver = true;
				
				//trace("Original was a lvl " + originalTileState);
				
				if (OriginatorTileState == 2)
				{
					team = tileTeam;
					
					if (state == 0)
					{
						state = 1;
						reset();
					}
					else if (state == 1)
					{
						state = 2;
						spillOver();
					}
					else if (state == 5)
					{
						state = 2;
						spillOver();
					}
				}
				else if (OriginatorTileState == 3)
				{
					team = tileTeam;
					
					//trace("Original was a lvl 2");
					
					if (state == 0)
					{
						state = 2;
						reset();
					}
					else if (state == 1)
					{
						//trace("This one's State was a lvl 1");
						
						state = 2;
						spillOver();
					}
					else if (state == 5)
					{
						state = 2;
						spillOver();
					}
					else if (state == 2)
					{
						state = 3;
						spillOver();
					}
					else if (state == 6)
					{
						state = 3;
						spillOver();
					}
				}
				else if (OriginatorTileState == 4)
				{					
					state = 4;
					team = tileTeam;
					spillOver();
				}
				else if (OriginatorTileState == 0)
				{
					state = 0;
					team = -1;
					spillOver();
				}
			}
		}
		
		public function spillOver():void
		{
			if (AI.virtualBoard != null)
			{
				//trace("Spill Over");
				var numOfViableAdjacentTiles:int = 0;
				//rotation = 0;
				
				if (state != 4 && state != 0)
				{
					if (AI.virtualBoard[adjacentTopTileLoc] != null)
					{
						//trace("Creating Adjacent");
						if (AI.virtualBoard[adjacentTopTileLoc].state == 0 && AI.virtualBoard[adjacentTopTileLoc].hasAlreadySpilledOver == false)
						{
							numOfViableAdjacentTiles++;
							top = true;
							//trace("Top");
						}
					}
					if (AI.virtualBoard[adjacentRightTileLoc])
					{
						if (AI.virtualBoard[adjacentRightTileLoc].state == 0 && AI.virtualBoard[adjacentRightTileLoc].hasAlreadySpilledOver == false)
						{
							numOfViableAdjacentTiles++;
							right = true;
							//trace("Right");
						}
					}
					if (AI.virtualBoard[adjacentBottomTileLoc] != null)
					{
						if (AI.virtualBoard[adjacentBottomTileLoc].state == 0 && AI.virtualBoard[adjacentBottomTileLoc].hasAlreadySpilledOver == false)
						{
							numOfViableAdjacentTiles++;
							bottom = true;
							//trace("Bottom");
						}
					}
					if (AI.virtualBoard[adjacentLeftTileLoc] != null)
					{
						if (AI.virtualBoard[adjacentLeftTileLoc].state == 0 && AI.virtualBoard[adjacentLeftTileLoc].hasAlreadySpilledOver == false)
						{
							numOfViableAdjacentTiles++;
							left = true;
							//trace("Left");
						}
					}
				}
				
				if (state == 2)
				{
					if ((AI.virtualBoard[adjacentTopTileLoc].state == 1) || (AI.virtualBoard[adjacentTopTileLoc].state == 5))
					{
						AI.virtualBoard[adjacentTopTileLoc].cascade(state, 0, team, originalTileState);
					}
					if ((AI.virtualBoard[adjacentBottomTileLoc].state == 1) || (AI.virtualBoard[adjacentBottomTileLoc].state == 5))
					{
						AI.virtualBoard[adjacentBottomTileLoc].cascade(state, 1, team, originalTileState);
					}
					if ((AI.virtualBoard[adjacentRightTileLoc].state == 1) || (AI.virtualBoard[adjacentRightTileLoc].state == 5))
					{
						AI.virtualBoard[adjacentRightTileLoc].cascade(state, 2, team, originalTileState);
					}
					if ((AI.virtualBoard[adjacentLeftTileLoc].state == 1) || (AI.virtualBoard[adjacentLeftTileLoc].state == 5))
					{
						AI.virtualBoard[adjacentLeftTileLoc].cascade(state, 3, team, originalTileState);
					}
				}
					
				else if (state == 3)
				{
					//trace("Cascade lvl 1's in all directions");
					if ((AI.virtualBoard[adjacentTopTileLoc].state == 1) || (AI.virtualBoard[adjacentTopTileLoc].state == 2) || (AI.virtualBoard[adjacentTopTileLoc].state == 6))
					{
						AI.virtualBoard[adjacentTopTileLoc].cascade(state, 0, team, originalTileState);
					}
					if ((AI.virtualBoard[adjacentBottomTileLoc].state == 1) || (AI.virtualBoard[adjacentBottomTileLoc].state == 2) || (AI.virtualBoard[adjacentBottomTileLoc].state == 6))
					{
						AI.virtualBoard[adjacentBottomTileLoc].cascade(state, 1, team,  originalTileState);
					}
					if ((AI.virtualBoard[adjacentRightTileLoc].state == 1) || (AI.virtualBoard[adjacentRightTileLoc].state == 2) || (AI.virtualBoard[adjacentRightTileLoc].state == 6))
					{
						AI.virtualBoard[adjacentRightTileLoc].cascade(state, 2, team, originalTileState);
					}
					if ((AI.virtualBoard[adjacentLeftTileLoc].state == 1) || (AI.virtualBoard[adjacentLeftTileLoc].state == 2) || (AI.virtualBoard[adjacentLeftTileLoc].state == 6))
					{
						AI.virtualBoard[adjacentLeftTileLoc].cascade(state, 3, team, originalTileState);
					}
				}
					
				else if (state == 4)
				{
					if (AI.virtualBoard[adjacentTopTileLoc].state == 3)
					{
						//if (AI.debugCalc)
							//trace("cascading to tile " + adjacentTopTileLoc);
						AI.virtualBoard[adjacentTopTileLoc].cascade(state, 0, team, originalTileState);
					}
					if (AI.virtualBoard[adjacentBottomTileLoc].state == 3)
					{
						//if (AI.debugCalc)
							//trace("cascading to tile " + adjacentBottomTileLoc);
						
						AI.virtualBoard[adjacentBottomTileLoc].cascade(state, 1, team, originalTileState);
					}
					if (AI.virtualBoard[adjacentRightTileLoc].state == 3)
					{
						//if (AI.debugCalc)
							//trace("cascading to tile " + adjacentRightTileLoc);
						
						AI.virtualBoard[adjacentRightTileLoc].cascade(state, 2, team, originalTileState);
					}
					if (AI.virtualBoard[adjacentLeftTileLoc].state == 3)
					{
						//if (AI.debugCalc)
							//trace("cascading to tile " + adjacentLeftTileLoc);
						
						AI.virtualBoard[adjacentLeftTileLoc].cascade(state, 3, team /* owner */, originalTileState);
					}
				}
					
				else if (state == 0)
				{	
					if (originalTileState == 3)
					{
						if (AI.virtualBoard[adjacentTopTileLoc].state == 3)
						{
							AI.virtualBoard[adjacentTopTileLoc].cascade(state, 0, team, originalTileState);
						}
						if (AI.virtualBoard[adjacentBottomTileLoc].state == 3)
						{
							AI.virtualBoard[adjacentBottomTileLoc].cascade(state, 1, team, originalTileState);
						}
						if (AI.virtualBoard[adjacentRightTileLoc].state == 3)
						{
							AI.virtualBoard[adjacentRightTileLoc].cascade(state, 2, team, originalTileState);
						}
						if (AI.virtualBoard[adjacentLeftTileLoc].state == 3)
						{
							AI.virtualBoard[adjacentLeftTileLoc].cascade(state, 3, team, originalTileState);
						}
					}
					else if (originalTileState == 2)
					{
						if (AI.virtualBoard[adjacentTopTileLoc].state == 2 || AI.virtualBoard[adjacentTopTileLoc].state == 6)
						{
							AI.virtualBoard[adjacentTopTileLoc].cascade(state, 0, team, originalTileState);
						}
						if (AI.virtualBoard[adjacentBottomTileLoc].state == 2 || AI.virtualBoard[adjacentBottomTileLoc].state == 6)
						{
							AI.virtualBoard[adjacentBottomTileLoc].cascade(state, 1, team, originalTileState);
						}
						if (AI.virtualBoard[adjacentRightTileLoc].state == 2 || AI.virtualBoard[adjacentRightTileLoc].state == 6)
						{
							AI.virtualBoard[adjacentRightTileLoc].cascade(state, 2, team, originalTileState);
						}
						if (AI.virtualBoard[adjacentLeftTileLoc].state == 2 || AI.virtualBoard[adjacentLeftTileLoc].state == 6)
						{
							AI.virtualBoard[adjacentLeftTileLoc].cascade(state, 3, team, originalTileState);
						}
					}
					else if (originalTileState == 1)
					{
						if (AI.virtualBoard[adjacentTopTileLoc].state == 1 || AI.virtualBoard[adjacentTopTileLoc].state == 5)
						{
							AI.virtualBoard[adjacentTopTileLoc].cascade(state, 0, team, originalTileState);
						}
						if (AI.virtualBoard[adjacentBottomTileLoc].state == 1 || AI.virtualBoard[adjacentBottomTileLoc].state == 5)
						{
							AI.virtualBoard[adjacentBottomTileLoc].cascade(state, 1, team, originalTileState);
						}
						if (AI.virtualBoard[adjacentRightTileLoc].state == 1 || AI.virtualBoard[adjacentRightTileLoc].state == 5)
						{
							AI.virtualBoard[adjacentRightTileLoc].cascade(state, 2, team, originalTileState);
						}
						if (AI.virtualBoard[adjacentLeftTileLoc].state == 1 || AI.virtualBoard[adjacentLeftTileLoc].state == 5)
						{
							AI.virtualBoard[adjacentLeftTileLoc].cascade(state, 3, team, originalTileState);
						}
					}
				}
				
				//trace("Num of Viable Adjacent Tiles" + numOfViableAdjacentTiles);
				
				// if adjacent tiles
				if (numOfViableAdjacentTiles > 0)
				{
					//trace("Spreading 1");
					hitAdjacentTiles();
				}
				else if (numOfViableAdjacentTiles == 0)
				{
					reset();
					
					if (state == 0)
					{
						team = -1;
					}
					
				}
			}
		}
		
		public function reset():void
		{
			//stateChange = false;
			top = false;
			right = false;
			bottom = false;
			left = false;
			//rotation = 0;
		}
		
		public function hitAdjacentTiles():void
		{
			if (top)
			{
				AI.virtualBoard[adjacentTopTileLoc].cascade(state, null, team, originalTileState);
			}
			if (right)
			{
				AI.virtualBoard[adjacentRightTileLoc].cascade(state, null, team, originalTileState);
			}
			if (bottom)
			{
				AI.virtualBoard[adjacentBottomTileLoc].cascade(state, null, team, originalTileState);
			}
			if (left)
			{
				AI.virtualBoard[adjacentLeftTileLoc].cascade(state, null, team, originalTileState);
			}
			
			reset();
		}
		
		public function tileTouchesSameLevelDifferentPlayer():Boolean
		{
			//trace("function start " + tileLocation + " hasTraced = " + hasTraced);
			
			// only check tile if has not been traced yet
			if (!hasTraced)
			{
				//trace("starting trace for tile " + tileLocation);
				
				hasTraced = true;
				
				// if not a playable tile, return false
				if (state < 0 || state > 3)
				{	
					//trace("found nonplayable tile - returning false");
					return false;
				}
				
				// check adjacent tiles
				if (adjacentTopTileLoc != -1)
				{
					if (AI.virtualBoard[adjacentTopTileLoc].state == AI.tileScanState)
					{
						if (AI.virtualBoard[adjacentTopTileLoc].team == AI.currentPlayer.playerTeam)
						{
							//trace("internal check " + adjacentTopTileLoc + " tile location = " + AI.virtualBoard[adjacentTopTileLoc].tileLocation);
							if (AI.virtualBoard[adjacentTopTileLoc].tileTouchesSameLevelDifferentPlayer())
							{
								// add tile owner team to the array of teams
								if (AI.teamsWithConnectedTiles.indexOf(AI.virtualBoard[adjacentTopTileLoc].playerOwner) < 0)
								{
									//trace("top IN tile " + tileLocation + " added team " + AI.virtualBoard[adjacentTopTileLoc].team);
									AI.teamsWithConnectedTiles.push(AI.virtualBoard[adjacentTopTileLoc].playerOwner);
								}
								
								//trace("found another tile to check");
								return true;
							}
						}
						else
						{
							// add tile owner team to the array of teams
							if (AI.teamsWithConnectedTiles.indexOf(AI.virtualBoard[adjacentTopTileLoc].playerOwner) < 0)
							{
								//trace("top OUT tile " + tileLocation + " added team " + AI.virtualBoard[adjacentTopTileLoc].team);
								AI.teamsWithConnectedTiles.push(AI.virtualBoard[adjacentTopTileLoc].playerOwner);
							}
							
							//trace("found enemy tile");
							return true;
						}
					}
				}
				if (adjacentRightTileLoc != -1)
				{
					if (AI.virtualBoard[adjacentRightTileLoc].state == AI.tileScanState)
					{
						if (AI.virtualBoard[adjacentRightTileLoc].team == AI.currentPlayer.playerTeam)
						{
							//trace("internal check " + adjacentRightTileLoc + " tile location = " + AI.virtualBoard[adjacentRightTileLoc].tileLocation);
							if (AI.virtualBoard[adjacentRightTileLoc].tileTouchesSameLevelDifferentPlayer())
							{
								// add tile owner team to the array of teams
								if (AI.teamsWithConnectedTiles.indexOf(AI.virtualBoard[adjacentRightTileLoc].playerOwner) < 0)
								{
									//trace("right IN tile " + tileLocation + " added team " + AI.virtualBoard[adjacentRightTileLoc].team);
									AI.teamsWithConnectedTiles.push(AI.virtualBoard[adjacentRightTileLoc].playerOwner);
								}
								
								//trace("found another tile to check");
								return true;
							}
						}
						else
						{
							// add tile owner team to the array of teams
							if (AI.teamsWithConnectedTiles.indexOf(AI.virtualBoard[adjacentRightTileLoc].playerOwner) < 0)
							{
								//trace("right OUT tile " + tileLocation + " added team " + AI.virtualBoard[adjacentRightTileLoc].team);
								AI.teamsWithConnectedTiles.push(AI.virtualBoard[adjacentRightTileLoc].playerOwner);
							}
							
							//trace("found enemy tile");
							return true;
						}
					}
				}
				if (adjacentBottomTileLoc != -1)
				{
					if (AI.virtualBoard[adjacentBottomTileLoc].state == AI.tileScanState)
					{
						if (AI.virtualBoard[adjacentBottomTileLoc].team == AI.currentPlayer.playerTeam)
						{
							//trace("internal check " + adjacentBottomTileLoc + " tile location = " + AI.virtualBoard[adjacentBottomTileLoc].tileLocation);
							if (AI.virtualBoard[adjacentBottomTileLoc].tileTouchesSameLevelDifferentPlayer())
							{
								// add tile owner team to the array of teams
								if (AI.teamsWithConnectedTiles.indexOf(AI.virtualBoard[adjacentBottomTileLoc].playerOwner) < 0)
								{
									//trace("bottom IN tile " + tileLocation + " added team " + AI.virtualBoard[adjacentBottomTileLoc].team);
									AI.teamsWithConnectedTiles.push(AI.virtualBoard[adjacentBottomTileLoc].playerOwner);
								}
								
								//trace("found another tile to check");
								return true;
							}
						}
						else
						{
							// add tile owner team to the array of teams
							if (AI.teamsWithConnectedTiles.indexOf(AI.virtualBoard[adjacentBottomTileLoc].playerOwner) < 0)
							{
								//trace("bottom OUT tile " + tileLocation + " added team " + AI.virtualBoard[adjacentBottomTileLoc].team);
								AI.teamsWithConnectedTiles.push(AI.virtualBoard[adjacentBottomTileLoc].playerOwner);
							}
							
							//trace("found enemy tile");
							return true;
						}
					}
				}
				if (adjacentLeftTileLoc != -1)
				{
					if (AI.virtualBoard[adjacentLeftTileLoc].state == AI.tileScanState)
					{
						if (AI.virtualBoard[adjacentLeftTileLoc].team == AI.currentPlayer.playerTeam)
						{
							//trace("internal check " + adjacentLeftTileLoc + " tile location = " + AI.virtualBoard[adjacentLeftTileLoc].tileLocation);
							if (AI.virtualBoard[adjacentLeftTileLoc].tileTouchesSameLevelDifferentPlayer())
							{
								// add tile owner team to the array of teams
								if (AI.teamsWithConnectedTiles.indexOf(AI.virtualBoard[adjacentLeftTileLoc].playerOwner) < 0)
								{
									//trace("left IN tile " + tileLocation + " added team " + AI.virtualBoard[adjacentLeftTileLoc].team);
									AI.teamsWithConnectedTiles.push(AI.virtualBoard[adjacentLeftTileLoc].playerOwner);
								}
								
								//trace("found another tile to check");
								return true;
							}
						}
						else
						{
							// add tile owner team to the array of teams
							if (AI.teamsWithConnectedTiles.indexOf(AI.virtualBoard[adjacentLeftTileLoc].playerOwner) < 0)
							{
								//trace("left OUT tile " + tileLocation + " added team " + AI.virtualBoard[adjacentLeftTileLoc].team);
								AI.teamsWithConnectedTiles.push(AI.virtualBoard[adjacentLeftTileLoc].playerOwner);
							}
							
							//trace("found enemy tile");
							return true;
						}
					}
				}
			}
			return false;
		}
		
		public function numberOfAdjacentSameTiles():int
		{
			var totalCount:int = 0;
			//trace("function start " + tileLocation + " hasCounted = " + hasCounted);
			
			// only check tile if has not been traced yet
			if (!hasCounted)
			{
				//trace("starting count for tile " + tileLocation);
				
				hasCounted = true;
				totalCount++;
				
				// if not a playable tile, return false
				if (state != 3 && state != 7)
				{	
					// trace("found nonplayable tile - returning false");
					return 0;
				}
				if (team == AI.currentPlayer.playerTeam)
					AI.hitsCurrentPlayerThrees = true;
				
				// check adjacent tiles
				if (adjacentTopTileLoc != -1)
				{
					if (AI.originalBoard[adjacentTopTileLoc].state == 3 || AI.originalBoard[adjacentTopTileLoc].state == 7)
					{
						totalCount += AI.originalBoard[adjacentTopTileLoc].numberOfAdjacentSameTiles();
					}
				}
				if (adjacentRightTileLoc != -1)
				{
					if (AI.originalBoard[adjacentRightTileLoc].state == 3 || AI.originalBoard[adjacentRightTileLoc].state == 7)
					{
						totalCount += AI.originalBoard[adjacentRightTileLoc].numberOfAdjacentSameTiles();
					}
				}
				if (adjacentBottomTileLoc != -1)
				{
					if (AI.originalBoard[adjacentBottomTileLoc].state == 3 || AI.originalBoard[adjacentBottomTileLoc].state == 7)
					{
						totalCount += AI.originalBoard[adjacentBottomTileLoc].numberOfAdjacentSameTiles();
					}
				}
				if (adjacentLeftTileLoc != -1)
				{
					if (AI.originalBoard[adjacentLeftTileLoc].state == 3 || AI.originalBoard[adjacentLeftTileLoc].state == 7)
					{
						totalCount += AI.originalBoard[adjacentLeftTileLoc].numberOfAdjacentSameTiles();
					}
				}
			}
			return totalCount;
		}
		
		public function numberOfAdjacentThrees():int
		{
			var totalCount:int = 0;
			//trace("function start " + tileLocation + " hasCounted = " + hasCounted);
			
			// only check tile if has not been traced yet
			if (!hasCountedThrees)
			{
				//trace("starting count for tile " + tileLocation);
				
				hasCountedThrees = true;
				totalCount++;
				
				// if not a playable tile, return false
				if (state != 3 && state != 7)
				{	
					// trace("found nonplayable tile - returning false");
					return 0;
				}
				
				// check adjacent tiles
				if (adjacentTopTileLoc != -1)
				{
					if (AI.originalBoard[adjacentTopTileLoc].state == 3 || AI.originalBoard[adjacentTopTileLoc].state == 7)
					{
						totalCount += AI.originalBoard[adjacentTopTileLoc].numberOfAdjacentThrees();
					}
				}
				if (adjacentRightTileLoc != -1)
				{
					if (AI.originalBoard[adjacentRightTileLoc].state == 3 || AI.originalBoard[adjacentRightTileLoc].state == 7)
					{
						totalCount += AI.originalBoard[adjacentRightTileLoc].numberOfAdjacentThrees();
					}
				}
				if (adjacentBottomTileLoc != -1)
				{
					if (AI.originalBoard[adjacentBottomTileLoc].state == 3 || AI.originalBoard[adjacentBottomTileLoc].state == 7)
					{
						totalCount += AI.originalBoard[adjacentBottomTileLoc].numberOfAdjacentThrees();
					}
				}
				if (adjacentLeftTileLoc != -1)
				{
					if (AI.originalBoard[adjacentLeftTileLoc].state == 3 || AI.originalBoard[adjacentLeftTileLoc].state == 7)
					{
						totalCount += AI.originalBoard[adjacentLeftTileLoc].numberOfAdjacentThrees();
					}
				}
			}
			return totalCount;
		}
		
		public function numberOfAdjacentTwos():int
		{
			var totalCount:int = 0;
			//trace("function start " + tileLocation + " hasCounted = " + hasCounted);
			
			// only check tile if has not been traced yet
			if (!hasCountedTwos)
			{
				//trace("starting count for tile " + tileLocation);
				
				hasCountedTwos = true;
				totalCount++;
				
				// if not a playable tile, return false
				if (state != 2 && state != 6)
				{	
					// trace("found nonplayable tile - returning false");
					return 0;
				}
				
				// check adjacent tiles
				if (adjacentTopTileLoc != -1)
				{
					if (AI.originalBoard[adjacentTopTileLoc].state == 2 || AI.originalBoard[adjacentTopTileLoc].state == 6)
					{
						totalCount += AI.originalBoard[adjacentTopTileLoc].numberOfAdjacentTwos();
					}
				}
				if (adjacentRightTileLoc != -1)
				{
					if (AI.originalBoard[adjacentRightTileLoc].state == 2 || AI.originalBoard[adjacentRightTileLoc].state == 6)
					{
						totalCount += AI.originalBoard[adjacentRightTileLoc].numberOfAdjacentTwos();
					}
				}
				if (adjacentBottomTileLoc != -1)
				{
					if (AI.originalBoard[adjacentBottomTileLoc].state == 2 || AI.originalBoard[adjacentBottomTileLoc].state == 6)
					{
						totalCount += AI.originalBoard[adjacentBottomTileLoc].numberOfAdjacentTwos();
					}
				}
				if (adjacentLeftTileLoc != -1)
				{
					if (AI.originalBoard[adjacentLeftTileLoc].state == 2 || AI.originalBoard[adjacentLeftTileLoc].state == 6)
					{
						totalCount += AI.originalBoard[adjacentLeftTileLoc].numberOfAdjacentTwos();
					}
				}
			}
			return totalCount;
		}
		
		public function numberOfAdjacentTwosForUpgrade():int
		{
			var totalCount:int = 0;
			//trace("function start " + tileLocation + " hasCounted = " + hasCounted);
			
			// only check tile if has not been traced yet
			if (!hasCountedTwosForUpgrade)
			{
				//trace("starting count for tile " + tileLocation);
				
				hasCountedTwosForUpgrade = true;
				totalCount++;
				
				// if not a playable tile, return false
				if (state != 2 && state != 6)
				{	
					// trace("found nonplayable tile - returning false");
					return 0;
				}
				
				// check adjacent tiles
				if (adjacentTopTileLoc != -1)
				{
					if (AI.originalBoard[adjacentTopTileLoc].state == 2 || AI.originalBoard[adjacentTopTileLoc].state == 6)
					{
						totalCount += AI.originalBoard[adjacentTopTileLoc].numberOfAdjacentTwosForUpgrade();
					}
				}
				if (adjacentRightTileLoc != -1)
				{
					if (AI.originalBoard[adjacentRightTileLoc].state == 2 || AI.originalBoard[adjacentRightTileLoc].state == 6)
					{
						totalCount += AI.originalBoard[adjacentRightTileLoc].numberOfAdjacentTwosForUpgrade();
					}
				}
				if (adjacentBottomTileLoc != -1)
				{
					if (AI.originalBoard[adjacentBottomTileLoc].state == 2 || AI.originalBoard[adjacentBottomTileLoc].state == 6)
					{
						totalCount += AI.originalBoard[adjacentBottomTileLoc].numberOfAdjacentTwosForUpgrade();
					}
				}
				if (adjacentLeftTileLoc != -1)
				{
					if (AI.originalBoard[adjacentLeftTileLoc].state == 2 || AI.originalBoard[adjacentLeftTileLoc].state == 6)
					{
						totalCount += AI.originalBoard[adjacentLeftTileLoc].numberOfAdjacentTwosForUpgrade();
					}
				}
			}
			return totalCount;
		}
	}
}