package ArtificialIntelligence
{
	public class AIBestMove
	{
		public var location:int;
		public var weight:int;
		public var differential:int;
		
		public function AIBestMove(inLocation:int, inWeight:int):void
		{
			location = inLocation;
			weight = inWeight;
		}
	}
}