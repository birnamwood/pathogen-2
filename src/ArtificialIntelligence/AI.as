package ArtificialIntelligence
{
	import Core.GameManager;
	import Core.Player;
	
	import UI.Main;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	
	//import starling.events.Event;
	
	public class AI extends Sprite
	{
		//const MOVE_TIME:int = 24;
		private const MOVE_DELAY_TIME:Number = 2.0;  // the time the AI takes before making a move, in seconds
		
		// weight constants
		private const GOOD_PLACE:int = 10;
		private const BAD_PLACE:int = -10;
		private const GOOD_ADJ:int = 3;
		private const BAD_ADJ:int = -3;
		private const BETTER_ADJ:int = 5;
		private const UNPLAYABLE:int = -10;
		private const ADJ_PENALTY:int = -2;
		
		// difficulty constants
		private const HARD_TOP_WEIGHT_PERCENT:int = 100;
		private const MEDIUM_TOP_WEIGHT_PERCENT:int = 40;
		private const EASY_TOP_WEIGHT_PERCENT:int = 15;
		private const MEDIUM_TOP_DIFF_PERCENT:int = 20;
		private const EASY_TOP_DIFF_PERCENT:int = 40;
		private const HARD_LEVEL_1_NOT_RANDOM_BLANK_PERCENT:int = 95;  // inclination to pick a best move for level 1 (otherwise pick blank spot) - hard
		private const MEDIUM_LEVEL_1_NOT_RANDOM_BLANK_PERCENT:int = 50;  // inclination to pick a best move for level 1 (otherwise pick blank spot) - medium
		private const EASY_LEVEL_1_NOT_RANDOM_BLANK_PERCENT:int = 25;  // inclination to pick a best move for level 1 (otherwise pick blank spot) - easy
		
		// tile play selection constants
		private const PLAY_LEVEL_2_PERCENT:int = 50;  // base inclination to play level 2 (if has a level 2 tile)
		private const PLAY_LEVEL_3_PERCENT:int = 50;  // base inclination to play level 3 (if has a level 3 tile)
		private const PLAY_PARASITE_PERCENT:int = 50;  // base inclination to play parasite (if has a parasite)
		
		// minimum number of moves in best moves list
		private const LEVEL_2_MINIMUM_MOVES:int = 3;
		private const LEVEL_3_MINIMUM_MOVES:int = 3;
		private const PARASITE_MINIMUM_MOVES:int = 3;
		
		// thesholds for decision making based on point differentials
		private const LEVEL_2_THRESHOLD_PERCENT:int = 10;  // inclination % to play level 2 if amount over theshold
		private const LEVEL_2_POINT_THESHOLD:int = 10;  // if player has over this percent of playable space as 2's, inclination for 2 goes up
		private const LEVEL_3_THRESHOLD_PERCENT:int = 10;  // inclination % to play level 3 if amount over theshold
		private const LEVEL_3_POINT_THESHOLD:int = 10;  // if player has over this percent of playable space as 3's, inclination for 2 goes up
		private const LEVEL_3_CONNECTED_TO_ENEMY_3_PERCENT:int = 30;  // inclination % to play level 3 if enemy 3's are connected to player's level 3
		private const PARASITE_POINT_THESHOLD:int = 10;  // inclination % to play parasite if over the thresholds
		private const PARASITE_ON_3_THRESHOLD_1_PERCENT:int = 8;  // if enemy has over this percent of playable space, inclination for parasite goes up
		private const PARASITE_ON_3_THRESHOLD_2_PERCENT:int = 16;  // if enemy has over this percent of playable space, inclination for parasite goes up even more
		private const PARASITE_ON_3_THRESHOLD_3_PERCENT:int = 24;  // if enemy has over this percent of playable space, inclination for parasite goes up even more
		private const LEVEL_3_OCCUPY_POINT_THRESHOLD:int = 10;  // inclination % to play level 3 if over the threshold
		private const LEVEL_3_OCCUPY_1_PERCENT:int = 8;  // if player has over this percent of playable space in connected level 3s, inclination to play level 3 goes up
		private const LEVEL_3_OCCUPY_2_PERCENT:int = 16;  // if player has over this percent of playable space in connected level 3s, inclination to play level 3 goes up even more
		private const LEVEL_3_OCCUPY_3_PERCENT:int = 24;  // if player has over this percent of playable space in connected level 3s, inclination to play level 3 goes up even more
		private const PARASITE_ON_2_THRESHOLD_1_PERCENT:int = 15;  // if enemy has over this percent of playable space, inclination for parasite goes up
		private const PARASITE_ON_2_THRESHOLD_2_PERCENT:int = 30;  // if enemy has over this percent of playable space, inclination for parasite goes up even more
		private const PARASITE_ON_2_THRESHOLD_3_PERCENT:int = 45;  // if enemy has over this percent of playable space, inclination for parasite goes up even more
		private const LEVEL_2_OCCUPY_POINT_THRESHOLD:int = 10;  // inclination % to play level 2 if over the threshold
		private const LEVEL_2_OCCUPY_1_PERCENT:int = 8;  // if player has over this percent of playable space in connected level 2s, inclination to play level 3 goes up
		private const LEVEL_2_OCCUPY_2_PERCENT:int = 16;  // if player has over this percent of playable space in connected level 2s, inclination to play level 3 goes up even more
		private const LEVEL_2_OCCUPY_3_PERCENT:int = 24;  // if player has over this percent of playable space in connected level 2s, inclination to play 
		
		// board covereage needed to use a level 3 on a small area
		private const PLAYABLE_TILES_NEEDED_FOR_LEVEL_3_SMALL_TAKEOVER:int = 16;
		private const LEVEL_3_TILES_FOR_SMALL_TAKEOVER:int = 4;
		
		// desperation move values
		private const DESPERATION_SPACES_AVAILABLE_PERCENT:int = 20;  // amount of board with open spaces for enemy to consider desperation move
		private const DESPERATION_OCCUPY_THRESHOLD:int = 30;  // if enemy has this much or less of the board, enemy will make desperation move
		private const DESPERATION_PERCENT_INCREASE:int = 30;
		
		private const HIGHEST_DIFFERENTIAL_FACTOR:int = 5;  // if the highest differential score is more than 5 times the second highest, always pick the highest score
		
		private var gameManager:GameManager;
		
		private var tileScores:Array;
		private var numPlayers:int;
		private var originalScores:Array = [];
		private var virtualScores:Array = [];
		private var blankTiles:Array = [];  // array of ints for locations of blank tiles
		private var gameBoard:Array;
		private var totalPlayableTiles:int;
		private var gameBoardSize:int;
		private var moveTimer:int;
		private var difficultyLevel:int;  // 1 = easy, 2 = medium, 3 = hard
		
		// variables specific to tile selector types
		// best moves for all tiles
		private var level1BestMoves:Array = [];  // array of AIBestMoves for level 1 tiles
		private var level2BestMoves:Array = [];  // array of AIBestMoves for level 2 tiles
		private var level3BestMoves:Array = [];  // array of AIBestMoves for level 3 tiles
		private var parasiteBestMoves:Array = [];  // array of AIBestMoves for parasite tiles
		
		// percentages to use tiles > level 1
		private var level2Percent:int;
		private var level3Percent:int;
		private var parasitePercent:int;
		
		// how many tiles the player has, and how many turns they have to get another
		private var level2Tiles:int;  // # level 2 tiles
		private var level2Turns:int;  // turns til next level 2
		private var level3Tiles:int;  // # level 3 tiles
		private var level3Turns:int;  // turns til next level 3
		private var parasite:Boolean;  // has parasite tile
		private var parasiteTurns:int;  // # turns til parasite
		
		// these are used for the single player campaign levels, of which some do not allow certain special tiles
		private var allowLevel2:Boolean = false;
		private var allowLevel3:Boolean = false;
		private var allowParasite:Boolean = false;
		private var captureZoneLevel:Boolean = false;
		
		// static stuff used by AITiles
		public static var originalBoard:Array = [];  // array of AITiles that is a copy of the gameBoard
		public static var selectedTileLevel:int;  // the tile level being played - used for projections
		public static var virtualBoard:Array = [];  // array of AITiles that changes based on projections
		public static var currentPlayer:Player;  // the current player
		public static var tileScanState:int;  // the state used for scanning - connections of same tiles
		public static var hitsCurrentPlayerThrees:Boolean;
		public static var teamsWithConnectedTiles:Array = [];
		
		private var verbose:Boolean = false;
		//public static var debugCalc:Boolean = false;
		
		public function AI(inGameManager:GameManager, inBoard:Array, inNumPlayers:int):void
		{
			super();
			
			gameManager = inGameManager;
			gameBoard = inBoard;
			gameBoardSize = 0;
			moveTimer = 0;
			
			//trace("in constructor, gameboard length = " + gameBoard.length);
			
			originalBoard = [];
			virtualBoard = [];
			var newAITile:AITile;
			var i:int;
			
			// build the virtual board
			for (i = 0; i < gameBoard.length; i++)
			{
				// add tile to the original board
				newAITile = new AITile(gameBoard[i].location);
				newAITile.state = gameBoard[i].state;
				if (gameBoard[i].owner != null)
				{
					newAITile.team = gameBoard[i].owner.playerTeam;
					newAITile.playerOwner = gameBoard[i].owner.getPlayerArrayIndex();
				}
				else
					newAITile.team = -1;
				
				newAITile.activeCapturePoint = gameBoard[i].activeCapturePoint;
				
				if (gameBoard[i].adjacentLeftCell != null)
					newAITile.adjacentLeftTileLoc = gameBoard[i].adjacentLeftCell.location;
				else
					newAITile.adjacentLeftTileLoc = -1;
				if (gameBoard[i].adjacentRightCell != null)
					newAITile.adjacentRightTileLoc = gameBoard[i].adjacentRightCell.location;
				else
					newAITile.adjacentRightTileLoc = -1;
				if (gameBoard[i].adjacentTopCell != null)
					newAITile.adjacentTopTileLoc = gameBoard[i].adjacentTopCell.location;
				else
					newAITile.adjacentTopTileLoc = -1;
				if (gameBoard[i].adjacentBottomCell != null)
					newAITile.adjacentBottomTileLoc = gameBoard[i].adjacentBottomCell.location;
				else
					newAITile.adjacentBottomTileLoc = -1;
				originalBoard.push(newAITile);
				
				// add tile to the living board
				newAITile = new AITile(gameBoard[i].location);
				newAITile.state = gameBoard[i].state;
				if (gameBoard[i].owner != null)
				{
					newAITile.team = gameBoard[i].owner.playerTeam;
					newAITile.playerOwner = gameBoard[i].owner.getPlayerArrayIndex();
				}
				else
					newAITile.team = -1;
				newAITile.activeCapturePoint = gameBoard[i].activeCapturePoint;
				
				if (gameBoard[i].adjacentLeftCell != null)
					newAITile.adjacentLeftTileLoc = gameBoard[i].adjacentLeftCell.location;
				else
					newAITile.adjacentLeftTileLoc = -1;
				if (gameBoard[i].adjacentRightCell != null)
					newAITile.adjacentRightTileLoc = gameBoard[i].adjacentRightCell.location;
				else
					newAITile.adjacentRightTileLoc = -1;
				if (gameBoard[i].adjacentTopCell != null)
					newAITile.adjacentTopTileLoc = gameBoard[i].adjacentTopCell.location;
				else
					newAITile.adjacentTopTileLoc = -1;
				if (gameBoard[i].adjacentBottomCell != null)
					newAITile.adjacentBottomTileLoc = gameBoard[i].adjacentBottomCell.location;
				else
					newAITile.adjacentBottomTileLoc = -1;
				virtualBoard.push(newAITile);
				
				// if tile is playable, add it to the gameboard size
				if (gameBoard[i].state != 9 && gameBoard[i].state != 10)
				{
					gameBoardSize++;
				}
			}
			
			//trace("gameboard size = " + gameBoardSize);
			
			numPlayers = inNumPlayers;
			tileScores = new Array(1, 2, 4, 6);  // level 1, 2, 3 and 4 cell values on the board - used for score differentials
			
			//trace("numPlayers = " + numPlayers);
			
			for (i = 0; i < numPlayers; i++)
			{
				originalScores.push(0);
				virtualScores.push(0);
			}
			
			// set up tile selector allowance for into campaign levels
			if (Main.campaign != null)
			{
				var levelNumber:int = Main.campaign.currentCampaignLevel.levelNumber;
				
				if (levelNumber == 0)
				{
					allowLevel2 = false;
					allowLevel3 = false;
					allowParasite = false;
				}
				else if (levelNumber == 1)
				{
					allowLevel2 = true;
					allowLevel3 = false;
					allowParasite = false;
				}
				else if (levelNumber == 2)
				{
					allowLevel2 = true;
					allowLevel3 = true;
					allowParasite = false;
				}
				else if (levelNumber == 3)
				{
					allowLevel2 = true;
					allowLevel3 = true;
					allowParasite = false;
				}
				else if (levelNumber > 3)
				{
					allowLevel2 = true;
					allowLevel3 = true;
					allowParasite = true;
				}
				
				// set up capture zones for choosing tile weights
				if (levelNumber == 5 || levelNumber == 7 || levelNumber == 13 || levelNumber == 14 || levelNumber == 17 || levelNumber == 18)
					captureZoneLevel = true;
				
				// if level 1-4, always make easy
				if (levelNumber < 4)
					difficultyLevel = 1;
				else 
					difficultyLevel = Main.campaign.difficultySelector.difficulty;
			}
			else
			{
				allowLevel2 = true;
				allowLevel3 = true;
				allowParasite = true;
				
				//difficultyLevel = gameManager.getMain().getGameSetupDifficulty();
				//trace("difficultyLevel = " + difficultyLevel);
			}
		}
		
		public function copyBoard(inPlayer:Player):void
		{
			/*if (inPlayer.getAiControl())
			verbose = true;
			else
			verbose = false;
			
			if (verbose)
			trace("*** AI PLAYER " + inPlayer.playerTeam + " ***");*/
			
			var openTiles:int = 0;
			var currentPlayerOccupiedTiles:int = 0;
			var desperate:Boolean = false;
			var i:int;
			var output:String;
			
			/*
			for (var i:int = 0; i < gameManager.playerArray.length; i++)
			{
			trace("player " + i);
			trace("level 1 tiles = " + gameManager.playerArray[i].cellSelectorArray[0].currentNumOfSavedTiles + " num needed for next = " + gameManager.playerArray[i].cellSelectorArray[0].numNeeded);
			trace("level 2 tiles = " + gameManager.playerArray[i].cellSelectorArray[1].currentNumOfSavedTiles + " num needed for next = " + gameManager.playerArray[i].cellSelectorArray[1].numNeeded);
			trace("level 3 tiles = " + gameManager.playerArray[i].cellSelectorArray[2].currentNumOfSavedTiles + " num needed for next = " + gameManager.playerArray[i].cellSelectorArray[2].numNeeded);
			trace("parasite tiles = " + gameManager.playerArray[i].cellSelectorArray[3].currentNumOfSavedTiles + " num needed for next = " + gameManager.playerArray[i].cellSelectorArray[3].numNeeded);
			}
			*/
			
			// set current player
			currentPlayer = inPlayer;
			
			// set all selector data
			if (allowLevel2)
			{
				level2Tiles = currentPlayer.cellSelectorArray[1].currentNumOfSavedTiles;
				//level2Turns = currentPlayer.cellSelectorArray[1].getNumNeeded();
				level2Turns = currentPlayer.cellSelectorArray[1].getNumNeededForNextTile();
			}
			else
			{
				level2Tiles = 0;
				level2Turns = 0;
			}
			if (allowLevel3)
			{
				level3Tiles = currentPlayer.cellSelectorArray[2].currentNumOfSavedTiles;
				//level3Turns = currentPlayer.cellSelectorArray[2].getNumNeeded();
				level3Turns = currentPlayer.cellSelectorArray[2].getNumNeededForNextTile();
			}
			else
			{
				level3Tiles = 0;
				level3Turns = 0;
			}
			if (allowParasite)
			{
				if (currentPlayer.cellSelectorArray[3].currentNumOfSavedTiles > 0)
					parasite = true;
				else
					parasite = false;
				//parasiteTurns = currentPlayer.cellSelectorArray[3].getNumNeeded();
				parasiteTurns = currentPlayer.cellSelectorArray[3].getNumNeededForNextTile();
			}
			else
			{
				parasite = false;
				parasiteTurns = 0;
			}
			
			//trace("AI player " + currentPlayer.playerTeam + " 2tiles = " + level2Tiles + " 2turns = " + level2Turns + " 3tiles = " + level3Tiles + " 3turns = " + level3Turns);
			
			//trace("copying board for player " + currentPlayer.playerTeam);
			
			totalPlayableTiles = 0;
			
			// build the virtual board
			for (i = 0; i < gameBoard.length; i++)
			{
				originalBoard[i].state = gameBoard[i].state;
				
				// get total playable tiles
				if (originalBoard[i].state >= 0 && originalBoard[i].state <= 3)
					totalPlayableTiles++;
				
				// get open tiles
				if (originalBoard[i].state == 0)
					openTiles++;
				
				// get the tiles occupied by the current player
				if (originalBoard[i].team == currentPlayer.playerTeam)
					currentPlayerOccupiedTiles++;
				
				// assign the team
				if (gameBoard[i].owner != null)
				{
					originalBoard[i].team = gameBoard[i].owner.playerTeam;
					//trace("in copy board, set team for tile " + i + " to 
				}
				else
					originalBoard[i].team = null;
				
				if (verbose)
				{
					if (originalBoard[i].team == -1)
						trace("found -1 at " + i);
				}
				
				// default variables
				originalBoard[i].hasAlreadySpilledOver = false;
				originalBoard[i].hasCounted = false;
				originalBoard[i].hasCountedThrees = false;
				originalBoard[i].hasCountedTwos = false;
				
				virtualBoard[i].state = gameBoard[i].state;
				if (gameBoard[i].owner != null)
					virtualBoard[i].team = gameBoard[i].owner.playerTeam;
				else
					virtualBoard[i].team = null;
				virtualBoard[i].hasAlreadySpilledOver = false;
			}
			
			var percentOpen:int = (openTiles / gameBoardSize) * 100;
			
			//trace("percent Open = " + percentOpen);
			
			// check if player is desperate
			// check if amount of open tiles is less than the desperation percentage threshold
			if (percentOpen <= DESPERATION_SPACES_AVAILABLE_PERCENT)
			{
				var currentPlayerOccupiedPercent:Number = (currentPlayerOccupiedTiles / gameBoardSize) * 100;
				
				//trace("percent occupied by player = " + currentPlayerOccupiedPercent);
				
				// if it is, check if the amount of the board occupied by the player is less than the threshold
				if (currentPlayerOccupiedPercent <= DESPERATION_OCCUPY_THRESHOLD)
					desperate = true;
			}
			
			// set base usage percentages for level 2 moves
			if (level2Tiles == 2)
				level2Percent = 75;
			else if (level2Tiles == 1)
			{
				level2Percent = 40;
				
				if (level2Turns <= 1)
				{
					level2Percent += 10;
				}
				
				if (desperate)
					level3Percent += DESPERATION_PERCENT_INCREASE;
			}
			else
				level2Percent = 0;
			
			// set base usage percentages for level 3 moves
			if (level3Tiles == 2)
				level3Percent = 75;
			else if (level3Tiles == 1)
			{
				level3Percent = 30;
				
				if (level3Turns <= 2)
					level3Percent += 10;
				
				if (desperate)
					level3Percent += DESPERATION_PERCENT_INCREASE;
			}
			else
				level3Percent = 0;
			
			// set base usage percentages for parasite moves
			if (parasite)
				parasitePercent = 40;
			else
				parasitePercent = 0;
			
			//trace("tendencies: level2 - " + level2Percent + " level3 - " + level3Percent + " parasite - " + parasitePercent);
			
			assignWeights();
			selectBestMoves();
			
			if (verbose)
			{
				output = "WEIGHTS: ";
				for (i = 0; i < level1BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level1BestMoves[i].location + "] diff = " + level1BestMoves[i].weight;
				}
				//trace(output);
			}
			
			projectBestMoves();
			sortBestMovesByDifferential();
			
			/*
			if (level2BestMoves.length > 0)
			{
			var output:String = "LEVEL 2 DIFFS: ";
			for (var i:int = 0; i < level2BestMoves.length; i++)
			{
			if (i > 0)
			output += ", ";
			output += "loc[" + level2BestMoves[i].location + "] diff = " + level2BestMoves[i].differential;
			}
			trace(output);
			}
			*/
			
			/*
			var output:String = "";
			for (var i:int = 0; i < level1BestMoves.length; i++)
			{
			if (i > 0)
			output += ", ";
			output += "loc[" + level1BestMoves[i].location + "] diff = " + level1BestMoves[i].differential;
			}
			trace(output);
			*/
			/*
			//output weights
			var output:String = "";
			for (var i:int = 0; i < originalBoard.length; i++)
			{
			if (i > 0)
			output += ", ";
			output += originalBoard[i].weight[0];
			}
			*/
			
			/*
			// output scores
			var output:String = "";
			
			for (var i:int = 0; i < numPlayers; i++)
			{
			if (i > 0)
			output += ", ";
			output += "player " + (i + 1) + " = " + score[i];
			}
			trace("Pre-move - " + output);
			*/
			
			/*
			// output scores post-move
			var output:String = "";
			
			for (var i:int = 0; i < numPlayers; i++)
			{
			if (i > 0)
			output += ", ";
			output += "player " + (i + 1) + " = " + score[i];
			}
			trace("Post-move - " + output);
			*/
		}
		
		public function calcOriginalScore():void
		{
			var i:int;
			
			for (i = 0; i < numPlayers; i++)
				originalScores[i] = 0;
			
			for (i = 0; i < originalBoard.length; i++)
			{
				if (originalBoard[i].state > 0 && originalBoard[i].state < 5)
				{
					originalScores[originalBoard[i].team] += tileScores[originalBoard[i].state - 1];
				}
			}
		}
		
		public function calcVirtualScore():void
		{
			var i:int;
			
			for (i = 0; i < numPlayers; i++)
				virtualScores[i] = 0;
			
			for (i = 0; i < virtualBoard.length; i++)
			{
				if (virtualBoard[i].state > 0 && virtualBoard[i].state < 5)
				{
					virtualScores[virtualBoard[i].team] += tileScores[virtualBoard[i].state - 1];
					//if (debugCalc)
					//trace("added " + (tileScores[virtualBoard[i].state - 1]) + " to the virtual board for loc " + i + " and team " + virtualBoard[i].team);
				}
			}
			
			/*
			for (i = 0; i < numPlayers; i++)
			{
			if (debugCalc)
			trace("team " + i + " score = " + virtualScores[i]);
			}
			*/
		}
		
		public function resetVirtualBoard():void
		{
			// reset the virtual board
			for (var i:int = 0; i < virtualBoard.length; i++)
			{
				virtualBoard[i].state = originalBoard[i].state;
				virtualBoard[i].team = originalBoard[i].team;
				virtualBoard[i].hasAlreadySpilledOver = false;
			}
		}
		
		public function assignWeights():void
		{
			// parasite playing decision factors
			var connectedThrees:int = 0;
			var connectedUsableThrees:int = 0;
			var maxConnectedThrees:int = 0;
			var maxConnectedUsableThrees:int = 0;
			var addedParasiteOn3Theshold1:Boolean = false;
			var addedParasiteOn3Theshold2:Boolean = false;
			var addedParasiteOn3Theshold3:Boolean = false;
			var added3Threshold1:Boolean = false;
			var added3Threshold2:Boolean = false;
			var added3Threshold3:Boolean = false;
			var connectedTwos:int = 0;
			var connectedUsableTwos:int = 0;
			var maxConnectedTwos:int = 0;
			var maxConnectedUsableTwos:int = 0;
			var addedParasiteOn2Theshold1:Boolean = false;
			var addedParasiteOn2Theshold2:Boolean = false;
			var addedParasiteOn2Theshold3:Boolean = false;
			var added2Threshold1:Boolean = false;
			var added2Threshold2:Boolean = false;
			var added2Threshold3:Boolean = false;
			var boardCoverage:Number;
			
			// assign weights for each tile
			for (var i:int = 0; i < originalBoard.length; i++)
			{
				// if a playable space
				if (originalBoard[i].state >= 0 && originalBoard[i].state < 4)
				{
					// if the space is empty
					if (originalBoard[i].state == 0)
					{
						// set original weights - good for placing a level 1 tile
						originalBoard[i].weight[0] = GOOD_PLACE;
						originalBoard[i].weight[1] = BAD_PLACE;
						originalBoard[i].weight[2] = BAD_PLACE;
						originalBoard[i].weight[3] = BAD_PLACE;
						
						// check if capture point
						if (captureZoneLevel)
						{
							if (originalBoard[i].activeCapturePoint)
								originalBoard[i].weight[0] += GOOD_PLACE;
						}
						
						// check adjacent spaces
						// check left tile
						if (originalBoard[i].adjacentLeftTileLoc != -1)
						{
							// if adjacent is level 1
							if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].state == 1)
							{
								// if belongs to other team, subtract. otherwise, add
								if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].team != currentPlayer.playerTeam)
									originalBoard[i].weight[0] += BAD_ADJ;
								else
									originalBoard[i].weight[0] += GOOD_ADJ;
							}
								// if adjacent is gray level 1, add
							else if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].state == 5)
								originalBoard[i].weight[0] += BETTER_ADJ;
								// if adjacent is gray level 2, add
							else if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].state == 6)
								originalBoard[i].weight[1] += BETTER_ADJ;
								// if adjacent is gray level 3, add
							else if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].state == 7)
								originalBoard[i].weight[2] += BETTER_ADJ;
								// if adjacent is other gray, subtract
							else if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].state > 7)
								originalBoard[i].weight[0] += ADJ_PENALTY;
						}
						
						// check right tile
						if (originalBoard[i].adjacentRightTileLoc != -1)
						{
							// if adjacent is level 1
							if (originalBoard[virtualBoard[i].adjacentRightTileLoc].state == 1)
							{
								// if belongs to other team, subtract. otherwise, add
								if (originalBoard[virtualBoard[i].adjacentRightTileLoc].team != currentPlayer.playerTeam)
									originalBoard[i].weight[0] += BAD_ADJ;
								else
									originalBoard[i].weight[0] += GOOD_ADJ;
							}
								// if adjacent is gray level 1, add
							else if (originalBoard[virtualBoard[i].adjacentRightTileLoc].state == 5)
								originalBoard[i].weight[0] += BETTER_ADJ;
								// if adjacent is gray level 2, add
							else if (originalBoard[virtualBoard[i].adjacentRightTileLoc].state == 6)
								originalBoard[i].weight[1] += BETTER_ADJ;
								// if adjacent is gray level 3, add
							else if (originalBoard[virtualBoard[i].adjacentRightTileLoc].state == 7)
								originalBoard[i].weight[2] += BETTER_ADJ;
								// if adjacent is other gray, subtract
							else if (originalBoard[virtualBoard[i].adjacentRightTileLoc].state > 7)
								originalBoard[i].weight[0] += ADJ_PENALTY;
						}
						
						// check top tile
						if (originalBoard[i].adjacentTopTileLoc != -1)
						{
							// if adjacent is level 1
							if (originalBoard[virtualBoard[i].adjacentTopTileLoc].state == 1)
							{
								// if belongs to other team, subtract. otherwise, add
								if (originalBoard[virtualBoard[i].adjacentTopTileLoc].team != currentPlayer.playerTeam)
									originalBoard[i].weight[0] += BAD_ADJ;
								else
									originalBoard[i].weight[0] += GOOD_ADJ;
							}
								// if adjacent is gray level 1, add
							else if (originalBoard[virtualBoard[i].adjacentTopTileLoc].state == 5)
								originalBoard[i].weight[0] += BETTER_ADJ;
								// if adjacent is gray level 2, add
							else if (originalBoard[virtualBoard[i].adjacentTopTileLoc].state == 6)
								originalBoard[i].weight[1] += BETTER_ADJ;
								// if adjacent is gray level 3, add
							else if (originalBoard[virtualBoard[i].adjacentTopTileLoc].state == 7)
								originalBoard[i].weight[2] += BETTER_ADJ;
								// if adjacent is other gray, subtract
							else if (originalBoard[virtualBoard[i].adjacentTopTileLoc].state > 7)
								originalBoard[i].weight[0] += ADJ_PENALTY;
						}
						
						// check bottom tile
						if (originalBoard[i].adjacentBottomTileLoc != -1)
						{
							// if adjacent is level 1
							if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].state == 1)
							{
								// if belongs to other team, subtract. otherwise, add
								if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].team != currentPlayer.playerTeam)
									originalBoard[i].weight[0] += BAD_ADJ;
								else
									originalBoard[i].weight[0] += GOOD_ADJ;
							}
								// if adjacent is gray level 1, add
							else if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].state == 5)
								originalBoard[i].weight[0] += BETTER_ADJ;
								// if adjacent is gray level 2, add
							else if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].state == 6)
								originalBoard[i].weight[1] += BETTER_ADJ;
								// if adjacent is gray level 3, add
							else if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].state == 7)
								originalBoard[i].weight[2] += BETTER_ADJ;
								// if adjacent is other gray, subtract
							else if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].state > 7)
								originalBoard[i].weight[0] += ADJ_PENALTY;
						}
					}
					else if (originalBoard[i].state == 1)
					{
						//trace("found 1 level 1 tile in slot " + i + " current team = " + currentPlayer.playerTeam + " tile owner = " + originalBoard[i].team);
						
						if (originalBoard[i].team == currentPlayer.playerTeam)
						{
							// set original weights - good for placing a level 1 tile
							originalBoard[i].weight[0] = GOOD_PLACE + GOOD_PLACE;
							originalBoard[i].weight[1] = BAD_PLACE;
							originalBoard[i].weight[2] = BAD_PLACE;
							originalBoard[i].weight[3] = BAD_PLACE;
							
							// check adjacent spaces
							if (originalBoard[i].adjacentLeftTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].state == 1)
								{
									if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[0] += GOOD_ADJ;
								}
							}
							if (originalBoard[i].adjacentRightTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentRightTileLoc].state == 1)
								{
									if (originalBoard[virtualBoard[i].adjacentRightTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[0] += GOOD_ADJ;
								}
							}
							if (originalBoard[i].adjacentTopTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentTopTileLoc].state == 1)
								{
									if (originalBoard[virtualBoard[i].adjacentTopTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[0] += GOOD_ADJ;
								}
							}
							if (originalBoard[i].adjacentBottomTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].state == 1)
								{
									if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[0] += GOOD_ADJ;
								}
							}
						}
						else
						{
							// set original weights - bad for placing a level 1 tile, good for placing a level 2 tile
							originalBoard[i].weight[0] = UNPLAYABLE;
							originalBoard[i].weight[1] = GOOD_PLACE;
							originalBoard[i].weight[2] = BAD_PLACE;
							originalBoard[i].weight[3] = BAD_PLACE;
						}
					}
					else if (originalBoard[i].state == 2)
					{
						if (originalBoard[i].team == currentPlayer.playerTeam)
						{
							// set original weights - good for placing a level 2 tile
							originalBoard[i].weight[0] = UNPLAYABLE;
							originalBoard[i].weight[1] = GOOD_PLACE;
							originalBoard[i].weight[2] = BAD_PLACE;
							originalBoard[i].weight[3] = BAD_PLACE;
							
							// check adjacent spaces
							if (originalBoard[i].adjacentLeftTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].state == 2)
								{
									if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[1] += GOOD_ADJ;
								}
							}
							if (originalBoard[i].adjacentRightTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentRightTileLoc].state == 2)
								{
									if (originalBoard[virtualBoard[i].adjacentRightTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[1] += GOOD_ADJ;
								}
							}
							if (originalBoard[i].adjacentTopTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentTopTileLoc].state == 2)
								{
									if (originalBoard[virtualBoard[i].adjacentTopTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[1] += GOOD_ADJ;
								}
							}
							if (originalBoard[i].adjacentBottomTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].state == 2)
								{
									if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[1] += GOOD_ADJ;
								}
							}
							
							// check for connected 2 tiles
							if (!originalBoard[i].hasCountedTwosForUpgrade)
								connectedUsableTwos = originalBoard[i].numberOfAdjacentTwosForUpgrade();
							
							// if it is the most connected threes
							if (connectedUsableTwos > maxConnectedUsableTwos)
							{
								//trace("found " + connectedUsableThrees + " connected usable threes");
								
								maxConnectedUsableTwos = connectedUsableTwos;
								originalBoard[i].weight[1] += GOOD_PLACE * 2;
								
								boardCoverage = (maxConnectedUsableTwos / totalPlayableTiles) * 100
								
								//trace("board cov = " + boardCoverage);
								
								if ((boardCoverage > LEVEL_2_OCCUPY_1_PERCENT) && !added2Threshold1)
								{
									//trace("in here");
									
									added2Threshold1 = true;
									level2Percent += LEVEL_2_OCCUPY_POINT_THRESHOLD;
									
									if ((boardCoverage > LEVEL_2_OCCUPY_2_PERCENT) && !added2Threshold2)
									{
										level2Percent += LEVEL_2_OCCUPY_POINT_THRESHOLD;
										added2Threshold2 = true;
									}
									if ((boardCoverage > LEVEL_2_OCCUPY_3_PERCENT) && !added2Threshold3)
									{
										level2Percent += LEVEL_2_OCCUPY_POINT_THRESHOLD;
										added2Threshold3 = true;
									}
								}
								
								//trace("after calc, parasite percent = " + parasitePercent);
							}
						}
						else
						{
							// check for connected enemy 2 tiles - for parasite tendency
							if (!originalBoard[i].hasCountedTwos)
								connectedTwos = originalBoard[i].numberOfAdjacentTwos();
							
							// if it is the most connected threes
							if (connectedTwos > maxConnectedTwos)
							{
								//trace("checking connected enemy 2s");
								//trace(originalBoard[i].team);
								//trace(gameManager.playerArray.length);
								
								// check to see if the enemy who is connected has the ability to upgrade
								//trace("location = " + i + " team = " + (originalBoard[i].team));
								//var enemy2Tiles:int = gameManager.playerArray[originalBoard[i].team].cellSelectorArray[1].currentNumOfSavedTiles;
								//var enemy2Turns:int = gameManager.playerArray[originalBoard[i].team].cellSelectorArray[1].getNumNeeded();
								var enemy2Tiles:int = gameManager.playerArray[originalBoard[i].playerOwner].cellSelectorArray[1].currentNumOfSavedTiles;
								//var enemy2Turns:int = gameManager.playerArray[originalBoard[i].playerOwner].cellSelectorArray[1].getNumNeeded();
								var enemy2Turns:int = gameManager.playerArray[originalBoard[i].playerOwner].cellSelectorArray[1].getNumNeededForNextTile();
								
								//trace("enemy 2 tiles = " + enemy2Tiles + " enemy2Turns = " + enemy2Turns);
								
								// check to see if the enemy who is connected has the ability to upgrade
								if (enemy2Tiles > 0 || enemy2Turns <= 1)
								{
									//trace("found " + connectedTwos + " connected twos");
									
									maxConnectedTwos = connectedTwos;
									originalBoard[i].weight[3] = GOOD_PLACE + GOOD_PLACE;
									
									boardCoverage = (maxConnectedTwos / totalPlayableTiles) * 100
									
									//trace("board cov = " + boardCoverage);
									
									if ((boardCoverage > PARASITE_ON_2_THRESHOLD_1_PERCENT) && !addedParasiteOn2Theshold1)
									{
										//trace("in here");
										
										addedParasiteOn2Theshold1 = true;
										parasitePercent += PARASITE_POINT_THESHOLD;
										
										if ((boardCoverage > PARASITE_ON_2_THRESHOLD_2_PERCENT) && !addedParasiteOn2Theshold2)
										{
											parasitePercent += PARASITE_POINT_THESHOLD;
											addedParasiteOn2Theshold2 = true;
										}
										if ((boardCoverage > PARASITE_ON_2_THRESHOLD_3_PERCENT) && !addedParasiteOn2Theshold3)
										{
											parasitePercent += PARASITE_POINT_THESHOLD;
											addedParasiteOn2Theshold3 = true;
										}
									}
								}
								
								//trace("after calc, parasite percent = " + parasitePercent);
							}
							else
								originalBoard[i].weight[3] = BAD_PLACE;
							
							
							// set original weights - bad for placing a level 2 tile, good for placing a level 3 tile
							originalBoard[i].weight[0] = UNPLAYABLE;
							originalBoard[i].weight[1] = UNPLAYABLE;
							originalBoard[i].weight[2] = GOOD_PLACE;
						}
					}
					// state == 3
					else
					{
						if (originalBoard[i].team == currentPlayer.playerTeam)
						{
							// set original weights - good for placing a level 3 tile
							originalBoard[i].weight[0] = UNPLAYABLE;
							originalBoard[i].weight[1] = UNPLAYABLE;
							originalBoard[i].weight[2] = GOOD_PLACE;
							originalBoard[i].weight[3] = BAD_PLACE;
							
							// check adjacent spaces
							if (originalBoard[i].adjacentLeftTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].state == 3)
								{
									if (originalBoard[virtualBoard[i].adjacentLeftTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[2] += GOOD_ADJ;
								}
							}
							if (originalBoard[i].adjacentRightTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentRightTileLoc].state == 3)
								{
									if (originalBoard[virtualBoard[i].adjacentRightTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[2] += GOOD_ADJ;
								}
							}
							if (originalBoard[i].adjacentTopTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentTopTileLoc].state == 3)
								{
									if (originalBoard[virtualBoard[i].adjacentTopTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[2] += GOOD_ADJ;
								}
							}
							if (originalBoard[i].adjacentBottomTileLoc != -1)
							{
								if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].state == 3)
								{
									if (originalBoard[virtualBoard[i].adjacentBottomTileLoc].team == currentPlayer.playerTeam)
										originalBoard[i].weight[2] += GOOD_ADJ;
								}
							}
							
							// check for connected 3 tiles
							if (!originalBoard[i].hasCountedThrees)
								connectedUsableThrees = originalBoard[i].numberOfAdjacentThrees();
							
							// RIGHT HERE!
							// if it is the most connected threes
							if (connectedUsableThrees > maxConnectedUsableThrees)
							{
								if (totalPlayableTiles > PLAYABLE_TILES_NEEDED_FOR_LEVEL_3_SMALL_TAKEOVER && connectedUsableThrees < LEVEL_3_TILES_FOR_SMALL_TAKEOVER)
								{
									// less chance of using a level 3...not enough board will be taken
								}
								else
								{
									//trace("found " + connectedUsableThrees + " connected usable threes");
									
									maxConnectedUsableThrees = connectedUsableThrees;
									originalBoard[i].weight[2] += GOOD_PLACE * 2;
									
									boardCoverage = (maxConnectedUsableThrees / totalPlayableTiles) * 100
									
									//trace("board cov = " + boardCoverage);
									
									if ((boardCoverage > LEVEL_3_OCCUPY_1_PERCENT) && !added3Threshold1)
									{
										//trace("in here");
										
										added3Threshold1 = true;
										level3Percent += LEVEL_3_OCCUPY_POINT_THRESHOLD;
										
										if ((boardCoverage > LEVEL_3_OCCUPY_2_PERCENT) && !added3Threshold2)
										{
											level3Percent += LEVEL_3_OCCUPY_POINT_THRESHOLD;
											added3Threshold2 = true;
										}
										if ((boardCoverage > LEVEL_3_OCCUPY_3_PERCENT) && !added3Threshold3)
										{
											level3Percent += LEVEL_3_OCCUPY_POINT_THRESHOLD;
											added3Threshold3 = true;
										}
									}
								}
								//trace("after calc, parasite percent = " + parasitePercent);
							}
							//trace("for loc " + i + " connectedUsableThrees = " + connectedUsableThrees);
						}
						else
						{
							// set original weights - bad for placing a level 3 tile, good for placing a parasite
							originalBoard[i].weight[0] = UNPLAYABLE;
							originalBoard[i].weight[1] = UNPLAYABLE;
							originalBoard[i].weight[2] = UNPLAYABLE;
							originalBoard[i].weight[3] = GOOD_PLACE;
							
							hitsCurrentPlayerThrees = false;
							
							// check for connected enemy 3 tiles
							if (!originalBoard[i].hasCounted)
								connectedThrees = originalBoard[i].numberOfAdjacentSameTiles();
							
							// if it is the most connected threes
							if (connectedThrees > maxConnectedThrees)
							{
								// check to see if the enemy who is connected has the ability to upgrade
								//var enemy3Tiles:int = gameManager.playerArray[originalBoard[i].team].cellSelectorArray[2].currentNumOfSavedTiles;
								//var enemy3Turns:int = gameManager.playerArray[originalBoard[i].team].cellSelectorArray[2].getNumNeeded();
								var enemy3Tiles:int = gameManager.playerArray[originalBoard[i].playerOwner].cellSelectorArray[2].currentNumOfSavedTiles;
								//var enemy3Turns:int = gameManager.playerArray[originalBoard[i].playerOwner].cellSelectorArray[2].getNumNeeded();
								var enemy3Turns:int = gameManager.playerArray[originalBoard[i].playerOwner].cellSelectorArray[2].getNumNeededForNextTile();
								
								//trace("enemy 2 tiles = " + enemy2Tiles + " enemy2Turns = " + enemy2Turns);
								
								// if the enemy can upgrade, increase parasite chance
								if (enemy3Tiles > 0 || enemy3Turns <= 1)
								{
									//trace("found " + connectedThrees + " connected threes");
									
									maxConnectedThrees = connectedThrees;
									originalBoard[i].weight[3] += GOOD_PLACE;
									
									boardCoverage = (maxConnectedThrees / totalPlayableTiles) * 100
									
									//trace("board cov = " + boardCoverage);
									
									if (hitsCurrentPlayerThrees && (level3Tiles > 0))
									{
										//trace("connected to enemy 3s");
										level3Percent += LEVEL_3_CONNECTED_TO_ENEMY_3_PERCENT;
									}
									else if ((boardCoverage > PARASITE_ON_3_THRESHOLD_1_PERCENT) && !addedParasiteOn3Theshold1 && !hitsCurrentPlayerThrees)
									{
										//trace("in here");
										
										addedParasiteOn3Theshold1 = true;
										parasitePercent += PARASITE_POINT_THESHOLD;
										
										if ((boardCoverage > PARASITE_ON_3_THRESHOLD_2_PERCENT) && !addedParasiteOn3Theshold2)
										{
											parasitePercent += PARASITE_POINT_THESHOLD;
											addedParasiteOn3Theshold2 = true;
										}
										if ((boardCoverage > PARASITE_ON_3_THRESHOLD_3_PERCENT) && !addedParasiteOn3Theshold3)
										{
											parasitePercent += PARASITE_POINT_THESHOLD;
											addedParasiteOn3Theshold3 = true;
										}
									}
								}
								//trace("after calc, parasite percent = " + parasitePercent);
							}
						}
					}
				}
				else
				{
					originalBoard[i].weight[0] = UNPLAYABLE;
					originalBoard[i].weight[1] = UNPLAYABLE;
					originalBoard[i].weight[2] = UNPLAYABLE;
					originalBoard[i].weight[3] = UNPLAYABLE;
				}
				
				//trace("spot " + i + " has weight = " + originalBoard[i].weight[0]);
			}
		}
		
		public function selectBestMoves():void
		{
			var i:int;
			var output:String;
			
			if (verbose)
				trace("selecting best moves for player " + currentPlayer.playerTeam);
			
			// clear best move arrays
			//bestMoves = new Array(new Array(), new Array(), new Array(), new Array());
			level1BestMoves = [];
			level2BestMoves = [];
			level3BestMoves = [];
			parasiteBestMoves = [];
			
			// clear blank tiles array
			blankTiles = [];
			
			// initialize best weight array
			//var bestWeights:Array = [];
			//for (var i:int = 0; i < 4; i++)
			//bestWeights.push(0);
			
			// trace("original board length = " + originalBoard.length);
			
			// find the best weight
			for (i = 0; i < originalBoard.length; i++)
			{
				for (var j:int = 0; j < 4; j++)
				{
					/*
					if (originalBoard[i].weight[j] > bestWeights[j])
					{
					bestWeights[j] = originalBoard[i].weight[j];
					//bestMoves[j] = new Array();
					bestMoves[j].push(i);
					}
					else if (originalBoard[i].weight[j] == bestWeights[j])
					bestMoves[j].push(i);
					*/
					//if (originalBoard[i].weight[j] > bestWeights[j])
					
					var bestMove:AIBestMove;
					
					if (originalBoard[i].weight[j] > 0)
					{
						if (j == 0)
						{
							//level1BestMoves = [];
							bestMove = new AIBestMove(originalBoard[i].location, originalBoard[i].weight[j]);
							level1BestMoves.push(bestMove);
							//bestWeights[j] = originalBoard[i].weight[j];
						}
						else if (j == 1)
						{
							//level2BestMoves = [];
							bestMove = new AIBestMove(originalBoard[i].location, originalBoard[i].weight[j]);
							level2BestMoves.push(bestMove);
							//bestWeights[j] = originalBoard[i].weight[j];
						}
						else if (j == 2)
						{
							//level3BestMoves = [];
							bestMove = new AIBestMove(originalBoard[i].location, originalBoard[i].weight[j]);
							level3BestMoves.push(bestMove);
							//bestWeights[j] = originalBoard[i].weight[j];
						}
						else
						{
							//parasiteBestMoves = [];
							bestMove = new AIBestMove(originalBoard[i].location, originalBoard[i].weight[j]);
							parasiteBestMoves.push(bestMove);
							//bestWeights[j] = originalBoard[i].weight[j];
						}
					}
					/*
					else if (originalBoard[i].weight[j] == bestWeights[j])
					{
					if (j == 0)
					{
					var bestMove = new AIBestMove(originalBoard[i].location, originalBoard[i].weight[j]);
					level1BestMoves.push(bestMove);
					}
					else if (j == 1)
					{
					var bestMove = new AIBestMove(originalBoard[i].location, originalBoard[i].weight[j]);
					level2BestMoves.push(bestMove);
					}
					else if (j == 2)
					{
					var bestMove = new AIBestMove(originalBoard[i].location, originalBoard[i].weight[j]);
					level3BestMoves.push(bestMove);
					}
					else
					{
					var bestMove = new AIBestMove(originalBoard[i].location, originalBoard[i].weight[j]);
					parasiteBestMoves.push(bestMove);
					}
					} */
				}
				if (originalBoard[i].state == 0)
					blankTiles.push(originalBoard[i].location);
			}
			
			/*
			var output:String = "";
			for (var i:int = 0; i < blankTiles.length; i++)
			{
			if (i > 0)
			output += ", ";
			output += "[" + i + "] = " + blankTiles[i];
			}
			trace("Blank tiles: " + output);
			*/
			
			if (verbose)
			{
				trace ("PRE SELECTION");
				output = "";
				
				for (i = 0; i < level1BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level1BestMoves[i].location + "] = " + level1BestMoves[i].weight;
				}
				trace("level 1 moves:" + output);
				
				output = "";
				
				for (i = 0; i < level2BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level2BestMoves[i].location + "] = " + level2BestMoves[i].weight;
				}
				trace("level 2 moves:" + output);
				
				output = "";
				
				for (i = 0; i < level3BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level3BestMoves[i].location + "] = " + level3BestMoves[i].weight;
				}
				trace("level 3 moves:" + output);
				
				output = "";
				
				for (i = 0; i < parasiteBestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + parasiteBestMoves[i].location + "] = " + parasiteBestMoves[i].weight;
				}
				trace("parasite moves:" + output);
			}
			
			// get the move percent, based on difficulty level
			var movePercent:int = 0;
			if (difficultyLevel == 1)
				movePercent = EASY_TOP_WEIGHT_PERCENT;
			else if (difficultyLevel == 2)
				movePercent = MEDIUM_TOP_WEIGHT_PERCENT;
			else
				movePercent = HARD_TOP_WEIGHT_PERCENT;
			
			// sort the level 1 tile best moves
			var numberOfMoves:int = 0;  // used to get the number of moves in the moves lists
			level1BestMoves.sort(compareBestMovesByWeight);
			
			// trim level 1 tile best moves
			numberOfMoves = (movePercent / 100) * level1BestMoves.length;
			if (numberOfMoves < 0)
				numberOfMoves = 1;
			numberOfMoves--;
			//trace("number of moves for level 1 = " + numberOfMoves);
			for (i = (level1BestMoves.length - 1); i >= 0; i--)
			{
				if (i > numberOfMoves)
				{
					//trace("popped move " + i);
					level1BestMoves.pop();
				}
			}
			
			// if the player has a level 2 tile
			if (level2Tiles > 0)
			{
				// sort the level 2 tile best moves
				level2BestMoves.sort(compareBestMovesByWeight);
				
				// if more moves than the minimum number
				if (level2BestMoves.length > LEVEL_2_MINIMUM_MOVES)
				{
					// trim level 2 tile best moves
					numberOfMoves = (movePercent / 100) * level2BestMoves.length;
					if (numberOfMoves < 0)
						numberOfMoves = 1;
					numberOfMoves--;
					for (i = (level2BestMoves.length - 1); i >= 0; i--)
					{
						if (i > numberOfMoves)
							level2BestMoves.pop();
					}
				}
			}
			
			// if the player has a level 3 tile
			if (level3Tiles > 0)
			{
				// sort the level 3 tile best moves
				level3BestMoves.sort(compareBestMovesByWeight);
				
				// if more moves than the minimum number
				if (level3BestMoves.length > LEVEL_3_MINIMUM_MOVES)
				{
					// trim level 3 tile best moves
					numberOfMoves = (movePercent / 100) * level3BestMoves.length;
					if (numberOfMoves < 0)
						numberOfMoves = 1;	
					numberOfMoves--;	
					for (i = (level3BestMoves.length - 1); i >= 0; i--)
					{
						if (i > numberOfMoves)
							level3BestMoves.pop();
					}
				}
			}
			
			// if the player has a parasite
			if (parasite)
			{
				// sort the level 3 tile best moves
				parasiteBestMoves.sort(compareBestMovesByWeight);
				
				// if more moves than the minimum number
				if (parasiteBestMoves.length > PARASITE_MINIMUM_MOVES)
				{
					// trim level 3 tile best moves
					numberOfMoves = (movePercent / 100) * parasiteBestMoves.length;
					if (numberOfMoves < 0)
						numberOfMoves = 1;	
					numberOfMoves--;	
					for (i = (parasiteBestMoves.length - 1); i >= 0; i--)
					{
						if (i > numberOfMoves)
							parasiteBestMoves.pop();
					}
				}
			}
			
			if (verbose)
			{
				trace ("POST WEIGHT SELECTION - current player = " + currentPlayer.playerTeam);
				output = "";
				
				for (i = 0; i < level1BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level1BestMoves[i].location + "] = " + level1BestMoves[i].weight;
				}
				trace("level 1 moves:" + output);
				
				output = "";
				
				for (i = 0; i < level2BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level2BestMoves[i].location + "] = " + level2BestMoves[i].weight;
				}
				trace("level 2 moves:" + output);
				
				output = "";
				
				for (i = 0; i < level3BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level3BestMoves[i].location + "] = " + level3BestMoves[i].weight;
				}
				trace("level 3 moves:" + output);
				
				output = "";
				
				for (i = 0; i < parasiteBestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + parasiteBestMoves[i].location + "] = " + parasiteBestMoves[i].weight;
				}
				trace("parasite moves:" + output);
			}
			
			//trace("pre-sort there are " + level1BestMoves.length + " moves for level 1 tiles");
			
			/*
			// trim level 1 tile best moves
			numberOfMoves = (TOP_WEIGHT_PERCENT / 100) * level1BestMoves.length;
			if (numberOfMoves < 0)
			numberOfMoves = 1;
			
			for (var i:int = (level1BestMoves.length - 1); i >= 0; i--)
			{
			if (i > numberOfMoves)
			level1BestMoves.pop();
			}
			*/
			//trace("post-sort there are " + level1BestMoves.length + " moves for level 1 tiles");
			
			/*
			var output:String = "";
			
			for (var i:int = 0; i < level1BestMoves.length; i++)
			{
			if (i > 0)
			output += ", ";
			output += "loc[" + level1BestMoves[i].location + "] = " + level1BestMoves[i].weight;
			}
			trace("moves:" + output);
			*/
			/*
			var output:String = "";
			
			for (var i:int = 0; i < blankTiles.length; i++)
			{
			if (i > 0)
			output += ", ";
			output += blankTiles[i];
			}
			trace("after select moves: " + output);
			*/
		}
		
		public function projectBestMoves():void
		{
			var addedLevel2Theshold:Boolean = false;
			var addedLevel3Theshold:Boolean = false;
			var enemyCanUpgrade:Boolean = false;
			var enemy3Tiles:int = 0;
			var enemy3Turns:int = 0;
			var i:int;
			var j:int;
			
			// get base score for the board
			calcOriginalScore();
			
			var originalScore:int = 0;
			for (i = 0; i < numPlayers; i++)
			{
				if (gameManager.playerArray[i].playerTeam == currentPlayer.playerTeam)
				{
					originalScore += originalScores[i];
				}
				else
				{
					originalScore -= originalScores[i];
				}
			}
			
			if (verbose)
				trace("board original score = " + originalScore);
			
			// set projection change state to level 1
			selectedTileLevel = 0;
			
			// project level 1 tiles
			for (i = 0; i < level1BestMoves.length; i++)
			{
				virtualBoard[level1BestMoves[i].location].changeState();
				
				if (virtualBoard[level1BestMoves[i].location].state == 2)
				{
					tileScanState = 2;
					resetTileTraces();
					
					// initialize arrays for enemies with connected tiles
					teamsWithConnectedTiles = [];
					
					// if it touches
					if (virtualBoard[level1BestMoves[i].location].tileTouchesSameLevelDifferentPlayer())
					{
						//trace("touch");
						level1BestMoves.splice(i, 1);
					}
					else 
					{
						//trace("no touch");
						calcVirtualScore();
						level1BestMoves[i].differential = calcScoreDifferential();
						//trace("level 1 - differential for location " + level1BestMoves[i].location + " = " + level1BestMoves[i].differential);
						
						/*
						// if point differential above the level 2 theshold, increase chance of playing a level 2 tile
						if (!addedLevel2Theshold && level2BestMoves[i].differential > LEVEL_2_POINT_THESHOLD)
						{
						addedLevel2Theshold = true;
						level2Percent += LEVEL_2_THRESHOLD_PERCENT;
						//trace("2 level2Percent now = " + level2Percent);
						}
						*/
					}
				}
				else
				{
					calcVirtualScore();
					level1BestMoves[i].differential = calcScoreDifferential();
					//trace("level 1 - differential for location " + level1BestMoves[i].location + " = " + level1BestMoves[i].differential);
				}
				resetVirtualBoard();
			}
			
			// if the player has level 2 tiles
			if (level2Tiles > 0)
			{
				// set projection change state to level 2
				selectedTileLevel = 1;
				
				// project level 2 tiles
				for (i = (level2BestMoves.length - 1); i >= 0; i--)
				{
					virtualBoard[level2BestMoves[i].location].changeState();
					
					//trace("changed location " + level2BestMoves[i].location + " to level " + virtualBoard[level2BestMoves[i].location].state);
					
					// if the tile upgraded to a level 3 - check if connected to other players level 3s
					if (virtualBoard[level2BestMoves[i].location].state == 3)
					{
						// set up to check level 3 tiles
						tileScanState = 3;
						resetTileTraces();
						
						// initialize arrays for enemies with connected tiles
						teamsWithConnectedTiles = [];
						
						//trace("checking " + level2BestMoves[i].location);
						
						// if it touches enemy 3 tiles
						if (virtualBoard[level2BestMoves[i].location].tileTouchesSameLevelDifferentPlayer())
						{
							// check if touching enemy(s) can upgrade
							enemy3Tiles = 0;
							enemy3Turns = 0;
							enemyCanUpgrade = false;
							
							for (j = 0; j < teamsWithConnectedTiles.length; j++)
							{
								if (teamsWithConnectedTiles[j] != currentPlayer.playerTeam)
								{
									//trace("checking enemy " + teamsWithConnectedTiles[j]);
									enemy3Tiles = gameManager.playerArray[teamsWithConnectedTiles[j]].cellSelectorArray[2].currentNumOfSavedTiles;
									//enemy3Turns = gameManager.playerArray[teamsWithConnectedTiles[j]].cellSelectorArray[2].getNumNeeded();
									enemy3Turns = gameManager.playerArray[teamsWithConnectedTiles[j]].cellSelectorArray[2].getNumNeededForNextTile();
									
									//trace("enemy3Tiles = " + enemy3Tiles + " enemy3Turns = " + enemy3Turns);
									
									if (enemy3Tiles > 0 || enemy3Turns <= 1)
									{
										enemyCanUpgrade = true;
										break;
									}
								}
							}
							//trace("touch");
							
							//trace("enemyCanUpgrade = " + enemyCanUpgrade);
							
							if (enemyCanUpgrade)
							{
								// if enemy can upgrade, remove the move from the moves list
								level2BestMoves.splice(i, 1);
							}
							else
							{
								// otherwise, process normally
								calcVirtualScore();
								level2BestMoves[i].differential = calcScoreDifferential();
								//trace("level 2 - differential for location " + level2BestMoves[i].location + " = " + level2BestMoves[i].differential);
								
								// if point differential above the level 2 theshold, increase chance of playing a level 2 tile
								if (!addedLevel2Theshold && level2BestMoves[i].differential > LEVEL_2_POINT_THESHOLD)
								{
									addedLevel2Theshold = true;
									level2Percent += LEVEL_2_THRESHOLD_PERCENT;
									//trace("2 level2Percent now = " + level2Percent);
								}
							}
						}
							// not touching enemy 3 tiles
						else 
						{
							//trace("no touch");
							calcVirtualScore();
							level2BestMoves[i].differential = calcScoreDifferential();
							//trace("level 2 - differential for location " + level2BestMoves[i].location + " = " + level2BestMoves[i].differential);
							
							// if point differential above the level 2 theshold, increase chance of playing a level 2 tile
							if (!addedLevel2Theshold && level2BestMoves[i].differential > LEVEL_2_POINT_THESHOLD)
							{
								addedLevel2Theshold = true;
								level2Percent += LEVEL_2_THRESHOLD_PERCENT;
								//trace("2 level2Percent now = " + level2Percent);
							}
						}
					}
					else if (virtualBoard[level2BestMoves[i].location].state == 2)
					{
						// set up to check level 2 tiles
						tileScanState = 2;
						resetTileTraces();
						
						// initialize arrays for enemies with connected tiles
						teamsWithConnectedTiles = [];
						
						// if it touches
						//trace("checking " + level2BestMoves[i].location);
						if (virtualBoard[level2BestMoves[i].location].tileTouchesSameLevelDifferentPlayer())
						{
							// check if touching enemy(s) can upgrade
							var enemy2Tiles:int = 0;
							var enemy2Turns:int = 0;
							enemyCanUpgrade = false;
							
							for (j = 0; j < teamsWithConnectedTiles.length; j++)
							{
								if (teamsWithConnectedTiles[j] != currentPlayer.playerTeam)
								{
									//trace("checking enemy " + teamsWithConnectedTiles[j]);
									enemy2Tiles = gameManager.playerArray[teamsWithConnectedTiles[j]].cellSelectorArray[1].currentNumOfSavedTiles;
									//enemy2Turns = gameManager.playerArray[teamsWithConnectedTiles[j]].cellSelectorArray[1].getNumNeeded();
									enemy2Turns = gameManager.playerArray[teamsWithConnectedTiles[j]].cellSelectorArray[1].getNumNeededForNextTile();
									
									//trace("enemy3Tiles = " + enemy3Tiles + " enemy3Turns = " + enemy3Turns);
									
									if (enemy3Tiles > 0 || enemy3Turns <= 1)
									{
										enemyCanUpgrade = true;
										break;
									}
								}
							}
							
							//trace("touch");
							
							if (enemyCanUpgrade)
							{
								// if enemy can upgrade, remove the move from the moves list
								level2BestMoves.splice(i, 1);
							}
							else
							{
								// otherwise, process normally
								calcVirtualScore();
								level2BestMoves[i].differential = calcScoreDifferential();
							}
						}
						else 
						{
							//trace("no touch");
							calcVirtualScore();
							level2BestMoves[i].differential = calcScoreDifferential();
							//trace("level 2 - differential for location " + level2BestMoves[i].location + " = " + level2BestMoves[i].differential);
							
							/*
							// if point differential above the level 2 theshold, increase chance of playing a level 2 tile
							if (!addedLevel2Theshold && level2BestMoves[i].differential > LEVEL_2_POINT_THESHOLD)
							{
							addedLevel2Theshold = true;
							level2Percent += LEVEL_2_THRESHOLD_PERCENT;
							//trace("2 level2Percent now = " + level2Percent);
							}
							*/
						}
					}
					else
					{
						calcVirtualScore();
						//trace("calculating differential for location " + level2BestMoves[i].location);
						level2BestMoves[i].differential = calcScoreDifferential();
						//trace("level 2 - differential for location " + level2BestMoves[i].location + " = " + level2BestMoves[i].differential);
						
						// if point differential above the level 2 theshold, increase chance of playing a level 2 tile
						if (!addedLevel2Theshold && level2BestMoves[i].differential > LEVEL_2_POINT_THESHOLD)
						{
							addedLevel2Theshold = true;
							level2Percent += LEVEL_2_THRESHOLD_PERCENT;
							//trace("1 level2Percent now = " + level2Percent);
						}
					}
					resetVirtualBoard();
				}
			}
			
			// if the player has level 3 tiles
			if (level3Tiles > 0)
			{
				// set projection change state to level 3
				selectedTileLevel = 2;
				
				// project level 3 tiles
				for (i = 0; i < level3BestMoves.length; i++)
				{
					/*
					if (level3BestMoves[i].location == 100)
					debugCalc = true;
					else
					debugCalc = false;
					*/
					
					var removed:Boolean = false;
					
					//if (debugCalc)
					//trace("changing state for " + level3BestMoves[i].location + " - tile level = " + selectedTileLevel);
					
					virtualBoard[level3BestMoves[i].location].changeState();
					
					if (virtualBoard[level3BestMoves[i].location].state == 3)
					{
						// set up to check level 3 tiles
						tileScanState = 3;
						resetTileTraces();
						
						// initialize arrays for enemies with connected tiles
						teamsWithConnectedTiles = [];
						
						// if it touches
						//trace("checking " + level2BestMoves[i].location);
						if (virtualBoard[level3BestMoves[i].location].tileTouchesSameLevelDifferentPlayer())
						{
							// check if touching enemy(s) can upgrade
							enemy3Tiles = 0;
							enemy3Turns = 0;
							enemyCanUpgrade = false;
							
							for (j = 0; j < teamsWithConnectedTiles.length; j++)
							{
								if (teamsWithConnectedTiles[j] != currentPlayer.playerTeam)
								{
									//trace("checking enemy " + teamsWithConnectedTiles[j]);
									enemy3Tiles = gameManager.playerArray[teamsWithConnectedTiles[j]].cellSelectorArray[2].currentNumOfSavedTiles;
									//enemy3Turns = gameManager.playerArray[teamsWithConnectedTiles[j]].cellSelectorArray[2].getNumNeeded();
									enemy3Turns = gameManager.playerArray[teamsWithConnectedTiles[j]].cellSelectorArray[2].getNumNeededForNextTile();
									
									//trace("enemy3Tiles = " + enemy3Tiles + " enemy3Turns = " + enemy3Turns);
									
									if (enemy3Tiles > 0 || enemy3Turns <= 1)
									{
										enemyCanUpgrade = true;
										break;
									}
								}
							}
							//trace("touch");
							
							//trace("enemyCanUpgrade = " + enemyCanUpgrade);
							
							if (enemyCanUpgrade)
							{
								// if enemy can upgrade, remove the move from the moves list
								level3BestMoves.splice(i, 1);
								removed = true;
								//trace("touch");
							}
							else
							{
								// otherwise, process normally
								//trace("1 level3move " + level3BestMoves[i].location);
								calcVirtualScore();
								level3BestMoves[i].differential = calcScoreDifferential();
							}
						}
						else 
						{
							//trace("no touch");
							//trace("2 level3move " + level3BestMoves[i].location);
							calcVirtualScore();
							level3BestMoves[i].differential = calcScoreDifferential();
							// trace("level 3 - differential for location " + level3BestMoves[i].location + " = " + level3BestMoves[i].differential);
						}
					}
					else
					{
						//trace("3 level3move " + level3BestMoves[i].location);
						calcVirtualScore();
						level3BestMoves[i].differential = calcScoreDifferential();
						// trace("level 3 - differential for location " + level3BestMoves[i].location + " = " + level3BestMoves[i].differential);
					}
					
					//if (level3BestMoves[i].location == 100)
					//trace("differential for space 100 = " + level3BestMoves[i].differential);
					
					// if point differential above the level 3 theshold, increase chance of playing a level 3 tile
					if (!removed)
					{
						if (!addedLevel3Theshold && level3BestMoves[i].differential > LEVEL_3_POINT_THESHOLD)
						{
							addedLevel3Theshold = true;
							level3Percent += LEVEL_3_THRESHOLD_PERCENT;
						}
					}
					
					resetVirtualBoard();
				}
			}
			
			// if the player has a parasite
			if (parasite)
			{
				// set projection state to parasite
				selectedTileLevel = 3;
				
				// project the parasite
				for (i = 0; i < parasiteBestMoves.length; i++)
				{
					virtualBoard[parasiteBestMoves[i].location].changeState();
					calcVirtualScore();
					parasiteBestMoves[i].differential = calcScoreDifferential();
					
					//trace("parasite - differential for location " + parasiteBestMoves[i].location + " = " + parasiteBestMoves[i].differential);
					
					resetVirtualBoard();
				}
			}
			
			/*
			var output:String = "";
			for (var i:int = 0; i < level1BestMoves.length; i++)
			{
			if (i > 0)
			output += ", ";
			output += "loc[" + level1BestMoves[i].location + "] diff = " + level1BestMoves[i].differential;
			}
			trace(output);
			*/
			
			if (verbose)
			{
				trace ("PRE SCORE SELECTION");
				var output:String = "";
				
				for (i = 0; i < level1BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level1BestMoves[i].location + "] = " + level1BestMoves[i].differential;
				}
				trace("level 1 moves:" + output);
				
				output = "";
				
				for (i = 0; i < level2BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level2BestMoves[i].location + "] = " + level2BestMoves[i].differential;
				}
				trace("level 2 moves:" + output);
				
				output = "";
				
				for (i = 0; i < level3BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level3BestMoves[i].location + "] = " + level3BestMoves[i].differential;
				}
				trace("level 3 moves:" + output);
				
				output = "";
				
				for (i = 0; i < parasiteBestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + parasiteBestMoves[i].location + "] = " + parasiteBestMoves[i].differential;
				}
				trace("parasite moves:" + output);
			}
		}
		
		public function calcScoreDifferential():int
		{
			var original:int = 0;
			var virtual:int = 0;
			
			for (var i:int = 0; i < numPlayers; i++)
				//for (var i:int = 0; i < virtualBoard.length; i++)
			{
				/*
				if (originalBoard[i].team == currentPlayer.playerTeam)
				{
				original += originalScores[i];
				}
				else
				{
				original -= originalScores[i];
				}
				
				if (virtualBoard[i].team == currentPlayer.playerTeam)
				{
				virtual += virtualScores[i];
				}
				else
				{
				virtual -= virtualScores[i];
				}
				*/
				
				
				if (gameManager.playerArray[i].playerTeam == currentPlayer.playerTeam)
				{
					//if (debugCalc && verbose)
					//trace("loc " + i +
					
					original += originalScores[i];
					virtual += virtualScores[i];
				}
				else
				{
					original -= originalScores[i];
					virtual -= virtualScores[i];
				}
				
			}
			
			//trace("virtual = " + virtual + " original = " + original);
			
			return (virtual - original);
		}
		
		public function sortBestMovesByDifferential():void
		{
			// get the move percent, based on difficulty level
			var movePercent:int = 0;
			var i:int;
			
			if (difficultyLevel == 1)
				movePercent = EASY_TOP_DIFF_PERCENT;
			else if (difficultyLevel == 2)
				movePercent = MEDIUM_TOP_DIFF_PERCENT;
			else
				movePercent = HARD_TOP_WEIGHT_PERCENT;
			
			var numberOfMoves:int = 0;
			
			if (difficultyLevel == 3)
			{
				var allOnes:Boolean = false;
				for (i = 0; i < level1BestMoves.length; i++)
				{
					if (level1BestMoves[i].differential > 1)
						break;
					if (i == (level1BestMoves.length - 1))
						allOnes = true;
				}
				//trace("all ones = " + allOnes);
				
				if (!allOnes)
				{
					// sort level 1 best moves by score differential
					level1BestMoves.sort(compareBestMovesByDifferential);
				}
				
				// trim level 1 best moves
				if (level1BestMoves.length > 1)
				{
					// trim best moves by the diff percentage
					numberOfMoves = 0;
					for (i = (level1BestMoves.length - 1); i >= 0; i--)
					{
						if (i > numberOfMoves)
							level1BestMoves.pop();
					}
				}
			}
			else
			{
				// sort level 1 best moves by score differential
				level1BestMoves.sort(compareBestMovesByDifferential);
				
				// trim level 1 best moves
				if (level1BestMoves.length > 1)
				{
					// trim best moves by the diff percentage
					if (difficultyLevel != 3)
					{
						numberOfMoves = (movePercent / 100) * level1BestMoves.length;
						if (numberOfMoves < 0)
							numberOfMoves = 1;
					}
					else
						numberOfMoves = 1;
					numberOfMoves--;
					for (i = (level1BestMoves.length - 1); i >= 0; i--)
					{
						if (i > numberOfMoves)
							level1BestMoves.pop();
					}
				}
			}
			
			// sort level 2 best moves by score differential
			level2BestMoves.sort(compareBestMovesByDifferential);
			
			// trim level 2 best moves
			if (level2BestMoves.length > 1)
			{
				// trim best moves by the diff percentage
				if (difficultyLevel != 3)
				{
					numberOfMoves = (movePercent / 100) * level2BestMoves.length;
					if (numberOfMoves < 0)
						numberOfMoves = 1;
				}
				else
					numberOfMoves = 1;
				numberOfMoves--;
				for (i = (level2BestMoves.length - 1); i >= 0; i--)
				{
					if (i > numberOfMoves)
						level2BestMoves.pop();
				}
			}
			
			// sort level 3 best moves by score differential
			level3BestMoves.sort(compareBestMovesByDifferential);
			
			// trim level 3 best moves
			if (level3BestMoves.length > 1)
			{
				// trim best moves by the diff percentage
				if (difficultyLevel != 3)
				{
					numberOfMoves = (movePercent / 100) * level3BestMoves.length;
					if (numberOfMoves < 0)
						numberOfMoves = 1;
				}
				else
					numberOfMoves = 1;	
				numberOfMoves--;
				for (i = (level3BestMoves.length - 1); i >= 0; i--)
				{
					if (i > numberOfMoves)
						level3BestMoves.pop();
				}
			}
			
			// sort parasite best moves by score differential
			parasiteBestMoves.sort(compareBestMovesByDifferential);
			
			// trim parasite best moves
			if (parasiteBestMoves.length > 1)
			{
				// trim best moves by the diff percentage
				if (difficultyLevel != 3)
				{
					numberOfMoves = (movePercent / 100) * parasiteBestMoves.length;
					if (numberOfMoves < 0)
						numberOfMoves = 1;
				}
				else
					numberOfMoves = 1;
				numberOfMoves--;
				for (i = (parasiteBestMoves.length - 1); i >= 0; i--)
				{
					if (i > numberOfMoves)
						parasiteBestMoves.pop();
				}
			}
			
			if (verbose)
			{
				trace ("POST SCORE SELECTION");
				var output:String = "";
				
				for (i = 0; i < level1BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level1BestMoves[i].location + "] = " + level1BestMoves[i].differential;
				}
				trace("level 1 moves:" + output);
				
				output = "";
				
				for (i = 0; i < level2BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level2BestMoves[i].location + "] = " + level2BestMoves[i].differential;
				}
				trace("level 2 moves:" + output);
				
				output = "";
				
				for (i = 0; i < level3BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level3BestMoves[i].location + "] = " + level3BestMoves[i].differential;
				}
				trace("level 3 moves:" + output);
				
				output = "";
				
				for (i = 0; i < parasiteBestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + parasiteBestMoves[i].location + "] = " + parasiteBestMoves[i].differential;
				}
				trace("parasite moves:" + output);
			}
		}
		
		public function triggerAIMove():void
		{
			//trace("trigger AI Move");
			
			Starling.juggler.delayCall(playMove, MOVE_DELAY_TIME);
		}
		
		public function playMove():void
		{
			if (verbose)
				trace("playing a move");
			
			var choice:int;
			var index:int = -1;
			var choiceMade:Boolean = false;
			var randomFactor:int;
			var randomSign:int;
			var calcPercent:int = 0;
			var parasiteIndex:int = -1;
			var level3Index:int = -1;
			var level2Index:int = -1;
			var parasiteChoice:int;
			var level3Choice:int;
			var level2Choice:int;
			var i:int;
			var output:String;
			
			if (verbose)
			{
				// output all best moves in strings
				output = "";
				
				for (i = 0; i < parasiteBestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + parasiteBestMoves[i].location + "] diff = " + parasiteBestMoves[i].differential;
				}
				trace("ParasiteMoves --- " + parasiteBestMoves.length + " moves --- " + output);
				
				output = "";
				for (i = 0; i < level3BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level3BestMoves[i].location + "] diff = " + level3BestMoves[i].differential;
				}
				trace("Level3Moves --- " + level3BestMoves.length + " moves --- " + output);
				
				output = "";
				for (i = 0; i < level2BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level2BestMoves[i].location + "] diff = " + level2BestMoves[i].differential;
				}
				trace("Level2Moves --- " + level2BestMoves.length + " moves --- " + output);
				
				output = "";
				for (i = 0; i < level1BestMoves.length; i++)
				{
					if (i > 0)
						output += ", ";
					output += "loc[" + level1BestMoves[i].location + "] diff = " + level1BestMoves[i].differential;
				}
				trace("Level1Moves --- " + level1BestMoves.length + " moves --- " + output);
			}
			
			// select the best parasite move
			if (parasite)
			{
				if (verbose)
					trace("selecting parasite");
				
				// add or subtract random factor to parasite percentage
				randomFactor = Math.random() * 10;
				randomSign = Math.random() * 100;
				
				// if easy or medium
				if (difficultyLevel != 3)
				{
					// add or subtract random factor
					if (randomSign < 50)
						calcPercent = parasitePercent - randomFactor;
					else
						calcPercent = parasitePercent + randomFactor;
				}
					// if hard, always add random factor
				else
					calcPercent = parasitePercent + randomFactor;
				
				if (verbose)
					trace("parasite: base = " + parasitePercent + " random = " + randomFactor + " calc = " + calcPercent);
				
				// check to play a parasite
				if (calcPercent >= PLAY_PARASITE_PERCENT)
				{	
					if (verbose)
						trace("there are " + parasiteBestMoves.length + " best parasite moves");
					
					if (parasiteBestMoves.length > 0)
					{
						if (parasiteBestMoves.length > 1)
						{
							// check if the top move is a huge difference in points
							if (parasiteBestMoves[0].differential >= (parasiteBestMoves[1].differential * HIGHEST_DIFFERENTIAL_FACTOR))
							{
								if (verbose)
									trace("parasite found best move with much higher differential");
								parasiteIndex = parasiteBestMoves[0].location;
							}
							else
							{
								parasiteChoice = Math.round(Math.random() * (parasiteBestMoves.length - 1));
								parasiteIndex = parasiteBestMoves[parasiteChoice].location;
							}
						}
						else
						{
							parasiteIndex = parasiteBestMoves[0].location;
						}
					}
				}
			}
			
			// select the best level 3 move
			if (level3Tiles > 0)
			{
				if (verbose)	
					trace("selecting level 3");
				
				// add or subtract random factor to level 3 percentage
				randomFactor = Math.random() * 10;
				randomSign = Math.random() * 100;
				
				// if easy or medium
				if (difficultyLevel != 3)
				{
					// add or subtract random factor
					if (randomSign < 50)
						calcPercent = level3Percent - randomFactor;
					else
						calcPercent = level3Percent + randomFactor;
				}
					// if hard, always add random factor
				else
					calcPercent = level3Percent + randomFactor;
				
				if (verbose)
					trace("level 3: base = " + level3Percent + " random = " + randomFactor + " calc = " + calcPercent);
				
				// check to play a level 3
				if (calcPercent >= PLAY_LEVEL_3_PERCENT)
				{
					if (level3BestMoves.length > 0)
					{
						if (level3BestMoves.length > 1)
						{
							// check if the top move is a huge difference in points
							if (level3BestMoves[0].differential >= (level3BestMoves[1].differential * HIGHEST_DIFFERENTIAL_FACTOR))
							{
								if (verbose)
									trace("level 3 found best move with much higher differential");
								level3Index = level3BestMoves[0].location;
							}
							else
							{
								level3Choice = Math.round(Math.random() * (level3BestMoves.length - 1));
								level3Index = level3BestMoves[level3Choice].location;
							}
						}
						else
						{
							level3Index = level3BestMoves[0].location;
						}
					}
				}
			}
			
			// select the best level 2 move
			if (level2Tiles > 0)
			{
				//trace("player " + gameManager.getCurrentPlayerNum() + " has " + level2Tiles + " level2Tiles");
				
				if (verbose)
					trace("selecting level 2");
				
				// add or subtract random factor to level 2 percentage
				randomFactor = Math.random() * 10;
				randomSign = Math.random() * 100;
				
				// if easy or medium
				if (difficultyLevel != 3)
				{
					// add or subtract random factor
					if (randomSign < 50)
						calcPercent = level2Percent - randomFactor;
					else
						calcPercent = level2Percent + randomFactor;
				}
					// if hard, always add random factor
				else
					calcPercent = level2Percent + randomFactor;
				
				if (verbose)
					trace("level 2: base = " + level2Percent + " random = " + randomFactor + " calc = " + calcPercent);	
				
				// check to play a level 2
				if (calcPercent >= PLAY_LEVEL_2_PERCENT)
				{
					if (verbose)
						trace("there are " + level2BestMoves.length + " best level 2 moves");
					
					if (level2BestMoves.length > 0)
					{
						if (level2BestMoves.length > 1)
						{
							// check if the top move is a huge difference in points
							if (level2BestMoves[0].differential >= (level2BestMoves[1].differential * HIGHEST_DIFFERENTIAL_FACTOR))
							{
								if (verbose)
									trace("level 2 found best move with much higher differential");
								level2Index = level2BestMoves[0].location;
							}
							else
							{
								level2Choice = Math.round(Math.random() * (level2BestMoves.length - 1));
								level2Index = level2BestMoves[level2Choice].location;
							}
						}
						else
						{
							level2Index = level2BestMoves[0].location;
						}
					}
				}
			}
			
			var debugStr:String = "";
			if (parasiteIndex != -1)
				debugStr += "best parasite = " + parasiteBestMoves[parasiteChoice].differential + " ";
			if (level3Index != -1)
				debugStr += "best level 3 = " + level3BestMoves[level3Choice].differential + " ";
			if (level2Index != -1)
				debugStr += "best level 2 = " + level2BestMoves[level2Choice].differential;
			//trace(debugStr);
			
			// pick the best move from parasite, level 3 and level 2
			var bestScoreDiff:int = 0;
			var foundBetterWithLower:Boolean;
			if (parasiteIndex != -1)
			{
				bestScoreDiff = parasiteBestMoves[parasiteChoice].differential;
				choice = parasiteChoice;
				index = parasiteIndex;
				gameManager.setCurrentCellTypeSelected(currentPlayer.cellSelectorArray[3]);
			}
			if (level3Index != -1)
			{
				// check to see if a level 2 move has the same or better score differential
				foundBetterWithLower = false;
				for (i=0; i<level2BestMoves.length; i++)
				{
					if (level2Tiles > 0)
					{
						if (level2BestMoves[i].differential >= level3BestMoves[level3Choice].differential)
						{
							foundBetterWithLower = true;
							bestScoreDiff = level2BestMoves[i].differential;
							choice = i;
							index = level2BestMoves[choice].location;
							gameManager.setCurrentCellTypeSelected(currentPlayer.cellSelectorArray[1]);
							break;
						}
					}
				}
				
				// if didn't find a level 2 move that is better, use the level 3 move
				if (!foundBetterWithLower)
				{
					if (level3BestMoves[level3Choice].differential > bestScoreDiff)
					{
						bestScoreDiff = level3BestMoves[level3Choice].differential;
						choice = level3Choice;
						index = level3Index;
						gameManager.setCurrentCellTypeSelected(currentPlayer.cellSelectorArray[2]);
					}
				}
			}
			if (level2Index != -1)
			{
				// check to see if a level 1 move has the same or better score differential
				foundBetterWithLower = false;
				for (i=0; i<level1BestMoves.length; i++)
				{
					if (level1BestMoves[i].differential >= level2BestMoves[level2Choice].differential)
					{
						foundBetterWithLower = true;
						bestScoreDiff = level1BestMoves[i].differential;
						choice = i;
						index = level1BestMoves[choice].location;
						gameManager.setCurrentCellTypeSelected(currentPlayer.cellSelectorArray[1]);
						break;
					}
				}
				
				// if didn't find a level 1 move that is better, use the level 2 move
				if (!foundBetterWithLower)
				{
					if (level2BestMoves[level2Choice].differential > bestScoreDiff)
					{
						bestScoreDiff = level2BestMoves[level2Choice].differential;
						choice = level2Choice;
						index = level2Index;
						gameManager.setCurrentCellTypeSelected(currentPlayer.cellSelectorArray[1]);
					}
				}
			}
			
			if(verbose)
			{
				// trace out choice
				trace("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				if (gameManager.getCurrentCellTypeSelected() == currentPlayer.cellSelectorArray[3])
				trace("playing parasite at loc " + index);
				else if (gameManager.getCurrentCellTypeSelected() == currentPlayer.cellSelectorArray[2])
				trace("playing level 3 at loc " + index);
				else if (gameManager.getCurrentCellTypeSelected() == currentPlayer.cellSelectorArray[1])
				trace("playing level 2 at loc " + index);
			}
			
			/*
			if (gameManager.getCurrentCellTypeSelected() == currentPlayer.cellSelectorArray[3])
				trace("playing parasite at loc " + index);
			else if (gameManager.getCurrentCellTypeSelected() == currentPlayer.cellSelectorArray[2])
				trace("playing level 3 at loc " + index);
			else if (gameManager.getCurrentCellTypeSelected() == currentPlayer.cellSelectorArray[1])
				trace("playing level 2 at loc " + index);
			trace("player " + gameManager.getCurrentPlayerNum() + " ------------------------");
			*/
			
			// if a move hasn't been chosen yet, play a level 1
			if (index == -1)
			{
				if (verbose)
					trace("selecting level 1");
				
				// add or subtract random factor to level 1 percentage
				randomFactor = Math.random() * 10;
				randomSign = Math.random() * 100;
				var randomPercent:int = Math.random() * 100;
				if (randomSign < 50)
					calcPercent = randomPercent - randomFactor;
				else
					calcPercent = randomPercent + randomFactor;
				
				// get threshold for picking a level 1 or blank, depending on difficulty level
				var comparePercent:int = 0;
				if (difficultyLevel == 1)
					comparePercent = EASY_LEVEL_1_NOT_RANDOM_BLANK_PERCENT;
				else if (difficultyLevel == 2)
					comparePercent = MEDIUM_LEVEL_1_NOT_RANDOM_BLANK_PERCENT;
				else
					comparePercent = HARD_LEVEL_1_NOT_RANDOM_BLANK_PERCENT;
				
				// play best move
				if (calcPercent < comparePercent && level1BestMoves.length > 0)
				{
					if (verbose)
						trace("playing best move");
					
					if (level1BestMoves.length > 1)
					{
						// check if the top move is a huge difference in points
						if (level1BestMoves[0].differential >= (level1BestMoves[1].differential * HIGHEST_DIFFERENTIAL_FACTOR))
						{
							index = level1BestMoves[0].location;
						}
						else
						{
							choice = Math.round(Math.random() * (level1BestMoves.length - 1));
							index = level1BestMoves[choice].location;
						}
					}
					else
					{
						index = level1BestMoves[0].location;
					}
					if (verbose)
						trace("playing level 1 best move at loc " + index);
				}
					// play on blank
				else
				{
					if (verbose)
						trace("playing blank");
					
					var bestBlanks:Array = [];
					// get the blanks with a good play weight
					for (i = 0; i < blankTiles.length; i++)
					{
						if (originalBoard[blankTiles[i]].weight[0] >= GOOD_PLACE)
						{
							bestBlanks.push(blankTiles[i]);
						}
					}
					// if none, get blanks with positive weight
					if (bestBlanks.length < 1)
					{
						for (i = 0; i < blankTiles.length; i++)
						{
							if (originalBoard[blankTiles[i]].weight[0] > 0)
							{
								bestBlanks.push(blankTiles[i]);
							}
						}
					}
					// if none, get rest of blanks
					if (bestBlanks.length < 1)
					{
						for (i = 0; i < blankTiles.length; i++)
						{
							bestBlanks.push(blankTiles[i]);
						}
					}
					
					if (verbose)
					{
						trace("after choosing, there are " + bestBlanks.length + " blank tiles in the bestBlanks array");
						output = "";
						
						for (i = 0; i < originalBoard.length; i++)
						{
							if (i > 0)
								output += ", ";
							output += "[" + i + "] = " + bestBlanks[i];
						}
						trace("Best blanks: " + output);
					}
					
					// then play a blank move
					if (bestBlanks.length > 0)
					{
						//trace("best blanks length = " + bestBlanks.length);
						choice = Math.round(Math.random() * (bestBlanks.length - 1));
						index = bestBlanks[choice];
					}
					else
					{
						if (level1BestMoves.length > 1)
						{
							//trace("level 1 best moves = " + level1BestMoves.length);
							
							choice = Math.round(Math.random() * (level1BestMoves.length - 1));
							index = level1BestMoves[choice].location;
						}
						else
						{
							if (level1BestMoves.length > 0)
								index = level1BestMoves[0].location;
						}
					}
					if (verbose)
						trace("playing level 1 blank at loc " + index + " blankTiles.length = " + blankTiles.length + " bestBlanks.length = " + bestBlanks.length);
					
					bestBlanks = [];
					
					/*
					var output:String = "";
					
					for (var i:int = 0; i < originalBoard.length; i++)
					{
					if (i > 0)
					output += ", ";
					output += "[" + i + "] = " + originalBoard[i].weight[0]
					}
					trace("All weights: " + output);
					
					
					var output:String = "";
					
					for (var i:int = 0; i < bestBlanks.length; i++)
					{
					if (i > 0)
					output += ", ";
					output += bestBlanks[i]
					}
					trace("Best Blanks: " + output);
					*/
					
					/*
					var choiceMade:Boolean = false;
					while (!choiceMade)
					{
					choice = Math.floor(Math.random() * blankTiles.length);
					index = blankTiles[choice];
					blankTiles.splice(choice, 1);
					if (originalBoard[index].weight > 0 || blankTiles.length <= 0)
					choiceMade = true;
					}
					if (index == -1)
					{
					choice = Math.floor(Math.random() * bestMoves[0].length);
					index = bestMoves[0][choice];
					}
					*/
				}
				gameManager.setCurrentCellTypeSelected(currentPlayer.cellSelectorArray[0]);
			}
			// found a valid move. make the move
			if (index != -1)
			{
				gameManager.setPlayerAndSelectedTile(gameBoard[index]);
				gameBoard[index].changeState();
				gameManager.makeAIMove(index);
			}
			// didn't find a valid move. retry AI algorithm
			else
			{
				gameManager.retryAIMove();
			}
		}
		
		public function cleanUp():void
		{
			originalBoard = [];
			virtualBoard = [];
			originalScores = [];
			virtualScores = [];
			tileScores = [];
			level1BestMoves = [];
			level2BestMoves = [];
			level3BestMoves = [];
			parasiteBestMoves = []; 
			blankTiles = [];
			
			currentPlayer = null;
			gameManager = null;
			//myStage.removeEventListener(KeyboardEvent.KEY_UP, keyUp);
		}
		
		public function resetTileTraces():void
		{
			for (var i:int = 0; i < originalBoard.length; i++)
			{
				originalBoard[i].hasTraced = false;
				virtualBoard[i].hasTraced = false;
			}
		}
		
		public function resetTileCounts():void
		{
			for (var i:int = 0; i < originalBoard.length; i++)
			{
				originalBoard[i].hasCounted = false;
			}
		}
		
		public function tracePaths():void
		{
			tileScanState = 2;
			
			for (var i:int = 0; i < originalBoard.length; i++)
			{
				resetTileTraces();
				
				/*
				if (originalBoard[i].team == currentPlayer.playerTeam && originalBoard[i].state == tileScanState)
				{
					trace("tracePaths checking tile " + originalBoard[i].location);
					if (originalBoard[i].tileTouchesSameLevelDifferentPlayer())
						trace("trace yes at " + i);
				}
				*/
			}
		}
		
		public function countThrees():void
		{
			var connectedThrees:int = 0;
			var maxConnectedThrees:int = 0;
			
			resetTileCounts();
			
			for (var i:int = 0; i < originalBoard.length; i++)
			{
				if (!originalBoard[i].hasCounted)
					connectedThrees = originalBoard[i].numberOfAdjacentSameTiles();
				
				if (connectedThrees > maxConnectedThrees)
				{
					maxConnectedThrees = connectedThrees;
					// trace("found " + maxConnectedThrees + " threes from loc " + originalBoard[i].location);
				}
			}
		}
		
		public function compareBestMovesByWeight(first:AIBestMove, second:AIBestMove):int
		{
			if (first.weight < second.weight)
				return 1;
			else if (first.weight > second.weight)
				return -1;
			else
				return 0;
		}
		
		public function compareBestMovesByDifferential(first:AIBestMove, second:AIBestMove):int
		{
			if (first.differential < second.differential)
				return 1;
			else if (first.differential > second.differential)
				return -1;
			else
				return 0;
		}
	}
}