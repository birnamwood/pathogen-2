package
{
	import Sound.SoundController;
	
	import UI.Main;
	
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageAspectRatio;
	import flash.display.StageDisplayState;
	import flash.display.StageOrientation;
	import flash.display.StageQuality;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.StageOrientationEvent;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.AudioPlaybackMode;
	import flash.media.SoundMixer;
	import flash.system.Capabilities;
	
	import starling.core.Starling;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;
	
	[SWF(width="2048", height="1536", frameRate="30", backgroundColor="#000033")]
	public class PathogenTest extends Sprite
	{
		private var myStarling:Starling;
		private var viewPort:Rectangle;
		private var assets:AssetManager;
		
		public static var stageWidth:int;
		public static var stageHeight:int;
		public static var stageCenter:Point;
		
		public static var wTenPercent:Number;
		public static var hTenPercent:Number;
		
		public static var scaleFactor:Number;
		public static var HD_Multiplyer:int = 1;
		public static var IGF_Build:Boolean = false;
		
		//iOS
		public static const iPAD_2:String = "iPad_2";
		public static const iPAD_HD:String = "iPad_HD";
		public static const iPHONE_4:String = "iPhone_4";
		public static const iPHONE_5:String = "iPhone_5";
		//Kindle
		public static const KINDLE_FIRE:String = "KINDLE_Fire";
		public static const KINDLE_FIRE_HD:String = "KINDLE_Fire_HD";
		public static const KINDLE_FIRE_HDX:String = "KINDLE_Fire_HDX";
		public static const KINDLE_FIRE_HDX_8inch:String = "KINDLE_FIRE_HDX_8inch";
		//Android
		public static const GOOGLE_NEXUS_4:String = "GOOGLE_NEXUS_4";
		public static const LG_OPTIMUS:String = "LG_OPTIMUS";
		public static const MOTOROLA_MOTO_X:String = "MOTOROLA_MOTO_X";
		public static const MOTOROLA_DROID_RAZR:String = "MOTOROLA_DROID_RAZR";
		public static const MOTOROLA_DROID_M:String = "MOTOROLA_DROID_M";
		//Nook
		public static const NOOK_HD:String = "NOOK_HD";
		public static const NOOK_HD_PLUS:String = "NOOK_HD+";
		// generic Droid
		public static const ANDROID:String = "Android";
		
		public static var sPixelsPerInch:int;
		
		public static const PC:String = "PC";
		public static var device:String;
		public static var KINDLE:Boolean;
		public static var KINDLE_TYPE:String;
		
		public function PathogenTest()
		{
			super();
			
			if (stage)
			{
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.align = StageAlign.TOP_LEFT;
				stage.frameRate = 30;
			}
			
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
		}
		private function onAddedToStage( event : Event ) : void
		{
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
			var iOS:Boolean = Capabilities.manufacturer.indexOf("iOS") != -1;
			Starling.multitouchEnabled = true;
			Starling.handleLostContext = !iOS;
			
			viewPort = new Rectangle(0, 0, 2048, 1536);
			
			myStarling = new Starling(Main, stage, viewPort);
			myStarling.antiAliasing = 1;
			
			determineIfStageIsReady();
		}
		
		private function determineDeviceType() : void
		{
			// workaround for testing on desktop until ADL stops offsetting the starling stage in fullScreen mode.
			if (Capabilities.os.indexOf("Windows") >= 0 || Capabilities.os.indexOf("Mac OS") >= 0)
			{
				stage.displayState = StageDisplayState.NORMAL;
				stage.setOrientation(StageOrientation.ROTATED_LEFT);
			}
			else
			{
				if (stage.orientation == StageOrientation.ROTATED_LEFT)
					stage.setOrientation(StageOrientation.ROTATED_LEFT);
				else
					stage.setOrientation(StageOrientation.ROTATED_RIGHT);
			}
		}
		
		private function determineIfStageIsReady() : void
		{
			
			stage.addEventListener(Event.RESIZE, resizeStage);
			
			// this will make sure that Stage3D is ready to rock!
			myStarling.stage3D.addEventListener( Event.CONTEXT3D_CREATE, function(e:Event):void
			{
				stageHeight = Starling.current.viewPort.height;
				stageWidth = Starling.current.viewPort.width;
				
				stageCenter = new Point(stageWidth/2, stageHeight/2);
				
				wTenPercent = stageWidth/10;
				hTenPercent = stageHeight/10;
				
				var appDir:File = File.applicationDirectory;
				assets = new AssetManager(scaleFactor);
				//trace("Scale Factor: " + scaleFactor);
				
				//assets.verbose = Capabilities.isDebugger;
				if(HD_Multiplyer == 2)
				{
					// enqueue HD assets
					assets.enqueue(
						appDir.resolvePath("HD_BlueSprites_1.png"),
						appDir.resolvePath("HD_BlueSprites_1.xml"),
						appDir.resolvePath("HD_BlueSprites_2.png"),
						appDir.resolvePath("HD_BlueSprites_2.xml"),
						appDir.resolvePath("HD_GreenSprites_1.png"),
						appDir.resolvePath("HD_GreenSprites_1.xml"),
						appDir.resolvePath("HD_GreenSprites_2.png"),
						appDir.resolvePath("HD_GreenSprites_2.xml"),
						appDir.resolvePath("HD_RedSprites_1.png"),
						appDir.resolvePath("HD_RedSprites_1.xml"),
						appDir.resolvePath("HD_RedSprites_2.png"),
						appDir.resolvePath("HD_RedSprites_2.xml"),
						appDir.resolvePath("HD_YellowSprites_1.png"),
						appDir.resolvePath("HD_YellowSprites_1.xml"),
						appDir.resolvePath("HD_YellowSprites_2.png"),
						appDir.resolvePath("HD_YellowSprites_2.xml"),
						appDir.resolvePath("HD_GameSprites.png"),
						appDir.resolvePath("HD_GameSprites.xml"),
						appDir.resolvePath("HD_DarkCircle.png"),
						appDir.resolvePath("HD_LightCircle.png"),
						appDir.resolvePath("HD_Pathogen_Bubble.png")
					);
				}
				else
				{
					// enqueue regular assets
					assets.enqueue(
						appDir.resolvePath("BlueSprites.png"),
						appDir.resolvePath("BlueSprites.xml"),
						appDir.resolvePath("GreenSprites.png"),
						appDir.resolvePath("GreenSprites.xml"),
						appDir.resolvePath("RedSprites.png"),
						appDir.resolvePath("RedSprites.xml"),
						appDir.resolvePath("YellowSprites.png"),
						appDir.resolvePath("YellowSprites.xml"),
						appDir.resolvePath("GameSprites.png"),
						appDir.resolvePath("GameSprites.xml"),
						appDir.resolvePath("DarkCircle.png"),
						appDir.resolvePath("LightCircle.png"),
						appDir.resolvePath("Pathogen_Bubble.png")
					);
				}
				
				// enqueue common audio assets
				assets.enqueue(
					appDir.resolvePath("pathogen_in_game_2.mp3"),
					appDir.resolvePath("pathogen_menu.mp3"),
					appDir.resolvePath("C_1.mp3"),
					appDir.resolvePath("C_2.mp3"),
					appDir.resolvePath("C_3.mp3"),
					appDir.resolvePath("C_4.mp3"),
					appDir.resolvePath("C_5.mp3"),
					appDir.resolvePath("C_6.mp3"),
					appDir.resolvePath("C_7.mp3"),
					appDir.resolvePath("C_8.mp3"),
					appDir.resolvePath("D_1.mp3"),
					appDir.resolvePath("D_2.mp3"),
					appDir.resolvePath("D_3.mp3"),
					appDir.resolvePath("D_4.mp3"),
					appDir.resolvePath("D_5.mp3"),
					appDir.resolvePath("D_6.mp3"),
					appDir.resolvePath("D_7.mp3"),
					appDir.resolvePath("D_8.mp3"),
					appDir.resolvePath("E_1.mp3"),
					appDir.resolvePath("E_2.mp3"),
					appDir.resolvePath("E_3.mp3"),
					appDir.resolvePath("E_4.mp3"),
					appDir.resolvePath("E_5.mp3"),
					appDir.resolvePath("E_6.mp3"),
					appDir.resolvePath("E_7.mp3"),
					appDir.resolvePath("E_8.mp3"),
					appDir.resolvePath("Lose.mp3"),
					appDir.resolvePath("Win.mp3")
				);
				
				// enqueue second in-game song, if not android
				if (device == iPAD_2 || device == iPAD_HD || device == iPHONE_4 || device == iPHONE_5 || device == PC)
					assets.enqueue(appDir.resolvePath("pathogen_in_game_1.mp3"));
				
				var main:Main = myStarling.root as Main;
				main.startGame(assets);
				myStarling.start();
				
				determineDeviceType();
				stage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGING, onStage_OrientationChange);
				
				NativeApplication.nativeApplication.addEventListener(
					flash.events.Event.ACTIVATE, function (e:*):void { SoundController.unPauseAudio(); myStarling.start();  });
				// and for use capture phase
				NativeApplication.nativeApplication.addEventListener(
					flash.events.Event.ACTIVATE, function (e:*):void { SoundController.unPauseAudio(); myStarling.start();  }, true, 0, false);
				
				NativeApplication.nativeApplication.addEventListener(
					flash.events.Event.DEACTIVATE, function (e:*):void { SoundController.pauseAudio(); myStarling.stop();  });
				// and for use capture phase
				NativeApplication.nativeApplication.addEventListener(
					flash.events.Event.DEACTIVATE, function (e:*):void { SoundController.pauseAudio(); myStarling.stop();  }, true, 0, false);
			} );
		}
		
		private function resizeStage(event:Event):void
		{
			var resolutionY:int = Capabilities.screenResolutionY;
			var resolutionX:int = Capabilities.screenResolutionX;
			
			// IOS
			/*
			if (resolutionY == 1024 && resolutionX == 768)
			{
				//iPad 2 resolution
				Starling.current.viewPort = new Rectangle(0, 0, 1024, 768);
				myStarling.stage.stageWidth = 1024;
				myStarling.stage.stageHeight = 768;
				scaleFactor = 1.0;
				device = iPAD_2;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 2048 && resolutionX == 1536)
			{
				//HD iPad resolution
				Starling.current.viewPort = new Rectangle(0, 0, 2048, 1536);
				
				myStarling.stage.stageWidth = 2048;
				myStarling.stage.stageHeight = 1536;
				scaleFactor = 1.0;
				device = iPAD_HD;
				HD_Multiplyer = 2;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 960 && resolutionX == 640)
			{
				//iPhone 4 resolution
				Starling.current.viewPort = new Rectangle(0, 0, 960, 640);
				myStarling.stage.stageWidth = 960;
				myStarling.stage.stageHeight = 640;
				scaleFactor = 1.07;
				device = iPHONE_4;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 1136 && resolutionX == 640)
			{
				//iPhone 5 resolution
				Starling.current.viewPort = new Rectangle(0, 0, 1136, 640);
				myStarling.stage.stageWidth = 1136;
				myStarling.stage.stageHeight = 640;
				scaleFactor = 1.07;
				device = iPHONE_5;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 480 && resolutionX == 320)
			{
				//Iphone 3GS resolution
				Starling.current.viewPort = new Rectangle(0, 0, 480, 320);
				myStarling.stage.stageWidth = 480;
				myStarling.stage.stageHeight = 320;
				scaleFactor = 2.13;
				device = iPHONE_4;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			*/
			// KINDLES
			/*
			if (resolutionY == 1024 && resolutionX == 600)
			{
				//Kindle Fire Generation 1 & 2
				Starling.current.viewPort = new Rectangle(0, 0, 1024, 600);
				myStarling.stage.stageWidth = 1024;
				myStarling.stage.stageHeight = 600;
				scaleFactor = 1;
				device = ANDROID;
				KINDLE = true;
				KINDLE_TYPE = KINDLE_FIRE;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 1024 && resolutionY == 600)
			{
				//Kindle Fire Generation 1 & 2
				Starling.current.viewPort = new Rectangle(0, 0, 1024, 600);
				myStarling.stage.stageWidth = 1024;
				myStarling.stage.stageHeight = 600;
				scaleFactor = 1;
				device = ANDROID;
				KINDLE = true;
				KINDLE_TYPE = KINDLE_FIRE;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 1280 && resolutionX == 800)
			{
				//Kindle Fire HD Generation 2 & 3
				Starling.current.viewPort = new Rectangle(0, 0, 1280, 800);
				myStarling.stage.stageWidth = 1280;
				myStarling.stage.stageHeight = 800;
				scaleFactor = 1;
				device = ANDROID;
				KINDLE = true;
				KINDLE_TYPE = KINDLE_FIRE_HD;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 1280 && resolutionY == 800)
			{
				//Kindle Fire HD Generation 2 & 3
				Starling.current.viewPort = new Rectangle(0, 0, 1280, 800);
				myStarling.stage.stageWidth = 1280;
				myStarling.stage.stageHeight = 800;
				scaleFactor = 1;
				device = ANDROID;
				KINDLE = true;
				KINDLE_TYPE = KINDLE_FIRE_HD;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 1920 && resolutionX == 1200)
			{
				//Kindle Fire HDX Generation
				Starling.current.viewPort = new Rectangle(0, 0, 1920, 1200);
				myStarling.stage.stageWidth = 1920;
				myStarling.stage.stageHeight = 1200;
				scaleFactor = 1.06;
				HD_Multiplyer = 2;
				device = ANDROID;
				KINDLE = true;
				KINDLE_TYPE = KINDLE_FIRE_HDX;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 1920 && resolutionY == 1200)
			{
				//Kindle Fire HDX Generation
				Starling.current.viewPort = new Rectangle(0, 0, 1920, 1200);
				myStarling.stage.stageWidth = 1920;
				myStarling.stage.stageHeight = 1200;
				scaleFactor = 1.06;
				HD_Multiplyer = 2;
				device = ANDROID;
				KINDLE = true;
				KINDLE_TYPE = KINDLE_FIRE_HDX;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 2560 && resolutionX == 1600)
			{
				//Kindle Fire HDX Generation 8 Inch
				Starling.current.viewPort = new Rectangle(0, 0, 2560, 1600);
				myStarling.stage.stageWidth = 2560;
				myStarling.stage.stageHeight = 1600;
				scaleFactor = .8;
				HD_Multiplyer = 2;
				device = ANDROID;
				KINDLE = true;
				KINDLE_TYPE = KINDLE_FIRE_HDX_8inch;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 2560 && resolutionY == 1600)
			{
				//Kindle Fire HDX Generation 8 Inch
				Starling.current.viewPort = new Rectangle(0, 0, 2560, 1600);
				myStarling.stage.stageWidth = 2560;
				myStarling.stage.stageHeight = 1600;
				scaleFactor = .8;
				HD_Multiplyer = 2;
				device = ANDROID;
				KINDLE = true;
				KINDLE_TYPE = KINDLE_FIRE_HDX_8inch;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
*/
			
			// ANDROIDS
			if (resolutionY == 1280 && resolutionX == 768)
			{
				//Google Nexus 4
				Starling.current.viewPort = new Rectangle(0, 0, 1280, 768);
				myStarling.stage.stageWidth = 1280;
				myStarling.stage.stageHeight = 768;
				scaleFactor = 1;
				device = GOOGLE_NEXUS_4;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 1280 && resolutionY == 768)
			{
				//Google Nexus 4
				Starling.current.viewPort = new Rectangle(0, 0, 1280, 768);
				myStarling.stage.stageWidth = 1280;
				myStarling.stage.stageHeight = 768;
				scaleFactor = 1;
				device = GOOGLE_NEXUS_4;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 800 && resolutionX == 480)
			{
				//LG Optimus
				Starling.current.viewPort = new Rectangle(0, 0, 800, 480);
				myStarling.stage.stageWidth = 800;
				myStarling.stage.stageHeight = 480;
				//scaleFactor = scaleFactor = 1024 / resolutionY;
				scaleFactor = 1.35;
				device = LG_OPTIMUS;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 800 && resolutionY == 480)
			{
				//LG Optimus
				Starling.current.viewPort = new Rectangle(0, 0, 800, 480);
				myStarling.stage.stageWidth = 800;
				myStarling.stage.stageHeight = 480;
				scaleFactor = scaleFactor = 1024 / resolutionX;
				scaleFactor = 1.35;
				device = LG_OPTIMUS;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 1280 && resolutionX == 720)
			{
				//Motorola Moto X
				Starling.current.viewPort = new Rectangle(0, 0, 1280, 720);
				myStarling.stage.stageWidth = 1280;
				myStarling.stage.stageHeight = 720;
				scaleFactor = 1;
				device = MOTOROLA_MOTO_X;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 1280 && resolutionY == 720)
			{
				//Motorola Moto X
				Starling.current.viewPort = new Rectangle(0, 0, 1280, 720);
				myStarling.stage.stageWidth = 1280;
				myStarling.stage.stageHeight = 720;
				scaleFactor = 1;
				device = MOTOROLA_MOTO_X;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 960 && resolutionX == 540)
			{
				//Motorola Droid Razr
				Starling.current.viewPort = new Rectangle(0, 0, 960, 540);
				myStarling.stage.stageWidth = 960;
				myStarling.stage.stageHeight = 540;
				scaleFactor = 1.2;
				device = MOTOROLA_DROID_RAZR;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 960 && resolutionY == 540)
			{
				//Motorola Droid Razr
				Starling.current.viewPort = new Rectangle(0, 0, 960, 540);
				myStarling.stage.stageWidth = 960;
				myStarling.stage.stageHeight = 540;
				scaleFactor = 1.25;
				device = MOTOROLA_DROID_RAZR;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (((resolutionY >= 890) && (resolutionY <= 900)) && resolutionX == 540)
			{
				// Motorola Droid M
				Starling.current.viewPort = new Rectangle(0, 0, resolutionY, 540);
				myStarling.stage.stageWidth = resolutionY;
				myStarling.stage.stageHeight = 540;
				scaleFactor = 1024 / resolutionX;
				device = MOTOROLA_DROID_M;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (((resolutionX >= 890) && (resolutionX <= 900)) && resolutionY == 540)
			{
				// Motorola Droid M
				Starling.current.viewPort = new Rectangle(0, 0, resolutionX, 540);
				myStarling.stage.stageWidth = resolutionX;
				myStarling.stage.stageHeight = 540;
				scaleFactor = 1024 / resolutionX;
				device = MOTOROLA_DROID_M;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 1242 && resolutionY == 682)
			{
			//trace("NEXUS3");
			
				Starling.current.viewPort = new Rectangle(0, 0, 1242, 682);
				myStarling.stage.stageWidth = 1242;
				myStarling.stage.stageHeight = 682;
				scaleFactor = 1;
				HD_Multiplyer = 1;
				device = ANDROID;
			}
			else if (resolutionX == 682 && resolutionY == 1242)
			{
			//trace("NEXUS3");
			
				Starling.current.viewPort = new Rectangle(0, 0, 1242, 682);
				myStarling.stage.stageWidth = 1242;
				myStarling.stage.stageHeight = 682;
				scaleFactor = 1;
				HD_Multiplyer = 1;
				device = ANDROID;
			}
			else if ((resolutionX >= 1840 && resolutionX <= 1920) && (resolutionY >= 1000 && resolutionY <= 1080))
			{
			//trace("Sony Experia");
			
				Starling.current.viewPort = new Rectangle(0, 0, resolutionX, resolutionY);
				myStarling.stage.stageWidth = resolutionX;
				myStarling.stage.stageHeight = resolutionY;
				scaleFactor = 1.45;
				HD_Multiplyer = 2;
				device = ANDROID;
			}
			else if ((resolutionY >= 1840 && resolutionY <= 1920) && (resolutionX >= 1000 && resolutionX <= 1080))
			{
			//trace("Sony Experia");
			
				Starling.current.viewPort = new Rectangle(0, 0, resolutionY, resolutionX);
				myStarling.stage.stageWidth = resolutionY;
				myStarling.stage.stageHeight = resolutionX;
				scaleFactor = 1.45;
				HD_Multiplyer = 2;
				device = ANDROID;
			}
			else if ((resolutionX >= 1840 && resolutionX <= 1920) && (resolutionY >= 1120 && resolutionY <= 1200))
			{
				Starling.current.viewPort = new Rectangle(0, 0, resolutionX, resolutionY);
				myStarling.stage.stageWidth = resolutionX;
				myStarling.stage.stageHeight = resolutionY;
				scaleFactor = 1.3;
				HD_Multiplyer = 2;
				device = ANDROID;
			}
			else if ((resolutionY >= 1840 && resolutionY <= 1920) && (resolutionX >= 1120 && resolutionX <= 1200))
			{
				Starling.current.viewPort = new Rectangle(0, 0, resolutionY, resolutionX);
				myStarling.stage.stageWidth = resolutionY;
				myStarling.stage.stageHeight = resolutionX;
				scaleFactor = 1.3;
				HD_Multiplyer = 2;
				device = ANDROID;
			}
			else if (resolutionX == 1600 && resolutionY == 2560)
			{
				Starling.current.viewPort = new Rectangle(0, 0, 2560, 1600);
				myStarling.stage.stageWidth = 2560;
				myStarling.stage.stageHeight = 1600;
				scaleFactor = 1;
				HD_Multiplyer = 2;
				device = ANDROID;
			}
			else if (resolutionX == 2560 && resolutionY == 1600)
			{
				Starling.current.viewPort = new Rectangle(0, 0, 2560, 1600);
				myStarling.stage.stageWidth = 2560;
				myStarling.stage.stageHeight = 1600;
				scaleFactor = 1;
				HD_Multiplyer = 2;
				device = ANDROID;
			}
			else if (resolutionX == 1920 && (resolutionY >= 1100 && resolutionY <= 1200))
			{
				Starling.current.viewPort = new Rectangle(0, 0, 1920, resolutionY);
				myStarling.stage.stageWidth = 1920;
				myStarling.stage.stageHeight = resolutionY;
				scaleFactor = 1.3;
				HD_Multiplyer = 2;
				device = ANDROID;
			}
			else if (resolutionY == 1920 && (resolutionX >= 1100 && resolutionX <= 1200))
			{
				Starling.current.viewPort = new Rectangle(0, 0, 1920, resolutionX);
				myStarling.stage.stageWidth = 1920;
				myStarling.stage.stageHeight = resolutionX;
				scaleFactor = 1.3;
				HD_Multiplyer = 2;
				device = ANDROID;
			}
			
			// NOOKS
			/*
			if (resolutionY == 1440 && resolutionX == 900)
			{
				//Nook HD
				Starling.current.viewPort = new Rectangle(0, 0, 1440, 900);
				myStarling.stage.stageWidth = 1440;
				myStarling.stage.stageHeight = 900;
				scaleFactor = 1;
				device = NOOK_HD;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 1440 && resolutionY == 900)
			{
				//Nook HD
				Starling.current.viewPort = new Rectangle(0, 0, 1440, 900);
				myStarling.stage.stageWidth = 1440;
				myStarling.stage.stageHeight = 900;
				scaleFactor = 1;
				device = NOOK_HD;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionY == 1920 && resolutionX == 1280)
			{
				//Nook HD +
				Starling.current.viewPort = new Rectangle(0, 0, 1920, 1280);
				myStarling.stage.stageWidth = 1920;
				myStarling.stage.stageHeight = 1280;
				scaleFactor = 1;
				device = NOOK_HD_PLUS;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			else if (resolutionX == 1920 && resolutionY == 1280)
			{
				//Nook HD +
				Starling.current.viewPort = new Rectangle(0, 0, 1920, 1280);
				myStarling.stage.stageWidth = 1920;
				myStarling.stage.stageHeight = 1280;
				scaleFactor = 1;
				device = NOOK_HD_PLUS;
				
				SoundMixer.audioPlaybackMode=AudioPlaybackMode.AMBIENT;
			}
			*/
			else
			{
				//Other
				if (resolutionX > resolutionY)
				{
					Starling.current.viewPort = new Rectangle(0, 0, resolutionX, resolutionY);
					myStarling.stage.stageWidth = resolutionX;
					myStarling.stage.stageHeight = resolutionY;
					
					if(resolutionX <= 1280)//Than use the standard assets
					{
						HD_Multiplyer = 1;
						
						if (resolutionX >= 1024 && resolutionY <= 800)
							scaleFactor = 1;
						else
						{
							scaleFactor = 1024 / resolutionX;
							if (scaleFactor < 1)
								scaleFactor = 1;
						}
					}
					else //Otherwise use the HD assets
					{
						HD_Multiplyer = 2;
						if (resolutionX >= 1800 && resolutionY <= 1600)
							scaleFactor = 1;
						else
						{
							scaleFactor = 2048 / resolutionX;
							if (scaleFactor < 1)
								scaleFactor = 1;
						}
					}
				}
				else
				{
					Starling.current.viewPort = new Rectangle(0, 0, resolutionY, resolutionX);
					myStarling.stage.stageWidth = resolutionY;
					myStarling.stage.stageHeight = resolutionX;
					
					if(resolutionY <= 1280)//Than use the standard assets
					{
						HD_Multiplyer = 1;
						
						if (resolutionY >= 1024 && resolutionX <= 800)
							scaleFactor = 1;
						else
						{
							scaleFactor = 1024 / resolutionY;
							if (scaleFactor < 1)
								scaleFactor = 1;
						}
					}
					else //Otherwise use the HD assets
					{
						HD_Multiplyer = 2;
						if (resolutionY >= 1800 && resolutionX <= 1600)
							scaleFactor = 1;
						else
						{
							scaleFactor = 2048 / resolutionY;
							if (scaleFactor < 1)
								scaleFactor = 1;
						}
					}
				}
				
				device = ANDROID;

				/*
				//Default
				Starling.current.viewPort = new Rectangle(0, 0, 960, 562);
				myStarling.stage.stageWidth = 960;
				myStarling.stage.stageHeight = 562;
				scaleFactor = 1.3;
				HD_Multiplyer = 1;
				device = PC;
				*/
			}
			
			stage.setAspectRatio(StageAspectRatio.LANDSCAPE);
			
			stageWidth = myStarling.stage.stageWidth;
			stageHeight = myStarling.stage.stageHeight;
			
			stageCenter = new Point(stageWidth/2, stageHeight/2);
			
			wTenPercent = stageWidth/10;
			hTenPercent = stageHeight/10;
			
			sPixelsPerInch = flash.system.Capabilities.screenDPI;
		}
		
		private function onStage_OrientationChange(e:StageOrientationEvent ):void
		{
			// this locks the iPad orientation to lansdscapeMode so it won't accidentally shift to portraitMode
			if (e.afterOrientation == "upsideDown" || e.afterOrientation == "default")
			{
				//e.preventDefault();
			}
		}
	}
}