package Core
{
	import OnlinePlay.OnlineGameManager;
	
	import UI.Main;
	
	import Utils.BW_UI;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.animation.DelayedCall;
	import starling.core.Starling;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class TipWindow extends BW_UI
	{
		//Parent Class
		private var _gameManager:GameManager;
		private var _onlineGameManager:OnlineGameManager;
		
		private var _text:TextField;
		private var _delayCall:DelayedCall;
		
		public function TipWindow(gameManager:GameManager, selectorCellType:int, cellState:int, onlineGameManager:OnlineGameManager = null)
		{
			super();
			
			if(gameManager)
				_gameManager = gameManager;
			
			if(onlineGameManager)
				_onlineGameManager = onlineGameManager;
			
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20, 20, 20, 20));
			
			var background:Scale9Image = new Scale9Image(textures);
			background.width = PathogenTest.wTenPercent * 3;
			background.height = PathogenTest.hTenPercent * 2;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.color = 0x00001a;
			addChild(background);
			
			var tipTitle:TextField = new TextField(background.width, PathogenTest.hTenPercent * .5, "Quick Tip", "Dekar",PathogenTest.HD_Multiplyer * 32 / PathogenTest.scaleFactor, Color.WHITE);
			tipTitle.pivotX = tipTitle.width/2;
			addChild(tipTitle);
			tipTitle.y = (-background.height * .45)
			//tipTitle.border = true;
				
			_text = new TextField(background.width * .9, background.height * .8, "", "Dekar",PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
			_text.pivotX = _text.width/2;
			_text.y = tipTitle.y + tipTitle.height/2;
			addChild(_text);
			//_text.border = true;
			
			if(selectorCellType == 0 && cellState == 1 || selectorCellType == 0 && cellState == 5)
				_text.text = "A-Cells may only be played on an empty space or your own-A Cells";
			if(selectorCellType == 0 && cellState == 2 || selectorCellType == 0 && cellState == 6)
				_text.text = "A-Cells may not be played on B-Cells";
			if(selectorCellType == 0 && cellState == 3)
				_text.text = "A-Cells may not be played on C-Cells";
			if(selectorCellType == 0 && cellState == 4 || selectorCellType == 0 && cellState == 9)
				_text.text = "A-Cells may not be played on Wall Cells";
			
			if(selectorCellType == 1 && cellState == 2 || selectorCellType == 1 && cellState == 6)
				_text.text = "B-Cells may not be played on enemy B-Cells";
			if(selectorCellType == 1 && cellState == 3)
				_text.text = "B-Cells may not be played on C-Cells";
			if(selectorCellType == 1 && cellState == 4 || selectorCellType == 1 && cellState == 9)
				_text.text = "B-Cells may not be played on Wall Cells";
			
			if(selectorCellType == 2 && cellState == 3)
				_text.text = "C-Cells may not be played on enemy C-Cells";
			if(selectorCellType == 2 && cellState == 4 || selectorCellType == 2 && cellState == 9)
				_text.text = "C-Cells may not be played on Wall Cells";
			
			if (selectorCellType == 3 && cellState == 0)
				_text.text = "A Virus must be played on a cell";
			else if (selectorCellType == 3 && cellState == 1)
				_text.text = "A Virus may not be played on your own cells";
			else if (selectorCellType == 3 && cellState == 2)
				_text.text = "A Virus may not be played on your own cells";
			else if (selectorCellType == 3 && cellState == 3)
				_text.text = "A Virus may not be played on your own cells";
			else if (selectorCellType == 3 && cellState == 4 || selectorCellType == 3 && cellState == 9)
				_text.text = "A Virus may not be played on Wall Cells";
			else if (selectorCellType == 3)
				_text.text = "A Virus may not be played on neutral cells";
			
			addEventListener(TouchEvent.TOUCH, hideTutorial);
			
			_delayCall = Starling.juggler.delayCall(fadeOUT, 3);
		}
		private function fadeOUT():void
		{
			startFadeOut();
		}
		public function hideTutorial($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if(touch)
			{
				Starling.juggler.remove(_delayCall);
				startFadeOut();
			}
				
		}
		public override function death():void
		{
			if(_gameManager)
				_gameManager.removeTipWindow();
			
			if(_onlineGameManager)
				_onlineGameManager.removeTipWindow();
		}
	}
}