package Core
{
	import UI.Main;
	
	import Utils.BW_UI;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.animation.DelayedCall;
	import starling.core.Starling;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class CaptureZoneScreen extends BW_UI
	{
		//Parent Class
		private var _gameManger:GameManager;
		
		private var _delayCall:DelayedCall;
		
		public function CaptureZoneScreen(gameManager:GameManager, roundsUntilClaim:int)
		{
			super();
			
			_gameManger = gameManager;
			
			const texture:Texture = Main.assets.getTexture("scale9");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20, 20, 20, 20));
			
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 5;
			background.height = PathogenTest.hTenPercent * 2;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			
			var fontSize:int = 75 / PathogenTest.scaleFactor;
			var captureZoneRoundsUntilClaim:TextField = new TextField(PathogenTest.wTenPercent/1.5, background.height, roundsUntilClaim.toString(), "Dekar", fontSize, Color.WHITE);
			captureZoneRoundsUntilClaim.pivotY = captureZoneRoundsUntilClaim.height/2;
			addChild(captureZoneRoundsUntilClaim);
			captureZoneRoundsUntilClaim.x = -background.width/2;
			
			fontSize = 45 / PathogenTest.scaleFactor;
			var textField:TextField = new TextField(PathogenTest.wTenPercent * 3.5, background.height, "Rounds until Control Zones Claimed", "Dekar", fontSize, Color.WHITE);
			textField.pivotY = textField.height/2;
			addChild(textField);
			textField.hAlign = HAlign.LEFT;
			textField.x = captureZoneRoundsUntilClaim.x + captureZoneRoundsUntilClaim.width;
			
			_delayCall = Starling.juggler.delayCall(fadeOut, 2);
			addEventListener(TouchEvent.TOUCH, onTouchRemove);
		}
		private function onTouchRemove($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				Starling.juggler.remove(_delayCall);
				startFadeOut();
			}
		}
		private function fadeOut():void
		{
			startFadeOut();
		}
		public override function death():void
		{
			_gameManger.removeCaptureZoneScreen();
			_gameManger = null;
		}
	}
}