package Core
{
	public class CaptureZone
	{
		private var _zoneArray:Array = [];
		private var _claimed:Boolean;
		private var _identifier:int;
		private var _active:Boolean;
		private var _turnClaimed:int;
		
		public function CaptureZone()
		{
			
		}
		public function promoteAllCells():void
		{
			_claimed = true;
			
			for each (var cell:Cell in _zoneArray)
			{
				cell.promote();
			}
		}
		public function activateAllCells():void
		{
			for each (var cell:Cell in _zoneArray)
			{
				cell.setCapturePoint();
			}
		}
		public function checkAllCells(currentTeam:int):Boolean
		{
			//If all cells are owned by the same player, return TRUE
			var claimed:Boolean = true;
			
			for each (var cell:Cell in _zoneArray)
			{
				if (cell.owner != null)
				{	
					if (cell.owner.playerTeam != currentTeam)
						claimed = false;
				}
				else
					claimed = false;
			}
			
			if(claimed)
				promoteAllCells();
			
			return claimed;
		}
		
		public function get zoneArray():Array {return _zoneArray};
		public function get claimed():Boolean {return _claimed};
		public function get identifier():int {return _identifier};
		public function get active():Boolean {return _active};
		public function get turnClaimed():int {return _turnClaimed};
		
		public function set zoneArray(zoneArray:Array):void {_zoneArray = zoneArray};
		public function set claimed(claimed:Boolean):void {_claimed = claimed};
		public function set identifier(identifier:int):void {_identifier = identifier};
		public function set active(active:Boolean):void {_active = active};
		public function set turnClaimed(turnClaimed:int):void {_turnClaimed = turnClaimed};
	}
}