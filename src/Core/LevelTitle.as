package Core
{
	import Tutorial.TutorialManager;
	
	import UI.Main;
	
	import Utils.BW_UI;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class LevelTitle extends Sprite
	{
		protected static const FADE_IN:int = 0;
		protected static const ALIVE:int = 1;
		protected static const FADE_OUT:int = 2;
		private const MOVE_SPEED:Number = 10;
		
		//Parent Classes
		private var _gameManager:GameManager;
		private var _tutorialManager:TutorialManager;
		
		private var _background:Quad;
		
		protected var state:int;
		private var _mapTitle:String;
		
		
		public function LevelTitle(gameManager:GameManager, mapTitle:String, tutorialManager:TutorialManager = null)
		{
			super();
			
			_mapTitle = mapTitle;
			
			if(gameManager)
				_gameManager = gameManager;
			
			if(tutorialManager)
				_tutorialManager = tutorialManager;
			
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			addChild(transBackground);
			transBackground.alpha = 0;
			
			_background = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * .2, Color.BLACK);
			_background.pivotX = _background.width/2;
			_background.pivotY = _background.height/2;
			addChild(_background);
			_background.y = PathogenTest.stageCenter.y;
			_background.x = PathogenTest.wTenPercent * 2;
			
			state = FADE_IN;
			alpha = 0;
			
			addEventListener(Event.ENTER_FRAME, update);
		}
		private function update($e:Event):void
		{
			if(state == FADE_IN)
			{
				alpha += Main.FADE_SPEED;
				
				if(alpha >= .75)
				{
					state = ALIVE;
				}
			}
			
			if(state == FADE_IN || state == ALIVE)
			{
				if(_background.x < PathogenTest.stageCenter.x)
					_background.x += PathogenTest.HD_Multiplyer * MOVE_SPEED / PathogenTest.scaleFactor;
				else
					_background.x = PathogenTest.stageCenter.x;
				
				if(_background.x == PathogenTest.stageCenter.x)
				{
					if(_background.height < PathogenTest.hTenPercent * 2)
						_background.height += PathogenTest.HD_Multiplyer * MOVE_SPEED / PathogenTest.scaleFactor;
					else
					{
						//Main.updateDebugText("1");
						
						removeEventListener(Event.ENTER_FRAME, update);
						
						//Main.updateDebugText("2");
						
						_background.height = PathogenTest.hTenPercent * 2;
						
						//Main.updateDebugText("2a - " + (PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor));
						
						//var fontSize:int = 80;
						var textX:int = _background.width;
						if (textX > Main.TEXTURE_LIMIT)
							textX = Main.TEXTURE_LIMIT;
						//var mapTitle:TextField = new TextField(_background.width, _background.height, _mapTitle, "Dekar", PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor, Color.WHITE);
						//var mapTitle:TextField = new TextField(_background.width, _background.height, _mapTitle, "Dekar", 50 / PathogenTest.scaleFactor, Color.WHITE);
						//var mapTitle:TextField = new TextField(_background.width * 0.75, _background.height, _mapTitle, "Dekar", PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor, Color.WHITE);
						var mapTitle:TextField = new TextField(textX, _background.height, _mapTitle, "Dekar", PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor, Color.WHITE);
						
						//Main.updateDebugText("2b");
						
						mapTitle.pivotX = mapTitle.width/2;
						mapTitle.pivotY = mapTitle.height/2;
						//mapTitle.pivotX = _background.width / 2;
						//mapTitle.pivotY = _background.height / 2;
						
						//Main.updateDebugText("2c");
						
						addChild(mapTitle);
						
						//Main.updateDebugText("2d");
						
						mapTitle.x = PathogenTest.stageCenter.x;
						
						//Main.updateDebugText("2e");
						
						mapTitle.y = PathogenTest.stageCenter.y;
						
						//Main.updateDebugText("2f");
						
						mapTitle.alpha = 0;
						
						//Main.updateDebugText("3");
						
						var tween:Tween = new Tween(mapTitle, 1);
						tween.fadeTo(1);
						Starling.juggler.add(tween);
						
						//Main.updateDebugText("4");
						
						Starling.juggler.delayCall(fadeOut, 2);
						
						//Main.updateDebugText("5");
					}
				}
			}
			
			if(state == FADE_OUT)
			{
				alpha -= Main.FADE_SPEED;
				
				if(alpha <= 0)
				{
					removeEventListener(Event.ENTER_FRAME, update);
					death();
				}
			}
		}
		private function fadeOut():void
		{
			//Main.updateDebugText("fade out");
			
			state = FADE_OUT;
			addEventListener(Event.ENTER_FRAME, update);
		}
		public function death():void
		{
			if(_gameManager)
				_gameManager.removeTitle();
			
			if(_tutorialManager)
				_tutorialManager.removeTitle();
		}
	}
}