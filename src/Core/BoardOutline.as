package Core
{
	import Utils.BW_UI;
	
	import starling.display.Quad;
	import starling.utils.Color;
	
	public class BoardOutline extends BW_UI
	{
		private var quad_01:Quad;
		private var quad_02:Quad;
		private var quad_03:Quad;
		private var quad_04:Quad;
		
		private var boardWidth:Number = PathogenTest.hTenPercent * .075;
		
		public function BoardOutline()
		{
			quad_01 = new Quad(PathogenTest.stageWidth, boardWidth, Color.WHITE);
			addChild(quad_01);
			quad_01.pivotX = quad_01.width/2;
			quad_01.x = PathogenTest.stageCenter.x;
			
			quad_02 = new Quad(boardWidth, PathogenTest.stageHeight, Color.WHITE);
			addChild(quad_02);
			quad_02.pivotX = quad_02.width/2;
			quad_02.x = PathogenTest.stageWidth - (quad_02.width/2);
			
			quad_03 = new Quad(PathogenTest.stageWidth, boardWidth, Color.WHITE);
			addChild(quad_03);
			quad_03.pivotX = quad_03.width/2;
			quad_03.x = PathogenTest.stageCenter.x;
			quad_03.y = PathogenTest.stageHeight - quad_03.height;
			
			quad_04 = new Quad(boardWidth, PathogenTest.stageHeight, Color.WHITE);
			addChild(quad_04);
		}
		public function changeColors(color:int):void
		{
			quad_01.color = color;
			quad_02.color = color;
			quad_03.color = color;
			quad_04.color = color;
		}
	}
}