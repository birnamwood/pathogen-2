package Core
{
	import UI.Main;
	
	import flash.geom.Point;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	
	public class Player extends Sprite
	{
		//classes
		public var gameManager:GameManager;
		
		public var playerTeam:int;
		
		//Cell Selector Locations
		private var topRightPosition:Point = new Point(PathogenTest.wTenPercent * .8, PathogenTest.hTenPercent * 1.5);
		private var topLeftPostition:Point = new Point(PathogenTest.wTenPercent * 9.2, PathogenTest.hTenPercent * 1.5);
		private var bottomRightPosition:Point = new Point(PathogenTest.wTenPercent * .8, PathogenTest.hTenPercent * 5.5);
		private var bottomLeftPosition:Point = new Point(PathogenTest.wTenPercent * 9.2, PathogenTest.hTenPercent * 5.5);
		
		private var startingPoint:Point
		
		private var numOfSelectorTiles:int = 4;
		
		public var cellSelectorArray:Array = [];
		private var aiControl:Boolean;
		
		// percent bar
		public var percentBar:Quad;
		public var percentBarOfTierThree:Quad;
		private var _percentBarWidth:Number;
		private var _percentBarOfTierThreeWidth:Number;
		
		//Colors
		public var playerHexColor:Number;
		
		private var green:Number = 0x52CAAF;
		private var red:Number = 0xE34081;
		private var blue:Number = 0x5A69CE;
		private var yellow:Number = 0xE0AF38;
		
		//Stats
		public var playerPercent:Number;
		public var numOfTriangleTiles:int;
		public var numOfDiamondTiles:int;
		public var numOfStarTiles:int;
		public var numOfSolidTiles:int;
		public var totalNumOfTiles:int;
		public var arrayOfTileCounts:Array;
		public var previousNumOfTriangleTiles:int;
		public var totalNumOfTriangleTiles:int;
		public var previousNumOfDiamondTiles:int;
		public var totalNumOfDiamondTiles:int;
		public var previousNumOfStarTiles:int;
		public var totalNumOfStarTiles:int;
		public var previousNumOfSolidTiles:int;
		public var totalNumOfSolidTiles:int;
		public var previousTotalNumOfTiles:int;
		public var totalNumOfAllTilesAllGame:int;
		public var playerPercentOfAllTiles:Number;
		public var totalCellsDestroyed:int;
		
		public var arrayofAllTotalTileCounts:Array;
		
		public var teamName:String;
		
		//History
		public var historyArrayOfTiles:Array = [0];
		
		private var playerArrayIndex:int;
		
		public function Player(color:int, location:int, gameManager:GameManager)
		{
			super();
			
			this.gameManager = gameManager;
			playerTeam = color;
			
			switch(color)
			{
				case 0:
					playerHexColor = green;
					teamName = "Green";
					break;
				
				case 1:
					playerHexColor = red;
					teamName = "Red";
					break;
				
				case 2:
					playerHexColor = blue;
					teamName = "Blue";
					break;
				
				case 3:
					playerHexColor = yellow;
					teamName = "Yellow";
					break;
				
				default:
					break;
			}
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				topRightPosition.y = PathogenTest.hTenPercent * 1.35;
				topLeftPostition.y = PathogenTest.hTenPercent * 1.35;
			}
			
			switch(location)
			{
				case 0:
					startingPoint = topRightPosition;
					break;
				case 1:
					startingPoint = topLeftPostition;
					break;
				case 2:
					startingPoint = bottomLeftPosition;
					break;
				case 3:
					startingPoint = bottomRightPosition;
					break;
				default:
					break;
			}
			
			createSelectedTiles()
		}
		public function createSelectedTiles():void
		{
			for (var i:int = 0; i < numOfSelectorTiles; i++)
			{
				var cellSelector:CellSelector = new CellSelector(i, this, gameManager)
				addChild(cellSelector)
				cellSelector.x = startingPoint.x;
				
				var distanceBetweenSelectorTiles:int = PathogenTest.hTenPercent * .9;
				
				cellSelector.y = startingPoint.y + (distanceBetweenSelectorTiles * i);
				
				cellSelector.rememberOriginalSizeAndPlace();
				
				cellSelectorArray.push(cellSelector)
			}
			
			//darkenAllSelectedCells();
		}
		public function showAllSelectedCells():void
		{
			for each (var cellSelector:CellSelector in cellSelectorArray)
			{
				cellSelector.alpha = 1;
			}
		}
		public function darkenAllSelectedCells():void
		{
			for each (var cellSelector:CellSelector in cellSelectorArray)
			{
				cellSelector.alpha = .5;
			}
		}
		
		public function resetEndGameTileCount():void
		{
			numOfTriangleTiles = 0;
			numOfDiamondTiles = 0;
			numOfStarTiles = 0;
			numOfSolidTiles = 0;
			playerPercent = 0;
			totalNumOfTiles = 0;
			arrayOfTileCounts = [];
		}
		
		public function getAiControl():Boolean
		{
			return aiControl;
		}
		
		public function setAiControl(inAiControl:Boolean):void
		{
			aiControl = inAiControl;
		}
		
		public function increaseSelectorSizes():void
		{
			for (var i:int=0; i<cellSelectorArray.length; i++)
			{
				// increase scale
				cellSelectorArray[i].increaseSize();
				
				// modify y coordinate
				var adjustSign:int = 1;
				if (playerArrayIndex <= 1)
				{
					if (i == 1)
						cellSelectorArray[i].adjustYPosition(adjustSign, 10);
					else if (i == 2)
						cellSelectorArray[i].adjustYPosition(adjustSign, 25);
					else if (i == 3)
						cellSelectorArray[i].adjustYPosition(adjustSign, 30);
				}
				else
				{
					adjustSign = -1;
					if (i == 2)
						cellSelectorArray[i].adjustYPosition(adjustSign, 10);
					else if (i == 1)
						cellSelectorArray[i].adjustYPosition(adjustSign, 25);
					else if (i == 0)
						cellSelectorArray[i].adjustYPosition(adjustSign, 30);
				}
				
				// modify x coordinate
				if (playerArrayIndex == 0 || playerArrayIndex == 3)
					adjustSign = 1;
				else
					adjustSign = -1;
				cellSelectorArray[i].adjustXPosition(adjustSign);
			}
		}
		
		public function revertSelectors():void
		{
			for (var i:int=0; i<cellSelectorArray.length; i++)
			{
				cellSelectorArray[i].revertToOriginalSizeAndPlace();
			}
		}
		
		public function garbageCollection():void
		{
			gameManager = null;
			
			for each (var cellSelector:CellSelector in cellSelectorArray)
			{
				cellSelector.GarbageCollection();
				cellSelector.dispose();
				removeChild(cellSelector)
				cellSelector = null;
			}
			
			cellSelectorArray = [];
			arrayofAllTotalTileCounts = [];
			arrayOfTileCounts = [];
			historyArrayOfTiles = [];
		}
		public function getPlayerArrayIndex():int
		{
			return playerArrayIndex;
		}
		public function setPlayerArrayIndex(inPlayerArrayIndex:int):void
		{
			playerArrayIndex = inPlayerArrayIndex;
		}
		
		public function get percentBarWidth():Number { return _percentBarWidth; }
		public function get percentBarOfTierThreeWidth():Number { return _percentBarOfTierThreeWidth; }
		
		public function set percentBarWidth(percentBarWidth:Number):void { _percentBarWidth = percentBarWidth; }
		public function set percentBarOfTierThreeWidth(percentBarOfTierThreeWidth:Number):void { _percentBarOfTierThreeWidth = percentBarOfTierThreeWidth; }
	}
}