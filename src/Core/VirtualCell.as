package Core
{
	import UI.Main;
	
	import com.milkmangames.nativeextensions.GoViral;
	
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class VirtualCell extends Cell
	{
		adjacentTopCell:VirtualCell;
		adjacentBottomCell:VirtualCell;
		adjacentLeftCell:VirtualCell;
		adjacentRightCell:VirtualCell;
		
		public function VirtualCell(gameManager:GameManager, location:int)
		{
			super(location);
			
			this.gameManager = gameManager;
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		private function onTouch($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if (touch && !gameManager.moveUnderway)
			{
				gameManager.convertVirtualToReal(this);
			}
		}
		
		override public function changeState():void
		{
			//The Cells is now changing & mark it as already been changed so that it may not be changed again.
			hasAlreadySpilledOver = true;
			var stateChange:Boolean;
			
			//Depending on what is placed, do the following.
			switch(selectedCellType.type)
			{
				//If the player has selected a level t0 Tile
				case 0:
				{
					//If the player is stacking the same level on itself
					if ((state == 1) && (owner.playerTeam == currentPlayer.playerTeam))
					{
						state = 2;
						stateChange = true;
					}
					//If the Tile has not been Taken
					else if (state == 0)
					{
						state = 1;
						stateChange = true;
					}
					break;
				}
				//If the player has selected a t1 Tile
				case 1:
				{
					//If the player is stacking the same level on itself
					if ((state == 2) && (owner.playerTeam == currentPlayer.playerTeam))
					{
						state = 3;
						stateChange = true;
					}
					//Overwrite a lesser color
					else if (state == 1)
					{
						state = 2;
						stateChange = true;
					}
					//If its an empty space or a grey cell.
					else if (state == 0 || state == 5)
					{
						state = 2;
						stateChange = true;
					}
					break;
				}
				//If the player has selected a t2 cell
				case 2:
				{
					//t2 Grey cell
					if (state == 6)
					{
						state = 3;
						stateChange = true;
					}
						//t1 Grey cell
					else if (state == 5)
					{
						state = 3;
						stateChange = true;
					}
						//stacking a t2 on itself to create a t3.
					else if ((state == 3) && (owner.playerTeam == currentPlayer.playerTeam))
					{
						state = 4;
						stateChange = true;
					}
						//override a lower tier
					else if (state == 2)
					{
						state = 3;
						stateChange = true;
					}
						//override a lower tier
					else if (state == 1)
					{
						state = 3;
						stateChange = true;
					}
						//place one in an empy space.
					else if (state == 0)
					{
						state = 3;
						stateChange = true;
					}
					break;
				}
					//If the player has selected a Virus
				case 3:
				{
					//If they are placing it on an enemy t2 cell
					if ((state == 3) && (owner.playerTeam != currentPlayer.playerTeam))
					{
						state = 0;
						originalTileState = 3;
						stateChange = true;
					}
					//If they are placing it on an enemy t1 cell
					if ((state == 2) && (owner.playerTeam != currentPlayer.playerTeam))
					{
						state = 0;
						originalTileState = 2;
						stateChange = true;
					}
					//If they are placing it on an enemy t0 cell
					if ((state == 1) && (owner.playerTeam != currentPlayer.playerTeam))
					{
						state = 0;
						originalTileState = 1;
						stateChange = true;
					}
					break;
				}
			}
			if (stateChange)
			{
				//Valid Move
				owner = currentPlayer;
				
				//If the placed cell is higher than a t0, begin cascade function.
				if (state != 1)
					spillOver()
				else
					reset();
			}
			else
				notAValidMove = true;
		}
		override public function spillOver():void
		{
			//How many adjacent spaces are empty?
			var numOfViableAdjacentTiles:int = 0;
			rotation = 0;
			
			//Checking for empty spaces
			if (state != 4 && state != 0)
			{
				if (adjacentTopCell.state == 0 && adjacentTopCell.hasAlreadySpilledOver == false)
				{
					numOfViableAdjacentTiles++;
					top = true;
				}
				if (adjacentRightCell.state == 0 && adjacentRightCell.hasAlreadySpilledOver == false)
				{
					numOfViableAdjacentTiles++;
					right = true;
				}
				if (adjacentBottomCell.state == 0 && adjacentBottomCell.hasAlreadySpilledOver == false)
				{
					numOfViableAdjacentTiles++;
					bottom = true;
				}
				if (adjacentLeftCell.state == 0 && adjacentLeftCell.hasAlreadySpilledOver == false)
				{
					numOfViableAdjacentTiles++;
					left = true;
				}
			}
			
			//Checking for Neighboring Cells of the -1 type.
			switch(state)
			{
				case 2:
				{
					//t1 cells can spill over onto neighboring t0,greyt0.
					if ((adjacentTopCell.state == 1) || (adjacentTopCell.state == 5))
						adjacentTopCell.cascade(state, 0, owner, originalTileState,0);
					if ((adjacentBottomCell.state == 1) || (adjacentBottomCell.state == 5))
						adjacentBottomCell.cascade(state, 1, owner, originalTileState, 0);
					if ((adjacentRightCell.state == 1) || (adjacentRightCell.state == 5))
						adjacentRightCell.cascade(state, 2, owner, originalTileState, 0);
					if ((adjacentLeftCell.state == 1) || (adjacentLeftCell.state == 5))
						adjacentLeftCell.cascade(state, 3, owner, originalTileState, 0);
					
					break;
				}
				case 3:
				{
					//t2 cells can spill over onto neighboring t0,t1,greyt0,greyt1.
					if ((adjacentTopCell.state == 1) || (adjacentTopCell.state == 2) || (adjacentTopCell.state == 5) || (adjacentTopCell.state == 6))
						adjacentTopCell.cascade(state, 0, owner, originalTileState, 0);
					if ((adjacentBottomCell.state == 1) || (adjacentBottomCell.state == 2) || (adjacentBottomCell.state == 5) || (adjacentBottomCell.state == 6))
						adjacentBottomCell.cascade(state, 1, owner,  originalTileState, 0);	
					if ((adjacentRightCell.state == 1) || (adjacentRightCell.state == 2) || (adjacentRightCell.state == 5) || (adjacentRightCell.state == 6))
						adjacentRightCell.cascade(state, 2, owner, originalTileState, 0);
					if ((adjacentLeftCell.state == 1) || (adjacentLeftCell.state == 2) || (adjacentLeftCell.state == 5) || (adjacentLeftCell.state == 6))
						adjacentLeftCell.cascade(state, 3, owner, originalTileState, 0);
					
					break;
				}
				case 4:
				{
					//t3 cells can only spill over onto neighboring t3 cells.
					if (adjacentTopCell.state == 3)
						adjacentTopCell.cascade(state, 0, owner, originalTileState, 0);
					if (adjacentBottomCell.state == 3)
						adjacentBottomCell.cascade(state, 1, owner, originalTileState, 0);
					if (adjacentRightCell.state == 3)
						adjacentRightCell.cascade(state, 2, owner, originalTileState, 0);
					if (adjacentLeftCell.state == 3)
						adjacentLeftCell.cascade(state, 3, owner, originalTileState, 0);
					
					break;
				}
				case 0:
				{
					//Spreading the effect of a virus!
					switch(originalTileState)
					{
						case 1:
						{
							//Check all adjacent sides for t0 of any color & grey t0
							if (adjacentTopCell.state == 1 || adjacentTopCell.state == 5)
								adjacentTopCell.cascade(state, 0, owner, originalTileState, 0);
							if (adjacentBottomCell.state == 1 || adjacentBottomCell.state == 5)
								adjacentBottomCell.cascade(state, 1, owner, originalTileState, 0);
							if (adjacentRightCell.state == 1 || adjacentRightCell.state == 5)
								adjacentRightCell.cascade(state, 2, owner, originalTileState, 0);
							if (adjacentLeftCell.state == 1 || adjacentLeftCell.state == 5)
								adjacentLeftCell.cascade(state, 3, owner, originalTileState, 0);
							break;
						}
						case 2:
						{
							//Check all adjacent sides for t1 of any color & grey t1
							if (adjacentTopCell.state == 2 || adjacentTopCell.state == 6)
								adjacentTopCell.cascade(state, 0, owner, originalTileState, 0);
							if (adjacentBottomCell.state == 2 || adjacentBottomCell.state == 6)
								adjacentBottomCell.cascade(state, 1, owner, originalTileState, 0);
							if (adjacentRightCell.state == 2 || adjacentRightCell.state == 6)
								adjacentRightCell.cascade(state, 2, owner, originalTileState, 0);
							if (adjacentLeftCell.state == 2 || adjacentLeftCell.state == 6)
								adjacentLeftCell.cascade(state, 3, owner, originalTileState, 0);
							break;
						}
						case 3:
						{
							//Check all adjacent sides for t2 of any color
							if (adjacentTopCell.state == 3)
								adjacentTopCell.cascade(state, 0, owner, originalTileState, 0);
							if (adjacentBottomCell.state == 3)
								adjacentBottomCell.cascade(state, 1, owner, originalTileState, 0);
							if (adjacentRightCell.state == 3)
								adjacentRightCell.cascade(state, 2, owner, originalTileState, 0);
							if (adjacentLeftCell.state == 3)
								adjacentLeftCell.cascade(state, 3, owner, originalTileState, 0);
							break;
						}
							
						default:
							break;
					}
					break;
				}
					
				default:
					break;
			}
			
			if(numOfViableAdjacentTiles != 0)
			{
				if (top)
					adjacentTopCell.cascade(state, -1, owner, originalTileState, 0);
				if (right)
					adjacentRightCell.cascade(state, -1, owner, originalTileState, 0);
				if (bottom)
					adjacentBottomCell.cascade(state, -1, owner, originalTileState, 0);
				if (left)
					adjacentLeftCell.cascade(state, -1, owner, originalTileState, 0);
			}
			
			reset();
			
		}
		override public function cascade(OriginatorTileState:int, OriginatorTilesRelativePos:int, tileOwner:Player, originatorOriginalTileState:int, reactionNum:int):void
		{
			//What was the orginal cell that was placed by the player?
			originalTileState = originatorOriginalTileState;
			
			//As long as the cell has not already changed this turn, change it.
			if (!hasAlreadySpilledOver)
			{
				hasAlreadySpilledOver = true;
				
				//The original tile was a t1.
				if (OriginatorTileState == 2)
				{
					owner = tileOwner;
					
					//Based on this cells current state, do different actions.
					switch(state)
					{
						case 0:
							state = 1;
							reset();
							break;
						case 1:
							state = 2;
							spillOver();
							break;
						case 5:
							state = 2;
							spillOver();
							break;
						default:
							flipping = false;
							break;
					}
				}
					//The original tile was a t2.
				else if (OriginatorTileState == 3)
				{
					owner = tileOwner;
					
					//Based on this cells current state, do different actions.
					switch(state)
					{
						case 0:
							state = 2;
							reset();
							break;
						case 1:
							state = 2;
							spillOver();
							break;
						case 2:
							state = 3;
							spillOver();
							break;
						case 5:
							state = 2;
							spillOver();
							break;
						case 6:
							state = 3;
							spillOver();
						default:
							flipping = false;
							break;
					}
				}
					//The original tile was a t3
				else if (OriginatorTileState == 4)
				{
					state = 4;
					owner = tileOwner;
					spillOver();
				}
					//A virus was used.
				else if (OriginatorTileState == 0)
				{
					state = 0;
					owner = null;
					spillOver();
				}
			}
			else
			{
				//This Cell has already been changed! But a better move could be "further" down the line so lets check for that.
				//trace("This Cell has already changed! at location: " + location);
				
				//The original tile was a t1.
				if (OriginatorTileState == 2)
				{
					owner = tileOwner;
					
					//Based on this cells current state, do different actions.
					switch(_originalState)
					{
						case 0:
							_potentialState = 1;
							if(_potentialState > state)
							{
								state = _potentialState;
								reset();
							}
							break;
						case 1:
							_potentialState = 2;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
							break;
						case 5:
							_potentialState = 2;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
							break;
						default:
							flipping = false;
							break;
					}
				}
					//The original tile was a t2.
				else if (OriginatorTileState == 3)
				{
					//trace("Originator Tile was a T2");
					
					owner = tileOwner;
					
					//Based on this cells current state, do different actions.
					switch(_originalState)
					{
						case 0:
							//trace("Location " + location + " was originally an empty space, that should evolve into a T1");
							//trace("Location " + location + " current state is: " + state);
							_potentialState = 2;
							if(_potentialState > state)
							{
								state = _potentialState;
								reset();
							}
								
							break;
						case 1:
							_potentialState = 2;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
							break;
						case 2:
							_potentialState = 3;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
							break;
						case 5:
							_potentialState = 2;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
							break;
						case 6:
							_potentialState = 3;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
						default:
							flipping = false;
							break;
					}
				}
					//The original tile was a t3
				else if (OriginatorTileState == 4)
				{
					_potentialState = 4;
					
					if(_potentialState > state)
					{
						owner = tileOwner;
						spillOver();
					}
				}
					//A virus was used.
				else if (OriginatorTileState == 0)
				{
					_potentialState = 0;
					
					if(_potentialState > state)
					{
						owner = null;
						spillOver();
					}
				}
			}
		}
		override public function reset():void
		{
			//Stop animation
			if(_display != null)
			{
				removeChild(_display);
			}
			
			//Set its final color.
			setColor();
			
			//reset variables
			top = false;
			right = false;
			bottom = false;
			left = false;
			
			visible = true;
		}
		override public function setColor():void
		{
			//Render the cell
			switch(state)
			{
				case 0:
					_display = null;
					break;
				
				case 1:
					//T1 Cell
					switch(owner.playerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage01"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage01"), Main.animationFramerate);
							break;
						case 2:
							_display = new MovieClip(Main.assets.getTextures("Blue_Stage01"), Main.animationFramerate);
							break;
						case 3:
							_display = new MovieClip(Main.assets.getTextures("Yellow_Stage01"), Main.animationFramerate);
							break;
					}
					break;
				case 2:
					//T2 Cells
					switch(owner.playerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage02"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage02"), Main.animationFramerate);
							break;
						case 2:
							_display = new MovieClip(Main.assets.getTextures("Blue_Stage02"), Main.animationFramerate);
							break;
						case 3:
							_display = new MovieClip(Main.assets.getTextures("Yellow_Stage02"), Main.animationFramerate);
							break;
					}
					break;
				case 3:
					//T3 Cells
					switch(owner.playerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage03"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage03"), Main.animationFramerate);
							break;
						case 2:
							_display = new MovieClip(Main.assets.getTextures("Blue_Stage03"), Main.animationFramerate);
							break;
						case 3:
							_display = new MovieClip(Main.assets.getTextures("Yellow_Stage03"), Main.animationFramerate);
							break;
					}
					break;
				case 4:
					//T4 Cells
					switch(owner.playerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage04"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage04"), Main.animationFramerate);
							break;
						case 2:
							_display = new MovieClip(Main.assets.getTextures("Blue_Stage04"), Main.animationFramerate);
							break;
						case 3:
							_display = new MovieClip(Main.assets.getTextures("Yellow_Stage04"), Main.animationFramerate);
							break;
					}
					break;
				case 5:
					//Grey T1 Cell
					_display = new MovieClip(Main.assets.getTextures("Grey_Stage01"), Main.animationFramerate);
					break;
				case 6:
					//Grey T2 Cell
					_display = new MovieClip(Main.assets.getTextures("Grey_Stage02"), Main.animationFramerate);
					break;
				case 9:
					//Grey T3 Cell
					_display = new MovieClip(Main.assets.getTextures("Grey_Stage04"), Main.animationFramerate);
					break;
			}
			
			addDisplay();
		}
		public function destroy():void
		{
			adjacentBottomCell = null;
			adjacentLeftCell = null;
			adjacentRightCell = null;
			adjacentTopCell = null;
			
			gameManager = null;
		}
	}
}