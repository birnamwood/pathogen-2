package Core
{
	import OnlinePlay.OnlineGameManager;
	
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import com.sticksports.nativeExtensions.flurry.Flurry;
	
	import feathers.controls.PageIndicator;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.deg2rad;
	
	public class InGameTutorial extends BW_UI
	{
		private var _gameManager:GameManager;
		private var _onlineGameManager:OnlineGameManager;
		
		private var background:Scale9Image;
		private var TitleText:TextField;
		private var pageTitle:TextField
		private var _closeButton:BW_Button;
		private var _nextButton:BW_Button;
		private var _backButton:BW_Button;
		
		private var _screenHolder:Sprite;
		private var _pageHolder:Sprite;
		
		private var _tutorialGameManager:TutorialGameManager;
		
		private var _movingCell:Sprite;
		private var _pageIndicatorArray:Array = [];
		
		public function InGameTutorial(gameManager:GameManager, onlineGameManager:OnlineGameManager = null)
		{
			super();
			
			if(onlineGameManager == null)
				_gameManager = gameManager;
			else
				_onlineGameManager = onlineGameManager;
			
			if(Flurry.isSupported)
				Flurry.logEvent("Ingame Tutorial");

			//Create a transparent background.
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			addChild(transBackground);
			transBackground.alpha = .75;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			_screenHolder = new Sprite();
			addChild(_screenHolder);
			_screenHolder.x = PathogenTest.stageCenter.x;
			_screenHolder.y = PathogenTest.stageCenter.y;
			
			//Creating background so that content stands out.
			background = new Scale9Image(textures);
			_screenHolder.addChild(background);
			background.width = PathogenTest.wTenPercent * 4;
			background.height = PathogenTest.hTenPercent * 6.5;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .9;
			background.color = 0x00001a;
			
			const texture_02:Texture = Main.assets.getTexture("scale9");
			const textures_02:Scale9Textures = new Scale9Textures(texture_02, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var header:Scale9Image = new Scale9Image(textures_02);
			_screenHolder.addChild(header);
			header.width = background.width
			header.height = PathogenTest.hTenPercent * 1.1;
			header.pivotX = header.width/2;
			header.y = -background.height/2;
			
			var fontSize:int =PathogenTest.HD_Multiplyer * 32 / PathogenTest.scaleFactor;
			TitleText = new TextField(background.width, PathogenTest.hTenPercent/2, "How to Play", "Dekar", fontSize, Color.WHITE);
			TitleText.pivotX = TitleText.width/2;
			_screenHolder.addChild(TitleText);
			TitleText.y = -background.height/2 + (PathogenTest.hTenPercent * .1);
			
			fontSize =PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor;
			pageTitle = new TextField(background.width, PathogenTest.hTenPercent/2, "", "Dekar", fontSize, Color.WHITE);
			pageTitle.pivotX = pageTitle.width/2;
			_screenHolder.addChild(pageTitle);
			pageTitle.y = TitleText.y + (TitleText.height * .9);
			
			//Create Next button.
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_nextButton = new BW_Button(0.7, 1.0, "", 0);
			else
				_nextButton = new BW_Button(0.5, 0.75, "", 0);
			_nextButton.addImage(Main.assets.getTexture("Pathogen_BTN_Back"), Main.assets.getTexture("Pathogen_BTN_Back_Down"));
			_nextButton.rotation = deg2rad(180);
			_screenHolder.addChild(_nextButton);
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_nextButton.x = background.width * 0.55;
			else
				_nextButton.x = background.width/2;
			_nextButton.y = PathogenTest.hTenPercent * .3;
			_nextButton.visible = true;
			
			//Create back button
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_backButton = new BW_Button(0.7, 1.0, "", 0);
			else
				_backButton = new BW_Button(0.5, 0.75, "", 0);
			_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_Back"), Main.assets.getTexture("Pathogen_BTN_Back_Down"));
			_screenHolder.addChild(_backButton);
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_backButton.x = -(background.width * 0.55);
			else
				_backButton.x = -background.width/2;
			_backButton.y = _nextButton.y;
			_backButton.visible = false;
			
			//Create close button.
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_closeButton = new BW_Button(0.7, 1.0, "Close", 24);
			else
				_closeButton = new BW_Button(0.5, 0.7, "Close", 24);
			_closeButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			_screenHolder.addChild(_closeButton);
			_closeButton.x = -background.width/2 + _closeButton.width * .8;
			_closeButton.y = background.height/2 - _closeButton.height * .8;
			_closeButton.addEventListener(TouchEvent.TOUCH, quitOptions);
			
			_tutorialGameManager = new TutorialGameManager();
			_screenHolder.addChild(_tutorialGameManager);
			_tutorialGameManager.x = PathogenTest.wTenPercent * .2;
			
			
			var pageIndicatorHolder:Sprite = new Sprite();
			_screenHolder.addChildAt(pageIndicatorHolder, _screenHolder.numChildren - 1);
			
			for (var i:int = 0; i < 6; i++)
			{
				var pageIndicator:BW_Button = new BW_Button(0,0, "", 0);
				pageIndicator.addImage(Main.assets.getTexture("Page_Mark"), Main.assets.getTexture("Selected_Page_Mark"));
				pageIndicator.toggle = true;
				pageIndicatorHolder.addChild(pageIndicator);
				pageIndicator.x = i * PathogenTest.wTenPercent * .3;
				_pageIndicatorArray.push(pageIndicator);
				
				if(i == 0)
					pageIndicator.addEventListener(TouchEvent.TOUCH, openPageOne);
				if(i == 1)
					pageIndicator.addEventListener(TouchEvent.TOUCH, openPageTwo);
				if(i == 2)
					pageIndicator.addEventListener(TouchEvent.TOUCH, openPageThree);
				if(i == 3)
					pageIndicator.addEventListener(TouchEvent.TOUCH, openPageFour);
				if(i == 4)
					pageIndicator.addEventListener(TouchEvent.TOUCH, openPageFive);
				if(i == 5)
					pageIndicator.addEventListener(TouchEvent.TOUCH, openPageSix);
			}
			
			_pageIndicatorArray[0].toggleDown();
			
			pageIndicatorHolder.pivotX = pageIndicatorHolder.width/2;
			pageIndicatorHolder.pivotY = pageIndicatorHolder.height/2;
			
			pageIndicatorHolder.x = PathogenTest.wTenPercent * .16;
			pageIndicatorHolder.y = background.height/2 - (pageIndicatorHolder.height * 1.5);
			
			openPageOne(null);
		}
		private function openPageOne($e:TouchEvent):void
		{
			if($e)
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				var open:Boolean = true;
			
			if (touch || open)
			{
				resetPageButtons();
				_pageIndicatorArray[0].toggleDown();
				
				if(_pageHolder)
					_screenHolder.removeChild(_pageHolder);
				
				_pageHolder = new Sprite();
				_screenHolder.addChild(_pageHolder);
				
				pageTitle.text = "Starting the Game";
				
				var fontSize:int =PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
				var textField_01:TextField = new TextField(background.width, PathogenTest.hTenPercent, "Tap to place a cell", "Dekar", fontSize, Color.WHITE);
				textField_01.pivotX = textField_01.width/2;
				_pageHolder.addChild(textField_01);
				textField_01.y = -PathogenTest.hTenPercent * 1.5;

				var textField_02:TextField = new TextField(background.width, (PathogenTest.hTenPercent * 1.5), "The game is over when the " + "\n" + "board is filled", "Dekar", fontSize, Color.WHITE);
				textField_02.pivotX = textField_02.width/2;
				_pageHolder.addChild(textField_02);
				textField_02.y = PathogenTest.hTenPercent * .8;
				
				_nextButton.visible = true;
				_backButton.visible = false;
				
				removeButtonEventListeners();
				_nextButton.addEventListener(TouchEvent.TOUCH, openPageTwo);
				
				_tutorialGameManager.playPage1();
			}
		}
		private function openPageTwo($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				resetPageButtons();
				_pageIndicatorArray[1].toggleDown();
				
				if(_pageHolder)
					_screenHolder.removeChild(_pageHolder);
				
				_pageHolder = new Sprite();
				_screenHolder.addChild(_pageHolder);
				
				pageTitle.text = "Tools";
				
				var countDownIndicator:tutorialCountDownIndicator = new tutorialCountDownIndicator();
				_pageHolder.addChild(countDownIndicator);
				countDownIndicator.scaleX = 1.5;
				countDownIndicator.scaleY = 1.5;
				countDownIndicator.x = -background.width/2 + (countDownIndicator.width * .8);
				countDownIndicator.y = -PathogenTest.hTenPercent * .7;
				
				var fontSize:int =PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
				var textField_02:TextField = new TextField(background.width * .6, (PathogenTest.hTenPercent * 1.5), "Advanced cells gain one charge per turn", "Dekar", fontSize, Color.WHITE);
				textField_02.pivotY = textField_02.height/2;
				_pageHolder.addChild(textField_02);
				textField_02.x = countDownIndicator.x + (countDownIndicator.width * .6);
				textField_02.y = countDownIndicator.y
				textField_02.hAlign = HAlign.LEFT;
				
				var undoButton:BW_Button = new BW_Button(.5,.7, "", 0);
				undoButton.addImage(Main.assets.getTexture("Pathogen_BTN_Undo"), Main.assets.getTexture("Pathogen_BTN_Undo_down"));
				_pageHolder.addChild(undoButton);
				undoButton.x = countDownIndicator.x;
				undoButton.y = PathogenTest.hTenPercent;
					
				var textField_01:TextField = new TextField(background.width * .6, (PathogenTest.hTenPercent * 1.5), "Undo reverses the last move.", "Dekar", fontSize, Color.WHITE);
				textField_01.pivotY = textField_01.height/2;
				_pageHolder.addChild(textField_01);
				textField_01.x = undoButton.x + (undoButton.width * .8);
				textField_01.y = undoButton.y;
				textField_01.hAlign = HAlign.LEFT;
				
				//undoButton.y = quitButton.y;
				
				_nextButton.visible = true;
				_backButton.visible = true;
				
				//remove event listeners
				removeButtonEventListeners();
				
				_nextButton.addEventListener(TouchEvent.TOUCH, openPageThree);
				_backButton.addEventListener(TouchEvent.TOUCH, openPageOne);
				
				_tutorialGameManager.playPage2();
			}
		}
		private function openPageThree($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				resetPageButtons();
				_pageIndicatorArray[2].toggleDown();
				
				if(_pageHolder)
					_screenHolder.removeChild(_pageHolder);
				
				_pageHolder = new Sprite();
				_screenHolder.addChild(_pageHolder);
				
				pageTitle.text = "Evolve and Spread";
				
				var fontSize:int =PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
				var textField_02:TextField = new TextField(background.width, (PathogenTest.hTenPercent * 1.5), "Two of the same cell can be stacked to evolve", "Dekar", fontSize, Color.WHITE);
				textField_02.pivotX = textField_02.width/2;
				_pageHolder.addChild(textField_02);
				textField_02.y = PathogenTest.hTenPercent * .8;
				
				_nextButton.visible = true;
				_backButton.visible = true;
				
				//remove event listeners
				removeButtonEventListeners();
				
				_nextButton.addEventListener(TouchEvent.TOUCH, openPageFour);
				_backButton.addEventListener(TouchEvent.TOUCH, openPageTwo);
				
				_tutorialGameManager.playPage3();
			}
		}
		private function openPageFour($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				resetPageButtons();
				_pageIndicatorArray[3].toggleDown();
				
				if(_pageHolder)
					_screenHolder.removeChild(_pageHolder);
				
				_pageHolder = new Sprite();
				_screenHolder.addChild(_pageHolder);
				
				pageTitle.text = "Locking Down";
				
				var fontSize:int =PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
				var textField_02:TextField = new TextField(background.width, (PathogenTest.hTenPercent * 1.5), "Wall cells cannot be taken or destroyed by an enemy.", "Dekar", fontSize, Color.WHITE);
				textField_02.pivotX = textField_02.width/2;
				_pageHolder.addChild(textField_02);
				textField_02.y = PathogenTest.hTenPercent * .8;
				
				_nextButton.visible = true;
				_backButton.visible = true;
				
				//remove event listeners
				removeButtonEventListeners();
				
				_nextButton.addEventListener(TouchEvent.TOUCH, openPageFive);
				_backButton.addEventListener(TouchEvent.TOUCH, openPageThree);
				
				_tutorialGameManager.playPage4();
			}
		}
		private function openPageFive($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				resetPageButtons();
				_pageIndicatorArray[4].toggleDown();
				
				if(_pageHolder)
					_screenHolder.removeChild(_pageHolder);
				
				_pageHolder = new Sprite();
				_screenHolder.addChild(_pageHolder);
				
				pageTitle.text = "Conquer";
				
				var fontSize:int =PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
				var textField_02:TextField = new TextField(background.width, PathogenTest.hTenPercent * 1.5, "Conquer cells by placing higher level cells on lower level cells.", "Dekar", fontSize, Color.WHITE);
				textField_02.pivotX = textField_02.width/2;
				_pageHolder.addChild(textField_02);
				textField_02.y = PathogenTest.hTenPercent * .56;
				
				_nextButton.visible = true;
				_backButton.visible = true;
				
				removeButtonEventListeners();
				
				_nextButton.addEventListener(TouchEvent.TOUCH, openPageSix);
				_backButton.addEventListener(TouchEvent.TOUCH, openPageFour);

				_tutorialGameManager.playPage5();
			}
		}
		private function openPageSix($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				resetPageButtons();
				_pageIndicatorArray[5].toggleDown();
				
				if(_pageHolder)
					_screenHolder.removeChild(_pageHolder);
				
				_pageHolder = new Sprite();
				_screenHolder.addChild(_pageHolder);
				
				pageTitle.text = "The Virus";
				
				var fontSize:int =PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
				var textField_02:TextField = new TextField(background.width, (PathogenTest.hTenPercent * 1.5), "Viruses destroy all adjacent cells of the same type, even your own.", "Dekar", fontSize, Color.WHITE);
				textField_02.pivotX = textField_02.width/2;
				_pageHolder.addChild(textField_02);
				textField_02.y = PathogenTest.hTenPercent * .8;
				
				_nextButton.visible = false;
				_backButton.visible = true;
				
				removeButtonEventListeners();
				_backButton.addEventListener(TouchEvent.TOUCH, openPageFive);
				
				_tutorialGameManager.playPage6();
			}
		}
		private function resetPageButtons():void
		{
			for( var i:int = 0; i < _pageIndicatorArray.length; i++)
			{
				var pageIndicator:BW_Button = _pageIndicatorArray[i];
				pageIndicator.toggleUp();
			}
		}
		private function removeButtonEventListeners():void
		{
			_backButton.removeEventListener(TouchEvent.TOUCH, openPageFive);
			_backButton.removeEventListener(TouchEvent.TOUCH, openPageFour);
			_backButton.removeEventListener(TouchEvent.TOUCH, openPageThree);
			_backButton.removeEventListener(TouchEvent.TOUCH, openPageTwo);
			_backButton.removeEventListener(TouchEvent.TOUCH, openPageOne);
			
			_nextButton.removeEventListener(TouchEvent.TOUCH, openPageSix);
			_nextButton.removeEventListener(TouchEvent.TOUCH, openPageFive);
			_nextButton.removeEventListener(TouchEvent.TOUCH, openPageFour);
			_nextButton.removeEventListener(TouchEvent.TOUCH, openPageThree);
			_nextButton.removeEventListener(TouchEvent.TOUCH, openPageTwo);
		}
		private function quitOptions($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				//leave & return to Game.
				startFadeOut();
			}
		}
		override public function death():void
		{
			if(_gameManager)
				_gameManager.removeInGameTutorial();
			
			if(_onlineGameManager)
				_onlineGameManager.removeInGameTutorial();
		}
		public function GarbageCollection():void
		{
			var i:int;
			
			if(_gameManager)
				_gameManager = null;
			
			if(_onlineGameManager)
				_onlineGameManager = null;
			
			_closeButton.destroy();
			_closeButton.dispose();
			_screenHolder.removeChild(_closeButton);
			_closeButton = null;
			
			_nextButton.destroy();
			_nextButton.dispose();
			_screenHolder.removeChild(_nextButton);
			_nextButton = null;
			
			_backButton.destroy();
			_backButton.dispose();
			_screenHolder.removeChild(_backButton);
			_backButton = null;
			
			if(_tutorialGameManager)
			{
				_tutorialGameManager.garbageCollection();
				_tutorialGameManager.dispose();
				_screenHolder.removeChild(_tutorialGameManager);
				_tutorialGameManager = null;
			}
			
			if(_pageHolder)
			{
				_pageHolder.dispose();
				_screenHolder.removeChild(_pageHolder);
				_pageHolder = null;
			}
			
			if(_pageIndicatorArray.length > 0)
			{
				for(i = 0; i < _pageIndicatorArray.length; i++)
				{
					_pageIndicatorArray[i].dispose();
				}
			}
			
			if(_screenHolder)
			{
				_screenHolder.dispose();
				
				var numChildren:int = _screenHolder.numChildren;
				var child:Sprite = _screenHolder.getChildAt(numChildren - 1) as Sprite;
				
				for(i = child.numChildren - 1; i >= 0; i--)
				{
					child.getChildAt(i).dispose();
					child.removeChildAt(i);
				}
				
				for(i = numChildren - 1; i >= 0; i--)
				{
					_screenHolder.getChildAt(i).dispose();
					_screenHolder.removeChildAt(i);
				}
				
				removeChild(_screenHolder);
				_screenHolder = null;
			}
			
			
		}
	}
}