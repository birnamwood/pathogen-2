package Core
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	import flash.text.ReturnKeyLabel;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	
	public class QuitScreen extends BW_UI
	{
		private var _gameManager:GameManager;
		
		//Campaign Buttons
		private var _BacktoCampaign:BW_Button;
		private var _resumeBt:BW_Button;
		private var _retryBt:BW_Button;
		
		//Core Buttons
		private var _yesButton:BW_Button;
		private var _noButton:BW_Button;
		
		public function QuitScreen(gameManager:GameManager)
		{
			super();
			
			_gameManager = gameManager;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			addChild(transBackground);
			transBackground.alpha = .75;
			
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 5;
			background.height = PathogenTest.hTenPercent * 5.0;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			
			var header:Scale9Image = new Scale9Image(textures);
			addChild(header);
			header.width = PathogenTest.wTenPercent * 5;
			header.height = PathogenTest.hTenPercent;
			header.pivotX = header.width/2;
			header.y = -background.height/2;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
			var title:TextField = new TextField(header.width, header.height, "Quit", "Questrial", fontSize, Color.WHITE);
			title.pivotX = title.width/2;
			addChild(title);
			title.y = header.y;
			
			if(_gameManager.campaign)
			{
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_resumeBt = new Utils.BW_Button(2, 1.0, "Resume", 32);
				else
					_resumeBt = new Utils.BW_Button(2, 0.75, "Resume", 32);
				addChild(_resumeBt);
				_resumeBt.y = -PathogenTest.hTenPercent * 0.75;
				_resumeBt.addEventListener(TouchEvent.TOUCH, onTouchResume);
				
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_retryBt = new Utils.BW_Button(2, 1.0, "Retry", 32);
				else
					_retryBt = new Utils.BW_Button(2, 0.75, "Retry", 32);
				addChild(_retryBt);
				_retryBt.y = PathogenTest.hTenPercent * 0.5;
				_retryBt.addEventListener(TouchEvent.TOUCH, onTouchRetry);
				
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_BacktoCampaign = new Utils.BW_Button(2, 1.0, "Campaign", 32);
				else
					_BacktoCampaign = new Utils.BW_Button(2, 0.75, "Campaign", 32);
				addChild(_BacktoCampaign);
				_BacktoCampaign.y = PathogenTest.hTenPercent * 1.75;
				_BacktoCampaign.addEventListener(TouchEvent.TOUCH, onTouchBackToCampaign);
			}
			else
			{
				fontSize = PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor;
				var textY:int = 200 / PathogenTest.scaleFactor;
				var textfield:TextField = new TextField(background.width, textY, "Are you sure you want to Quit?", "Dekar", fontSize, Color.WHITE);
				textfield.pivotX = textfield.width/2;
				textfield.pivotY = textfield.height/2;
				addChild(textfield);
				
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_yesButton = new Utils.BW_Button(2, 1.0, "Yes", 32);
				else
					_yesButton = new Utils.BW_Button(2, 0.75, "Yes", 32);
				addChild(_yesButton);
				_yesButton.x = -background.width/2 + _yesButton.width/2 + PathogenTest.wTenPercent * .2;
				_yesButton.y = background.height/2 - _yesButton.height;
				_yesButton.addEventListener(TouchEvent.TOUCH, onTouchYes);
				
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_noButton = new Utils.BW_Button(2, 1.0, "No", 32);
				else
					_noButton = new Utils.BW_Button(2, 0.75, "No", 32);
				addChild(_noButton);
				_noButton.x = background.width/2 - _noButton.width/2 - PathogenTest.wTenPercent * .2;
				_noButton.y = background.height/2 - _yesButton.height;
				_noButton.addEventListener(TouchEvent.TOUCH, onTouchNo);
			}
		}
		private function onTouchResume($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				removeListeners();
				startFadeOut();
			}
		}
		private function onTouchRetry($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				removeListeners();
				_gameManager.retryGame();
				startFadeOut();
			}
		}
		private function onTouchBackToCampaign($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				removeListeners();
				_gameManager.Quit();
				startFadeOut();
			}
		}
		private function onTouchYes($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				removeListeners();
				_gameManager.quitGame();
				startFadeOut();
			}
		}
		private function onTouchNo($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				removeListeners();
				startFadeOut();
			}
		}
		private function removeListeners():void
		{
			if(_gameManager.campaign)
			{
				_resumeBt.removeEventListener(TouchEvent.TOUCH, onTouchResume);
				_retryBt.removeEventListener(TouchEvent.TOUCH, onTouchRetry);
				_BacktoCampaign.removeEventListener(TouchEvent.TOUCH, onTouchBackToCampaign);
			}
			else
			{
				_yesButton.removeEventListener(TouchEvent.TOUCH, onTouchYes);
				_noButton.removeEventListener(TouchEvent.TOUCH, onTouchNo);
			}
		}
		public override function death():void
		{
			if(_gameManager.campaign)
			{
				_resumeBt.destroy();
				_resumeBt.dispose();
				removeChild(_resumeBt);
				_resumeBt = null;
				
				_retryBt.destroy();
				_retryBt.dispose();
				removeChild(_retryBt);
				_retryBt = null;
				
				_BacktoCampaign.destroy();
				_BacktoCampaign.dispose();
				removeChild(_BacktoCampaign);
				_BacktoCampaign = null;
			}
			else
			{
				_yesButton.destroy();
				_yesButton.dispose();
				removeChild(_yesButton);
				_yesButton = null;
				
				_noButton.destroy();
				_noButton.dispose();
				removeChild(_noButton);
				_noButton = null;
			}
			
			_gameManager.removeQuit();
			_gameManager = null;
		}
	}
}