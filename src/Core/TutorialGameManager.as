package Core
{
	import UI.Main;
	
	import flash.geom.Point;
	
	import starling.animation.DelayedCall;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class TutorialGameManager extends Sprite
	{
		private const BOARD_WIDTH:int = 6;
		private const BOARD_HEIGHT:int = 3;
		private const CLOSE_ENOUGH:Number = 4.0;
		private var MOVING_CELL_SPEED:Number = 4.5;
		private const MOVING_CELL_START_X:int = -25;
		private const MOVING_CELL_START_Y:int = -PathogenTest.hTenPercent * .3;
		private const PAUSE_SECONDS:int = 4;
		private const DELAY_BEFORE_START:Number = 1.0;
		
		private var _gameBoard:Array = [];
		private var _cellHolder:Sprite;
		private var _cellSize:int;
		
		private var _playerTeam:int;
		private var _cellType:int;
		
		private var _movingCell:Image;
		private var _page1Tween:Tween;
		
		private var currentPage:int = 1;
		private var _moveUnderway:Boolean;
		
		private var targetCell:TutorialCell;
		
		private var tutorial3Stage:int = 0;
		
		private var delayedCall:DelayedCall;
		
		public function TutorialGameManager()
		{
			createBoard();
			
			//if(PathogenTest.device == PathogenTest.iPAD_HD)
				//MOVING_CELL_SPEED = MOVING_CELL_SPEED * PathogenTest.scaleFactor;
			MOVING_CELL_SPEED = MOVING_CELL_SPEED * PathogenTest.HD_Multiplyer;
			_playerTeam = 0;
			_cellType = 0;
		}
		private function createBoard():void
		{
			_cellHolder = new Sprite();
			addChild(_cellHolder);
			
			_cellSize = 0;
			var cellCount:int = 0;
			//Create the board
			for (var i:int = 0; i<BOARD_WIDTH; i++)
			{
				for (var j:int = 0; j<BOARD_HEIGHT; j++)
				{
					var newCell:TutorialCell = new TutorialCell(cellCount, this);
					
					newCell.scaleX = 1.0;
					newCell.scaleY = 1.0;
					newCell.ownerTeam = -1;
					
					if (_cellSize == 0)
						_cellSize = newCell.width;
					
					var tileGap:Number = PathogenTest.wTenPercent * 0.5 * newCell.scaleX;
					
					newCell.x = tileGap * i;
					newCell.y = tileGap * j;
					
					newCell.state = 0;
					newCell.reset();
					
					_cellHolder.addChild(newCell);
					_gameBoard.push(newCell);
					
					cellCount++;
				}
			}
			
			//Assign neighboring cells to all cells.
			for each(var cell:TutorialCell in _gameBoard)
			{
				cell.adjacentTopCell = _gameBoard[cell.location - 1];
				cell.adjacentBottomCell = _gameBoard[cell.location + 1];
				cell.adjacentLeftCell = _gameBoard[cell.location - BOARD_HEIGHT];
				cell.adjacentRightCell = _gameBoard[cell.location + BOARD_HEIGHT];
			}
			
			_cellHolder.pivotX = _cellHolder.width / 2;
			_cellHolder.pivotY = _cellHolder.height / 2;
		}
		
		public function playPage1():void
		{
			currentPage = 1;
			
			resetAllCells();
			
			for (var i:int=0; i<_gameBoard.length; i++)
				_gameBoard[i].visible = false;
			
			_movingCell = new Image(Main.assets.getTexture("Green_Stage01"));
			addMovingCell();
			
			_movingCell.x = PathogenTest.hTenPercent * 1.8;
			_movingCell.y = PathogenTest.wTenPercent * .8;
			
			page1Animation();
		}
		private function page1Animation():void
		{
			if(_movingCell)
			{
				_movingCell.alpha = 0;
				_movingCell.scaleX = 0;
				_movingCell.scaleY = 0;
				
				_page1Tween = new Tween(_movingCell, 1);
				_page1Tween.fadeTo(1);
				_page1Tween.scaleTo(2);
				_page1Tween.onComplete = page1AnimationComplete;
				Starling.juggler.add(_page1Tween);
			}
		}
		private function page1AnimationComplete():void
		{
			delayedCall = Starling.juggler.delayCall(page1Animation, DELAY_BEFORE_START* 3)
		}
		public function playPage2():void
		{
			resetAllCells();
			_moveUnderway = false;
			currentPage = 2;
			
			for (var i:int=0; i<_gameBoard.length; i++)
				_gameBoard[i].visible = false;
		}
		public function playPage3():void
		{
			resetAllCells();
			_moveUnderway = false;
			currentPage = 3;
			tutorial3Stage = 0;
			
			// set up board
			_gameBoard[4].state = 1;
			_gameBoard[4].ownerTeam = 0;
			_gameBoard[4].setColor();
			
			_gameBoard[13].state = 2;
			_gameBoard[13].ownerTeam = 0;
			_gameBoard[13].setColor();
			
			// create and start moving cell
			_movingCell = new Image(Main.assets.getTexture("Green_Stage01"));
			addMovingCell();
			
			_cellType = 1;
			_playerTeam = 0;
			
			targetCell = _gameBoard[4]
			addEventListener(Event.ENTER_FRAME, moveCell);
		}						  
		public function playPage4():void
		{
			resetAllCells();
			_moveUnderway = false;
			currentPage = 4;
			
			for (var i:int=0; i<_gameBoard.length; i++)
			{
				if (i%3 == 1)
					_gameBoard[i].state = 3;
				else
					_gameBoard[i].state = 2;
				
				if (i <= 8)
					_gameBoard[i].ownerTeam = 0;
				else
					_gameBoard[i].ownerTeam = 1;
				
				_gameBoard[i].reset();
			}
			
			// create and start moving cell
			_movingCell = new Image(Main.assets.getTexture("Green_Stage03"));
			addMovingCell();
			
			_playerTeam = 0;
			_cellType = 2;
			
			targetCell = _gameBoard[7];
			addEventListener(Event.ENTER_FRAME, moveCell)
		}		
		public function playPage5():void
		{
			resetAllCells();
			_moveUnderway = false;
			currentPage = 5;
			
			_gameBoard[4].state = 1;
			_gameBoard[4].ownerTeam = 1;
			_gameBoard[4].setColor();
			_gameBoard[7].state = 1;
			_gameBoard[7].ownerTeam = 1;
			_gameBoard[7].setColor();
			_gameBoard[10].state = 1;
			_gameBoard[10].ownerTeam = 1;
			_gameBoard[10].setColor();
			_gameBoard[13].state = 1;
			_gameBoard[13].ownerTeam = 1;
			_gameBoard[13].setColor();
			
			// create and start moving cell
			_movingCell = new Image(Main.assets.getTexture("Green_Stage02"));
			addMovingCell();
			
			_cellType = 1;
			_playerTeam = 0;
			
			targetCell = _gameBoard[7];
			addEventListener(Event.ENTER_FRAME, moveCell);
		}
		public function playPage6():void
		{
			resetAllCells();
			_moveUnderway = false;
			currentPage = 6;
			
			for (var i:int=0; i<_gameBoard.length; i++)
			{
				if (i%3 == 1)
					_gameBoard[i].state = 3;
				else
					_gameBoard[i].state = 2;
				
				if (i <= 8)
					_gameBoard[i].ownerTeam = 0;
				else
					_gameBoard[i].ownerTeam = 1;
				_gameBoard[i].setColor();
			}
			
			// create and start moving cell
			_movingCell = new Image(Main.assets.getTexture("Red_Virus"));
			addMovingCell();
			
			_cellType = 3;
			_playerTeam = 1;
			
			targetCell = _gameBoard[4];
			addEventListener(Event.ENTER_FRAME, moveCell);
		}
		private function moveCell($e:Event):void
		{
			if (currentPage > 2)
			{
				if (_movingCell.x != targetCell.x && _movingCell.y != targetCell.y)
				{
					var direction:Point = new Point(targetCell.x - _movingCell.x, targetCell.y - _movingCell.y);
					
					if (direction.length < (PathogenTest.HD_Multiplyer * CLOSE_ENOUGH / PathogenTest.scaleFactor))
					{
						removeEventListener(Event.ENTER_FRAME, moveCell);
						_movingCell.visible = false;
						
						targetCell.changeState(cellType, playerTeam);
						
						_moveUnderway = true;
						addEventListener(Event.ENTER_FRAME, update);
					}
					else
					{
						direction.normalize(PathogenTest.HD_Multiplyer * MOVING_CELL_SPEED / PathogenTest.scaleFactor);
						_movingCell.x += direction.x;
						_movingCell.y += direction.y;
					}
				}
			}
		}
		private function update($e:Event):void
		{
			if(_moveUnderway)
			{
				var complete:Boolean = true;
				
				for (var i:int = 0; i < _gameBoard.length; i++)
				{
					var cell:TutorialCell = _gameBoard[i];
					
					if(cell.flipping)
						complete = false;
				}
				
				if(complete)
				{
					removeEventListener(Event.ENTER_FRAME, update);
					
					if(currentPage == 3)
					{
						tutorial3Stage++;
						
						if(tutorial3Stage == 1)
						{
							if (_movingCell)
								removeMovingCell();
							
							_movingCell = new Image(Main.assets.getTexture("Green_Stage02"));
							addMovingCell();
							
							_movingCell.x = PathogenTest.wTenPercent * 3;
							
							_playerTeam = 0;
							_cellType = 2;
							
							targetCell = _gameBoard[13];
							addEventListener(Event.ENTER_FRAME, moveCell);
							
						}
						else
						{
							tutorial3Stage = 0;
							delayedCall = Starling.juggler.delayCall(playPage3, DELAY_BEFORE_START);
						}
					}
						
					if(currentPage == 4)
						delayedCall = Starling.juggler.delayCall(playPage4, DELAY_BEFORE_START);
					if(currentPage == 5)

						delayedCall = Starling.juggler.delayCall(playPage5, DELAY_BEFORE_START);
					if(currentPage == 6)
						delayedCall = Starling.juggler.delayCall(playPage6, DELAY_BEFORE_START);
				}
			}
		}
		public function shiftStack(cell:TutorialCell):void
		{
			if(_cellHolder)
				_cellHolder.setChildIndex(cell, _cellHolder.numChildren -1);
		}
		public function resetAllCells():void
		{
			Starling.juggler.remove(delayedCall);
			removeEventListener(Event.ENTER_FRAME, moveCell);
			
			removeMovingCell();
			
			if(_gameBoard)
			{
				for (var i:int=0; i<_gameBoard.length; i++)
				{
					_gameBoard[i].CleanCell();
					_gameBoard[i].removeRippleFromGameMan();
					_gameBoard[i].hasAlreadySpilledOver = false;
					_gameBoard[i].visible = true;
				}
			}
		}
		private function addMovingCell():void
		{
			_movingCell.pivotX = _movingCell.width / 2;
			_movingCell.pivotY = _movingCell.height / 2;
			_cellHolder.addChild(_movingCell);
			_movingCell.x = MOVING_CELL_START_X;
			_movingCell.y = MOVING_CELL_START_Y;
		}
		private function removeMovingCell():void
		{
			if(_movingCell)
			{
				Starling.juggler.removeTweens(_movingCell);
				_movingCell.dispose();
				
				if(_cellHolder)
					_cellHolder.removeChild(_movingCell)
						
				_movingCell = null;
			}
		}
		
		public function garbageCollection():void
		{
			Starling.juggler.remove(delayedCall);
			Starling.juggler.remove(_page1Tween);
			
			removeEventListener(Event.ENTER_FRAME, update);
			removeEventListener(Event.ENTER_FRAME, moveCell);
			
			targetCell = null;
			
			// reset all cells
			for each (var cell:TutorialCell in _gameBoard)
			{
				cell.visible = true;
				cell.CleanCell()
				cell.dispose();
				_cellHolder.removeChild(cell);
				cell = null;
			}
			
			if(_movingCell)
			{
				_movingCell.dispose();
				_cellHolder.removeChild(_movingCell);
				_movingCell = null;
			}
			
			
			
			removeChild(_cellHolder);
			_cellHolder = null;
		}
		
		public function get playerTeam():int { return _playerTeam; }
		public function get cellType():int { return _cellType; }
	}
}