package Core
{
	import PieChart.PieChart;
	
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_SocialButton;
	import Utils.BW_UI;
	
	import com.milkmangames.nativeextensions.GoViral;
	import com.milkmangames.nativeextensions.events.GVFacebookEvent;
	import com.sticksports.nativeExtensions.flurry.Flurry;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.deg2rad;
	
	public class PostGame extends BW_UI
	{
		private var gameManager:GameManager;
		
		//Buttons
		private var viewReplay:BW_Button;
		private var leaveGame:BW_Button;
		private var viewMap:BW_Button;
		
		private var endGameOverview:BW_Button;
		private var gameOverview:BW_Button;
		private var graphs:BW_Button;
		
		//Arrays
		private var arrayOfTriangleCounts:Array = [];
		private var arrayOfDiamondCounts:Array = [];
		private var arrayOfStarCounts:Array = [];
		private var arrayOfSolidCounts:Array = [];
		private var arrayOfAllTileCounts:Array = [];
		private var arrayOfHighestCounts:Array = [];
		private var playerPercentArray:Array = [];
		private var gameOverviewChartPlayerPercentArray:Array = [];
		private var playerPercentArrayOfAllTiles:Array = [];
		
		// arrays for overall
		private var overallArrayOfTriangleCounts:Array = [];
		private var overallArrayOfDiamondCounts:Array = [];
		private var overallArrayOfStarCounts:Array = [];
		private var overallArrayOfSolidCounts:Array = [];
		private var overallArrayOfAllTileCounts:Array = [];
		private var overallArrayOfHighestCounts:Array = [];
			
		//Counts
		private var highestTriangleCount:int;
		private var highestDiamondCount:int;
		private var highestStarCount:int;
		private var highestSolidCount:int;
		private var highestOfAllTileCounts:int;
		
		// counts for overall
		private var overallHighestTriangleCount:int;
		private var overallHighestDiamondCount:int;
		private var overallHighestStarCount:int;
		private var overallHighestSolidCount:int;
		private var overallHighestOfAllTileCounts:int;
		
		private var percentHolder:Sprite;;
		
		private var pieChartImage:Image;
		private var bData:BitmapData;
		
		private var graphChartImage:Image;
		private var linesChartImage:Image;
		
		//private var facebookButton:BW_SocialButton;
		//private var twitterButton:BW_SocialButton;
		private var facebookButton:BW_Button;
		private var twitterButton:BW_Button;
		
		private var postedToFacebook:Boolean;
		
		private var _pieChartSliceMax:Array;
		private var _pieChartSliceCurrent:Array;
		private var _pieChartColors:Array;
		private var _pieChartTextFields:Array;
		
		private var linesbData:BitmapData;
		private var graphbData:BitmapData;
		
		public function PostGame(gameManager:GameManager)
		{
			super();
			
			this.gameManager = gameManager;
			
			var transBackground:Quad = new Quad(PathogenTest.wTenPercent * 10, PathogenTest.hTenPercent * 10, 0x000000);
			addChild(transBackground);
			transBackground.alpha = .75;

			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));

			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 10;
			//if (background.width > Main.TEXTURE_LIMIT)
				//background.width = Main.TEXTURE_LIMIT;
			background.height = PathogenTest.hTenPercent * 5.5;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.x = PathogenTest.stageCenter.x;
			background.y = PathogenTest.hTenPercent * 5.5;
			background.color = 0x00001a;
	
			var buttonHeight:Number;
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				buttonHeight = 1;
			else
				buttonHeight = 0.75;
			
			viewReplay = new Utils.BW_Button(2, buttonHeight, "View Replay", 32);
			addChild(viewReplay);
			viewReplay.x = PathogenTest.wTenPercent * 2.5;
			viewReplay.y = PathogenTest.hTenPercent * 9.1;
			viewReplay.addEventListener(TouchEvent.TOUCH, onTouchReplay);

			leaveGame = new Utils.BW_Button(2.25, buttonHeight * 1.25, "Leave Game", 32);
			addChild(leaveGame);
			leaveGame.x = PathogenTest.stageCenter.x;
			leaveGame.y = viewReplay.y;
			leaveGame.addEventListener(TouchEvent.TOUCH, onTouchLeaveGame);

			viewMap = new Utils.BW_Button(2, buttonHeight, "View Map", 32);
			addChild(viewMap);
			viewMap.x = PathogenTest.wTenPercent * 7.5;
			viewMap.y = viewReplay.y;
			viewMap.addEventListener(TouchEvent.TOUCH, gameManager.viewMap);

			if(GoViral.isSupported() && GoViral.goViral.isFacebookSupported())
			{
				// facebook button
				if (Main.isPhone())
					facebookButton = new BW_Button(0.75, 1.0, "", 0);
				else
					facebookButton = new BW_Button(0.55, 0.75, "", 0);
				facebookButton.addImage(Main.assets.getTexture("Facebook"), Main.assets.getTexture("Facebook_Active"));
				addChild(facebookButton);
				//facebookButton.x = viewReplay.x + viewReplay.width/2;
				facebookButton.x = PathogenTest.wTenPercent * 9.5;
				//facebookButton.y = viewReplay.y + (facebookButton.height * 1.2);
				facebookButton.y = PathogenTest.hTenPercent * 9.1;
				facebookButton.addEventListener(TouchEvent.TOUCH, onTouchFacebook);
			}
			
			// twitter button
			if(GoViral.isSupported() && GoViral.goViral.isTweetSheetAvailable())
			{
				if (Main.isPhone())
					twitterButton = new BW_Button(0.75, 1.0, "", 0);
				else
					twitterButton = new BW_Button(0.55, 0.75, "", 0);
				twitterButton.addImage(Main.assets.getTexture("Twitter"), Main.assets.getTexture("Twitter_Active"));
				addChild(twitterButton);
				//twitterButton.x = viewMap.x - viewMap.width/2;
				twitterButton.x = PathogenTest.wTenPercent * 0.5;
				//twitterButton.y = facebookButton.y
				twitterButton.y = PathogenTest.hTenPercent * 9.1;
				twitterButton.addEventListener(TouchEvent.TOUCH, onTouchTwitter);
			}
			
			var buffer:Number = PathogenTest.wTenPercent * .2;
			
			endGameOverview = new Utils.BW_Button(2.5, 1.25, "EndGame Overview", 24, Color.WHITE);
			addChild(endGameOverview);
			endGameOverview.toggle = true;
			endGameOverview.x = PathogenTest.wTenPercent * 1.5;
			endGameOverview.y = PathogenTest.hTenPercent * 2.25;
			//endGameOverview.y = (PathogenTest.stageCenter.y - background.height/2) - PathogenTest.hTenPercent * .28;
			endGameOverview.addEventListener(TouchEvent.TOUCH, navigateToEndGamePage);
			endGameOverview.toggleDown();
			setChildIndex(endGameOverview, 0);

			gameOverview = new Utils.BW_Button(2.5, 1.25, "Game Overview", 24, Color.WHITE);
			addChild(gameOverview);
			gameOverview.toggle = true;
			gameOverview.x = (endGameOverview.x + endGameOverview.width) + buffer;
			gameOverview.y = endGameOverview.y;
			gameOverview.addEventListener(TouchEvent.TOUCH, navigateToGameOverview);
			setChildIndex(gameOverview, 0);

			graphs = new Utils.BW_Button(2.5, 1.25, "Graph", 24, Color.WHITE);
			addChild(graphs);
			graphs.toggle = true;
			graphs.x = (gameOverview.x + gameOverview.width) + buffer;
			graphs.y = endGameOverview.y;
			graphs.addEventListener(TouchEvent.TOUCH, navigateToGraphsPage);
			setChildIndex(graphs, 0);
			
			setChildIndex(transBackground, 0);

			var fontSize:int = PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
			var textX:int = (PathogenTest.wTenPercent * 5) / PathogenTest.scaleFactor;
			var textY:int = PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor;
			var name:Array = gameManager.mapName.split("\n");
			var mapName:TextField;
			
			if(name.length > 1)
			{
				var tmpName:String = name[0] + name[name.length - 1];
				mapName = new TextField(textX, textY, tmpName + " : " + gameManager.minuteCounter + " Minutes" , "Dekar", fontSize, Color.WHITE);
			}
			else
				mapName = new TextField(textX, textY, gameManager.mapName + " : " + gameManager.minuteCounter + " Minutes" , "Dekar", fontSize, Color.WHITE);
			
			mapName.pivotY = mapName.height/2;
			mapName.hAlign = HAlign.LEFT;
			addChild(mapName);
			mapName.x = endGameOverview.x - endGameOverview.width/2;
			mapName.y = PathogenTest.hTenPercent * 1.3;

			var player:Player;
				
			for each (player in gameManager.playerArray)
			{
				playerPercentArray.push(player.playerPercent);
			}
			
			var highestPercent:int = Math.max.apply(null, playerPercentArray)
			
			var winningTeam:String;
			var winningTeamsColor:Number;
			var numOfWinners:int;
			
			for  each (player in gameManager.playerArray)
			{
				if (player.playerPercent >= highestPercent)
				{
					winningTeam = player.teamName + " has won!";
					winningTeamsColor = player.playerHexColor;
					
					numOfWinners++;
				}
			}
			
			if (numOfWinners > 1)
			{
				winningTeam = "Tie!";
				winningTeamsColor = Color.WHITE;
			}
			
			fontSize = PathogenTest.HD_Multiplyer * 70 / PathogenTest.scaleFactor;
			textX = PathogenTest.stageWidth;
			if (textX > Main.TEXTURE_LIMIT)
				textX = Main.TEXTURE_LIMIT;
			textY = PathogenTest.HD_Multiplyer * 100 / PathogenTest.scaleFactor;
			var winnerField:TextField = new TextField(textX, textY, winningTeam , "Questrial", fontSize, winningTeamsColor);
			winnerField.pivotY = mapName.height/2;
			winnerField.hAlign = HAlign.LEFT;
			addChild(winnerField);
			winnerField.x = endGameOverview.x - endGameOverview.width/2;
			winnerField.y = PathogenTest.hTenPercent * .4;

			var totalNumberOfCellsCreated:int = 0;
			var totalNumberofCellsDestoyed:int = 0;
			
			// generate data for overall page
			for each (player in gameManager.playerArray)
			{
				overallArrayOfTriangleCounts.push(player.totalNumOfTriangleTiles);
				overallArrayOfDiamondCounts.push(player.totalNumOfDiamondTiles);
				overallArrayOfStarCounts.push(player.totalNumOfStarTiles);
				overallArrayOfSolidCounts.push(player.totalNumOfSolidTiles);
				
				player.totalNumOfAllTilesAllGame = player.totalNumOfTriangleTiles + player.totalNumOfDiamondTiles + player.totalNumOfStarTiles + player.totalNumOfSolidTiles;
				overallArrayOfAllTileCounts.push(player.totalNumOfAllTilesAllGame);
				
				player.arrayofAllTotalTileCounts = [player.totalNumOfTriangleTiles, player.totalNumOfDiamondTiles, player.totalNumOfStarTiles, player.totalNumOfSolidTiles, player.totalNumOfAllTilesAllGame];
				
				totalNumberOfCellsCreated += player.totalNumOfAllTilesAllGame;
				totalNumberofCellsDestoyed += player.totalCellsDestroyed;
			}
			
			overallHighestTriangleCount = Math.max.apply(null, overallArrayOfTriangleCounts);
			overallHighestDiamondCount = Math.max.apply(null, overallArrayOfDiamondCounts);
			overallHighestStarCount = Math.max.apply(null, overallArrayOfStarCounts);
			overallHighestSolidCount = Math.max.apply(null, overallArrayOfSolidCounts);
			overallHighestOfAllTileCounts = Math.max.apply(null, overallArrayOfAllTileCounts);
			overallArrayOfHighestCounts = [overallHighestTriangleCount, overallHighestDiamondCount, overallHighestStarCount, overallHighestSolidCount, overallHighestOfAllTileCounts];
			
			if(Flurry.isSupported && gameManager.campaign == null)
				Flurry.logEvent("Local Game Over", {WinningColor: winningTeam, WinningPercent: highestPercent, TotalCellsCreated: totalNumberOfCellsCreated, TotalCellsDestoyed: totalNumberofCellsDestoyed});
			
			endGameOverviewPage();
		}
		private function onTouchLeaveGame($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.BEGAN);
			if (touch)
			{
				gameManager.Quit();
			}
		}
		private function onTouchReplay($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.BEGAN);
			if (touch)
			{
				gameManager.addReplayBar();
			}
		}
		private function onTouchFacebook($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				Main.debugText.text = GoViral.isSupported() + " -- " + GoViral.goViral.isFacebookSupported();
				
				if(GoViral.isSupported() && GoViral.goViral.isFacebookSupported() && !postedToFacebook)
				{
					postedToFacebook = true;
					
					if(!GoViral.goViral.isFacebookAuthenticated())
					{
						//Main.updateDebugText("not authenticated");
						
						GoViral.goViral.addEventListener(GVFacebookEvent.FB_LOGGED_IN,faceBookLoginSuccessful);
						GoViral.goViral.addEventListener(GVFacebookEvent.FB_LOGIN_FAILED, facebookLoginFailure);
						GoViral.goViral.authenticateWithFacebook("basic_info,publish_stream,user_photos");
					}
					else
					{
						//Main.updateDebugText("authenticated");
						
						var screenshot:BitmapData = takeScreenshot();
						GoViral.goViral.addEventListener(GVFacebookEvent.FB_REQUEST_RESPONSE, facebookPhotoPostSuccessful);
						GoViral.goViral.addEventListener(GVFacebookEvent.FB_REQUEST_FAILED, facebookPhotoPostFailure);
						GoViral.goViral.facebookPostPhoto("Results from Pathogen", screenshot);
					}
				}
			}
		}
		private function faceBookLoginSuccessful($e:GVFacebookEvent):void
		{
			//Main.updateDebugText("Facebook login success");
			
			GoViral.goViral.removeEventListener(GVFacebookEvent.FB_LOGGED_IN,faceBookLoginSuccessful);
			GoViral.goViral.removeEventListener(GVFacebookEvent.FB_LOGIN_FAILED, facebookLoginFailure);
			
			var screenshot:BitmapData = takeScreenshot();
			GoViral.goViral.facebookPostPhoto("Results from Pathogen", screenshot);
		}
		private function facebookLoginFailure($e:GVFacebookEvent):void
		{
			//Main.updateDebugText("Facebook login failure" + $e.errorMessage);
			
			GoViral.goViral.removeEventListener(GVFacebookEvent.FB_LOGGED_IN,faceBookLoginSuccessful);
			GoViral.goViral.removeEventListener(GVFacebookEvent.FB_LOGIN_FAILED, facebookLoginFailure);
		}
		private function facebookPhotoPostSuccessful($e:GVFacebookEvent):void
		{
			//Main.updateDebugText("Facebook Photo Post successful");
			
			GoViral.goViral.removeEventListener(GVFacebookEvent.FB_REQUEST_RESPONSE, facebookPhotoPostSuccessful);
			GoViral.goViral.removeEventListener(GVFacebookEvent.FB_REQUEST_FAILED, facebookPhotoPostFailure);
		}
		private function facebookPhotoPostFailure($e:GVFacebookEvent):void
		{
			//Main.updateDebugText("Facebook Photo Post failure" + $e.errorMessage);
			
			GoViral.goViral.removeEventListener(GVFacebookEvent.FB_REQUEST_RESPONSE, facebookPhotoPostSuccessful);
			GoViral.goViral.removeEventListener(GVFacebookEvent.FB_REQUEST_FAILED, facebookPhotoPostFailure);
		}
		private function onTouchTwitter($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(GoViral.isSupported() && GoViral.goViral.isTweetSheetAvailable())
				{
					var screenshot:BitmapData = takeScreenshot();
					GoViral.goViral.showTweetSheetWithImage("Pathogen Results!",screenshot);
				}
			}
		}
		private function takeScreenshot():BitmapData
		{
			var screenshot:BitmapData = new BitmapData(stage.stageWidth, stage.stageHeight);
			
			var support:RenderSupport = new RenderSupport();
			//override Starling's rendering engine : force to clean and render
			support.clear(stage.color, 1);
			support.setOrthographicProjection(0, 0, stage.stageWidth, stage.stageHeight);
			stage.render( support, 1.0 );
			support.finishQuadBatch();
			//force rendering now
			Starling.context.drawToBitmapData(screenshot);
			
			//Main.updateDebugText("took screenshot");
			
			return screenshot;
		}
		private function navigateToEndGamePage($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.BEGAN);
			if (touch)
			{
				arrayReset();
				gameOverview.toggleUp();
				graphs.toggleUp();
				endGameOverviewPage();
			}
		}
		private function navigateToGameOverview($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.BEGAN);
			if (touch)
			{
				arrayReset();
				endGameOverview.toggleUp();
				graphs.toggleUp();
				overallGameCounts();
			}
		}
		private function navigateToGraphsPage($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.BEGAN);
			if (touch)
			{
				if (pieChartImage)
				{
					if (!Main.isPhone())
					//if(PathogenTest.device != PathogenTest.iPHONE_4 && PathogenTest.device != PathogenTest.iPHONE_5)
						removeEventListener(Event.ENTER_FRAME, updatePieChartSlices);
					
					removeChild(pieChartImage);
					if (pieChartImage.texture)
						pieChartImage.texture.dispose();
					pieChartImage.dispose();
					pieChartImage = null;
				}
				
				arrayReset();
				gameOverview.toggleUp();
				endGameOverview.toggleUp();
				graphPage();
			}
		}
		private function endGameOverviewPage():void
		{	
			//Main.updateDebugText("endGameOverviewPage");
			
			var player:Player;
			
			percentHolder = new Sprite()
			addChild(percentHolder);
			
			var gap:Number = 1.3;
			
			//Images
			var dot:MovieClip = new MovieClip(Main.assets.getTextures("Grey_Stage01"));
			dot.pivotX = 26;
			dot.pivotY = 26;
			percentHolder.addChild(dot);
			dot.x = PathogenTest.stageCenter.x - (PathogenTest.wTenPercent * gap);
			dot.y = PathogenTest.hTenPercent * 4.0;
			
			var circle:MovieClip = new MovieClip(Main.assets.getTextures("Grey_Stage02"));
			circle.pivotX = 26;
			circle.pivotY = 26;
			percentHolder.addChild(circle);
			circle.x = dot.x + (PathogenTest.wTenPercent * gap);
			circle.y = dot.y;
			
			var square:MovieClip = new MovieClip(Main.assets.getTextures("Grey_Stage03"));
			square.pivotX = 26;
			square.pivotY = 26;
			percentHolder.addChild(square);
			square.x = circle.x + (PathogenTest.wTenPercent * gap);
			square.y = dot.y;
			
			var full:MovieClip = new MovieClip(Main.assets.getTextures("Grey_Stage04"));
			full.pivotX = 26;
			full.pivotY = 26;
			percentHolder.addChild(full);
			full.x = square.x + (PathogenTest.wTenPercent * gap);
			full.y = dot.y;
			
			//Main.updateDebugText("1");
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 32 / PathogenTest.scaleFactor;
			var textX:int = PathogenTest.HD_Multiplyer * 100 / PathogenTest.scaleFactor;
			var textY:int = PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor;
			var total:TextField = new TextField(textX, textY, "Total", "Dekar", fontSize, Color.WHITE);
			total.pivotX = total.width/2;
			total.pivotY = total.height/2;
			percentHolder.addChild(total);
			total.x = full.x + (PathogenTest.wTenPercent * gap);
			total.y = dot.y;
			
			fontSize = PathogenTest.HD_Multiplyer * 22 / PathogenTest.scaleFactor;
			textX = PathogenTest.HD_Multiplyer * 400 / PathogenTest.scaleFactor;
			textY = PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor;
			var descriptionText:TextField = new TextField(textX, textY, "Total number of each tile at end of game", "Dekar", fontSize, Color.WHITE);
			descriptionText.pivotX = descriptionText.width/2;
			descriptionText.pivotY = descriptionText.height/2;
			percentHolder.addChild(descriptionText);
			descriptionText.x = square.x;
			descriptionText.y = square.y - (PathogenTest.hTenPercent * .7);
			
			//Data
			for each (player in gameManager.playerArray)
			{
				arrayOfTriangleCounts.push(player.numOfTriangleTiles);
				arrayOfDiamondCounts.push(player.numOfDiamondTiles);
				arrayOfStarCounts.push(player.numOfStarTiles);
				arrayOfSolidCounts.push(player.numOfSolidTiles);
				arrayOfAllTileCounts.push(player.totalNumOfTiles);
			}
			
			highestTriangleCount = Math.max.apply(null, arrayOfTriangleCounts);
			highestDiamondCount = Math.max.apply(null, arrayOfDiamondCounts);
			highestStarCount = Math.max.apply(null, arrayOfStarCounts);
			highestSolidCount = Math.max.apply(null, arrayOfSolidCounts);
			highestOfAllTileCounts = Math.max.apply(null, arrayOfAllTileCounts);
			arrayOfHighestCounts = [highestTriangleCount, highestDiamondCount, highestStarCount, highestSolidCount, highestOfAllTileCounts];
			
			//Main.updateDebugText("2");
			
			var totalPercent:Number = 0;  // used to make sure overall percentage does not exceed 100%
			
			for (var i:int = 0; i < gameManager.playerArray.length; i++)
			{
				player = gameManager.playerArray[i];
				
				var tempPercent:Number = player.playerPercent;
				if (tempPercent > 100)
					gameOverviewChartPlayerPercentArray.push(tempPercent.toPrecision(3));
				else
				{
					var decimalPlaces:Number = Math.pow(10, 0);
					
					tempPercent = Math.round(decimalPlaces * player.playerPercent) / decimalPlaces;
					
					if ((totalPercent + tempPercent) <= 100)
						gameOverviewChartPlayerPercentArray.push(tempPercent);
					else
						gameOverviewChartPlayerPercentArray.push(tempPercent - 1);
				}
				
				//trace("player " + gameManager.playerArray[i].teamName + " added " + tempPercent);
				
				totalPercent += tempPercent;
				
				var startingPos:Point = new Point(dot.x, dot.y + PathogenTest.hTenPercent);
				var horizontalGap:Number = PathogenTest.wTenPercent * gap;
				var verticalGap:Number = PathogenTest.hTenPercent * .9;
				
				for (var j:int = 0; j < gameManager.playerArray[i].arrayOfTileCounts.length ; j++)
				{
					var newPercentBar:PercentBar = new PercentBar(player, player.arrayOfTileCounts[j], arrayOfHighestCounts[j]);
					newPercentBar.x = startingPos.x + (horizontalGap * j);
					newPercentBar.y = startingPos.y + (verticalGap * i);
					percentHolder.addChild(newPercentBar);
				}
			}
			
			if (playerPercentArray.length == 0)
			{
				for each (player in gameManager.playerArray)
				{
					playerPercentArray.push(player.playerPercent);
				}
			}
			
			//Main.updateDebugText("3");
			
			createPieChart2(playerPercentArray);
		}
		private function overallGameCounts():void
		{
			var player:Player;
			
			var gap:Number = 1.3;
			
			percentHolder = new Sprite()
			addChild(percentHolder);
			
			//Images
			var dot:MovieClip = new MovieClip(Main.assets.getTextures("Grey_Stage01"));
			dot.pivotX = 26;
			dot.pivotY = 26;
			percentHolder.addChild(dot);
			dot.x = PathogenTest.stageCenter.x - (PathogenTest.wTenPercent * gap);
			dot.y = PathogenTest.hTenPercent * 4.0;
			
			var circle:MovieClip = new MovieClip(Main.assets.getTextures("Grey_Stage02"));
			circle.pivotX = 26;
			circle.pivotY = 26;
			percentHolder.addChild(circle);
			circle.x = dot.x + (PathogenTest.wTenPercent * gap);
			circle.y = dot.y;
			
			var square:MovieClip = new MovieClip(Main.assets.getTextures("Grey_Stage03"));
			square.pivotX = 26;
			square.pivotY = 26;
			percentHolder.addChild(square);
			square.x = circle.x + (PathogenTest.wTenPercent * gap);
			square.y = dot.y;
			
			var full:MovieClip = new MovieClip(Main.assets.getTextures("Grey_Stage04"));
			full.pivotX = 26;
			full.pivotY = 26;
			percentHolder.addChild(full);
			full.x = square.x + (PathogenTest.wTenPercent * gap);
			full.y = dot.y;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 32 / PathogenTest.scaleFactor;
			var textX:int = PathogenTest.HD_Multiplyer * 100 / PathogenTest.scaleFactor;
			var textY:int = PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor;
			var total:TextField = new TextField(textX, textY, "Total", "Dekar", fontSize, Color.WHITE);
			total.pivotX = total.width/2;
			total.pivotY = total.height/2;
			percentHolder.addChild(total);
			total.x = full.x + (PathogenTest.wTenPercent * gap);
			total.y = dot.y;
			
			fontSize = PathogenTest.HD_Multiplyer * 22 / PathogenTest.scaleFactor;
			textX = PathogenTest.HD_Multiplyer * 600 / PathogenTest.scaleFactor;
			textY = PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor;
			var descriptionText:TextField = new TextField(textX, textY, "Total number of each tile created over the course of the game", "Dekar", fontSize, Color.WHITE);
			descriptionText.pivotX = descriptionText.width/2;
			descriptionText.pivotY = descriptionText.height/2;
			percentHolder.addChild(descriptionText);
			descriptionText.x = square.x;
			descriptionText.y = square.y - (PathogenTest.hTenPercent * .7)
			
			var totalTiles:int = 0;
			
			for (var i:int = 0; i<gameManager.playerArray.length; i++)
			{
				player = gameManager.playerArray[i];
				
				totalTiles += player.totalNumOfAllTilesAllGame;
				
				var startingPos:Point = new Point(dot.x, dot.y + PathogenTest.hTenPercent);
				var horizontalGap:Number = PathogenTest.wTenPercent * gap;
				var verticalGap:Number = PathogenTest.hTenPercent * .9;
				
				for (var j:int = 0; j<gameManager.playerArray[i].arrayOfTileCounts.length ; j++)
				{
					//trace("player " + i + " slot " + j + " = " + arrayOfHighestCounts[j]);
					var newPercentBar:PercentBar = new PercentBar(player, player.arrayofAllTotalTileCounts[j], overallArrayOfHighestCounts[j]);
					newPercentBar.x = startingPos.x + (horizontalGap * j);
					newPercentBar.y = startingPos.y + (verticalGap * i);
					percentHolder.addChild(newPercentBar);
				}
			}

			for each (player in gameManager.playerArray)
			{
				player.playerPercentOfAllTiles = (player.totalNumOfAllTilesAllGame / totalTiles) * 100;
				//trace(player.teamName + " has " + player.playerPercentOfAllTiles.toString() + "% of all cells played"); 
				playerPercentArrayOfAllTiles.push(player.playerPercentOfAllTiles);
			}
			
			/*
			// test data
			totalTiles = 911;
			gameManager.playerArray[0].playerPercentOfAllTiles = (260 / totalTiles) * 100;
			playerPercentArrayOfAllTiles.push(gameManager.playerArray[0].playerPercentOfAllTiles);
			gameManager.playerArray[1].playerPercentOfAllTiles = (297 / totalTiles) * 100;
			playerPercentArrayOfAllTiles.push(gameManager.playerArray[1].playerPercentOfAllTiles);
			gameManager.playerArray[2].playerPercentOfAllTiles = (197 / totalTiles) * 100;
			playerPercentArrayOfAllTiles.push(gameManager.playerArray[2].playerPercentOfAllTiles);
			gameManager.playerArray[3].playerPercentOfAllTiles = (157 / totalTiles) * 100;
			playerPercentArrayOfAllTiles.push(gameManager.playerArray[3].playerPercentOfAllTiles);
			*/
			
			//trace("total tiles = " + totalTiles);
			//trace("creating pie chart with " + playerPercentArrayOfAllTiles.length + " elements");
			
			createPieChart2(playerPercentArrayOfAllTiles);
			
		}
		private function graphPage():void
		{
			var player:Player;
			
			percentHolder = new Sprite()
			addChild(percentHolder);
			
			var graphBackgroundX:int = PathogenTest.wTenPercent * 8.5;
			if (graphBackgroundX > (0.97 * Main.TEXTURE_LIMIT))
				graphBackgroundX = 0.97 * Main.TEXTURE_LIMIT;
			var graphBackground:Quad = new Quad(graphBackgroundX, PathogenTest.hTenPercent * 4.5, 0x000033);
			graphBackground.pivotX = graphBackground.width/2;
			graphBackground.pivotY = graphBackground.height/2;
			percentHolder.addChild(graphBackground);
			graphBackground.x = PathogenTest.stageCenter.x;
			graphBackground.y = PathogenTest.hTenPercent * 5.5;
			graphBackground.alpha = .5;

			var graphWidth:Number = graphBackground.width;
			
			var highestNumArray:Array = [];
			
			for each (player in gameManager.playerArray)
			{
				highestNumArray = highestNumArray.concat(player.historyArrayOfTiles);
			}
			
			// draw lines
			var lines:Shape = new Shape();
			lines.graphics.clear();
			lines.graphics.lineStyle(2 * PathogenTest.HD_Multiplyer, 0xFFFFFF);
			//lines.graphics.lineStyle(5 * PathogenTest.HD_Multiplyer, 0xFFFFFF);
			lines.graphics.moveTo(0, 0);
			lines.graphics.lineTo(0, -graphBackground.height);
			lines.graphics.moveTo(0, 0);
			lines.graphics.lineTo(graphWidth, 0);
			
			var maxVal:int = Math.max.apply(null, highestNumArray);
			
			//Draw Chart
			var myChart:Shape = new Shape();
			myChart.graphics.clear();
			
			//myChart.y += 20;
			//lines.y -= 25;

			var intervals:int = gameManager.playerArray[0].historyArrayOfTiles.length;
			var pointInterval:Number;
			if (intervals >= 1)
				pointInterval = graphWidth / (intervals - 1);
			else
				pointInterval = graphWidth;
			
			var lineText:TextField;
			var textXPos:int = PathogenTest.stageCenter.x - graphBackground.width / 2;
			var textYPos:int = (PathogenTest.hTenPercent * 5.5) + graphBackground.height / 2 + (12.0 / PathogenTest.scaleFactor);
			var textSizeX:int;
			var fontSize:int = PathogenTest.HD_Multiplyer * 12 / PathogenTest.scaleFactor;
			var startTextSizeX:int = PathogenTest.HD_Multiplyer * 10 / PathogenTest.scaleFactor;
			var startTextSizeY:int = PathogenTest.HD_Multiplyer * 10 / PathogenTest.scaleFactor;
			var segmentPer:int;
			var maxTurns:int = 0;
			var i:int;
			for (i=0; i<gameManager.playerArray.length; i++)
			{
				//Setting Line Color
				myChart.graphics.lineStyle(2 * PathogenTest.HD_Multiplyer, gameManager.playerArray[i].playerHexColor);
				//myChart.graphics.lineStyle(5 * PathogenTest.HD_Multiplyer, gameManager.playerArray[i].playerHexColor);
				
				segmentPer = 1;
				if (gameManager.playerArray[i].historyArrayOfTiles.length > 50)
				{
					segmentPer = 5;
				}
				for (var n:int = 0; n < gameManager.playerArray[i].historyArrayOfTiles.length; n++)
				{
					//trace("Team:" + player.teamName + " has " + player.historyArrayOfTiles[n] + " tiles.");
					
					var thisHeight:Number = gameManager.playerArray[i].historyArrayOfTiles[n] * graphBackground.height / maxVal;
					
					if (i == 0)
					{
						// make turn count segments
						if (n%segmentPer == 0)
						{
							lines.graphics.moveTo(pointInterval * n, 0);
							lines.graphics.lineTo(pointInterval * n, 7.5);

							if (n <= 9)
								textSizeX = startTextSizeX;
							else if (n <= 99)
								textSizeX = startTextSizeX * 2;
							else
								textSizeX = startTextSizeX * 3;
							
							lineText = new TextField(textSizeX, startTextSizeY, n.toString(), "Questrial", fontSize, Color.WHITE);
							percentHolder.addChild(lineText);
							lineText.x = textXPos + (pointInterval * n) - (lineText.width / 2);
							lineText.y = textYPos;
						}
					}
					
					if (n == 0)
					{
						//trace("this height = " + thisHeight);
						myChart.graphics.moveTo(pointInterval * n, -thisHeight - i);
						//trace("move to (" + (pointInterval * n) + ", " + (0 - thisHeight) + ")");
					}
					else
					{
						myChart.graphics.lineTo(pointInterval * n, -thisHeight - i);
					}

					//trace("line to (" + (pointInterval * n) + ", " + (0 - thisHeight) + ")");
					if (n > maxTurns)
						maxTurns = n;
				}
				
				if (gameManager.playerArray[i].historyArrayOfTiles.length < (maxTurns + 1))
				{
					myChart.graphics.lineTo(pointInterval * n, -thisHeight - i);
				}
				
				myChart.graphics.endFill();
			}
			
			// make tile count segments
			if (gameManager.playerArray[0].historyArrayOfTiles.length > 1)
			{
				var yInterval:int = graphBackground.height / maxVal;
				var tileCount:int = 0;
				segmentPer = 5;
				if (maxVal > 50)
				{
					segmentPer = 25;
				}
				for (i=0; i<graphBackground.height; i+=yInterval)
				{
					if (tileCount%segmentPer == 0)
					{
						lines.graphics.moveTo(0, -i);
						lines.graphics.lineTo(-7.5, -i);
						
						if (tileCount <= 9)
							textSizeX = startTextSizeX;
						else if (tileCount <= 99)
							textSizeX = startTextSizeX * 2;
						else
							textSizeX = startTextSizeX * 3;
						
						lineText = new TextField(textSizeX, startTextSizeY, tileCount.toString(), "Questrial", fontSize, Color.WHITE);
						percentHolder.addChild(lineText);
						if (tileCount <= 9)
							lineText.x = textXPos - (lineText.width * 2);
						else if (tileCount <= 99)
							lineText.x = textXPos - (lineText.width * 1.25);
						else
							lineText.x = textXPos - lineText.width;
							
						lineText.y = textYPos - i - (lineText.height * 1.75);
					}
					
					tileCount++;
				}
			}
			
			lines.graphics.endFill();
			
			// initial cleanup
			if (linesbData)
			{
				linesbData.dispose();
				linesbData = null;
			}
			
			if (graphbData)
			{
				graphbData.dispose();
				graphbData = null;
			}
			
			if (linesChartImage)
			{
				removeChild(linesChartImage);
				if (linesChartImage.texture)
					linesChartImage.texture.dispose();
				linesChartImage.dispose();
				linesChartImage = null;
			}
			
			if (graphChartImage)
			{
				removeChild(graphChartImage);
				if (graphChartImage.texture)
					graphChartImage.texture.dispose();
				graphChartImage.dispose();
				graphChartImage = null;
			}
			
			linesbData = new BitmapData(lines.width + 1, lines.height +1, true, 0x000000);
			var rect_02:Rectangle = myChart.getRect(lines);
			var mtr_02:Matrix = new Matrix(1,0,0,1,-rect_02.x, -rect_02.y);
			linesbData.draw(lines, mtr_02);
			//var texture_02:Texture = Texture.fromBitmapData(linesbData, true, false, PathogenTest.scaleFactor);
			//var texture_02:Texture = Texture.fromBitmapData(linesbData, true, false, PathogenTest.scaleFactor);
			var texture_02:Texture = Texture.fromBitmapData(linesbData, true, false, 1.0);
			linesChartImage = new Image(texture_02);
			percentHolder.addChild(linesChartImage);
			linesChartImage.pivotY = linesChartImage.height;
			
			linesChartImage.x = PathogenTest.stageCenter.x - graphBackground.width / 2;
			linesChartImage.y = (PathogenTest.hTenPercent * 5.5) + graphBackground.height / 2 + PathogenTest.hTenPercent * .075;
			
			graphbData = new BitmapData(myChart.width + 1, myChart.height +1, true, 0x000000);
			var rect:Rectangle = myChart.getRect(myChart);
			var mtr:Matrix = new Matrix(1,0,0,1,-rect.x, -rect.y);
			graphbData.draw(myChart, mtr);
			//var texture:Texture = Texture.fromBitmapData(graphbData, true, false, PathogenTest.scaleFactor);
			var texture:Texture = Texture.fromBitmapData(graphbData, true, false, 1.0);
			graphChartImage = new Image(texture);
			percentHolder.addChild(graphChartImage);

			/*
			if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				graphChartImage.scaleX = PathogenTest.HD_Multiplyer * PathogenTest.scaleFactor;
				graphChartImage.scaleY = PathogenTest.HD_Multiplyer * PathogenTest.scaleFactor;
			}
			*/
			
			graphChartImage.pivotY = graphChartImage.height;
			
			graphChartImage.x = PathogenTest.stageCenter.x - graphBackground.width / 2;
			graphChartImage.y = (PathogenTest.hTenPercent * 5.5) + graphBackground.height / 2;
			
			// add variable definition text
			var variableText:TextField = new TextField(startTextSizeX * 4, startTextSizeY, "Turns", "Questrial", fontSize, Color.WHITE);
			percentHolder.addChild(variableText);
			variableText.x = PathogenTest.stageCenter.x - (variableText.width / 2);
			variableText.y = (PathogenTest.hTenPercent * 5.5) + graphBackground.height / 2 + (variableText.height * 2.5);
			
			variableText = new TextField(startTextSizeX * 6, startTextSizeY, "Cells", "Questrial", fontSize, Color.WHITE);
			percentHolder.addChild(variableText);
			variableText.pivotX = variableText.width / 2;
			variableText.pivotY = variableText.height / 2;
			variableText.rotation = deg2rad(-90);
			variableText.x = PathogenTest.stageCenter.x - graphBackground.width / 2 - (variableText.width * 3.0);
			variableText.y = PathogenTest.hTenPercent * 5.5;
		}
		private function createPieChart2(percentArray:Array):void
		{
			//Main.updateDebugText("createPieChart2");
			
			// create text number positions
			var topLeftPosition:Point = new Point(PathogenTest.wTenPercent * 1.1, PathogenTest.hTenPercent * 6.5);
			var topRightPosition:Point = new Point(topLeftPosition.x + PathogenTest.wTenPercent, topLeftPosition.y);
			var bottomLeftPosition:Point = new Point(topLeftPosition.x, topLeftPosition.y + PathogenTest.hTenPercent);
			var bottomRightPosition:Point = new Point(topRightPosition.x * 1,bottomLeftPosition.y);
			var percentTextLocationArray:Array = [topLeftPosition, topRightPosition, bottomLeftPosition, bottomRightPosition];
			var i:int;
			
			//trace("---------------------------------------------------------\ncreating chart with");
			//for (i=0; i<percentArray.length; i++)
				//trace("player " + i + " starting % = " + percentArray[i]);
			
			//Create Pie Chart
			var radius:int = 100 / PathogenTest.scaleFactor;
			var pieChart:PieChart = new PieChart();
			pieChart.radius = radius;
			pieChart.cacheAsBitmap = true;
			pieChart.rotation = 90;
			pieChart.autoBalance = false; //don't autobalance
			
			//pieChart.
			
			var totalPercent:Number = 0.0;
			
			if (!Main.isPhone())
			//if(PathogenTest.device != PathogenTest.iPHONE_4 && PathogenTest.device != PathogenTest.iPHONE_5)
			{
				_pieChartSliceMax = [];
				_pieChartSliceCurrent = [];
				_pieChartColors = [];
				_pieChartTextFields = [];
			}
			
			//Main.updateDebugText("a");
			
			//Add Slices to the pie chart
			for (i = 0; i < gameManager.playerArray.length; i++)
			{
				var currentPlayer:Player = gameManager.playerArray[i];
				
				var tempPercent:Number = percentArray[i];
				
				//trace("player " + i + " has " + tempPercent);
				
				if (isNaN(tempPercent))
					tempPercent = 0.0;
				
				if (tempPercent > 100)
				{
					tempPercent = 100.0;
					//playerPercentArray.push(tempPercent.toPrecision(3));
				}
				else
				{
					var decimalPlaces:Number = Math.pow(10, 0);
					tempPercent = Math.round(decimalPlaces * percentArray[i]) / decimalPlaces;
				}
				
				if (isNaN(tempPercent))
					tempPercent = 0.0;

				if ((totalPercent + tempPercent) <= 100)
				{
					if (Main.isPhone())
					//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
						pieChart.addSlice("", tempPercent, currentPlayer.playerHexColor);
					else
					{
						_pieChartSliceMax.push(tempPercent);
						_pieChartSliceCurrent.push(0);
						pieChart.addSlice("", 0, currentPlayer.playerHexColor);
						
						//trace("A slice " + i + " = " + tempPercent + " %");
					}
				}
				else
				{
					tempPercent = 100 - totalPercent;
					
					if (Main.isPhone())
						pieChart.addSlice("", tempPercent, currentPlayer.playerHexColor);
					else
					{
						_pieChartSliceMax.push(tempPercent);
						_pieChartSliceCurrent.push(0);
						pieChart.addSlice("", 0, currentPlayer.playerHexColor);
						
						//trace("B slice " + i + " = " + tempPercent + " %");
					}
					
					/*
					if (Main.isPhone())
					//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
						pieChart.addSlice("", tempPercent - 1, currentPlayer.playerHexColor);
					else
					{
						_pieChartSliceMax.push(tempPercent - 1);
						_pieChartSliceCurrent.push(0);
						pieChart.addSlice("", 0, currentPlayer.playerHexColor);
						
						trace("B slice " + i + " = " + tempPercent + " %");
					}
					*/
				}
				
				if (!Main.isPhone())
				//if(PathogenTest.device != PathogenTest.iPHONE_4 && PathogenTest.device != PathogenTest.iPHONE_5)
					_pieChartColors.push(currentPlayer.playerHexColor);
				
				totalPercent += tempPercent;
			}
			pieChart.setOuterBorder(1, 0x222245, 1); // sets the outer border style
			pieChart.moveChart();
			
			//trace("on creation, pie chart has " + pieChart.slices.length + " slices");

			if (totalPercent <= 100)
			{
				pieChart.draw();
				
				if (bData)
				{
					bData.dispose();
					bData = null;
				}
				
				if (pieChartImage)
				{
					if (!Main.isPhone())
					//if(PathogenTest.device != PathogenTest.iPHONE_4 && PathogenTest.device != PathogenTest.iPHONE_5)
						removeEventListener(Event.ENTER_FRAME, updatePieChartSlices);
					
					removeChild(pieChartImage);
					if (pieChartImage.texture)
						pieChartImage.texture.dispose();
					pieChartImage.dispose();
					pieChartImage = null;
				}
				
				bData = new BitmapData(pieChart.width + 1, pieChart.height + 1, true, 0x000000);
				var rect:Rectangle = pieChart.getRect(pieChart);
				var mtr:Matrix = new Matrix(1,0,0,1,-rect.x, -rect.y);
				bData.draw(pieChart, mtr);
				var texture:Texture = Texture.fromBitmapData(bData, true, false, PathogenTest.scaleFactor);
				pieChartImage = new Image(texture);
				pieChartImage.pivotX = pieChartImage.width / 2;
				pieChartImage.pivotY = pieChartImage.height / 2;
				addChild(pieChartImage);
				pieChartImage.rotation = deg2rad(-90);
				pieChartImage.scaleX = PathogenTest.HD_Multiplyer * 1 * PathogenTest.scaleFactor;
				pieChartImage.scaleY = PathogenTest.HD_Multiplyer * 1 * PathogenTest.scaleFactor;
				pieChartImage.x = PathogenTest.wTenPercent * 1.5;
				pieChartImage.y = PathogenTest.hTenPercent * 4.6;
				
				// create player percent text to display under pie chart
				for (i = 0; i < gameManager.playerArray.length; i++)
				{
					var checkedPercent:Number = percentArray[i];
					if (isNaN(checkedPercent))
						checkedPercent = 0.0;
					var percent:String = Math.round(checkedPercent) + "%";
					
					var fontSize:int = PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
					var endGamePercentText:TextField = new TextField(PathogenTest.stageWidth / 2, PathogenTest.hTenPercent, percent , "Questrial", fontSize, gameManager.playerArray[i].playerHexColor);
					endGamePercentText.pivotX = endGamePercentText.width / 2;
					endGamePercentText.pivotY = endGamePercentText.height / 2;
					percentHolder.addChild(endGamePercentText);
					endGamePercentText.x = percentTextLocationArray[i].x;
					endGamePercentText.y = percentTextLocationArray[i].y;
					
					if (!Main.isPhone())
						_pieChartTextFields.push(endGamePercentText);
				}
				
				if (!Main.isPhone())
					addEventListener(Event.ENTER_FRAME, updatePieChartSlices);
			}
			
			//Main.updateDebugText("b");
			
		}
		
		private function updatePieChartSlices(event:Event):void
		{
			// dispose of old pie chart bitmap data
			if (pieChartImage)
			{
				bData.dispose();
				bData = null;
				
				removeChild(pieChartImage);
				if (pieChartImage.texture)
					pieChartImage.texture.dispose();
				pieChartImage.dispose();
				pieChartImage = null;
			}
			
			var i:int;
			
			//Create Pie Chart
			var radius:int = 100 / PathogenTest.scaleFactor;
			var pieChart:PieChart = new PieChart();
			pieChart.radius = radius;
			pieChart.cacheAsBitmap = true;
			pieChart.rotation = 90;
			pieChart.autoBalance = false; //don't autobalance
			
			var allDone:Boolean = true;
			for (i=0; i<_pieChartSliceCurrent.length; i++)
			{
				if (_pieChartSliceCurrent[i] < _pieChartSliceMax[i])
				{
					_pieChartSliceCurrent[i] += 1;
					if (_pieChartSliceCurrent[i] < _pieChartSliceMax[i])
						allDone = false;
				}
			}
			
			var totalPercent:Number = 0.0;

			//trace("------------------------");
			
			//Add Slices to the pie chart
			for (i=0; i<_pieChartSliceCurrent.length; i++)
			{
				var tempPercent:Number = _pieChartSliceCurrent[i];
				
				if (isNaN(tempPercent))
					tempPercent = 0.0;
				
				if (tempPercent > 100)
				{
					tempPercent = 100.0;
					//playerPercentArray.push(tempPercent.toPrecision(3));
				}
				else
				{
					var decimalPlaces:Number = Math.pow(10, 0);
					tempPercent = Math.round(decimalPlaces * _pieChartSliceCurrent[i]) / decimalPlaces;
				}
				
				if (isNaN(tempPercent))
					tempPercent = 0.0;
				
				if ((totalPercent + tempPercent) <= 100)
				{
					//pieChart.addSlice("", _pieChartSliceCurrent[i], _pieChartColors[i]);
					pieChart.addSlice("", tempPercent, _pieChartColors[i]);
				}
				else
				{
					tempPercent = 100 - totalPercent;
					pieChart.addSlice("", tempPercent, _pieChartColors[i]);
					//pieChart.addSlice("", _pieChartSliceCurrent[i] - 1, _pieChartColors[i]);
				}
				
				_pieChartTextFields[i].text = _pieChartSliceCurrent[i].toString() + "%";
				
				//trace("slice " + i + " = " + tempPercent + " total = " + totalPercent);
				
				totalPercent += tempPercent;
			}
			pieChart.setOuterBorder(1, 0x222245, 1); // sets the outer border style
			pieChart.moveChart();

			if (totalPercent <= 100)
			{
				pieChart.draw();
				bData = new BitmapData(pieChart.width + 1, pieChart.height + 1, true, 0x000000);
				var rect:Rectangle = pieChart.getRect(pieChart);
				var mtr:Matrix = new Matrix(1,0,0,1,-rect.x, -rect.y);
				bData.draw(pieChart, mtr);
				var texture:Texture = Texture.fromBitmapData(bData, true, false, PathogenTest.scaleFactor);
				pieChartImage = new Image(texture);
				pieChartImage.pivotX = pieChartImage.width / 2;
				pieChartImage.pivotY = pieChartImage.height / 2;
				addChild(pieChartImage);
				pieChartImage.rotation = deg2rad(-90);
				pieChartImage.scaleX = PathogenTest.HD_Multiplyer * 1 * PathogenTest.scaleFactor;
				pieChartImage.scaleY = PathogenTest.HD_Multiplyer * 1 * PathogenTest.scaleFactor;
				pieChartImage.x = PathogenTest.wTenPercent * 1.5;
				pieChartImage.y = PathogenTest.hTenPercent * 4.1;
			}
			
			if (allDone)
				removeEventListener(Event.ENTER_FRAME, updatePieChartSlices);
		}
		
		private function arrayReset():void
		{
			// reset buttons
			if (percentHolder != null)
				removeChild(percentHolder);
			
			playerPercentArrayOfAllTiles = [];
			playerPercentArray = [];
			arrayOfTriangleCounts = [];
			arrayOfDiamondCounts = [];
			arrayOfStarCounts = [];
			arrayOfSolidCounts = [];
			arrayOfAllTileCounts = [];
			
			if (pieChartImage)
			{
				removeChild(pieChartImage);
				if (pieChartImage.texture)
					pieChartImage.texture.dispose();
				pieChartImage.dispose();
				pieChartImage = null;
			}
		}
		public function garbageCollection():void
		{
			viewReplay.removeEventListener(TouchEvent.TOUCH, onTouchReplay)
			viewReplay.dispose();
			viewReplay.destroy();
			removeChild(viewReplay);
			viewReplay = null;
			
			leaveGame.removeEventListener(TouchEvent.TOUCH, onTouchLeaveGame);
			leaveGame.dispose();
			leaveGame.destroy();
			removeChild(leaveGame);
			leaveGame = null;
			
			viewMap.removeEventListener(TouchEvent.TOUCH, gameManager.viewMap);
			viewMap.dispose();
			viewMap.destroy();
			removeChild(viewMap);
			viewMap = null;
			
			endGameOverview.removeEventListener(TouchEvent.TOUCH, navigateToEndGamePage);
			endGameOverview.dispose();
			endGameOverview.destroy();
			removeChild(endGameOverview);
			endGameOverview = null;
			
			gameOverview.removeEventListener(TouchEvent.TOUCH, navigateToGameOverview);
			gameOverview.dispose();
			gameOverview.destroy();
			removeChild(gameOverview);
			gameOverview = null;
			
			graphs.removeEventListener(TouchEvent.TOUCH, navigateToGraphsPage);
			graphs.dispose();
			graphs.destroy();
			removeChild(graphs);
			graphs = null;
			
			if(twitterButton)
			{
				twitterButton.removeEventListener(TouchEvent.TOUCH, onTouchTwitter);
				twitterButton.dispose();
				twitterButton.destroy();
				removeChild(twitterButton);
				twitterButton = null;
			}
			
			if(facebookButton)
			{
				facebookButton.removeEventListener(TouchEvent.TOUCH, onTouchFacebook);
				facebookButton.dispose();
				facebookButton.destroy();
				removeChild(facebookButton);
				facebookButton = null;
			}
			
			if (pieChartImage)
			{
				if (!Main.isPhone())
				//if(PathogenTest.device != PathogenTest.iPHONE_4 && PathogenTest.device != PathogenTest.iPHONE_5)
					removeEventListener(Event.ENTER_FRAME, updatePieChartSlices);
				
				removeChild(pieChartImage);
				if (pieChartImage.texture)
					pieChartImage.texture.dispose();
				pieChartImage.dispose();
				pieChartImage = null;
			}
			
			if (bData)
			{
				bData.dispose();
				bData = null;
			}
			
			if (linesbData)
			{
				linesbData.dispose();
				linesbData = null;
			}
			
			if (graphbData)
			{
				graphbData.dispose();
				graphbData = null;
			}
			
			if (linesChartImage)
			{
				removeChild(linesChartImage);
				if (linesChartImage.texture)
					linesChartImage.texture.dispose();
				linesChartImage.dispose();
				linesChartImage = null;
			}
			
			if (graphChartImage)
			{
				removeChild(graphChartImage);
				if (graphChartImage.texture)
					graphChartImage.texture.dispose();
				graphChartImage.dispose();
				graphChartImage = null;
			}
			
			for (var i:int = 0; i < percentHolder.numChildren; i++)
			{
				percentHolder.removeChildAt(i);
			}
			
			percentHolder.dispose();
			removeChild(percentHolder);
			percentHolder = null;
			
			gameManager = null;
		}
	}
}