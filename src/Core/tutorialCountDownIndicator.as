package Core
{
	import UI.Main;
	
	import Utils.BW_Selector;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.deg2rad;
	import starling.core.Starling;
	
	public class tutorialCountDownIndicator extends Sprite
	{
		//Static colors
		private static const GREEN:int = 0x419685;
		
		private static const OFF:Number = .5;
		
		//Countdown Vars
		private var owner:Player;
		private var cellSelector:CellSelector
		private var numOfPieces:int;
		
		private var radians:Number;
		private var gap:Number;
		
		//Arrays
		private var pieceArray:Array = [];
		private var whiteFlashArray:Array = [];
		
		public function tutorialCountDownIndicator()
		{
			super();
			
			//Create all pieces
			for (var i:int = 0; i < 8; i++)
			{
				var piece:Image = new Image(Main.assets.getTexture("PieceOfEightWhite"));
				var whiteFlashDisplay:Image = new Image(Main.assets.getTexture("PieceOfEightWhite"));
				
				piece.color = GREEN;
				
				//Positioning
				piece.pivotX = piece.width/2;
				piece.pivotY = piece.height/2;
				
				var whiteFlash:BW_Selector = new BW_Selector(0,0,whiteFlashDisplay, true, .95, 1, 1);
				
				//Add to screen
				addChild(piece);
				addChild(whiteFlash);
				
				piece.alpha = OFF;
				
				//Add to corrisponding arrays
				pieceArray.push(piece);
				whiteFlashArray.push(whiteFlash);
				
				//Place the pieces in a circle	
				gap = piece.width * 1.5;
						
				switch(i)
				{
					case 0:
					{
						piece.x = gap *.325;
						piece.y = -gap * .8;
						radians = deg2rad(0);
						piece.rotation = radians;
						break;
					}
					case 1:
					{
						piece.x = gap * .8;
						piece.y = -gap * .35;
						radians = deg2rad(45);
						piece.rotation = radians;
						break;
					}
					case 2:
					{
						piece.x = gap * .8;
						piece.y = gap * .35;
						radians = deg2rad(90);
						piece.rotation = radians;
						break;
					}
					case 3:
					{
						piece.x = gap *.325;
						piece.y = gap * .8;
						radians = deg2rad(135);
						piece.rotation = radians;
						break;
					}
					case 4:
					{
						piece.x = -gap * .325;
						piece.y = gap * .8;
						radians = deg2rad(180);
						piece.rotation = radians;
						break;
					}
					case 5:
					{
						piece.x = -gap * .8;
						piece.y = gap * .35;
						radians = deg2rad(225);
						piece.rotation = radians;
						break;
					}
					case 6:
					{
						piece.x = -gap * .8;
						piece.y = -gap * .35;
						radians = deg2rad(275);
						piece.rotation = radians;
						break;
					}
					case 7:
					{
						piece.x = -gap * .325;
						piece.y = -gap * .8;
						radians = deg2rad(315);
						piece.rotation = radians;
						break;
					}
				}
				
				whiteFlash.x = piece.x
				whiteFlash.y = piece.y;
				whiteFlash.rotation = radians;
			}
			
			var symbol:Image = new Image(Main.assets.getTexture("SquareSymbol"));
			
			//Create symbol
			symbol.pivotX = symbol.width/2;
			symbol.pivotY = symbol.height/2;
			addChild(symbol);
			
			//Turn everything off
			for each (piece in pieceArray)
			{
				piece.alpha = OFF;
			}
			
			var delayAmount:Number = 0;
			var delayincrement:Number = .4;
			
			startWhiteFlash();
		}
		public function startWhiteFlash():void
		{
			var piece:Image;
			var whiteFlash:BW_Selector;
			
			var delayAmount:Number = 0;
			var delayincrement:Number = .4;
			
			//Turn on the correct indicators
			for (var i:int = 0; i < 6; i++)
			{
				piece = pieceArray[i];
				piece.alpha = 1;
				
				whiteFlash = whiteFlashArray[i];
				
				Starling.juggler.delayCall(whiteFlash.startFlash, delayAmount);
				
				delayAmount += delayincrement;
			}
		}
	}
}