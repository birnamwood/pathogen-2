package Core
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import com.sticksports.nativeExtensions.flurry.Flurry;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Quad;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class DefeatScreen extends BW_UI
	{
		private var _gameManager:GameManager;
		private var viewStats:BW_Button;
		private var retryBt:BW_Button;
		private var BacktoCampaign:BW_Button;
		
		public function DefeatScreen(gameManager:GameManager)
		{
			super();
			
			_gameManager = gameManager;
			
			//Creating Background
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			addChild(transBackground);
			transBackground.alpha = .75;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			var texture:Texture = Main.assets.getTexture("scale9");
			var textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 3.5;
			background.height = PathogenTest.hTenPercent * 4.75;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			
			//Creating Header & Title
			var header:Scale9Image = new Scale9Image(textures);
			addChild(header);
			header.width = background.width;
			header.height = PathogenTest.hTenPercent;
			header.pivotX = header.width/2;
			header.y = -background.height/2;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
			var title:TextField = new TextField(header.width, header.height, "Defeat", "Questrial", fontSize, Color.WHITE);
			title.pivotX = title.width/2;
			addChild(title);
			title.y = header.y;
			
			//Creating Buttons
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				viewStats = new BW_Button(2, 1.0, "View Stats", 32);
			else
				viewStats = new BW_Button(2, 0.75, "View Stats", 32);
			addChild(viewStats);
			viewStats.y = title.y + PathogenTest.hTenPercent * 1.8;
			viewStats.addEventListener(TouchEvent.TOUCH, onTouchViewStats);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				retryBt = new BW_Button(2, 1.0, "Retry", 32);
			else
				retryBt = new BW_Button(2, 0.75, "Retry", 32);
			addChild(retryBt);
			retryBt.y = viewStats.y + PathogenTest.hTenPercent * 1;
			retryBt.addEventListener(TouchEvent.TOUCH, onTouchRetry);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				BacktoCampaign = new BW_Button(2, 1.0, "Campaign", 32);
			else
				BacktoCampaign = new BW_Button(2, 0.75, "Campaign", 32);
			addChild(BacktoCampaign);
			BacktoCampaign.y = retryBt.y + PathogenTest.hTenPercent * 1;
			BacktoCampaign.addEventListener(TouchEvent.TOUCH, onTouchBackToCampaign);
			
			Main.sSoundController.playLossSound();
		}
		private function onTouchViewStats($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_gameManager.addPostGameScreen();
				startFadeOut();
			}
		}
		private function onTouchRetry($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_gameManager.retryGame();
				startFadeOut();
			}
		}
		private function onTouchBackToCampaign($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_gameManager.Quit();
			}
		}
		public override function death():void
		{
			if(viewStats)
			{
				viewStats.removeEventListener(TouchEvent.TOUCH, onTouchViewStats);
				viewStats.destroy();
				viewStats.dispose();
				removeChild(viewStats);
				viewStats = null;
			}
			
			if(retryBt)
			{
				retryBt.removeEventListener(TouchEvent.TOUCH, onTouchRetry);
				retryBt.destroy();
				retryBt.dispose();
				removeChild(retryBt);
				retryBt = null;
			}
			
			if(BacktoCampaign)
			{
				BacktoCampaign.removeEventListener(TouchEvent.TOUCH, onTouchBackToCampaign);
				BacktoCampaign.destroy();
				BacktoCampaign.dispose();
				removeChild(BacktoCampaign);
				BacktoCampaign = null;
			}
			
			_gameManager.removeDefeatScreen();
		}
	}
}