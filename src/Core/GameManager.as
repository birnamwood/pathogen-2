package Core
{
	import ArtificialIntelligence.AI;
	
	import Campaign.Campaign;
	import Campaign.Tutorial;
	
	import Sound.SoundController;
	
	import UI.AchievementPopUp;
	import UI.Achievements;
	import UI.Main;
	import UI.fSprite;
	
	import Utils.BW_Button;
	import Utils.BW_Selector;
	import Utils.BW_SocialButton;
	import Utils.BW_UI;
	import Utils.GeneralFunctions;
	
	import com.sticksports.nativeExtensions.flurry.Flurry;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.display.Scale9Image;
	import feathers.events.FeathersEventType;
	import feathers.textures.Scale3Textures;
	import feathers.textures.Scale9Textures;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	
	import flashx.textLayout.formats.ITabStopFormat;
	import flashx.textLayout.tlf_internal;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.deg2rad;
	
	public class GameManager extends BW_UI
	{
		// local constants
		private const ZONE_SCREEN_REMOVE_TIME:Number = 4.0;
		private const OTHER_SELECTORS_ALPHA:Number = 0.8;
		private const SELECTOR_FADE_TIME:Number = 2.0;
		private const UPDATE_BAR_SPEED:Number = 2.5;
		private const TUTORIAL_UNLOCK_MOVE_TIME:Number = 2.0;
		
		//Classes
		private var main:Main;
		private var _campaign:Campaign;
		private var postGame:PostGame;
		private var _tipWindow:TipWindow;
		
		//Map Data
		private var tileHolder:Sprite;
		private var _virtualTileHolder:Sprite;
		private var _mapSize:int;
		public var mapName:String;
		private var gameBoardHeight:int;
		private var gameBoardWidth:int;
		private var gameBoard:Array = [];
		private var virtualGameBoard:Array = [];
		private var totalNumOfPlayableTiles:int = 0;
		
		private var currentCellTypeSelected:CellSelector;
		
		//Player Data
		public var playerArray:Array = [];
		public var playerLocations:Array;
		public var playerColors:Array;
		private var playerAI:Array;
		private var numOfPlayers:int;
		
		private var currentPlayerNum:int;
		private var currentPlayer:Player;
		
		//Gameplay Vars
		public var moveUnderway:Boolean;
		private var roundCounter:int = 0;
		public var minuteCounter:int;
		private var CurrentFrameCount:int;
		private var secondCounter:int;
		
		//Imported Game Data from Editor
		private var mapData:Array;
		
		//History
		private var turnList:Array = [];
		private var turn:TurnRecord;
		private var playLog:Array = [];
		private var playLogOwnership:Array = [];
		
		//Modes
		private var modes:Array;
		private var erosionZoneMode:Boolean;
		private var captureZoneMode:Boolean;
		private var movingCaptureZone:Boolean;
		private var tileSelector:BW_Selector;
		private var tutorial_01:Boolean;
		private var tutorial_02:Boolean;
		private var tutorial_03:Boolean;
		private var tutorial_04:Boolean;
		private var tutorial_05:Boolean;
		private var tutorial_CaptureZone:Boolean;
		private var tutorial_InfectionMode:Boolean;
		private var tutorial_ErosionMode:Boolean;
		private var lightning:Boolean;
		private var timer:Boolean;
		private var infection:Boolean;
		
		//Mode Vars
		private var numOfLightningRounds:int = 10;
		
		//CaptureZone
		private var fullCaptureZoneList:Vector.<CaptureZone> = new Vector.<CaptureZone>;
		private var currentZoneNumber:int = 1;
		private var captureZoneList:Array = [];
		private var numOfActiveControlZones:int;
		private var captureZoneScreen:CaptureZoneScreen;
		private var claimNum:int = 10;
		private var currentClaimCount:int = 0;
		private var captureZoneIndicator:Sprite;
		private var claimText:TextField;
		private var _claimTextExtension:TextField;
		
		//erosionZoneMode Zone
		private var fullErosionZoneList:Vector.<ErosionZone> = new Vector.<ErosionZone>;
		private var erosionZoneList:Array = [];
		private var currentErosionZoneNumber:int = 1;
		private var currentlyActiveErosionZone:ErosionZone;
		
		//Clock
		private var clock:TextField;
		private var timerThreshold:int = 20;
		private var timerCounter:int = 0;
		
		//Lightning
		private var lightningCounter:TextField;
		private var lightningIndicator:Sprite;
		
		//Gameplay Options
		private var showUndo:Boolean;
		
		//Buttons
		private var quitButton:BW_Button;
		private var rulesButton:BW_Button;
		private var optionsButton:BW_Button;
		private var _undoButton:BW_Button;
		
		//Save
		private var sharedData:SharedObject
		
		//Quit Menu
		private var quitScreen:QuitScreen;
		
		//Options Menu
		private var gameOptions:GameOptions;
		private var UndoCheckbox:BW_Button;
		private var percentCheckbox:BW_Button;
		private var practiceCheckbox:BW_Button;
		private var optionsQuit:BW_Button;
		
		//PercentBar
		private var _percentBar:Sprite;
		private var percentFrameWidth:Number;
		private var percentBarRate:Number;
		
		//End Game
		private var forfiet:Boolean;
		private var _replay:Boolean = false;
		
		// undo
		private var undoing:Boolean = false;
		private var hasPressedUndo:Boolean;
		
		// _replay
		private var currentTurn:int = 0;
		private var playPauseButton:BW_Button;
		private var stepForwardButton:BW_Button;
		private var stepBackwardButton:BW_Button;
		private var replayPause:Boolean;
		private var playReplay:Boolean = false;
		
		// game mode stuff
		private var skipped:Boolean = false;
		
		//Campaign
		public var _retry:Boolean = false;
		private var _victoryScreen:VictoryScreen;
		private var viewStats:BW_Button;
		private var nextLevel:BW_Button;
		private var _defeatScreen:DefeatScreen;
		
		private var defeat:Boolean;
		public var success:Boolean;
		
		// AI
		private var ai:AI;
		private var aiOn:Boolean = true;
		private var aiCellLocation:int;
		
		private var EndOfGame:Boolean;  // set when game is over, but still in the game
		private var _domination:Boolean;
		
		// tutorial
		private var tutorial:Tutorial;
		private var tutorialFlashAllowed:Boolean = false;
		private const TUTORIAL_FLASH_AMT:Number = 0.1;
		private var tutorialScreenDir:int;
		private var tutorialScreenAlpha:Number;
		private var tutorialScreenFlashCount:int;
		private var lastClickedCell:Cell;
		private const ARROW_MOVEMENT_MAX:Number = 7.5;
		private const ARROW_MOVEMENT_AMT:Number = 0.5;
		private var arrow:MovieClip;
		private var arrow2:MovieClip;
		private var arrowDir:int;
		private var arrow2Dir:int;
		private var arrowMovement:Number;
		private var arrow2Movement:Number;
		private var arrowStartX:Number;
		private var arrow2StartX:Number;
		private var arrowStartY:Number;
		private var undidTurnInCampaign:Boolean = false;
		
		private var inGameTutorial:InGameTutorial;
		private var undoWhiteFlash:BW_Selector;
		private var greenStage2Unlock:Image;
		private var greenStage3Unlock:Image;
		private var greenStage4Unlock:Image;
		private var transparentBackground:Quad;
		private var playerCell:Cell;
		
		private var _hasPlayedPathogenBefore:Boolean;
		private var _playerClickedTheTutorialButton:Boolean;
		
		// victory screen
		private var victoryCell:Sprite;
		private var victoryCellFrameCount:int;
		
		//Scale Test
		private var sliding:Boolean;
		private var targetCenter:Quad;
		
		private var previousObjectCenter:Point;
		private var currentObjectCenter:Point
		
		private var previousTargetCenter:Point;
		private var currentTargetCenter:Point;
		
		//Dragging
		private var horizontalBar:Quad;
		private var verticalBar:Quad;
		private var closestCell:Cell;
		private var confirmButton:BW_Button;
		public var numOfCellsAffected:int = 0;
		
		//Title
		private var levelTitle:LevelTitle;
		
		// zooming
		private var _holderInitialPosition:Point;
		private var _holderInitialScaleX:Number;
		private var _holderInitialScaleY:Number;
		private var _lastHolderScaleX:Number;
		private var _topBoundary:Number;
		private var _bottomBoundary:Number;
		private var _leftBoundary:Number;
		private var _rightBoundary:Number;
		private var _leftQuad:Quad;
		private var _rightQuad:Quad;
		private var _topQuad:Quad;
		private var _bottomQuad:Quad;
		private var _holderCenterQuad:Quad;
		private var _startLeftBoundary:Number;
		private var _startRightBoundary:Number;
		private var _startTopBoundary:Number;
		private var _startBottomBoundary:Number;
		private var _startLeftQuad:Quad;
		private var _startRightQuad:Quad;
		private var _startTopQuad:Quad;
		private var _startBottomQuad:Quad;
		private var _zoomPanDelay:int;
		private const ZOOM_PAN_DELAY_TIME:int = 15;
		private var _zoomButton:BW_SocialButton;
		
		private var _moveHasBeenTaken:Boolean;
		
		private var boardOutline:BoardOutline;
		
		private var _winButton:BW_Button;
		
		private var _debug:Boolean = false;
		
		public function GameManager(main:Main, _campaign:Campaign, playerColors:Array, playerLocations:Array, playerAI:Array, mapData:Array, mapName:String, _mapSize:int, modes:Array)
		{
			super();
			
			//Main.debugText.text = "Start Game Manager";
			
			//Imported Game Vars
			this.main = main;
			this._campaign = _campaign;
			
			this.playerColors = playerColors;
			this.playerLocations = playerLocations;
			this.playerAI = playerAI;
			this.mapData = mapData;
			this.mapName = mapName;
			this._mapSize = _mapSize;
			this.modes = modes;
			
			//Save
			sharedData = Main.saveDataObject;
			
			//Determine if any Modes are present.
			for each (var mode:String in modes)
			{
				if (mode == "Tutorial_01")
					tutorial_01 = true;
				if (mode == "Tutorial_02")
					tutorial_02 = true;
				if (mode == "Tutorial_03")
					tutorial_03 = true;
				if (mode == "Tutorial_04")
					tutorial_04 = true;
				if (mode == "Tutorial_05")
					tutorial_05 = true;
				
				if (mode == "CaptureZoneTutorial")
					tutorial_CaptureZone = true;
				if (mode == "ErosionZoneTutorial")
					tutorial_ErosionMode = true;
				if (mode == "InfectionTutorial")
					tutorial_InfectionMode = true;
				
				if (mode == "MovingCaptureZone")
					movingCaptureZone = true;
				if (mode == "ErosionZone")
					erosionZoneMode = true;
				if (mode == "Infection")
					infection = true;
				if (mode == "Lightning")
					lightning = true;
				
			}
			
			//Main.updateDebugText("New Game, Campaign: " + campaign);
			
			if(campaign)
			{
				//Main.updateDebugText("Current Level Num: " + campaign.currentCampaignLevel.levelNumber);
				
				switch(campaign.currentCampaignLevel.levelNumber)
				{
					case 0:
						Flurry.startTimedEvent("Getting Started", { difficulty: campaign.difficultySelector.difficulty });
						break;
					case 1:
						Flurry.startTimedEvent("Evolution", { difficulty: campaign.difficultySelector.difficulty });
						break;
					case 2:
						Flurry.startTimedEvent("Locked Down", { difficulty: campaign.difficultySelector.difficulty });
						break;
					case 3:
						Flurry.startTimedEvent("Conquer", { difficulty: campaign.difficultySelector.difficulty });
						break;
					case 4:
						Flurry.startTimedEvent("Nuclear Option", { difficulty: campaign.difficultySelector.difficulty });
						break;
					case 5:
						{
							//Main.updateDebugText("Current Sub Level Num: " + campaign.currentCampaignLevel.chosenSubLevel.levelNumber);
						
							switch(campaign.currentCampaignLevel.chosenSubLevel.levelNumber)
							{
								case 0:
									Flurry.startTimedEvent("Outmanned_01", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 1:
									Flurry.startTimedEvent("Outmanned_02", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 2:
									Flurry.startTimedEvent("Outmanned_03", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 3:
									Flurry.startTimedEvent("Outmanned_04", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 4:
									Flurry.startTimedEvent("Outmanned_05", { difficulty: campaign.difficultySelector.difficulty });
									break;
							}
						}
						break;
					case 6:
						{
							switch(campaign.currentCampaignLevel.chosenSubLevel.levelNumber)
							{
								case 0:
									Flurry.startTimedEvent("CaptureZone_01", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 1:
									Flurry.startTimedEvent("CaptureZone_02", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 2:
									Flurry.startTimedEvent("CaptureZone_03", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 3:
									Flurry.startTimedEvent("CaptureZone_04", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 4:
									Flurry.startTimedEvent("CaptureZone_05", { difficulty: campaign.difficultySelector.difficulty });
									break;
							}
						}
						break;
					case 7:
						{
							switch(campaign.currentCampaignLevel.chosenSubLevel.levelNumber)
							{
								case 0:
									Flurry.startTimedEvent("Infection_01", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 1:
									Flurry.startTimedEvent("Infection_02", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 2:
									Flurry.startTimedEvent("Infection_03", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 3:
									Flurry.startTimedEvent("Infection_04", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 4:
									Flurry.startTimedEvent("Infection_05", { difficulty: campaign.difficultySelector.difficulty });
									break;
							}
						}
						break;
					case 8:
						{
							switch(campaign.currentCampaignLevel.chosenSubLevel.levelNumber)
							{
								case 0:
									Flurry.startTimedEvent("ErosionZone_01", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 1:
									Flurry.startTimedEvent("ErosionZone_02", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 2:
									Flurry.startTimedEvent("ErosionZone_03", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 3:
									Flurry.startTimedEvent("ErosionZone_04", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 4:
									Flurry.startTimedEvent("ErosionZone_05", { difficulty: campaign.difficultySelector.difficulty });
									break;
							}
						}
						break;
					case 9:
						{
							switch(campaign.currentCampaignLevel.chosenSubLevel.levelNumber)
							{
								case 0:
									Flurry.startTimedEvent("Underdog_01", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 1:
									Flurry.startTimedEvent("Underdog_02", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 2:
									Flurry.startTimedEvent("Underdog_03", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 3:
									Flurry.startTimedEvent("Underdog_04", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 4:
									Flurry.startTimedEvent("Underdog_05", { difficulty: campaign.difficultySelector.difficulty });
									break;
							}
						}
						break;
					case 10:
						{
							switch(campaign.currentCampaignLevel.chosenSubLevel.levelNumber)
							{
								case 0:
									Flurry.startTimedEvent("Lightning_01", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 1:
									Flurry.startTimedEvent("Lightning_02", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 2:
									Flurry.startTimedEvent("Lightning_03", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 3:
									Flurry.startTimedEvent("Lightning_04", { difficulty: campaign.difficultySelector.difficulty });
									break;
								case 4:
									Flurry.startTimedEvent("Lightning_05", { difficulty: campaign.difficultySelector.difficulty });
									break;
							}
						}
						break;
					case 11:
						Flurry.startTimedEvent("Final", { difficulty: campaign.difficultySelector.difficulty });
						break;
				}
			}
			
			mapSetup();
			
			//Main.updateDebugText("Map Setup Complete");
			
			Main.sSoundController.transitionToInGameMusic();
			
			//Main.updateDebugText("transitionToInGameMusic Complete");
			
			// set up the AI
			if (aiOn)
			{
				ai = new AI(this, gameBoard, numOfPlayers);
				ai.copyBoard(currentPlayer);
				
				if(currentPlayer.getAiControl())
					tileSelector.visible = false;
			}
			
			//Setting up initial vars.
			_moveHasBeenTaken = false;
			percentBarRate = UPDATE_BAR_SPEED / PathogenTest.scaleFactor;
			percentBarRate *= PathogenTest.HD_Multiplyer;
			victoryCell = null;
			victoryCellFrameCount = 0;
			
			_zoomPanDelay = ZOOM_PAN_DELAY_TIME;
			
			if (_debug)
				Starling.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKey);
			
			//Main.updateDebugText("GameManager Alpha: " + alpha);
		}
		private function onTouchWin($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				var playerSave:Player;
				var openCellCounter:int;
				var i:int;
				
				playerSave = currentPlayer;
				openCellCounter = 0;
				currentPlayer = playerArray[0];
				
				for (i=0; i<gameBoard.length; i++)
				{
					if (gameBoard[i].state == 0)
					{
						if (openCellCounter > 0)
						{
							gameBoard[i].state = 1;
							gameBoard[i].owner = currentPlayer;
							gameBoard[i].reset();
						}
						openCellCounter++;
					}
				}
				currentPlayer = playerSave;
			}
		}
		private function onKey(event:KeyboardEvent):void
		{
			var playerSave:Player;
			var openCellCounter:int;
			var i:int;
			
			if (event.keyCode == 65)
			{
				playerSave = currentPlayer;
				openCellCounter = 0;
				currentPlayer = playerArray[0];

				for (i=0; i<gameBoard.length; i++)
				{
					if (gameBoard[i].state == 0)
					{
						if (openCellCounter > 0)
						{
							gameBoard[i].state = 1;
							gameBoard[i].owner = currentPlayer;
							gameBoard[i].reset();
						}
						openCellCounter++;
					}
				}
				currentPlayer = playerSave;
			}
			else if ((event.keyCode == 66) && (playerArray.length > 1))
			{
				playerSave = currentPlayer;
				openCellCounter = 0;
				currentPlayer = playerArray[1];
				
				for (i=0; i<gameBoard.length; i++)
				{
					if (gameBoard[i].state == 0)
					{
						if (openCellCounter > 0)
						{
							gameBoard[i].state = 1;
							gameBoard[i].owner = currentPlayer;
							gameBoard[i].reset();
						}
						openCellCounter++;
					}
				}
				currentPlayer = playerSave;
			}
			else if ((event.keyCode == 67) && (playerArray.length > 2))
			{
				playerSave = currentPlayer;
				openCellCounter = 0;
				currentPlayer = playerArray[2];
				
				for (i=0; i<gameBoard.length; i++)
				{
					if (gameBoard[i].state == 0)
					{
						if (openCellCounter > 0)
						{
							gameBoard[i].state = 1;
							gameBoard[i].owner = currentPlayer;
							gameBoard[i].reset();
						}
						openCellCounter++;
					}
				}
				currentPlayer = playerSave;
			}
			else if ((event.keyCode == 68) && (playerArray.length > 3))
			{
				playerSave = currentPlayer;
				openCellCounter = 0;
				currentPlayer = playerArray[3];
				
				for (i=0; i<gameBoard.length; i++)
				{
					if (gameBoard[i].state == 0)
					{
						if (openCellCounter > 0)
						{
							gameBoard[i].state = 1;
							gameBoard[i].owner = currentPlayer;
							gameBoard[i].reset();
						}
						openCellCounter++;
					}
				}
				currentPlayer = playerSave;
			}
		}
		private function arrowBounce($e:Event):void
		{
			//Functionality for pointer arrow in Tutorials.
			if (arrow != null)
			{
				if (arrow.visible)
				{
					arrowMovement += ARROW_MOVEMENT_AMT * arrowDir;
					if (Math.abs(arrowMovement) > ARROW_MOVEMENT_MAX)
					{
						if (arrowDir == 1)
							arrowDir = -1;
						else if (arrowDir == -1)
							arrowDir = 1;
						
						if(arrowDir == 2)
							arrowDir = -2;
						else if (arrowDir == -2)
							arrowDir = 2;
					}
					
					if(arrowDir == 1 || arrowDir == -1)
						arrow.x = arrowStartX + arrowMovement;
					if(arrowDir == 2 || arrow2Dir == -2)
						arrow.y = arrowStartY + arrowMovement;
				}
			}
			if (arrow2 != null)
			{
				if (arrow2.visible)
				{
					arrow2Movement += ARROW_MOVEMENT_AMT * arrow2Dir;
					if (Math.abs(arrow2Movement) > ARROW_MOVEMENT_MAX)
					{
						if (arrow2Dir == 1)
							arrow2Dir = -1;
						else
							arrow2Dir = 1;
					}
					arrow2.x = arrow2StartX + arrowMovement;
				}
			}
		}
		private function tutorialScreenFlash($e:Event):void
		{
			//Functionality for making the tutorial flash.
			//This triggers when the player attempts to make an "illegal" move.
			tutorialScreenAlpha += TUTORIAL_FLASH_AMT * tutorialScreenDir;
			
			if (tutorialScreenDir == -1)
			{
				if (tutorialScreenAlpha <= 0.0)
				{
					tutorialScreenAlpha = 0.0;
					tutorialScreenDir = 1;
				}
			}
			else
			{
				if (tutorialScreenAlpha >= 1.0)
				{
					tutorialScreenFlashCount++;
					if (tutorialScreenFlashCount >= 3)
					{
						removeEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
						tutorialScreenAlpha = 1.0;
					}
					else
					{
						tutorialScreenDir = -1;
					}
				}
			}
			tutorial.alpha = tutorialScreenAlpha;
		}
		private function mapSetup():void
		{	
			//Main.updateDebugText("Map Setup");
			
			if (Main.isPhone())
			{
				//New Clip Rect
				var clipSprite:Sprite = new Sprite();
				var clipRectWidth:Number = PathogenTest.wTenPercent * 7;
				var clipRectHeight:Number = PathogenTest.hTenPercent * 9;
				var rect:Rectangle = new Rectangle(PathogenTest.stageCenter.x - (clipRectWidth/2), PathogenTest.stageCenter.y - (clipRectHeight/2), clipRectWidth, clipRectHeight)
				clipSprite.clipRect = rect;
				addChild(clipSprite);
			}
			
			tileHolder = new Sprite();
			_virtualTileHolder = new Sprite();
			
			if (Main.isPhone())
			{
				clipSprite.addChild(tileHolder);
				clipSprite.addChild(_virtualTileHolder);
			}
			else
			{
				addChild(tileHolder);
				addChild(_virtualTileHolder);
			}
			
			var cellScaleX:Number;
			var cellScaleY:Number;
			
			//slight change in centering for a medium map
			if (_mapSize == 0)
			{
				gameBoardHeight = 14;
				gameBoardWidth = 16;
				cellScaleX = 1;
				cellScaleY = 1;
			}
			//slight change in centering for a medium map
			if (_mapSize == 1)
			{
				gameBoardHeight = 18;
				gameBoardWidth = 20;
				cellScaleX = .8;
				cellScaleY = .8;
			}
			//slight change in centering for a Large map
			if (_mapSize == 2)
			{
				gameBoardHeight = 22;
				gameBoardWidth = 24;
				cellScaleX = .65;
				cellScaleY = .65;
			}
			
			//Creating temporary players for maps where there are pre-populated cells.
			var tempGreenPlayer:Player = new Player(0,0,this);
			var tempRedPlayer:Player = new Player(1,1,this);
			var tempBluePlayer:Player = new Player(2,2,this);
			var tempOrangePlayer:Player = new Player(3,3,this);
			
			var tileCounter:int = 0;
			
			//Main.updateDebugText("Create Board");
			
			//Create the board
			for (var i:int = 0; i < gameBoardWidth; i++)
			{
				for (var j:int = 0; j < gameBoardHeight; j++)
				{
					var newCell:Cell = new Cell(tileCounter);
					
					newCell.gameManager = this;
					newCell.owner = null;
					
					//import boundaries from the chosen map
					if (mapData[tileCounter] == 0)
						newCell.state = 0;
					if (mapData[tileCounter] == 4)
					{
						newCell.state = 10;
						newCell.visible = false;
					}
					if (mapData[tileCounter] == 3)
						newCell.state = 9;
					//Grey Tiles
					if (mapData[tileCounter] == 2)
						newCell.state = 6;
					if (mapData[tileCounter] == 1)
						newCell.state = 5;
					//Inporting Player Colors
					//Green
					if (mapData[tileCounter] == 5)
					{
						newCell.state = 1;
						newCell.owner = tempGreenPlayer;
					}
					if (mapData[tileCounter] == 6)
					{
						newCell.state = 2;
						newCell.owner = tempGreenPlayer;
					}
					if (mapData[tileCounter] == 7)
					{
						newCell.state = 3;
						newCell.owner = tempGreenPlayer;
					}
					if (mapData[tileCounter] == 8)
					{
						newCell.state = 4;
						newCell.owner = tempGreenPlayer;
					}
					//Red
					if (mapData[tileCounter] == 9)
					{
						newCell.state = 1;
						newCell.owner = tempRedPlayer;
					}
					if (mapData[tileCounter] == 10)
					{
						newCell.state = 2;
						newCell.owner = tempRedPlayer;
					}
					if (mapData[tileCounter] == 11)
					{
						newCell.state = 3;
						newCell.owner = tempRedPlayer;
					}
					if (mapData[tileCounter] == 12)
					{
						newCell.state = 4;
						newCell.owner = tempRedPlayer;
					}
					//Blue
					if (mapData[tileCounter] == 13)
					{
						newCell.state = 1;
						newCell.owner = tempBluePlayer;
					}
					if (mapData[tileCounter] == 14)
					{
						newCell.state = 2;
						newCell.owner = tempBluePlayer;
					}
					if (mapData[tileCounter] == 15)
					{
						newCell.state = 3;
						newCell.owner = tempBluePlayer;
					}
					if (mapData[tileCounter] == 16)
					{
						newCell.state = 4;
						newCell.owner = tempBluePlayer;
					}
					//Orange
					if (mapData[tileCounter] == 17)
					{
						newCell.state = 1;
						newCell.owner = tempOrangePlayer;
					}
					if (mapData[tileCounter] == 18)
					{
						newCell.state = 2;
						newCell.owner = tempOrangePlayer;
					}
					if (mapData[tileCounter] == 19)
					{
						newCell.state = 3;
						newCell.owner = tempOrangePlayer;
					}
					if (mapData[tileCounter] == 20)
					{
						newCell.state = 4;
						newCell.owner = tempOrangePlayer;
					}
					if (mapData[tileCounter] == 21)
					{
						captureZoneMode = true;
						
						newCell.capturePoint = true;
						newCell.state = 0;
						newCell.setCapturePoint();
					}
					if (mapData[tileCounter] == 22)
					{
						erosionZoneMode = true;
						
						newCell.erosionCell = true;
						newCell.state = 9;
						newCell.activeErosionCell = false;
					}
					
					newCell.addTouchEvent();
					newCell.setColor();
					newCell.reset();
					newCell.addDisplayForNoAnimations();
					
					var tileGap:Number = newCell.width * cellScaleX;
					
					newCell.x = tileGap * i;
					newCell.y = tileGap * j;
					
					newCell.scaleX = cellScaleX - .05;
					newCell.scaleY = cellScaleY - .05;
					
					tileHolder.addChild(newCell);
					gameBoard.push(newCell);
					
					tileCounter++;
				}
			}
			
			var tileWidth:Number = gameBoard[0].width / 2;
			var tileHeight:Number = gameBoard[0].height / 2;
			
			tileHolder.pivotX = tileHolder.width / 2;
			tileHolder.pivotY = tileHolder.height / 2;
			
			tileHolder.x = PathogenTest.stageCenter.x + tileWidth;
			tileHolder.y = PathogenTest.stageCenter.y + tileHeight;
			
			_virtualTileHolder.pivotX = _virtualTileHolder.width / 2;
			_virtualTileHolder.pivotY = _virtualTileHolder.height / 2;
			
			_virtualTileHolder.x = tileHolder.x;
			_virtualTileHolder.y = tileHolder.y;
			
			_holderInitialPosition = new Point(tileHolder.x, tileHolder.y);
			
			if (Main.isPhone())
			{
				//Main.updateDebugText("Phone True, Creating boundaries");
				
				tileHolder.addEventListener(TouchEvent.TOUCH, onTouchLocation);
				
				// set the boundaries
				_topBoundary = tileHolder.y - tileHolder.height * 0.5;
				_bottomBoundary = tileHolder.y + tileHolder.height * 0.5;
				_leftBoundary = tileHolder.x - tileHolder.width * 0.5;
				_rightBoundary = tileHolder.x + tileHolder.width * 0.5;
				
				// set the initial boundaries
				_startTopBoundary = _topBoundary;
				_startBottomBoundary = _bottomBoundary;
				_startLeftBoundary = _leftBoundary;
				_startRightBoundary = _rightBoundary;
				
				// create zoom button
				_zoomButton = new BW_SocialButton(1.5, 1.0, "Zoom", "Zoom", 24, Main.assets.getTexture("ZoomIn_Off"), Main.assets.getTexture("ZoomOut_off"));
				_zoomButton.toggle = true;
				_zoomButton.x = PathogenTest.stageCenter.x;
				_zoomButton.y = PathogenTest.hTenPercent * 9.35;
				addChild(_zoomButton);
				_zoomButton.addEventListener(TouchEvent.TOUCH, onTouchZoom);
				_zoomButton.visible = true;
			}
			else
				_zoomPanDelay = ZOOM_PAN_DELAY_TIME;
			
			//Push Original State so that the player can roll back to the beginning of the game.
			var currentGameBoard:Array = [];
			var currentGameBoardOwnership:Array = [];
			
			for (var k:int = 0; k < gameBoard.length; k++)
			{				
				var currentTile:Cell = gameBoard[k];
				
				currentGameBoard.push(currentTile.state);
				
				if (currentTile.owner != null)
					currentGameBoardOwnership.push(currentTile.owner.playerTeam);
				else
					currentGameBoardOwnership.push(5);
			}
			
			playLog.push(currentGameBoard);
			playLogOwnership.push(currentGameBoardOwnership);
			
			//Assign neighboring cells to all cells.
			for each(var cell:Cell in gameBoard)
			{
				cell.adjacentTopCell = gameBoard[cell.location - 1];
				cell.adjacentBottomCell = gameBoard[cell.location + 1];
				cell.adjacentLeftCell = gameBoard[cell.location - gameBoardHeight];
				cell.adjacentRightCell = gameBoard[cell.location + gameBoardHeight];
			}
			
			//Main.updateDebugText("Creating Buttons");
			
			//Create Quit Button
			if (Main.isPhone())
				quitButton = new BW_Button(0.75, 1.0, "", 0);
			else
				quitButton = new BW_Button(0.5, 0.7, "", 0);
			quitButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(quitButton);
			quitButton.x = PathogenTest.wTenPercent *.5;
			quitButton.y = PathogenTest.hTenPercent * 9.35;
			quitButton.addEventListener(TouchEvent.TOUCH, addQuit);
			
			// rules button
			if (Main.isPhone())
				rulesButton = new BW_Button(0.75, 1.0, "", 0);
			else
				rulesButton = new BW_Button(0.5, 0.7, "", 0);
			rulesButton.addImage(Main.assets.getTexture("Pathogen_BTN_Help"), Main.assets.getTexture("Pathogen_BTN_Help_down"));
			addChild(rulesButton);
			
			if (Main.isPhone())
				rulesButton.x = PathogenTest.wTenPercent * 1.3;
			else
				rulesButton.x = PathogenTest.wTenPercent * 1.1;
			rulesButton.y = quitButton.y
			rulesButton.addEventListener(TouchEvent.TOUCH, openInGameTutorial);
			
			/*_winButton = new BW_Button(0.5, .7, "W", 18);
			addChild(_winButton);
			_winButton.x = PathogenTest.wTenPercent * 1.7;
			_winButton.y = quitButton.y
			_winButton.addEventListener(TouchEvent.TOUCH, onTouchWin);*/
			
			//Create Options Button
			if (Main.isPhone())
				optionsButton = new BW_Button(0.75, 1.0, "", 0);
			else
				optionsButton = new BW_Button(0.5, 0.7, "", 0);
			optionsButton.addImage(Main.assets.getTexture("Pathogen_BTN_Options"), Main.assets.getTexture("Pathogen_BTN_Down"));
			addChild(optionsButton);
			optionsButton.x = PathogenTest.wTenPercent * 9.5;
			optionsButton.y = quitButton.y;
			optionsButton.addEventListener(TouchEvent.TOUCH, addOptions);
			
			// create undo button
			if (Main.isPhone())
				_undoButton = new BW_Button(0.75, 1.0, "", 0);
			else
				_undoButton = new BW_Button(0.5, 0.7, "", 0);
			_undoButton.addImage(Main.assets.getTexture("Pathogen_BTN_Undo"), Main.assets.getTexture("Pathogen_BTN_Undo_down"));
			addChild(_undoButton);
			if (Main.isPhone())
				_undoButton.x = PathogenTest.wTenPercent * 8.7;
			else
				_undoButton.x = PathogenTest.wTenPercent * 8.9;
			_undoButton.y = quitButton.y;
			_undoButton.addEventListener(TouchEvent.TOUCH, onTouchUndo);

			//Confirm Button
			if (Main.isPhone())
				confirmButton = new BW_Button(2, 1.0, "Confirm", 24);
			else
				confirmButton = new BW_Button(2, 0.75, "Confirm", 24);
			addChild(confirmButton);
			confirmButton.x = PathogenTest.stageCenter.x;
			confirmButton.y = quitButton.y;
			confirmButton.addEventListener(TouchEvent.TOUCH, makeMove);
			confirmButton.visible = false;
			
			tutorialFlashAllowed = false;
			
			horizontalBar = new Quad(tileHolder.width, PathogenTest.hTenPercent * .5 * newCell.scaleX, Color.WHITE);
			addChild(horizontalBar);
			horizontalBar.pivotX = horizontalBar.width/2;
			horizontalBar.pivotY = horizontalBar.height/2;
			
			horizontalBar.x = PathogenTest.stageCenter.x;
			horizontalBar.y = PathogenTest.stageCenter.y;
			horizontalBar.alpha = .1;
			
			verticalBar = new Quad(PathogenTest.hTenPercent * .5 * newCell.scaleX, tileHolder.height, Color.WHITE);
			addChild(verticalBar);
			verticalBar.pivotX = verticalBar.width/2;
			verticalBar.pivotY = verticalBar.height/2;
			
			verticalBar.x = PathogenTest.stageCenter.x;
			verticalBar.y = PathogenTest.stageCenter.y;
			verticalBar.alpha = .1;
			
			horizontalBar.visible = false;
			verticalBar.visible = false;
			
			boardOutline = new BoardOutline();
			addChild(boardOutline);
			
			//Create all players
			playerSetup();
			
			//Show first time tutorial of this unique game mode.
			if(captureZoneMode)
			{
				if(sharedData.data.havePlayedACaptureZoneMap == null)
				{
					tutorial_CaptureZone = true;
					sharedData.data.havePlayedACaptureZoneMap = tutorial_CaptureZone;
				}
			}
			//Show first time tutorial of this unique game mode.
			if(erosionZoneMode)
			{
				if(sharedData.data.havePlayedAnErosionZoneMap == null)
				{
					tutorial_ErosionMode = true;
					sharedData.data.havePlayedAnErosionZoneMap = tutorial_ErosionMode;
				}
			}
			
			addPercentUI();
			
			//Do special functions to setup this mode.
			if(captureZoneMode)
			{
				createCaptureZones();
				addCaptureZoneIndicator();
			}
			//Do special functions to setup this mode.
			if (erosionZoneMode)
				createErosionZones();
			
			//Determine if the player chose to play a Lighting game
			if(sharedData.data.GameLength != null)
			{
				numOfLightningRounds = sharedData.data.GameLength;
				if(numOfLightningRounds != 100)
				{
					addLightningCounter();
					lightning = true;
				}
			}
			//Determine if the player chose to play a Timed game
			if(sharedData.data.TurnLength != null)
			{
				timerThreshold = sharedData.data.TurnLength;
				if(timerThreshold != 30)
				{
					addClock();
					timerCounter = timerThreshold;
					timer = true;
				}
			}
			
			if(tutorial_01)
			{
				_undoButton.visible = false;
				_percentBar.visible = false;
			}
			
			if(tutorial_02)
				currentPlayer.cellSelectorArray[1].visible = false	
			if(tutorial_03)
			{
				currentPlayer.cellSelectorArray[2].currentNum = 6;
				currentPlayer.cellSelectorArray[2].incrementLvl();
				currentPlayer.cellSelectorArray[2].visible = false
				playerArray[1].cellSelectorArray[2].resetSelectedTile();
			}
			if(tutorial_04)
			{
				playerArray[1].cellSelectorArray[1].resetSelectedTile();
				playerArray[1].cellSelectorArray[2].resetSelectedTile();
			}
				
			if(tutorial_05)
			{
				currentPlayer.cellSelectorArray[3].visible = false
				playerArray[1].cellSelectorArray[3].resetSelectedTile();
			}
			
			checkEndGame();
			
			addEventListener(Event.ENTER_FRAME, update);
			
			levelTitle = new LevelTitle(this,mapName);
			addChild(levelTitle);
			
			if(campaign == null && Flurry.isSupported)
				Flurry.startTimedEvent("Local Multiplayer Game", { NumberOfPlayers : playerColors.length, MapSize : mapSize, MapName : mapName, Timer: timer, Lightning : lightning, CaptureZone : captureZoneMode, Erosion : erosionZoneMode, Infection: infection});
		}
		public function removeTitle():void
		{
			levelTitle.dispose();
			removeChild(levelTitle);
			levelTitle = null;
			
			if (tutorial_01 || tutorial_02 || tutorial_03 || tutorial_04 || tutorial_05 || tutorial_CaptureZone || tutorial_ErosionMode || tutorial_InfectionMode)
				addTutorialScreen()
			
			//If 1st player is an AI, take their turn.
			if (currentPlayer.getAiControl())
			{
				ai.copyBoard(currentPlayer);
				ai.triggerAIMove();
			}
			
			if(sharedData.data.hasPlayedPathogenBefore == null && _campaign == null)
			{
				openInGameTutorial(null);
				_hasPlayedPathogenBefore = true;
				sharedData.data.hasPlayedPathogenBefore = _hasPlayedPathogenBefore;
			}
		}
		private function openInGameTutorial($e:TouchEvent):void
		{
			if($e)
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				var open:Boolean = true;
			
			if (touch || open)
			{
				inGameTutorial = new InGameTutorial(this);
				addChild(inGameTutorial);
			}
		}
		public function removeInGameTutorial():void
		{
			inGameTutorial.GarbageCollection();
			inGameTutorial.dispose();
			removeChild(inGameTutorial);
			inGameTutorial = null;
		}
		
		private function onTouchLocation($e:TouchEvent):void
		{
			var touches:Vector.<Touch>
			
			touches = $e.getTouches(this, TouchPhase.MOVED);
			
			if (touches.length == 1 && tileHolder.scaleX > 1.0)
			{
				// one finger touching -> move
				var delta:Point = touches[0].getMovement(parent);
				
				// if the holder is not going off screen, move it left or right
				if (delta.x < 0.0)
				{
					if (_rightBoundary > _startRightBoundary)
					{
						tileHolder.x += delta.x;
						if ((tileHolder.x + (tileHolder.width / 2)) < _startRightBoundary)
							tileHolder.x = _startRightBoundary - (tileHolder.width / 2);
					}
				}
				else if (delta.x > 0.0)
				{
					if (_leftBoundary < _startLeftBoundary)
					{
						tileHolder.x += delta.x;
						if ((tileHolder.x - (tileHolder.width / 2)) > _startLeftBoundary)
							tileHolder.x = _startLeftBoundary + (tileHolder.width / 2);
					}
				}
				
				// if the holder is not going off screen, move it up or down
				if (delta.y < 0.0)
				{
					if (_bottomBoundary > _startBottomBoundary)
					{
						tileHolder.y += delta.y;
						if ((tileHolder.y + (tileHolder.height / 2)) < _startBottomBoundary)
							tileHolder.y = _startBottomBoundary - (tileHolder.height / 2);
					}
				}
				else if (delta.y > 0.0)
				{
					if (_topBoundary < _startTopBoundary)
					{
						tileHolder.y += delta.y;
						if ((tileHolder.y - (tileHolder.height / 2)) > _startTopBoundary)
							tileHolder.y = _startTopBoundary + (tileHolder.height / 2);
					}
				}
				
				_topBoundary = tileHolder.y - (tileHolder.height / 2);
				_bottomBoundary = tileHolder.y + (tileHolder.height / 2);
				_leftBoundary = tileHolder.x - (tileHolder.width / 2);
				_rightBoundary = tileHolder.x + (tileHolder.width / 2);
				
				_zoomPanDelay = 0;
			}
			else if (touches.length == 2 && _zoomPanDelay >= ZOOM_PAN_DELAY_TIME)
			{
				// two fingers touching -> scale
				var touchA:Touch = touches[0];
				var touchB:Touch = touches[1];
				
				var currentPosA:Point  = touchA.getLocation(this);
				var previousPosA:Point = touchA.getPreviousLocation(this);
				var currentPosB:Point  = touchB.getLocation(this);
				var previousPosB:Point = touchB.getPreviousLocation(this);
				
				var currentVector:Point  = currentPosA.subtract(currentPosB);
				var previousVector:Point = previousPosA.subtract(previousPosB);
				
				// scale
				var sizeDiff:Number = currentVector.length / previousVector.length;
				
				if(sizeDiff > 1.0 && tileHolder.scaleX <= 2.0)
				{
					tileHolder.scaleX = 2.0;
					tileHolder.scaleY = 2.0;
				}
				else if (sizeDiff < 1.0 && tileHolder.scaleX >= 1.0)
				{
					tileHolder.scaleX = 1.0;
					tileHolder.scaleY = 1.0;
				}
				
				// check if scaling smaller than smallest, and if it is, adjust to the smallest size
				if (tileHolder.scaleX < _holderInitialScaleX)
				{
					tileHolder.scaleX = _holderInitialScaleX;
				}
				if (tileHolder.scaleY < _holderInitialScaleY)
				{
					tileHolder.scaleY = _holderInitialScaleY;
				}

				// check if scaling is the initial, and if so, position at initial position
				if (tileHolder.scaleX == _holderInitialScaleX)
				{
					tileHolder.x = _holderInitialPosition.x;
					tileHolder.y = _holderInitialPosition.y;
				}
				
				if ((tileHolder.x + (tileHolder.width / 2)) < _startRightBoundary)
					tileHolder.x = _startRightBoundary - (tileHolder.width / 2);
				if ((tileHolder.x - (tileHolder.width / 2)) > _startLeftBoundary)
					tileHolder.x = _startLeftBoundary + (tileHolder.width / 2);
				if ((tileHolder.y + (tileHolder.height / 2)) < _startBottomBoundary)
					tileHolder.y = _startBottomBoundary - (tileHolder.height / 2);
				if ((tileHolder.y - (tileHolder.height / 2)) > _startTopBoundary)
					tileHolder.y = _startTopBoundary + (tileHolder.height / 2);
				
				_topBoundary = tileHolder.y - (tileHolder.height / 2);
				_bottomBoundary = tileHolder.y + (tileHolder.height / 2);
				_leftBoundary = tileHolder.x - (tileHolder.width / 2);
				_rightBoundary = tileHolder.x + (tileHolder.width / 2);
				
				_zoomPanDelay = 0;
			}
		}
		
		private function onTouchZoom(event:TouchEvent):void
		{
			if(event)
			{
				var touch:Touch = event.getTouch(this, TouchPhase.ENDED);

				if (touch && _zoomPanDelay >= ZOOM_PAN_DELAY_TIME)
				{
					if(tileHolder.scaleX < 2.0)
					{
						tileHolder.scaleX = 2.0;
						tileHolder.scaleY = 2.0;
					}
					else
					{
						tileHolder.scaleX = 1.0;
						tileHolder.scaleY = 1.0;
					}
				
					// check if scaling smaller than smallest, and if it is, adjust to the smallest size
					if (tileHolder.scaleX < _holderInitialScaleX)
					{
						tileHolder.scaleX = _holderInitialScaleX;
					}
					if (tileHolder.scaleY < _holderInitialScaleY)
					{
						tileHolder.scaleY = _holderInitialScaleY;
					}
	
					// check if scaling is the initial, and if so, position at initial position
					if (tileHolder.scaleX == _holderInitialScaleX)
					{
						tileHolder.x = _holderInitialPosition.x;
						tileHolder.y = _holderInitialPosition.y;
					}
				
					if ((tileHolder.x + (tileHolder.width / 2)) < _startRightBoundary)
						tileHolder.x = _startRightBoundary - (tileHolder.width / 2);
					if ((tileHolder.x - (tileHolder.width / 2)) > _startLeftBoundary)
						tileHolder.x = _startLeftBoundary + (tileHolder.width / 2);
					if ((tileHolder.y + (tileHolder.height / 2)) < _startBottomBoundary)
						tileHolder.y = _startBottomBoundary - (tileHolder.height / 2);
					if ((tileHolder.y - (tileHolder.height / 2)) > _startTopBoundary)
						tileHolder.y = _startTopBoundary + (tileHolder.height / 2);
				
					_topBoundary = tileHolder.y - (tileHolder.height / 2);
					_bottomBoundary = tileHolder.y + (tileHolder.height / 2);
					_leftBoundary = tileHolder.x - (tileHolder.width / 2);
					_rightBoundary = tileHolder.x + (tileHolder.width / 2);
				
					_zoomPanDelay = 0;
				}
			}
		}
		
		private function createCaptureZones():void
		{
			var cell:Cell;
			var captureZone:CaptureZone;
			
			//Loop through all cells, if cell is a capture point & capture point does not already belong to a group, begin assignment.
			for each (cell in gameBoard)
			{
				if (cell.capturePoint == true && cell.captureZoneNumber == 0)
				{
					cell.assignCaptureZone(currentZoneNumber);
					currentZoneNumber++;
				}
			}
			
			//Create each capture zone.
			for (var i:int = 1; i < currentZoneNumber; i++)
			{
				captureZone = new CaptureZone();
				captureZone.identifier = i;
				
				for each (cell in gameBoard)
				{
					//All cells assigned to 1 are put into the same capture zone.
					if (cell.captureZoneNumber == i)
						captureZone.zoneArray.push(cell);
				}
				
				captureZoneList.push(captureZone);
				fullCaptureZoneList.push(captureZone);
			}
			
			//Determine how many capture zones are active.
			if(movingCaptureZone)
				pickActiveCaptureZone();
			else
			{
				for each (captureZone in captureZoneList)
				{
					captureZone.active = true;
				}
			}
		}
		private function pickActiveCaptureZone():void
		{
			var cell:Cell;
			var captureZone:CaptureZone;
			
			//Decide on how many capture zones are active at 1 time.
			if (captureZoneList.length > 3)
				numOfActiveControlZones = 2;
			else
				numOfActiveControlZones = 1;
			
			//activate the capture zones.
			for (var i:int = 0; i < numOfActiveControlZones; i++)
			{
				var randomNum:int = Math.round(Math.random() * (captureZoneList.length - 1));
				
				captureZone = captureZoneList[randomNum];
				captureZone.active = true;
				
				for each (cell in captureZone.zoneArray)
				{
					cell.activeCapturePoint = true;
					cell.setCapturePoint();
				}	
			}
		}
		public function createErosionZones():void
		{
			var cell:Cell;
			var erosionZone:ErosionZone;
			
			//Loop through all cells, if cell is a erosion cell & it does not already belong to a group, begin assignment.
			for each (cell in gameBoard)
			{
				if (cell.erosionCell == true && cell.erosionZoneNumber == 0)
				{
					cell.assignErosionZone(currentErosionZoneNumber);
					currentErosionZoneNumber++;
				}
			}
			
			//Create each Erosion Zone
			for (var i:int = 1; i < currentErosionZoneNumber; i++)
			{
				erosionZone = new ErosionZone(this);
				erosionZone.identifier = i;
				
				for each (cell in gameBoard)
				{
					//All cells assigned to 1 are put into the same erosion zone.
					if (cell.erosionZoneNumber == i)
						erosionZone.zoneArray.push(cell);
				}
				
				erosionZoneList.push(erosionZone);
				fullErosionZoneList.push(erosionZone);
			}
			
			pickActiveErosionZone();
		}
		public function pickActiveErosionZone():void
		{
			if(erosionZoneList.length > 0)
			{
				var randomZone:int = Math.round(Math.random() * (erosionZoneList.length - 1));
				currentlyActiveErosionZone = erosionZoneList[randomZone];
				erosionZoneList.splice(randomZone,1);
			}
		}
		private function addPercentUI():void
		{
			_percentBar = new Sprite();
			addChild(_percentBar);
			_percentBar.pivotX = _percentBar.width/2;
			_percentBar.pivotY = _percentBar.height/2;
			_percentBar.x = PathogenTest.stageCenter.x;
			
			if (Main.isPhone() && _mapSize != 0)
			//if(PathogenTest.device == PathogenTest.iPHONE_4 && _mapSize != 0 || PathogenTest.device == PathogenTest.iPHONE_5 && _mapSize != 0)
				_percentBar.y = PathogenTest.hTenPercent * .3;
			else
				_percentBar.y = PathogenTest.hTenPercent * .5;
			
			var percentBarFrame:Quad = new Quad(PathogenTest.wTenPercent * 7, PathogenTest.hTenPercent * .2, Color.BLACK)
			_percentBar.addChild(percentBarFrame);
			percentBarFrame.pivotX = percentBarFrame.width/2;
			percentBarFrame.pivotY = percentBarFrame.height/2;
			percentBarFrame.alpha = .7;
			
			percentFrameWidth = percentBarFrame.width;
			
			for each (var player:Player in playerArray)
			{
				player.percentBar = new Quad(1,percentBarFrame.height, player.playerHexColor);
				player.percentBar.pivotY = player.percentBar.height /2;
				player.percentBar.alpha = .5;
				_percentBar.addChild(player.percentBar);
				player.percentBar.visible = false;
				
				player.percentBarOfTierThree = new Quad(1,percentBarFrame.height, player.playerHexColor);
				player.percentBarOfTierThree.pivotY = player.percentBarOfTierThree.height /2;
				player.percentBarOfTierThree.alpha = .8;
				_percentBar.addChild(player.percentBarOfTierThree);
				player.percentBarOfTierThree.visible = false;
				
			}
			
			for (var i:int = 0; i < 11; i++)
			{
				var tenPercentMark:Quad = new Quad(2, percentBarFrame.height, Color.GRAY);
				_percentBar.addChild(tenPercentMark);
				tenPercentMark.pivotY = tenPercentMark.height/2;
				
				tenPercentMark.x = (percentBarFrame.x - percentBarFrame.width/2)  + ((percentBarFrame.width/10) * i);
			}
		}
		private function addClock():void
		{
			//Add an in-game clock
			var fontSize:int = PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
			var textX:int = PathogenTest.HD_Multiplyer * 200 / PathogenTest.scaleFactor;
			var textY:int = PathogenTest.HD_Multiplyer * 100 / PathogenTest.scaleFactor;
			clock = new TextField(textX, textY, "0:" + timerThreshold.toString(), "Dekar", fontSize, Color.WHITE);
			clock.pivotY = clock.height/2;
			addChild(clock);
			clock.x = PathogenTest.wTenPercent * 8.25;
			clock.y = PathogenTest.hTenPercent * 0.5;
		}
		private function addLightningCounter():void
		{
			var fontSize:int;
			
			lightningIndicator = new Sprite();
			addChild(lightningIndicator);
			lightningIndicator.x = PathogenTest.wTenPercent * 0.25;
			lightningIndicator.y = PathogenTest.hTenPercent * 0.5;
			if(captureZoneMode)
				lightningIndicator.y = PathogenTest.hTenPercent * .8;
			
			fontSize = PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor;
			lightningCounter = new TextField(PathogenTest.wTenPercent * .35, PathogenTest.hTenPercent/2, numOfLightningRounds.toString(), "Dekar", fontSize, Color.WHITE);
			lightningCounter.pivotY = lightningCounter.height/2;
			lightningIndicator.addChild(lightningCounter);
			
			var textField:TextField = new TextField(PathogenTest.wTenPercent * 1.2, PathogenTest.hTenPercent, "Rounds", "Dekar", fontSize, Color.WHITE);
			textField.pivotY = textField.height/2;
			lightningIndicator.addChild(textField);
			textField.hAlign = HAlign.LEFT;
			textField.x = lightningCounter.x + lightningCounter.width;
		}
		private function addOptions($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && !moveUnderway)
			{
				gameOptions = new GameOptions(this);
				addChild(gameOptions);
				gameOptions.x = PathogenTest.stageCenter.x;
				gameOptions.y = PathogenTest.stageCenter.y;
			}
		}
		public function removeOptions():void
		{
			gameOptions.dispose();
			removeChild(gameOptions);
			gameOptions = null;
		}
		private function addQuit($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && !moveUnderway)
			{
				if(defeat)
				{
					addDefeatScreen();
				}
				else
				{
					quitScreen = new QuitScreen(this);
					addChild(quitScreen);
					quitScreen.x = PathogenTest.stageCenter.x;
					quitScreen.y = PathogenTest.stageCenter.y;
				}
			}
		}
		public function removeQuit():void
		{
			if(quitScreen)
			{
				quitScreen.dispose();
				removeChild(quitScreen);
				quitScreen = null;
			}
		}
		public function quitGame():void
		{
			forfiet = true;
			checkEndGame();
		}
		public function retryGame():void
		{
			_retry = true;
			Quit();
		}
		private function addTutorialScreen():void
		{
			//Some tutorials have bouncing arrows that help direct the player.
			undidTurnInCampaign = false;
			
			arrow = new MovieClip(Main.assets.getTextures("Arrow"));
			arrow.pivotX = arrow.width / 2;
			arrow.pivotY = arrow.height / 2;
			addChild(arrow);
			arrow.visible = false;
			
			arrow2 = new MovieClip(Main.assets.getTextures("Arrow"));
			arrow2.pivotX = arrow.width / 2;
			arrow2.pivotY = arrow.height / 2;
			addChild(arrow2);
			arrow2.visible = false;;
			
			var tween:Tween;
			
			if (tutorial_01)
			{
				_undoButton.visible = false;
				_percentBar.visible = false;
				
				if (roundCounter == 0)
				{
					for ( var m:int = 0; m < gameBoard.length; m++)
					{
						var cell:Cell = gameBoard[m];
						
						if(cell.state == 0)
							cell.createWhiteFlash();
					}
				}
			}
			else if (tutorial_02)
			{
				transparentBackground = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
				addChild(transparentBackground);
				transparentBackground.alpha = .65;
				
				greenStage2Unlock = new Image(Main.assets.getTexture("Green_Stage02_Big"));
				greenStage2Unlock.pivotX = greenStage2Unlock.width/2;
				greenStage2Unlock.pivotY = greenStage2Unlock.height/2;
				addChild(greenStage2Unlock);
				greenStage2Unlock.x = PathogenTest.stageCenter.x;
				greenStage2Unlock.y = PathogenTest.hTenPercent * 4.5;
				greenStage2Unlock.alpha = 0;
				
				tween = new Tween(greenStage2Unlock, 1)
				tween.fadeTo(1);
				Starling.juggler.add(tween);
				Starling.juggler.delayCall(moveGreenUnlock02, 2);
			}
			else if (tutorial_03)
			{
				transparentBackground = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
				addChild(transparentBackground);
				transparentBackground.alpha = .65;
				
				greenStage3Unlock = new Image(Main.assets.getTexture("Green_Stage03_Big"));
				greenStage3Unlock.pivotX = greenStage3Unlock.width/2;
				greenStage3Unlock.pivotY = greenStage3Unlock.height/2;
				addChild(greenStage3Unlock);
				greenStage3Unlock.x = PathogenTest.stageCenter.x;
				greenStage3Unlock.y = PathogenTest.hTenPercent * 4.5;
				greenStage3Unlock.alpha = 0;
				
				tween = new Tween(greenStage3Unlock, 1)
				tween.fadeTo(1);
				Starling.juggler.add(tween);
				Starling.juggler.delayCall(moveGreenUnlock03, 2);
			}
			else if (tutorial_04)
			{
				currentPlayer.cellSelectorArray[1].createWhiteFlash();
				
				arrow.visible = true;
				arrow.x = currentPlayer.cellSelectorArray[1].x + (currentPlayer.cellSelectorArray[1].width * .7);
				arrow.y = currentPlayer.cellSelectorArray[1].y;
				arrowStartX = arrow.x;
				arrowDir = 1;
				arrowMovement = 0.0;
				arrow.addEventListener(Event.ENTER_FRAME, arrowBounce);
			}
			else if (tutorial_05)
			{
				transparentBackground = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
				addChild(transparentBackground);
				transparentBackground.alpha = .65;
				
				greenStage4Unlock = new Image(Main.assets.getTexture("Green_Virus_Big"));
				greenStage4Unlock.pivotX = greenStage4Unlock.width/2;
				greenStage4Unlock.pivotY = greenStage4Unlock.height/2;
				addChild(greenStage4Unlock);
				greenStage4Unlock.x = PathogenTest.stageCenter.x;
				greenStage4Unlock.y = PathogenTest.hTenPercent * 4.5;
				greenStage4Unlock.alpha = 0;
				
				tween = new Tween(greenStage4Unlock, 1)
				tween.fadeTo(1);
				Starling.juggler.add(tween);
				Starling.juggler.delayCall(moveGreenUnlock04, 2);
			}
			
			if (tutorial_01)
				tutorial = new Tutorial(this, "Tutorial_01");
			if (tutorial_02)
				tutorial = new Tutorial(this, "Tutorial_02");
			if (tutorial_03)
				tutorial = new Tutorial(this, "Tutorial_03");
			if (tutorial_04)
				tutorial = new Tutorial(this, "Tutorial_04");
			if (tutorial_05)
				tutorial = new Tutorial(this, "Tutorial_05");
			
			if (tutorial_CaptureZone)
				tutorial = new Tutorial(this, "TutorialCaptureZone");
			if (tutorial_ErosionMode)
				tutorial = new Tutorial(this, "TutorialErosionZone");
			if (tutorial_InfectionMode)
				tutorial = new Tutorial(this, "TutorialInfection");
			
			addChild(tutorial);
			tutorial.x = PathogenTest.stageCenter.x;
			tutorial.y = PathogenTest.stageHeight - PathogenTest.hTenPercent * 2.5;
		}
		private function moveGreenUnlock02():void
		{
			var tween:Tween = new Tween(greenStage2Unlock, TUTORIAL_UNLOCK_MOVE_TIME);
			tween.moveTo(currentPlayer.cellSelectorArray[1].x + 30,currentPlayer.cellSelectorArray[1].y);
			tween.scaleTo(.3);
			tween.onComplete = removeGreenUnlock02;
			Starling.juggler.add(tween);
			
			var backgroundTween:Tween = new Tween(transparentBackground, 2);
			backgroundTween.fadeTo(0);
			Starling.juggler.add(backgroundTween);
		}
		private function removeGreenUnlock02():void
		{
			removeChild(greenStage2Unlock)
			removeChild(transparentBackground);
			
			currentPlayer.cellSelectorArray[1].visible = true;
			currentPlayer.cellSelectorArray[1].createWhiteFlash();
			
			arrow.visible = true;
			arrow.x = currentPlayer.cellSelectorArray[1].x + ((5 * currentPlayer.cellSelectorArray[1].width) / 8);
			arrow.y = currentPlayer.cellSelectorArray[1].y;
			arrowStartX = arrow.x;
			arrowDir = 1;
			arrowMovement = 0.0;
			arrow2Movement = 0.0;
			arrow.addEventListener(Event.ENTER_FRAME, arrowBounce);
		}
		private function moveGreenUnlock03():void
		{
			var tween:Tween = new Tween(greenStage3Unlock, TUTORIAL_UNLOCK_MOVE_TIME);
			tween.moveTo(currentPlayer.cellSelectorArray[2].x + 30,currentPlayer.cellSelectorArray[2].y);
			tween.scaleTo(.3);
			tween.onComplete = removeGreenUnlock03;
			Starling.juggler.add(tween);
			
			var backgroundTween:Tween = new Tween(transparentBackground, 2);
			backgroundTween.fadeTo(0);
			Starling.juggler.add(backgroundTween);
		}
		private function removeGreenUnlock03():void
		{
			removeChild(greenStage3Unlock)
			removeChild(transparentBackground);
			
			currentPlayer.cellSelectorArray[2].visible = true;
			currentPlayer.cellSelectorArray[2].createWhiteFlash();
			
			arrow.visible = true;
			arrow.x = currentPlayer.cellSelectorArray[2].x + ((5 * currentPlayer.cellSelectorArray[2].width) / 8);
			arrow.y = currentPlayer.cellSelectorArray[2].y;
			arrowStartX = arrow.x;
			arrowDir = 1;
			arrowMovement = 0.0;
			arrow2Movement = 0.0;
			arrow.addEventListener(Event.ENTER_FRAME, arrowBounce);
		}
		private function moveGreenUnlock04():void
		{
			var tween:Tween = new Tween(greenStage4Unlock, TUTORIAL_UNLOCK_MOVE_TIME);
			tween.moveTo(currentPlayer.cellSelectorArray[3].x,currentPlayer.cellSelectorArray[3].y);
			tween.scaleTo(.3);
			tween.onComplete = removeGreenUnlock04;
			Starling.juggler.add(tween);
			
			var backgroundTween:Tween = new Tween(transparentBackground, 2);
			backgroundTween.fadeTo(0);
			Starling.juggler.add(backgroundTween);
		}
		private function removeGreenUnlock04():void
		{
			removeChild(greenStage4Unlock)
			removeChild(transparentBackground);
			
			currentPlayer.cellSelectorArray[3].visible = true;
			currentPlayer.cellSelectorArray[3].createWhiteFlash();
			
			arrow.visible = true;
			arrow.x = currentPlayer.cellSelectorArray[3].x + (currentPlayer.cellSelectorArray[1].width / 2);
			arrow.y = currentPlayer.cellSelectorArray[3].y;
			arrowStartX = arrow.x;
			arrowDir = 1;
			arrowMovement = 0.0;
			arrow.addEventListener(Event.ENTER_FRAME, arrowBounce);
		}
		private function addCaptureZoneIndicator():void
		{
			captureZoneIndicator = new Sprite();
			addChild(captureZoneIndicator);
			captureZoneIndicator.x = PathogenTest.wTenPercent * .25;
			captureZoneIndicator.y = PathogenTest.hTenPercent * .5;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor;
			claimText = new TextField(PathogenTest.wTenPercent * .3, PathogenTest.hTenPercent/2, "10", "Dekar", fontSize, Color.WHITE);
			claimText.pivotY = claimText.height/2;
			captureZoneIndicator.addChild(claimText);
			
			_claimTextExtension = new TextField(PathogenTest.wTenPercent * 1.2, PathogenTest.hTenPercent, "Rounds", "Dekar", fontSize, Color.WHITE);
			_claimTextExtension.pivotY = _claimTextExtension.height/2;
			captureZoneIndicator.addChild(_claimTextExtension);
			_claimTextExtension.hAlign = HAlign.LEFT;
			_claimTextExtension.x = claimText.x + claimText.width;
			
			updateClaimText();
		}
		private function updateClaimText():void
		{
			var roundsUntilClaim:int = claimNum - currentClaimCount;
			
			claimText.text = roundsUntilClaim.toString()
		}
		private function addCaptureZoneScreen():void
		{
			if(captureZoneScreen == null)
			{
				var roundsUntilClaim:int = claimNum - currentClaimCount;
				captureZoneScreen = new CaptureZoneScreen(this, roundsUntilClaim);
				addChild(captureZoneScreen);
				captureZoneScreen.x = PathogenTest.stageCenter.x;
				captureZoneScreen.y = PathogenTest.stageCenter.y;
			}
		}
		public function removeCaptureZoneScreen():void
		{
			captureZoneScreen.dispose();
			removeChild(captureZoneScreen);
			captureZoneScreen = null;
		}
		private function playerSetup():void
		{
			//trace("playerSetup");
			
			var i:int;
			
			//Create players
			numOfPlayers = playerLocations.length;
			
			for (i = 0; i < numOfPlayers; i++)
			{
				var newPlayer:Player = new Player(playerColors[i],playerLocations[i], this);
				if (playerAI[i])
					newPlayer.setAiControl(true);
				addChild(newPlayer);
				playerArray.push(newPlayer);
				playerArray[i].setPlayerArrayIndex(i);
			}
			//Set the current player to player 1
			currentPlayerNum = 0;
			currentPlayer = playerArray[currentPlayerNum];
			
			//trace("set player to " + currentPlayer);
			
			// if iphone - selector resize
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				if (!currentPlayer.getAiControl())
				{
					currentPlayer.increaseSelectorSizes();
				}
			}
			
			//currentPlayer.showAllSelectedCells();
			boardOutline.changeColors(currentPlayer.playerHexColor);
			
			//Create Tile Selector icon
			var selectorDisplay:Image = new Image(Main.assets.getTexture("Selector"));
			
			tileSelector = new BW_Selector(0,0, selectorDisplay);
			addChild(tileSelector);
			setChildIndex(tileSelector, 0);
			
			// if iphone - selector resize
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				tileSelector.scaleX = 1.3;
				tileSelector.scaleY = 1.3;
			}
			
			//Automatically select the level 1 tile of the current player
			cellSelectorHasBeenClicked(currentPlayer.cellSelectorArray[0]);
			
			for each (var cellSelector:CellSelector in currentPlayer.cellSelectorArray)
			{
				if (cellSelector.countDownIndicator != null)
					cellSelector.incrementLvl();
			}
			
			// make selectors invisible for particular tutorial levels
			var player:Player;
			var selectedCell:CellSelector;
			if (tutorial_01)
			{
				for each (player in playerArray)
				{
					for (i = 1; i < player.cellSelectorArray.length; i++)
					{
						selectedCell = player.cellSelectorArray[i];
						selectedCell.visible = false;
					}
				}
			}
			else if (tutorial_02)
			{
				for each (player in playerArray)
				{
					for (i = 2; i < player.cellSelectorArray.length; i++)
					{
						selectedCell = player.cellSelectorArray[i];
						selectedCell.visible = false;
					}
				}
			}
			else if (tutorial_03 || tutorial_04)
			{
				for each (player in playerArray)
				{
					for (i = 3; i < player.cellSelectorArray.length; i++)
					{
						selectedCell = player.cellSelectorArray[i];
						selectedCell.visible = false;
					}
				}
			}
			
			// remove undo button if all AI
			var allAI:Boolean = true;
			for (i=0; i<playerArray.length; i++)
			{
				if (!playerArray[i].getAiControl())
				{
					allAI = false;
					break;
				}
			}
			
			//trace("allAI = " + allAI);
			
			if (allAI)
				_undoButton.visible = false;
		}
		public function cellSelectorHasBeenClicked(whatCellSelector:CellSelector):void
		{
			//trace(" " + whatCellSelector.type + " has been clicked"); 
			if (!currentPlayer.getAiControl())
			{
				if(whatCellSelector.owner == currentPlayer)
				{
					var allowClick:Boolean = true;
					var arrowPos:Point;
					
					if (tutorial_02)
					{
						if (roundCounter == 0)
						{
							if (tutorialFlashAllowed)
							{
								if (whatCellSelector.type != 1)
								{
									allowClick = false;
									tutorialScreenFlashCount = 0;
									tutorialScreenAlpha = 1.0;
									tutorialScreenDir = -1;
									addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
								}
								if(!_playerClickedTheTutorialButton && whatCellSelector.type == 1)
								{
									_playerClickedTheTutorialButton = true;
									
									tutorial.nextPage();
									
									gameBoard[75].createWhiteFlash();
									gameBoard[76].createWhiteFlash();
									gameBoard[77].createWhiteFlash();
									
									currentPlayer.cellSelectorArray[1].removeWhiteFlash();
									
									if(arrow)
										arrow.visible = false;
								}
							}
							else
								tutorialFlashAllowed = true;
						}
					}
					else if (tutorial_03)
					{
						if (!undidTurnInCampaign)
						{
							if (roundCounter == 0)
							{
								if (tutorialFlashAllowed)
								{
									if (whatCellSelector.type != 2)
									{
										allowClick = false;
										tutorialScreenFlashCount = 0;
										tutorialScreenAlpha = 1.0;
										tutorialScreenDir = -1;
										addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
									}
									if(!_playerClickedTheTutorialButton && whatCellSelector.type == 2)
									{
										_playerClickedTheTutorialButton = true;
										
										tutorial.nextPage();
										
										for ( var j:int = 0; j < gameBoard.length; j++)
										{
											var cell:Cell = gameBoard[j];
											
											if(cell.state == 3 && cell.owner.playerTeam == currentPlayer.playerTeam)
												cell.createWhiteFlash();
											
										}
										
										currentPlayer.cellSelectorArray[2].removeWhiteFlash();
										
										if(arrow)
											arrow.visible = false;
									}
								}
								else
									tutorialFlashAllowed = true;
							}
						}
					}
					else if (tutorial_04)
					{
						if (roundCounter == 0)
						{
							if (tutorialFlashAllowed)
							{
								if (whatCellSelector.type != 1)
								{
									allowClick = false;
									tutorialScreenFlashCount = 0;
									tutorialScreenAlpha = 1.0;
									tutorialScreenDir = -1;
									addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
								}
								if(!_playerClickedTheTutorialButton && whatCellSelector.type == 1)
								{
									_playerClickedTheTutorialButton = true;
									
									for ( var k:int = 0; k < gameBoard.length; k++)
									{
										cell = gameBoard[k];
										
										if(cell.state == 1 && cell.owner.playerTeam != currentPlayer.playerTeam)
											cell.createWhiteFlash();
									}
									
									tutorial.nextPage();
									
									currentPlayer.cellSelectorArray[1].removeWhiteFlash();
									
									if(arrow)
										arrow.visible = false;
								}
							}
							else
								tutorialFlashAllowed = true;
						}
						if(roundCounter == 1 && !_playerClickedTheTutorialButton)
						{
							_playerClickedTheTutorialButton = true;
							
							for ( var m:int = 0; m < gameBoard.length; m++)
							{
								cell = gameBoard[m];
								
								if(cell.state == 2 && cell.owner.playerTeam != currentPlayer.playerTeam)
									cell.createWhiteFlash();
							}
							
							playerArray[0].cellSelectorArray[2].createWhiteFlash(true);
						}
					}
					else if (tutorial_05)
					{
						if (roundCounter == 0)
						{
							if (whatCellSelector.type == 3 && !_playerClickedTheTutorialButton)
							{
								_playerClickedTheTutorialButton = true;
								
								for ( var n:int = 0; n < gameBoard.length; n++)
								{
									cell = gameBoard[n];
									
									if(cell.state == 3 && cell.owner.playerTeam != currentPlayer.playerTeam)
										cell.createWhiteFlash();
								}
								
								tutorial.nextPage();
								
								currentPlayer.cellSelectorArray[3].removeWhiteFlash();
								
								if(arrow)
									arrow.visible = false;
							}
						}
					}
					
					if (allowClick)
					{
						currentCellTypeSelected = whatCellSelector;
						currentCellTypeSelected.alpha = 1.0;
						
						tileSelector.visible = true;
						
						// if iphone - adjust selector position
						if (Main.isPhone())
						//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
							tileSelector.x = currentCellTypeSelected.x + (currentCellTypeSelected.display.x * 1.3);
						else
							tileSelector.x = currentCellTypeSelected.x + currentCellTypeSelected.display.x;
						
						tileSelector.y = currentCellTypeSelected.y;
						
						if (currentPlayer)
						{
							for (var i:int=0; i<currentPlayer.cellSelectorArray.length; i++)
							{
								//Deactivate other selectors.
								if (currentPlayer.cellSelectorArray[i] != currentCellTypeSelected)
									currentPlayer.cellSelectorArray[i].alpha = OTHER_SELECTORS_ALPHA;
							}
						}
					}
					else
						tileSelector.visible = false;	
				}
			}
		}
		public function previewMove(cell:Cell):void
		{
			closestCell = cell;
			closestCell.currentPlayer = currentPlayer;
			closestCell.selectedCellType = currentCellTypeSelected;
			
			cellSpaceHasBeenClicked(closestCell)
		}
		
		/*
		public function makeCrosshairs(cell:Cell):void
		{
			trace("making crosshairs - cell dim = " + _cellWidth + " by " + _cellHeight);
			
			if (!_horizontalCrosshair)
			{
				if (_mapSize == 0)
					_horizontalCrosshair = new Quad((tileHolder.width * 0.925), _cellHeight, Color.WHITE);
				else if (_mapSize == 1)
					_horizontalCrosshair = new Quad((tileHolder.width * 1.15), _cellHeight, Color.WHITE);
				else if (_mapSize == 2)
					_horizontalCrosshair = new Quad((tileHolder.width * 0.925), _cellHeight, Color.WHITE);
				_horizontalCrosshair.pivotX = _horizontalCrosshair.width / 2;
				_horizontalCrosshair.pivotY = _horizontalCrosshair.height / 2;
				tileHolder.addChild(_horizontalCrosshair);
				_horizontalCrosshair.alpha = 0.5;
			}
			else
				_horizontalCrosshair.visible = true;
			
			if (_mapSize == 0)
				_horizontalCrosshair.x = tileHolder.x - (_cellWidth * 3.1);
			else if (_mapSize == 1)
				_horizontalCrosshair.x = tileHolder.x - (_cellWidth * 5.9);
			else if (_mapSize == 2)
				_horizontalCrosshair.x = tileHolder.x;
			_horizontalCrosshair.y = cell.y;
			
			if (!_verticalCrosshair)
			{
				_verticalCrosshair = new Quad(_cellWidth, tileHolder.height, Color.WHITE);
				_verticalCrosshair.pivotX = _verticalCrosshair.width / 2;
				_verticalCrosshair.pivotY = _verticalCrosshair.height / 2;
				tileHolder.addChild(_verticalCrosshair);
				_verticalCrosshair.alpha = 0.5;
			}
			else
				_verticalCrosshair.visible = true;
			
			_verticalCrosshair.x = cell.x;
			_verticalCrosshair.y = tileHolder.y;
		}
		
		public function removeCrosshairs():void
		{
			_horizontalCrosshair.visible = false;
			_verticalCrosshair.visible = false;
		}
		*/
		private function makeMove($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if(touch)
			{
				for (var j:int = 0; j < virtualGameBoard.length; j++)
				{
					//Clone State
					virtualGameBoard[j].state = gameBoard[j].state;
					virtualGameBoard[j].visible = false;
				}
				
				cellSpaceHasBeenClicked(closestCell);
				confirmButton.visible = false;
			}
		}
		public function cellSpaceHasBeenClicked(WhatCell:Cell):void
		{	
			// if the player is not AI
			if (!currentPlayer.getAiControl())
			{
				//Make sure the tile is not already flipping.
				if (!moveUnderway && _zoomPanDelay >= ZOOM_PAN_DELAY_TIME)
				{
					var allowMove:Boolean = true;
					
					// check for tutorial clicks
					if (tutorial_01)
					{
						if(roundCounter == 0 && !hasPressedUndo)
						{
							for ( var m:int = 0; m < gameBoard.length; m++)
							{
								var cell:Cell = gameBoard[m];
								cell.removeWhiteFlash();
							}
						}
						if (roundCounter == 1 && !hasPressedUndo)
						{
							if (WhatCell != playerCell)
							{
								allowMove = false;
								tutorialScreenFlashCount = 0;
								tutorialScreenAlpha = 1.0;
								tutorialScreenDir = -1;
								addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
							}
							else
							{
								arrow.visible = false;
								arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
							}
						}
					}
					else if (tutorial_02)
					{
						if (roundCounter == 0 && !hasPressedUndo)
						{
							if (currentCellTypeSelected.type != 1)
							{
								allowMove = false;
								tutorialScreenFlashCount = 0;
								tutorialScreenAlpha = 1.0;
								tutorialScreenDir = -1;
								addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
							}
							else
							{
								if (WhatCell.location != 75 && WhatCell.location != 76 && WhatCell.location != 77)
								{
									allowMove = false;
									tutorialScreenFlashCount = 0;
									tutorialScreenAlpha = 1.0;
									tutorialScreenDir = -1;
									addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
								}
								else
								{
									arrow.visible = false;
									arrow2.visible = false;
									arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
								}
							}
						}
					}
					else if (tutorial_03)
					{
						if (!undidTurnInCampaign)
						{
							//trace("03");
							if (roundCounter == 0 && !hasPressedUndo)
							{
								//trace("round 0 - type = " + currentCellTypeSelected.type);
								if (currentCellTypeSelected.type != 2)
								{
									allowMove = false;
									tutorialScreenFlashCount = 0;
									tutorialScreenAlpha = 1.0;
									tutorialScreenDir = -1;
									addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
								}
								else
								{
									if (WhatCell.location != 76 && WhatCell.location != 77 && WhatCell.location != 90 && WhatCell.location != 91 && WhatCell.location != 102 && WhatCell.location != 103 && WhatCell.location != 104 && WhatCell.location != 105 && WhatCell.location != 106 && WhatCell.location != 107 )
									{
										allowMove = false;
										tutorialScreenFlashCount = 0;
										tutorialScreenAlpha = 1.0;
										tutorialScreenDir = -1;
										addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
									}
									else
									{
										arrow.visible = false;
										arrow2.visible = false;
										arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
									}
								}
							}
						}
					}
					else if (tutorial_04)
					{
						if (roundCounter == 0 && !hasPressedUndo)
						{
							if (currentCellTypeSelected.type != 1)
							{
								allowMove = false;
								tutorialScreenFlashCount = 0;
								tutorialScreenAlpha = 1.0;
								tutorialScreenDir = -1;
								addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
							}
							else
							{
								if (WhatCell.location != 47 && WhatCell.location != 61 && WhatCell.location != 75 && WhatCell.location != 89 && WhatCell.location != 103 && WhatCell.location != 117 && WhatCell.location != 131 && WhatCell.location != 145 && WhatCell.location != 159 && WhatCell.location != 173)
								{
									allowMove = false;
									tutorialScreenFlashCount = 0;
									tutorialScreenAlpha = 1.0;
									tutorialScreenDir = -1;
									addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
								}
								else
								{
									arrow.visible = false;
									arrow2.visible = false;
									arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
								}
							}
						}
						if (roundCounter == 1 && !hasPressedUndo)
						{
							if (currentCellTypeSelected.type != 2)
							{
								allowMove = false;
								tutorialScreenFlashCount = 0;
								tutorialScreenAlpha = 1.0;
								tutorialScreenDir = -1;
								addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
							}
							else
							{
								if (WhatCell.location != 45 && WhatCell.location != 59 && WhatCell.location != 73 && WhatCell.location != 87 && WhatCell.location != 101 && WhatCell.location != 115 && WhatCell.location != 129 && WhatCell.location != 143 && WhatCell.location != 157 && WhatCell.location != 171)
								{
									allowMove = false;
									tutorialScreenFlashCount = 0;
									tutorialScreenAlpha = 1.0;
									tutorialScreenDir = -1;
									addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
								}
								else
								{
									arrow.visible = false;
									arrow2.visible = false;
									arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
								}
							}
						}
					}
					else if (tutorial_05)
					{
						if (roundCounter == 0 && !hasPressedUndo)
						{
							if (currentCellTypeSelected.type != 3)
							{
								allowMove = false;
								tutorialScreenFlashCount = 0;
								tutorialScreenAlpha = 1.0;
								tutorialScreenDir = -1;
								addEventListener(Event.ENTER_FRAME, tutorialScreenFlash);
							}
						}
					}
					
					if (allowMove)
					{
						if (!_moveHasBeenTaken)
							_moveHasBeenTaken = true;
						
						//Set the current player/selected cell type for the cell
						WhatCell.currentPlayer = currentPlayer;
						WhatCell.selectedCellType = currentCellTypeSelected;
						
						WhatCell.changeState();
						lastClickedCell = WhatCell;
						moveUnderway = true;
						closestCell = null;
						
						if(tutorial_01 && roundCounter == 0)
							playerCell = lastClickedCell;
					}
				}
			}
		}
		public function shiftStack(cell:Cell):void
		{
			tileHolder.setChildIndex(cell, tileHolder.numChildren -1);
		}
		private function update($e:Event):void
		{
			CurrentFrameCount++;
			
			if (_zoomPanDelay < ZOOM_PAN_DELAY_TIME)
				_zoomPanDelay++;
			
			//Always keep the clock running so it displays the correct time even if the player turns it on mid-game.
			if (CurrentFrameCount >= 30)
			{
				secondCounter++;
				if(timer)
				{
					if(timerCounter > 0)
						timerCounter--;
					else
					{
						skipped = true;
						changePlayers();
					}
					
					if(timerCounter < 10)
						clock.text = "0:0" + timerCounter.toString();
					else
						clock.text = "0:" + timerCounter.toString();
				}
				CurrentFrameCount = 0;
			}
			
			if (secondCounter >= 60)
			{
				minuteCounter++
					secondCounter = 0;
			}
			
			if(moveUnderway)
			{
				if (lastClickedCell.notAValidMove)
				{
					lastClickedCell.notAValidMove = false;
					lastClickedCell.flipping = false;
					lastClickedCell.hasAlreadySpilledOver = false;
					moveUnderway = false;
				}
				else
				{
					var moveComplete:Boolean = true;
					
					for each (var i:Cell in gameBoard)
					{
						if (i.flipping == true)
							moveComplete = false;
					}
					
					if (moveComplete)
					{	
						moveUnderway = false;
						changePlayers();
					}
				}
				
				// this method was an attempt at updating the percent bar while the player's cells are cascading
				
				// update player percent bars
				var playerCounter:int = 0;
				for each(var player:Player in playerArray)
				{
					// adjust bar widths
					if (player.percentBar.width < player.percentBarWidth)
					{
						player.percentBar.width += percentBarRate;
						if (player.percentBar.width >= player.percentBarWidth)
							player.percentBar.width = player.percentBarWidth;
					}
					else if (player.percentBar.width > player.percentBarWidth)
					{
						player.percentBar.width -= percentBarRate;
						if (player.percentBar.width <= player.percentBarWidth)
							player.percentBar.width = player.percentBarWidth;
					}
					
					//trace("player " + player.teamName + " percentbarof3 = " + player.percentBarOfTierThree.width + " playerbarof3 = " + player.percentBarOfTierThreeWidth);
					
					if (player.percentBarOfTierThree.width < player.percentBarOfTierThreeWidth)
					{
						player.percentBarOfTierThree.width += percentBarRate;
						if (player.percentBarOfTierThree.width >= player.percentBarOfTierThreeWidth)
							player.percentBarOfTierThree.width = player.percentBarOfTierThreeWidth;
					}
					else if (player.percentBarOfTierThree.width > player.percentBarOfTierThreeWidth)
					{
						player.percentBarOfTierThree.width -= percentBarRate;
						if (player.percentBarOfTierThree.width <= player.percentBarOfTierThreeWidth)
							player.percentBarOfTierThree.width = player.percentBarOfTierThreeWidth;
					}
					
					// adjust bar locations
					if (playerCounter == 0)
						player.percentBar.x = -((_percentBar.width/2) - 1);
					else
						player.percentBar.x = playerArray[playerCounter - 1].percentBar.x + playerArray[playerCounter - 1].percentBar.width;
					player.percentBarOfTierThree.x = player.percentBar.x;
					
					playerCounter++;
				}
			}
			
			if (victoryCell)
			{
				if (victoryCell.alpha < 1.0)
				{
					victoryCellFrameCount++;
					
					if (victoryCellFrameCount > 30)
					{
						victoryCell.alpha += 0.025;
						if (victoryCell.alpha >= 1.0)
							victoryCell.alpha = 1.0;
					}
				}
			}
		}
		private function changePlayers():void
		{
			//trace("***********************************************");
			
			var cell:Cell;
			var erosionZone:ErosionZone;
			var captureZone:CaptureZone
			var i:int;
			
			horizontalBar.visible = false;
			verticalBar.visible = false;
			
			closestCell = null;
			
			SoundController.resetSoundCounters();
			
			//Check to see if the game is over
			checkEndGame();
			resetTiles();
			//currentPlayer.darkenAllSelectedCells();
			
			if(!skipped)
			{
				currentPlayer.cellSelectorArray[1].removeWhiteFlash();
				currentPlayer.cellSelectorArray[2].removeWhiteFlash();
				currentPlayer.cellSelectorArray[3].removeWhiteFlash();
				
				//Reset the Special cell selector if used
				if (currentCellTypeSelected.type == 1)
					currentPlayer.cellSelectorArray[1].resetSelectedTile();
				if (currentCellTypeSelected.type == 2)
					currentPlayer.cellSelectorArray[2].resetSelectedTile();
				if (currentCellTypeSelected.type == 3)
					currentPlayer.cellSelectorArray[3].resetSelectedTile();
			}
			
			if (currentPlayerNum == numOfPlayers - 1)
				spreadInfection();
			
			if (!_replay)
			{
				
				//Have each Selected Tile remember their state.
				for each (var player:Player in playerArray)
				{
					//trace("player " + player.teamName);
					for each (var selector:CellSelector in player.cellSelectorArray)
					{
						selector.rememberState();
					}
				}
				
				//Pushing Game State to History
				var currentGameBoard:Array = [];
				var currentGameBoardOwnership:Array = [];
				
				//remember every cell & who owns them
				for each (cell in gameBoard)
				{
					currentGameBoard.push(cell.state);
					
					if (cell.owner != null)
						currentGameBoardOwnership.push(cell.owner.playerTeam);
					else
						currentGameBoardOwnership.push(5);
				}
				
				playLog.push(currentGameBoard);
				playLogOwnership.push(currentGameBoardOwnership);
				
				//Have the player record how many tiles they had that turn.
				currentPlayer.historyArrayOfTiles.push(currentPlayer.totalNumOfTiles);
				
				var cellLocation:int;
				
				if (skipped)
					cellLocation = 0;
				else
					cellLocation = lastClickedCell.location;
				
				//Create a record of the turn that holds a variety of differnet information needed for the _replay.
				var newTurn:TurnRecord = new TurnRecord(currentCellTypeSelected.type, cellLocation, currentPlayerNum, 0);
				turnList.push(newTurn);
			}
			
			//Hide tutorial screen at the end of every turn.
			if (tutorial && tutorial.complete)
				tutorial.visible = false;
			
			//Check to see if a zone can be claimed early.
			if (captureZoneMode)
			{
				for (i = captureZoneList.length -1; i >= 0; i--)
				{
					captureZone = captureZoneList[i];
					
					if(captureZone.active && !captureZone.claimed)
					{
						//Check to see if all cells are owned by the same player.
						if (captureZone.checkAllCells(currentPlayer.playerTeam))
						{
							captureZone.turnClaimed = playLog.length-1;
							//remove capture zone from the list.
							captureZoneList.splice(i,1);
							
							//If moving CZ mode & there are still CZ, activate another zone.
							if (movingCaptureZone && captureZoneList.length > 0)
							{
								currentClaimCount = 0;
								pickActiveCaptureZone();
							}
						}
					}
				}
				
				if (captureZoneList.length <= 0)
				{
					claimText.visible = false;
					_claimTextExtension.visible = false;
				}
			}
			
			//Make the capture zones claim when they were supposed to.
			if(captureZoneMode && _replay)
			{
				for each (captureZone in fullCaptureZoneList)
				{
					//trace("Capture Zone Claimed: " + captureZone.claimed);
					//trace("Capture Zone was calimed on " + captureZone.turnClaimed);
					//trace("Current Turn Number: " + currentTurn.toString());
					
					if(captureZone.turnClaimed == currentTurn && captureZone.claimed)
						captureZone.promoteAllCells();
				}
			}
			
			//Increment to next player
			currentPlayerNum++
			//If all player have taken a turn, end the round, begin another.
			if (currentPlayerNum >= numOfPlayers)
			{
				currentPlayerNum = 0;
				roundCounter++;
				
				//increment the current number of turns until Capture Zones claim themselves
				if(captureZoneMode && (captureZoneList.length > 0) && !_replay)
				{
					currentClaimCount++;
					updateClaimText()
					
					//After 6 turns have passed, show a pop-up warning that the Zones will claim soon.
					if(currentClaimCount > 6 && currentClaimCount < 10)
						addCaptureZoneScreen();
					
					//10 Rounds have passed, claim all Capture Zones
					if(currentClaimCount == claimNum)
					{
						//trace("Claim Count has been reached, all active zones are being Claimed");
						
						for (i = captureZoneList.length -1; i >= 0; i--)
						{
							captureZone = captureZoneList[i];
							
							if(captureZone.active && !captureZone.claimed)
							{
								/*trace("Claiming Capture Zone");
								trace("-----------------");*/
								
								captureZone.promoteAllCells();
								captureZone.turnClaimed = playLog.length-1;
								//remove capture zone from the list.
								captureZoneList.splice(i,1);
							}
							
						}
						//If moving CZ mode & there are still CZ, activate another zone.
						if (movingCaptureZone && captureZoneList.length > 0)
						{
							currentClaimCount = 0;
							pickActiveCaptureZone();
						}
					}
				}
				//increment the current number of turns until erosion zones decay.
				if (erosionZoneMode && !currentlyActiveErosionZone.decayed && !_replay)
					currentlyActiveErosionZone.incrementDecay(playLog.length-1);
				
				if(lightning)
				{
					//Count down every round
					if(numOfLightningRounds > 0)
					{
						numOfLightningRounds--;
						lightningCounter.text = numOfLightningRounds.toString();
					}
					
					//End the game
					if(numOfLightningRounds == 0)
						checkEndGame();
				}
				
				if(tutorial)
					updateTutorialMessages();
			}
			
			if(erosionZoneMode)
			{
				for each (erosionZone in fullErosionZoneList)
				{
					if(erosionZone.decayed)
					{
						/*trace("-------------------");
						trace("Erosion Claimed: " + erosionZone.decayed);
						trace("Erosion Zone " + erosionZone.identifier.toString() + " was claimed on turn " + erosionZone.turnDecayed);
						trace("Current Turn Number: " + roundCounter);*/
						
						
						if(erosionZone.turnDecayed == currentTurn+1 && _replay)
							erosionZone.decayCells();
						
						if(erosionZone.turnDecayed == playLog.length-1 && !_replay)
							erosionZone.decayCells();
						
						//trace("-------------------");
					}
				}
			}
			
			//Assign Current Player
			currentPlayer = playerArray[currentPlayerNum];
			boardOutline.changeColors(currentPlayer.playerHexColor);
			
			// modify selector sizes and places
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				for (i=0; i<playerArray.length; i++)
				{
					playerArray[i].revertSelectors();
				}
				
				if (!currentPlayer.getAiControl())
				{
					currentPlayer.increaseSelectorSizes();
				}
			}
			
			cellSelectorHasBeenClicked(currentPlayer.cellSelectorArray[0]);
			
			//Let all SelectedTiles know that their players turn has begun.
			for each (var cellSelector:CellSelector in currentPlayer.cellSelectorArray)
			{
				if (cellSelector.countDownIndicator != null)
				{
					cellSelector.incrementLvl();
					if (cellSelector.type > 0)
						cellSelector.alignCells();
				}
			}

			//Show the tile Selector if the next player is human
			if (!currentPlayer.getAiControl() && tileSelector.visible == true)
				tileSelector.visible = true;
			
			//Reset timer variables if playing a TIMED game type.
			if (timer)
			{
				skipped = false;
				timerCounter = timerThreshold;
				clock.text = "0:" + timerCounter.toString();
			}
			
			//Put in a manual pause between turns to slow it down.
			if (playReplay)
			{
				if (turnList[currentTurn].type == 0)
				{
					currentTurn++;
					Starling.juggler.delayCall(delayedTurn, 0.3);
				}
				else	
				{
					currentTurn++;
					setTimeLine();
				}
			}
			
			//Check to see if the spreading caused by the infected cells would end the game
			if(infection)
				checkEndGame();
			
			if(aiOn)
				checkEndGame();
			
			// set ai board
			if (aiOn && !playReplay && !EndOfGame && !success && !defeat)
			{
				if (currentPlayer.getAiControl())
				{
					ai.copyBoard(currentPlayer);
					ai.triggerAIMove();
				}
			}
			
			// set board cell states if animations off
			if (!Main.sAnimationsOn)
			{
				for (i=0; i<gameBoard.length; i++)
				{
					gameBoard[i].setColor();
					gameBoard[i].addDisplayForNoAnimations();
				}
			}
			
			//trace("--------------------------------------------------------------");
		}
		public function retryAIMove():void
		{
			if (aiOn && !playReplay && !EndOfGame && !success && !defeat)
			{
				if (currentPlayer.getAiControl())
				{
					ai.copyBoard(currentPlayer);
					ai.triggerAIMove();
				}
			}
		}
		private function spreadInfection():void
		{
			var cell:Cell;
			
			if (infection)
			{
				//Have every cell spread if they can
				for each (cell in gameBoard)
				{
					if (cell.owner != null && cell.infected == false && (cell.state == 1 || cell.state == 2))
						cell.spreadInfection();
					
				}
				
				//Reset infection bools for next turn.
				for each (cell in gameBoard)
				{
					if (cell.infected == true)
						cell.infected = false;
				}
			}
			
			checkEndGame();
		}
		private function delayedTurn():void
		{
			setTimeLine();
		}
		private function updateTutorialMessages():void
		{
			var tween:Tween;
			
			if(tutorial.complete)
			{
				tutorial.visible = false;
				arrow.visible = false;
				arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
			}
			if (tutorial_01 && !tutorial.complete)
			{
				tutorial.visible = true;
				tutorial.nextPage();
				
				if (roundCounter == 1)
					playerCell.createWhiteFlash();
				else if(roundCounter == 4 && !hasPressedUndo)
				{
					//Creating an arrow that points at the undo button
					arrow.visible = true;
					arrow.x = _undoButton.x - (arrow.width * 1.5);
					arrow.y = _undoButton.y;
					arrowStartX = _undoButton.x - arrow.width;
					arrowDir = -1;
					arrowMovement = 0.0;
					arrow.addEventListener(Event.ENTER_FRAME, arrowBounce);
					arrow.rotation = deg2rad(180);
					
					//Fade in Undo Button.
					_undoButton.visible = true;
					_undoButton.alpha = 0;
					tween = new Tween(_undoButton, 1);
					tween.fadeTo(1);
					Starling.juggler.add(tween);
					
					undoWhiteFlash = new BW_Selector(.5, .7, null, false, .8, 1);
					addChild(undoWhiteFlash);
					undoWhiteFlash.x = _undoButton.x;
					undoWhiteFlash.y = _undoButton.y;
					undoWhiteFlash.addEventListener(TouchEvent.TOUCH, whiteFlashUndoTurn);
				}
				else if(roundCounter == 5 && !hasPressedUndo)
				{
					if(_percentBar.visible == false)
					{
						arrow.visible = true;
						arrow.x = PathogenTest.stageCenter.x;
						arrow.y = _percentBar.y + (arrow.height * 1.5);
						arrow.rotation = deg2rad(90);
						arrowStartY = _percentBar.y + arrow.height;
						arrowDir = -2;
						arrowMovement = 0.0;
						arrow.addEventListener(Event.ENTER_FRAME, arrowBounce);
						
						_percentBar.visible = true;
						_percentBar.alpha = 0;
						
						modifyPercentBar();
						
						var tween02:Tween = new Tween(_percentBar, 1);
						tween02.fadeTo(1);
						Starling.juggler.add(tween02);
						
						undoWhiteFlash.visible = false;
					}
				}
				else
				{
					arrow.visible = false;
					arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
				}
			}
			else if (tutorial_02 && !tutorial.complete)
			{
				tutorial.visible = true;
				tutorial.nextPage();
				
				if (roundCounter == 2)
				{
					arrow.visible = false;
					arrow2.visible = false;
					arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
				}
				if (roundCounter == 2 || roundCounter == 1)
				{
					arrow.visible = true;
					arrow.x = playerArray[0].cellSelectorArray[1].x + ((5 * playerArray[0].cellSelectorArray[1].width) / 8);
					arrow.y = currentPlayer.cellSelectorArray[1].y;
					arrowStartX = arrow.x;
					arrowDir = 1;
					arrowMovement = 0.0;
					arrow2Movement = 0.0;
					arrow.addEventListener(Event.ENTER_FRAME, arrowBounce);
				}
				if(roundCounter == 3)
				{
					arrow.visible = false;
					arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
				}
			}
			else if (tutorial_03 && !tutorial.complete)
			{
				tutorial.visible = true;
				tutorial.nextPage();
				
				if (roundCounter == 4)
					arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
			}
			else if (tutorial_04 && !tutorial.complete)
			{
				_playerClickedTheTutorialButton = false;
				
				tutorial.visible = true;
				tutorial.nextPage();
				
				if(roundCounter == 1)
				{
					arrow.visible = true;
					arrow.y = currentPlayer.cellSelectorArray[2].y;
					arrowStartX = arrow.x;
					arrowDir = 1;
					arrowMovement = 0.0;
					arrow.addEventListener(Event.ENTER_FRAME, arrowBounce);
				}
			}
			else if (tutorial_05 && !tutorial.complete)
			{
				tutorial.visible = true;
				tutorial.nextPage();
			}
			else if (tutorial_CaptureZone && !tutorial.complete)
			{
				tutorial.visible = true;
				tutorial.nextPage();
			}
			else if (tutorial_ErosionMode && !tutorial.complete)
			{
				tutorial.visible = true;
				tutorial.nextPage();
			}
			else if (tutorial_InfectionMode && !tutorial.complete)
			{
				tutorial.visible = true;
				tutorial.nextPage();
			}
		}
		private function whiteFlashUndoTurn($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				undoWhiteFlash.removeEventListener(TouchEvent.TOUCH, whiteFlashUndoTurn);
				removeChild(undoWhiteFlash);
				
				onTouchUndo($e)
				
				if(_percentBar.visible == false)
				{
					tutorial.nextPage();
					
					arrow.visible = true;
					arrow.x = PathogenTest.stageCenter.x;
					arrow.y = _percentBar.y + (arrow.height * 1.5);
					arrow.rotation = deg2rad(90);
					arrowStartY = _percentBar.y + arrow.height;
					arrowDir = -2;
					arrowMovement = 0.0;
					arrow.addEventListener(Event.ENTER_FRAME, arrowBounce);
					
					_percentBar.visible = true;
					_percentBar.alpha = 0;
					
					modifyPercentBar();
					
					var tween:Tween = new Tween(_percentBar, 1);
					tween.fadeTo(1);
					Starling.juggler.add(tween);
				}
			}
		}
		private function checkEndGame():void
		{
			//Assume the game is over
			var boundaryTileCounter:int = 0;
			var currentPlayer:Player;
			
			EndOfGame = true;
			
			//Tell each player to reset their counts to 0.
			for each (currentPlayer in playerArray)
			{
				currentPlayer.resetEndGameTileCount();
			}
			
			for (var i:int = 0; i < gameBoard.length; i++)
			{
				var currentTile:Cell = gameBoard[i];
				
				currentTile.previousOwner = currentTile.owner;
				
				//If even a single space is empty
				if (currentTile.state == 0)
				{
					EndOfGame = false;
				}
					//Else, check state of the Cell
				else
				{
					//Dot cells
					if (currentTile.state == 1)
					{
						for each (currentPlayer in playerArray)
						{	
							if (currentTile.owner.playerTeam == currentPlayer.playerTeam)
							{
								currentPlayer.numOfTriangleTiles++;
								break;
							}
						}
					}
					//Diamond Tiles
					if (currentTile.state == 2)
					{
						for each (currentPlayer in playerArray)
						{
							if (currentTile.owner.playerTeam == currentPlayer.playerTeam)
							{
								currentPlayer.numOfDiamondTiles++;
								break;
							}
						}
					}
					//Star Tiles
					if ( currentTile.state == 3)
					{
						for each (currentPlayer in playerArray)
						{
							if (currentTile.owner.playerTeam == currentPlayer.playerTeam)
							{
								currentPlayer.numOfStarTiles++;
								break;
							}
						}
					}
					//Solid Tiles
					if (currentTile.state == 4)
					{
						for each (currentPlayer in playerArray)
						{
							if (currentTile.owner.playerTeam == currentPlayer.playerTeam)
							{
								currentPlayer.numOfSolidTiles++;
								break;
							}
						}
					}
					
					//How many tiles are boundary tiles on this map?
					if (currentTile.state == 5 || currentTile.state == 6 || currentTile.state == 9 || currentTile.state == 10)
					{
						boundaryTileCounter++;
					}
				}
			}
			
			//Pie Chart Stuff
			totalNumOfPlayableTiles = (gameBoardHeight * gameBoardWidth) - boundaryTileCounter;
			
			//Determine Player Percentage.
			for each (currentPlayer in playerArray)
			{
				currentPlayer.totalNumOfTiles = currentPlayer.numOfTriangleTiles + currentPlayer.numOfDiamondTiles + currentPlayer.numOfStarTiles + currentPlayer.numOfSolidTiles;
				currentPlayer.playerPercent = ( currentPlayer.totalNumOfTiles / totalNumOfPlayableTiles) * 100;
				//trace("player + " + currentPlayer.teamName + " totalNumOfTiles = " + currentPlayer.totalNumOfTiles + " playerPercent = " + currentPlayer.playerPercent);
				currentPlayer.arrayOfTileCounts = [currentPlayer.numOfTriangleTiles , currentPlayer.numOfDiamondTiles , currentPlayer.numOfStarTiles , currentPlayer.numOfSolidTiles, currentPlayer.totalNumOfTiles];
				
				//Triangles
				if (currentPlayer.numOfTriangleTiles > currentPlayer.previousNumOfTriangleTiles)
					currentPlayer.totalNumOfTriangleTiles += (currentPlayer.numOfTriangleTiles - currentPlayer.previousNumOfTriangleTiles);
				//Diamonds
				if (currentPlayer.numOfDiamondTiles > currentPlayer.previousNumOfDiamondTiles)
					currentPlayer.totalNumOfDiamondTiles += (currentPlayer.numOfDiamondTiles - currentPlayer.previousNumOfDiamondTiles);
				//Stars
				if (currentPlayer.numOfStarTiles > currentPlayer.previousNumOfStarTiles)
					currentPlayer.totalNumOfStarTiles += (currentPlayer.numOfStarTiles - currentPlayer.previousNumOfStarTiles);
				//Solids
				if (currentPlayer.numOfSolidTiles > currentPlayer.previousNumOfSolidTiles)
					currentPlayer.totalNumOfSolidTiles += (currentPlayer.numOfSolidTiles - currentPlayer.previousNumOfSolidTiles);
				
				//trace("_moveHasBeenTaken = " + _moveHasBeenTaken + " current player team = " + currentPlayer.teamName + " ai = " + currentPlayer.getAiControl() + " prev = " + currentPlayer.previousTotalNumOfTiles + " total = " + currentPlayer.totalNumOfTiles);
				
				if (_moveHasBeenTaken)
				{
					// check for creating achievement
					if (currentPlayer.previousTotalNumOfTiles != currentPlayer.totalNumOfTiles)
					{
						var difference:Number = currentPlayer.totalNumOfTiles - currentPlayer.previousTotalNumOfTiles;
	
						// check for creating achievement - created cells
						if(difference > 0 && !currentPlayer.getAiControl())
						{
							Main.achievements.updatePlayerStatistics(Achievements.CREATING, difference);
						}
					}
					
					// check for destroying achievement
					if (!currentPlayer.getAiControl())
					{
						//trace("Checking for destruction");
						
						var numCellsDestroyed:int = 0;
						
						for (var j:int=0; j<playerArray.length; j++)
						{
							if (playerArray[j].playerTeam != currentPlayer.playerTeam)
							{
								var totalCells:int = playerArray[j].numOfTriangleTiles + playerArray[j].numOfDiamondTiles + playerArray[j].numOfStarTiles + playerArray[j].numOfSolidTiles;
								if (totalCells < playerArray[j].previousTotalNumOfTiles)
								{
									//trace("Total Cells: " + totalCells);
									//trace("Previous Num of Total Tile: " + playerArray[j].previousTotalNumOfTiles);
									numCellsDestroyed += playerArray[j].previousTotalNumOfTiles - totalCells;
								}
							}
						}
						
						//trace("Current Cell Type Selected: " + currentCellTypeSelected.type);
						//trace("num Cells Destoyed: " + numCellsDestroyed);
						
						if(numCellsDestroyed > 0 && currentCellTypeSelected.type == 3)
						{
							//trace("Cells where destroyed");
							Main.achievements.updatePlayerStatistics(Achievements.DESTROYING, Math.abs(numCellsDestroyed));
							currentPlayer.totalCellsDestroyed += Math.abs(numCellsDestroyed);
						}
					}
				}
				
				//Setting previous
				currentPlayer.previousNumOfTriangleTiles = currentPlayer.numOfTriangleTiles;
				currentPlayer.previousNumOfDiamondTiles = currentPlayer.numOfDiamondTiles;
				currentPlayer.previousNumOfStarTiles = currentPlayer.numOfStarTiles;
				currentPlayer.previousNumOfSolidTiles = currentPlayer.numOfSolidTiles;
				currentPlayer.previousTotalNumOfTiles = currentPlayer.totalNumOfTiles;
				
				if(currentPlayer.numOfSolidTiles >= Math.round(totalNumOfPlayableTiles /2))
					_domination = true;
			}
			
			//Update the bar every turn if its turned on.
			if (_percentBar.visible)
				modifyPercentBar();
			
			//If the game has really ended, show the end screen.
			if (EndOfGame || forfiet || (lightning && (numOfLightningRounds <= 0)) || _domination)
			{
				if (_zoomButton)
				{
					_zoomButton.visible = false;
				}
				
				if (_campaign)
				{
					var playerPercentArray:Array = [];
					var player:Player;
					
					for each (player in playerArray)
					{
						playerPercentArray.push(player.playerPercent);
					}
					
					var highestPercent:int = Math.max.apply(null, playerPercentArray);
					
					if (playerArray[0].playerPercent == highestPercent || playerArray[0].playerPercent >= highestPercent)
						addVictoryScreen();
					else
						addDefeatScreen();
				}
				else
					addVictoryScreen();
			}
		}
		private function addDefeatScreen():void
		{
			defeat = true;
			
			if(_defeatScreen == null)
			{
				_defeatScreen = new DefeatScreen(this);
				addChild(_defeatScreen);
				_defeatScreen.x = PathogenTest.stageCenter.x;
				_defeatScreen.y = PathogenTest.stageCenter.y;
			}
		}
		public function removeDefeatScreen():void
		{
			_defeatScreen.dispose();
			removeChild(_defeatScreen);
			_defeatScreen = null;
		}
		private function addVictoryScreen():void
		{
			success = true;
			
			if(_victoryScreen == null)
			{
				trace("Adding Victory Screen")
				
				_victoryScreen = new VictoryScreen(this);
				addChild(_victoryScreen);
			}
		}
		public function removeVictoryScreen():void	
		{
			if(_victoryScreen)
			{
				_victoryScreen.dispose();
				removeChild(_victoryScreen);
				_victoryScreen = null;
				Main.achievements.victoryScreen = null;
			}
		}	
		public function addPostGameScreen():void
		{
			hideUI();
			
			if (postGame == null)
				postGame = new PostGame(this);
			
			addChild(postGame);
		}
		public function addReplayBar():void
		{
			removeChild(postGame);
			revealUI();
			
			//reseting victory conditions
			forfiet = false;
			_replay = true;
			_domination = false;
			Main.sAnimationsOn = true;
			
			// create play/pause button
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				playPauseButton = new BW_Button(0.7, 1.0, "", 0);
			else
				playPauseButton = new BW_Button(0.5, 0.7, "", 0);
			playPauseButton.addImage(Main.assets.getTexture("Pathogen_BTN_Play"), Main.assets.getTexture("Pathogen_BTN_Pause"));
			playPauseButton._toggle = true;
			addChild(playPauseButton)
			playPauseButton.x = PathogenTest.stageCenter.x;
			playPauseButton.y = rulesButton.y;
			playPauseButton.addEventListener(TouchEvent.TOUCH, playPauseReplay);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				stepForwardButton = new BW_Button(0.7, 1.0, "", 10);
			else
				stepForwardButton = new BW_Button(0.5, 0.7, "", 10);
			stepForwardButton.addImage(Main.assets.getTexture("StepBTN_Off"), Main.assets.getTexture("StepBTN_On"));
			addChild(stepForwardButton)
			stepForwardButton.x = playPauseButton.x + (stepForwardButton.width * 1.2)
			stepForwardButton.y = rulesButton.y;
			stepForwardButton.addEventListener(TouchEvent.TOUCH, stepForward);
			stepForwardButton.rotation = deg2rad(180);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				stepBackwardButton = new BW_Button(0.7, 1.0, "", 10);
			else
				stepBackwardButton = new BW_Button(0.5, 0.7, "", 10);
			stepBackwardButton.addImage(Main.assets.getTexture("StepBTN_Off"), Main.assets.getTexture("StepBTN_On"));
			addChild(stepBackwardButton)
			stepBackwardButton.x = playPauseButton.x - (stepForwardButton.width * 1.2)
			stepBackwardButton.y = rulesButton.y;
			stepBackwardButton.addEventListener(TouchEvent.TOUCH, stepBackward);
			
			replayPause = true;
			
			//Set the current moment in time.
			currentTurn = turnList.length;
		}
		private function stepForward($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && !moveUnderway && replayPause)
			{
				currentTurn++;
				resetBoardInTime();
			}
		}
		private function stepBackward($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && !moveUnderway && replayPause)
			{
				currentTurn--;
				resetBoardInTime();
			}
		}
		private function playPauseReplay($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				//If the _replay is paused, begin stepping forward from the current moment in time.
				if (replayPause)
				{
					if (currentTurn == (turnList.length - 1))
						currentTurn = 0;
					
					playReplay = true;
					replayPause = false;
					resetBoardInTime();
					setTimeLine();
					
				}
				else
				{
					//Pause the _replay
					replayPause = true;
					playReplay = false;
				}
			}
		}
		private function resetBoardInTime():void
		{
			trace("Reset Board in Time");
			
			if (currentTurn >= turnList.length || currentTurn < 0)
				currentTurn = 0;
			
			//Reset all cells on the board to what they were at the current location in time.
			for ( var i:int = 0; i < gameBoard.length; i++)
			{
				var currentTile:Cell = gameBoard[i];
				
				var previousTileState:int = playLog[currentTurn][i];
				var previousTileOwner:int = playLogOwnership[currentTurn][i];
				
				gameBoard[i].state = previousTileState;
				//Make sure the tiles are assigned to their correct owners
				if (previousTileOwner == 5)
					gameBoard[i].owner = null;
				else
				{
					var tempOwnerPlayer:Player = new Player(previousTileOwner, playerColors[previousTileOwner], this);
					
					gameBoard[i].owner = tempOwnerPlayer;
					gameBoard[i].currentPlayer = tempOwnerPlayer;
				}
				gameBoard[i].reset();
			}
			
			//Reset Branches
			for (var k:int = 0; k < gameBoard.length; k++)
			{
				gameBoard[k].determineBranches();
			}
			
			var player:Player;
			
			//For each player return their playing pieces back to what they were at the current moment in time.
			for each (player in playerArray) 
			{
				for each (var selector:CellSelector in player.cellSelectorArray)
				{
					selector.restoreState(currentTurn);
				}
			}
			
			//Change the player to be who it was at the current moment in time.
			turn = turnList[currentTurn];
			currentPlayerNum = turn.currentPlayerNum;
			currentPlayer = playerArray[currentPlayerNum];
			currentCellTypeSelected = currentPlayer.cellSelectorArray[turn.type];
			
			// modify selector sizes and places
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				for (i=0; i<playerArray.length; i++)
				{
					playerArray[i].revertSelectors();
				}
				
				if (!currentPlayer.getAiControl())
				{
					currentPlayer.increaseSelectorSizes();
				}
			}
			
			// if iphone - adjust selector position
			if (Main.isPhone())
				tileSelector.x = currentCellTypeSelected.x + (currentCellTypeSelected.display.x * 1.3);
			else
				tileSelector.x = currentCellTypeSelected.x + currentCellTypeSelected.display.x;
			
			tileSelector.y = currentCellTypeSelected.y;
			
			cellSelectorHasBeenClicked(currentPlayer.cellSelectorArray[turn.type]);
			
			var captureZone:CaptureZone;
			
			//Make the capture zones claim when they were supposed to.
			if(captureZoneMode)
			{
				 trace("reseting Board in Time")
				
				if(currentTurn == 0)
				{
					for each (captureZone in fullCaptureZoneList)
					{
						captureZone.activateAllCells();
					}
				}
				
				for each (captureZone in fullCaptureZoneList)
				{
					if(captureZone.turnClaimed == currentTurn && captureZone.claimed)
						captureZone.promoteAllCells();
				}
			}
			//Make the erosions zones decay when they were supposed to.
			if(erosionZoneMode)
			{
				for each (var erosionZone:ErosionZone in fullErosionZoneList)
				{
					if(erosionZone.turnDecayed == currentTurn && erosionZone.decayed)
						erosionZone.decayCells();
				}
			}
			
			//Check to see if they won.
			checkEndGame();
		}
		private function setTimeLine():void
		{
			trace("set timeline");
			
			//If at the end of known time, stop the _replay.
			if (currentTurn >= turnList.length - 1)
				playReplay = false;
			
			//Cannot go back in time where there is no data
			if (turnList.length > 0)
			{
				turn = turnList[currentTurn];
				
				currentPlayerNum = turn.currentPlayerNum;
				currentPlayer = playerArray[currentPlayerNum];
				currentCellTypeSelected = new CellSelector(turn.type, currentPlayer, this);
				
				//If the players turn was skipped in the past, skip it again in the _replay (only used in Lightning).
				if (turn.location == 0)
				{
					skipped = true;
					changePlayers();
				}
				else
				{
					//Otherwise determine what type of cell they placed and where it was played.
					var targetCell:Cell = gameBoard[turn.location];
					
					targetCell.currentPlayer = currentPlayer;
					targetCell.selectedCellType = currentCellTypeSelected;
					targetCell.changeState();
					lastClickedCell = gameBoard[turn.location];
					
					moveUnderway = true;
				}
				
				//Change play button to play
				if (currentTurn >= (turnList.length - 1))
				{
					currentTurn++;
					replayPause = true;
					playReplay = false;
					playPauseButton.toggleUp();
				}
			}
		}
		private function hideUI():void
		{
			//Hide all UI features of the Game
			for each (var player:Player in playerArray)
			{
				player.visible = false;
			}
			
			if(tutorial)
			{
				tutorial.visible = false;
			}
			
			tileSelector.visible = false;
			tileHolder.visible = false;
			optionsButton.visible = false;
			rulesButton.visible = false;
			quitButton.visible = false;
			_undoButton.visible = false;
			_percentBar.visible = false;
			boardOutline.visible = false;
			
			if(undoWhiteFlash)
				undoWhiteFlash.visible = false;
			
			if (timer)
			{
				timer = false;
				clock.visible = false;
			}
			
			if (lightning)
			{
				lightningCounter.visible = false;
				lightningIndicator.visible = false;
			}
		}
		private function revealUI():void
		{
			//Reveal all UI features of the Game
			for each (var player:Player in playerArray)
			{
				player.visible = true;
			}
			
			tileSelector.visible = true;
			tileHolder.visible = true;
			optionsButton.visible = true;
			rulesButton.visible = true;
			quitButton.visible = true;
			_undoButton.visible = false;
			_percentBar.visible = true;
			boardOutline.visible = true;
		}
		private function modifyPercentBar():void
		{
			//Animate the percent bar to make it "grow/shrink" to its new size, not snap.
			var onePercentOfFrame:Number = percentFrameWidth / 100;
			var playerCounter:int = 0;
			var decimalPlaces:Number = Math.pow(10, 0);
			
			for each(var player:Player in playerArray)
			{
				player.percentBarWidth = player.playerPercent * onePercentOfFrame;
				player.percentBarWidth = Math.round(decimalPlaces * player.percentBarWidth) / decimalPlaces;
				
				if (player.numOfSolidTiles > 0)
				{
					player.percentBarOfTierThreeWidth = ((player.numOfSolidTiles / player.totalNumOfTiles) * player.percentBar.width);
					player.percentBarOfTierThreeWidth = Math.round(decimalPlaces * player.percentBarOfTierThreeWidth) / decimalPlaces;
					player.percentBarOfTierThree.visible = true;
				}
				else
				{
					player.percentBarOfTierThreeWidth = 0;
					
				}
				playerCounter++;
			}
			addEventListener(Event.ENTER_FRAME, updatePercentBar);
		}
		private function updatePercentBar(event:Event):void
		{
			var playerCounter:int = 0;
			var done:Boolean = true;
			
			if (_percentBar.visible)
				modifyPercentBar();
			
			//For each player update the width of their own percent bar & realign it with adjacent players.
			for each(var player:Player in playerArray)
			{
				// adjust bar widths
				if (player.percentBar.width < player.percentBarWidth)
				{
					player.percentBar.visible = true;
					player.percentBar.width += percentBarRate;
					if (player.percentBar.width >= player.percentBarWidth)
						player.percentBar.width = player.percentBarWidth;
					else
						done = false;
				}
				else if (player.percentBar.width > player.percentBarWidth)
				{
					player.percentBar.width -= percentBarRate;
					if (player.percentBar.width <= player.percentBarWidth)
						player.percentBar.width = player.percentBarWidth;
					else
						done = false;
				}
				if (player.percentBarOfTierThree.width < player.percentBarOfTierThreeWidth)
				{
					player.percentBarOfTierThree.width += percentBarRate;
					if (player.percentBarOfTierThree.width >= player.percentBarOfTierThreeWidth)
						player.percentBarOfTierThree.width = player.percentBarOfTierThreeWidth;
					else
						done = false;
				}
				else if (player.percentBarOfTierThree.width > player.percentBarOfTierThreeWidth)
				{
					player.percentBarOfTierThree.width -= percentBarRate;
					if (player.percentBarOfTierThree.width <= player.percentBarOfTierThreeWidth)
						player.percentBarOfTierThree.width = player.percentBarOfTierThreeWidth;
					else
						done = false;
				}
				
				// adjust bar locations
				if (playerCounter == 0)
					player.percentBar.x = -((_percentBar.width/2) - 1);
				else
					player.percentBar.x = playerArray[playerCounter - 1].percentBar.x + playerArray[playerCounter - 1].percentBar.width;
				
				player.percentBarOfTierThree.x = player.percentBar.x;
				
				playerCounter++;
			}
			
			if (done)
				removeEventListener(Event.ENTER_FRAME, updatePercentBar);
		}
		private function resetTiles():void
		{
			//All tiles are told its a new turn, so they may once again start their cascade
			for each (var cell:Cell in gameBoard)
			{
				if(cell.hasAlreadySpilledOver)
					cell.hasAlreadySpilledOver = false;
				
				cell.reactionNumber = 0;
				cell.potentialState = 0;
				cell.originalState = cell.state;
				cell.determineBranches();
			}
		}
		public function viewMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				forfiet = false;
				removeChild(postGame);
				revealUI();
			}
		}
		public function Quit():void
		{
			if(main != null)
			{
				garbageCollection();
				main.removeGameManager();
			}
			if(_campaign != null)
			{
				garbageCollection();
				_campaign.removeGameManager();
			}
		}
		private function onTouchUndo($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && !currentPlayer.getAiControl() && !undoing && !moveUnderway && !_replay)
			{
				undoing = true;
				
				Starling.juggler.delayCall(undoTurn, .1);
			}
		}
		private function undoTurn():void
		{
			//How far back in time are you going?
			var howFarBack:int = 2;
			
			//In the _campaign you always undo back to your turn.
			if (_campaign)
				howFarBack = numOfPlayers + 1;
			else
			{
				var undoPlayerNum:int = currentPlayerNum;
				undoPlayerNum--;
				if (undoPlayerNum < 0)
					undoPlayerNum = numOfPlayers - 1;
				while(playerArray[undoPlayerNum].getAiControl())
				{
					howFarBack++;
					undoPlayerNum--;
					if (undoPlayerNum < 0)
						undoPlayerNum = numOfPlayers - 1;
					
					// safe guard so no crashing
					if (howFarBack > 10)
						break;
				}
			}
			//trace("howFarBack = " + howFarBack);
			//You cannot undo a turn if:
			//The game is over
			//You are already trying to undo a move
			//A move it playing
			//Or you are trying to go back in time where there is no data.
			
			if (playLog.length - howFarBack >= 0)
			{
				if (_campaign)
				{
					undidTurnInCampaign = true;
					if (arrow != null)
					{
						arrow.visible = false;
						arrow.removeEventListener(Event.ENTER_FRAME, arrowBounce);
					}
					if (arrow2 != null)
						arrow2.visible = false;
				}
				
				var i:int;
				
				//Go back as far as need be.
				for (var j:int = 0; j < howFarBack-1; j++)
				{
					
					//For every player restore the last state in the selector
					for (i=0; i<playerArray.length; i++)
					{
						for each (var selector:CellSelector in playerArray[i].cellSelectorArray)
						{
							if (selector.getType() > 0)
							{
								var addTile:Boolean = false;
								if (turnList[turnList.length - 1].currentPlayerNum == i && turnList[turnList.length - 1].type == selector.getType())
								{
									addTile = true;
								}
								
								selector.restoreLastState(i, addTile);
							}
						}
					}
					
					//For every cell on the board, roll back in history
					for (i=0; i < gameBoard.length; i++)
					{
						var currentCell:Cell = gameBoard[i];
						
						var previousCellState:int = playLog[playLog.length - 2][i];
						var previousCellOwner:int = playLogOwnership[playLogOwnership.length - 2][i];
						
						//If the tile is different than it was last time
						if (currentCell.state != previousCellState)
						{
							//trace("cell " + i + " state = " + gameBoard[i].state + " previous state = " + previousCellState);
							
							gameBoard[i].state = previousCellState;
							
							//Make sure the tiles are assigned to their correct owners
							if (previousCellOwner == 5)
							{
								gameBoard[i].owner = null;
							}
							else
							{
								var tempOwnerPlayer:Player = new Player(previousCellOwner, playerColors[previousCellOwner], this);
								
								gameBoard[i].owner = tempOwnerPlayer;
								gameBoard[i].currentPlayer = tempOwnerPlayer;
							}
							gameBoard[i].reset();
							if(!Main.sAnimationsOn)
								gameBoard[i].addDisplayForNoAnimations();
						}
						
						gameBoard[i].potentialState = 0;
						gameBoard[i].originalState = gameBoard[i].state;
					}
					
					if(captureZoneMode)
					{
						//trace("Is is a Capture Zone Map");
						
						//trace("There is a total of " + fullCaptureZoneList.length + " Capture Zones");
						
						for each (var captureZone:CaptureZone in fullCaptureZoneList)
						{
							/*trace("This capture Zone has been claimed: " + captureZone.claimed);
							trace("This capture zone was claimed on " + captureZone.turnClaimed + " turn");
							trace("Current Turn number: " + (playLog.length-1).toString());*/
							
							if(captureZone.claimed && playLog.length-1 == captureZone.turnClaimed)
							{
								//trace("Capture Zone " + captureZone.identifier + "has been claimed last turn");
								//trace("-------------------");
								
								captureZone.activateAllCells();
								captureZone.turnClaimed = 0;
								captureZone.claimed = false;
								
								captureZoneList.push(captureZone);
							}
						}
						
						updateClaimText();
					}
					
					if(erosionZoneMode)
					{
						for each (var erosionZone:ErosionZone in fullErosionZoneList)
						{
							if(erosionZone.decayed && playLog.length-1 == erosionZone.decayed)
								erosionZone.activateCells();
						}
					}
					
					//Reverse change players
					restoreLastState();
					
					//remove the turn from the logs.
					playLog.pop();
					playLogOwnership.pop();
					turnList.pop();
					
					checkEndGame();
				}
				
				if(captureZoneMode && captureZoneList.length > 0)
				{
					currentClaimCount--;
					updateClaimText();
				}
				
				if(erosionZoneMode && !currentlyActiveErosionZone.decayed)
					currentlyActiveErosionZone.reverseDecay();
				
				spreadInfection();
				
				boardOutline.changeColors(currentPlayer.playerHexColor);
				
				Starling.juggler.delayCall(undoResetDelay, .1);
				hasPressedUndo = true;
				
				//Reset Branches
				for (var k:int = 0; k < gameBoard.length; k++)
				{
					gameBoard[k].determineBranches();
				}
			}
		}
		private function undoResetDelay():void
		{
			undoing = false;
		}
		private function restoreLastState():void
		{
			//currentPlayer.darkenAllSelectedCells();
			
			//Assign Current Player
			if (_campaign)
			{
				for (var i:int = 0; i < playerArray.length; i++)
				{
					//trace("Went back Should be twice");
					
					//Increment to next player
					currentPlayerNum--;
					
					//Cannot have less than 0;
					if (currentPlayerNum < 0)
					{
						currentPlayerNum = numOfPlayers - 1;
						roundCounter--;
					}
					
					currentPlayer = playerArray[currentPlayerNum];
					currentPlayer.historyArrayOfTiles.pop();
				}
			}
			else
			{
				//Increment to next player
				currentPlayerNum--;
				
				//Cannot have less than 0;
				if (currentPlayerNum < 0)
				{
					currentPlayerNum = numOfPlayers - 1;
					roundCounter--;
				}
				
				currentPlayer = playerArray[currentPlayerNum];
				currentPlayer.historyArrayOfTiles.pop();
			}
			
			// modify selector sizes and places
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				for (i=0; i<playerArray.length; i++)
				{
					playerArray[i].revertSelectors();
				}
				
				if (!currentPlayer.getAiControl())
				{
					currentPlayer.increaseSelectorSizes();
				}
			}
			
			currentCellTypeSelected = currentPlayer.cellSelectorArray[0];
			tileSelector.x = currentCellTypeSelected.x;
			tileSelector.y = currentCellTypeSelected.y;
			setChildIndex(tileSelector, numChildren -1)
			//currentPlayer.showAllSelectedCells();
		}
		
		// AI functions
		public function setPlayerAndSelectedTile(inCell:Cell):void
		{
			// if (inCell == null)
				// trace("ERROR - passed " + inCell + " into setPlayerAndSelectedTile --- player = " + currentPlayer);
			
			inCell.currentPlayer = currentPlayer;
			inCell.selectedCellType = currentCellTypeSelected;
		}
		
		public function makeAIMove(inLocation:int):void
		{
			//trace("AI is making a move");
			
			aiCellLocation = inLocation;
			lastClickedCell = gameBoard[aiCellLocation];
			moveUnderway = true;
		}
		
		// getters & setters
		public function getCurrentPlayerNum():int
		{
			return currentPlayerNum;
		}
		public function getPlayReplay():Boolean
		{
			return playReplay;
		}
		
		public function getCurrentCellTypeSelected():CellSelector
		{
			return currentCellTypeSelected;
		}
		public function setCurrentCellTypeSelected(inSelector:CellSelector):void
		{
			currentCellTypeSelected = inSelector;
		}
		public function HideSelector():void
		{
			// scale down cell selector
			if(currentCellTypeSelected.type > 0)
				currentCellTypeSelected.scaleAndFadeWhenClicked();
			
			tileSelector.visible = false;
		}
		public function modifyPlayerPercentBar(player:Player):void
		{
			player.totalNumOfTiles = player.numOfTriangleTiles + player.numOfDiamondTiles + player.numOfStarTiles + player.numOfSolidTiles;	
			player.playerPercent = (player.totalNumOfTiles / totalNumOfPlayableTiles) * 100;
			
			var onePercentOfFrame:Number = percentFrameWidth / 100;
			
			player.percentBarWidth = player.playerPercent * onePercentOfFrame;
			
			if (player.numOfSolidTiles > 0)
			{
				player.percentBarOfTierThreeWidth = ((player.numOfSolidTiles / player.totalNumOfTiles) * player.percentBar.width);
			}
			else
			{
				player.percentBarOfTierThreeWidth = 0;
			}
			
			//trace("modified player " + player.teamName + " to have " + player.playerPercent);
		}
		public function getMain():Main
		{
			return main;
		}
		public function addToPlayerTileCount(player:Player, cell:Cell):void
		{
			if (cell.state == 1)
				player.numOfTriangleTiles++;
			else if (cell.state == 2)
				player.numOfDiamondTiles++;
			else if (cell.state == 3)
				player.numOfStarTiles++;
			else if (cell.state == 4)
				player.numOfSolidTiles++;
			
			modifyPlayerPercentBar(player);
			
			//trace("modified " + player.teamName + " to have " + player.numOfTriangleTiles + ", " + player.numOfDiamondTiles);
		}
		public function subtractFromPlayerTileCount(player:Player, cell:Cell):void
		{
			if (cell.state == 1)
				player.numOfTriangleTiles--;
			else if (cell.state == 2)
				player.numOfDiamondTiles--;
			else if (cell.state == 3)
				player.numOfStarTiles--;
			else if (cell.state == 4)
				player.numOfSolidTiles--;
			
			modifyPlayerPercentBar(player);
		}
		public function addTipWindow(cell:Cell):void
		{
			if(_tipWindow == null)
			{
				var globalLocation:Point = cell.localToGlobal(new Point(0,0));
				
				_tipWindow = new TipWindow(this, currentCellTypeSelected.type, cell.state);
				addChild(_tipWindow);
				_tipWindow.x = globalLocation.x;
				_tipWindow.y = globalLocation.y;
			}
		}
		public function removeTipWindow():void
		{
			if(_tipWindow)
			{
				_tipWindow.dispose();
				removeChild(_tipWindow);
				_tipWindow = null;
			}
		}
		
		public function isZoomed():Boolean
		{
			if (tileHolder.scaleX > 1.0)
				return true;
			return false;
		}
		public function addAchievementPopUp(achievementNumber:int):void
		{
			trace("Game Manager Adding Achievement");
			
			var achievementPopUp:AchievementPopUp = new AchievementPopUp();
			addChild(achievementPopUp);
			achievementPopUp.init(achievementNumber);
			achievementPopUp.x = PathogenTest.stageCenter.x;
			achievementPopUp.pivotX = achievementPopUp.width/2;
			achievementPopUp.y = PathogenTest.stageHeight;
			achievementPopUp.gameManager = this;
			achievementPopUp.start();
		}
		public function removeAchievementPopUp(achivementPopUp:AchievementPopUp):void
		{
			achivementPopUp.dispose();
			removeChild(achivementPopUp);
			achivementPopUp = null;
		}
		private function garbageCollection():void
		{
			//trace("Garbage Collection");
			
			for each (var player:Player in playerArray)
			{				
				player.garbageCollection();
				player.dispose();
				removeChild(player);
				player = null;
			}
			
			playerArray = [];
			currentCellTypeSelected = null;
			currentPlayer = null;
			
			if (postGame)
			{
				postGame.garbageCollection();
				postGame.dispose();
				postGame = null;
			}
			
			//Reset all cells
			for each (var cell:Cell in gameBoard)
			{
				cell.CleanCell()
				cell.dispose();
				cell = null;
			}
			
			gameBoard = [];
			
			removeChild(tileHolder);
			tileHolder = null;
			
			optionsButton.removeEventListener(TouchEvent.TOUCH, addOptions);
			optionsButton.dispose();
			removeChild(optionsButton);
			optionsButton = null;
			
			quitButton.removeEventListener(TouchEvent.TOUCH, addQuit);
			quitButton.dispose();
			removeChild(quitButton);
			quitButton = null;
			
			removeQuit();
			removeVictoryScreen();
			
			if(boardOutline)
			{
				boardOutline.dispose();
				removeChild(boardOutline);
				boardOutline = null;
			}
			
			if(undoWhiteFlash)
			{
				undoWhiteFlash.destroy();
				undoWhiteFlash.dispose();
				removeChild(undoWhiteFlash);
				undoWhiteFlash = null;
			}
			
			if (_undoButton)
			{
				_undoButton.removeEventListener(TouchEvent.TOUCH, onTouchUndo);
				_undoButton.dispose();
				removeChild(_undoButton);
				_undoButton = null;
			}
			
			if (_zoomButton)
			{
				_zoomButton.removeEventListener(TouchEvent.TOUCH, onTouchZoom);
				_zoomButton.dispose();
				removeChild(_zoomButton);
				_zoomButton = null;
			}
			
			if(_winButton)
			{
				_winButton.dispose();
				removeChild(_winButton);
				_winButton = null;
			}
			
			for each (turn in turnList)
			{
				turn.dispose();
				turn = null;
			}
			
			turnList = null;
			
			if(_replay)
			{
				playPauseButton.dispose();
				playPauseButton.destroy();
				removeChild(playPauseButton);
				playPauseButton = null;
				
				stepBackwardButton.destroy();
				stepBackwardButton.dispose();
				removeChild(stepBackwardButton);
				stepBackwardButton = null;
				
				stepForwardButton.destroy();
				stepForwardButton.dispose();
				removeChild(stepForwardButton);
				stepForwardButton = null;
			}
			
			_percentBar.dispose();
			removeChild(_percentBar);
			_percentBar = null;
			
			if(tutorial)
			{
				tutorial.dispose();
				removeChild(tutorial);
				tutorial = null;
			}
			
			removeEventListener(Event.ENTER_FRAME, update)
			
			// remove AI
			if (aiOn)
			{
				ai.cleanUp();
				ai = null;
			}
			
			if (tileSelector)
			{
				Starling.juggler.removeTweens(tileSelector);
				tileSelector.destroy();
				tileSelector.dispose();
				removeChild(tileSelector);
				tileSelector = null;
			}
			
			if (_debug)
				Starling.current.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKey);
			
			/*
			// crosshair
			if (_horizontalCrosshair)
			{
				_horizontalCrosshair.dispose();
				tileHolder.removeChild(_horizontalCrosshair);
				_horizontalCrosshair = null;
			}
			
			if (_verticalCrosshair)
			{
				_verticalCrosshair.dispose();
				tileHolder.removeChild(_verticalCrosshair);
				_verticalCrosshair = null;
			}
			*/
			
			endFlurryEvents();
		}
		private function endFlurryEvents():void
		{
			if(campaign && Flurry.isSupported)
			{
				var outcome:String;
				var levelName:String;
				
				if(success)
					outcome = "Win";
				else
					outcome = "Loss";
				
				levelName = campaign.currentCampaignLevel._worldName;
				Flurry.endTimedEvent(levelName, { difficulty: campaign.difficultySelector.difficulty, Outcome: outcome  });
				
				switch(campaign.difficultySelector.difficulty)
				{
					case 3:
						levelName = levelName + " - Hard";
						break;
					
					case 2:
						levelName = levelName + " - Medium";
						break;
					
					case 1:
						levelName = levelName + " - Easy";
						break;
				}
				
				Flurry.logEvent(levelName, { Outcome: outcome});
				
			}
			if(campaign == null && Flurry.isSupported)
				Flurry.endTimedEvent("Local Multiplayer Game", { NumberOfPlayers : playerColors.length, MapSize : mapSize, MapName : mapName, Timer: timer, Lightning : lightning, CaptureZone : captureZoneMode, Erosion : erosionZoneMode, Infection: infection});
		}
		
		public function get percentBar():Sprite { return _percentBar};
		public function get undoButton():BW_Button { return _undoButton};
		public function get domination():Boolean {return _domination};
		public function get campaign():Campaign {return _campaign};
		public function get retry():Boolean {return _retry};
		public function get mapSize():int {return _mapSize};
		public function get undoingBool():Boolean {return undoing};
		public function get replay():Boolean {return _replay};
		//public function get zooming():Boolean { return _zooming; }
		
	}
}