package Core
{
	import starling.display.Sprite;
	
	public class TurnRecord extends Sprite
	{
		public var type:int;
		public var location:int;
		public var currentPlayerNum:int;
		public var surrendered:int;
		
		public function TurnRecord(type:int, location:int, currentPlayerNum:int, surrendered:int)
		{
			super();
			
			this.type = type;
			this.location = location;
			this.currentPlayerNum = currentPlayerNum;
			this.surrendered = surrendered;
		}
		
	}
}