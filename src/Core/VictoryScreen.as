package Core
{
	import UI.AchievementPopUp;
	import UI.Achievements;
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	
	public class VictoryScreen extends BW_UI
	{
		private var _gameManager:GameManager;
		
		private var victoryCell:Sprite
		private var victoryParticles:PDParticleSystem
		
		private var viewStats:BW_Button;
		private var nextLevel:BW_Button
		
		private var quitting:Boolean;
		
		public function VictoryScreen(gameManager:GameManager)
		{
			super();
			
			_gameManager = gameManager;
			
			//Creating background
			var transBackground:Quad = new Quad(PathogenTest.wTenPercent * 10, PathogenTest.hTenPercent * 10, 0x000000);
			addChild(transBackground);
			transBackground.alpha = .70;
			
			//Determining Winner
			var playerPercentArray:Array = [];
			var player:Player;
			
			for  each (player in gameManager.playerArray)
			{
				playerPercentArray.push(player.playerPercent);
			}
			
			var highestPercent:Number = Math.max.apply(null, playerPercentArray)
			
			var winningPlayer:Player;
			var winningTeam:String;
			var numOfWinners:int = 0;
			
			for  each (player in gameManager.playerArray)
			{
				if (player.playerPercent >= highestPercent)
				{
					winningTeam = player.teamName;
					winningPlayer = player;
					//trace(winningTeam + ": " + player.playerPercent + "%");
					
					numOfWinners++;
				}
			}
			
			if (numOfWinners > 1)
				winningTeam = "Tie";
			
			//Creating Victory Cell
			victoryCell = new Sprite();
			victoryCell.pivotX = victoryCell.width/2;
			victoryCell.pivotY = victoryCell.height/2;
			addChild(victoryCell);
			
			var quarterSlice_01:Image;
			var quarterSlice_02:Image;
			var quarterSlice_03:Image;
			var quarterSlice_04:Image
			
			switch(winningTeam)
			{
				case "Green":
					quarterSlice_01 = new Image(Main.assets.getTexture("GreenVictory_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("GreenVictory_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("GreenVictory_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("GreenVictory_Slice"));	
					break;
				case "Red":
					quarterSlice_01 = new Image(Main.assets.getTexture("RedVictory_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("RedVictory_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("RedVictory_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("RedVictory_Slice"));	
					break;
				case "Blue":
					quarterSlice_01 = new Image(Main.assets.getTexture("BlueVictory_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("BlueVictory_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("BlueVictory_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("BlueVictory_Slice"));	
					break;
				case "Yellow":
					quarterSlice_01 = new Image(Main.assets.getTexture("YellowVictory_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("YellowVictory_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("YellowVictory_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("YellowVictory_Slice"));	
					break;
				case "Tie":
					quarterSlice_01 = new Image(Main.assets.getTexture("Tie_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("Tie_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("Tie_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("Tie_Slice"));	
					break;
				default:
					quarterSlice_01 = new Image(Main.assets.getTexture("Tie_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("Tie_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("Tie_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("Tie_Slice"));	
					break;
			}
			
			victoryCell.addChild(quarterSlice_01);
			victoryCell.addChild(quarterSlice_02);
			victoryCell.addChild(quarterSlice_03);
			victoryCell.addChild(quarterSlice_04);
			
			quarterSlice_01.pivotX = quarterSlice_01.width/2;
			quarterSlice_01.pivotY = quarterSlice_01.height/2;
			
			quarterSlice_02.pivotX = quarterSlice_02.width/2;
			quarterSlice_02.pivotY = quarterSlice_02.height/2;
			
			quarterSlice_03.pivotX = quarterSlice_03.width/2;
			quarterSlice_03.pivotY = quarterSlice_03.height/2;
			
			quarterSlice_04.pivotX = quarterSlice_04.width/2;
			quarterSlice_04.pivotY = quarterSlice_04.height/2;
			
			quarterSlice_02.rotation = deg2rad(90);
			quarterSlice_02.x = quarterSlice_01.x + quarterSlice_01.width;
			
			
			quarterSlice_03.rotation = deg2rad(180);
			quarterSlice_03.x = quarterSlice_02.x;
			quarterSlice_03.y = quarterSlice_01.y + quarterSlice_01.height;
			
			quarterSlice_04.rotation = deg2rad(270);
			quarterSlice_04.x = quarterSlice_01.x
			quarterSlice_04.y = quarterSlice_03.y;
			
			victoryCell.x = PathogenTest.stageCenter.x -victoryCell.width/4 ;
			victoryCell.y = PathogenTest.stageCenter.y -victoryCell.height/4;
			
			// add particles
			var texture:Texture = Main.assets.getTexture("Particle");
			var config:XML = XML(new Main.ParticlePEX());
			victoryParticles = new PDParticleSystem(config, texture);
			addChild(victoryParticles);
			Starling.juggler.add(victoryParticles);
			victoryParticles.scaleX = PathogenTest.HD_Multiplyer * 1 / PathogenTest.scaleFactor;
			victoryParticles.scaleY = PathogenTest.HD_Multiplyer * 1 / PathogenTest.scaleFactor;
			victoryParticles.start();
			victoryParticles.x = PathogenTest.stageCenter.x;
			victoryParticles.y = PathogenTest.stageCenter.y;
			
			//Adding Victory Text
			var fontSize:int = PathogenTest.HD_Multiplyer * 72 / PathogenTest.scaleFactor;
			var textX:int = PathogenTest.HD_Multiplyer * 450 / PathogenTest.scaleFactor;
			var textY:int = PathogenTest.HD_Multiplyer * 200 / PathogenTest.scaleFactor;
			var textfield:TextField = new TextField(textX, textY, "VICTORY", "Questrial", fontSize, Color.WHITE);
			textfield.pivotX = textfield.width/2;
			textfield.pivotY = textfield.height/2;
			addChild(textfield);
			textfield.x = PathogenTest.stageCenter.x;
			textfield.y = victoryCell.y - (victoryCell.height * .3) - (PathogenTest.hTenPercent * .5);
			
			//Creating Buttons
			if (Main.isPhone())
				viewStats = new BW_Button(2, 1.0, "View Stats", 32);
			else
				viewStats = new BW_Button(2, 0.75, "View Stats", 32);
			addChild(viewStats);
			viewStats.x = PathogenTest.stageCenter.x;
			viewStats.y = victoryCell.y + (victoryCell.height * .8) + (PathogenTest.hTenPercent * .5);
			viewStats.addEventListener(TouchEvent.TOUCH, onTouchStatsScreen);
			
			if(gameManager.campaign)
			{
				if (Main.isPhone())
					nextLevel = new BW_Button(2, 1.0, "Next Level", 32);
				else
					nextLevel = new BW_Button(2, .75, "Next Level", 32);
				addChild(nextLevel);
				nextLevel.x = viewStats.x;
				nextLevel.y = viewStats.y + PathogenTest.hTenPercent;
				nextLevel.addEventListener(TouchEvent.TOUCH, onTouchNextLevel);
			}
			
			Main.achievements.victoryScreen = this;
			
			if(winningTeam == "Tie")
			{
				textfield.text = "TIE";
				
				if(Main.achievements.progress_Tie_Game != Achievements.COMPLETE)
				{
					Main.achievements.updateAchievementProgress(Achievements.TIE_GAME);
				}
				
			}
			if(gameManager.domination && winningPlayer && !winningPlayer.getAiControl())
			{	
				textfield.text = "DOMINATION VICTORY";
				
				if(Main.achievements.progress_Domination_Game != Achievements.COMPLETE)
				{
					Main.achievements.updateAchievementProgress(Achievements.DOMINATION_GAME);
				}
				
				
				textfield.y -= PathogenTest.hTenPercent * .5;
			}
			
			Main.sSoundController.playWinSound();
		}
		public function onTouchNextLevel($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				nextLevel.removeEventListener(TouchEvent.TOUCH, onTouchNextLevel);
				viewStats.removeEventListener(TouchEvent.TOUCH, onTouchStatsScreen);
				
				quitting = true;
				
				startFadeOut();
			}
		}
		public function onTouchStatsScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(nextLevel)
					nextLevel.removeEventListener(TouchEvent.TOUCH, onTouchNextLevel);
				
				viewStats.removeEventListener(TouchEvent.TOUCH, onTouchStatsScreen);
				
				_gameManager.addPostGameScreen();
				startFadeOut();
			}
		}
		public function addAchievementPopUp(achievementNumber:int):void
		{
			trace("Victory Screen adding achievement");
			
			var achievementPopUp:AchievementPopUp = new AchievementPopUp();
			addChild(achievementPopUp);
			achievementPopUp.init(achievementNumber);
			achievementPopUp.x = PathogenTest.stageCenter.x;
			achievementPopUp.pivotX = achievementPopUp.width/2;
			achievementPopUp.y = PathogenTest.stageHeight;
			achievementPopUp.victoryScreen = this;
			achievementPopUp.start();
		}
		public function removeAchievementPopUp(achivementPopUp:AchievementPopUp):void
		{
			achivementPopUp.dispose();
			removeChild(achivementPopUp);
			achivementPopUp = null;
		}
		public override function death():void
		{
			if(victoryCell)
			{
				victoryCell.dispose();
				removeChild(victoryCell);
				victoryCell = null;
			}
			
			if(nextLevel)
			{
				nextLevel.destroy();
				nextLevel.dispose();
				removeChild(nextLevel);
				nextLevel = null;
			}
			
			if(viewStats)
			{
				viewStats.destroy();
				viewStats.dispose();
				removeChild(viewStats);
				viewStats = null;
			}
			
			if(victoryParticles)
			{
				victoryParticles.stop(true);
				victoryParticles.removeFromParent(true);
				Starling.juggler.remove(victoryParticles);
				victoryParticles = null;
			}
			
			if(_gameManager)
			{
				_gameManager.removeVictoryScreen();
				
				if(quitting)
					_gameManager.Quit();
				
				_gameManager = null;
			}
			
		}
	}
}