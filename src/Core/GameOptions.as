package Core
{
	import Sound.SoundController;
	
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import feathers.controls.Slider;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class GameOptions extends BW_UI
	{
		//Parent Class
		private var _gameManager:GameManager
		
		//Textfields
		private var _MusicVolumeTitle:TextField;
		private var _MusicVolumeTextfield:TextField;
		private var _soundEffectsTitle:TextField;
		private var _soundEffectsTextfield:TextField;
		private var _animationSpeedTitle:TextField;
		
		//Sliders
		private var _musicSlider:Slider;
		private var _soundEffectsSlider:Slider;
		
		//Save
		private var sharedDataObject:SharedObject;
		
		// buttons
		private var optionsQuit:BW_Button;
		private var _animationSpeedButton:BW_Button;
		
		// checkboxes
		private var _animationCheckbox:BW_Button;
		private var _practiceCheckbox:BW_Button
		private var _percentCheckbox:BW_Button;
		private var _undoCheckbox:BW_Button;
		
		public function GameOptions(gameManager:GameManager)
		{
			super();
			
			_gameManager = gameManager;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			var texture:Texture = Main.assets.getTexture("scale9");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			addChild(transBackground);
			transBackground.alpha = .75;
			
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 5;
			background.height = PathogenTest.hTenPercent * 9.75;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			
			var header:Scale9Image = new Scale9Image(textures);
			addChild(header);
			header.width = PathogenTest.wTenPercent * 5;
			header.height = PathogenTest.hTenPercent;
			header.pivotX = header.width/2;
			header.y = -background.height/2;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
			var title:TextField = new TextField(header.width, header.height, "Options", "Questrial", fontSize, Color.WHITE);
			title.pivotX = title.width/2;
			addChild(title);
			title.y = header.y;
			
			//Music Volume
			_MusicVolumeTitle = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent/2, "Music Volume", "Questrial", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			_MusicVolumeTitle.pivotX = _MusicVolumeTitle.width/2;
			addChild(_MusicVolumeTitle);
			_MusicVolumeTitle.y = title.y + (title.height * 1.2);
			
			_MusicVolumeTextfield = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent/2,  SoundController.getMusicVolume().toString() + "%", "Dekar", PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
			_MusicVolumeTextfield.pivotX = _MusicVolumeTextfield.width/2;
			addChild(_MusicVolumeTextfield);
			_MusicVolumeTextfield.y = _MusicVolumeTitle.y + _MusicVolumeTitle.height;
			
			//Create Slider
			_musicSlider = new Slider();
			_musicSlider.minimum = 0;
			_musicSlider.maximum = 100;
			_musicSlider.value = SoundController.getMusicVolume();
			_musicSlider.step = 10
			_musicSlider.direction = Slider.DIRECTION_HORIZONTAL;
			_musicSlider.liveDragging = true;
			_musicSlider.thumbProperties.defaultSkin = new Image(Main.assets.getTexture("SliderKnob"));
			_musicSlider.thumbProperties.width = PathogenTest.HD_Multiplyer * 30;
			_musicSlider.minimumTrackProperties.defaultSkin = new Quad(PathogenTest.wTenPercent * 3,PathogenTest.hTenPercent/3, 0x222245);
			_musicSlider.pivotX = _musicSlider.minimumTrackProperties.defaultSkin.width/2;
			_musicSlider.pivotY = _musicSlider.minimumTrackProperties.defaultSkin.height/2;
			addChild(_musicSlider);
			_musicSlider.y = _MusicVolumeTextfield.y + (PathogenTest.hTenPercent * .8)
			_musicSlider.addEventListener(Event.CHANGE, changeMusicSlider);
			
			//Sound Effect Volume
			_soundEffectsTitle = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent/2, "Sound Effects Volume", "Questrial", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			_soundEffectsTitle.pivotX = _soundEffectsTitle.width/2;
			addChild(_soundEffectsTitle);
			_soundEffectsTitle.y = _musicSlider.y + (PathogenTest.hTenPercent * .8);
			
			_soundEffectsTextfield = new TextField(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent/2,  SoundController.getSoundVolume().toString() + "%", "Dekar", PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
			_soundEffectsTextfield.pivotX = _soundEffectsTextfield.width/2;
			addChild(_soundEffectsTextfield);
			_soundEffectsTextfield.y = _soundEffectsTitle.y + _soundEffectsTitle.height;
			
			//Create Slider
			_soundEffectsSlider = new Slider();
			_soundEffectsSlider.minimum = 0;
			_soundEffectsSlider.maximum = 100;
			_soundEffectsSlider.value = SoundController.getSoundVolume();
			_soundEffectsSlider.step = 5;
			_soundEffectsSlider.direction = Slider.DIRECTION_HORIZONTAL;
			_soundEffectsSlider.liveDragging = true;
			_soundEffectsSlider.thumbProperties.defaultSkin = new Image(Main.assets.getTexture("SliderKnob"));
			_soundEffectsSlider.thumbProperties.width = PathogenTest.HD_Multiplyer * 30;
			_soundEffectsSlider.minimumTrackProperties.defaultSkin = new Quad(PathogenTest.wTenPercent * 3,PathogenTest.hTenPercent/3, 0x222245);
			_soundEffectsSlider.pivotX = _soundEffectsSlider.minimumTrackProperties.defaultSkin.width/2;
			_soundEffectsSlider.pivotY = _soundEffectsSlider.minimumTrackProperties.defaultSkin.height/2;
			addChild(_soundEffectsSlider);
			_soundEffectsSlider.y = _soundEffectsTextfield.y + (PathogenTest.hTenPercent * .8)
			_soundEffectsSlider.addEventListener(Event.CHANGE, changeSoundEffectsSlider);
			
			// animation speed text
			_animationSpeedTitle = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent/2, "Animations", "Questrial", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			_animationSpeedTitle.pivotX = _animationSpeedTitle.width/2;
			addChild(_animationSpeedTitle);
			_animationSpeedTitle.y = _soundEffectsSlider.y + (PathogenTest.hTenPercent * .8);
			
			var animationOnText:TextField = new TextField(PathogenTest.wTenPercent * 0.5, PathogenTest.hTenPercent * 0.5, "On", "Dekar", PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor, Color.WHITE);
			animationOnText.pivotX = animationOnText.width/2;
			animationOnText.pivotY = animationOnText.height/2;
			addChild(animationOnText);
			animationOnText.x -= PathogenTest.wTenPercent * 1.75;
			animationOnText.y = _animationSpeedTitle.y + (PathogenTest.hTenPercent * 1.0);
			
			_animationCheckbox = new BW_Button(.5,.7, "", 0);
			_animationCheckbox.addImage(Main.assets.getTexture("CheckBoxUp"), Main.assets.getTexture("CheckBoxDown"));
			_animationCheckbox.toggle = true;
			addChild(_animationCheckbox);
			_animationCheckbox.x = animationOnText.x + PathogenTest.wTenPercent * 0.75;
			_animationCheckbox.y = animationOnText.y;
			_animationCheckbox.addEventListener(TouchEvent.TOUCH, onCheckAnimations);
			if (Main.sAnimationsOn)
				_animationCheckbox.toggleDown();
			
			var animationSpeedText:TextField = new TextField(PathogenTest.wTenPercent * 1.0, PathogenTest.hTenPercent * 0.5, "Speed", "Dekar", PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor, Color.WHITE);
			animationSpeedText.pivotX = animationSpeedText.width/2;
			animationSpeedText.pivotY = animationSpeedText.height/2;
			addChild(animationSpeedText);
			animationSpeedText.x = _animationCheckbox.x + PathogenTest.wTenPercent * 1.25;
			animationSpeedText.y = animationOnText.y;
			
			var animationSpeedString:String;
			if (Main.animationFramerate == Main.ANIMATION_SPEED_NORMAL)
				animationSpeedString = "Normal";
			else
				animationSpeedString = "Fast";
			_animationSpeedButton = new BW_Button(1.0, 0.75, animationSpeedString, 24);
			addChild(_animationSpeedButton);
			_animationSpeedButton.x = animationSpeedText.x + (PathogenTest.wTenPercent * 1.25);
			_animationSpeedButton.y = _animationCheckbox.y;
			
			if (!Main.sAnimationsOn)
				_animationSpeedButton.alpha = 0.5;
			else
				_animationSpeedButton.addEventListener(TouchEvent.TOUCH, onChangeAnimationSpeed);
			
			var percentTextfield:TextField = new TextField(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent * .5, "Show Top Bar", "Dekar", PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor, Color.WHITE);
			percentTextfield.pivotX = percentTextfield.width/2;
			percentTextfield.pivotY = percentTextfield.height/2;
			addChild(percentTextfield);
			percentTextfield.y = _animationSpeedButton.y + (PathogenTest.hTenPercent * 1.2);
			
			_percentCheckbox = new BW_Button(.5,.7, "", 0);
			_percentCheckbox.addImage(Main.assets.getTexture("CheckBoxUp"), Main.assets.getTexture("CheckBoxDown"));
			_percentCheckbox.toggle = true;
			addChild(_percentCheckbox);
			_percentCheckbox.x = PathogenTest.wTenPercent * 1.5;
			_percentCheckbox.y = percentTextfield.y;
			_percentCheckbox.addEventListener(TouchEvent.TOUCH, togglePercentBar);
			
			if(gameManager.percentBar.visible)
				_percentCheckbox.toggleDown();
			
			var undoTextfield:TextField = new TextField(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent * .5, "Show Undo", "Dekar", PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor, Color.WHITE);
			undoTextfield.pivotX = undoTextfield.width/2;
			undoTextfield.pivotY = undoTextfield.height/2;
			addChild(undoTextfield);
			undoTextfield.y = percentTextfield.y + (percentTextfield.height * 1.5);
			
			_undoCheckbox = new BW_Button(.5,.7, "", 0);
			_undoCheckbox.addImage(Main.assets.getTexture("CheckBoxUp"), Main.assets.getTexture("CheckBoxDown"));
			_undoCheckbox.toggle = true;
			addChild(_undoCheckbox);
			_undoCheckbox.x = _percentCheckbox.x
			_undoCheckbox.y = undoTextfield.y;
			_undoCheckbox.addEventListener(TouchEvent.TOUCH, hideUndoButton);
			
			if(gameManager.undoButton.visible)
				_undoCheckbox.toggleDown();
			
			optionsQuit = new BW_Button(.5,.7, "", 0);
			optionsQuit.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(optionsQuit);
			optionsQuit.x = - background.width/2 + (optionsQuit.width * .7);
			optionsQuit.y = background.height/2 - (optionsQuit.height * .7);
			optionsQuit.addEventListener(TouchEvent.TOUCH, removeOptions);
		}
		private function togglePercentBar($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(_gameManager.percentBar.visible == false)
					_gameManager.percentBar.visible = true;
				else
					_gameManager.percentBar.visible = false
			}
		}
		private function hideUndoButton($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(_gameManager.undoButton.visible == true)
					_gameManager.undoButton.visible = false;
				else
					_gameManager.undoButton.visible = true;
			}
		}
		private function removeOptions($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				startFadeOut();
			}
		}
		private function changeMusicSlider($e:Event):void
		{
			//Updating music volumne information
			_MusicVolumeTextfield.text = _musicSlider.value.toString() + "%";
			
			SoundController.setMusicVolume(_musicSlider.value);
		}
		private function changeSoundEffectsSlider($e:Event):void
		{
			//Updating sound Effects Volume information.
			_soundEffectsTextfield.text = _soundEffectsSlider.value.toString()  + "%";
			
			SoundController.setSoundVolume(_soundEffectsSlider.value);
		}
		private function onCheckAnimations(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if (Main.sAnimationsOn)
				{
					Main.sAnimationsOn = false;
					Main.saveDataObject.data.AnimationsOn = false;
					_animationSpeedButton.removeEventListener(TouchEvent.TOUCH, onChangeAnimationSpeed);
					_animationSpeedButton.alpha = 0.5;
				}
				else
				{
					Main.sAnimationsOn = true;
					Main.saveDataObject.data.AnimationsOn = true;
					_animationSpeedButton.addEventListener(TouchEvent.TOUCH, onChangeAnimationSpeed);
					_animationSpeedButton.alpha = 1.0;
				}
			}
		}
		private function onChangeAnimationSpeed(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				if (Main.animationFramerate == Main.ANIMATION_SPEED_NORMAL)
				{
					Main.animationFramerate = Main.ANIMATION_SPEED_FAST;
					Main.saveDataObject.data.AnimationSpeed = Main.ANIMATION_SPEED_FAST;
					_animationSpeedButton.changeText("Fast");
				}
				else
				{
					Main.animationFramerate = Main.ANIMATION_SPEED_NORMAL;
					Main.saveDataObject.data.AnimationSpeed = Main.ANIMATION_SPEED_NORMAL;
					_animationSpeedButton.changeText("Normal");
				}
			}
		}
		override public function death():void
		{
			optionsQuit.removeEventListener(TouchEvent.TOUCH, removeOptions);
			optionsQuit.destroy();
			optionsQuit.dispose();
			removeChild(optionsQuit);
			optionsQuit = null;

			_animationSpeedButton.removeEventListener(TouchEvent.TOUCH, onChangeAnimationSpeed);
			_animationSpeedButton.destroy();
			_animationSpeedButton.dispose();
			removeChild(_animationSpeedButton);
			_animationSpeedButton = null;
			
			_animationCheckbox.removeEventListener(TouchEvent.TOUCH, onCheckAnimations);
			_animationCheckbox.destroy();
			_animationCheckbox.dispose();
			removeChild(_animationCheckbox);
			_animationCheckbox = null;
			
			_percentCheckbox.removeEventListener(TouchEvent.TOUCH, togglePercentBar);
			_percentCheckbox.destroy();
			_percentCheckbox.dispose();
			removeChild(_percentCheckbox);
			_percentCheckbox = null;
			
			_undoCheckbox.removeEventListener(TouchEvent.TOUCH, hideUndoButton);
			_undoCheckbox.destroy();
			_undoCheckbox.dispose();
			removeChild(_undoCheckbox);
			_undoCheckbox = null;
			
			_soundEffectsSlider.removeEventListener(Event.CHANGE, changeSoundEffectsSlider);
			_soundEffectsSlider.dispose();
			removeChild(_soundEffectsSlider);
			_soundEffectsSlider = null;
			
			_musicSlider.removeEventListener(Event.CHANGE, changeMusicSlider);
			_musicSlider.dispose();
			removeChild(_musicSlider);
			_musicSlider = null;
			
			_gameManager.removeOptions();
		}
	}
}