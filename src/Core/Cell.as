package Core
{
	import Sound.*;
	
	import UI.Main;
	
	import Utils.BW_Selector;
	
	import feathers.controls.renderers.DefaultGroupedListHeaderOrFooterRenderer;
	
	import flash.geom.Point;
	import flash.media.SoundCodec;
	import flash.sampler.stopSampling;
	import flash.text.ReturnKeyLabel;
	
	import flashx.textLayout.formats.BackgroundColor;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	
	public class Cell extends Sprite
	{
		//Classes
		public var gameManager:GameManager;
		
		private var stateChange:Boolean;
		private var frameCounter:int;
		
		//Adjacentcy
		protected var top:Boolean;
		protected var right:Boolean;
		protected var bottom:Boolean;
		protected var left:Boolean;
		public var flipping:Boolean;
		
		public var adjacentTopCell:Cell;
		public var adjacentBottomCell:Cell;
		public var adjacentLeftCell:Cell;
		public var adjacentRightCell:Cell;
		
		//Cell Information
		public var owner:Player;
		public var state:int;
		public var location:int;
		
		//Rotation
		public static const DEGREE_90:Number = deg2rad(90);
		public static const DEGREE_180:Number =  deg2rad(180);
		public static const DEGREE_270:Number = deg2rad(-90);
		
		//Sprites
		protected var _display:MovieClip;
		private var grid:Image;
		protected var bridge:MovieClip;
		
		//Erosion Zone Vars
		public var activeErosionCell:Boolean;
		public var erosionCell:Boolean;
		
		//State Change  Vars
		public var currentPlayer:Player;
		public var selectedCellType:CellSelector;
		
		//Cascade Vars
		public var hasAlreadySpilledOver:Boolean = false;
		public var notAValidMove:Boolean;
		protected var originalTileState:int;
		
		//Capture Zone Vars
		public var capturePoint:Boolean;
		public var activeCapturePoint:Boolean;
		public var captureZoneNumber:int = 0;
		
		//Erosion Zone
		public var erosionZoneNumber:int;
		
		private var tween:Tween;
		
		public var infected:Boolean;
		
		private var _previousOwner:Player;
		
		//Scale in/out vars
		private static const SCALE_IN:int = 0;
		private static const SCALE_OUT:int = 1;
		
		private var _status:int;
		private var _miniumumScale:Number = .9;
		private var _scaleSpeed:int = 1;
		
		//Tutorial
		private var whiteFlash:BW_Selector;
		
		//Sound
		private var _reactionNumber:int = 0;
		
		private var currentTileHolderLocation:Point;
		private var previousTileHolderLocation:Point;
		
		protected var _originalState:int;
		protected var _potentialState:int;
		
		//Branch
		private var _rightBranch:Quad;
		private var _leftBranch:Quad;
		private var _topBranch:Quad;
		private var _bottomBranch:Quad;
		
		public function Cell(location:int)
		{
			super();
			
			this.location = location;
			
			_previousOwner = null;
		}
		private function onTouch($e:TouchEvent):void
		{
			//If cell is touched and a turn is not already underway
			if(!gameManager.moveUnderway)
			{
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
				if(touch)
				{
					gameManager.previewMove(this);
				}
			}
		}
		public function addTouchEvent():void
		{
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		public function changeState():void
		{
			//trace("Selected Cell: " + selectedCellType.type + " at location " + location);
			
			//The Cells is now changing & mark it as already been changed so that it may not be changed again.
			flipping = true;
			hasAlreadySpilledOver = true;
			
			//Depending on what is placed, do the following.
			switch(selectedCellType.type)
			{
				//If the player has selected a level t0 Tile
				case 0:
				{
					//If the player is stacking the same level on itself
					if ((state == 1) && (owner.playerTeam == currentPlayer.playerTeam))
					{
						state = 2;
						stateChange = true;
					}
					//If the Tile has not been Taken
					else if (state == 0)
					{
						state = 1;
						stateChange = true;
					}
					break;
				}
					//If the player has selected a t1 Tile
				case 1:
				{
					//If the player is stacking the same level on itself
					if ((state == 2) && (owner.playerTeam == currentPlayer.playerTeam))
					{
						state = 3;
						stateChange = true;
					}
					//Overwrite a lesser color
					else if (state == 1)
					{
						state = 2;
						stateChange = true;
					}
					//If its an empty space or a grey cell.
					else if (state == 0 || state == 5)
					{
						state = 2;
						stateChange = true;
					}
					break;
				}
				//If the player has selected a t2 cell
				case 2:
				{
					//t2 Grey cell
					if (state == 6)
					{
						state = 3;
						stateChange = true;
					}
					//t1 Grey cell
					else if (state == 5)
					{
						state = 3;
						stateChange = true;
					}
					//stacking a t2 on itself to create a t3.
					else if ((state == 3) && (owner.playerTeam == currentPlayer.playerTeam))
					{
						state = 4;
						stateChange = true;
					}
					//override a lower tier
					else if (state == 2)
					{
						state = 3;
						stateChange = true;
					}
					//override a lower tier
					else if (state == 1)
					{
						//trace("here!");
						
						state = 3;
						stateChange = true;
					}
					//place one in an empy space.
					else if (state == 0)
					{
						state = 3;
						stateChange = true;
					}
					break;
				}
				//If the player has selected a Virus
				case 3:
				{
					//If they are placing it on an enemy t2 cell
					if ((state == 3) && (owner.playerTeam != currentPlayer.playerTeam))
					{
						state = 0;
						originalTileState = 3;
						stateChange = true;
					}
					//If they are placing it on an enemy t1 cell
					if ((state == 2) && (owner.playerTeam != currentPlayer.playerTeam))
					{
						state = 0;
						originalTileState = 2;
						stateChange = true;
					}
					//If they are placing it on an enemy t0 cell
					if ((state == 1) && (owner.playerTeam != currentPlayer.playerTeam))
					{
						state = 0;
						originalTileState = 1;
						stateChange = true;
					}
					break;
				}
			}
			if (stateChange)
			{
				//trace("State Change: " + state + " at location " + location);
				
				//Valid Move
				gameManager.HideSelector();
				owner = currentPlayer;
				SoundController.selectScaleSound(state);
				
				//If the placed cell is higher than a t0, begin cascade function.
				if (state != 1)
				{
					//trace("Clicked on Cell: " + location);
					//trace("Origin State: " + state); 
					spillOver();
				}
				else
				{
					//Otherwise we know its a t0 cell being created in a brand new space
					setColor();
					
					if (Main.sAnimationsOn)
					{
						// Have the cell scale & fade into existance
						_display.alpha = 0;
						_display.scaleX = 0;
						_display.scaleY = 0;
						
						var tweenTime:Number = 0.3;
						tween = new Tween(_display, tweenTime);
						tween.fadeTo(1);
						tween.scaleTo(1);
						tween.onComplete = removeTween;
						Starling.juggler.add(tween);
						
						_reactionNumber++;
						SoundController.randomNote();
						SoundController.playSpreadSound(_reactionNumber);
					}
					else
						reset();
				}
			}
			else
			{
				notAValidMove = true;
				gameManager.addTipWindow(this);
			}
				
		}
		public function checkMove(selectedCellType:int):Boolean
		{
			var validMove:Boolean;
			
			//Depending on what is placed, do the following.
			switch(selectedCellType)
			{
				//If the player has selected a level t0 Tile
				case 0:
				{
					//If the player is stacking the same level on itself
					if ((state == 1) && (owner.playerTeam == currentPlayer.playerTeam))
						validMove = true;
					
					//If the Tile has not been Taken
					else if (state == 0)
						validMove = true;
					break;
				}
				//If the player has selected a t1 Tile
				case 1:
				{
					//If the player is stacking the same level on itself
					if ((state == 2) && (owner.playerTeam == currentPlayer.playerTeam))
						validMove = true;
					//Overwrite a lesser color
					else if (state == 1)
						validMove = true;
					//If its an empty space or a grey cell.
					else if (state == 0 || state == 5)
						validMove = true;
					break;
				}
				//If the player has selected a t2 cell
				case 2:
				{
					//t2 Grey cell
					if (state == 6)
						validMove = true;
						//t1 Grey cell
					else if (state == 5)
						validMove = true;
						//stacking a t2 on itself to create a t3.
					else if ((state == 3) && (owner.playerTeam == currentPlayer.playerTeam))
						validMove = true;
						//override a lower tier
					else if (state == 2)
						validMove = true;
						//override a lower tier
					else if (state == 1)
						validMove = true;
						//place one in an empy space.
					else if (state == 0)
						validMove = true;
					break;
				}
				//If the player has selected a Virus
				case 3:
				{
					//If they are placing it on an enemy t2 cell
					if ((state == 3) && (owner.playerTeam != currentPlayer.playerTeam))
						validMove = true;
					//If they are placing it on an enemy t1 cell
					if ((state == 2) && (owner.playerTeam != currentPlayer.playerTeam))
						validMove = true;
					//If they are placing it on an enemy t0 cell
					if ((state == 1) && (owner.playerTeam != currentPlayer.playerTeam))
						validMove = true;
					break;
				}
			}
			
			return validMove;
		}
		private function removeTween():void
		{
			//Reset Tween
			Starling.juggler.removeTweens(_display);
			tween = null;
			reset();
		}
		public function cascade(OriginatorTileState:int, OriginatorTilesRelativePos:int, tileOwner:Player, originatorOriginalTileState:int, reactionNumber:int):void
		{
			//What was the orginal cell that was placed by the player?
			originalTileState = originatorOriginalTileState;
			_reactionNumber = reactionNumber;
			
			//As long as the cell has not already changed this turn, change it.
			if (!hasAlreadySpilledOver)
			{
				hasAlreadySpilledOver = true;
				flipping = true;
				
				//The original tile was a t1.
				if (OriginatorTileState == 2)
				{
					owner = tileOwner;
					
					//Based on this cells current state, do different actions.
					switch(state)
					{
						case 0:
							state = 1;
							setColor();
							reset();
							break;
						case 1:
							state = 2;
							if (Main.sAnimationsOn)
								addBridge(OriginatorTilesRelativePos);
							else
								spillOver();
							break;
						case 5:
						 	state = 2;
							if (Main.sAnimationsOn)
								addBridge(OriginatorTilesRelativePos);
							else
								spillOver();
							break;
						default:
							flipping = false;
							break;
					}
				}
				//The original tile was a t2.
				else if (OriginatorTileState == 3)
				{
					owner = tileOwner;
					
					//Based on this cells current state, do different actions.
					switch(state)
					{
						case 0:
							state = 2;
							setColor();
							//reset();
							spillOver();
							break;
						case 1:
							state = 2;
							if (Main.sAnimationsOn)
								addBridge(OriginatorTilesRelativePos);
							else
								spillOver();
							break;
						case 2:
							state = 3;
							if (Main.sAnimationsOn)
								addBridge(OriginatorTilesRelativePos);
							else
								spillOver()
							break;
						case 5:
							state = 2;
							if (Main.sAnimationsOn)
								addBridge(OriginatorTilesRelativePos);
							else
								spillOver()
							break;
						case 6:
							state = 3;
							if (Main.sAnimationsOn)
								addBridge(OriginatorTilesRelativePos);
							else
								spillOver()
						default:
							flipping = false;
							break;
					}
				}
				//The original tile was a t3
				else if (OriginatorTileState == 4)
				{
					state = 4;
					owner = tileOwner;
					if (Main.sAnimationsOn)
						Starling.juggler.delayCall(spillOver, .3);
					else
						spillOver()
				}
				//A virus was used.
				else if (OriginatorTileState == 0)
				{
					state = 0;
					owner = null;
					if (Main.sAnimationsOn)
						Starling.juggler.delayCall(spillOver, .3);
					else
						spillOver()
				}
				else
					flipping = false;
			}
			else
			{
				//This Cell has already been changed! But a better move could be "further" down the line so lets check for that.
				//trace("This Cell has already changed! at location: " + location);
				
				//The original tile was a t1.
				if (OriginatorTileState == 2)
				{
					owner = tileOwner;
					
					//Based on this cells current state, do different actions.
					switch(_originalState)
					{
						case 0:
							_potentialState = 1;
							if(_potentialState > state)
							{
								state = _potentialState;
								reset();
							}
							break;
						case 1:
							_potentialState = 2;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
							break;
						case 5:
							_potentialState = 2;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
							break;
						default:
							flipping = false;
							break;
					}
				}
					//The original tile was a t2.
				else if (OriginatorTileState == 3)
				{
					//trace("Originator Tile was a T2");
					
					owner = tileOwner;
					
					//Based on this cells current state, do different actions.
					switch(_originalState)
					{
						case 0:
							//trace("Location " + location + " was originally an empty space, that should evolve into a T1");
							//trace("Location " + location + " current state is: " + state);
							_potentialState = 2;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
								//reset();
							}
							
							break;
						case 1:
							_potentialState = 2;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
							break;
						case 2:
							_potentialState = 3;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
							break;
						case 5:
							_potentialState = 2;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
							break;
						case 6:
							_potentialState = 3;
							if(_potentialState > state)
							{
								state = _potentialState;
								spillOver();
							}
						default:
							flipping = false;
							break;
					}
				}
					//The original tile was a t3
				else if (OriginatorTileState == 4)
				{
					_potentialState = 4;
					
					if(_potentialState > state)
					{
						owner = tileOwner;
						spillOver();
					}
				}
					//A virus was used.
				else if (OriginatorTileState == 0)
				{
					_potentialState = 0;
					
					if(_potentialState > state)
					{
						owner = null;
						spillOver();
					}
				}
			}
		}
		private function addBridge(OriginatorTilesRelativePos:int):void
		{
			//Where is this cell in relation to the cell that is spilling over into it?
			bridge = new MovieClip(Main.assets.getTextures("EnergyTransfer_"), Main.animationFramerate);
			addChild(bridge);
			
			bridge.pivotX = bridge.width/2;
			bridge.pivotY = bridge.height/2;
			
			switch(OriginatorTilesRelativePos)
			{
				case 0:
					bridge.rotation = DEGREE_270;
					break;
				case 1:
					bridge.rotation = DEGREE_90;
					break
				case 3:
					bridge.rotation = DEGREE_180;
					break;
			}
			
			Starling.juggler.add(bridge);
			bridge.addEventListener(Event.COMPLETE, spillOver)
		}
		private function _checkViable(tierToCheck:int):int
		{
			 var viable:int = 0;
			 
			 if(adjacentTopCell.state != 0 && 
			   (adjacentTopCell.state <= tierToCheck || 
		       (adjacentTopCell.state > 4 && 
			   (adjacentTopCell.state - 4) <= tierToCheck)) &&
			   !adjacentTopCell.hasAlreadySpilledOver)
			 {
			 	viable++; 
			 }
			 
			 if(adjacentBottomCell.state != 0 &&
			   (adjacentBottomCell.state <= tierToCheck || 
			   (adjacentBottomCell.state > 4 && 
			   (adjacentBottomCell.state - 4) <= tierToCheck)) &&
			   !adjacentBottomCell.hasAlreadySpilledOver)
			 {
				 viable++; 
			 }
			 
			 if(adjacentLeftCell.state != 0 &&
			   (adjacentLeftCell.state <= tierToCheck || 
	     	   (adjacentLeftCell.state > 4 && 
			   (adjacentLeftCell.state - 4) <= tierToCheck)) &&
			   !adjacentLeftCell.hasAlreadySpilledOver)
			 {
			 	viable++; 
			 }
			 
			 if((adjacentRightCell.state <= tierToCheck || 
			   (adjacentRightCell.state > 4 && 
			   (adjacentRightCell.state - 4) <= tierToCheck)) &&
			   !adjacentRightCell.hasAlreadySpilledOver)
			 {
				 viable++; 
			 }
			 
			 return viable;
		}
		public function spillOver():void
		{
			//trace("Spill Over at location: " + location);
			
			_reactionNumber++;
			
			if (Main.sAnimationsOn)
			{
				//Remove the old cell, changing it out for one of the following animations.
				if(_display != null)
				{
					_display.removeEventListener(Event.COMPLETE, spillOver);
					removeChild(_display);
				}
				
				if(state != 0 && state != 4)
				{
					addRipple();
					gameManager.shiftStack(this);
				}
				
				//If a bridge animatin was played, remove it.
				if(bridge)
				{
					bridge.stop();
					removeChild(bridge);
				}
			}
				
			//How many adjacent spaces are empty?
			var numOfViableAdjacentTiles:int = 0;
			rotation = 0;
			
			//Checking for empty spaces
			if (state != 4 && state != 0)
			{
				if (adjacentTopCell.state == 0 && adjacentTopCell.hasAlreadySpilledOver == false)
				{
					numOfViableAdjacentTiles++;
					top = true;
				}
				if (adjacentRightCell.state == 0 && adjacentRightCell.hasAlreadySpilledOver == false)
				{
					numOfViableAdjacentTiles++;
					right = true;
				}
				if (adjacentBottomCell.state == 0 && adjacentBottomCell.hasAlreadySpilledOver == false)
				{
					numOfViableAdjacentTiles++;
					bottom = true;
				}
				if (adjacentLeftCell.state == 0 && adjacentLeftCell.hasAlreadySpilledOver == false)
				{
					numOfViableAdjacentTiles++;
					left = true;
				}
			}

			var numOfCascadingCells:int = 0;
			
			//Checking for Neighboring Cells of the -1 type.
			switch(state)
			{
				case 2:
				{
					//trace("T1 Cell Spreading at location " + location);
					
					if ((adjacentTopCell.state == 1) || (adjacentTopCell.state == 5))
					{
						adjacentTopCell.cascade(state, 0, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
						//trace("Cascading to the Top from location " + location);
					}
					if ((adjacentBottomCell.state == 1) || (adjacentBottomCell.state == 5))
					{
						adjacentBottomCell.cascade(state, 1, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
						//trace("Cascading to the Bottom from location " + location);
					}
					if ((adjacentRightCell.state == 1) || (adjacentRightCell.state == 5))
					{
						adjacentRightCell.cascade(state, 2, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
						//trace("Cascading to the Right from location " + location);
					}
					if ((adjacentLeftCell.state == 1) || (adjacentLeftCell.state == 5))
					{
						adjacentLeftCell.cascade(state, 3, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
						//trace("Cascading to the Left from location " + location);
					}
					break;
				}
				case 3:
				{
					//trace("T2 Cell Spreading at location " + location);
					
					if ((adjacentTopCell.state == 1) || (adjacentTopCell.state == 2) || (adjacentTopCell.state == 5) || (adjacentTopCell.state == 6))
					{
						//trace("Cascade UP to cell " + adjacentTopCell.location);
						adjacentTopCell.cascade(state, 0, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
						//trace("Cascading to the Top from location " + location);
					}
					if ((adjacentBottomCell.state == 1) || (adjacentBottomCell.state == 2) || (adjacentBottomCell.state == 5) || (adjacentBottomCell.state == 6))
					{
						//trace("Cascade DOWN to cell " + adjacentBottomCell.location);
						adjacentBottomCell.cascade(state, 1, owner,  originalTileState, _reactionNumber);
						numOfCascadingCells++;
						//trace("Cascading to the Bottom from location " + location);
					}
					if ((adjacentRightCell.state == 1) || (adjacentRightCell.state == 2) || (adjacentRightCell.state == 5) || (adjacentRightCell.state == 6))
					{
						//trace("Cascade RIGHT to cell " + adjacentRightCell.location);
						adjacentRightCell.cascade(state, 2, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
						//trace("Cascading to the Right from location " + location);
					}
					if ((adjacentLeftCell.state == 1) || (adjacentLeftCell.state == 2) || (adjacentLeftCell.state == 5) || (adjacentLeftCell.state == 6))
					{
						//trace("Cascade LEFT to cell " + adjacentLeftCell.location);
						adjacentLeftCell.cascade(state, 3, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
						//trace("Cascading to the Left from location " + location);
					}	
					break;
				}
				case 4:
				{
					//trace("Creating Walls")
					//t3 cells can only spill over onto neighboring t3 cells.
					if (adjacentTopCell.state == 3)
					{
						adjacentTopCell.cascade(state, 0, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
					}
					if (adjacentBottomCell.state == 3)
					{
						adjacentBottomCell.cascade(state, 1, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
					}
					if (adjacentRightCell.state == 3)
					{
						adjacentRightCell.cascade(state, 2, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
					}
					if (adjacentLeftCell.state == 3)
					{
						adjacentLeftCell.cascade(state, 3, owner, originalTileState, _reactionNumber);
						numOfCascadingCells++;
					}
					break;
				}
				case 0:
				{
					//trace("Spreading a Virus");
					//Spreading the effect of a virus!
					switch(originalTileState)
					{
						case 1:
						{
							//Check all adjacent sides for t0 of any color & grey t0
							if (adjacentTopCell.state == 1 || adjacentTopCell.state == 5)
							{
								adjacentTopCell.cascade(state, 0, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							if (adjacentBottomCell.state == 1 || adjacentBottomCell.state == 5)
							{
								adjacentBottomCell.cascade(state, 1, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							if (adjacentRightCell.state == 1 || adjacentRightCell.state == 5)
							{
								adjacentRightCell.cascade(state, 2, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							if (adjacentLeftCell.state == 1 || adjacentLeftCell.state == 5)
							{
								adjacentLeftCell.cascade(state, 3, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							break;
						}
						case 2:
						{
							//Check all adjacent sides for t1 of any color & grey t1
							if (adjacentTopCell.state == 2 || adjacentTopCell.state == 6)
							{
								adjacentTopCell.cascade(state, 0, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							if (adjacentBottomCell.state == 2 || adjacentBottomCell.state == 6)
							{
								adjacentBottomCell.cascade(state, 1, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							if (adjacentRightCell.state == 2 || adjacentRightCell.state == 6)
							{
								adjacentRightCell.cascade(state, 2, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							if (adjacentLeftCell.state == 2 || adjacentLeftCell.state == 6)
							{
								adjacentLeftCell.cascade(state, 3, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							break;
						}
						case 3:
						{
							//Check all adjacent sides for t2 of any color
							if (adjacentTopCell.state == 3)
							{
								adjacentTopCell.cascade(state, 0, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							if (adjacentBottomCell.state == 3)
							{
								adjacentBottomCell.cascade(state, 1, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							if (adjacentRightCell.state == 3)
							{
								adjacentRightCell.cascade(state, 2, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							if (adjacentLeftCell.state == 3)
							{
								adjacentLeftCell.cascade(state, 3, owner, originalTileState, _reactionNumber);
								numOfCascadingCells++;
							}
							break;
						}
							
						default:
							break;
					}
					break;
				}
					
				default:
					break;
			}
			
			if (Main.sAnimationsOn)
			{
				if(numOfViableAdjacentTiles > 0 || numOfCascadingCells > 0)
					SoundController.playSpreadSound(_reactionNumber);
			}
			
			switch(numOfViableAdjacentTiles)
			{
				case 0:
				{
					//Not spreading in any direction, just go ahead and evolve.
					switch(state)
					{
						case 2:
						{
							//trace("Not Spreading, Just Evolving from T1 to T2");
							
							//Evolving from a t1 to t2
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Evolution_1To2_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Evolution_1To2_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_1To2_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_1To2_"), Main.animationFramerate);
									break;
							}
							break;
						}
						case 3:
						{
							//trace("Not Spreading, Just Evolving from T2 to T3");
							
							//Evolving from a t2 to t3
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Evolution_2To3_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Evolution_2To3_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_2To3_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_2To3_"), Main.animationFramerate);
									break;
							}
							break;
						}
						case 4:
						{
							//trace("Not Spreading, Just Evolving from T3 to T4");
							
							//Evolving from a t3 to t4
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Evolution_3To4_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Evolution_3To4_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_3To4_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_3To4_"), Main.animationFramerate);
									break;
							}
							break;
						}
						case 0:
						{
							//trace("Destruct Animation");
							
							//Destruction Animation
							
							//If no owner, its a grey cell.
							if(previousOwner == null)
							{
								switch(originalTileState)
								{
									case 1:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Grey_Destruction_Stage1_"), Main.animationFramerate);
										break;
									
									case 2:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Grey_Destruction_Stage2_"), Main.animationFramerate);
										break;
								}
							}
							else
							{
								//Who was the previous Owner?
								switch(previousOwner.playerTeam)
								{
									case 0:
										//What was the original tile state?
										switch(originalTileState)
										{
											case 1:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Green_Destruction_Stage1_"), Main.animationFramerate);
												break;
											case 2:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Green_Destruction_Stage2_"), Main.animationFramerate);
												break;
											case 3:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Green_Destruction_Stage3_"), Main.animationFramerate);
												break;
										}
										break;
									
									case 1:
										//What was the original tile state?
										switch(originalTileState)
										{
											case 1:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Red_Destruction_Stage1_"), Main.animationFramerate);
												break;
											case 2:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Red_Destruction_Stage2_"), Main.animationFramerate);
												break;
											case 3:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Red_Destruction_Stage3_"), Main.animationFramerate);
												break;
										}
										break;
									
									case 2:
										//What was the original tile state?
										switch(originalTileState)
										{
											case 1:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Blue_Destruction_Stage1_"), Main.animationFramerate);
												break;
											case 2:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Blue_Destruction_Stage2_"), Main.animationFramerate);
												break;
											case 3:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Blue_Destruction_Stage3_"), Main.animationFramerate);
												break;
										}
										break;
									
									case 3:
										//What was the original tile state?
										switch(originalTileState)
										{
											case 1:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Yellow_Destruction_Stage1_"), Main.animationFramerate);
												break;
											case 2:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Yellow_Destruction_Stage2_"), Main.animationFramerate);
												break;
											case 3:
												if (Main.sAnimationsOn)
													_display = new MovieClip(Main.assets.getTextures("Yellow_Destruction_Stage3_"), Main.animationFramerate);
												break;
										}
										break;
								}
								break;
							}
						}
					}
					
					if (Main.sAnimationsOn)
					{
						addDisplay();
						Starling.juggler.add(_display);
						_display.addEventListener(Event.COMPLETE, reset);
					}
					else
						reset();
					break;
				}
				case 1:
				{
					//trace("Spreading in 1 Direction at location " + location);
					
					// Spreading in 1 direction
					switch(state)
					{
						case 2:
						{
							//Speading t1 cells
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division1_Stage1_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division1_Stage1_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division1_Stage1_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division1_Stage1_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						}
						case 3:
						{
							//Speading t2 cells
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division1_Stage2_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division1_Stage2_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division1_Stage2_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division1_Stage2_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						}
						default:
							break;
					}
					
					if (Main.sAnimationsOn)
					{
						//Determine Orientation
						if (top)
							_display.rotation = DEGREE_270;
						if (bottom)
							_display.rotation = DEGREE_90
						if (left)
							_display.rotation = DEGREE_180;
					}
					
					if (Main.sAnimationsOn)
					{
						addDisplay();
						Starling.juggler.add(_display);
						_display.addEventListener(Event.COMPLETE, animationWatcher);
					}
					else
						animationWatcher();
					
					break;
				}
				case 2:
				{
					//trace("Spreading in 2 Directions at location " + location);
					
					//If 2 sides are spreading to an open space
					var adjacent:Boolean = true;
					
					//Opposite Sides are spreading
					if(top && bottom || left && right)
						adjacent = false;
					
					if (adjacent)
					{
						//Spreading on two adjacent sides
						switch(state)
						{
							case 2:
							{
								//Speading t1 cells
								switch(owner.playerTeam)
								{
									case 0:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Green_Division2_Stage1_"), Main.animationFramerate);
										break;
									case 1:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Red_Division2_Stage1_"), Main.animationFramerate);
										break;
									case 2:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Blue_Division2_Stage1_"), Main.animationFramerate);
										break;
									case 3:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Yellow_Division2_Stage1_"), Main.animationFramerate);
										break;
									default:
										break;
								}
								break;
							}
							case 3:
							{
								//Speading t2 cells
								switch(owner.playerTeam)
								{
									case 0:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Green_Division2_Stage2_"), Main.animationFramerate);
										break;
									case 1:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Red_Division2_Stage2_"), Main.animationFramerate);
										break;
									case 2:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Blue_Division2_Stage2_"), Main.animationFramerate);
										break;
									case 3:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Yellow_Division2_Stage2_"), Main.animationFramerate);
										break;
									default:
										break;
								}
								break;
							}
							default:
								break;
						}
						
						if (Main.sAnimationsOn)
						{
							//determine orientation
							if (right && top)
								_display.rotation = DEGREE_270;
							if (bottom && left)
								_display.rotation = DEGREE_90;
							if (left && top)
								_display.rotation = DEGREE_180;
						}
						
						if (Main.sAnimationsOn)
						{
							addDisplay();
							Starling.juggler.add(_display);
							_display.addEventListener(Event.COMPLETE, animationWatcher);
						}
						else
							animationWatcher();
					}
					else
					{
						//Spreading in opposite directions
						switch(state)
						{
							case 2:
							{	
								//Speading t1 cells
								switch(owner.playerTeam)
								{
									case 0:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Green_Division2_Across_Stage1_"), Main.animationFramerate);
										break;
									case 1:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Red_Division2_Across_Stage1_"), Main.animationFramerate);
										break;
									case 2:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Blue_Division2_Across_Stage1_"), Main.animationFramerate);
										break;
									case 3:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Yellow_Division2_Across_Stage1_"), Main.animationFramerate);
										break;
									default:
										break;
								}
								break;
							}
							case 3:
							{
								//Speading t2 cells
								switch(owner.playerTeam)
								{
									case 0:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Green_Division2_Across_Stage2_"), Main.animationFramerate);
										break;
									case 1:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Red_Division2_Across_Stage2_"), Main.animationFramerate);
										break;
									case 2:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Blue_Division2_Across_Stage2_"), Main.animationFramerate);
										break;
									case 3:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Yellow_Division2_Across_Stage2_"), Main.animationFramerate);
										break;
									default:
										break;
								}
								break;
							}
							default:
								break;
						}
						
						if (Main.sAnimationsOn)
						{
							//determine orientation
							if(top && bottom)
								_display.rotation = DEGREE_90;
						}
						
						if (Main.sAnimationsOn)
						{
							addDisplay();
							Starling.juggler.add(_display);
							_display.addEventListener(Event.COMPLETE, animationWatcher);
						}
						else
							animationWatcher();
					}
					break;
				}
				case 3:
				{
					//trace("Spreading in 3 Directions at location " + location);
					
					//Spreading in 3 directions
					switch(state)
					{
						case 2:
						{
							//trace("spreading in 3 directions");
							
							//Speading t1 cells
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division3_Stage1_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division3_Stage1_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division3_Stage1_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division3_Stage1_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						} 
						case 3:
						{
							//Speading t2 cells
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division3_Stage2_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division3_Stage2_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division3_Stage2_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division3_Stage2_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						}
						default:
							break;
					}
					
					if (Main.sAnimationsOn)
					{
						//determine orientation
						if (right && bottom && top)
							_display.rotation = DEGREE_270;
						if (bottom && left && top)
							_display.rotation = DEGREE_90;
						if (left && top && right)
							_display.rotation = DEGREE_180;
					}
					
					if (Main.sAnimationsOn)
					{
						addDisplay();
						Starling.juggler.add(_display);
						_display.addEventListener(Event.COMPLETE, animationWatcher);
					}
					else
						animationWatcher();
					
					break;
				}
				case 4:
				{
					//trace("Spreading in 4 Directions at location " + location);
					
					//Spreading in four directions.
					switch(state)
					{
						case 2:
						{
							//Spreading Dots
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division4_Stage1_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division4_Stage1_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division4_Stage1_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division4_Stage1_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						}
						case 3:
						{
							//Spreading Circles
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division4_Stage2_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division4_Stage2_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division4_Stage2_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division4_Stage2_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						}
					}
					
					if (Main.sAnimationsOn)
					{
						addDisplay();
						Starling.juggler.add(_display);
						_display.addEventListener(Event.COMPLETE, animationWatcher);
					}
					else
						animationWatcher();
					
					break;
				}
			}
		}
		public function animationWatcher():void
		{
			if (Main.sAnimationsOn)
			{
				//Stop the animation & remove it.
				_display.stop();
				_display.removeEventListener(Event.COMPLETE, animationWatcher);
				removeChild(_display);
				
				//Reset orientation
				_display.rotation = 0;
			}
			
			//trace("Spreading to the following Adjacent Cells:");
			
			//Tell adjacent cells to go play their corrisponding animation.
			if (top)
			{
				adjacentTopCell.cascade(state, -1, owner, originalTileState, _reactionNumber);	
				//trace("Top");
			}
			if (right)
			{
				adjacentRightCell.cascade(state, -1, owner, originalTileState, _reactionNumber);
				//trace("Right");
			}
			if (bottom)
			{
				adjacentBottomCell.cascade(state, -1, owner, originalTileState, _reactionNumber);
				//trace("bottom");
			}
			if (left)
			{
				adjacentLeftCell.cascade(state, -1, owner, originalTileState, _reactionNumber);	
				//trace("left");
			}
			//trace("--------------------------");
			
			//Evolve the Cell.
			switch(state)
			{
				case 2:
				{
					//Evolve from t1 to t2.
					switch(owner.playerTeam)
					{
						case 0:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Green_Evolution_1To2_"), Main.animationFramerate);
							break;
						case 1:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Red_Evolution_1To2_"), Main.animationFramerate);
							break;
						case 2:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_1To2_"), Main.animationFramerate);
							break;
						case 3:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_1To2_"), Main.animationFramerate);
							break;
						default:
							break;
					}
					break;
				}
				case 3:
				{
					//Evolve from t2 to t3.
					switch(owner.playerTeam)
					{
						case 0:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Green_Evolution_2To3_"), Main.animationFramerate);
							break;
						case 1:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Red_Evolution_2To3_"), Main.animationFramerate);
							break;
						case 2:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_2To3_"), Main.animationFramerate);
							break;
						case 3:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_2To3_"), Main.animationFramerate);
							break;
						default:
							break;
					}
					break;
				}
				default:
					break;
			}
			
			if (Main.sAnimationsOn)
			{
				addDisplay();
				Starling.juggler.add(_display);
				_display.addEventListener(Event.COMPLETE, reset);
			}
			else
				reset();
		}
		public function reset():void
		{
			if (Main.sAnimationsOn)
			{
				//Stop animation
				if(_display != null)
				{
					_display.stop();
					_display.rotation = 0;
					removeChild(_display);
					Starling.juggler.remove(_display);
					_display.removeEventListener(Event.COMPLETE, infectionListener);
					_display.removeEventListener(Event.COMPLETE, reset);
				}
			}
			
			//Set its final color.
			setColor();
			
			//reset variables
			stateChange = false;
			top = false;
			right = false;
			bottom = false;
			left = false;
			flipping = false;
			
			if(whiteFlash)
				removeWhiteFlash();
			
			if(capturePoint)
				setCapturePoint();
			
			determineBranches();
		}
		public function determineBranches():void
		{
			if(state != 10 && state != 4 && state != 9 && _display)
				drawBranches();
			else
				hideBranches();
		}
		private function drawBranches():void
		{
			var ownerHexColor:int = 0x989898;
			var branchLength:int = 5 * PathogenTest.HD_Multiplyer;
			var branchHeight:int = 2 * PathogenTest.HD_Multiplyer;
			
			//Determining Owners Color
			if(owner)
				ownerHexColor = owner.playerHexColor
				
			if(_leftBranch == null)
			{
				_leftBranch = new Quad(branchLength, branchHeight, ownerHexColor);
				addChild(_leftBranch);
				_leftBranch.alpha = .75;
				_leftBranch.pivotY = _leftBranch.height/2;
				_leftBranch.visible =  false;
			}
			if(_rightBranch == null)
			{
				_rightBranch = new Quad(branchLength, branchHeight, ownerHexColor);
				addChild(_rightBranch);
				_rightBranch.alpha = .75;
				_rightBranch.pivotY = _rightBranch.height/2;
				_rightBranch.visible =  false;
			}
			if(_topBranch == null)
			{
				_topBranch = new Quad(branchHeight, branchLength, ownerHexColor);
				addChild(_topBranch);
				_topBranch.alpha = .75;
				_topBranch.pivotX = _topBranch.width/2;
				_topBranch.visible =  false;
			}
			if(_bottomBranch == null)
			{
				_bottomBranch = new Quad(branchHeight, branchLength, ownerHexColor);
				addChild(_bottomBranch);
				_bottomBranch.alpha = .75;
				_bottomBranch.pivotX = _bottomBranch.width/2;
				_bottomBranch.visible =  false;
			}
			
			if(adjacentLeftCell)
			{
				var leftBridgeShown:Boolean;
				
				if(adjacentLeftCell.state == state || state == 1 && adjacentLeftCell.state == 5 || state == 2 && adjacentLeftCell.state == 6 || state == 5 && adjacentLeftCell.state == 1 || state == 6 && adjacentLeftCell.state == 2)
				{
					_leftBranch.visible = true;
					_leftBranch.color = ownerHexColor;
					_leftBranch.x = -(_display.width/2) - (_leftBranch.width/2);
					leftBridgeShown = true;
				}
				if(!leftBridgeShown)
					_leftBranch.visible = false;
			}
			
			if(adjacentRightCell)
			{
				var rightBridgeShown:Boolean;
				
				if(adjacentRightCell.state == state || state == 1 && adjacentRightCell.state == 5 || state == 2 && adjacentRightCell.state == 6 || state == 5 && adjacentRightCell.state == 1 || state == 6 && adjacentRightCell.state == 2)
				{
					_rightBranch.visible = true;
					_rightBranch.color = ownerHexColor;
					_rightBranch.x = (_display.width/2) - (_rightBranch.width/2);
					rightBridgeShown = true;
				}
				if(!rightBridgeShown)
					_rightBranch.visible = false;
			}
			if(adjacentTopCell)
			{
				var topBridgeShown:Boolean;
				
				if(adjacentTopCell.state == state || state == 1 && adjacentTopCell.state == 5 || state == 2 && adjacentTopCell.state == 6 || state == 5 && adjacentTopCell.state == 1 || state == 6 && adjacentTopCell.state == 2)
				{
					_topBranch.visible = true;
					_topBranch.color = ownerHexColor;
					_topBranch.y = -(_display.height/2) - (_topBranch.height/2);
					topBridgeShown = true;
				}
				if(!topBridgeShown)
					_topBranch.visible = false;
			}
			if(adjacentBottomCell)
			{
				var bottomBridgeShown:Boolean;
				
				if(adjacentBottomCell.state == state || state == 1 && adjacentBottomCell.state == 5 || state == 2 && adjacentBottomCell.state == 6|| state == 5 && adjacentBottomCell.state == 1 || state == 6 && adjacentBottomCell.state == 2)
				{
					//trace("The adjacent left cell is the same as this one");
					_bottomBranch.visible = true;
					_bottomBranch.color = ownerHexColor;
					_bottomBranch.y = (_display.height/2) - (_bottomBranch.height/2);
					bottomBridgeShown = true;
				}
				if(!bottomBridgeShown)
					_bottomBranch.visible = false;
			}		
		}
		private function hideBranches():void
		{
			if(_leftBranch)
				_leftBranch.visible = false;
			if(_rightBranch)
				_rightBranch.visible = false;
			if(_topBranch)
				_topBranch.visible = false;
			if(_bottomBranch)
				_bottomBranch.visible = false;
		}
		public function setColor():void
		{
			if(_display)
				removeChild(_display);
			
			//Render the cell
			switch(state)
			{
				case 0:
					_display = null;
					
					if(capturePoint)
						setCapturePoint();
					else
						addGrid();
					break;
				
				case 1:
					//T1 Cell
					switch(owner.playerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage01"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage01"), Main.animationFramerate);
							break;
						case 2:
							_display = new MovieClip(Main.assets.getTextures("Blue_Stage01"), Main.animationFramerate);
							break;
						case 3:
							_display = new MovieClip(Main.assets.getTextures("Yellow_Stage01"), Main.animationFramerate);
							break;
					}
					break;
				case 2:
					//T2 Cells
					switch(owner.playerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage02"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage02"), Main.animationFramerate);
							break;
						case 2:
							_display = new MovieClip(Main.assets.getTextures("Blue_Stage02"), Main.animationFramerate);
							break;
						case 3:
							_display = new MovieClip(Main.assets.getTextures("Yellow_Stage02"), Main.animationFramerate);
							break;
					}
					break;
				case 3:
					//T3 Cells
					switch(owner.playerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage03"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage03"), Main.animationFramerate);
							break;
						case 2:
							_display = new MovieClip(Main.assets.getTextures("Blue_Stage03"), Main.animationFramerate);
							break;
						case 3:
							_display = new MovieClip(Main.assets.getTextures("Yellow_Stage03"), Main.animationFramerate);
							break;
					}
					break;
				case 4:
					//T4 Cells
					switch(owner.playerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage04"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage04"), Main.animationFramerate);
							break;
						case 2:
							_display = new MovieClip(Main.assets.getTextures("Blue_Stage04"), Main.animationFramerate);
							break;
						case 3:
							_display = new MovieClip(Main.assets.getTextures("Yellow_Stage04"), Main.animationFramerate);
							break;
					}
					break;
				case 5:
					//Grey T1 Cell
					_display = new MovieClip(Main.assets.getTextures("Grey_Stage01"), Main.animationFramerate);
					break;
				case 6:
					//Grey T2 Cell
					_display = new MovieClip(Main.assets.getTextures("Grey_Stage02"), Main.animationFramerate);
					break;
				case 9:
					//Grey T3 Cell
					_display = new MovieClip(Main.assets.getTextures("Grey_Stage04"), Main.animationFramerate);
					break;
			}
			
			if (Main.sAnimationsOn)
				addDisplay();
		}
		protected function addDisplay():void
		{
			//Add the display to the screen.
			if(_display != null)
			{
				addChild(_display);
				
				_display.x = 0;
				_display.y = 0;
				
				_display.pivotX = _display.width/2;
				_display.pivotY = _display.height/2;
				
				if(grid != null)
				{
					if(gameManager && !gameManager.undoingBool)
					{
						var gridTween:Tween = new Tween(grid, .5);
						gridTween.fadeTo(0);
						Starling.juggler.add(gridTween);
						gridTween.onComplete = removeGrid;
					}
					else
						removeGrid();
				}
			}
		}
		public function addDisplayForNoAnimations():void
		{
			//trace("Adding Display for no animations");
			
			//Add the display to the screen.
			if(_display != null)
			{
				removeChild(_display);
				addChild(_display);
				
				_display.x = 0;
				_display.y = 0;
				
				_display.pivotX = _display.width/2;
				_display.pivotY = _display.height/2;
				
				if(grid != null)
				{
					if(gameManager && !gameManager.undoingBool)
					{
						var gridTween:Tween = new Tween(grid, .5);
						gridTween.fadeTo(0);
						Starling.juggler.add(gridTween);
						gridTween.onComplete = removeGrid;
					}
					else
						removeGrid();
				}
			}
		}
		protected function addGrid():void
		{
			if(grid == null)
			{
				grid =  new Image(Main.assets.getTexture("Grid"));
				grid.pivotX = grid.width/2;
				grid.pivotY = grid.height/2;
				addChild(grid);
				
				if(gameManager && !gameManager.undoingBool)
				{
					grid.alpha = 0;
					
					tween = new Tween(grid, .5);
					tween.fadeTo(1);
					Starling.juggler.add(tween);
				}
			}
		}
		private function removeGrid():void
		{
			if(grid)
			{
				grid.alpha = 1;
				removeChild(grid);
				grid = null;
			}
		}
		public function setCapturePoint():void
		{
			if(grid != null)
				removeChild(grid);
			
			capturePoint = true;
			
			if(state == 0)
			{
				grid =  new MovieClip(Main.assets.getTextures("Capture_Zone"), Main.animationFramerate);
				grid.pivotX = grid.width/2;
				grid.pivotY = grid.height/2;
				addChild(grid);
				
				if(Main.sAnimationsOn && !gameManager.replay)
				{
					grid.alpha = 0;
					tween = new Tween(grid, .5);
					tween.fadeTo(1);
					Starling.juggler.add(tween);
				}
				
				setChildIndex(grid, 0);
			}
		}
		public function assignCaptureZone(zoneNumber:int):void
		{
			captureZoneNumber = zoneNumber;
			
			//If an adjacent cell is a capture point, but has not joined a group, Join this current group.
			if (adjacentBottomCell != null)
			{
				if (adjacentBottomCell.capturePoint == true && adjacentBottomCell.captureZoneNumber == 0)
					adjacentBottomCell.assignCaptureZone(captureZoneNumber);
			}
			if (adjacentLeftCell != null)
			{
				if (adjacentLeftCell.capturePoint == true && adjacentLeftCell.captureZoneNumber == 0)
					adjacentLeftCell.assignCaptureZone(captureZoneNumber);
			}
			if (adjacentRightCell != null)
			{
				if (adjacentRightCell.capturePoint == true && adjacentRightCell.captureZoneNumber == 0)
					adjacentRightCell.assignCaptureZone(captureZoneNumber);
			}
			if (adjacentTopCell != null)
			{
				if (adjacentTopCell.capturePoint == true && adjacentTopCell.captureZoneNumber == 0)
					adjacentTopCell.assignCaptureZone(captureZoneNumber);
			}
		}
		public function promote():void
		{
			if(_display != null)
				removeChild(_display);
			
			capturePoint = false;
			
			switch(state)
			{
				case 1:
				{
					switch(owner.playerTeam)
					{
						case 0:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Green_Evolution_3To4_"), Main.animationFramerate);
							break;
						case 1:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Red_Evolution_3To4_"), Main.animationFramerate);
							break;
						case 2:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_3To4_"), Main.animationFramerate);
							break;
						case 3:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_3To4_"), Main.animationFramerate);
							break;
					}
					break;
				}
				case 2:
				{
					switch(owner.playerTeam)
					{
						case 0:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Green_Evolution_3To4_"), Main.animationFramerate);
							break;
						case 1:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Red_Evolution_3To4_"), Main.animationFramerate);
							break;
						case 2:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_3To4_"), Main.animationFramerate);
							break;
						case 3:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_3To4_"), Main.animationFramerate);
							break;
					}
					break;
				}
				case 3:
				{
					switch(owner.playerTeam)
					{
						case 0:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Green_Evolution_3To4_"), Main.animationFramerate);
							break;
						case 1:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Red_Evolution_3To4_"), Main.animationFramerate);
							break;
						case 2:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_3To4_"), Main.animationFramerate);
							break;
						case 3:
							if (Main.sAnimationsOn)
								_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_3To4_"), Main.animationFramerate);
							break;
					}
					break;
				}
			}
			
			if(state == 0)
			{
				state = 9;
				setColor();
				
				if (Main.sAnimationsOn)
				{
					// Have the cell scale & fade into existance
					_display.alpha = 0;
					_display.scaleX = 0;
					_display.scaleY = 0;
					
					var tweenTime:Number = 0.3;
					tween = new Tween(_display, 1);
					tween.fadeTo(1);
					tween.scaleTo(1);
					tween.onComplete = removeTween;
					Starling.juggler.add(tween);
				}
				else
					reset();
			}
			else
				state = 4;
			
			
			if(state != 9)
			{
				if (Main.sAnimationsOn)
				{
					addDisplay();
					Starling.juggler.add(_display);
					_display.addEventListener(Event.COMPLETE, reset);
				}
				else
					reset();
			}
		}
		public function assignErosionZone(zoneNumber:int):void
		{
			erosionZoneNumber = zoneNumber;
			
			if (adjacentBottomCell != null)
			{
				if (adjacentBottomCell.erosionCell == true && adjacentBottomCell.erosionZoneNumber == 0)
				{
					adjacentBottomCell.assignErosionZone(erosionZoneNumber);
				}
			}
			if (adjacentLeftCell != null)
			{
				if (adjacentLeftCell.erosionCell == true && adjacentLeftCell.erosionZoneNumber == 0)
				{
					adjacentLeftCell.assignErosionZone(erosionZoneNumber);
				}
			}
			if (adjacentRightCell != null)
			{
				if (adjacentRightCell.erosionCell == true && adjacentRightCell.erosionZoneNumber == 0)
				{
					adjacentRightCell.assignErosionZone(erosionZoneNumber);
				}
			}
			if (adjacentTopCell != null)
			{
				if (adjacentTopCell.erosionCell == true && adjacentTopCell.erosionZoneNumber == 0)
				{
					adjacentTopCell.assignErosionZone(erosionZoneNumber);
				}
			}
			
			//_display.color = Color.RED;
		}
		public function CleanCell():void
		{	
			state = 0;
			owner = null;
			gameManager = null;
			currentPlayer = null;
			selectedCellType = null;
			previousOwner = null;
			
			removeWhiteFlash();
			
			if(captureZoneNumber>0)
			{
				capturePoint = false;
				captureZoneNumber = 0;
				
				removeChild(grid);
				grid = null;
			}
			
			if(erosionZoneNumber >0)
			{
				erosionCell = false;
				erosionZoneNumber = 0;
				
				if(_display)
					removeChild(_display);
				
				_display = null;
			}
			
			reset();
		}
		public function decayCell():void
		{
			erosionCell = false;
			capturePoint = false;
			captureZoneNumber = 0;
			
			state = 0;
			tween = new Tween(_display,1);
			tween.fadeTo(0);
			tween.scaleTo(0);
			Starling.juggler.add(tween);
			tween.onComplete = reset;
		}
		public function spreadInfection():void
		{
			var numOfViableLocations:int = 0;
			infected = true;
			
			//If an adjacent space is empty, spread into it.
			if (adjacentBottomCell.state == 0 && !adjacentBottomCell.infected)
			{
				numOfViableLocations++;
				bottom = true;
				adjacentBottomCell.owner = owner;
				adjacentBottomCell.state = state;
				adjacentBottomCell.infected = true;
			}
			if (adjacentLeftCell.state == 0 && !adjacentLeftCell.infected)
			{
				numOfViableLocations++;
				left = true
				adjacentLeftCell.owner = owner;
				adjacentLeftCell.state = state;
				adjacentLeftCell.infected = true;
			}
			if (adjacentRightCell.state == 0 && !adjacentRightCell.infected)
			{
				numOfViableLocations++;
				right = true;
				adjacentRightCell.owner = owner;
				adjacentRightCell.state = state;
				adjacentRightCell.infected = true;
			}
			if (adjacentTopCell.state == 0 && !adjacentTopCell.infected)
			{
				numOfViableLocations++;
				top = true;
				adjacentTopCell.owner = owner;
				adjacentTopCell.state = state;
				adjacentTopCell.infected = true;
			}
			
			if(numOfViableLocations > 0)
			{
				addRipple();
				gameManager.shiftStack(this);
			}
			
			//Based on the number of available adjacent spaces, play the corrisponding animation.
			if (numOfViableLocations > 0)
			{
				if(_display != null)
					removeChild(_display);
				
				if (numOfViableLocations == 1)
				{
					// Spreading in 1 direction
					switch(state)
					{
						case 1:
						{
							//Speading t1 cells
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division1_Stage1_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division1_Stage1_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division1_Stage1_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division1_Stage1_"), Main.animationFramerate);
									break;
							}
							break;
						}
						case 2:
						{
							//Speading t2 cells
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division1_Stage2_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division1_Stage2_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division1_Stage2_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division1_Stage2_"), Main.animationFramerate);
									break;
							}
							break;
						}
							
						default:
							break;
					}
					
					if (Main.sAnimationsOn)
					{
						//Determine Orientation
						if (top)
							_display.rotation = DEGREE_270;
						if (bottom)
							_display.rotation = DEGREE_90;
						if (left)
							_display.rotation = DEGREE_180;
					}
					
					if (Main.sAnimationsOn)
					{
						addDisplay();
						Starling.juggler.add(_display);
						_display.addEventListener(Event.COMPLETE, infectionListener);
					}
					else
					{
						if (top)
							adjacentTopCell.reset();
						if (right)
							adjacentRightCell.reset();
						if (bottom)
							adjacentBottomCell.reset();
						if (left)
							adjacentLeftCell.reset();
						
						reset();
					}
				}
				if (numOfViableLocations == 2)
				{
					// Spreading in 2 directions
					var adjacent:Boolean = true;
					
					//Opposite Sides are spreading
					if(top && bottom || left && right)
						adjacent = false;
					
					if (adjacent)
					{
						//Spreading on two adjacent sides
						switch(state)
						{
							case 1:
							{
								//Speading t1 cells
								switch(owner.playerTeam)
								{
									case 0:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Green_Division2_Stage1_"), Main.animationFramerate);
										break;
									case 1:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Red_Division2_Stage1_"), Main.animationFramerate);
										break;
									case 2:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Blue_Division2_Stage1_"), Main.animationFramerate);
										break;
									case 3:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Yellow_Division2_Stage1_"), Main.animationFramerate);
										break;
								}
								break;
							}
							case 2:
							{
								//Speading t2 cells
								switch(owner.playerTeam)
								{
									case 0:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Green_Division2_Stage2_"), Main.animationFramerate);
										break;
									case 1:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Red_Division2_Stage2_"), Main.animationFramerate);
										break;
									case 2:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Blue_Division2_Stage2_"), Main.animationFramerate);
										break;
									case 3:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Yellow_Division2_Stage2_"), Main.animationFramerate);
										break;
								}
								break;
							}
								
							default:
								break;
						}
						
						if (Main.sAnimationsOn)
						{
							//determine orientation
							if (right && top)
								_display.rotation = DEGREE_270;
							if (bottom && left)
								_display.rotation = DEGREE_90;
							if (left && top)
								_display.rotation = DEGREE_180;
						}
						
						if (Main.sAnimationsOn)
						{
							addDisplay();
							Starling.juggler.add(_display);
							_display.addEventListener(Event.COMPLETE, infectionListener);
						}
						else
						{
							if (top)
								adjacentTopCell.reset();
							if (right)
								adjacentRightCell.reset();
							if (bottom)
								adjacentBottomCell.reset();
							if (left)
								adjacentLeftCell.reset();
							
							reset();
						}
					}
					else
					{
						//Spreading in opposite directions
						switch(state)
						{
							case 1:
							{
								//Speading t1 cells
								switch(owner.playerTeam)
								{
									case 0:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Green_Division2_Across_Stage1_"), Main.animationFramerate);
										break;
									case 1:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Red_Division2_Across_Stage1_"), Main.animationFramerate);
										break;
									case 2:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Blue_Division2_Across_Stage1_"), Main.animationFramerate);
										break;
									case 3:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Yellow_Division2_Across_Stage1_"), Main.animationFramerate);
										break;
								}
								break;
							}
							case 2:
							{
								//Speading t2 cells
								switch(owner.playerTeam)
								{
									case 0:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Green_Division2_Across_Stage2_"), Main.animationFramerate);
										break;
									case 1:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Red_Division2_Across_Stage2_"), Main.animationFramerate);
										break;
									case 2:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Blue_Division2_Across_Stage2_"), Main.animationFramerate);
										break;
									case 3:
										if (Main.sAnimationsOn)
											_display = new MovieClip(Main.assets.getTextures("Yellow_Division2_Across_Stage2_"), Main.animationFramerate);
										break;
								}
								break;
							}
								
							default:
								break;
						}
					}
					
					if (Main.sAnimationsOn)
					{
						//determine orientation
						if(top && bottom)
							_display.rotation = DEGREE_90;
					}
					
					if (Main.sAnimationsOn)
					{
						addDisplay();
						Starling.juggler.add(_display);
						_display.addEventListener(Event.COMPLETE, infectionListener);
					}
					else
					{
						if (top)
							adjacentTopCell.reset();
						if (right)
							adjacentRightCell.reset();
						if (bottom)
							adjacentBottomCell.reset();
						if (left)
							adjacentLeftCell.reset();
						
						reset();
					}
				}
				if (numOfViableLocations == 3)
				{
					//Spreading in 3 directions.
					switch(state)
					{
						case 1:
						{
							//Speading t1 cells
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division3_Stage1_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division3_Stage1_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division3_Stage1_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division3_Stage1_"), Main.animationFramerate);
									break;
							}
							break;
						}
						case 2:
						{
							//Speading t2 cells
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division3_Stage2_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division3_Stage2_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division3_Stage2_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division3_Stage2_"), Main.animationFramerate);
									break;
							}
							break;
						}
							
						default:
							break;
					}
					
					if (Main.sAnimationsOn)
					{
						//determine orientation
						if (right && bottom && top)
							_display.rotation = DEGREE_270;
						if (bottom && left && top)
							_display.rotation = DEGREE_90;
						if (left && top && right)
							_display.rotation = DEGREE_180;
					}
					
					if (Main.sAnimationsOn)
					{
						addDisplay();
						Starling.juggler.add(_display);
						_display.addEventListener(Event.COMPLETE, infectionListener);
					}
					else
					{
						if (top)
							adjacentTopCell.reset();
						if (right)
							adjacentRightCell.reset();
						if (bottom)
							adjacentBottomCell.reset();
						if (left)
							adjacentLeftCell.reset();
						
						reset();
					}
					
				}
				if (numOfViableLocations == 4)
				{
					//Spreading in four directions.
					switch(state)
					{
						case 1:
						{
							//Spreading t1 Cells
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division4_Stage1_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division4_Stage1_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division4_Stage1_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division4_Stage1_"), Main.animationFramerate);
									break;
							}
							break;
						}
						case 2:
						{
							//Spreading t2 Cells
							switch(owner.playerTeam)
							{
								case 0:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Green_Division4_Stage2_"), Main.animationFramerate);
									break;
								case 1:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Red_Division4_Stage2_"), Main.animationFramerate);
									break;
								case 2:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Blue_Division4_Stage2_"), Main.animationFramerate);
									break;
								case 3:
									if (Main.sAnimationsOn)
										_display = new MovieClip(Main.assets.getTextures("Yellow_Division4_Stage2_"), Main.animationFramerate);
									break;
							}
							break;
						}
							
						default:
							break;
					}
					
					if (Main.sAnimationsOn)
					{
						addDisplay();
						Starling.juggler.add(_display);
						_display.addEventListener(Event.COMPLETE, infectionListener);
					}
					else
					{
						if (top)
							adjacentTopCell.reset();
						if (right)
							adjacentRightCell.reset();
						if (bottom)
							adjacentBottomCell.reset();
						if (left)
							adjacentLeftCell.reset();
						
						reset();
					}
				}
			}
		}
		public function infectionListener():void
		{
			_display.removeEventListener(Event.COMPLETE, infectionListener);
			
			if (top)
				adjacentTopCell.reset();
			if (right)
				adjacentRightCell.reset();
			if (bottom)
				adjacentBottomCell.reset();
			if (left)
				adjacentLeftCell.reset();
			
			reset();
		}
		private function adjustPlayerTileCounts():void
		{
			if (owner != null)
				gameManager.addToPlayerTileCount(owner, this);
			if (_previousOwner != null)
				gameManager.subtractFromPlayerTileCount(_previousOwner, this);
		}
		public function createWhiteFlash():void
		{
			if(whiteFlash == null)
			{
				var whiteCircle:Image;
				
				if(state == 0)
					whiteCircle = new Image(Main.assets.getTexture("Grid_White"));
				else
					whiteCircle = new Image(Main.assets.getTexture("White_Circle"));
				
				whiteFlash = new BW_Selector(0,0,whiteCircle, false, .8, 1);
				addChild(whiteFlash);
			}
		}
		public function removeWhiteFlash():void
		{
			if(whiteFlash)
			{
				whiteFlash.destroy();
				whiteFlash.dispose();
				removeChild(whiteFlash);
				whiteFlash = null;
			}
		}
		public function addRipple():void
		{
			//adding ripple effect
			var ripple:MovieClip = new MovieClip(Main.assets.getTextures("Ripple_"), Main.animationFramerate);
			addChild(ripple);
			
			ripple.pivotX = ripple.width/2;
			ripple.pivotY = ripple.height/2;
			
			if(gameManager.mapSize == 1)
			{
				ripple.scaleX = .8;
				ripple.scaleY = .8;
			}
			
			if(gameManager.mapSize == 2)
			{
				ripple.scaleX = .65;
				ripple.scaleY = .65;
			}
			
			ripple.addEventListener(Event.COMPLETE, removeRipple);
			
			Starling.juggler.add(ripple);
		}
		private function removeRipple($e:Event):void
		{
			//removing ripple from screen.
			var ripple:MovieClip = $e.currentTarget as MovieClip;
			ripple.removeEventListener(Event.COMPLETE, removeRipple);
			ripple.stop();
			ripple.dispose();
			removeChild(ripple);
		}
		
		public function get previousOwner():Player { return _previousOwner; }
		public function get reactionNumber():int {return _reactionNumber};
		
		public function set previousOwner(previousOwner:Player):void { _previousOwner = previousOwner; }
		public function set reactionNumber(reactionNumber:int):void { _reactionNumber = reactionNumber};
		public function set potentialState(potentialState:int):void { _potentialState = potentialState};
		public function set originalState(originalState:int):void { _originalState = originalState};
		
	}
}