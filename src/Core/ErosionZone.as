package Core
{
	public class ErosionZone
	{
		private var _zoneArray:Array = [];
		private var _decayed:Boolean;
		private var _identifier:int;
		private var _active:Boolean;
		private var _turnDecayed:int;
		private var _decayNum:int;
		
		private var _decayBaseline:int = 3;
		private var _decayMaximum:int = 6;
		
		private var _gameManager:GameManager;
		
		public function ErosionZone(gameManager:GameManager):void
		{
			_gameManager = gameManager;
			
			//determine how many turns until the first set of erosion zones, decays.
			var range:int = _decayMaximum - _decayBaseline;
			_decayNum = _decayBaseline + Math.round(Math.random() * range);
		}
		public function incrementDecay(currentTurn:int):void
		{
			if(!decayed)
			{
				_decayNum--;
				
				// trace("Zone " + _identifier.toString() +" will decay in " + _decayNum.toString() + " Rounds");
				
				if(decayNum <= 0)
				{
					_decayed = true;
					_turnDecayed = currentTurn;
					decayCells();
					_gameManager.pickActiveErosionZone();
				}
					
			}
		}
		public function reverseDecay():void
		{
			decayNum++;
		}
		public function decayCells():void
		{
			for each (var cell:Cell in _zoneArray)
			{
				cell.decayCell();
			}
		}
		public function activateCells():void
		{
			for each (var cell:Cell in _zoneArray)
			{
				cell.owner = null;
				cell.state = 9;
				cell.reset();
			}
		}
		
		public function get zoneArray():Array {return _zoneArray};
		public function get decayed():Boolean {return _decayed};
		public function get identifier():int {return _identifier};
		public function get active():Boolean {return _active};
		public function get turnDecayed():int {return _turnDecayed};
		public function get decayNum():int {return _decayNum};
		
		public function set zoneArray(zoneArray:Array):void {_zoneArray = zoneArray};
		public function set decayed(decayed:Boolean):void {_decayed = decayed};
		public function set identifier(identifier:int):void {_identifier = identifier};
		public function set active(active:Boolean):void {_active = active};
		public function set turnDecayed(turnDecayed:int):void {_turnDecayed = turnDecayed};
		public function set decayNum(decayNum:int):void {_decayNum = decayNum};
	}
}