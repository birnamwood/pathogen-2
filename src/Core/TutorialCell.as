package Core
{
	import Sound.SoundController;
	
	import Tutorial.TutorialManager;
	
	import UI.Main;
	
	import Utils.BW_Selector;
	
	import flash.sampler.stopSampling;
	import flash.text.ReturnKeyLabel;
	
	import flashx.textLayout.formats.BackgroundColor;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	
	public class TutorialCell extends Sprite
	{
		//Classes
		public var _gameManager:TutorialGameManager;
		private var _tutorialManager:TutorialManager;
		
		private var stateChange:Boolean;
		private var frameCounter:int;
		
		//Adjacentcy
		protected var top:Boolean;
		protected var right:Boolean;
		protected var bottom:Boolean;
		protected var left:Boolean;
		public var flipping:Boolean;
		
		public var adjacentTopCell:TutorialCell;
		public var adjacentBottomCell:TutorialCell;
		public var adjacentLeftCell:TutorialCell;
		public var adjacentRightCell:TutorialCell;
		
		//Cell Information
		private var _ownerTeam:int;
		public var state:int;
		public var location:int;
		
		//Rotation
		public static const DEGREE_90:Number = deg2rad(90);
		public static const DEGREE_180:Number =  deg2rad(180);
		public static const DEGREE_270:Number = deg2rad(-90);
		
		//Sprites
		protected var _display:MovieClip;
		private var grid:Image;
		protected var bridge:MovieClip;
		
		//Cascade Vars
		public var hasAlreadySpilledOver:Boolean = false;
		public var notAValidMove:Boolean;
		private var originalTileState:int;
		
		private var tween:Tween;
		private var _previousOwnerTeam:int;
		
		//Tutorial
		private var whiteFlash:BW_Selector;
		
		//Sound
		private var _reactionNumber:int = 0;
		
		private var _ripple:MovieClip;
		
		public function TutorialCell(location:int, tutorialGameManager:TutorialGameManager, tutorialManager:TutorialManager = null)
		{
			super();
			
			this.location = location;
			
			if(tutorialGameManager)
				_gameManager = tutorialGameManager;
			if(tutorialManager)
				_tutorialManager = tutorialManager;
			
			_previousOwnerTeam = -1;
		}
		private function onTouch($e:TouchEvent):void
		{
			//If cell is touched and a turn is not already underway
			if(!_tutorialManager.moveUnderway)
			{
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
				
				if(touch)
					_tutorialManager.makeMove(this);
			}
		}
		public function addTouchEvent():void
		{
			removeEventListener(TouchEvent.TOUCH, onTouch);
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		public function changeState(_cellType:int, _playerTeam:int):void
		{
			//The Cells is now changing & mark it as already been changed so that it may not be changed again.
			flipping = true;
			hasAlreadySpilledOver = true;
			
			//Depending on what is placed, do the following.
			switch(_cellType)
			{
				//If the player has selected a level t0 Tile
				case 0:
				{
					//If the player is stacking the same level on itself
					if ((state == 1) && (_ownerTeam == _playerTeam))
					{
						state = 2;
						stateChange = true;
					}
						//If the Tile has not been Taken
					else if (state == 0)
					{
						state = 1;
						stateChange = true;
					}
					break;
				}
					//If the player has selected a t1 Tile
				case 1:
				{
					//If the player is stacking the same level on itself
					if ((state == 2) && (_ownerTeam == _playerTeam))
					{
						state = 3;
						stateChange = true;
					}
						//Overwrite a lesser color
					else if (state == 1)
					{
						state = 2;
						stateChange = true;
					}
						//If its an empty space or a grey cell.
					else if (state == 0 || state == 5)
					{
						state = 2;
						stateChange = true;
					}
					break;
				}
					//If the player has selected a t2 cell
				case 2:
				{
					//t2 Grey cell
					if (state == 6)
					{
						state = 3;
						stateChange = true;
					}
					//t1 Grey cell
					else if (state == 5)
					{
						state = 3;
						stateChange = true;
					}
						//stacking a t2 on itself to create a t3.
					else if ((state == 3) && (_ownerTeam == _playerTeam))
					{
						state = 4;
						stateChange = true;
					}
						//override a lower tier
					else if (state == 2)
					{
						state = 3;
						stateChange = true;
					}
						//override a lower tier
					else if (state == 1)
					{
						state = 3;
						stateChange = true;
					}
						//place one in an empy space.
					else if (state == 0)
					{
						state = 3;
						stateChange = true;
					}
					break;
				}
				//If the player has selected a Virus
				case 3:
				{
					//If they are placing it on an enemy t2 cell
					if ((state == 3) && (_ownerTeam != _playerTeam))
					{
						state = 0;
						originalTileState = 3;
						stateChange = true;
					}
					//If they are placing it on an enemy t1 cell
					if ((state == 2) && (_ownerTeam != _playerTeam))
					{
						state = 0;
						originalTileState = 2;
						stateChange = true;
					}
					//If they are placing it on an enemy t0 cell
					if ((state == 1) && (_ownerTeam != _playerTeam))
					{
						state = 0;
						originalTileState = 1;
						stateChange = true;
					}
					break;
				}
			}
			if (stateChange)
			{
				//Valid Move
				_ownerTeam = _playerTeam;
				SoundController.selectScaleSound(state);
				
				//If the placed cell is higher than a t0, begin cascade function.
				if (state != 1)
					spillOver()
				else
				{
					//Otherwise we know its a t0 cell being created in a brand new space
					setColor();
					
					// Have the cell scale & fade into existance
					_display.alpha = 0;
					_display.scaleX = 0;
					_display.scaleY = 0;
					
					var tweenTime:Number = 0.3;
					tween = new Tween(_display, tweenTime);
					tween.fadeTo(1);
					tween.scaleTo(1);
					tween.onComplete = removeTween;
					Starling.juggler.add(tween);
					
					_reactionNumber++;
					SoundController.randomNote();
					SoundController.playSpreadSound(_reactionNumber);
				}
			}
			else
				notAValidMove = true;
		}
		private function removeTween():void
		{
			//Reset Tween
			Starling.juggler.removeTweens(_display);
			tween = null;
			reset();
		}
		public function cascade(OriginatorTileState:int, OriginatorTilesRelativePos:int, tileTeam:int, originatorOriginalTileState:int, reactionNumber:int):void
		{
			//What was the orginal cell that was placed by the player?
			originalTileState = originatorOriginalTileState;
			_reactionNumber = reactionNumber;
			
			//As long as the cell has not already changed this turn, change it.
			if (!hasAlreadySpilledOver)
			{
				hasAlreadySpilledOver = true;
				flipping = true;
				
				//The original tile was a t1.
				if (OriginatorTileState == 2)
				{
					_ownerTeam = tileTeam;
					
					//Based on this cells current state, do different actions.
					switch(state)
					{
						case 0:
							state = 1;
							setColor();
							reset();
							break;
						case 1:
							state = 2;
							addBridge(OriginatorTilesRelativePos);
							break;
						case 5:
							state = 2;
							addBridge(OriginatorTilesRelativePos);
							break;
						default:
							flipping = false;
							break;
					}
				}
					//The original tile was a t2.
				else if (OriginatorTileState == 3)
				{
					_ownerTeam = tileTeam;
					
					//Based on this cells current state, do different actions.
					switch(state)
					{
						case 0:
							state = 2;
							setColor();
							reset();
							break;
						case 1:
							state = 2;
							addBridge(OriginatorTilesRelativePos);
							break;
						case 2:
							state = 3;
							addBridge(OriginatorTilesRelativePos);
							break;
						case 5:
							state = 2;
							addBridge(OriginatorTilesRelativePos);
							break;
						case 6:
							state = 3;
							addBridge(OriginatorTilesRelativePos);
						default:
							flipping = false;
							break;
					}
				}
					//The original tile was a t3
				else if (OriginatorTileState == 4)
				{
					state = 4;
					_ownerTeam = tileTeam;
					Starling.juggler.delayCall(spillOver, .3);
				}
					//A virus was used.
				else if (OriginatorTileState == 0)
				{
					state = 0;
					//ownerTeam = -1;
					Starling.juggler.delayCall(spillOver, .3);
				}
				else
					flipping = false;
			}
		}
		private function addBridge(OriginatorTilesRelativePos:int):void
		{
			//Where is this cell in relation to the cell that is spilling over into it?
			bridge = new MovieClip(Main.assets.getTextures("EnergyTransfer_"), Main.animationFramerate);
			addChild(bridge);
			
			bridge.pivotX = bridge.width/2;
			bridge.pivotY = bridge.height/2;
			
			switch(OriginatorTilesRelativePos)
			{
				case 0:
					bridge.rotation = DEGREE_270;
					break;
				case 1:
					bridge.rotation = DEGREE_90;
					break
				case 3:
					bridge.rotation = DEGREE_180;
					break;
			}
			
			Starling.juggler.add(bridge);
			bridge.addEventListener(Event.COMPLETE, spillOver)
		}
		public function spillOver():void
		{
			//Remove the old cell, changing it out for one of the following animations.
			if(_display != null)
			{
				_display.removeEventListener(Event.COMPLETE, spillOver);
				removeChild(_display);
			}
			
			//If a bridge animatin was played, remove it.
			if(bridge)
			{
				bridge.stop();
				removeChild(bridge);
			}
			
			if(state != 1 && state != 4)
			{
				addRipple();
				
				if(_gameManager)
					_gameManager.shiftStack(this);
				if(_tutorialManager)
					_tutorialManager.shiftStack(this);
			}
			
			//How many adjacent spaces are empty?
			var numOfViableAdjacentTiles:int = 0;
			rotation = 0;
			
			//Checking for empty spaces
			if (state != 4 && state != 0)
			{
				if (adjacentTopCell != null)
				{
					if (adjacentTopCell.state == 0 && adjacentTopCell.hasAlreadySpilledOver == false)
					{
						numOfViableAdjacentTiles++;
						top = true;
					}
				}
				if (adjacentRightCell != null)
				{
					if (adjacentRightCell.state == 0 && adjacentRightCell.hasAlreadySpilledOver == false)
					{
						numOfViableAdjacentTiles++;
						right = true;
					}
				}
				if (adjacentBottomCell != null)
				{
					if (adjacentBottomCell.state == 0 && adjacentBottomCell.hasAlreadySpilledOver == false)
					{
						numOfViableAdjacentTiles++;
						bottom = true;
					}
				}
				if (adjacentLeftCell != null)
				{
					if (adjacentLeftCell.state == 0 && adjacentLeftCell.hasAlreadySpilledOver == false)
					{
						numOfViableAdjacentTiles++;
						left = true;
					}
				}
			}
			
			var numOfCascadingCells:int = 0;
			
			//Checking for Neighboring Cells of the -1 type.
			switch(state)
			{
				case 2:
				{
					//t1 cells can spill over onto neighboring t0,greyt0.
					if (adjacentTopCell != null)
					{
						if ((adjacentTopCell.state == 1) || (adjacentTopCell.state == 5))
						{
							adjacentTopCell.cascade(state, 0, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					if (adjacentRightCell != null)
					{
						if ((adjacentBottomCell.state == 1) || (adjacentBottomCell.state == 5))
						{
							adjacentBottomCell.cascade(state, 1, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					if (adjacentRightCell != null)
					{
						if ((adjacentRightCell.state == 1) || (adjacentRightCell.state == 5))
						{
							adjacentRightCell.cascade(state, 2, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					if (adjacentLeftCell != null)
					{
						if ((adjacentLeftCell.state == 1) || (adjacentLeftCell.state == 5))
						{
							adjacentLeftCell.cascade(state, 3, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					break;
				}
				case 3:
				{
					//t2 cells can spill over onto neighboring t0,t1,greyt0,greyt1.
					if (adjacentTopCell != null)
					{
						if ((adjacentTopCell.state == 1) || (adjacentTopCell.state == 2) || (adjacentTopCell.state == 5) || (adjacentTopCell.state == 6))
						{
							adjacentTopCell.cascade(state, 0, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					if (adjacentBottomCell != null)
					{
						if ((adjacentBottomCell.state == 1) || (adjacentBottomCell.state == 2) || (adjacentBottomCell.state == 5) || (adjacentBottomCell.state == 6))
						{
							adjacentBottomCell.cascade(state, 1, _ownerTeam,  originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					if (adjacentRightCell != null)
					{
						if ((adjacentRightCell.state == 1) || (adjacentRightCell.state == 2) || (adjacentRightCell.state == 5) || (adjacentRightCell.state == 6))
						{
							adjacentRightCell.cascade(state, 2, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					if (adjacentLeftCell != null)
					{
						if ((adjacentLeftCell.state == 1) || (adjacentLeftCell.state == 2) || (adjacentLeftCell.state == 5) || (adjacentLeftCell.state == 6))
						{
							adjacentLeftCell.cascade(state, 3, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					break;
				}
				case 4:
				{
					//t3 cells can only spill over onto neighboring t3 cells.
					if (adjacentTopCell != null)
					{
						if (adjacentTopCell.state == 3)
						{
							adjacentTopCell.cascade(state, 0, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					if (adjacentBottomCell != null)
					{
						if (adjacentBottomCell.state == 3)
						{
							adjacentBottomCell.cascade(state, 1, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					if (adjacentRightCell != null)
					{
						if (adjacentRightCell.state == 3)
						{
							adjacentRightCell.cascade(state, 2, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					if (adjacentLeftCell != null)
					{
						if (adjacentLeftCell.state == 3)
						{
							adjacentLeftCell.cascade(state, 3, _ownerTeam, originalTileState, _reactionNumber);
							numOfCascadingCells++;
						}
					}
					break;
				}
				case 0:
				{
					//Spreading the effect of a virus!
					switch(originalTileState)
					{
						case 1:
						{
							//Check all adjacent sides for t0 of any color & grey t0
							if (adjacentTopCell != null)
							{
								if (adjacentTopCell.state == 1 || adjacentTopCell.state == 5)
								{
									adjacentTopCell.cascade(state, 0, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							if (adjacentBottomCell != null)
							{
								if (adjacentBottomCell.state == 1 || adjacentBottomCell.state == 5)
								{
									adjacentBottomCell.cascade(state, 1, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							if (adjacentRightCell != null)
							{
								if (adjacentRightCell.state == 1 || adjacentRightCell.state == 5)
								{
									adjacentRightCell.cascade(state, 2, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							if (adjacentLeftCell != null)
							{
								if (adjacentLeftCell.state == 1 || adjacentLeftCell.state == 5)
								{
									adjacentLeftCell.cascade(state, 3, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							break;
						}
						case 2:
						{
							//Check all adjacent sides for t1 of any color & grey t1
							if (adjacentTopCell != null)
							{
								if (adjacentTopCell.state == 2 || adjacentTopCell.state == 6)
								{
									adjacentTopCell.cascade(state, 0, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							if (adjacentBottomCell != null)
							{
								if (adjacentBottomCell.state == 2 || adjacentBottomCell.state == 6)
								{
									adjacentBottomCell.cascade(state, 1, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							if (adjacentRightCell != null)
							{
								if (adjacentRightCell.state == 2 || adjacentRightCell.state == 6)
								{
									adjacentRightCell.cascade(state, 2, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							if (adjacentLeftCell != null)
							{
								if (adjacentLeftCell.state == 2 || adjacentLeftCell.state == 6)
								{
									adjacentLeftCell.cascade(state, 3, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							break;
						}
						case 3:
						{
							//Check all adjacent sides for t2 of any color
							if (adjacentTopCell != null)
							{
								if (adjacentTopCell.state == 3)
								{
									adjacentTopCell.cascade(state, 0, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							if (adjacentBottomCell != null)
							{
								if (adjacentBottomCell.state == 3)
								{
									adjacentBottomCell.cascade(state, 1, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							if (adjacentRightCell != null)
							{
								if (adjacentRightCell.state == 3)
								{
									adjacentRightCell.cascade(state, 2, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							if (adjacentLeftCell != null)
							{
								if (adjacentLeftCell.state == 3)
								{
									adjacentLeftCell.cascade(state, 3, _ownerTeam, originalTileState, _reactionNumber);
									numOfCascadingCells++;
								}
							}
							break;
						}
							
						default:
							break;
					}
					break;
				}
					
				default:
					break;
			}
			
			if(numOfViableAdjacentTiles > 0 || numOfCascadingCells > 0)
				SoundController.playSpreadSound(_reactionNumber);
			
			switch(numOfViableAdjacentTiles)
			{
				case 0:
				{
					//Not spreading in any direction, just go ahead and evolve.
					switch(state)
					{
						case 2:
						{
							//Evolving from a t1 to t2
							switch(_ownerTeam)
							{
								case 0:
									_display = new MovieClip(Main.assets.getTextures("Green_Evolution_1To2_"), Main.animationFramerate);
									break;
								case 1:
									_display = new MovieClip(Main.assets.getTextures("Red_Evolution_1To2_"), Main.animationFramerate);
									break;
								case 2:
									_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_1To2_"), Main.animationFramerate);
									break;
								case 3:
									_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_1To2_"), Main.animationFramerate);
									break;
							}
							break;
						}
						case 3:
						{
							//Evolving from a t2 to t3
							switch(_ownerTeam)
							{
								case 0:
									_display = new MovieClip(Main.assets.getTextures("Green_Evolution_2To3_"), Main.animationFramerate);
									break;
								case 1:
									_display = new MovieClip(Main.assets.getTextures("Red_Evolution_2To3_"), Main.animationFramerate);
									break;
								case 2:
									_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_2To3_"), Main.animationFramerate);
									break;
								case 3:
									_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_2To3_"), Main.animationFramerate);
									break;
							}
							break;
						}
						case 4:
						{
							//Evolving from a t3 to t4
							switch(_ownerTeam)
							{
								case 0:
									_display = new MovieClip(Main.assets.getTextures("Green_Evolution_3To4_"), Main.animationFramerate);
									break;
								case 1:
									_display = new MovieClip(Main.assets.getTextures("Red_Evolution_3To4_"), Main.animationFramerate);
									break;
								case 2:
									_display = new MovieClip(Main.assets.getTextures("Blue_Evolution_3To4_"), Main.animationFramerate);
									break;
								case 3:
									_display = new MovieClip(Main.assets.getTextures("Yellow_Evolution_3To4_"), Main.animationFramerate);
									break;
							}
							break;
						}
						case 0:
						{
							//Destruction Animation
							
							//Who was the previous Owner?
							switch(ownerTeam)
							{
								case 0:
									//What was the original tile state?
									switch(originalTileState)
									{
										case 1:
											_display = new MovieClip(Main.assets.getTextures("Green_Destruction_Stage1_"), Main.animationFramerate);
											break;
										case 2:
											_display = new MovieClip(Main.assets.getTextures("Green_Destruction_Stage2_"), Main.animationFramerate);
											break;
										case 3:
											_display = new MovieClip(Main.assets.getTextures("Green_Destruction_Stage3_"), Main.animationFramerate);
											break;
									}
									break;
								
								case 1:
									//What was the original tile state?
									switch(originalTileState)
									{
										case 1:
											_display = new MovieClip(Main.assets.getTextures("Red_Destruction_Stage1_"), Main.animationFramerate);
											break;
										case 2:
											_display = new MovieClip(Main.assets.getTextures("Red_Destruction_Stage2_"), Main.animationFramerate);
											break;
										case 3:
											_display = new MovieClip(Main.assets.getTextures("Red_Destruction_Stage3_"), Main.animationFramerate);
											break;
									}
									break;
							}
							break;
						}
					}
					
					addDisplay();
					Starling.juggler.add(_display);
					if (_display)
						_display.addEventListener(Event.COMPLETE, reset);
					break;
				}
				case 1:
				{
					// Spreading in 1 direction
					switch(state)
					{
						case 2:
						{
							//Speading t1 cells
							switch(_ownerTeam)
							{
								case 0:
									_display = new MovieClip(Main.assets.getTextures("Green_Division1_Stage1_"), Main.animationFramerate);
									break;
								case 1:
									_display = new MovieClip(Main.assets.getTextures("Red_Division1_Stage1_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						}
						case 3:
						{
							//Speading t2 cells
							switch(_ownerTeam)
							{
								case 0:
									_display = new MovieClip(Main.assets.getTextures("Green_Division1_Stage2_"), Main.animationFramerate);
									break;
								case 1:
									_display = new MovieClip(Main.assets.getTextures("Red_Division1_Stage2_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						}
						default:
							break;
					}
					
					//Determine Orientation
					if (top)
						_display.rotation = DEGREE_270;
					if (bottom)
						_display.rotation = DEGREE_90
					if (left)
						_display.rotation = DEGREE_180;
					
					addDisplay();
					
					Starling.juggler.add(_display);
					_display.addEventListener(Event.COMPLETE, animationWatcher);
					break;
				}
				case 2:
				{
					//If 2 sides are spreading to an open space
					var adjacent:Boolean = true;
					
					//Opposite Sides are spreading
					if(top && bottom || left && right)
						adjacent = false;
					
					if (adjacent)
					{
						//Spreading on two adjacent sides
						switch(state)
						{
							case 2:
							{
								//Speading t1 cells
								switch(_ownerTeam)
								{
									case 0:
										_display = new MovieClip(Main.assets.getTextures("Green_Division2_Stage1_"), Main.animationFramerate);
										break;
									case 1:
										_display = new MovieClip(Main.assets.getTextures("Red_Division2_Stage1_"), Main.animationFramerate);
										break;
									default:
										break;
								}
								break;
							}
							case 3:
							{
								//Speading t2 cells
								switch(_ownerTeam)
								{
									case 0:
										_display = new MovieClip(Main.assets.getTextures("Green_Division2_Stage2_"), Main.animationFramerate);
										break;
									case 1:
										_display = new MovieClip(Main.assets.getTextures("Red_Division2_Stage2_"), Main.animationFramerate);
										break;
									default:
										break;
								}
								break;
							}
							default:
								break;
						}
						
						//determine orientation
						if (right && top)
							_display.rotation = DEGREE_270;
						if (bottom && left)
							_display.rotation = DEGREE_90;
						if (left && top)
							_display.rotation = DEGREE_180;
						
						addDisplay();
						Starling.juggler.add(_display);
						_display.addEventListener(Event.COMPLETE, animationWatcher);
					}
					else
					{
						//Spreading in opposite directions
						switch(state)
						{
							case 2:
							{	
								//Speading t1 cells
								switch(_ownerTeam)
								{
									case 0:
										_display = new MovieClip(Main.assets.getTextures("Green_Division2_Across_Stage1_"), Main.animationFramerate);
										break;
									case 1:
										_display = new MovieClip(Main.assets.getTextures("Red_Division2_Across_Stage1_"), Main.animationFramerate);
										break;
									default:
										break;
								}
								break;
							}
							case 3:
							{
								//Speading t2 cells
								switch(_ownerTeam)
								{
									case 0:
										_display = new MovieClip(Main.assets.getTextures("Green_Division2_Across_Stage2_"), Main.animationFramerate);
										break;
									case 1:
										_display = new MovieClip(Main.assets.getTextures("Red_Division2_Across_Stage2_"), Main.animationFramerate);
										break;
									default:
										break;
								}
								break;
							}
							default:
								break;
						}
						
						//determine orientation
						if(top && bottom)
							_display.rotation = DEGREE_90;
						
						addDisplay();
						Starling.juggler.add(_display);
						_display.addEventListener(Event.COMPLETE, animationWatcher);
					}
					break;
				}
				case 3:
				{
					//Spreading in 3 directions
					switch(state)
					{
						case 2:
						{
							//Speading t1 cells
							switch(_ownerTeam)
							{
								case 0:
									_display = new MovieClip(Main.assets.getTextures("Green_Division3_Stage1_"), 30);
									break;
								case 1:
									_display = new MovieClip(Main.assets.getTextures("Red_Division3_Stage1_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						} 
						case 3:
						{
							//Speading t2 cells
							switch(_ownerTeam)
							{
								case 0:
									_display = new MovieClip(Main.assets.getTextures("Green_Division3_Stage2_"), Main.animationFramerate);
									break;
								case 1:
									_display = new MovieClip(Main.assets.getTextures("Red_Division3_Stage2_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						}
						default:
							break;
					}
					
					//determine orientation
					if (right && bottom && top)
						_display.rotation = DEGREE_270;
					if (bottom && left && top)
						_display.rotation = DEGREE_90;
					if (left && top && right)
						_display.rotation = DEGREE_180;
					
					addDisplay();
					Starling.juggler.add(_display);
					_display.addEventListener(Event.COMPLETE, animationWatcher);
					break;
				}
				case 4:
				{
					//Spreading in four directions.
					switch(state)
					{
						case 2:
						{
							//Spreading Dots
							switch(_ownerTeam)
							{
								case 0:
									_display = new MovieClip(Main.assets.getTextures("Green_Division4_Stage1_"), 30);
									break;
								case 1:
									_display = new MovieClip(Main.assets.getTextures("Red_Division4_Stage1_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						}
						case 3:
						{
							//Spreading Circles
							switch(_ownerTeam)
							{
								case 0:
									_display = new MovieClip(Main.assets.getTextures("Green_Division4_Stage2_"), Main.animationFramerate);
									break;
								case 1:
									_display = new MovieClip(Main.assets.getTextures("Red_Division4_Stage2_"), Main.animationFramerate);
									break;
								default:
									break;
							}
							break;
						}
					}
					
					addDisplay();
					Starling.juggler.add(_display);
					_display.addEventListener(Event.COMPLETE, animationWatcher);
					break;
				}
			}
		}
		public function animationWatcher():void
		{
			//Stop the animation & remove it.
			_display.stop();
			_display.removeEventListener(Event.COMPLETE, animationWatcher);
			removeChild(_display);
			
			//Reset orientation
			_display.rotation = 0;
			
			//Tell adjacent cells to go play their corrisponding animation.
			if (top)
				adjacentTopCell.cascade(state, -1, _ownerTeam, originalTileState, _reactionNumber);
			if (right)
				adjacentRightCell.cascade(state, -1, _ownerTeam, originalTileState, _reactionNumber);
			if (bottom)
				adjacentBottomCell.cascade(state, -1, _ownerTeam, originalTileState, _reactionNumber);
			if (left)
				adjacentLeftCell.cascade(state, -1, _ownerTeam, originalTileState, _reactionNumber);
			
			//Evolve the Cell.
			switch(state)
			{
				case 2:
				{
					//Evolve from t1 to t2.
					switch(_ownerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Evolution_1To2_"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Evolution_1To2_"), Main.animationFramerate);
							break;
						default:
							break;
					}
					break;
				}
				case 3:
				{
					//Evolve from t2 to t3.
					switch(_ownerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Evolution_2To3_"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Evolution_2To3_"), Main.animationFramerate);
							break;
						default:
							break;
					}
					break;
				}
				default:
					break;
			}
			
			addDisplay();
			Starling.juggler.add(_display);
			_display.addEventListener(Event.COMPLETE, reset);
		}
		public function reset():void
		{
			//Stop animation
			if(_display != null)
			{
				_display.stop();
				_display.rotation = 0;
				removeChild(_display);
			}
			
			//Set its final color.
			setColor();
			
			//reset variables
			stateChange = false;
			top = false;
			right = false;
			bottom = false;
			left = false;
			flipping = false;
			
			removeWhiteFlash();
		}
		public function setColor():void
		{
			//Render the cell
			switch(state)
			{
				case 0:
					_display = null;
					addGrid();
					ownerTeam = -1;
					break;
				
				case 1:
					//T1 Cell
					switch(_ownerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage01"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage01"), Main.animationFramerate);
							break;
					}
					break;
				case 2:
					//T2 Cells
					switch(_ownerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage02"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage02"), Main.animationFramerate);
							break;
					}
					break;
				case 3:
					//T3 Cells
					switch(_ownerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage03"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage03"), Main.animationFramerate);
							break;
					}
					break;
				case 4:
					//T4 Cells
					switch(_ownerTeam)
					{
						case 0:
							_display = new MovieClip(Main.assets.getTextures("Green_Stage04"), Main.animationFramerate);
							break;
						case 1:
							_display = new MovieClip(Main.assets.getTextures("Red_Stage04"), Main.animationFramerate);
							break;
					}
					break;
				case 5:
					//Grey T1 Cell
					_display = new MovieClip(Main.assets.getTextures("Grey_Stage01"), Main.animationFramerate);
					break;
				case 6:
					//Grey T2 Cell
					_display = new MovieClip(Main.assets.getTextures("Grey_Stage02"), Main.animationFramerate);
					break;
				case 9:
					//Grey T3 Cell
					_display = new MovieClip(Main.assets.getTextures("Grey_Stage04"), Main.animationFramerate);
					break;
			}
			
			addDisplay();
		}
		protected function addDisplay():void
		{
			//Add the display to the screen.
			if(_display != null)
			{
				addChild(_display);
				
				_display.x = 0;
				_display.y = 0;
				
				_display.pivotX = _display.width/2;
				_display.pivotY = _display.height/2;
				
				if(grid != null)
				{
					var gridTween:Tween = new Tween(grid, .5);
					gridTween.fadeTo(0);
					Starling.juggler.add(gridTween);
					gridTween.onComplete = removeGrid;
				}
			}
		}
		protected function addGrid():void
		{
			if(grid == null)
			{
				grid =  new Image(Main.assets.getTexture("Grid"));
				grid.pivotX = grid.width/2;
				grid.pivotY = grid.height/2;
				addChild(grid);
				grid.alpha = 0;
				
				tween = new Tween(grid, .5);
				tween.fadeTo(1);
				Starling.juggler.add(tween);
			}
		}
		private function removeGrid():void
		{
			alpha = 1;
			removeChild(grid);
			grid = null;
		}
		public function CleanCell():void
		{	
			state = 0;
			_ownerTeam = -1;
			_previousOwnerTeam = -1;
			reset();
		}
		public function createWhiteFlash():void
		{
			var whiteCircle:Image;
			
			if(state == 0)
				whiteCircle = new Image(Main.assets.getTexture("Grid_White"));
			else
				whiteCircle = new Image(Main.assets.getTexture("White_Circle"));
			
			whiteFlash = new BW_Selector(0,0,whiteCircle, false, .8, 1);
			addChild(whiteFlash);
		}
		public function removeWhiteFlash():void
		{
			if(whiteFlash)
			{
				whiteFlash.destroy();
				whiteFlash.dispose();
				removeChild(whiteFlash);
				whiteFlash = null;
			}
		}
		public function fadeIN(alpha:Number):void
		{
			if(visible == false && state != 10)
			{
				visible = true;
				
				if(grid)
				{
					grid.alpha = 0;
					
					tween = new Tween(grid, .5);
					tween.fadeTo(alpha);
					Starling.juggler.add(tween);
				}
				
				if(_display)
				{
					_display.alpha = 0;
					
					tween = new Tween(_display, .5);
					tween.fadeTo(alpha);
					Starling.juggler.add(tween);
				}
			}
		}
		public function fadeOUT(alpha:Number):void
		{
			if(visible == true)
			{
				if(grid)
				{
					tween = new Tween(grid, .5);
					tween.fadeTo(alpha);
					Starling.juggler.add(tween);
				}
				
				if(_display)
				{
					tween = new Tween(_display, .5);
					tween.fadeTo(alpha);
					Starling.juggler.add(tween);
				}
				
				if(alpha == 0)
					tween.onComplete = fadeOUTComplete;
			}
		}
		private function fadeOUTComplete():void
		{
			visible = false;
			removeEventListener(TouchEvent.TOUCH, onTouch);
		}
		public function addRipple():void
		{
			if (_ripple)
			{
				removeRippleFromGameMan();
			}
			
			//adding ripple effect
			_ripple = new MovieClip(Main.assets.getTextures("Ripple_"), Main.animationFramerate);
			addChild(_ripple);
			
			_ripple.pivotX = _ripple.width/2;
			_ripple.pivotY = _ripple.height/2;
			
			_ripple.addEventListener(Event.COMPLETE, removeRipple);
			
			Starling.juggler.add(_ripple);
		}
		private function removeRipple($e:Event):void
		{
			//removing ripple from screen.
			var ripple:MovieClip = $e.currentTarget as MovieClip;
			ripple.removeEventListener(Event.COMPLETE, removeRipple);
			ripple.stop();
			ripple.dispose();
			removeChild(ripple);
			ripple = null;
		}
		
		public function removeRippleFromGameMan():void
		{
			if (_ripple)
			{
				_ripple.removeEventListener(Event.COMPLETE, removeRipple);
				_ripple.stop();
				_ripple.dispose();
				removeChild(_ripple);
				_ripple = null;
			}
		}
		
		public function get previousOwnerTeam():int { return _previousOwnerTeam; }
		public function get ownerTeam():int { return _ownerTeam; }
		public function get reactionNumber():int { return _reactionNumber};
		
		public function set previousOwnerTeam(previousOwnerTeam:int):void { _previousOwnerTeam = previousOwnerTeam; }
		public function set ownerTeam(ownerTeam:int):void { _ownerTeam = ownerTeam; }
		public function set reactionNumber(reactionNumber:int):void { _reactionNumber = reactionNumber};
	}
}