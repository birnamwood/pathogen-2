package OnlinePlay
{
	import Core.BoardOutline;
	import Core.InGameTutorial;
	import Core.TipWindow;
	import Core.TurnRecord;
	import Core.TutorialCell;
	
	import FloxConnect.EntityManager;
	import FloxConnect.Turn;
	
	import Maps.Map;
	
	import UI.Achievements;
	import UI.Main;
	import UI.fSprite;
	
	import Utils.BW_Button;
	import Utils.BW_Selector;
	import Utils.BW_SocialButton;
	import Utils.BW_UI;
	
	import com.gamua.flox.utils.cloneObject;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.deg2rad;
	import UI.AchievementPopUp;
	
	public class OnlineGameManager extends BW_UI
	{
		// local constants
		//private const ZONE_SCREEN_REMOVE_TIME:Number = 4.0;
		private const OTHER_SELECTORS_ALPHA:Number = 0.8;
		private const SELECTOR_FADE_TIME:Number = 2.0;
		private const UPDATE_BAR_SPEED:Number = 2.5;
		private const CELL_SCALE_FOR_CLICK:Number = 0.3;
		private const LIVE_UPDATE_COUNT:int = 600;
		
		//Classes
		private var main:Main;
		private var postGame:OnlinePostGame;
		private var _tipWindow:TipWindow;
		
		//Map Data
		private var tileHolder:Sprite;
		private var _mapSize:int;
		public var mapName:String;
		private var gameBoardHeight:int;
		private var gameBoardWidth:int;
		private var gameBoard:Array = [];
		private var totalNumOfPlayableTiles:int = 0;
		
		private var currentCellTypeSelected:OnlineCellSelector;
		
		//Player Data
		public var playerArray:Array = [];
		public var playerLocations:Array;
		public var playerColors:Array;
		private var numOfPlayers:int;
		
		private var currentPlayerNum:int;
		private var currentPlayer:OnlinePlayer;
		
		//Gameplay Vars
		public var moveUnderway:Boolean;
		private var roundCounter:int = 0;
		public var minuteCounter:int;
		private var CurrentFrameCount:int;
		private var secondCounter:int;
		
		//Imported Game Data from Editor
		private var mapData:Array;
		
		//History
		private var turnList:Array = [];
		private var turn:TurnRecord;
		private var playLog:Array = [];
		private var playLogOwnership:Array = [];
		
		//Modes
		private var tileSelector:BW_Selector;
		private var selectorFade:Tween;
		
		//Gameplay Options
		private var showPercentBar:Boolean;
		
		//Buttons
		private var quitButton:BW_Button;
		private var rulesButton:BW_Button;
		private var optionsButton:BW_Button;
		
		//Save
		//private var sharedData:SharedObject
		
		//Quit Menu
		private var quitScreen:OnlineQuitScreen;
		private var yesBt:BW_Button;
		private var noBt:BW_Button;
		
		//Options Menu
		private var optionsScreen:OnlineOptions;
		private var percentCheckbox:Button;
		private var optionsQuit:Button;
		
		//PercentBar
		private var percentBar:Sprite;
		private var percentFrameWidth:Number;
		private var percentBarRate:Number;
		
		//End Game
		private var forfiet:Boolean;
		private var replay:Boolean = false;
		
		// replay
		private var currentTurn:int = 0;
		private var playPauseButton:BW_Button;
		private var stepForwardButton:BW_Button;
		private var stepBackwardButton:BW_Button;
		private var replayPause:Boolean;
		private var playReplay:Boolean = false;
		
		// game mode stuff
		private var skipped:Boolean = false;
		private var _domination:Boolean;
		
		private var cellScale:Number;
		
		//Campaign
		//private var victoryScreen:fSprite;
		private var _victoryScreen:OnlineVictoryScreen;
		private var viewStats:BW_Button;
		//private var defeatScreen:fSprite;
		private var _defeatScreen:OnlineDefeatScreen;
		private var viewMapBt:Button;
		private var defeat:Boolean;
		public var success:Boolean;
		private var EndOfGame:Boolean = true;  // set when game is over, but still in the game
		
		// tutorial
		private var lastClickedCell:OnlineCell;
		private var inGameTutorial:InGameTutorial;
		
		// victory screen
		private var victoryCell:Sprite;
		private var victoryCellFrameCount:int;
		private var victoryParticles:PDParticleSystem;
		
		private var _renderAnimations:Boolean;
		private var _currentGameIndex:int;
		private var _onlineFileLoader:OnlineFileLoader;
		private var _onlineGameLoadingText:TextField;
		private var _onlineGameLoading:Boolean;
		private var _gameID:String;
		private var _clickedCell:OnlineCell;
		private var _confirmButton:BW_Button;
		private var _confirmSelector:BW_Selector;
		private var _locationSelector:BW_Selector;
		private var _playerIndex:int;
		
		// screens
		private var _surrenderScreen:fSprite;
		
		// live updates
		private var _liveUpdateCounter:int;
		private var _liveUpdating:Boolean;
		private var _playingOneMove:Boolean;
		
		//Zoom
		private var sliding:Boolean;
		private var targetCenter:Quad;
		
		private var previousObjectCenter:Point;
		private var currentObjectCenter:Point
		
		private var previousTargetCenter:Point;
		private var currentTargetCenter:Point;
		
		private var topBoundary:Point;
		private var bottomBoundary:Point;
		private var leftBoundary:Point;
		private var rightBoundary:Point;
		
		private var _otherPlayerSurrendered:OtherPlayerSurrendered;
		
		private var _waitingForLiveUpdateToFinishToTakeTurn:Boolean;
		
		private var _internetConnectivityError:InternetConnectivityError;
		
		private var _confirmButtonNew:Sprite;
		private var _confirmButtonBG:Scale9Image;
		
		private var _moveHasBeenTaken:Boolean;
		
		// zooming
		private var _holderInitialPosition:Point;
		private var _holderInitialScaleX:Number;
		private var _holderInitialScaleY:Number;
		private var _lastHolderScaleX:Number;
		private var _topBoundary:Number;
		private var _bottomBoundary:Number;
		private var _leftBoundary:Number;
		private var _rightBoundary:Number;
		private var _leftQuad:Quad;
		private var _rightQuad:Quad;
		private var _topQuad:Quad;
		private var _bottomQuad:Quad;
		private var _holderCenterQuad:Quad;
		private var _startLeftBoundary:Number;
		private var _startRightBoundary:Number;
		private var _startTopBoundary:Number;
		private var _startBottomBoundary:Number;
		private var _startLeftQuad:Quad;
		private var _startRightQuad:Quad;
		private var _startTopQuad:Quad;
		private var _startBottomQuad:Quad;
		private var _zoomPanDelay:int;
		private const ZOOM_PAN_DELAY_TIME:int = 15;
		private var _zoomButton:BW_SocialButton;
		
		private var boardOutline:BoardOutline;
		
		private var _viewMap:Boolean;
		
		private var _debug:Boolean = false;
		private var _player1WinButton:BW_Button;
		private var _player2WinButton:BW_Button;
		
		public function OnlineGameManager(main:Main, playerColors:Array, playerLocations:Array, map:Map, gameID:String /*, modes:Array */ )
		{
			super();
			
			//Main.updateDebugText("added ongameman");
			
			this.main = main;
			_gameID = gameID;
			
			percentBarRate = UPDATE_BAR_SPEED / PathogenTest.scaleFactor;
			percentBarRate *= PathogenTest.HD_Multiplyer;
			//if(PathogenTest.device == PathogenTest.iPAD_HD)
				//percentBarRate *= 2.0;
			
			victoryCell = null;
			victoryCellFrameCount = 0;
			
			// set up online game loading text
			var textHeight:int = PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor;
			var fontSize:int = PathogenTest.HD_Multiplyer * 16 / PathogenTest.scaleFactor;
			_onlineGameLoadingText = new TextField(PathogenTest.stageWidth / 2, textHeight, "Loading Game Data", "Questrial", fontSize, Color.WHITE);
			_onlineGameLoadingText.pivotX = _onlineGameLoadingText.width / 2;
			_onlineGameLoadingText.pivotY = _onlineGameLoadingText.height / 2;
			_onlineGameLoadingText.x = PathogenTest.stageWidth / 2;
			_onlineGameLoadingText.y = PathogenTest.stageHeight / 2;
			addChild(_onlineGameLoadingText);
			
			// load game from online data
			if (_gameID == "")
			{
				//trace("gameid = blank, so no loading text. _currentGameIndex = " + _currentGameIndex);
				
				this.playerColors = playerColors;
				this.playerLocations = playerLocations;
				mapData = map.layout;
				mapName = map.name;
				_mapSize = map.size;
				_renderAnimations = true;
			}
			else
			{
				// load player colors, player locations and map data
				// for now, just generate generic data
				this.playerColors = new Array(0, 1);
				this.playerLocations = new Array(0, 1);
				var mapID:int = main.entityManager.getCurrentMap(); 
				mapData = main.mapManager.getMap(mapID).layout;
				mapName = main.mapManager.getMap(mapID).name;
				_mapSize = main.mapManager.getMap(mapID).size;
				_renderAnimations = false;
			}
			
			mapSetup();
			
			Main.sSoundController.transitionToInGameMusic();
			
			_moveHasBeenTaken = false;
			_liveUpdating = false;
			_playingOneMove = false;
			_waitingForLiveUpdateToFinishToTakeTurn = false;
			_viewMap = false;
			
			_playerIndex = main.entityManager.whatIsMyPlayerIndexForCurrentGame();
			
			showData();
		}
		private function mapSetup():void
		{
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				var clipSprite:Sprite = new Sprite();
				var clipRectWidth:Number = PathogenTest.wTenPercent * 7;
				var clipRectHeight:Number = PathogenTest.hTenPercent * 9;
				var rect:Rectangle = new Rectangle(PathogenTest.stageCenter.x - (clipRectWidth/2),PathogenTest.stageCenter.y - (clipRectHeight/2), clipRectWidth, clipRectHeight)
				clipSprite.clipRect = rect;
				addChild(clipSprite);
			}
			
			tileHolder = new Sprite;
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				clipSprite.addChild(tileHolder);
			else
				addChild(tileHolder);
			
			//slight change in centering for a medium map
			if (_mapSize == 0)
			{
				gameBoardHeight = 14;
				gameBoardWidth = 16;
				cellScale = 1;
			}
			//slight change in centering for a medium map
			if (_mapSize == 1)
			{
				gameBoardHeight = 18;
				gameBoardWidth = 20;
				cellScale = .8;
			}
			//slight change in centering for a Large map
			if (_mapSize == 2)
			{
				gameBoardHeight = 22;
				gameBoardWidth = 24;
				cellScale = .65;
			}
			
			var tempGreenPlayer:OnlinePlayer = new OnlinePlayer(0,0,this);
			var tempRedPlayer:OnlinePlayer = new OnlinePlayer(1,1,this);
			var tempBluePlayer:OnlinePlayer = new OnlinePlayer(2,2,this);
			var tempOrangePlayer:OnlinePlayer = new OnlinePlayer(3,3,this);
			
			var tileCounter:int = 0;
			
			//Create the board
			for (var i:int = 0; i < gameBoardWidth; i++)
			{
				for (var j:int = 0; j < gameBoardHeight; j++)
				{
					var newCell:OnlineCell = new OnlineCell(tileCounter);
					newCell.init();
					
					newCell.gameManager = this;
					newCell.scaleX = cellScale -.05;
					newCell.scaleY = cellScale -.05;
					newCell.owner = null;
					
					var tileGap:Number = (newCell.width * 1);
					
					newCell.x = tileGap * i;
					newCell.y = tileGap * j;
					//import boundaries from the chosen map
					if (mapData[tileCounter] == 0)
					{
						newCell.state = 0;
					}
					if (mapData[tileCounter] == 4)
					{
						newCell.state = 10;
						newCell.visible = false;
					}
					if (mapData[tileCounter] == 3)
					{
						newCell.state = 9;
					}
					//Grey Tiles
					if (mapData[tileCounter] == 2)
					{
						newCell.state = 6;
					}
					if (mapData[tileCounter] == 1)
					{
						newCell.state = 5;
					}
					//Inporting Player Colors
					//Green
					if (mapData[tileCounter] == 5)
					{
						newCell.state = 1;
						newCell.owner = tempGreenPlayer;
					}
					if (mapData[tileCounter] == 6)
					{
						newCell.state = 2;
						newCell.owner = tempGreenPlayer;
					}
					if (mapData[tileCounter] == 7)
					{
						newCell.state = 3;
						newCell.owner = tempGreenPlayer;
					}
					if (mapData[tileCounter] == 8)
					{
						newCell.state = 4;
						newCell.owner = tempGreenPlayer;
					}
					//Red
					if (mapData[tileCounter] == 9)
					{
						newCell.state = 1;
						newCell.owner = tempRedPlayer;
					}
					if (mapData[tileCounter] == 10)
					{
						newCell.state = 2;
						newCell.owner = tempRedPlayer;
					}
					if (mapData[tileCounter] == 11)
					{
						newCell.state = 3;
						newCell.owner = tempRedPlayer;
					}
					if (mapData[tileCounter] == 12)
					{
						newCell.state = 4;
						newCell.owner = tempRedPlayer;
					}
					//Blue
					if (mapData[tileCounter] == 13)
					{
						newCell.state = 1;
						newCell.owner = tempBluePlayer;
					}
					if (mapData[tileCounter] == 14)
					{
						newCell.state = 2;
						newCell.owner = tempBluePlayer;
					}
					if (mapData[tileCounter] == 15)
					{
						newCell.state = 3;
						newCell.owner = tempBluePlayer;
					}
					if (mapData[tileCounter] == 16)
					{
						newCell.state = 4;
						newCell.owner = tempBluePlayer;
					}
					//Orange
					if (mapData[tileCounter] == 17)
					{
						newCell.state = 1;
						newCell.owner = tempOrangePlayer;
					}
					if (mapData[tileCounter] == 18)
					{
						newCell.state = 2;
						newCell.owner = tempOrangePlayer;
					}
					if (mapData[tileCounter] == 19)
					{
						newCell.state = 3;
						newCell.owner = tempOrangePlayer;
					}
					if (mapData[tileCounter] == 20)
					{
						newCell.state = 4;
						newCell.owner = tempOrangePlayer;
					}
					
					newCell.addTouchEvent();
					newCell.reset();
					newCell.setColor();
					
					tileHolder.addChild(newCell);
					gameBoard.push(newCell);
					
					tileCounter++;
				}
			}
			
			var tileWidth:Number = gameBoard[0].width / 2;
			var tileHeight:Number = gameBoard[0].height / 2;
			
			tileHolder.pivotX = tileHolder.width / 2;
			tileHolder.pivotY = tileHolder.height / 2;
			
			tileHolder.x = PathogenTest.stageCenter.x + tileWidth;
			tileHolder.y = PathogenTest.stageCenter.y + tileHeight;
			
			//tileHolder.x = (PathogenTest.stageCenter.x - (tileHolder.width / 2) + tileWidth);
			//tileHolder.y = (PathogenTest.stageCenter.y - (tileHolder.height / 2) + tileHeight);
			
			_holderInitialPosition = new Point(tileHolder.x, tileHolder.y);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				tileHolder.addEventListener(TouchEvent.TOUCH, onTouchLocation);
				
				// set the boundaries
				_topBoundary = tileHolder.y - tileHolder.height * 0.5;
				_bottomBoundary = tileHolder.y + tileHolder.height * 0.5;
				_leftBoundary = tileHolder.x - tileHolder.width * 0.5;
				_rightBoundary = tileHolder.x + tileHolder.width * 0.5;
				
				// set the initial boundaries
				_startTopBoundary = _topBoundary;
				_startBottomBoundary = _bottomBoundary;
				_startLeftBoundary = _leftBoundary;
				_startRightBoundary = _rightBoundary;
			}
			else
				_zoomPanDelay = ZOOM_PAN_DELAY_TIME;
			
			//Push Original State so that the player can roll back to the beginning of the game.
			var currentGameBoard:Array = [];
			var currentGameBoardOwnership:Array = [];
			
			for (var k:int = 0; k < gameBoard.length; k++)
			{				
				var currentTile:OnlineCell = gameBoard[k];
				
				currentGameBoard.push(currentTile.state);
				
				if (currentTile.owner != null)
				{
					currentGameBoardOwnership.push(currentTile.owner.playerTeam);
				}
				else
				{
					currentGameBoardOwnership.push(5);
				}
			}
			
			playLog.push(currentGameBoard);
			playLogOwnership.push(currentGameBoardOwnership);
			
			for each(var cell:OnlineCell in gameBoard)
			{
				cell.adjacentTopCell = gameBoard[cell.location - 1];
				cell.adjacentBottomCell = gameBoard[cell.location + 1];
				cell.adjacentLeftCell = gameBoard[cell.location - gameBoardHeight];
				cell.adjacentRightCell = gameBoard[cell.location + gameBoardHeight];
			}
			
			//Create Quit Button
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				quitButton = new BW_Button(0.75, 1.0, "", 0);
			else
				quitButton = new BW_Button(0.5, 0.7, "", 0);
			quitButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(quitButton);
			quitButton.x = PathogenTest.wTenPercent *.5;
			quitButton.y = PathogenTest.hTenPercent * 9.35;
			quitButton.addEventListener(TouchEvent.TOUCH, addQuitScreen);
			
			// rules button
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				rulesButton = new BW_Button(0.75, 1.0, "", 0);
			else
				rulesButton = new BW_Button(0.5, 0.7, "", 0);
			rulesButton.addImage(Main.assets.getTexture("Pathogen_BTN_Help"), Main.assets.getTexture("Pathogen_BTN_Help_down"));
			addChild(rulesButton);
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				rulesButton.x = PathogenTest.wTenPercent * 1.3;
			else
				rulesButton.x = PathogenTest.wTenPercent * 1.1;
			rulesButton.y = quitButton.y;
			rulesButton.addEventListener(TouchEvent.TOUCH, addInGameTutorial);
			
			//Create Options Button
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				optionsButton = new BW_Button(0.75, 1.0, "", 0);
			else
				optionsButton = new BW_Button(0.5, 0.7, "", 0);
			optionsButton.addImage(Main.assets.getTexture("Pathogen_BTN_Options"), Main.assets.getTexture("Pathogen_BTN_Down"));
			addChild(optionsButton);
			optionsButton.x = PathogenTest.wTenPercent * 9.5;
			optionsButton.y = quitButton.y;
			optionsButton.addEventListener(TouchEvent.TOUCH, addOptions);
			
			// create confirm button
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_confirmButton = new BW_Button(1.5, 1.0, "Confirm", 24);
			else
				_confirmButton = new BW_Button(1.5, 0.75, "Confirm", 24);
			_confirmButton.x = PathogenTest.stageCenter.x;
			_confirmButton.y = quitButton.y;
			addChild(_confirmButton);
			_confirmButton.addEventListener(TouchEvent.TOUCH, onTouchConfirm);
			_confirmButton.visible = false;
			
			// debug buttons
			if (_debug)
			{
				// player 1 win button
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_player1WinButton = new BW_Button(0.75, 1.0, "1", 36);
				else
					_player1WinButton = new BW_Button(0.5, 0.7, "1", 36);
				addChild(_player1WinButton);
				_player1WinButton.x = rulesButton.x + rulesButton.width;
				_player1WinButton.y = quitButton.y;
				_player1WinButton.addEventListener(TouchEvent.TOUCH, onTouchPlayer1Win);
				
				// player 2 win button
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_player2WinButton = new BW_Button(0.75, 1.0, "2", 36);
				else
					_player2WinButton = new BW_Button(0.5, 0.7, "2", 36);
				addChild(_player2WinButton);
				_player2WinButton.x = _player1WinButton.x + _player1WinButton.width;
				_player2WinButton.y = quitButton.y;
				_player2WinButton.addEventListener(TouchEvent.TOUCH, onTouchPlayer2Win);
			}
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				// create zoom button
				_zoomButton = new BW_SocialButton(1.5, 1.0, "Zoom", "Zoom", 24, Main.assets.getTexture("ZoomIn_Off"), Main.assets.getTexture("ZoomOut_off"));
				_zoomButton.toggle = true;
				_zoomButton.x = PathogenTest.stageCenter.x;
				_zoomButton.y = PathogenTest.hTenPercent * 9.35;
				//_confirmButtonNew.x = PathogenTest.stageCenter.x - (_confirmButtonNew.width * 0.75);
				_confirmButton.x = PathogenTest.stageCenter.x - (_confirmButton.width * 0.75);
				addChild(_zoomButton); 
				_zoomButton.addEventListener(TouchEvent.TOUCH, onTouchZoom);
			}
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_confirmSelector = new BW_Selector(1.55, 1.05);
			else
				_confirmSelector = new BW_Selector(1.55, 0.8);
			addChild(_confirmSelector);
			_confirmSelector.x = _confirmButton.x;
			_confirmSelector.y = _confirmButton.y;
			setChildIndex(_confirmSelector, 0);
			_confirmSelector.visible = false;
			
			var selectorDisplay:Image = new Image(Main.assets.getTexture("Selector"));
			
			_locationSelector = new BW_Selector(0, 0, selectorDisplay);
			tileHolder.addChild(_locationSelector);
			_locationSelector.scaleX = cellScale;
			_locationSelector.scaleY = cellScale;
			_locationSelector.visible = false;
			
			boardOutline = new BoardOutline();
			addChild(boardOutline);
			
			//Create all players
			playerSetup();
			
			addPercentUI();
			showPercentBar = true;
			checkEndGame();
			
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		// debug buttons
		private function onTouchPlayer1Win(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && isItMyTurn())
			{
				if (!_liveUpdating)
				{
					var playerSave:OnlinePlayer;
					var openCellCounter:int;
					var i:int;
					
					playerSave = currentPlayer;
					openCellCounter = 0;
					currentPlayer = playerArray[0];
					
					for (i=0; i<gameBoard.length; i++)
					{
						if (gameBoard[i].state == 0)
						{
							if (openCellCounter > 0)
							{
								gameBoard[i].state = 1;
								gameBoard[i].owner = currentPlayer;
								gameBoard[i].setColor();
								gameBoard[i].reset();
							}
							openCellCounter++;
						}
					}
					currentPlayer = playerSave;
				}
			}
		}
		private function onTouchPlayer2Win(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && isItMyTurn())
			{
				if (!_liveUpdating)
				{
					var playerSave:OnlinePlayer;
					var openCellCounter:int;
					var i:int;
					
					playerSave = currentPlayer;
					openCellCounter = 0;
					currentPlayer = playerArray[1];
					
					for (i=0; i<gameBoard.length; i++)
					{
						if (gameBoard[i].state == 0)
						{
							if (openCellCounter > 0)
							{
								gameBoard[i].state = 1;
								gameBoard[i].owner = currentPlayer;
								gameBoard[i].setColor();
								gameBoard[i].reset();
							}
							openCellCounter++;
						}
					}
					currentPlayer = playerSave;
				}
			}
		}
		
		public function getTileHolderLocation():Point
		{
			var tileHolderLocation:Point = new Point(tileHolder.x, tileHolder.y);
			
			return tileHolderLocation;
		}
		private function addPercentUI():void
		{
			percentBar = new Sprite();
			addChild(percentBar);
			percentBar.pivotX = percentBar.width/2;
			percentBar.pivotY = percentBar.height/2;
			percentBar.x = PathogenTest.stageCenter.x;
			percentBar.y = PathogenTest.hTenPercent * .5;
			
			if (Main.isPhone() && _mapSize != 0)
			//if(PathogenTest.device == PathogenTest.iPHONE_4 && _mapSize != 0)
				percentBar.y = PathogenTest.hTenPercent * .3;
			else
				percentBar.y = PathogenTest.hTenPercent * .5;
			
			var percentBarFrame:Quad = new Quad(PathogenTest.wTenPercent * 7, PathogenTest.hTenPercent * .2, Color.BLACK)
			percentBar.addChild(percentBarFrame);
			percentBarFrame.pivotX = percentBarFrame.width/2;
			percentBarFrame.pivotY = percentBarFrame.height/2;
			percentBarFrame.alpha = .7;
			
			percentFrameWidth = percentBarFrame.width;
			
			for each (var player:OnlinePlayer in playerArray)
			{
				//player.percentBar;
				player.percentBar = new Quad(1,percentBarFrame.height, player.playerHexColor);
				player.percentBar.pivotY = player.percentBar.height /2;
				player.percentBar.alpha = .6;
				percentBar.addChild(player.percentBar);
				player.percentBar.visible = false;
				
				player.percentBarOfTierThree = new Quad(1,percentBarFrame.height, player.playerHexColor);
				player.percentBarOfTierThree.pivotY = player.percentBarOfTierThree.height /2;
				player.percentBarOfTierThree.alpha = .4;
				percentBar.addChild(player.percentBarOfTierThree);
				player.percentBarOfTierThree.visible = false;
			}
			
			for (var i:int = 0; i < 11; i++)
			{
				var tenPercentMark:Quad = new Quad(2, percentBarFrame.height, Color.GRAY);
				percentBar.addChild(tenPercentMark);
				tenPercentMark.pivotY = tenPercentMark.height/2;
				
				tenPercentMark.x = (percentBarFrame.x - percentBarFrame.width/2)  + ((percentBarFrame.width/10) * i);
			}
		}
		private function hidePercentUI(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(percentBar.visible == false)
				{
					percentBar.visible = true;
					percentCheckbox.upState = Main.assets.getTexture("CheckBoxDown");
					percentCheckbox.downState = Main.assets.getTexture("CheckBoxUp");
				}
				else
				{
					percentBar.visible = false
					percentCheckbox.upState = Main.assets.getTexture("CheckBoxUp");
					percentCheckbox.downState = Main.assets.getTexture("CheckBoxDown");
				}
			}
		}
		private function addOptions(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && !moveUnderway)
			{
				optionsScreen = new OnlineOptions(this);
				addChild(optionsScreen);
			}
		}
		public function removeOptions():void
		{
			optionsScreen.dispose();
			removeChild(optionsScreen);
			optionsScreen = null;
		}
		private function addInGameTutorial($e:TouchEvent):void
		{
			if($e)
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				var open:Boolean = true;
			
			if (touch || open)
			{
				inGameTutorial = new InGameTutorial(null, this);
				addChild(inGameTutorial);
			}
		}
		
		private function onTouchLocation($e:TouchEvent):void
		{
			var touches:Vector.<Touch>
			
			touches = $e.getTouches(this, TouchPhase.MOVED);
			
			if (touches.length == 1)
			{
				//Main.updateDebugText("1 touch");
				
				// one finger touching -> move
				var delta:Point = touches[0].getMovement(parent);
				
				// if the holder is not going off screen, move it left or right
				if (delta.x < 0.0)
				{
					if (_rightBoundary > _startRightBoundary)
					{
						tileHolder.x += delta.x;
						if ((tileHolder.x + (tileHolder.width / 2)) < _startRightBoundary)
							tileHolder.x = _startRightBoundary - (tileHolder.width / 2);
					}
				}
				else if (delta.x > 0.0)
				{
					if (_leftBoundary < _startLeftBoundary)
					{
						tileHolder.x += delta.x;
						if ((tileHolder.x - (tileHolder.width / 2)) > _startLeftBoundary)
							tileHolder.x = _startLeftBoundary + (tileHolder.width / 2);
					}
				}
				
				// if the holder is not going off screen, move it up or down
				if (delta.y < 0.0)
				{
					if (_bottomBoundary > _startBottomBoundary)
					{
						tileHolder.y += delta.y;
						if ((tileHolder.y + (tileHolder.height / 2)) < _startBottomBoundary)
							tileHolder.y = _startBottomBoundary - (tileHolder.height / 2);
					}
				}
				else if (delta.y > 0.0)
				{
					if (_topBoundary < _startTopBoundary)
					{
						tileHolder.y += delta.y;
						if ((tileHolder.y - (tileHolder.height / 2)) > _startTopBoundary)
							tileHolder.y = _startTopBoundary + (tileHolder.height / 2);
					}
				}
				
				_topBoundary = tileHolder.y - (tileHolder.height / 2);
				_bottomBoundary = tileHolder.y + (tileHolder.height / 2);
				_leftBoundary = tileHolder.x - (tileHolder.width / 2);
				_rightBoundary = tileHolder.x + (tileHolder.width / 2);
				
				_zoomPanDelay = 0;
			}
			else if (touches.length == 2 && _zoomPanDelay >= ZOOM_PAN_DELAY_TIME)
			{
				//Main.updateDebugText("2 touch");
				
				// two fingers touching -> scale
				var touchA:Touch = touches[0];
				var touchB:Touch = touches[1];
				
				var currentPosA:Point  = touchA.getLocation(this);
				var previousPosA:Point = touchA.getPreviousLocation(this);
				var currentPosB:Point  = touchB.getLocation(this);
				var previousPosB:Point = touchB.getPreviousLocation(this);
				
				var currentVector:Point  = currentPosA.subtract(currentPosB);
				var previousVector:Point = previousPosA.subtract(previousPosB);
				
				// scale
				var sizeDiff:Number = currentVector.length / previousVector.length;
				
				if(sizeDiff > 1.0 && tileHolder.scaleX <= 2.0)
				{
					tileHolder.scaleX = 2.0;
					tileHolder.scaleY = 2.0;
				}
				else if (sizeDiff < 1.0 && tileHolder.scaleX >= 1.0)
				{
					tileHolder.scaleX = 1.0;
					tileHolder.scaleY = 1.0;
				}
				
				// check if scaling smaller than smallest, and if it is, adjust to the smallest size
				if (tileHolder.scaleX < _holderInitialScaleX)
				{
					tileHolder.scaleX = _holderInitialScaleX;
				}
				if (tileHolder.scaleY < _holderInitialScaleY)
				{
					tileHolder.scaleY = _holderInitialScaleY;
				}
				
				// check if scaling is the initial, and if so, position at initial position
				if (tileHolder.scaleX == _holderInitialScaleX)
				{
					tileHolder.x = _holderInitialPosition.x;
					tileHolder.y = _holderInitialPosition.y;
				}
				
				if ((tileHolder.x + (tileHolder.width / 2)) < _startRightBoundary)
					tileHolder.x = _startRightBoundary - (tileHolder.width / 2);
				if ((tileHolder.x - (tileHolder.width / 2)) > _startLeftBoundary)
					tileHolder.x = _startLeftBoundary + (tileHolder.width / 2);
				if ((tileHolder.y + (tileHolder.height / 2)) < _startBottomBoundary)
					tileHolder.y = _startBottomBoundary - (tileHolder.height / 2);
				if ((tileHolder.y - (tileHolder.height / 2)) > _startTopBoundary)
					tileHolder.y = _startTopBoundary + (tileHolder.height / 2);
				
				_topBoundary = tileHolder.y - (tileHolder.height / 2);
				_bottomBoundary = tileHolder.y + (tileHolder.height / 2);
				_leftBoundary = tileHolder.x - (tileHolder.width / 2);
				_rightBoundary = tileHolder.x + (tileHolder.width / 2);
				
				_zoomPanDelay = 0;
			}
		}
		
		private function onTouchZoom(event:TouchEvent):void
		{
			if(event)
			{
				var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
				
				if (touch && _zoomPanDelay >= ZOOM_PAN_DELAY_TIME)
				{
					if(tileHolder.scaleX < 2.0)
					{
						tileHolder.scaleX = 2.0;
						tileHolder.scaleY = 2.0;
					}
					else
					{
						tileHolder.scaleX = 1.0;
						tileHolder.scaleY = 1.0;
					}
					
					// check if scaling smaller than smallest, and if it is, adjust to the smallest size
					if (tileHolder.scaleX < _holderInitialScaleX)
					{
						tileHolder.scaleX = _holderInitialScaleX;
					}
					if (tileHolder.scaleY < _holderInitialScaleY)
					{
						tileHolder.scaleY = _holderInitialScaleY;
					}
					
					// check if scaling is the initial, and if so, position at initial position
					if (tileHolder.scaleX == _holderInitialScaleX)
					{
						tileHolder.x = _holderInitialPosition.x;
						tileHolder.y = _holderInitialPosition.y;
					}
					
					if ((tileHolder.x + (tileHolder.width / 2)) < _startRightBoundary)
						tileHolder.x = _startRightBoundary - (tileHolder.width / 2);
					if ((tileHolder.x - (tileHolder.width / 2)) > _startLeftBoundary)
						tileHolder.x = _startLeftBoundary + (tileHolder.width / 2);
					if ((tileHolder.y + (tileHolder.height / 2)) < _startBottomBoundary)
						tileHolder.y = _startBottomBoundary - (tileHolder.height / 2);
					if ((tileHolder.y - (tileHolder.height / 2)) > _startTopBoundary)
						tileHolder.y = _startTopBoundary + (tileHolder.height / 2);
					
					_topBoundary = tileHolder.y - (tileHolder.height / 2);
					_bottomBoundary = tileHolder.y + (tileHolder.height / 2);
					_leftBoundary = tileHolder.x - (tileHolder.width / 2);
					_rightBoundary = tileHolder.x + (tileHolder.width / 2);
					
					_zoomPanDelay = 0;
				}
			}
		}
		
		public function removeInGameTutorial():void
		{
			inGameTutorial.GarbageCollection();
			inGameTutorial.dispose();
			removeChild(inGameTutorial);
			inGameTutorial = null;
		}
		public function addSurrenderScreen():void
		{
			_surrenderScreen = new fSprite();
			addChild(_surrenderScreen);
			_surrenderScreen.pivotX = _surrenderScreen.width / 2;
			_surrenderScreen.pivotY = _surrenderScreen.height / 2;
			_surrenderScreen.x = PathogenTest.stageCenter.x;
			_surrenderScreen.y = PathogenTest.stageCenter.y;
			
			const texture:Texture = Main.assets.getTexture("scale9");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20, 20, 20, 20));
			
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			_surrenderScreen.addChild(transBackground);
			transBackground.alpha = 0.75;
			
			var background:Scale9Image = new Scale9Image(textures);
			_surrenderScreen.addChild(background);
			background.width = PathogenTest.wTenPercent * 6;
			background.height = PathogenTest.hTenPercent * 3.5;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = 0.5;
			
			var header:Scale9Image = new Scale9Image(textures);
			_surrenderScreen.addChild(header);
			header.width = background.width;
			header.height = PathogenTest.hTenPercent;
			header.pivotX = header.width/2;
			header.y = -background.height/2;
			
			var fontSize:int =PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
			var title:TextField = new TextField(header.width, header.height, "Surrender", "Questrial", fontSize, Color.WHITE);
			title.pivotX = title.width/2;
			_surrenderScreen.addChild(title);
			title.y = header.y;
			
			fontSize =PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor;
			var textY:int =PathogenTest.HD_Multiplyer * 200 / PathogenTest.scaleFactor;
			var textfield:TextField = new TextField(_surrenderScreen.width, textY, "Are you sure you want to Surrender", "Dekar", fontSize, Color.WHITE);
			textfield.pivotX = textfield.width/2;
			textfield.pivotY = textfield.height/2;
			_surrenderScreen.addChild(textfield);
			
			var yesButton:Utils.BW_Button = new Utils.BW_Button(2, .75, "Yes", 32);
			_surrenderScreen.addChild(yesButton);
			yesButton.x = -background.width/2 + yesButton.width/2 + PathogenTest.wTenPercent * .2;
			yesButton.y = background.height/2 - yesButton.height;
			yesButton.addEventListener(TouchEvent.TOUCH, surrender);
			
			var noButton:Utils.BW_Button = new Utils.BW_Button(2, .75, "No", 32);
			_surrenderScreen.addChild(noButton);
			noButton.x = background.width/2 - noButton.width/2 - PathogenTest.wTenPercent * .2;
			noButton.y = background.height/2 - yesButton.height;
			noButton.addEventListener(TouchEvent.TOUCH, removeSurrenderScreen);
			
			_surrenderScreen.alpha = 0.0;
			_surrenderScreen.startfadeIn();
		}
		private function removeSurrenderScreen(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_surrenderScreen.startfadeOut();
			}
		}
		private function surrender(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				quitScreen.startFadeOut();
				main.entityManager.surrenderCurrentGame();
				_surrenderScreen.startfadeOut();
				checkEndGame();
			}
		}
		private function addQuitScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				quitScreen = new OnlineQuitScreen(this);
				addChild(quitScreen);
				quitScreen.x = PathogenTest.stageCenter.x;
				quitScreen.y = PathogenTest.stageCenter.y;
			}
		}
		public function removeQuitScreen():void
		{
			if (quitScreen)
			{
				quitScreen.dispose();
				removeChild(quitScreen);
				quitScreen = null;
			}
		}
		public function iPhoneBacktoGamesList():void
		{
			//Main.updateDebugText("Iphone Return to Games List");
			
			garbageCollection();
			main.returnToOnlineGamesList();
		}
		public function backToGamesList():void
		{
			//Main.debugText.text = "Return to Games List";
			
			garbageCollection();
			main.returnToOnlineGamesList();
		}
		private function playerSetup():void
		{
			var i:int;
			
			//Create players
			numOfPlayers = playerLocations.length;
			var playerNicknameArray:Array = main.entityManager.getPlayerNicknameArrayFromCurrentGame();
			
			for (i = 0; i < numOfPlayers; i++)
			{
				var nickString:String = playerNicknameArray[i];
				if (nickString == "" || nickString == null)
					nickString = "Waiting";
				var newPlayer:OnlinePlayer = new OnlinePlayer(playerColors[i],playerLocations[i], this, nickString);
				addChild(newPlayer);
				playerArray.push(newPlayer);
				playerArray[i].setPlayerArrayIndex(i);
			}
			//Set the current player to player 1
			currentPlayerNum = 0;
			currentPlayer = playerArray[currentPlayerNum];
			boardOutline.changeColors(currentPlayer.playerHexColor);
			
			// if iphone - selector resize
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				currentPlayer.increaseSelectorSizes();
			}
			
			//Create Tile Selector icon
			var selectorDisplay:Image = new Image(Main.assets.getTexture("Selector"));
			
			tileSelector = new BW_Selector(0,0, selectorDisplay);
			addChild(tileSelector);
			setChildIndex(tileSelector, 0);
			
			// if iphone - selector resize
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				tileSelector.scaleX = 1.3;
				tileSelector.scaleY = 1.3;
			}
			
			tileSelector.x = currentPlayer.cellSelectorArray[0].x;
			tileSelector.y = currentPlayer.cellSelectorArray[0].y;
			
			//Automatically select the level 1 tile of the current player
			cellSelectorHasBeenClicked(currentPlayer.cellSelectorArray[0]);
			
			for each (var cellSelector:OnlineCellSelector in currentPlayer.cellSelectorArray)
			{
				if (cellSelector.countDownIndicator != null)
				{
					cellSelector.incrementLvl();
				}
			}
		}
		public function cellSelectorHasBeenClicked(whatCellSelector:OnlineCellSelector):void
		{
			//trace("selector " + whatCellSelector.type + " has been clicked"); 
			if (isItMyTurn())
			{
				if(whatCellSelector.owner == currentPlayer)
				{
					currentCellTypeSelected = whatCellSelector;
					currentCellTypeSelected.alpha = 1.0;
					
					// if iphone - adjust selector position
					if (Main.isPhone())
					//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
						tileSelector.x = currentCellTypeSelected.x + (currentCellTypeSelected.display.x * 1.3);
					else
						tileSelector.x = currentCellTypeSelected.x + currentCellTypeSelected.display.x;
					tileSelector.y = currentCellTypeSelected.y;
					
					if (currentPlayer)
					{
						for (var i:int=0; i<currentPlayer.cellSelectorArray.length; i++)
						{
							if (currentPlayer.cellSelectorArray[i] != currentCellTypeSelected)
							{
								currentPlayer.cellSelectorArray[i].alpha = OTHER_SELECTORS_ALPHA;
							}
						}
					}
				}
			}
		}
		public function cellSpaceHasBeenClicked(whatCell:OnlineCell):void
		{
			//Main.updateDebugText("cellSpaceHasBeenClicked");
			if (!_waitingForLiveUpdateToFinishToTakeTurn)
			{
				// if the player is not AI
				if (isItMyTurn())
				{
					if (Main.isAndroid() && Main.isPhone())
					{
						//Make sure the tile is not already flipping.
						if (!moveUnderway && !_viewMap)
						{
							if (tileHolder.scaleX < 2.0 || _zoomPanDelay >= ZOOM_PAN_DELAY_TIME)
							{
								if(!replay)
								{
									if (whatCell.state == 0 || whatCell.state == 1 || whatCell.state == 2 || whatCell.state == 3 || whatCell.state == 5 || whatCell.state == 6)
									{
										if (_clickedCell != null)
										{
											if (whatCell != _clickedCell)
											{
												_clickedCell.scaleX = (cellScale - .05);
												_clickedCell.scaleY = (cellScale - .05);
											}
										}
										
										_clickedCell = whatCell;
										_clickedCell.scaleX = (cellScale - .05) + CELL_SCALE_FOR_CLICK;
										_clickedCell.scaleY = (cellScale - .05) + CELL_SCALE_FOR_CLICK;
										_confirmButton.visible = true;
										//_confirmButtonNew.visible = true;
										_confirmSelector.visible = true;
										
										if (_zoomButton)
											_zoomButton.x = PathogenTest.stageCenter.x + (_zoomButton.width * 0.75);
										
										//var globalLocation:Point = whatCell.localToGlobal(new Point(0,0));
										
										_locationSelector.x = whatCell.x;
										_locationSelector.y = whatCell.y;
										_locationSelector.visible = true;
									}
								}
								else
								{
									_clickedCell = whatCell;
									
									//Set the current player/selected cell type for the cell
									_clickedCell.currentPlayer = currentPlayer;
									_clickedCell.selectedCellType = currentCellTypeSelected;
									_clickedCell.changeState();
									lastClickedCell = _clickedCell;
									moveUnderway = true;
									_clickedCell = null;
									_confirmButton.visible = false;
									//_confirmButtonNew.visible = false;
									_confirmSelector.visible = false;
									_locationSelector.visible = false;
									
									if (_zoomButton)
										_zoomButton.x = PathogenTest.stageCenter.x;
								}
							}
						}
					}
					else
					{
						//Make sure the tile is not already flipping.
						if (!moveUnderway && !_viewMap && _zoomPanDelay >= ZOOM_PAN_DELAY_TIME)
						{
							if(!replay)
							{
								if (whatCell.state == 0 || whatCell.state == 1 || whatCell.state == 2 || whatCell.state == 3 || whatCell.state == 5 || whatCell.state == 6)
								{
									if (_clickedCell != null)
									{
										if (whatCell != _clickedCell)
										{
											_clickedCell.scaleX = (cellScale - .05);
											_clickedCell.scaleY = (cellScale - .05);
										}
									}
									
									_clickedCell = whatCell;
									_clickedCell.scaleX = (cellScale - .05) + CELL_SCALE_FOR_CLICK;
									_clickedCell.scaleY = (cellScale - .05) + CELL_SCALE_FOR_CLICK;
									_confirmButton.visible = true;
									//_confirmButtonNew.visible = true;
									_confirmSelector.visible = true;
									
									if (_zoomButton)
										_zoomButton.x = PathogenTest.stageCenter.x + (_zoomButton.width * 0.75);
									
									//var globalLocation:Point = whatCell.localToGlobal(new Point(0,0));
									
									_locationSelector.x = whatCell.x;
									_locationSelector.y = whatCell.y;
									_locationSelector.visible = true;
								}
							}
							else
							{
								_clickedCell = whatCell;
								
								//Set the current player/selected cell type for the cell
								_clickedCell.currentPlayer = currentPlayer;
								_clickedCell.selectedCellType = currentCellTypeSelected;
								_clickedCell.changeState();
								lastClickedCell = _clickedCell;
								moveUnderway = true;
								_clickedCell = null;
								_confirmButton.visible = false;
								//_confirmButtonNew.visible = false;
								_confirmSelector.visible = false;
								_locationSelector.visible = false;
								
								if (_zoomButton)
									_zoomButton.x = PathogenTest.stageCenter.x;
							}
						}
					}
				}
			}
		}
		public function shiftStack(cell:OnlineCell):void
		{
			tileHolder.setChildIndex(cell, tileHolder.numChildren -1);
		}
		private function update(event:Event):void
		{
			if (_zoomPanDelay < ZOOM_PAN_DELAY_TIME)
				_zoomPanDelay++;
			
			CurrentFrameCount++;
			
			//Always keep the clock running so it displays the correct time even if the player turns it on mid-game.
			if (CurrentFrameCount >= 30)
			{
				secondCounter++;
				
				CurrentFrameCount = 0;
			}
			
			if (secondCounter >= 60)
			{
				minuteCounter++
					secondCounter = 0;
			}
			
			if(moveUnderway)
			{
				if (lastClickedCell.notAValidMove)
				{
					//trace("Bad Move");
					lastClickedCell.notAValidMove = false;
					lastClickedCell.flipping = false;
					moveUnderway = false;
				}
				else
				{
					if (!_moveHasBeenTaken)
						_moveHasBeenTaken = true;
					
					var moveComplete:Boolean = true;
					
					for each (var i:OnlineCell in gameBoard)
					{
						if (i.flipping == true)
						{
							moveComplete = false;
						}
					}
					
					//trace("move complete = " + moveComplete);
					
					if (moveComplete)
					{
						moveUnderway = false;
						changePlayers();
						
						if (_playingOneMove)
						{
							_liveUpdating = false;
						}
					}
				}
				
				// this method was an attempt at updating the percent bar while the player's cells are cascading
				
				// update player percent bars
				var playerCounter:int = 0;
				for each(var player:OnlinePlayer in playerArray)
				{
					// adjust bar widths
					if (player.percentBar.width < player.percentBarWidth)
					{
						player.percentBar.width += percentBarRate;
						if (player.percentBar.width >= player.percentBarWidth)
							player.percentBar.width = player.percentBarWidth;
					}
					else if (player.percentBar.width > player.percentBarWidth)
					{
						player.percentBar.width -= percentBarRate;
						if (player.percentBar.width <= player.percentBarWidth)
							player.percentBar.width = player.percentBarWidth;
					}
					if (player.percentBarOfTierThree.width < player.percentBarOfTierThreeWidth)
					{
						player.percentBarOfTierThree.width += percentBarRate;
						if (player.percentBarOfTierThree.width >= player.percentBarOfTierThreeWidth)
							player.percentBarOfTierThree.width = player.percentBarOfTierThreeWidth;
					}
					else if (player.percentBarOfTierThree.width > player.percentBarOfTierThreeWidth)
					{
						player.percentBarOfTierThree.width -= percentBarRate;
						if (player.percentBarOfTierThree.width <= player.percentBarOfTierThreeWidth)
							player.percentBarOfTierThree.width = player.percentBarOfTierThreeWidth;
					}
					
					// adjust bar locations
					if (playerCounter == 0)
					{
						player.percentBar.x = -((percentBar.width/2) - 1);
					}
					else
					{
						player.percentBar.x = playerArray[playerCounter - 1].percentBar.x + playerArray[playerCounter - 1].percentBar.width;
					}
					player.percentBarOfTierThree.x = player.percentBar.x;
					
					playerCounter++;
				}
			}
			else
			{
				_liveUpdateCounter++;
				
				if (_liveUpdateCounter > LIVE_UPDATE_COUNT && !replay)
				{
					_liveUpdateCounter = 0;
					_liveUpdating = true;
					liveUpdate();
				}
			}
			
			if (victoryCell)
			{
				if (victoryCell.alpha < 1.0)
				{
					victoryCellFrameCount++;
					
					if (victoryCellFrameCount > 30)
					{
						victoryCell.alpha += 0.025;
						if (victoryCell.alpha >= 1.0)
						{
							victoryCell.alpha = 1.0;
						}
					}
				}
			}
		}
		private function changePlayers():void
		{
			//Check to see if the game is over
			checkEndGame();
			
			resetTiles();
			
			if(!skipped)
			{
				currentPlayer.cellSelectorArray[1].removeWhiteFlash();
				currentPlayer.cellSelectorArray[2].removeWhiteFlash();
				currentPlayer.cellSelectorArray[3].removeWhiteFlash();
				
				//Reseting Special Tile if used
				if (currentCellTypeSelected.type == 1)
					currentPlayer.cellSelectorArray[1].resetSelectedTile();
				if (currentCellTypeSelected.type == 2)
					currentPlayer.cellSelectorArray[2].resetSelectedTile();
				if (currentCellTypeSelected.type == 3)
					currentPlayer.cellSelectorArray[3].resetSelectedTile();
			}
			
			if (!replay)
			{
				//Have each Selected Tile remember their state.
				for each (var player:OnlinePlayer in playerArray)
				{
					//trace("player " + player.teamName);
					for each (var selector:OnlineCellSelector in player.cellSelectorArray)
					{
						selector.rememberState();
					}
				}
				
				//Pushing Game State to Log
				var currentGameBoard:Array = [];
				var currentGameBoardOwnership:Array = [];
				
				for each (var cell:OnlineCell in gameBoard)
				{
					currentGameBoard.push(cell.state);
					
					if (cell.owner != null)
					{
						currentGameBoardOwnership.push(cell.owner.playerTeam);
					}
					else
					{
						currentGameBoardOwnership.push(5);
					}
				}
				
				playLog.push(currentGameBoard);
				playLogOwnership.push(currentGameBoardOwnership);
				
				currentPlayer.historyArrayOfTiles.push(currentPlayer.totalNumOfTiles);
				
				var cellLocation:int;
				
				if (skipped)
				{
					cellLocation = 0;
				}
				else
				{
					cellLocation = lastClickedCell.location;
				}
				
				var newTurn:TurnRecord = new TurnRecord(currentCellTypeSelected.type, cellLocation, currentPlayerNum, 0);
				turnList.push(newTurn);
				
				if (_renderAnimations && !_liveUpdating)
				{
					//trace("taking turn in game manager");
					main.entityManager.takeTurnForCurrentGame(currentCellTypeSelected.type, cellLocation, false, EndOfGame);
				}
			}
			
			//Increment to next player
			currentPlayerNum++;
			
			//If all player have taken a turn, end the round, begin another.
			if (currentPlayerNum >= numOfPlayers)
			{
				currentPlayerNum = 0;
				roundCounter++;
			}
			
			//Assign Current Player
			currentPlayer = playerArray[currentPlayerNum];
			boardOutline.changeColors(currentPlayer.playerHexColor);
			
			// modify selector sizes and places
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				for (var i:int=0; i<playerArray.length; i++)
				{
					playerArray[i].revertSelectors();
				}
				
				if (!currentPlayer.getAiControl())
				{
					currentPlayer.increaseSelectorSizes();
				}
			}
			
			cellSelectorHasBeenClicked(currentPlayer.cellSelectorArray[0]);
			
			//Let all SelectedTiles know that their players turn has begun.
			for each (var cellSelector:OnlineCellSelector in currentPlayer.cellSelectorArray)
			{
				if (cellSelector.countDownIndicator != null)
				{
					cellSelector.incrementLvl();
					if (cellSelector.type > 0)
					{
						//trace("aligning cells 2 - cellSelector.type = " + cellSelector.type + "currentCellTypeSelected.type = " + currentCellTypeSelected.type);
						cellSelector.alignCells();
					}
				}
			}
			
			if (isItMyTurn())
				tileSelector.visible = true;
			else
				tileSelector.visible = false;
			
			// for replay
			if (playReplay)
			{
				if (turnList[currentTurn].type == 0)
				{
					currentTurn++;
					Starling.juggler.delayCall(delayedTurn, 0.3);
				}
				else	
				{
					currentTurn++;
					setTimeLine();
				}
			}
			if (!replay)
				renderBoard();
		}
		private function delayedTurn():void
		{
			setTimeLine();
		}
		private function checkEndGame():void
		{
			var currentCurrentPlayer:OnlinePlayer = currentPlayer;
			
			//Assume the game is over
			var boundaryTileCounter:int = 0;
			var currentPlayer:OnlinePlayer;
			
			EndOfGame = true;
			
			//Tell each player to reset their counts to 0.
			for each (currentPlayer in playerArray)
			{
				currentPlayer.resetEndGameTileCount();
			}
			
			for (var i:int = 0; i < gameBoard.length; i++)
			{
				var currentTile:OnlineCell = gameBoard[i];
				
				currentTile.previousOwner = currentTile.owner;
				
				//If even a single space is empty
				if (currentTile.state == 0)
				{
					EndOfGame = false;
				}
					//Else, check state of the Cell
				else
				{
					//Dot cells
					if (currentTile.state == 1)
					{
						for each (currentPlayer in playerArray)
						{	
							if (currentTile.owner.playerTeam == currentPlayer.playerTeam)
							{
								currentPlayer.numOfTriangleTiles++;
								break;
							}
						}
					}
					//Diamond Tiles
					if (currentTile.state == 2)
					{
						for each (currentPlayer in playerArray)
						{
							if (currentTile.owner.playerTeam == currentPlayer.playerTeam)
							{
								currentPlayer.numOfDiamondTiles++;
								break;
							}
						}
					}
					//Star Tiles
					if ( currentTile.state == 3)
					{
						for each (currentPlayer in playerArray)
						{
							if (currentTile.owner.playerTeam == currentPlayer.playerTeam)
							{
								currentPlayer.numOfStarTiles++;
								break;
							}
						}
					}
					//Solid Tiles
					if (currentTile.state == 4)
					{
						for each (currentPlayer in playerArray)
						{
							if (currentTile.owner.playerTeam == currentPlayer.playerTeam)
							{
								currentPlayer.numOfSolidTiles++;
								break;
							}
						}
					}
					
					//How many tiles are boundary tiles on this map?
					if (currentTile.state == 5 || currentTile.state == 6 || currentTile.state == 9 || currentTile.state == 10)
					{
						boundaryTileCounter++;
					}
				}
			}
			
			//Pie Chart Stuff
			totalNumOfPlayableTiles = (gameBoardHeight * gameBoardWidth) - boundaryTileCounter;
			
			var allCellsCurrent:int = 0;
			var allCellsPrevious:int = 0;
			//Determine Player Percentage.
			for each (currentPlayer in playerArray)
			{
				currentPlayer.totalNumOfTiles = currentPlayer.numOfTriangleTiles + currentPlayer.numOfDiamondTiles + currentPlayer.numOfStarTiles + currentPlayer.numOfSolidTiles;	
				currentPlayer.playerPercent = ( currentPlayer.totalNumOfTiles / totalNumOfPlayableTiles) * 100;
				//trace("player + " + currentPlayer.teamName + " totalNumOfTiles = " + currentPlayer.totalNumOfTiles + " playerPercent = " + currentPlayer.playerPercent);
				currentPlayer.arrayOfTileCounts = [currentPlayer.numOfTriangleTiles , currentPlayer.numOfDiamondTiles , currentPlayer.numOfStarTiles , currentPlayer.numOfSolidTiles, currentPlayer.totalNumOfTiles];
				
				//Triangles
				if (currentPlayer.numOfTriangleTiles > currentPlayer.previousNumOfTriangleTiles)
					currentPlayer.totalNumOfTriangleTiles += (currentPlayer.numOfTriangleTiles - currentPlayer.previousNumOfTriangleTiles);
				//Diamonds
				if (currentPlayer.numOfDiamondTiles > currentPlayer.previousNumOfDiamondTiles)
					currentPlayer.totalNumOfDiamondTiles += (currentPlayer.numOfDiamondTiles - currentPlayer.previousNumOfDiamondTiles);
				//Stars
				if (currentPlayer.numOfStarTiles > currentPlayer.previousNumOfStarTiles)
					currentPlayer.totalNumOfStarTiles += (currentPlayer.numOfStarTiles - currentPlayer.previousNumOfStarTiles);
				//Solids
				if (currentPlayer.numOfSolidTiles > currentPlayer.previousNumOfSolidTiles)
					currentPlayer.totalNumOfSolidTiles += (currentPlayer.numOfSolidTiles - currentPlayer.previousNumOfSolidTiles);
				
				if (_moveHasBeenTaken)
				{
					// check for creating achievement
					if (currentPlayer.previousTotalNumOfTiles != currentPlayer.totalNumOfTiles)
					{
						var difference:Number = currentPlayer.totalNumOfTiles - currentPlayer.previousTotalNumOfTiles;
						
						// check for creating achievement - created cells
						if(difference > 0)
						{
							Main.achievements.updatePlayerStatistics(Achievements.CREATING, difference);;
						}
					}
					
					// check for destroying achivement
					var numCellsDestroyed:int = 0;
					for (var k:int=0; k<playerArray.length; k++)
					{
						if (playerArray[k] != currentPlayer)
						{
							var totalCells:int = playerArray[k].numOfTriangleTiles + playerArray[k].numOfDiamondTiles + playerArray[k].numOfStarTiles + playerArray[k].numOfSolidTiles;
							if (totalCells < playerArray[k].previousTotalNumOfTiles)
								numCellsDestroyed += playerArray[k].previousTotalNumOfTiles - totalCells;
						}
					}
					if(numCellsDestroyed < 0 && currentCellTypeSelected.type == 3)
					{
						Main.achievements.updatePlayerStatistics(Achievements.DESTROYING, Math.abs(numCellsDestroyed));
					}
				}
				
				allCellsCurrent += currentPlayer.totalNumOfTiles;
				allCellsPrevious += currentPlayer.previousTotalNumOfTiles;
				
				//Setting previous
				currentPlayer.previousNumOfTriangleTiles = currentPlayer.numOfTriangleTiles;
				currentPlayer.previousNumOfDiamondTiles = currentPlayer.numOfDiamondTiles;
				currentPlayer.previousNumOfStarTiles = currentPlayer.numOfStarTiles;
				currentPlayer.previousNumOfSolidTiles = currentPlayer.numOfSolidTiles;
				currentPlayer.previousTotalNumOfTiles = currentPlayer.totalNumOfTiles;
				
				if(currentPlayer.numOfSolidTiles >= Math.round(totalNumOfPlayableTiles /2))
					_domination = true;
			}
			
			// tabulate cells destroyed
			if (allCellsCurrent < allCellsPrevious)
			{
				var cellsDestroyed:int = allCellsPrevious - allCellsCurrent;
				
				if (currentCellTypeSelected.type == 3)
				{
					currentCurrentPlayer.cellsDestroyed += cellsDestroyed;
				}
			}
			
			//Update the bar every turn if its turned on.
			if (showPercentBar)
				modifyPercentBar();
			
			if(!replay)
			{
				// check for surrender
				var surrender:Boolean = false;
				var surrenderingPlayer:String = main.entityManager.whoSurrenderedGame();
				//trace("surrendering player = " + surrenderingPlayer);
				if (surrenderingPlayer != "")
				{
					surrender = true;
				}
			}
			
			//If the game has really ended, show the end screen.
			if (EndOfGame || forfiet || surrender || _domination)
			{
				if (_zoomButton)
				{
					_zoomButton.visible = false;
				}
				
				var playerPercentArray:Array = [];
				var player:OnlinePlayer;
				var j:int;
				
				for (j=0; j<playerArray.length; j++)
				{
					playerPercentArray.push(playerArray[j].playerPercent);
					//trace("pushed " + playerArray[j].playerPercent + " into array");
				}
				
				var highestPercent:Number = Math.max.apply(null, playerPercentArray);
				
				// calculate an array of winners and losers to pass to entity manager for end game tabulation
				// 1 = victory, -1 = loss, 0 = tie
				var playerIDArray:Array = main.entityManager.getPlayerArrayFromCurrentGame();
				var playerRankDataArray:Array = [];
				var playerRankData:OnlinePlayerRankData;
				var winners:int = 0;
				for (j=0; j<playerPercentArray.length; j++)
				{
					var totalCellsCreated:int = playerArray[j].totalNumOfTriangleTiles + playerArray[j].totalNumOfDiamondTiles + playerArray[j].totalNumOfStarTiles + playerArray[j].totalNumOfSolidTiles;
					
					if (surrender)
					{
						if (surrenderingPlayer == playerIDArray[j])
						{
							playerRankData = new OnlinePlayerRankData(playerIDArray[j], playerArray[j].playerNickname, totalCellsCreated, playerArray[j].cellsDestroyed, playerPercentArray[j], false, true, false);
							playerRankDataArray.push(playerRankData);
						}
						else
						{
							playerRankData = new OnlinePlayerRankData(playerIDArray[j], playerArray[j].playerNickname, totalCellsCreated, playerArray[j].cellsDestroyed, playerPercentArray[j], true, false, false);
							playerRankDataArray.push(playerRankData);
							winners++;
						}
					}
					else
					{	
						if (playerPercentArray[j] == highestPercent)
						{
							playerRankData = new OnlinePlayerRankData(playerIDArray[j], playerArray[j].playerNickname, totalCellsCreated, playerArray[j].cellsDestroyed, playerPercentArray[j], true, false, false);
							playerRankDataArray.push(playerRankData);
							winners++;
						}
						else
						{
							playerRankData = new OnlinePlayerRankData(playerIDArray[j], playerArray[j].playerNickname, totalCellsCreated, playerArray[j].cellsDestroyed, playerPercentArray[j], false, true, false);
							playerRankDataArray.push(playerRankData);
						}
					}
					//trace("added player " + playerRankData.id + " to rank data in game manager");
				}
				// if tie, set to 0
				if (winners > 1)
				{
					for (j=0; j<playerRankDataArray.length; j++)
					{
						if (playerRankDataArray[j].percentBoardOwned == highestPercent)
						{
							playerRankDataArray[j].tie = true;
							playerRankDataArray[j].win = false;
						}
					}
				}
				
				//trace("player array = " + playerArray);
				//trace("player index = " + _playerIndex);
				if (surrender)
				{
					//trace("here");
					
					if (surrenderingPlayer == Main.sOnlineID)
						addDefeatScreen(playerRankDataArray);
					else
						addVictoryScreen(playerRankDataArray);
				}
				else
				{
					if (_playerIndex == EntityManager.NOT_FOUND)
						_playerIndex = main.entityManager.whatIsMyPlayerIndexForCurrentGame();
					
					if (playerArray[_playerIndex].playerPercent >= highestPercent)
						addVictoryScreen(playerRankDataArray);
					else
						addDefeatScreen(playerRankDataArray);
				}
			}
		}
		private function addDefeatScreen(playerRankDataArray:Array):void
		{
			defeat = true;
			
			// send info to entity manager for rankings
			if(!replay)	
				main.entityManager.endGame(playerRankDataArray);
			
			if(_defeatScreen == null)
			{
				_defeatScreen = new OnlineDefeatScreen(this, playerRankDataArray);
				addChild(_defeatScreen);
				_defeatScreen.x = PathogenTest.stageCenter.x;
				_defeatScreen.y = PathogenTest.stageCenter.y;
			}
			
			// this needs to be fixed for ranked play - makes stats invisible until rankings are done
			if (main.entityManager.isCurrentGameRanked())
			{
				_defeatScreen.setViewStatsButtonVisible(false);
				addEventListener(Event.ENTER_FRAME, waitForRakingsToComplete);
			}
		}
		private function addVictoryScreen(playerRankDataArray:Array):void
		{
			success = true;

			// send info to entity manager for rankings
			if(!replay)
				main.entityManager.endGame(playerRankDataArray);
			
			if(_victoryScreen == null)
			{
				_victoryScreen = new OnlineVictoryScreen(this, playerRankDataArray);
				addChild(_victoryScreen);
				_victoryScreen.x = PathogenTest.stageCenter.x;
				_victoryScreen.y = PathogenTest.stageCenter.y;
				Main.achievements.onlineVictoryScreen = _victoryScreen;
			}
			
			// this needs to be fixed for ranked play - make stats invisible until rankings done
			if (main.entityManager.isCurrentGameRanked())
			{
				_victoryScreen.setViewStatsButtonVisible(false);
				addEventListener(Event.ENTER_FRAME, waitForRakingsToComplete);
			}
		}
		
		private function waitForRakingsToComplete(event:Event):void
		{
			if (!main.entityManager.calculatingRanks)
			{
				removeEventListener(Event.ENTER_FRAME, waitForRakingsToComplete);
				if (_victoryScreen)
				{
					_victoryScreen.setViewStatsButtonVisible(true);
				}
				if (_defeatScreen)
					_defeatScreen.setViewStatsButtonVisible(true);
			}
		}
		public function HideSelector():void
		{
			if(renderAnimations)
			{
				// scale down cell selector
				if(currentCellTypeSelected.type > 0)
					currentCellTypeSelected.scaleAndFadeWhenClicked();
			}
			
			tileSelector.visible = false;
		}
		public function addPostGameScreen():void
		{
			hideUI();
			
			if (postGame == null)
				postGame = new OnlinePostGame(this);
			
			addChild(postGame);
		}
		public function enableReplay(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				addReplayBar();
			}
		}
		private function addReplayBar():void
		{
			removeChild(postGame);
			revealUI();
			
			//reseting victory conditions
			forfiet = false;
			replay = true;
			_domination = false;
			defeat = false;
			success = false;
			Main.sAnimationsOn = true;
			_renderAnimations = true;
			
			// create play/pause button
			if(playPauseButton == null)
			{
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					playPauseButton = new BW_Button(0.7, 1.0, "", 0);
				else
					playPauseButton = new BW_Button(0.5, 0.7, "", 0);
				playPauseButton.addImage(Main.assets.getTexture("Pathogen_BTN_Play"), Main.assets.getTexture("Pathogen_BTN_Pause"));
				playPauseButton._toggle = true;
				addChild(playPauseButton)
				playPauseButton.x = PathogenTest.stageCenter.x;
				playPauseButton.y = rulesButton.y;
				playPauseButton.addEventListener(TouchEvent.TOUCH, playPauseReplay);
			}
			
			if(stepForwardButton == null)
			{
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					stepForwardButton = new BW_Button(0.7, 1.0, "", 10);
				else
					stepForwardButton = new BW_Button(0.5, 0.7, "", 10);
				stepForwardButton.addImage(Main.assets.getTexture("StepBTN_Off"), Main.assets.getTexture("StepBTN_On"));
				addChild(stepForwardButton)
				stepForwardButton.x = playPauseButton.x + (stepForwardButton.width * 1.2)
				stepForwardButton.y = rulesButton.y;
				stepForwardButton.addEventListener(TouchEvent.TOUCH, stepForward);
				stepForwardButton.rotation = deg2rad(180);
			}
			
			if(stepBackwardButton == null)
			{
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					stepBackwardButton = new BW_Button(0.7, 1.0, "", 10);
				else
					stepBackwardButton = new BW_Button(0.5, 0.7, "", 10);
				stepBackwardButton.addImage(Main.assets.getTexture("StepBTN_Off"), Main.assets.getTexture("StepBTN_On"));
				addChild(stepBackwardButton)
				stepBackwardButton.x = playPauseButton.x - (stepForwardButton.width * 1.2)
				stepBackwardButton.y = rulesButton.y;
				stepBackwardButton.addEventListener(TouchEvent.TOUCH, stepBackward);
			}
			
			replayPause = true;
			
			//Set the current moment in time.
			currentTurn = turnList.length;
		}
		private function stepForward($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && !moveUnderway && replayPause)
			{
				currentTurn++;
				resetBoardInTime();
			}
		}
		private function stepBackward($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && !moveUnderway && replayPause)
			{
				currentTurn--;
				resetBoardInTime();
			}
		}
		private function playPauseReplay($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				//If the replay is paused, begin stepping forward from the current moment in time.
				if (replayPause)
				{
					if (currentTurn == (turnList.length - 1))
						currentTurn = 0;
					
					playReplay = true;
					replayPause = false;
					resetBoardInTime();
					setTimeLine();
					
				}
				else
				{
					//Pause the replay
					replayPause = true;
					playReplay = false;
				}
			}
		}
		private function resetBoardInTime():void
		{
			if (currentTurn >= turnList.length || currentTurn < 0)
				currentTurn = 0;
			
			for ( var i:int = 0; i < gameBoard.length; i++)
			{
				var currentTile:OnlineCell = gameBoard[i];
				
				var previousTileState:int = playLog[currentTurn][i];
				var previousTileOwner:int = playLogOwnership[currentTurn][i];
				
				gameBoard[i].state = previousTileState;
				//Make sure the tiles are assigned to their correct owners
				if (previousTileOwner == 5)
				{
					gameBoard[i].owner = null;
				}
				else
				{
					var tempOwnerPlayer:OnlinePlayer = new OnlinePlayer(previousTileOwner, playerColors[previousTileOwner], this);
					
					gameBoard[i].owner = tempOwnerPlayer;
					gameBoard[i].currentPlayer = tempOwnerPlayer;
				}
				gameBoard[i].reset();
			}
			
			var player:OnlinePlayer;
			
			for each (player in playerArray) 
			{
				for each (var selector:OnlineCellSelector in player.cellSelectorArray)
				{
					selector.restoreState(currentTurn);
				}
			}
			
			//trace("there are " + turnList.length + " total turns");
			
			turn = turnList[currentTurn];
			currentPlayerNum = turn.currentPlayerNum;
			currentPlayer = playerArray[currentPlayerNum];
			currentCellTypeSelected = currentPlayer.cellSelectorArray[turn.type];
			
			// modify selector sizes and places
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				for (i=0; i<playerArray.length; i++)
				{
					playerArray[i].revertSelectors();
				}
				
				if (!currentPlayer.getAiControl())
				{
					currentPlayer.increaseSelectorSizes();
				}
			}
			
			// if iphone - adjust selector position
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				tileSelector.x = currentCellTypeSelected.x + (currentCellTypeSelected.display.x * 1.3);
			else
				tileSelector.x = currentCellTypeSelected.x;
			
			tileSelector.y = currentCellTypeSelected.y;
			
			cellSelectorHasBeenClicked(currentPlayer.cellSelectorArray[turn.type]);
			
			checkEndGame();
		}
		private function setTimeLine():void
		{
			//If at the end of known time, stop the replay.
			if (currentTurn >= turnList.length - 1)
				playReplay = false;
			
			//Cannot go back in time where there is no data
			if (turnList.length > 0)
			{
				turn = turnList[currentTurn];
				
				currentPlayerNum = turn.currentPlayerNum;
				currentPlayer = playerArray[currentPlayerNum];
				currentCellTypeSelected = new OnlineCellSelector(turn.type, currentPlayer, this);
				
				//If the players turn was skipped in the past, skip it again in the replay (only used in Lightning).
				if (turn.location == 0)
				{
					skipped = true;
					changePlayers();
				}
				else
				{
					//Otherwise determine what type of cell they placed and where it was played.
					var targetCell:OnlineCell = gameBoard[turn.location];
					
					targetCell.currentPlayer = currentPlayer;
					targetCell.selectedCellType = currentCellTypeSelected;
					targetCell.changeState();
					lastClickedCell = gameBoard[turn.location];
					
					moveUnderway = true;
				}
				
				//Change play button to play
				if (currentTurn >= (turnList.length - 1))
				{
					currentTurn++;
					replayPause = true;
					playReplay = false;
					playPauseButton.toggleUp();
				}
			}
		}
		private function hideUI():void
		{
			for each (var player:OnlinePlayer in playerArray)
			{
				player.visible = false;
			}
			
			tileSelector.visible = false;
			tileHolder.visible = false;
			optionsButton.visible = false;
			rulesButton.visible = false;
			quitButton.visible = false;
			percentBar.visible = false;
			boardOutline.visible = false;
			
			if(replay)
			{
				stepBackwardButton.visible = false;
				stepForwardButton.visible = false;
				playPauseButton.visible = false;
			}
			
			_confirmButton.visible = false;
			//_confirmButtonNew.visible = false;
			_confirmSelector.visible = false;
			
			if (_zoomButton)
				_zoomButton.x = PathogenTest.stageCenter.x;
		}
		private function revealUI():void
		{
			for each (var player:OnlinePlayer in playerArray)
			{
				player.visible = true;
			}
			
			tileSelector.visible = true;
			tileHolder.visible = true;
			optionsButton.visible = true;
			rulesButton.visible = true;
			quitButton.visible = true;
			percentBar.visible = true;
			boardOutline.visible = true;
			
			if(replay)
			{
				stepBackwardButton.visible = true;
				stepForwardButton.visible = true;
				playPauseButton.visible = true;
				_viewMap = false;
			}
			else
			{
				_viewMap = true;
			}
		}
		private function modifyPercentBar():void
		{
			//Animate the percent bar to make it "grow/shrink" to its new size, not snap.
			var onePercentOfFrame:Number = percentFrameWidth / 100;
			var playerCounter:int = 0;
			var decimalPlaces:Number = Math.pow(10, 0);
			
			for each(var player:OnlinePlayer in playerArray)
			{
				player.percentBarWidth = player.playerPercent * onePercentOfFrame;
				player.percentBarWidth = Math.round(decimalPlaces * player.percentBarWidth) / decimalPlaces;
				
				if (player.numOfSolidTiles > 0)
				{
					player.percentBarOfTierThreeWidth = ((player.numOfSolidTiles / player.totalNumOfTiles) * player.percentBar.width);
 					player.percentBarOfTierThreeWidth = Math.round(decimalPlaces * player.percentBarOfTierThreeWidth) / decimalPlaces;
					player.percentBarOfTierThree.visible = true;
				}
				else
				{
					player.percentBarOfTierThreeWidth = 0;
				}
				playerCounter++;
			}
			addEventListener(Event.ENTER_FRAME, updatePercentBar);
		}
		private function updatePercentBar(event:Event):void
		{
			var playerCounter:int = 0;
			var done:Boolean = true;
			
			if (percentBar.visible)
				modifyPercentBar();
			
			for each(var player:OnlinePlayer in playerArray)
			{
				// adjust bar widths
				if (player.percentBar.width < player.percentBarWidth)
				{
					player.percentBar.visible = true;
					player.percentBar.width += percentBarRate;
					if (player.percentBar.width >= player.percentBarWidth)
						player.percentBar.width = player.percentBarWidth;
					else
						done = false;
				}
				else if (player.percentBar.width > player.percentBarWidth)
				{
					player.percentBar.width -= percentBarRate;
					if (player.percentBar.width <= player.percentBarWidth)
						player.percentBar.width = player.percentBarWidth;
					else
						done = false;
				}
				if (player.percentBarOfTierThree.width < player.percentBarOfTierThreeWidth)
				{
					player.percentBarOfTierThree.width += percentBarRate;
					if (player.percentBarOfTierThree.width >= player.percentBarOfTierThreeWidth)
						player.percentBarOfTierThree.width = player.percentBarOfTierThreeWidth;
					else
						done = false;
				}
				else if (player.percentBarOfTierThree.width > player.percentBarOfTierThreeWidth)
				{
					player.percentBarOfTierThree.width -= percentBarRate;
					if (player.percentBarOfTierThree.width <= player.percentBarOfTierThreeWidth)
						player.percentBarOfTierThree.width = player.percentBarOfTierThreeWidth;
					else
						done = false;
				}
				
				// adjust bar locations
				if (playerCounter == 0)
				{
					player.percentBar.x = -((percentBar.width/2) - 1);
				}
				else
				{
					player.percentBar.x = playerArray[playerCounter - 1].percentBar.x + playerArray[playerCounter - 1].percentBar.width;
				}
				player.percentBarOfTierThree.x = player.percentBar.x;
				
				playerCounter++;
			}
			
			if (done)
				removeEventListener(Event.ENTER_FRAME, updatePercentBar);
		}
		private function resetTiles():void
		{
			var numOfCells:int = 0;
			
			for each (var cell:OnlineCell in gameBoard)
			{
				if(cell.hasAlreadySpilledOver)
				{
					cell.hasAlreadySpilledOver = false;
					numOfCells++;
				}
				
				cell.reactionNumber = 0;
				cell.potentialState = 0;
				cell.originalState = cell.state;
				cell.determineBranches();
			}
		}
		public function viewMap(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				removeChild(postGame);
				revealUI();
			}
		}
		public function Quit(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(main != null)
				{
					garbageCollection();
					main.removeOnlineGameManager();
				}
			}
		}
		public function modifyPlayerPercentBar(player:OnlinePlayer):void
		{
			player.totalNumOfTiles = player.numOfTriangleTiles + player.numOfDiamondTiles + player.numOfStarTiles + player.numOfSolidTiles;	
			player.playerPercent = (player.totalNumOfTiles / totalNumOfPlayableTiles) * 100;
			
			var onePercentOfFrame:Number = percentFrameWidth / 100;
			
			player.percentBarWidth = player.playerPercent * onePercentOfFrame;
			
			if (player.numOfSolidTiles > 0)
			{
				player.percentBarOfTierThreeWidth = ((player.numOfSolidTiles / player.totalNumOfTiles) * player.percentBar.width);
			}
			else
			{
				player.percentBarOfTierThreeWidth = 0;
			}
			
			//trace("modified player " + player.teamName + " to have " + player.playerPercent);
		}
		public function addToPlayerTileCount(player:OnlinePlayer, cell:OnlineCell):void
		{
			if (cell.state == 1)
				player.numOfTriangleTiles++;
			else if (cell.state == 2)
				player.numOfDiamondTiles++;
			else if (cell.state == 3)
				player.numOfStarTiles++;
			else if (cell.state == 4)
				player.numOfSolidTiles++;
			
			modifyPlayerPercentBar(player);
			
			//trace("modified " + player.teamName + " to have " + player.numOfTriangleTiles + ", " + player.numOfDiamondTiles);
		}
		
		public function subtractFromPlayerTileCount(player:OnlinePlayer, cell:OnlineCell):void
		{
			if (cell.state == 1)
				player.numOfTriangleTiles--;
			else if (cell.state == 2)
				player.numOfDiamondTiles--;
			else if (cell.state == 3)
				player.numOfStarTiles--;
			else if (cell.state == 4)
				player.numOfSolidTiles--;
			
			modifyPlayerPercentBar(player);
		}
		
		/* ONLINE FUNCTIONS */
		public function showData():void
		{
			var i:int;
			if (_currentGameIndex != -1)
				loadAndRenderBoard();
			//else
			//trace("error - found no game");
		}
		
		private function onTouchConfirm(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && isItMyTurn() && _clickedCell != null)
			{
				if (!_liveUpdating)
				{
					//Set the current player/selected cell type for the cell
					_clickedCell.currentPlayer = currentPlayer;
					_clickedCell.selectedCellType = currentCellTypeSelected;
					_clickedCell.changeState();
					_clickedCell.scaleX = cellScale - .05;
					_clickedCell.scaleY = cellScale - .05;
					lastClickedCell = _clickedCell;
					moveUnderway = true;
					_clickedCell = null;
					_confirmButton.visible = false;
					//_confirmButtonNew.visible = false;
					_confirmSelector.visible = false;
					_locationSelector.visible = false;
					
					if (_zoomButton)
						_zoomButton.x = PathogenTest.stageCenter.x;
				}
				else
				{
					_waitingForLiveUpdateToFinishToTakeTurn = true;
					addEventListener(Event.ENTER_FRAME, waitForLiveUpdateToFinishToTakeTurn);
					_confirmButton.removeEventListener(TouchEvent.TOUCH, onTouchConfirm);
					//_confirmButtonNew.removeEventListener(TouchEvent.TOUCH, onTouchConfirm);
				}
			}
		}
		
		private function waitForLiveUpdateToFinishToTakeTurn(event:Event):void
		{
			if (!_liveUpdating)
			{
				removeEventListener(Event.ENTER_FRAME, waitForLiveUpdateToFinishToTakeTurn);
				
				//Set the current player/selected cell type for the cell
				_clickedCell.currentPlayer = currentPlayer;
				_clickedCell.selectedCellType = currentCellTypeSelected;
				_clickedCell.changeState();
				_clickedCell.scaleX = cellScale - .05;
				_clickedCell.scaleY = cellScale - .05;
				lastClickedCell = _clickedCell;
				moveUnderway = true;
				_clickedCell = null;
				_confirmButton.visible = false;
				//_confirmButtonNew.visible = false;
				_confirmSelector.visible = false;
				_locationSelector.visible = false;
				
				if (_zoomButton)
					_zoomButton.x = PathogenTest.stageCenter.x;
				
				_confirmButton.addEventListener(TouchEvent.TOUCH, onTouchConfirm);
				//_confirmButtonNew.addEventListener(TouchEvent.TOUCH, onTouchConfirm);
				_waitingForLiveUpdateToFinishToTakeTurn = false;
			}
		}
		
		public function isItMyTurn():Boolean
		{
			if(!replay)
			{
				if (_playerIndex == currentPlayerNum)
					return true;
				else return false;
				/*
				if (main.entityManager.getCurrentPlayerForCurrentGame() == main.entityManager.getMyPlayerID())
				{
				Main.debugText.text = "it is my turn";
				return true;
				}
				else
				{
				Main.debugText.text = "it is not my turn";
				return false;
				}
				*/
			}
			else
				return true;
		}
		
		public function renderBoard():void
		{
			for (var i:int=0; i<gameBoard.length; i++)
			{				
				var cell:OnlineCell = gameBoard[i];
				cell.setColor();
			}
		}
		private function loadGameRecordData():void
		{
			_renderAnimations = false;
			
			var turnLocations:Array = main.entityManager.getTurnLocationsForCurrentGame();
			var turnCellTypes:Array = main.entityManager.getTurnCellTypesForCurrentGame();
			var turnSurrenders:Array = main.entityManager.getTurnSurrendersForCurrentGame();
			
			for (var i:int=0; i<turnLocations.length; i++)
			{
				var cell:OnlineCell = gameBoard[turnLocations[i]];
				currentCellTypeSelected = currentPlayer.cellSelectorArray[turnCellTypes[i]];
				cell.selectedCellType = currentCellTypeSelected;
				cell.currentPlayer = currentPlayer;
				cell.changeState();
				lastClickedCell = cell;
				//trace("loc: " + turnLocations[i] + " type: " + turnCellTypes[i]);
				changePlayers();
			}
			_renderAnimations = true;
		}
		
		private function loadAndRenderBoard():void
		{
			setChildIndex(_onlineGameLoadingText, (numChildren - 1));
			
			_onlineGameLoading = true;
			_onlineGameLoadingText.visible = true;
			
			loadGameRecordData();
			renderBoard();
			
			_onlineGameLoading = false;
			_onlineGameLoadingText.visible = false;
		}
		private function liveUpdate():void
		{
			if (!isItMyTurn() && !moveUnderway)
				main.entityManager.updateCurrentGameForTurns();
			else
			{
				// trace("running live update");
				main.entityManager.updateCurrentGameNotForTurns();
			}
		}
		public function updateTurns(numOfTurns:int):void
		{
			if (numOfTurns > 0)
			{
				if (numOfTurns == 1)
				{
					_renderAnimations = true;
					_playingOneMove = true;
				}
				else
					_renderAnimations = false;
				
				var turnLocations:Array = main.entityManager.getSubsetOfTurnLocationsForCurrentGame(numOfTurns);
				var turnCellTypes:Array = main.entityManager.getSubsetOfTurnCellTypesForCurrentGame(numOfTurns);
				var turnSurrenders:Array = main.entityManager.getSubsetOfTurnSurrendersForCurrentGame(numOfTurns);
				
				//trace("turnLocations length = " + turnLocations.length);
				
				for (var i:int=0; i<turnLocations.length; i++)
				{
					//trace("playing 1 turn");
					
					var cell:OnlineCell = gameBoard[turnLocations[i]];
					currentCellTypeSelected = currentPlayer.cellSelectorArray[turnCellTypes[i]];
					cell.selectedCellType = currentCellTypeSelected;
					cell.currentPlayer = currentPlayer;
					cell.changeState();
					lastClickedCell = cell;
					if (_renderAnimations)
						moveUnderway = true;
					else
						changePlayers();
				}
			}
			if (!_playingOneMove)
			{
				_renderAnimations = true;
				_liveUpdating = false;
			}
		}
		
		public function refreshPlayerText():void
		{
			var playerNicknames:Array = main.entityManager.getCurrentGamePlayerNicknames();
			
			for (var i:int=0; i<playerArray.length; i++)
			{
				if (playerNicknames[i])
					playerArray[i].setPlayerNicknameAndText(playerNicknames[i]); 
			}
		}
		
		public function otherPlayerSurrendered(playerIndex:int):void
		{
			main.entityManager.refreshPlayerEntity();
			
			_otherPlayerSurrendered = new OtherPlayerSurrendered(playerArray[playerIndex].playerNickname, this);
			addChild(_otherPlayerSurrendered);
		}
		
		public function removeOtherPlayerSurrendered():void
		{
			if (_otherPlayerSurrendered)
			{
				_otherPlayerSurrendered.dispose();
				removeChild(_otherPlayerSurrendered);
				_otherPlayerSurrendered = null;
			}
			
			checkEndGame();
		}
		public function addTipWindow(cell:OnlineCell):void
		{
			var globalLocation:Point = cell.localToGlobal(new Point(0,0));
			
			_tipWindow = new TipWindow(null, currentCellTypeSelected.type, cell.state, this);
			addChild(_tipWindow);
			_tipWindow.x = globalLocation.x;
			_tipWindow.y = globalLocation.y;
		}
		public function removeTipWindow():void
		{
			if(_tipWindow)
			{
				_tipWindow.dispose();
				removeChild(_tipWindow);
				_tipWindow = null;
			}
		}
		
		public function addInternetConnectivityError():void
		{
			if (!_internetConnectivityError)
			{
				_internetConnectivityError = new InternetConnectivityError(this, null);
				addChild(_internetConnectivityError);
				_internetConnectivityError.x = 0;
				_internetConnectivityError.y = 0;
			}
			else
			{
				_internetConnectivityError.visible = true;
			}
		}
		
		public function removeInternetConnectivityError():void
		{
			if (_internetConnectivityError)
			{
				_internetConnectivityError.dispose();
				removeChild(_internetConnectivityError);
				_internetConnectivityError = null;
				
				garbageCollection();
				
				if (main)
				{
					main.recheckInternetConnectivity();
					main.removeOnlineGameManager();
				}
			}
		}
		
		public function areAllPlayersJoined():Boolean
		{
			return main.entityManager.haveAllPlayersJoinedCurrentGame();
		}
		
		public function isGameOver():Boolean
		{
			var status:int = main.entityManager.getcurrentGameStatus();
			if (status == EntityManager.GAME_STATUS_COMPLETE || status == EntityManager.GAME_STATUS_REJECTED)
				return true;
			return false;
		}
		
		public function removeVictoryScreen():void	
		{
			if(_victoryScreen)
			{
				_victoryScreen.dispose();
				removeChild(_victoryScreen);
				_victoryScreen = null;
				Main.achievements.onlineVictoryScreen = null;
			}
		}
		
		public function removeDefeatScreen():void	
		{
			if(_defeatScreen)
			{
				_defeatScreen.dispose();
				removeChild(_defeatScreen);
				_defeatScreen = null;
			}
		}
		
		public function isSurrendered():Boolean
		{
			var surrender:Boolean = false;
			var surrenderingPlayer:String = main.entityManager.whoSurrenderedGame();
			if (surrenderingPlayer != "")
			{
				surrender = true;
			}
			return surrender;
		}
		
		public function getSurrenderedPlayer():String
		{
			return main.entityManager.whoSurrenderedGame();
		}
		
		public function setVictoryScreenChangeInScore(changeInScore:int):void
		{
			if (_victoryScreen)
				_victoryScreen.setChangeInScoreText(changeInScore);
		}
		public function addAchievementPopUp(achievementNumber:int):void
		{
			var achievementPopUp:AchievementPopUp = new AchievementPopUp();
			addChild(achievementPopUp);
			achievementPopUp.init(achievementNumber);
			achievementPopUp.x = PathogenTest.stageCenter.x;
			achievementPopUp.pivotX = achievementPopUp.width/2;
			achievementPopUp.y = PathogenTest.stageHeight;
			achievementPopUp.onlineGameManager = this;
			achievementPopUp.start();
		}
		public function removeAchievementPopUp(achivementPopUp:AchievementPopUp):void
		{
			achivementPopUp.dispose();
			removeChild(achivementPopUp);
			achivementPopUp = null;
		}
		public function garbageCollection():void
		{
			//Main.updateDebugText("Garbage Collection");
			
			for each (var player:OnlinePlayer in playerArray)
			{				
				player.garbageCollection();
				player.dispose();
				removeChild(player);
				player = null;
			}
			
			playerArray = [];
			currentCellTypeSelected = null;
			currentPlayer = null;
			
			if (postGame)
			{
				postGame.garbageCollection();
				postGame.dispose();
				postGame = null;
			}
			
			//Removes all online cells
			for (var i:int=(gameBoard.length - 1); i >= 0; i--)
			{
				gameBoard[i].CleanCell();
				gameBoard[i].dispose();
				gameBoard[i] = null;
			}
			
			if (tileHolder)
			{
				tileHolder.dispose();
				removeChild(tileHolder);
				tileHolder.removeEventListener(TouchEvent.TOUCH, onTouchLocation);
				tileHolder = null;
			}
			
			gameBoard = null;
			
			removeChild(tileHolder);
			tileHolder = null;
			
			optionsButton.removeEventListener(TouchEvent.TOUCH, addOptions);
			optionsButton.dispose();
			removeChild(optionsButton);
			optionsButton = null;
			
			optionsScreen = null;
			
			quitButton.removeEventListener(TouchEvent.TOUCH, addQuitScreen);
			quitButton.dispose();
			removeChild(quitButton);
			quitButton = null;
			
			if (_zoomButton)
			{
				_zoomButton.removeEventListener(TouchEvent.TOUCH, onTouchZoom);
				_zoomButton.dispose();
				removeChild(_zoomButton);
				_zoomButton = null;
			}
			
			if (quitScreen)
			{
				quitScreen.dispose();
				removeChild(quitScreen);
				quitScreen = null;
			}
			
			if(boardOutline)
			{
				boardOutline.dispose();
				removeChild(boardOutline);
				boardOutline = null;
			}
			
			for each (turn in turnList)
			{
				turn.dispose();
				turn = null;
			}
			
			turnList = null;
			
			if(replay)
			{
				playPauseButton.dispose();
				playPauseButton.destroy();
				removeChild(playPauseButton);
				playPauseButton = null;
				
				stepBackwardButton.destroy();
				stepBackwardButton.dispose();
				removeChild(stepBackwardButton);
				stepBackwardButton = null;
				
				stepForwardButton.destroy();
				stepForwardButton.dispose();
				removeChild(stepForwardButton);
				stepForwardButton = null;
			}
			
			percentBar.dispose();
			removeChild(percentBar);
			percentBar = null;
			
			if(_victoryScreen)
				removeVictoryScreen();
			
			if(_defeatScreen)
				removeDefeatScreen();
			
			removeEventListener(Event.ENTER_FRAME, update);
			
			if (tileSelector)
			{
				tileSelector.destroy();
				tileSelector.dispose();
				removeChild(tileSelector);
				tileSelector = null;
			}
			
			if(_locationSelector)
			{
				_locationSelector.destroy();
				_locationSelector.dispose();
				removeChild(_locationSelector);
				_locationSelector = null;
			}
			
			if(_confirmButton)
			{
				_confirmButton.destroy();
				_confirmButton.dispose();
				removeChild(_confirmButton);
				_confirmButton = null;
				
				_confirmSelector.destroy();
				_confirmSelector.dispose();
				removeChild(_confirmSelector);
				_confirmSelector = null;
			}
			
			if(_confirmButtonNew)
			{
				_confirmButtonNew.dispose();
				removeChild(_confirmButtonNew);
				_confirmButtonNew = null;
				
				_confirmSelector.destroy();
				_confirmSelector.dispose();
				removeChild(_confirmSelector);
				_confirmSelector = null;
			}
		}
		
		// getters & setters
		public function get renderAnimations():Boolean { return _renderAnimations; }
		public function get onlineGameLoading():Boolean { return _onlineGameLoading; }
		public function getCurrentPlayerNum():int { return currentPlayerNum; }
		public function getPlayReplay():Boolean { return playReplay; }
		public function getCurrentCellTypeSelected():OnlineCellSelector { return currentCellTypeSelected; }
		public function getMain():Main { return main; }
		public function get mapSize():int {return _mapSize};
		public function get domination():Boolean { return _domination; }
		
		public function setCurrentCellTypeSelected(inSelector:OnlineCellSelector):void { currentCellTypeSelected = inSelector; }
		public function set liveUpdating(liveUpdating:Boolean):void { _liveUpdating = liveUpdating; }
	}
}