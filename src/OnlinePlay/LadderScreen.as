package OnlinePlay
{
	import FloxConnect.EntityManager;
	import FloxConnect.PlayerEntity;
	
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_Window;
	
	import feathers.controls.Header;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.layout.VerticalLayout;
	
	import flashx.textLayout.elements.OverflowPolicy;
	
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class LadderScreen extends BW_Window
	{
		//Parent Class
		private var _onlinePlay:OnlinePlay;
		
		//Buttons
		private var _quitButton:BW_Button;
		
		//Display
		private var _container:ScrollContainer;
		private var _currentLeague:String = "Ladder";
		private var _leagueIconString:String;
		
		private var player:PlayerEntity
		
		public function LadderScreen(onlinePlay:OnlinePlay)
		{
			super();
			
			_onlinePlay = onlinePlay;
			
			determineLeague();
			
			createbackground(8,8);
			createHeader(_currentLeague);
			
			init();
		}
		private function determineLeague():void
		{
			player = _onlinePlay.main.entityManager.currentPlayerEntity;
			
			var playerRank:int = player.ranks[0]
			var leagueIconString:String = "UserIcon";
			
			switch(_onlinePlay.main.entityManager.getLeagueName(player.league2Player))
			{
				case "Bronze":
					_currentLeague = "Bronze League";
					
					//Determine which Badge to show
					if(playerRank <= 10)
						_leagueIconString = "Bronze03";
					else if (playerRank > 10 && playerRank <= 25)
						_leagueIconString = "Bronze02";
					else
						_leagueIconString = "Bronze01";
					
					break;
				
				case "Silver":
					_currentLeague = "Silver League";
					
					//Determine which Badge to show
					if(playerRank <= 10)
						_leagueIconString = "Silver03";
					else if (playerRank > 10 && playerRank <= 25)
						_leagueIconString = "Silver02";
					else
						_leagueIconString = "Silver01";
					
					break;
				
				case "Gold":
					_currentLeague = "Gold League";
					
					//Determine which Badge to show
					if(playerRank <= 10)
						_leagueIconString = "Gold03";
					else if (playerRank > 10 && playerRank <= 25)
						_leagueIconString = "Gold02";
					else
						_leagueIconString = "Gold01";
					
					break;
				
				default:
					//Player has not yet ranked!
					break;
			}
		}
		private function init():void
		{
			// create Quit button
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_quitButton = new BW_Button(.75,1, "", 0);
			else
				_quitButton = new BW_Button(.5,.75, "", 0);
			
			_quitButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(_quitButton);
			_quitButton.x =  _background.x - _background.width/2 + (_quitButton.width * .75);
			_quitButton.y = _background.y + _background.height/2 - (_quitButton.width * .75);
			_quitButton.addEventListener(TouchEvent.TOUCH, onTouchQuit);
			
			//Create Scroll Container
			var layout:VerticalLayout = new VerticalLayout();
			
			_container = new ScrollContainer();
			_container.layout = layout;
			_container.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_container.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			_container.scrollerProperties.snapScrollPositionsToPixels = true;
			addChild(_container);
			
			_container.width = _background.width;
			_container.height = _background.height - (PathogenTest.hTenPercent * 3);
			_container.pivotX = _container.width/2;
			_container.pivotY = _container.height/2;
			_container.x = PathogenTest.stageCenter.x;
			_container.y = PathogenTest.stageCenter.y;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor;
			
			//Ladder Column Headers
			var rankTitle:TextField = new TextField(PathogenTest.hTenPercent, PathogenTest.hTenPercent * .75, "Rank", "Dekar", fontSize, Color.WHITE);
			addChild(rankTitle);
			rankTitle.y = _header.y + _header.height;
			rankTitle.x = _background.x - PathogenTest.wTenPercent * 3.85;
			
			var playerTitle:TextField = new TextField(PathogenTest.hTenPercent, PathogenTest.hTenPercent * .75, "Name", "Dekar", fontSize, Color.WHITE);
			addChild(playerTitle);
			playerTitle.y = rankTitle.y;
			playerTitle.x = _background.x - PathogenTest.wTenPercent * 2;
			
			var pointsTitle:TextField = new TextField(PathogenTest.hTenPercent, PathogenTest.hTenPercent * .75, "Points", "Dekar", fontSize, Color.WHITE);
			addChild(pointsTitle);
			pointsTitle.y = rankTitle.y;
			pointsTitle.x = _background.x + PathogenTest.wTenPercent * 1.5;
			
			var winsTitle:TextField = new TextField(PathogenTest.hTenPercent, PathogenTest.hTenPercent * .75, "Wins", "Dekar", fontSize, Color.WHITE);
			addChild(winsTitle);
			winsTitle.y = rankTitle.y;
			winsTitle.x = _background.x + PathogenTest.wTenPercent * 2.25;
			
			var lossesTitle:TextField = new TextField(PathogenTest.hTenPercent, PathogenTest.hTenPercent * .75, "Losses", "Dekar", fontSize, Color.WHITE);
			addChild(lossesTitle);
			lossesTitle.y = rankTitle.y;
			lossesTitle.x = _background.x + PathogenTest.wTenPercent * 3;
			
			var currentPlayer:PlayerEntity;
			var playerRank:int;
			var playerName:String;
			var lastRank:int;
			var state:int;
			var pointValue:int;
			var wins:int;
			var losses:int;
			
			if(_currentLeague != "Ladder")
			{
				var leagueIcon:Image = new Image(Main.assets.getTexture(_leagueIconString));
				leagueIcon.pivotY = leagueIcon.height/2;
				leagueIcon.pivotX = leagueIcon.width / 2;
				addChild(leagueIcon);
				leagueIcon.x = _header.x -_header.width/2 + (leagueIcon.width * .35);
				leagueIcon.y = _header.y + _header.height/2;
				leagueIcon.scaleX = .4;
				leagueIcon.scaleY = .4;
			}
			
			var ladderHeader_01:LadderHeader = new LadderHeader(1, _currentLeague);
			_container.addChild(ladderHeader_01);
			
			//Create Ladder Display
			for (var i:int = 0; i < _onlinePlay.main.entityManager.currentLadder.length; i++)
			{
				currentPlayer = _onlinePlay.main.entityManager.currentLadder[i];
				playerRank = currentPlayer.ranks[EntityManager.GAME_TYPE_2_PLAYER];
				
				// only display ranks 0 - 50
				if (playerRank >= 0 && playerRank < EntityManager.DIVISION_MAX)
				{
					lastRank = currentPlayer.lastRanks[EntityManager.GAME_TYPE_2_PLAYER]
					playerName = currentPlayer.nickname;
					pointValue = currentPlayer.scores[EntityManager.GAME_TYPE_2_PLAYER]
					wins = currentPlayer.wins[EntityManager.GAME_TYPE_2_PLAYER];
					losses = currentPlayer.losses[EntityManager.GAME_TYPE_2_PLAYER];
					
					//Player just joined a league.
					if(lastRank == -1)
						state = -1;
					else
					{
						if(lastRank > playerRank)
							state = 1; //Moving up
						else
							state = 0; //Moving down
					}
					
					var ladderDisplay:LadderDisplay = new LadderDisplay(playerRank, playerName, state, pointValue, wins, losses);
					_container.addChild(ladderDisplay);
					
					if(i == 9)
					{
						var ladderHeader_02:LadderHeader = new LadderHeader(2, _currentLeague);
						_container.addChild(ladderHeader_02);
					}
					
					if(i == 24)
					{
						var ladderHeader_03:LadderHeader = new LadderHeader(3, _currentLeague);
						_container.addChild(ladderHeader_03);
					}
				}	
			}
		}
		private function onTouchQuit(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this,TouchPhase.ENDED);
			if (touch)
			{
				_quitButton.removeEventListener(TouchEvent.TOUCH, onTouchQuit);
				startFadeOut();
			}
		}
		public override function death():void
		{
			if(_quitButton)
			{
				_quitButton.destroy();
				_quitButton.dispose();
				removeChild(_quitButton);
				_quitButton = null;
			}
			
			_onlinePlay.removeLadderScreen();
			_onlinePlay = null;
		}
	}
}