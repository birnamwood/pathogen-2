package OnlinePlay
{
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.deg2rad;
	
	import UI.Main;
	
	public class LadderHeader extends Sprite
	{
		public function LadderHeader(type:int, ladder:String)
		{
			super();
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 32 / PathogenTest.scaleFactor;
			
			var textX:int = PathogenTest.wTenPercent * 8;
			if (textX > Main.TEXTURE_LIMIT)
				textX = Main.TEXTURE_LIMIT;
			var background:Quad = new Quad(textX, PathogenTest.hTenPercent * .75, 0x000000);
			addChild(background);
			background.alpha = .75;
			
			var text:TextField = new TextField(PathogenTest.wTenPercent * 2, background.height, "", "Questrial", fontSize, Color.WHITE);
			addChild(text);
			text.pivotX = text.width/2;
			text.x = background.width/2;
			
			var leagueIconString:String;
			
			switch(type)
			{
				case 1:
					//background.color = 0xFF6600;
					text.text = "Top 10";
					
					switch(ladder)
					{
						case "Gold League":
							leagueIconString = "Gold03";
							break;
						case "Silver League":
							leagueIconString = "Silver03";
							break;
						case "Bronze League":
							leagueIconString = "Bronze03";
							break;
					}
					break;
				case 2:
					text.text = "Top 25";
					
					switch(ladder)
					{
						case "Gold League":
							leagueIconString = "Gold02";
							break;
						case "Silver League":
							leagueIconString = "Silver02";
							break;
						case "Bronze League":
							leagueIconString = "Bronze02";
							break;
					}
					
					break;
				case 3:
					text.text = "Top 50";
					
					switch(ladder)
					{
						case "Gold League":
							leagueIconString = "Gold01";
							break;
						case "Silver League":
							leagueIconString = "Silver01";
							break;
						case "Bronze League":
							leagueIconString = "Bronze01";
							break;
					}
					break;
			}
			
			var leagueIcon:Image = new Image(Main.assets.getTexture(leagueIconString));
			leagueIcon.pivotY = leagueIcon.height/2;
			leagueIcon.pivotX = leagueIcon.width/2;
			addChild(leagueIcon);
			leagueIcon.scaleX = .3;
			leagueIcon.scaleY = .3;
			leagueIcon.x = PathogenTest.wTenPercent * .5;
			leagueIcon.y = leagueIcon.height * .75;
		}
	}
}