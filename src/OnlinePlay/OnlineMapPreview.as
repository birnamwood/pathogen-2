package OnlinePlay
{
	import Maps.Map;
	import Maps.MapPreviewCell;
	
	import UI.Main;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class OnlineMapPreview extends Sprite
	{
		private var _onlineGameSetup:OnlineGameSetup;
		private var _map:Map;
		private var previewHolder:Sprite;
		private var previewCellHolder:Array = [];
		private var cellHolder:Sprite;
		
		public function OnlineMapPreview(map:Map, onlineGameSetup:OnlineGameSetup)
		{
			super();
			
			_map = map;
			_onlineGameSetup = onlineGameSetup;
			
			previewHolder = new Sprite();
			previewHolder.pivotX = previewHolder.width/2;
			previewHolder.pivotY = previewHolder.height/2;
			addChild(previewHolder);
			
			const blacktexture:Texture = Main.assets.getTexture("scale9_black");
			const blacktextures:Scale9Textures = new Scale9Textures(blacktexture, new Rectangle(20, 20, 20, 20));
			
			var background:Scale9Image = new Scale9Image(blacktextures);
			previewHolder.addChild(background);
			background.width = PathogenTest.wTenPercent * 5.5;
			background.height = PathogenTest.hTenPercent * 7;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			
			const texture:Texture = Main.assets.getTexture("scale9");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20, 20, 20, 20));
			
			var header:Scale9Image = new Scale9Image(textures);
			previewHolder.addChild(header);
			header.width = background.width;
			header.height = PathogenTest.hTenPercent;
			header.pivotX = header.width/2;
			header.y = -background.height/2;
			header.alpha = .5;
			
			var fontSize:int = 40 / PathogenTest.scaleFactor;
			var textField:TextField = new TextField(background.width, header.height, _map.name, "Questrial", fontSize, Color.WHITE);
			textField.pivotX = textField.width/2;
			textField.y = -(background.height/2);
			previewHolder.addChild(textField);
			
			createPreview();
			
			if(_onlineGameSetup)
			{
				alpha = 0;
				addEventListener(Event.ENTER_FRAME, fadeIn);
				addEventListener(TouchEvent.TOUCH, onTouch);
			}
		}
		private function fadeIn(event:Event):void
		{
			alpha += Main.FADE_SPEED;
			
			if(alpha >= 1)
			{
				removeEventListener(Event.ENTER_FRAME, fadeIn);
			}
		}
		public function onTouch(event:TouchEvent):void
		{
			if(_onlineGameSetup.sliding == false)
			{
				var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
				if (touch)
					_onlineGameSetup.selectMap(this);
			}
		}
		public function removeTouchEvents():void
		{
			removeEventListener(Event.ENTER_FRAME, fadeIn);
			removeEventListener(TouchEvent.TOUCH, onTouch);
		}
		public function createPreview():void
		{
			var tileCounter:int = 0;
			var gameBoardWidth:int;
			var gameBoardHeight:int;
			var startingPos:Point = new Point(0,0);
			var tileGap:int;
			var tileScaleX:Number;
			var tileScaleY:Number;
			
			//Determine Board Length/Height
			if (_map.size == 0)
			{
				gameBoardHeight = 14;
				gameBoardWidth = 16;
				tileScaleX = .7;
				tileScaleY = .7;
			}
			if (_map.size == 1)
			{
				gameBoardHeight = 18;
				gameBoardWidth = 20;
				tileScaleX = .55;
				tileScaleY = .55;
			}
			if (_map.size == 2)
			{
				gameBoardHeight = 22;
				gameBoardWidth = 24;
				tileScaleX = .45;
				tileScaleY = .45;
			}
			
			cellHolder = new Sprite();
			previewHolder.addChild(cellHolder);
			
			//Create a blank board
			for (var i:int = 0; i < gameBoardWidth; i++)
			{
				for (var j:int = 0; j < gameBoardHeight; j++)
				{
					var previewCell:MapPreviewCell = new MapPreviewCell();
					cellHolder.addChild(previewCell);
					previewCellHolder.push(previewCell);
					
					previewCell.changeColor(_map.layout[tileCounter])
					
					tileGap = (previewCell.width * .8) * tileScaleX;
					
					previewCell.x = startingPos.x + (tileGap * i);
					previewCell.y = startingPos.y + (tileGap * j);
					
					previewCell.scaleX = tileScaleX;
					previewCell.scaleY = tileScaleY;
					
					tileCounter++;
				}
			}
			
			cellHolder.pivotX = cellHolder.width/2;
			cellHolder.pivotY = cellHolder.height/2;
			
			cellHolder.x =  -(PathogenTest.wTenPercent * .15);
			cellHolder.y = PathogenTest.hTenPercent * .2;
			
			previewHolder.flatten();
		}
		
		public function garbageCollection():void
		{
			var currentNum:int = 0;
			
			for each(var previewCell:MapPreviewCell in previewCellHolder)
			{
				previewCell.dispose();
				cellHolder.removeChildAt(currentNum)
				previewCell = null;
				
				currentNum++;
			}

			previewCellHolder = [];
			
			cellHolder.dispose();
			removeChild(cellHolder);
			cellHolder = null;
			
			previewHolder.dispose();
			removeChild(previewHolder);
			previewHolder = null;
			
			_onlineGameSetup = null;
		}
		
		public function get map():Map { return _map; }
	}
}