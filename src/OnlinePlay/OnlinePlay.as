package OnlinePlay
{
	import Editor.SaveSlot;
	
	import FloxConnect.EntityManager;
	import FloxConnect.GameEntity;
	import FloxConnect.InviteEntity;
	
	import Maps.Map;
	import Maps.MapManager;
	
	import UI.Loader;
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import com.sticksports.nativeExtensions.flurry.Flurry;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.events.FeathersEventType;
	import feathers.layout.HorizontalLayout;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.HAlign;
	
	public class OnlinePlay extends BW_UI
	{
		private const REFRESH_TIME:int = 600;
		private const LOAD_TIMEOUT:int = 600;
		
		private var _main:Main;
		
		//Custom Screen
		private var _slider:ScrollContainer;
		private var _newGameButton:BW_Button;
		private var _randomGameButton:BW_Button;
		private var _selectorWidth:int;
		private var _selectorHeight:int;
		private var _sliding:Boolean;
		private var _invitedOnlineGamePreviews:Vector.<OnlineGamePreview>;
		private var _activeOnlineGamePreviews:Vector.<OnlineGamePreview>;
		private var _onlineGameSetup:OnlineGameSetup;
		private var _startGame:Boolean;
		private var _ranked:Boolean;
		
		// ranked screen
		private var _rankedSlider:ScrollContainer;
		private var _startRankedGameButton:BW_Button;
		private var _rankedSliding:Boolean;
		private var _activeRankedOnlineGamePreviews:Vector.<OnlineGamePreview>;
		private var _ladderButton:BW_Button;
		private var _ladderScreen:LadderScreen;
		
		//Tabs
		private var _backButton:BW_Button;
		private var _profileButton:BW_Button;
		private var _customButton:BW_Button;
		private var _rankedButton:BW_Button;
		
		private var _tab:Image;
		
		//Profile Screen
		private var _profileScreen:ProfileScreen;
		
		private var _invitesGathered:Boolean;
		private var _invitesFailed:Boolean;
		
		private var _gamesGathered:Boolean;
		private var _gamesFailed:Boolean;
		
		private var _rankedComingSoonScreen:RankedComingSoon;
		private var _onlineMaxSlotWindow:OnlineMaxSlotWindow;
		
		private var _UIHolder:Sprite;
		
		private var _openingTab:String;
		
		private var _internetConnectivityError:InternetConnectivityError;
		
		//Loader
		private var _loader:Loader;
		
		private var _refreshTimer:int;
		private var _refreshing:Boolean;
		
		private var _loadTimer:int;
		
		private var _ladderWaitTimer:int;
		
		private var _joinLeagueWindow:YouMustJoinLeagueWindow;
		
		public function OnlinePlay(main:Main, tab:String = "Profile")
		{
			super();
			
			_main = main;
			_openingTab = tab;
			_main.lastOnlinePlayTab = _openingTab;
			
			_startGame = false;
			_invitesGathered = false;
			_invitesFailed = false;
			_gamesGathered = false;
			_gamesFailed = false;
			_ranked = false;
			
			// display Loader
			_loader = new Loader("Loading Online Data", 38);
			addChild(_loader);
			_loader.x = PathogenTest.stageCenter.x;
			_loader.y = PathogenTest.stageCenter.y;
			
			_main.entityManager.gatherGamesFromPlayerID();
			_loadTimer = 0;
			addEventListener(Event.ENTER_FRAME, checkForGatherdGamesPartI);
			
			if(Flurry.isSupported)
				Flurry.startTimedEvent("Online");
			
			Main.saveDataObject.data.GameLength = 100;
			Main.saveDataObject.flush();
			
			Main.saveDataObject.data.TurnLength = 30;
			Main.saveDataObject.flush();
			
			if (!_main.loadedOnlinePlay)
			{
				_main.loadedOnlinePlay = true;
				_main.reloadGameCenterFriends();
			}
			
			_main.entityManager.refreshPlayerEntity();
		}
		
		private function checkForGatherdGamesPartI(event:Event):void
		{
			_loadTimer++;
			if (_loadTimer > LOAD_TIMEOUT)
			{
				startFadeOut();
			}
			else
			{		
				if (_gamesGathered)
				{
					if (_invitesGathered || _invitesFailed)
					{
						_loader.startFadeOut();
						createScreen();
						removeEventListener(Event.ENTER_FRAME, checkForGatherdGamesPartI);
					}
				}
				else if (_gamesFailed)
				{
					startFadeOut();
				}
			}
		}
		
		private function createScreen():void
		{
			if (!_UIHolder)
			{
				_UIHolder = new Sprite();
				addChild(_UIHolder);
				
				var bottomUIBand:Quad;
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					bottomUIBand = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1.8, Color.BLACK);
				else
					bottomUIBand = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1.5, Color.BLACK);
				bottomUIBand.pivotX = bottomUIBand.width/2;
				bottomUIBand.pivotY = bottomUIBand.height;
				_UIHolder.addChild(bottomUIBand);
				bottomUIBand.x = PathogenTest.stageCenter.x;
				bottomUIBand.y = PathogenTest.stageHeight;
				bottomUIBand.alpha = .5;
				
				/*
				var topUIBand:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1, Color.BLACK);
				topUIBand.pivotX = topUIBand.width/2;
				_UIHolder.addChild(topUIBand);
				topUIBand.x = PathogenTest.stageCenter.x;
				topUIBand.alpha = .5;
				
				//Title Text
				var username:TextField = new TextField(PathogenTest.wTenPercent * 4.5, PathogenTest.hTenPercent * .6, Main.sOnlineNickname, "Questrial", PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor, Color.WHITE);
				_UIHolder.addChild(username);
				username.hAlign = HAlign.LEFT;
				username.x = PathogenTest.wTenPercent * .1;
				username.y = PathogenTest.hTenPercent * .2;
				*/
			}

			var buttonHeight:Number;
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				buttonHeight = 1.0;
			else
				buttonHeight = 0.75;
			if (!_profileButton)
			{
				//Profile button
				_profileButton = new BW_Button(2, buttonHeight, "Profile", 36);
				_profileButton.x = PathogenTest.wTenPercent * 5;
				//_profileButton.y = PathogenTest.stageHeight - (PathogenTest.hTenPercent * .8);
				_profileButton.y = bottomUIBand.y - (bottomUIBand.height / 2);
				_UIHolder.addChild(_profileButton);
				_profileButton.addEventListener(TouchEvent.TOUCH, openProfileTab);
			}
			
			if (!_customButton)
			{
				// custom play button
				_customButton = new BW_Button(2, buttonHeight, "Custom Play", 36);
				_customButton.x = PathogenTest.wTenPercent * 2.5;
				_customButton.y = _profileButton.y;
				_UIHolder.addChild(_customButton);
				_customButton.addEventListener(TouchEvent.TOUCH, openCustomTab);
			}
			
			if (!_rankedButton)
			{
				// ranked play button
				_rankedButton = new BW_Button(2, buttonHeight, "Ranked Play", 36);
				_rankedButton.x = PathogenTest.wTenPercent * 7.5;
				_rankedButton.y = _profileButton.y;
				_UIHolder.addChild(_rankedButton);
				_rankedButton.addEventListener(TouchEvent.TOUCH, openRankedTab);
			}
			
			if (!_tab)
			{
				//Create "tab" indicator
				_tab = new Image(Main.assets.getTexture("Indicator"))
				_tab.pivotX = _tab.width/2;;
				_tab.x = _profileButton.x;
				_tab.y = _profileButton.y - (_profileButton.height * .8);
				_UIHolder.addChild(_tab);
			}
			
			if (!_backButton)
			{
				//Creating Tabs
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_backButton = new BW_Button(0.7, 1.0, "", 0);
				else
					_backButton = new BW_Button(0.5, 0.75, "", 0);
				_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_Back"), Main.assets.getTexture("Pathogen_BTN_Back_Down"));
				_UIHolder.addChild(_backButton);
				_backButton.x = PathogenTest.wTenPercent * .5;
				//_backButton.y = PathogenTest.stageHeight - (PathogenTest.hTenPercent * .8);
				_backButton.y = _profileButton.y
				_backButton.addEventListener(TouchEvent.TOUCH, onTouchBack);
			}
			
			if(_openingTab == "Custom")
				openCustomTab(null);
			else if (_openingTab == "Ranked")
				openRankedTab(null);
			else
				openProfileTab(null);
		}
		
		private function checkForInit(event:Event):void
		{
			if (_invitesGathered)
			{	
				if (_gamesGathered)
				{
					init();
					removeEventListener(Event.ENTER_FRAME, checkForInit);
				}
				else if (_gamesFailed)
				{
					startFadeOut();
				}
			}
			else if (_invitesFailed)
			{
				init();
				removeEventListener(Event.ENTER_FRAME, checkForInit);
				//startFadeOut();
			}
		}
		public function init():void
		{
			setUpButtons();
			_slider.alpha = 0;
			addEventListener(Event.ENTER_FRAME, fadeInSlider);
		}
		private function fadeInSlider($e:Event):void
		{
			_slider.alpha += Main.FADE_SPEED;
			
			if(_slider.alpha >= 1.0)
				removeEventListener(Event.ENTER_FRAME, fadeInSlider);
		}
		
		public function initRanked():void
		{
			setUpButtons();
			_rankedSlider.alpha = 0;
			addEventListener(Event.ENTER_FRAME, fadeInRankedSlider);
		}
		private function fadeInRankedSlider($e:Event):void
		{
			_rankedSlider.alpha += Main.FADE_SPEED;
			
			if(_rankedSlider.alpha >= 1.0)
				removeEventListener(Event.ENTER_FRAME, fadeInRankedSlider);
		}
		
		private function gatherCustomOnlineData():void
		{
			_main.entityManager.refreshPlayerEntity();
			//Main.updateDebugText("Gathering Custon Online Data");
			
			// gather games
			var saveGameIDsExist:Boolean = false;
			// check if there is save game data
			if (_main.onlineGameIDs)
			{
				if (_main.onlineGameIDs.length > 0)
				{
					saveGameIDsExist = true;
				}
			}
			_main.entityManager.gatherGamesFromPlayerID();
			
		}
		
		private function gatherRankedOnlineData():void
		{
			// gather games
			var saveGameIDsExist:Boolean = false;
			// check if there is save game data
			if (_main.onlineRankedGameIDs)
			{
				if (_main.onlineRankedGameIDs.length > 0)
				{
					saveGameIDsExist = true;
				}
			}
			// if there is, gather games from the save game data
			if (saveGameIDsExist)
				_main.entityManager.gatherRankedGamesFromSaveGameIDs(_main.onlineRankedGameIDs);
				// otherwise, gather data completely from online game records
			else
				_main.entityManager.gatherRankedGamesFromPlayerID();
		}
		
		private function setUpButtons():void
		{
			//trace("setUpButtons");
			
			var onlineGamePreview:OnlineGamePreview;
			var games:Vector.<GameEntity>;
			var fontSize:int
			var i:int;
			_selectorWidth = 4;
			_selectorHeight = 6;
			
			// button slider
			var layout:HorizontalLayout = new HorizontalLayout();
			layout.paddingTop = PathogenTest.hTenPercent * 3.25;
			layout.paddingLeft = PathogenTest.wTenPercent * 2.25;
			layout.gap = PathogenTest.wTenPercent / 2;
			
			//trace("added slider");
			
			// for custom games
			if (!_ranked)
			{
				if(_slider != null)
					removeChild(_slider);
				
				_slider = new ScrollContainer();
				_slider.layout = layout;
				_slider.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_ON;
				_slider.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
				_slider.scrollerProperties.snapScrollPositionsToPixels = true;
				_slider.width = PathogenTest.stageWidth;
				_slider.height = PathogenTest.hTenPercent * 7.5;
				_slider.y = PathogenTest.hTenPercent;
				_UIHolder.addChild(_slider);
				
				// new game button, if there is enough slots
				var slotsTaken:int = _main.entityManager.invitedGames.length + _main.entityManager.getTotalNumberIncompleteGames();
				//trace("slots taken = " + slotsTaken);
				
				// new game button
				fontSize = 48 / PathogenTest.scaleFactor;
				_newGameButton = new BW_Button(_selectorWidth, _selectorHeight, "", fontSize);
				_slider.addChild(_newGameButton);
				if (slotsTaken >= _main.entityManager.currentPlayerEntity.customGames)
					_newGameButton.addEventListener(TouchEvent.TOUCH, addOnlineMaxSlotWindow);
				else
					_newGameButton.addEventListener(TouchEvent.TOUCH, onTouchRandomGame);
				
				// add text to button
				fontSize = PathogenTest.HD_Multiplyer * 324 / PathogenTest.scaleFactor;
				var text:TextField;
				
				var plusIcon:Image = new Image(Main.assets.getTexture("NewGame"));
				plusIcon.pivotX = plusIcon.width/2;
				plusIcon.pivotY = plusIcon.height/2;
				_newGameButton.addChild(plusIcon);
				plusIcon.y = -PathogenTest.hTenPercent/2;
				
				fontSize = PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor;
				text = new TextField(PathogenTest.wTenPercent * 4.0, PathogenTest.hTenPercent * 1.0, "Start a Game", "Questrial", fontSize, Color.WHITE);
				text.x -= text.width / 2;
				text.y += text.height / 2;
				_newGameButton.addChild(text);
				
				fontSize = PathogenTest.HD_Multiplyer * 48 / PathogenTest.scaleFactor;
				text = new TextField(PathogenTest.wTenPercent * 4.0, PathogenTest.hTenPercent * 1.0, "Random Match", "Questrial", fontSize, Color.WHITE);
				text.x -= text.width / 2;
				text.y += text.height * 1.25;
				_newGameButton.addChild(text);
				
				if(!Main.isAndroid())
				{
					//Random Game Button
					_randomGameButton = new BW_Button(_selectorWidth, _selectorHeight, "", fontSize);
					_slider.addChild(_randomGameButton);
					
					if (slotsTaken >= _main.entityManager.currentPlayerEntity.customGames)
						_randomGameButton.addEventListener(TouchEvent.TOUCH, addOnlineMaxSlotWindow);
					else
						_randomGameButton.addEventListener(TouchEvent.TOUCH, onTouchNewGame);
					
					// add text to button
					fontSize = 324 / PathogenTest.scaleFactor;
					plusIcon = new Image(Main.assets.getTexture("NewGame"));
					plusIcon.pivotX = plusIcon.width/2;
					plusIcon.pivotY = plusIcon.height/2;
					_randomGameButton.addChild(plusIcon);
					plusIcon.y = -PathogenTest.hTenPercent/2;
					
					fontSize = PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor;
					text = new TextField(PathogenTest.wTenPercent * 4.0, PathogenTest.hTenPercent * 1.0, "Start a Game", "Questrial", fontSize, Color.WHITE);
					text.x -= text.width / 2;
					text.y += text.height / 2;
					_randomGameButton.addChild(text);
					
					fontSize = PathogenTest.HD_Multiplyer * 48 / PathogenTest.scaleFactor;
					text = new TextField(PathogenTest.wTenPercent * 4.0, PathogenTest.hTenPercent * 1.0, "Invite Friends", "Questrial", fontSize, Color.WHITE);
					text.x -= text.width / 2;
					text.y += text.height * 1.25;
					_randomGameButton.addChild(text);
				}
				
				// flurry for number of online games
				if(Flurry.isSupported)
					Flurry.logEvent("Number of Online Games", { OnlineGames: slotsTaken});
				
				// game invites
				_invitedOnlineGamePreviews = new Vector.<OnlineGamePreview>();
				var gameInvites:Vector.<InviteEntity> = _main.entityManager.invites;
				for (i=0; i<gameInvites.length; i++)
				{
					var invitedGame:GameEntity = _main.entityManager.findInvitedGameFromID(gameInvites[i].gameID);
					onlineGamePreview = new OnlineGamePreview(this, invitedGame.mapID, EntityManager.GAME_STATUS_WAITING, gameInvites[i].gameID, invitedGame.playerNicknames, invitedGame.colors);
					_invitedOnlineGamePreviews.push(onlineGamePreview);
					_slider.addChild(onlineGamePreview);
					//trace("added preview for game id " + gameInvites[i].gameID);
				}
				
				// games
				_activeOnlineGamePreviews = new Vector.<OnlineGamePreview>();
				games = _main.entityManager.games;
				//trace("in online play, there are " + games.length + " games");

				for (i=0; i<games.length; i++)
				{
					if (!games[i].ranked)
					{
						onlineGamePreview = new OnlineGamePreview(this, games[i].mapID, games[i].status, games[i].id, games[i].playerNicknames, games[i].colors);
						_activeOnlineGamePreviews.push(onlineGamePreview);
						//_slider.addChild(onlineGamePreview);
					}
				}
				//_activeOnlineGamePreviews.sort(sortOnlineGamePreview);
				for (i=0; i<_activeOnlineGamePreviews.length; i++)
					_slider.addChild(_activeOnlineGamePreviews[i]);
				
				// slider events
				_slider.addEventListener(FeathersEventType.SCROLL_COMPLETE, onScrollComplete);
				_slider.addEventListener(Event.SCROLL, onScroll);
				Starling.juggler.delayCall(stopSliding, 0.25);
			}
			// for ranked games
			else
			{
				if(_rankedSlider != null)
					removeChild(_rankedSlider);
				
				_rankedSlider = new ScrollContainer();
				_rankedSlider.layout = layout;
				_rankedSlider.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_ON;
				_rankedSlider.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
				_rankedSlider.scrollerProperties.snapScrollPositionsToPixels = true;
				_rankedSlider.width = PathogenTest.stageWidth;
				_rankedSlider.height = PathogenTest.hTenPercent * 7.5;
				_rankedSlider.y = PathogenTest.hTenPercent;
				_UIHolder.addChild(_rankedSlider);
				
				// Ladder Button
				_ladderButton = new BW_Button(_selectorWidth, _selectorHeight, "", fontSize);
				_rankedSlider.addChild(_ladderButton);
				if (_main.entityManager.currentPlayerEntity.league2Player != "" && _main.entityManager.currentPlayerEntity.division2Player != EntityManager.NOT_FOUND)
					_ladderButton.addEventListener(TouchEvent.TOUCH, onTouchViewLadder);
				else
					_ladderButton.addEventListener(TouchEvent.TOUCH, onTouchJoinLeagueWindow);
				
				// add text to button
				fontSize = 324 / PathogenTest.scaleFactor;
				plusIcon = new Image(Main.assets.getTexture("Ladder"));
				plusIcon.pivotX = plusIcon.width/2;
				plusIcon.pivotY = plusIcon.height/2;
				_ladderButton.addChild(plusIcon);
				plusIcon.y = -PathogenTest.hTenPercent/2;
				
				fontSize = PathogenTest.HD_Multiplyer * 48 / PathogenTest.scaleFactor;
				text = new TextField(PathogenTest.wTenPercent * 4.0, PathogenTest.hTenPercent * 1.0, "View Ladder", "Questrial", fontSize, Color.WHITE);
				text.x -= text.width / 2;
				text.y += text.height * 1.25;
				_ladderButton.addChild(text);
				
				// new game button, if there is enough slots
				if (_main.entityManager.getTotalNumberIncompleteRankedGames() < _main.entityManager.currentPlayerEntity.rankedGames)
				{
					// new ranked game button
					_startRankedGameButton = new BW_Button(_selectorWidth, _selectorHeight, "", fontSize);
					_rankedSlider.addChild(_startRankedGameButton);
					_startRankedGameButton.addEventListener(TouchEvent.TOUCH, onTouchNewRankedGame);
					
					// add text to button
					fontSize = 324 / PathogenTest.scaleFactor;
					plusIcon = new Image(Main.assets.getTexture("NewGame"));
					plusIcon.pivotX = plusIcon.width/2;
					plusIcon.pivotY = plusIcon.height/2;
					_startRankedGameButton.addChild(plusIcon);
					plusIcon.y = -PathogenTest.hTenPercent/2;
					
					fontSize = PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor;
					text = new TextField(PathogenTest.wTenPercent * 4.0, PathogenTest.hTenPercent * 1.0, "Start a Game", "Questrial", fontSize, Color.WHITE);
					text.x -= text.width / 2;
					text.y += text.height / 2;
					_startRankedGameButton.addChild(text);
					
					fontSize = PathogenTest.HD_Multiplyer * 48 / PathogenTest.scaleFactor;
					text = new TextField(PathogenTest.wTenPercent * 4.0, PathogenTest.hTenPercent * 1.0, "Ranked Match", "Questrial", fontSize, Color.WHITE);
					text.x -= text.width / 2;
					text.y += text.height * 1.25;
					_startRankedGameButton.addChild(text);
				}
				
				// games
				_activeRankedOnlineGamePreviews = new Vector.<OnlineGamePreview>();
				games = _main.entityManager.games;
				for (i=0; i<games.length; i++)
				{
					if (games[i].ranked)
					{
						onlineGamePreview = new OnlineGamePreview(this, games[i].mapID, games[i].status, games[i].id, games[i].playerNicknames, games[i].colors);
						_activeRankedOnlineGamePreviews.push(onlineGamePreview);
						//_rankedSlider.addChild(onlineGamePreview);
					}
				}
				_activeRankedOnlineGamePreviews.sort(sortOnlineGamePreview);
				for (i=0; i<_activeRankedOnlineGamePreviews.length; i++)
					_rankedSlider.addChild(_activeRankedOnlineGamePreviews[i]);
				
				// slider events
				_rankedSlider.addEventListener(FeathersEventType.SCROLL_COMPLETE, onRankedScrollComplete);
				_rankedSlider.addEventListener(Event.SCROLL, onRankedScroll);
				Starling.juggler.delayCall(stopRankedSliding, 0.25);
			}
			
			setChildIndex(_UIHolder, this.numChildren - 1);
		}
		private function addOnlineMaxSlotWindow(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && !_sliding && alpha == 1)
			{
				removeRefreshers();
				
				_onlineMaxSlotWindow = new OnlineMaxSlotWindow(this);
				addChild(_onlineMaxSlotWindow);
			}
		}
		public function removeOnlineMaxSlotWindow():void
		{
			_onlineMaxSlotWindow.dispose();
			removeChild(_onlineMaxSlotWindow);
			_onlineMaxSlotWindow = null;
		}
		
		private function onTouchJoinLeagueWindow(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && !_sliding && alpha == 1)
			{
				removeRefreshers();
				
				_joinLeagueWindow = new YouMustJoinLeagueWindow(this);
				addChild(_joinLeagueWindow);
			}
		}

		public function removeJoinLeagueWindow():void
		{
			_joinLeagueWindow.dispose();
			removeChild(_joinLeagueWindow);
			_joinLeagueWindow = null;
		}
		
		// custom slider functions
		public function onScrollComplete(event:Event):void
		{
			_sliding = false;
		}
		public function onScroll(event:Event):void
		{
			_sliding = true;
		}
		private function stopSliding():void
		{
			_sliding = false;
		}
		
		// ranked slider functions
		public function onRankedScrollComplete(event:Event):void
		{
			_rankedSliding = false;
		}
		public function onRankedScroll(event:Event):void
		{
			_rankedSliding = true;
		}
		private function stopRankedSliding():void
		{
			_rankedSliding = false;
		}
		
		private function onTouchNewGame(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && !_sliding && alpha == 1)
			{
				hideUI();
				_onlineGameSetup = new OnlineGameSetup(this)
				addChild(_onlineGameSetup);
			}
		}
		
		private function onTouchRandomGame(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && !_sliding && alpha == 1)
			{
				hideUI();
				
				_main.entityManager.findRandomGames();
				addEventListener(Event.ENTER_FRAME, checkForRandomGameMatch);
			}
		}
		
		private function onTouchNewRankedGame(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && !_rankedSliding && alpha == 1)
			{
				_main.entityManager.findRankedGames();
				addEventListener(Event.ENTER_FRAME, checkForRankedGameMatch);
			}
		}
		private function onTouchViewLadder($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && !_rankedSliding && alpha == 1)
			{
				removeRefreshers();
				
				if (_main.entityManager.currentPlayerEntity.league2Player != "" && _main.entityManager.currentPlayerEntity.division2Player != EntityManager.NOT_FOUND)
				{
					// add loader
					_loader = new Loader("Loading Rank Data", 38);
					addChild(_loader);
					_loader.x = PathogenTest.stageCenter.x;
					_loader.y = PathogenTest.stageCenter.y;
					
					_main.entityManager.generateCurrentLadder();
					
					_ladderWaitTimer = 0;
					addEventListener(Event.ENTER_FRAME, waitForLadder);
					removeEventListener(Event.ENTER_FRAME, updateToRefreshRankedGames);
				}
			}
		}
		private function waitForLadder(event:Event):void
		{
			_ladderWaitTimer++;
			if (_ladderWaitTimer > LOAD_TIMEOUT)
			{
				if (_loader)
				{
					_loader.dispose();
					removeChild(_loader);
					_loader = null;
				}
				removeEventListener(Event.ENTER_FRAME, waitForLadder);
				refreshProfile();
			}
			if (_main.entityManager.currentLadder)
			{
				if (_main.entityManager.currentLadder.length > 0)
				{
					if (_loader)
					{
						_loader.dispose();
						removeChild(_loader);
						_loader = null;
					}
					removeEventListener(Event.ENTER_FRAME, waitForLadder);
					
					_ladderScreen = new LadderScreen(this);
					addChild(_ladderScreen);
				}
			}
		}
		public function removeLadderScreen():void
		{
			_ladderScreen.dispose();
			removeChild(_ladderScreen);
			_ladderScreen = null;
		}
		
		public function removeOnlineGameSetup():void
		{
			if(_onlineGameSetup)
			{
				_onlineGameSetup.dispose();
				removeChild(_onlineGameSetup);
				_onlineGameSetup = null;
			}
			
			showUI();
		}
		private function checkForRankedGameMatch(event:Event):void
		{
			if (!_main.entityManager.seekingRankedGame)
			{
				if (_main.entityManager.foundRankedGameToJoin)
				{
					//trace("in onlineplay, check for ranked game match");
					
					_main.initOnlineGameManager(null, null, null, _main.entityManager.games[_main.entityManager.currentGameIndex].id);
					removeToStartGame();
					removeEventListener(Event.ENTER_FRAME, checkForRankedGameMatch);
				}
				else
				{
					startNewRankedGame();
					removeEventListener(Event.ENTER_FRAME, checkForRankedGameMatch);
				}
			}
		}
		
		private function checkForRandomGameMatch(event:Event):void
		{
			if (!_main.entityManager.seekingRandomGame)
			{
				if (_main.entityManager.foundRandomGameToJoin)
				{
					//trace("in onlineplay, check for ranked game match");

					//_main.initOnlineGameManager(null, null, _main.mapManager.maps[0], _main.entityManager.games[_main.entityManager.currentGameIndex].id);
					_main.initOnlineGameManager(null, null, null, _main.entityManager.games[_main.entityManager.currentGameIndex].id);
					removeToStartGame();
					removeEventListener(Event.ENTER_FRAME, checkForRandomGameMatch);
				}
				else
				{
					startNewRandomGame();
					removeEventListener(Event.ENTER_FRAME, checkForRandomGameMatch);
				}
			}
		}
		
		private function onTouchBack(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && !_sliding && alpha == 1)
			{
				backToMainMenu();
			}
		}
		
		public function startNewCustomGame(otherPlayerID:String, map:Map):void
		{
			var playerArray:Array = [];
			var playerColors:Array = [];
			var playerLocations:Array = [];
			var player:OnlinePlayer = new OnlinePlayer(0, 0, _main.onlineGameManager);
			playerArray.push(player);
			playerColors.push(0);
			playerLocations.push(0);
			player = new OnlinePlayer(1, 1, _main.onlineGameManager);
			playerArray.push(player);
			playerColors.push(1);
			playerLocations.push(1);
			
			// create online game record
			var invitedPlayerArray:Array = [];
			invitedPlayerArray.push(otherPlayerID);
			_main.entityManager.addGame(2, invitedPlayerArray, map, false, false);

			// begin game
			_main.initOnlineGameManager(playerColors, playerLocations, map, "");
			removeToStartGame();
		}
		
		public function startNewRankedGame():void
		{
			// get random map
			var mapIndex:int = Math.random() * MapManager.END_SMALL_INDEX;
			//var mapIndex:int = 0;
			var map:Map = _main.mapManager.getMap(mapIndex);
			
			var playerArray:Array = [];
			var playerColors:Array = [];
			var playerLocations:Array = [];
			var player:OnlinePlayer = new OnlinePlayer(0, 0, _main.onlineGameManager);
			playerArray.push(player);
			playerColors.push(0);
			playerLocations.push(0);
			player = new OnlinePlayer(1, 1, _main.onlineGameManager);
			playerArray.push(player);
			playerColors.push(1);
			playerLocations.push(1);
			
			// create online game record
			_main.entityManager.addGame(2, null, map, true, false);
			
			// begin game
			_main.initOnlineGameManager(playerColors, playerLocations, map, "");
			removeToStartGame();
		}
		
		public function startNewRandomGame():void
		{
			//trace("starting new random game");
			
			// get random map
			var mapIndex:int = Math.random() * MapManager.END_SMALL_INDEX;
			//var mapIndex:int = 0;
			var map:Map = _main.mapManager.getMap(mapIndex);
			
			var playerArray:Array = [];
			var playerColors:Array = [];
			var playerLocations:Array = [];
			var player:OnlinePlayer = new OnlinePlayer(0, 0, _main.onlineGameManager);
			playerArray.push(player);
			playerColors.push(0);
			playerLocations.push(0);
			player = new OnlinePlayer(1, 1, _main.onlineGameManager);
			playerArray.push(player);
			playerColors.push(1);
			playerLocations.push(1);
			
			// create online game record
			_main.entityManager.addGame(2, null, map, false, true);
			
			// begin game
			_main.initOnlineGameManager(playerColors, playerLocations, map, "");
			removeToStartGame();
		}
		
		public function loadGame(gameID:String):void
		{
			if (!_sliding)
			{
				_main.initOnlineGameManager(null, null, null, gameID);
				_main.removeGameLoader();
				removeToStartGame();
			}
		}
		
		public function showUI():void
		{
			if (_openingTab == "Profile")
				addEventListener(Event.ENTER_FRAME, updateToRefreshProfile);
			else if (_openingTab == "Custom")
				addEventListener(Event.ENTER_FRAME, updateToRefreshCustomGames);
			else
				addEventListener(Event.ENTER_FRAME, updateToRefreshRankedGames);
			
			_UIHolder.visible = true;
			var tween:Tween = new Tween(_UIHolder, 1);
			tween.fadeTo(1);
			Starling.juggler.add(tween);
		}
		private function showUIComplete():void
		{
			
		}
		private function hideUI():void
		{
			removeEventListener(Event.ENTER_FRAME, updateToRefreshCustomGames);
			removeEventListener(Event.ENTER_FRAME, updateToRefreshProfile);
			removeEventListener(Event.ENTER_FRAME, updateToRefreshRankedGames);
			
			var tween:Tween = new Tween(_UIHolder, 1);
			tween.fadeTo(0);
			tween.onComplete = hideUIComplete;
			Starling.juggler.add(tween);
		}
		private function hideUIComplete():void
		{
			if (_UIHolder)
				_UIHolder.visible = false;
		}
		
		public function backToMainMenu():void
		{
			_startGame = false;
			startFadeOut();
		}
		
		public function removeToStartGame():void
		{
			_startGame = true;
			startFadeOut();
		}
		
		private function openProfileTab($e:TouchEvent):void
		{
			var touch:Touch = null;
			var open:Boolean = false;
			
			if($e)
				touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				open = true;
			
			if (touch || open)
			{
				if(_profileScreen == null)
				{
					_profileScreen = new ProfileScreen(this);
					addChild(_profileScreen);
				}
				
				//turn off all screens, but this one.
				if(_profileScreen)
					_profileScreen.visible = true;
				if(_slider)
					_slider.visible = false;
				if(_rankedSlider)
					_rankedSlider.visible = false;
				
				_tab.x = _profileButton.x;
				
				_openingTab = "Profile";
				_main.lastOnlinePlayTab = _openingTab;
				_refreshTimer = 0;
				addEventListener(Event.ENTER_FRAME, updateToRefreshProfile);
				removeEventListener(Event.ENTER_FRAME, updateToRefreshCustomGames);
				removeEventListener(Event.ENTER_FRAME, updateToRefreshRankedGames);
			}
		}
		
		private function openCustomTab($e:TouchEvent):void
		{
			var touch:Touch = null;
			var open:Boolean = false;
			
			if($e)
				touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				open = true;
			
			if (touch || open)
			{
				if(_slider == null || _refreshing || _ranked)
				{
					_refreshing = false;
					_ranked = false;
					init();
				}
				else
					_ranked = false;
				
				_tab.x = _customButton.x;
				
				//turn off all screens, but this one.
				if(_slider)
					_slider.visible = true;
				if(_profileScreen)
					_profileScreen.visible = false;
				if (_rankedSlider)
					_rankedSlider.visible = false;
				
				_openingTab = "Custom";
				_main.lastOnlinePlayTab = _openingTab;
				_refreshTimer = 0;
				addEventListener(Event.ENTER_FRAME, updateToRefreshCustomGames);
				removeEventListener(Event.ENTER_FRAME, updateToRefreshProfile);
				removeEventListener(Event.ENTER_FRAME, updateToRefreshRankedGames);
			}
		}
		
		private function openRankedTab($e:TouchEvent):void
		{
			var touch:Touch = null;
			var open:Boolean = false;
			
			if ($e)
				touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				open = true;
			
			if (touch || open)
			{
				if(_rankedSlider == null || _refreshing || !_ranked)
				{
					_refreshing = false;
					_ranked = true;
					initRanked();
				}
				else
					_ranked = true;
				
				_tab.x = _rankedButton.x;
				
				//turn off all screens, but this one.
				if (_rankedSlider)
					_rankedSlider.visible = true;
				if(_slider)
					_slider.visible = false;
				if(_profileScreen)
					_profileScreen.visible = false;
				
				_openingTab = "Ranked";
				_main.lastOnlinePlayTab = _openingTab;
				_refreshTimer = 0;
				addEventListener(Event.ENTER_FRAME, updateToRefreshRankedGames);
				removeEventListener(Event.ENTER_FRAME, updateToRefreshCustomGames);
				removeEventListener(Event.ENTER_FRAME, updateToRefreshProfile);
			}
		}
		
		public function removeRankedComingSoonScreen():void
		{
			_rankedComingSoonScreen.dispose();
			removeChild(_rankedComingSoonScreen);
			_rankedComingSoonScreen = null;
			
			if (_openingTab == "Profile")
				addEventListener(Event.ENTER_FRAME, updateToRefreshProfile);
			else
				addEventListener(Event.ENTER_FRAME, updateToRefreshCustomGames);
		}
		
		public function revertOtherMapPreviewsToUnselected(gamePreview:OnlineGamePreview):void
		{
			var i:int;
			if (_invitedOnlineGamePreviews)
			{
				for (i=0; i<_invitedOnlineGamePreviews.length; i++)
				{
					if (_invitedOnlineGamePreviews[i] != gamePreview)
						_invitedOnlineGamePreviews[i].revertToUnselected();
				}
			}
			if (_activeOnlineGamePreviews)
			{
				for (i=0; i<_activeOnlineGamePreviews.length; i++)
				{
					if (_activeOnlineGamePreviews[i] != gamePreview)
						_activeOnlineGamePreviews[i].revertToUnselected();
				}
			}
		}
		
		public function addInternetConnectivityError():void
		{
			if (!_internetConnectivityError)
			{
				_internetConnectivityError = new InternetConnectivityError(null, this);
				addChild(_internetConnectivityError);
				_internetConnectivityError.x = 0;
				_internetConnectivityError.y = 0;
			}
			else
			{
				_internetConnectivityError.visible = true;
			}
		}
		
		public function removeInternetConnectivityError():void
		{
			if (_internetConnectivityError)
			{
				_internetConnectivityError.dispose();
				removeChild(_internetConnectivityError);
				_internetConnectivityError = null;
				
				death();
				
				if (main)
				{
					main.recheckInternetConnectivity();
					main.removeOnlinePlay(false);
				}
			}
		}
		
		private function updateToRefreshCustomGames(event:Event):void
		{
			_refreshTimer++;
			if (_refreshTimer >= REFRESH_TIME)
			{
				_refreshing = true;
				_loader = new Loader("Refreshing Online Data", 38);
				addChild(_loader);
				_loader.x = PathogenTest.stageCenter.x;
				_loader.y = PathogenTest.stageCenter.y;
				_openingTab = "Custom";
				_main.lastOnlinePlayTab = _openingTab;
				_refreshTimer = 0;
				refreshCustomGames();
			}
		}
		
		private function refreshCustomGames():void
		{
			//trace("refreshing custom games");
			
			_main.reloadGameCenterFriends();
			_main.entityManager.refreshPlayerEntity();
			
			// remove all current content from custom games tab
			var i:int;
			if (_invitedOnlineGamePreviews)
			{
				for (i=(_invitedOnlineGamePreviews.length - 1); i>=0; i--)
				{
					_invitedOnlineGamePreviews[i].garbageCollection();
					_slider.removeChild(_invitedOnlineGamePreviews[i]);
					_invitedOnlineGamePreviews.pop();
				}
				_invitedOnlineGamePreviews = null;
			}
			if (_activeOnlineGamePreviews)
			{
				for (i=(_activeOnlineGamePreviews.length - 1); i>=0; i--)
				{
					_activeOnlineGamePreviews[i].garbageCollection();
					_slider.removeChild(_activeOnlineGamePreviews[i]);
					_activeOnlineGamePreviews.pop();
				}
				_activeOnlineGamePreviews = null;
			}
			if (_randomGameButton)
			{
				_randomGameButton.destroy();
				_slider.removeChild(_randomGameButton);
				_randomGameButton = null;
			}
			if (_newGameButton)
			{
				_newGameButton.destroy();
				_slider.removeChild(_newGameButton);
				_newGameButton = null;
			}
			if (_slider)
			{
				_slider.removeEventListener(FeathersEventType.SCROLL_COMPLETE, onScrollComplete);
				_slider.removeEventListener(Event.SCROLL, onScroll);
				_slider.dispose();
				_UIHolder.removeChild(_slider);
				_slider = null;
			}
			
			// reset variables
			_invitesGathered = false;
			_invitesFailed = false;
			_gamesGathered = false;
			_gamesFailed = false;
			
			// initiate game data refresh
			_main.entityManager.gatherGamesFromPlayerID();
			_loadTimer = 0;
			addEventListener(Event.ENTER_FRAME, checkForGatherdGamesPartI);
		}
		
		private function updateToRefreshRankedGames(event:Event):void
		{
			_refreshTimer++;
			if (_refreshTimer >= REFRESH_TIME)
			{
				_refreshing = true;
				_loader = new Loader("Refreshing Online Data", 38);
				addChild(_loader);
				_loader.x = PathogenTest.stageCenter.x;
				_loader.y = PathogenTest.stageCenter.y;
				_openingTab = "Ranked";
				_main.lastOnlinePlayTab = _openingTab;
				_refreshTimer = 0;
				refreshRankedGames();
			}
		}
		
		private function refreshRankedGames():void
		{
			//trace("refreshing custom games");
			
			_main.reloadGameCenterFriends();
			_main.entityManager.refreshPlayerEntity();
			
			// remove all current content from custom games tab
			var i:int;
			if (_invitedOnlineGamePreviews)
			{
				for (i=(_invitedOnlineGamePreviews.length - 1); i>=0; i--)
				{
					_invitedOnlineGamePreviews[i].garbageCollection();
					_slider.removeChild(_invitedOnlineGamePreviews[i]);
					_invitedOnlineGamePreviews.pop();
				}
				_invitedOnlineGamePreviews = null;
			}
			if (_activeOnlineGamePreviews)
			{
				for (i=(_activeOnlineGamePreviews.length - 1); i>=0; i--)
				{
					_activeOnlineGamePreviews[i].garbageCollection();
					_slider.removeChild(_activeOnlineGamePreviews[i]);
					_activeOnlineGamePreviews.pop();
				}
				_activeOnlineGamePreviews = null;
			}
			if (_startRankedGameButton)
			{
				_startRankedGameButton.removeEventListener(TouchEvent.TOUCH, onTouchNewRankedGame);
				_startRankedGameButton.destroy();
				_rankedSlider.removeChild(_startRankedGameButton);
				_startRankedGameButton = null;
			}
			if (_ladderButton)
			{
				_ladderButton.removeEventListener(TouchEvent.TOUCH, onTouchViewLadder);
				_ladderButton.destroy();
				_rankedSlider.removeChild(_ladderButton);
				_ladderButton = null;
			}
			if (_rankedSlider)
			{
				_rankedSlider.removeEventListener(FeathersEventType.SCROLL_COMPLETE, onRankedScrollComplete);
				_rankedSlider.removeEventListener(Event.SCROLL, onRankedScroll);
				_rankedSlider.dispose();
				_UIHolder.removeChild(_rankedSlider);
				_rankedSlider = null;
			}
			
			// reset variables
			_invitesGathered = false;
			_invitesFailed = false;
			_gamesGathered = false;
			_gamesFailed = false;
			
			// initiate game data refresh
			_main.entityManager.gatherGamesFromPlayerID();
			_loadTimer = 0;
			addEventListener(Event.ENTER_FRAME, checkForGatherdGamesPartI);
		}
		
		private function updateToRefreshProfile(event:Event):void
		{
			_refreshTimer++;
			if (_refreshTimer >= REFRESH_TIME)
			{
				_refreshing = true;
				_loader = new Loader("Refreshing Online Data", 38);
				addChild(_loader);
				_loader.x = PathogenTest.stageCenter.x;
				_loader.y = PathogenTest.stageCenter.y;
				_openingTab = "Profile";
				_main.lastOnlinePlayTab = _openingTab;
				_refreshTimer = 0;
				refreshProfile();
			}
		}
		
		private function refreshProfile():void
		{
			//trace("refreshing profile");
			
			_main.reloadGameCenterFriends();
			_main.entityManager.refreshPlayerEntity();
			
			// remove all current content from custom games tab
			_profileScreen.death();
			removeChild(_profileScreen);
			_profileScreen = null;
			
			// reset variables
			_invitesGathered = false;
			_invitesFailed = false;
			_gamesGathered = false;
			_gamesFailed = false;
			
			// initiate game data refresh
			_main.entityManager.gatherGamesFromPlayerID();
			addEventListener(Event.ENTER_FRAME, checkForGatherdGamesPartI);
		}
		
		public function removeRefreshers():void
		{
			removeEventListener(Event.ENTER_FRAME, updateToRefreshCustomGames);
			removeEventListener(Event.ENTER_FRAME, updateToRefreshProfile);
			removeEventListener(Event.ENTER_FRAME, updateToRefreshRankedGames);
		}
		
		override public function death():void
		{
			var i:int;
			
			// custom
			if (_invitedOnlineGamePreviews)
			{
				for (i=(_invitedOnlineGamePreviews.length - 1); i>=0; i--)
				{
					_invitedOnlineGamePreviews[i].garbageCollection();
					_slider.removeChild(_invitedOnlineGamePreviews[i]);
					_invitedOnlineGamePreviews.pop();
				}
				_invitedOnlineGamePreviews = null;
			}
			if (_activeOnlineGamePreviews)
			{
				for (i=(_activeOnlineGamePreviews.length - 1); i>=0; i--)
				{
					_activeOnlineGamePreviews[i].garbageCollection();
					_slider.removeChild(_activeOnlineGamePreviews[i]);
					_activeOnlineGamePreviews.pop();
				}
				_activeOnlineGamePreviews = null;
			}
			if (_newGameButton)
			{
				_newGameButton.removeEventListener(TouchEvent.TOUCH, addOnlineMaxSlotWindow);
				_newGameButton.removeEventListener(TouchEvent.TOUCH, onTouchRandomGame);
				_newGameButton.destroy();
				_slider.removeChild(_newGameButton);
				
				_newGameButton = null;
			}
			if (_randomGameButton)
			{
				_randomGameButton.removeEventListener(TouchEvent.TOUCH, addOnlineMaxSlotWindow);
				_randomGameButton.removeEventListener(TouchEvent.TOUCH, onTouchNewGame);
				_randomGameButton.destroy();
				_slider.removeChild(_randomGameButton);
				_randomGameButton = null;
			}
			if (_slider)
			{
				_slider.removeEventListener(FeathersEventType.SCROLL_COMPLETE, onScrollComplete);
				_slider.removeEventListener(Event.SCROLL, onScroll);
				_slider.dispose();
				_UIHolder.removeChild(_slider);
				_slider = null;
			}
			
			// ranked
			if (_activeRankedOnlineGamePreviews)
			{
				for (i=(_activeRankedOnlineGamePreviews.length - 1); i>=0; i--)
				{
					_activeRankedOnlineGamePreviews[i].garbageCollection();
					_rankedSlider.removeChild(_activeRankedOnlineGamePreviews[i]);
					_activeRankedOnlineGamePreviews.pop();
				}
				_activeRankedOnlineGamePreviews = null;
			}
			if (_startRankedGameButton)
			{
				_startRankedGameButton.removeEventListener(TouchEvent.TOUCH, onTouchNewRankedGame);
				_startRankedGameButton.destroy();
				_rankedSlider.removeChild(_startRankedGameButton);
				_startRankedGameButton = null;
			}
			if (_ladderButton)
			{
				_ladderButton.removeEventListener(TouchEvent.TOUCH, onTouchViewLadder);
				_ladderButton.destroy();
				_rankedSlider.removeChild(_ladderButton);
				_ladderButton = null;
			}
			if (_rankedSlider)
			{
				_rankedSlider.removeEventListener(FeathersEventType.SCROLL_COMPLETE, onRankedScrollComplete);
				_rankedSlider.removeEventListener(Event.SCROLL, onRankedScroll);
				_rankedSlider.dispose();
				_UIHolder.removeChild(_rankedSlider);
				_rankedSlider = null;
			}
			
			if (_onlineGameSetup)
			{
				_onlineGameSetup.dispose();
				removeChild(_onlineGameSetup);
				_onlineGameSetup = null;
			}
			if (_profileButton)
			{
				_profileButton.removeEventListener(TouchEvent.TOUCH, openProfileTab);
				_profileButton.destroy();
				_UIHolder.removeChild(_profileButton);
				_profileButton = null;
			}
			if (_customButton)
			{
				_customButton.removeEventListener(TouchEvent.TOUCH, openCustomTab);
				_customButton.destroy();
				_UIHolder.removeChild(_customButton);
				_customButton = null;
			}
			if (_rankedButton)
			{
				_rankedButton.removeEventListener(TouchEvent.TOUCH, openRankedTab);
				_rankedButton.destroy();
				_UIHolder.removeChild(_rankedButton);
				_rankedButton = null;
			}
			if (_backButton)
			{
				_backButton.removeEventListener(TouchEvent.TOUCH, onTouchBack);
				_backButton.destroy();
				_UIHolder.removeChild(_backButton);
				_backButton = null;
			}
			if(_loader)
			{
				_loader.dispose();
				removeChild(_loader);
				_loader = null;
			}
			if (_profileScreen)
			{
				_profileScreen.death();
				removeChild(_profileScreen);
				_profileScreen = null;
			}
			if (_tab)
			{
				_tab.dispose();
				_UIHolder.removeChild(_tab);
				_tab = null;
			}
			if(_UIHolder)
			{
				_UIHolder.dispose();
				removeChild(_UIHolder);
				_UIHolder = null;
			}
			
			if (_joinLeagueWindow)
			{
				_joinLeagueWindow.dispose();
				removeChild(_joinLeagueWindow);
				_joinLeagueWindow = null;
			}
			
			removeEventListener(Event.ENTER_FRAME, updateToRefreshCustomGames);
			removeEventListener(Event.ENTER_FRAME, updateToRefreshProfile);
			removeEventListener(Event.ENTER_FRAME, updateToRefreshRankedGames);
			removeEventListener(Event.ENTER_FRAME, checkForGatherdGamesPartI);
			removeEventListener(Event.ENTER_FRAME, checkForRandomGameMatch);
			
			_main.removeOnlinePlay(_startGame);
		}
		private function sortOnlineGamePreview(preview1:OnlineGamePreview, preview2:OnlineGamePreview):int
		{
			if (preview1.sortValue < preview2.sortValue)
				return -1;
			else if (preview1.sortValue > preview2.sortValue)
				return 1;
			else
				return 0;
		}
		
		public function get sliding():Boolean { return _sliding; }
		public function get main():Main { return _main; }
		public function get ranked():Boolean { return _ranked; }
		public function get gamesGathered():Boolean { return _gamesGathered; }
		public function get profileScreen():ProfileScreen { return _profileScreen; }
		public function get openingTab():String { return _openingTab; }
		
		public function set invitesGathered(invitesGathered:Boolean):void { _invitesGathered = invitesGathered; }
		public function set invitesFailed(invitesFailed:Boolean):void { _invitesFailed = invitesFailed; }
		public function set gamesGathered(gamesGathered:Boolean):void { _gamesGathered = gamesGathered; }
		public function set gamesFailed(gamesFailed:Boolean):void { _gamesFailed = gamesFailed; }
	}
}