package OnlinePlay
{
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.Color;
	import UI.Main;
	
	public class OnlinePercentBar extends Sprite
	{
		private const FILL_SPEED:Number = 0.25;
		private const FILL_SEGMENTS:int = 40;
		
		private var _total:Number;
		private var _current:Number;
		private var _max:int;
		private var _percentBar:Quad;
		private var _bgWidth:int;
		private var _fillSegmentWidth:Number;
		
		public function OnlinePercentBar(player:OnlinePlayer, currentValue:int, maxValue:int)
		{
			super();
			
			var backgroundRect:Image =  new Image(Main.assets.getTexture("PercentFrame"));
			backgroundRect.pivotX = backgroundRect.width / 2;
			backgroundRect.pivotY = backgroundRect.height / 2;
			addChild(backgroundRect);
			_bgWidth = backgroundRect.width;
			
			_max = maxValue;
			_total = currentValue;
			_current = 0;
			_fillSegmentWidth = _total / FILL_SEGMENTS;
			
			var value:String = currentValue.toString()
			var percentFill:Number = (backgroundRect.width * 0.98) / (_max / _current);
			
			_percentBar = new Quad(backgroundRect.width, backgroundRect.height, player.playerHexColor);
			_percentBar.pivotY = _percentBar.height / 2;
			addChild(_percentBar);
			_percentBar.x = -((backgroundRect.width / 2) - 1);
			_percentBar.width = percentFill;
			_percentBar.addEventListener(Event.ENTER_FRAME, updateGrowBar);
			
			setChildIndex(backgroundRect, (numChildren - 1));
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 28 / PathogenTest.scaleFactor;
			var textField:TextField = new TextField(backgroundRect.width,backgroundRect.height, value, "Dekar", fontSize, Color.WHITE);
			textField.pivotX = textField.width / 2;
			textField.pivotY = textField.height / 2;
			addChild(textField);
		}
		
		public function updateGrowBar(event:Event):void
		{
			if (_current < _total)
			{
				_current += PathogenTest.HD_Multiplyer * _fillSegmentWidth / PathogenTest.scaleFactor;
				if (_current >= _total)
				{
					_percentBar.removeEventListener(Event.ENTER_FRAME, updateGrowBar);
					_current = _total;
				}
				
				var percentFill:Number = (_bgWidth * 0.98) / (_max / _current);
				_percentBar.width = percentFill;
			}
			else
			{
				_percentBar.removeEventListener(Event.ENTER_FRAME, updateGrowBar);
			}
		}
	}
}