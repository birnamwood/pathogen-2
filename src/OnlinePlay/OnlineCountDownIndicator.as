package OnlinePlay
{
	import UI.Main;
	import Utils.BW_Selector;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.deg2rad;
	
	public class OnlineCountDownIndicator extends Sprite
	{
		//Static colors
		private static const GREEN:int = 0x419685;
		private static const RED:int = 0x8F4957;
		private static const BLUE:int = 0x5A69CE;
		private static const GOLD:int = 0xE0AF38;
		
		private static const OFF:Number = .5;
		
		//Countdown Vars
		private var owner:OnlinePlayer;
		private var cellSelector:OnlineCellSelector;
		private var numOfPieces:int;
		
		private var radians:Number;
		private var gap:Number;
		
		//Arrays
		private var pieceArray:Array = [];
		private var whiteFlashArray:Array = [];
		
		public function OnlineCountDownIndicator(owner:OnlinePlayer, cellSelector:OnlineCellSelector ,numOfPieces:int, symbol:Image)
		{
			super();
			
			this.owner = owner;
			this.cellSelector = cellSelector;
			
			//Create all pieces
			for (var i:int = 0; i < numOfPieces; i++)
			{
				var piece:Image;
				var whiteFlashDisplay:Image;
				
				//Can be various number of pieces
				switch(numOfPieces)
				{
					case 4:
						piece = new Image(Main.assets.getTexture("PieceOfFourWhite"));
						whiteFlashDisplay = new Image(Main.assets.getTexture("PieceOfFourWhite"));
						break;
					
					
					case 6:
						piece = new Image(Main.assets.getTexture("PieceOSixWhite"));
						whiteFlashDisplay = new Image(Main.assets.getTexture("PieceOfSixWhite"));
						break
					
					case 8:
						piece = new Image(Main.assets.getTexture("PieceOfEightWhite"));
						whiteFlashDisplay = new Image(Main.assets.getTexture("PieceOfEightWhite"));
						break
				}
				
				var whiteFlash:BW_Selector = new BW_Selector(0,0,whiteFlashDisplay, true, .95, 1, 1);
				
				//Which player color?
				switch(owner.playerTeam)
				{
					case 0:
						piece.color = GREEN;
						break;
					
					case 1:
						piece.color = RED;
						break;
					
					case 2:
						piece.color = BLUE;
						break;
					
					case 3:
						piece.color = GOLD;
						break;
				}
				
				
				//Positioning
				piece.pivotX = piece.width/2;
				piece.pivotY = piece.height/2;
				
				//Add to screen
				addChild(piece);
				addChild(whiteFlash);
				
				piece.alpha = OFF;
				
				//Add to corrisponding arrays
				pieceArray.push(piece);
				whiteFlashArray.push(whiteFlash);
				
				//Place the pieces in a circle
				switch(numOfPieces)
				{
					case 4:
					{
						gap = piece.width/1.8
						
						switch(i)
						{
							case 0:
							{
								piece.x = gap;
								piece.y = -gap;
								radians = deg2rad(0);
								piece.rotation = radians;
								break;
							}
							case 1:
							{
								piece.x = gap;
								piece.y = gap;
								radians = deg2rad(90);
								piece.rotation = radians;
								break;
							}
							case 2:
							{
								piece.x = -gap;
								piece.y = gap;
								radians = deg2rad(180);
								piece.rotation = radians;
								break;
							}
							case 3:
							{
								piece.x = -gap;
								piece.y = -gap;
								radians = deg2rad(-90);
								piece.rotation = radians;
								break;
							}
								
							default:
								break;
						}
						break;
					}
						
					case 6:
					{
						gap = piece.width;
						
						switch(i)
						{
							case 0:
							{
								piece.x = gap /1.9;
								piece.y = -gap * .95;
								radians = deg2rad(0);
								piece.rotation = radians;
								break;
							}
							case 1:
							{
								piece.x = gap * 1.1;
								radians = deg2rad(60);
								piece.rotation = radians;
								break;
							}
							case 2:
							{
								piece.x = gap /1.9;
								piece.y = gap * .95;
								radians = deg2rad(120);
								piece.rotation = radians;
								break;
							}
							case 3:
							{
								piece.x = -gap/1.9;
								piece.y = gap * .95;
								radians = deg2rad(180);
								piece.rotation = radians;
								break;
							}
							case 4:
							{
								piece.x = -gap * 1.1;
								radians = deg2rad(240);
								piece.rotation = radians;
								break;
							}
							case 5:
							{
								piece.x = -gap/1.9;
								piece.y = -gap * .95;
								radians = deg2rad(300);
								piece.rotation = radians;
								break;
							}
								
							default:
								break;
						}
						break;
					}
						
					case 8:
					{
						gap = piece.width * 1.5;
						
						switch(i)
						{
							case 0:
							{
								piece.x = gap *.325;
								piece.y = -gap * .8;
								radians = deg2rad(0);
								piece.rotation = radians;
								break;
							}
							case 1:
							{
								piece.x = gap * .8;
								piece.y = -gap * .35;
								radians = deg2rad(45);
								piece.rotation = radians;
								break;
							}
							case 2:
							{
								piece.x = gap * .8;
								piece.y = gap * .35;
								radians = deg2rad(90);
								piece.rotation = radians;
								break;
							}
							case 3:
							{
								piece.x = gap *.325;
								piece.y = gap * .8;
								radians = deg2rad(135);
								piece.rotation = radians;
								break;
							}
							case 4:
							{
								piece.x = -gap * .325;
								piece.y = gap * .8;
								radians = deg2rad(180);
								piece.rotation = radians;
								break;
							}
							case 5:
							{
								piece.x = -gap * .8;
								piece.y = gap * .35;
								radians = deg2rad(225);
								piece.rotation = radians;
								break;
							}
							case 6:
							{
								piece.x = -gap * .8;
								piece.y = -gap * .35;
								radians = deg2rad(275);
								piece.rotation = radians;
								break;
							}
							case 7:
							{
								piece.x = -gap * .325;
								piece.y = -gap * .8;
								radians = deg2rad(315);
								piece.rotation = radians;
								break;
							}
							default:
								break;
						}
						break;
					}
						
					default:
						break;
				}
				
				whiteFlash.x = piece.x
				whiteFlash.y = piece.y;
				whiteFlash.rotation = radians;
				
				//Create symbol
				symbol.pivotX = symbol.width/2;
				symbol.pivotY = symbol.height/2;
				addChild(symbol);
				
				if(cellSelector.type == 3)
				{
					symbol.scaleX = .5;
					symbol.scaleY = .5;
					symbol.pivotX = symbol.width;
					symbol.pivotY = symbol.height;
				}
				
			}
		}
		public function changeState():void
		{
			//Change indicator
			var piece:Image;
			var whiteFlash:BW_Selector;
			
			//Turn everything off
			for each (piece in pieceArray)
			{
				piece.alpha = OFF;
			}
			
			var delayAmount:Number = 0;
			var delayincrement:Number = .4;
			
			//Turn on the correct indicators
			for (var i:int = 0; i < cellSelector.getCurrentNum(); i++)
			{
				piece = pieceArray[i];
				piece.alpha = 1;
				
				whiteFlash = whiteFlashArray[i];
				
				Starling.juggler.delayCall(whiteFlash.startFlash, delayAmount);
				
				delayAmount += delayincrement;
			}
		}
		public function garbageCollection():void
		{
			for each (var onDisplay:Image in pieceArray)
			{
				onDisplay.dispose();
				removeChild(onDisplay);
				onDisplay = null;
			}
			
			pieceArray = [];
			
			owner = null;
			cellSelector = null;
		}
	}
}