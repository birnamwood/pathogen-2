package OnlinePlay
{
	import Core.CountDownIndicator;
	
	import Sound.SoundController;
	
	import UI.Main;
	
	import Utils.BW_Selector;
	
	import flash.geom.Point;
	import flash.utils.flash_proxy;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class OnlineCellSelector extends Sprite
	{
		private const SCALE_AND_FADE_TIME:Number = 0.5;
		
		//Classes
		public var onlineGameManager:OnlineGameManager;
		
		public var type:int;
		public var owner:OnlinePlayer;
		
		//Images
		public var display:Image;
		private var display_02:Image;
		public var countDownIndicator:OnlineCountDownIndicator;
		private var symbol:Image;
		
		//Selector Nums
		public var currentNumOfSavedTiles:int = 1;
		private var maxNumOfSavedTiles:int = 2;
		public var currentNum:int = 0;
		private var numNeeded:int;
		
		private var numNeededForLevel_02Unlock:int = 4;
		private var numNeededForLevel_03Unlock:int = 8;
		private var numNeededForLevel_04Unlock:int = 6;
		
		//History
		public var savedTilesStore:Array = [];
		public var numStore:Array = [];
		
		//Tutorial
		private var whiteFlash:BW_Selector;
		
		// resizing
		private var originalScale:Number;
		private var originalLoc:Point;
		
		public function OnlineCellSelector(type:int, owner:OnlinePlayer, onlineGameManager:OnlineGameManager)
		{
			super();
			
			this.type = type;
			this.owner = owner;
			this.onlineGameManager = onlineGameManager;
			
			switch(type)
			{
				case 0:
					switch(owner.playerTeam)
					{
						case 0:
							//Green Dot
							display = new Image(Main.assets.getTexture("Green_Stage01"));
							break;
						case 1:
							//Red Dot
							display = new Image(Main.assets.getTexture("Red_Stage01"));
							break;
						case 2:
							//Blue Dot
							display = new Image(Main.assets.getTexture("Blue_Stage01"));
							break;
						case 3:
							//Yellow Dot
							display = new Image(Main.assets.getTexture("Yellow_Stage01"));
							break;
					}
					break;
				case 1:
					
					numNeeded = numNeededForLevel_02Unlock;
					
					symbol = new Image(Main.assets.getTexture("CircleSymbol"));
					countDownIndicator = new OnlineCountDownIndicator(owner, this, numNeeded, symbol);
					
					switch(owner.playerTeam)
					{
						case 0:
							//Green Circle
							display = new Image(Main.assets.getTexture("Green_Stage02"));
							display_02 = new Image(Main.assets.getTexture("Green_Stage02"));
							break;
						case 1:
							//Red Circle
							display = new Image(Main.assets.getTexture("Red_Stage02"));
							display_02 = new Image(Main.assets.getTexture("Red_Stage02"));
							break;
						case 2:
							//Blue Circle
							display = new Image(Main.assets.getTexture("Blue_Stage02"));
							display_02 = new Image(Main.assets.getTexture("Blue_Stage02"));
							break;
						case 3:
							//Yellow Circle
							display = new Image(Main.assets.getTexture("Yellow_Stage02"));
							display_02 = new Image(Main.assets.getTexture("Yellow_Stage02"));
							break;
					}
					break;
				case 2:
					
					numNeeded = numNeededForLevel_03Unlock;
					
					symbol = new Image(Main.assets.getTexture("SquareSymbol"));
					countDownIndicator = new OnlineCountDownIndicator(owner, this, numNeeded, symbol);
					
					switch(owner.playerTeam)
					{
						case 0:
							//Green Square
							display = new Image(Main.assets.getTexture("Green_Stage03"));
							display_02 = new Image(Main.assets.getTexture("Green_Stage03"));
							break;
						case 1:
							//Red Square
							display = new Image(Main.assets.getTexture("Red_Stage03"));
							display_02 = new Image(Main.assets.getTexture("Red_Stage03"));
							break;
						case 2:
							//Blue Square
							display = new Image(Main.assets.getTexture("Blue_Stage03"));
							display_02 = new Image(Main.assets.getTexture("Blue_Stage03"));
							break;
						case 3:
							//Yellow Square
							display = new Image(Main.assets.getTexture("Yellow_Stage03"));
							display_02 = new Image(Main.assets.getTexture("Yellow_Stage03"));
							break;
					}
					break;
				case 3:
					//Virus
					maxNumOfSavedTiles = 1;
					numNeeded = numNeededForLevel_03Unlock;
					
					switch(owner.playerTeam)
					{
						case 0:
							symbol = new Image(Main.assets.getTexture("Green_Virus"));
							display = new MovieClip(Main.assets.getTextures("Green_Virus"));
							display_02 = new MovieClip(Main.assets.getTextures("Green_Virus"));
							break
						case 1:
							symbol = new Image(Main.assets.getTexture("Red_Virus"));
							display = new MovieClip(Main.assets.getTextures("Red_Virus"));
							display_02 = new MovieClip(Main.assets.getTextures("Red_Virus"));
							break
						case 2:
							symbol = new Image(Main.assets.getTexture("Blue_Virus"));
							display = new MovieClip(Main.assets.getTextures("Blue_Virus"));
							display_02 = new MovieClip(Main.assets.getTextures("Blue_Virus"));
							break
						case 3:
							symbol = new Image(Main.assets.getTexture("Yellow_virus"));
							display = new MovieClip(Main.assets.getTextures("Yellow_virus"));
							display_02 = new MovieClip(Main.assets.getTextures("Yellow_virus"));
							break
					}
					countDownIndicator = new OnlineCountDownIndicator(owner, this, numNeeded, symbol);
					break
			}
			
			display.pivotX = display.width/2;
			display.pivotY = display.height/2;
			addChild(display);
			if(type == 1 || type == 2)
			{
				display.x = PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
			}
			
			if(display_02 != null)
			{
				display_02.pivotX = display_02.width/2;
				display_02.pivotY = display_02.height/2;
				addChild(display_02);
				display_02.x = PathogenTest.HD_Multiplyer * -30 / PathogenTest.scaleFactor;
				display_02.visible = false;
			}
			
			if(countDownIndicator != null)
			{
				addChild(countDownIndicator);
				countDownIndicator.x = PathogenTest.HD_Multiplyer * -30 / PathogenTest.scaleFactor;
				
				if(type == 1 || type == 2)
				{
					countDownIndicator.visible = true;
				}
				else
				{
					countDownIndicator.visible = false;
				}
			}
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		private function onTouch(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && !onlineGameManager.moveUnderway && !onlineGameManager.onlineGameLoading)
			{
				if(currentNumOfSavedTiles > 0)
					onlineGameManager.cellSelectorHasBeenClicked(this);
				
				if(whiteFlash)
					removeWhiteFlash();
				
				switch(type)
				{
					case 0:
						SoundController.playCellSelector1();
						break;
					case 1:
						SoundController.playCellSelector2();
						break;
					case 2:
						SoundController.playCellSelector3();
						break;
					case 3:
						SoundController.playCellSelector4();
						break;
				}
			}
		}
		public function incrementLvl():void
		{	
			if (currentNumOfSavedTiles != maxNumOfSavedTiles)
			{
				countDownIndicator.visible = true;
				
				if (currentNumOfSavedTiles == 1)
				{
					countDownIndicator.x = PathogenTest.HD_Multiplyer * -30 / PathogenTest.scaleFactor;
				}
				
				currentNum++;
				
				if (currentNum == numNeeded)
				{
					currentNumOfSavedTiles++;
					currentNum = 0;
					createWhiteFlash();
				}
				
				countDownIndicator.changeState();
			}
		}
		public function alignCells():void
		{
			if (display_02)
			{
				display_02.scaleX = 1.0;
				display_02.scaleY = 1.0;
				display_02.alpha = 1.0;
			}
			
			if (display)
			{
				display.scaleX = 1.0;
				display.scaleY = 1.0;
				display.alpha = 1.0;
			}
			//trace("alignCells for type " + type + " currentSaved = " + currentNumOfSavedTiles);
			
			if (currentNumOfSavedTiles == 0)
			{
				display.visible = false;
				display_02.visible = false;
				if (whiteFlash)
					whiteFlash.x = 0;
				if (countDownIndicator != null)
				{
					countDownIndicator.visible = true;
					countDownIndicator.x = 0;
				}
			}
			else if (currentNumOfSavedTiles == 1)
			{
				display.visible = true;
				display_02.visible = false;
				if (countDownIndicator != null)
					countDownIndicator.visible = false;
				display.x = 0;
				if (currentNum > 0)
				{
					//display_02.visible = true;
					display_02.x = PathogenTest.HD_Multiplyer * -30 / PathogenTest.scaleFactor;
					display.x = PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
					if (whiteFlash)
						whiteFlash.x = display.x;
					if (countDownIndicator != null)
					{
						countDownIndicator.visible = true;
						countDownIndicator.x = PathogenTest.HD_Multiplyer * -30 / PathogenTest.scaleFactor;
					}
				}
			}
			else if (currentNumOfSavedTiles == 2)
			{
				display_02.visible = true;
				display_02.x = PathogenTest.HD_Multiplyer * -30 / PathogenTest.scaleFactor;
				display.x = PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
				if (whiteFlash)
					whiteFlash.x = display.x;
				if (countDownIndicator != null)
					countDownIndicator.visible = false;
			}
		}
		public function resetSelectedTile():void
		{
			//removeSaveTile();
			currentNumOfSavedTiles--;
			alignCells();
			countDownIndicator.changeState();
			removeWhiteFlash();
		}
		public function rememberState():void
		{	
			numStore.push(currentNum);
			savedTilesStore.push(currentNumOfSavedTiles);
		}
		public function restoreLastState(playerNum:int, addTile:Boolean):void
		{
			//if (type == 1 || type == 2)
			//trace("restoring last state for " + type + " numStore.length = " + numStore.length + " last element = " + numStore[numStore.length - 1]);
			
			var lastCurrentNum:int = currentNum;
			var lastCurrentSavedTiles:int = currentNumOfSavedTiles;
			
			if (type != 0)
			{
				if (numStore.length > 0)
				{
					currentNum = numStore.pop();
					currentNumOfSavedTiles = savedTilesStore.pop();
				}
				else
				{
					if (playerNum == 0)
					{
						currentNum = 1;
						currentNumOfSavedTiles = 1;
					}
					else
					{
						currentNum = 0;
						currentNumOfSavedTiles = 1;
					}
				}
				
				if (addTile)
					currentNumOfSavedTiles++;
				
				alignCells();
				countDownIndicator.changeState();
			}
			
			if(whiteFlash)
				removeWhiteFlash();
		}
		public function restoreState(whenInTime:int):void
		{
			//trace("restoreState");
			
			if (type != 0)
			{
				if (numStore.length > 0)
				{	
					currentNum = numStore[whenInTime];
					currentNumOfSavedTiles = savedTilesStore[whenInTime];
					countDownIndicator.changeState();
				}
			}
		}
		public function popCounts():void
		{
			numStore.pop();
			savedTilesStore.pop();
		}
		public function scaleAndFadeWhenClicked():void
		{
			if(Main.sAnimationsOn)
			{
				var tween:Tween = new Tween(display, SCALE_AND_FADE_TIME);
				tween.fadeTo(0.0);
				tween.scaleTo(0.0);
				Starling.juggler.add(tween);
			}
			
			if(whiteFlash)
				removeWhiteFlash();
		}
		
		public function getCurrentNum():int
		{
			return currentNum;
		}
		
		public function getCurrentNumOfSavedTiles():int
		{
			return currentNumOfSavedTiles;
		}
		
		public function getType():int
		{
			return type;
		}
		
		public function getNumNeeded():int
		{
			return numNeeded;
		}
		public function createWhiteFlash():void
		{
			var whiteCircle:Image = new Image(Main.assets.getTexture("White_Circle"));
			
			whiteFlash = new BW_Selector(0,0,whiteCircle, false, .8, 1);
			addChild(whiteFlash);
			whiteFlash.x = display.x;
			
			if(currentNum == 0 && currentNumOfSavedTiles == 1)
				whiteFlash.x = 0;
		}
		public function removeWhiteFlash():void
		{
			if(whiteFlash)
			{
				whiteFlash.destroy();
				whiteFlash.dispose();
				removeChild(whiteFlash);
				whiteFlash = null;
			}
		}
		
		public function rememberOriginalSizeAndPlace():void
		{
			if (!originalLoc)
				originalLoc = new Point();
			
			originalScale = scaleX;
			originalLoc.x = x;
			originalLoc.y = y;
		}
		
		public function increaseSize():void
		{
			scaleX = scaleX * 1.3;
			scaleY = scaleY * 1.3;
		}
		
		public function adjustXPosition(adjustSign:int):void
		{
			x += 10 * PathogenTest.scaleFactor * adjustSign;
		}
		
		public function adjustYPosition(adjustSign:int, adjustAmount:int):void
		{
			y += adjustAmount * PathogenTest.scaleFactor * adjustSign;
		}
		
		public function revertToOriginalSizeAndPlace():void
		{
			scaleX = originalScale;
			scaleY = originalScale;
			x = originalLoc.x;
			y = originalLoc.y;
		}
		
		public function GarbageCollection():void
		{
			onlineGameManager = null;
			
			removeEventListener(TouchEvent.TOUCH, onTouch);
			
			removeWhiteFlash();
			
			if(countDownIndicator)
			{
				countDownIndicator.garbageCollection();
				countDownIndicator.dispose();
				removeChild(countDownIndicator);
				countDownIndicator = null;
			}
			if(display)
			{
				display.dispose();
				removeChild(display);
				display = null;
			}
			if(display_02)
			{
				display_02.dispose();
				removeChild(display_02);
				display_02 = null;
			}
			if(symbol)
			{
				symbol.dispose();
				removeChild(symbol);
				symbol = null;
			}
			owner = null;
			
			savedTilesStore = [];
			numStore = [];
		}
	}
}