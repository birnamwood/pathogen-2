package OnlinePlay
{
	import FloxConnect.PlayerEntity;

	public class OnlinePlayerRankData
	{
		private var _id:String;
		private var _nickname:String;
		private var _numCellsCreated:int;
		private var _numCellsDestroyed:int;
		private var _percentBoardOwned:Number;
		private var _win:Boolean;
		private var _lose:Boolean;
		private var _tie:Boolean;
		private var _status:int;
		private var _playerEntity:PlayerEntity;
		private var _avgOpponentScore:int;
		private var _setLeague:Boolean;
		
		public function OnlinePlayerRankData(id:String, nickname:String, numCellsCreated:int, numCellsDestroyed:int, percentBoardOwned:Number, win:Boolean, lose:Boolean, tie:Boolean)
		{
			_id = id;
			_nickname = nickname;
			_numCellsCreated = numCellsCreated;
			_numCellsDestroyed = numCellsDestroyed;
			_percentBoardOwned = percentBoardOwned;
			_win = win;
			_lose = lose;
			_tie = tie;
			_playerEntity = null;
			_setLeague = false;
		}
		
		public function get id():String { return _id; }
		public function get nickname():String { return _nickname; }
		public function get numCellsCreated():int { return _numCellsCreated; }
		public function get numCellsDestroyed():int { return _numCellsDestroyed; }
		public function get percentBoardOwned():Number { return _percentBoardOwned; }
		public function get win():Boolean { return _win; }
		public function get lose():Boolean { return _lose; }
		public function get tie():Boolean { return _tie; }
		public function get status():int { return _status; }
		public function get playerEntity():PlayerEntity { return _playerEntity; }
		public function get avgOpponentScore():int { return _avgOpponentScore; }
		public function get setLeague():Boolean { return _setLeague; }
		
		public function set nickname(nickname:String):void { _nickname = nickname; }
		public function set win(win:Boolean):void { _win = win; }
		public function set lose(lose:Boolean):void { _lose = lose; }
		public function set tie(tie:Boolean):void { _tie = tie; }
		public function set status(status:int):void { _status = status; }
		public function set playerEntity(playerEntity:PlayerEntity):void { _playerEntity = playerEntity; }
		public function set avgOpponentScore(avgOpponentScore:int):void { _avgOpponentScore = avgOpponentScore; }
		public function set setLeague(setLeague:Boolean):void { _setLeague = setLeague; }
	}
}