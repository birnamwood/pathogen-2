package OnlinePlay
{
	import FloxConnect.EntityManager;
	
	import Maps.Map;
	import Maps.MapInfo;
	import Maps.MapPreviewCell;
	
	import UI.Loader;
	import UI.Main;
	
	import Utils.BW_Button;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class OnlineGamePreview extends Sprite
	{
		//Parent Class
		private var _onlinePlay:OnlinePlay;
		
		//Game Information
		private var _gameID:String;
		private var _mapIndex:int;
		private var _playerNicknames:Array;
		private var _playerColors:Array;
		private var _status:int;
		private var _sortValue:int;
		private var _previewCellHolder:Array = [];
		private var _playerColorImages:Array;
		private var _playerNicknamesTextfields:Array;
		
		//Buttons
		private var _acceptInviteButton:BW_Button;
		private var _rejectInviteButton:BW_Button;
		private var _enterGameButton:BW_Button;
		private var _forfeitGameButton:BW_Button;
		private var _removeGameButton:BW_Button;
		private var _yesButton:BW_Button;
		private var _noButton:BW_Button;
		
		//Scale 9's
		private var _background:Scale9Image;
		private var _header:Scale9Image;
		private var _subBackground:Scale9Image;
		
		//TextFields
		private var _mapName:TextField;
		private var _statusText:TextField;
		
		//Holders
		private var _previewHolder:Sprite;
		private var _surrenderScreen:Sprite;
		private var _removeScreen:Sprite;

		public function OnlineGamePreview(onlinePlay:OnlinePlay, mapIndex:int, status:int, gameID:String, playerNicknames:Array, playerColors:Array)
		{
			super();
			
			_onlinePlay = onlinePlay;
			_gameID = gameID;
			_playerNicknames = playerNicknames;
			_playerColors = playerColors;

			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			var texture:Texture = Main.assets.getTexture("scale9_White");
			var textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			_background = new Scale9Image(textures);
			_background.width = PathogenTest.wTenPercent * 4;
			_background.height = PathogenTest.hTenPercent * 6;
			_background.pivotX = _background.width/2;
			_background.pivotY = _background.height/2;
			addChild(_background);
			_background.color = Color.BLACK;
			_background.alpha = .5
				
			_header = new Scale9Image(textures);
			_header.width = PathogenTest.wTenPercent * 4;
			_header.height = PathogenTest.hTenPercent * .7;
			_header.pivotX = _header.width/2;
			addChild(_header);
			_header.color = BW_Button.BACKGROUND_UP;
			_header.y = -_background.height/2;
			
			var statusString:String;
			_status = status;
			if (_status == EntityManager.GAME_STATUS_WAITING)
			{
				if (_onlinePlay.main.entityManager.amIFirstPlayer(_gameID))
				{
					if (_onlinePlay.main.entityManager.hasPlayerTakenATurn(_gameID))
						statusString = "- Their Turn -";
					else
						statusString = "- Your Turn -";
				}
				else
					statusString = "- Invitation -";
			}
			else if (_status == EntityManager.GAME_STATUS_BEGUN)
			{
				if (_onlinePlay.main.entityManager.isItMyTurn(_gameID))
				{
					statusString = "- Your Turn -";
					_sortValue = 0;
				}
				else
				{
					statusString = "- Their Turn -";
					_sortValue = 1;
				}
			
			}
			else if (_status == EntityManager.GAME_STATUS_COMPLETE)
				statusString = "- Game Over -";
			else if (_status == EntityManager.GAME_STATUS_REJECTED)
				statusString = "- Invite Rejected -";
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor;
			_statusText = new TextField(_background.width * 0.6, PathogenTest.hTenPercent, statusString, "Questrial", fontSize, Color.WHITE);
			_statusText.pivotX = _statusText.width / 2;
			_statusText.pivotY = _statusText.height / 2;
			_statusText.x = 0;
			_statusText.y = -(PathogenTest.hTenPercent * 2.6);
			addChild(_statusText);

			_mapIndex = mapIndex;
			createPreview();
			
			// add player colors and text
			fontSize = PathogenTest.HD_Multiplyer * 22 / PathogenTest.scaleFactor;
			_playerNicknamesTextfields = [];
			_playerColorImages = [];
			var playerText:TextField;
			var playerColor:Image;
			var position:Point;
			for (var i:int=0; i<_playerNicknames.length; i++)
			{
				if (i == 0)
					position = new Point(-(PathogenTest.wTenPercent * 1.6), (_previewHolder.y + _previewHolder.height * .6));
				else if (i == 1)
					position = new Point(PathogenTest.wTenPercent * 0.25, (_previewHolder.y + _previewHolder.height * .6));
				else if (i == 2)
					position = new Point(-(PathogenTest.wTenPercent * 1.6), (_previewHolder.y + _previewHolder.height * .8));
				else if (i == 3)
					position = new Point(PathogenTest.wTenPercent * 0.25, (_previewHolder.y + _previewHolder.height * .8));
				
				if (_playerColors[i] == 0)
					playerColor = new Image(Main.assets.getTexture("Green_Stage01"));
				else if (_playerColors[i] == 1)
					playerColor = new Image(Main.assets.getTexture("Red_Stage01"));
				else if (_playerColors[i] == 2)
					playerColor = new Image(Main.assets.getTexture("Blue_Stage01"));
				else if (_playerColors[i] == 3)
					playerColor = new Image(Main.assets.getTexture("Yellow_Stage01"));

				playerColor.pivotX = playerColor.width / 2;
				playerColor.pivotY = playerColor.height / 2;
				playerColor.scaleX = 0.9;
				playerColor.scaleY = 0.9;
				playerColor.x = position.x;
				playerColor.y = position.y;
				addChild(playerColor);
				_playerColorImages.push(playerColor);
				
				playerText = new TextField(_background.width / 3, PathogenTest.hTenPercent / 2, _playerNicknames[i], "Questrial", fontSize, Color.WHITE);
				playerText.pivotX = playerText.width / 2;
				playerText.pivotY = playerText.height / 2;
				playerText.x = (playerColor.width * 2.0) + position.x;
				playerText.y = position.y;
				addChild(playerText);
				_playerNicknamesTextfields.push(playerText);
			}

			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		public function createPreview():void
		{
			var mapData:Array;
			var tileCounter:int = 0;
			var gameBoardWidth:int;
			var gameBoardHeight:int;
			var startingPos:Point = new Point(0,0);
			var tileGap:int;
			var tileScaleX:Number;
			var tileScaleY:Number;

			var map:Map = _onlinePlay.main.mapManager.getMap(_mapIndex);
			//trace("got map " + map.name + " " + map.size + " " + map.layout);
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 30 / PathogenTest.scaleFactor;
			_mapName = new TextField(_background.width / 2, PathogenTest.hTenPercent, map.name, "Questrial", fontSize, Color.WHITE);
			_mapName.pivotX = _mapName.width / 2;
			_mapName.pivotY = _mapName.height / 2;
			_mapName.x = 0;
			_mapName.y = -(PathogenTest.hTenPercent * 2.0);
			addChild(_mapName);
			
			//Determine Board Length/Height
			if (map.size == 0)
			{
				gameBoardHeight = 14;
				gameBoardWidth = 16;
				tileScaleX = .9;
				tileScaleY = .9;
			}
			else if (map.size == 1)
			{
				gameBoardHeight = 18;
				gameBoardWidth = 20;
				tileScaleX = .7;
				tileScaleY = .7;
			}
			else
			{
				gameBoardHeight = 22;
				gameBoardWidth = 24;
				tileScaleX = .55;
				tileScaleY = .55;
			}
			
			_previewHolder = new Sprite();
			addChild(_previewHolder);
			
			//Create a blank board
			for (var i:int = 0; i < gameBoardWidth; i++)
			{
				for (var j:int = 0; j < gameBoardHeight; j++)
				{
					var previewCell:MapPreviewCell = new MapPreviewCell();
					_previewHolder.addChild(previewCell);
					_previewCellHolder.push(previewCell);

					previewCell.changeColor(map.layout[tileCounter]);
					
					tileGap = (previewCell.width * 1) * tileScaleX;
					
					previewCell.x = startingPos.x + (tileGap * i);
					previewCell.y = startingPos.y + (tileGap * j);
					
					previewCell.scaleX = tileScaleX;
					previewCell.scaleY = tileScaleY;
					
					tileCounter++;
				}
			}
			
			_previewHolder.pivotX = _previewHolder.width/2;
			_previewHolder.pivotY = _previewHolder.height/2;
			_previewHolder.x -= PathogenTest.wTenPercent * .1;
			_previewHolder.y -= PathogenTest.hTenPercent * .2;
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_previewHolder.y = 0;
			
			_previewHolder.touchable = false;
			_previewHolder.flatten();
		}
		
		private function onTouch(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if (!_onlinePlay.sliding)
				{
					_onlinePlay.revertOtherMapPreviewsToUnselected(this);
					_onlinePlay.removeRefreshers();
					
					removeEventListener(TouchEvent.TOUCH, onTouch);
					createSubMenu();
				}
			}
		}
		
		private function createSubMenu():void
		{
			// create a transparent background
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			_subBackground = new Scale9Image(textures);
			addChild(_subBackground);
			_subBackground.width = _background.width;
			_subBackground.height = _background.height;
			_subBackground.pivotX = _subBackground.width/2;
			_subBackground.pivotY = _subBackground.height/2;
			_subBackground.alpha = .75;
			_subBackground.color = Color.BLACK;
			
			// create buttons
			// if invite
			if (_status == EntityManager.GAME_STATUS_WAITING)
			{
				if (_onlinePlay.main.entityManager.amIFirstPlayer(_gameID))
				{
					if (Main.isPhone())
					//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
						_enterGameButton = new BW_Button(2.5, 1.0, "Enter Game", 36);
					else
						_enterGameButton = new BW_Button(2.5, .75, "Enter Game", 36);
					_enterGameButton.y = -_enterGameButton.height * .75;
					addChild(_enterGameButton);
					_enterGameButton.addEventListener(TouchEvent.TOUCH, onTouchEnterGame);
					
					if (Main.isPhone())
					//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
						_removeGameButton = new BW_Button(2.5,1.0, "Remove Game", 36);
					else
						_removeGameButton = new BW_Button(2.5, .75, "Remove Game", 36);
					_removeGameButton.y = _removeGameButton.height * .75 ;
					addChild(_removeGameButton);
					_removeGameButton.addEventListener(TouchEvent.TOUCH, onTouchRemoveGame);
				}
				else
				{
					if (Main.isPhone())
					//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
						_acceptInviteButton = new BW_Button(2.5, 1.0, "Accept Invite", 36);
					else
						_acceptInviteButton = new BW_Button(2.5, .75, "Accept Invite", 36);
					_acceptInviteButton.y = -_acceptInviteButton.height * .75;
					addChild(_acceptInviteButton);
					_acceptInviteButton.addEventListener(TouchEvent.TOUCH, onTouchAcceptInvite);
					
					if (Main.isPhone())
					//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
						_rejectInviteButton = new BW_Button(2.5, 1.0, "Reject Invite", 36);
					else
						_rejectInviteButton = new BW_Button(2.5, .75, "Reject Invite", 36);
					_rejectInviteButton.y = _rejectInviteButton.height * .75;
					addChild(_rejectInviteButton);
					_rejectInviteButton.addEventListener(TouchEvent.TOUCH, onTouchRejectInvite);
				}
			}
			else 
			{
				var enterGameStr:String;
				
				if (_status == EntityManager.GAME_STATUS_COMPLETE)
					enterGameStr = "View Results";
				else
					enterGameStr = "Enter Game";
				
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_enterGameButton = new BW_Button(2.5, 1.0, enterGameStr, 36);
				else
					_enterGameButton = new BW_Button(2.5, 0.75, enterGameStr, 36);
				_enterGameButton.y = -_enterGameButton.height * .75;
				addChild(_enterGameButton);
				_enterGameButton.addEventListener(TouchEvent.TOUCH, onTouchEnterGame);
				
				if (_status != EntityManager.GAME_STATUS_COMPLETE && _onlinePlay.main.entityManager.haveAllPlayersJoinedGame(_gameID))
				{
					if (Main.isPhone())
					//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
						_forfeitGameButton = new BW_Button(2.5, 1.0, "Forfeit Game", 36);
					else
						_forfeitGameButton = new BW_Button(2.5, .75, "Forfeit Game", 36);
					_forfeitGameButton.y = _forfeitGameButton.height * .75;
					addChild(_forfeitGameButton);
					_forfeitGameButton.addEventListener(TouchEvent.TOUCH, onTouchForfeitGame);
				}
				else
				{
					if (Main.isPhone())
					//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
						_removeGameButton = new BW_Button(2.5, 1.0, "Remove Game", 36);
					else
						_removeGameButton = new BW_Button(2.5, .75, "Remove Game", 36);
					_removeGameButton.y = _removeGameButton.height * .75;
					addChild(_removeGameButton);
					_removeGameButton.addEventListener(TouchEvent.TOUCH, onTouchRemoveGame);
				}
			}
		}
		
		private function onTouchAcceptInvite(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this);
			if(touch.phase == TouchPhase.ENDED)
			{
				_onlinePlay.main.entityManager.pushInviteGameToActiveList(_gameID);
			}
		}
		
		private function onTouchRejectInvite(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this);
			if(touch.phase == TouchPhase.ENDED)
			{
				_onlinePlay.main.entityManager.rejectInvite(_gameID);
				_onlinePlay.main.returnToOnlineGamesList();
			}
		}
		
		private function onTouchEnterGame(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this);
			if(touch.phase == TouchPhase.ENDED)
			{
				_onlinePlay.main.createGameLoader();
				
				//_onlinePlay.main.entityManager.setCurrentGameIndex(_gameID);
				//_onlinePlay.loadGame(_gameID);
				Starling.juggler.delayCall(enterGame, 0.5);
			}
		}
		
		private function enterGame():void
		{
			_onlinePlay.main.entityManager.setCurrentGameIndex(_gameID);
			_onlinePlay.loadGame(_gameID);
		}
		
		private function onTouchForfeitGame(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this);
			if(touch.phase == TouchPhase.ENDED)
			{
				_enterGameButton.removeEventListener(TouchEvent.TOUCH, onTouchEnterGame);
				_forfeitGameButton.removeEventListener(TouchEvent.TOUCH, onTouchForfeitGame);
				addSurrenderScreen();
			}
		}
		
		private function onTouchRemoveGame(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this);
			if(touch.phase == TouchPhase.ENDED)
			{
				createAreYouSureRemove();
			}
		}
		
		private function createAreYouSureRemove():void
		{
			_removeScreen = new Sprite();
			addChild(_removeScreen);
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var background:Scale9Image = new Scale9Image(textures);
			_removeScreen.addChild(background);
			background.width = _background.width;
			background.height = _background.height;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.color = Color.BLACK;

			var text:TextField = new TextField(background.width, PathogenTest.hTenPercent * 1.5, "Are you sure?" , "Dekar", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			text.pivotX = text.width/2;
			text.pivotY = text.height/2;
			_removeScreen.addChild(text);
			text.y = -PathogenTest.hTenPercent;
			
			// buttons
			_yesButton = new BW_Button(2.0, .75, "Yes", 36);
			_yesButton.y = text.y + (text.height * .8);
			_removeScreen.addChild(_yesButton);
			_yesButton.addEventListener(TouchEvent.TOUCH, onTouchRemoveYes);
			
			_noButton = new BW_Button(2.0, .75, "No", 36);
			_noButton.y = _yesButton.y + (_yesButton.height * 1.5)
			_removeScreen.addChild(_noButton);
			_noButton.addEventListener(TouchEvent.TOUCH, onTouchRemoveNo);
		}
		private function onTouchRemoveYes($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if(touch)
			{
				_onlinePlay.main.entityManager.removeGame(_gameID);
				_yesButton.removeEventListener(TouchEvent.TOUCH, onTouchRemoveYes);
				_noButton.removeEventListener(TouchEvent.TOUCH, onTouchRemoveNo);
			}
		}
		private function onTouchRemoveNo($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if(touch)
			{
				removeRemoveScreen();
			}
		}
		private function removeRemoveScreen():void
		{
			_yesButton.removeEventListener(TouchEvent.TOUCH, onTouchRemoveYes);
			_yesButton.dispose();
			_yesButton.destroy();
			_removeScreen.removeChild(_yesButton);
			_yesButton = null;
			
			_noButton.removeEventListener(TouchEvent.TOUCH, onTouchRemoveNo);
			_noButton.dispose();
			_noButton.destroy();
			_removeScreen.removeChild(_noButton);
			_noButton = null;
			
			_removeScreen.dispose();
			removeChild(_removeScreen);
			_removeScreen = null;
		}
		private function addSurrenderScreen():void
		{
			_surrenderScreen = new Sprite();
			addChild(_surrenderScreen);
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var background:Scale9Image = new Scale9Image(textures);
			_surrenderScreen.addChild(background);
			background.width = _background.width;
			background.height = _background.height;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.color = Color.BLACK;
			
			var text:TextField = new TextField(background.width, PathogenTest.hTenPercent * 1.5, "Are you sure?" + "\n" + "This will count as a loss.", "Dekar", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			text.pivotX = text.width/2;
			text.pivotY = text.height/2;
			_surrenderScreen.addChild(text);
			text.y = -PathogenTest.hTenPercent;
			
			// buttons
			_yesButton = new BW_Button(2.0, .75, "Yes", 36);
			_yesButton.y = text.y + (text.height * .8);
			_surrenderScreen.addChild(_yesButton);
			_yesButton.addEventListener(TouchEvent.TOUCH, onTouchForfeitYes);
			
			_noButton = new BW_Button(2.0, .75, "No", 36);
			_noButton.y = _yesButton.y + (_yesButton.height * 1.5)
			_surrenderScreen.addChild(_noButton);
			_noButton.addEventListener(TouchEvent.TOUCH, onTouchForfeitNo);
		}
		
		private function onTouchForfeitYes(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this);
			if(touch.phase == TouchPhase.ENDED)
			{
				_yesButton.removeEventListener(TouchEvent.TOUCH, onTouchForfeitYes);
				_noButton.removeEventListener(TouchEvent.TOUCH, onTouchForfeitNo);
				_onlinePlay.main.entityManager.surrenderGameByID(_gameID);
				_onlinePlay.main.entityManager.setCurrentGameIndex(_gameID);
				_onlinePlay.loadGame(_gameID);
				//_onlinePlay.main.returnToOnlineGamesList();
			}
		}
		
		private function onTouchForfeitNo(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this);
			if(touch.phase == TouchPhase.ENDED)
			{
				removeSurrenderScreen();
				_enterGameButton.addEventListener(TouchEvent.TOUCH, onTouchEnterGame);
				if (_status != EntityManager.GAME_STATUS_COMPLETE)
					_forfeitGameButton.addEventListener(TouchEvent.TOUCH, onTouchForfeitGame);
			}
		}
		
		private function removeSurrenderScreen():void
		{
			_yesButton.removeEventListener(TouchEvent.TOUCH, onTouchForfeitYes);
			_yesButton.dispose();
			_yesButton.destroy();
			_surrenderScreen.removeChild(_yesButton);
			_yesButton = null;
			
			_noButton.removeEventListener(TouchEvent.TOUCH, onTouchForfeitNo);
			_noButton.dispose();
			_noButton.destroy();
			_surrenderScreen.removeChild(_noButton);
			_noButton = null;
			
			_surrenderScreen.dispose();
			removeChild(_surrenderScreen);
			_surrenderScreen = null;
		}
		
		public function revertToUnselected():void
		{
			if (_acceptInviteButton)
			{
				_acceptInviteButton.destroy();
				_acceptInviteButton.removeEventListener(TouchEvent.TOUCH, onTouchAcceptInvite);
				removeChild(_acceptInviteButton);
				_acceptInviteButton = null;
			}
			if (_rejectInviteButton)
			{
				_rejectInviteButton.destroy();
				_rejectInviteButton.removeEventListener(TouchEvent.TOUCH, onTouchRejectInvite);
				removeChild(_rejectInviteButton);
				_rejectInviteButton = null;
			}
			if (_enterGameButton)
			{
				_enterGameButton.destroy();
				_enterGameButton.removeEventListener(TouchEvent.TOUCH, onTouchEnterGame);
				removeChild(_enterGameButton);
				_enterGameButton = null;
			}
			if (_forfeitGameButton)
			{
				_forfeitGameButton.destroy();
				_forfeitGameButton.removeEventListener(TouchEvent.TOUCH, onTouchForfeitGame);
				removeChild(_forfeitGameButton);
				_forfeitGameButton = null;
			}
			if (_removeGameButton)
			{
				_removeGameButton.destroy();
				_removeGameButton.removeEventListener(TouchEvent.TOUCH, onTouchRemoveGame);
				removeChild(_removeGameButton);
				_removeGameButton = null;
			}
			
			if(_surrenderScreen)
				removeSurrenderScreen();
			
			if(_removeScreen)
				removeRemoveScreen();
			
			if (_subBackground)
			{
				_subBackground.dispose();
				removeChild(_subBackground);
				_subBackground = null;
			}
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		public function garbageCollection():void
		{
			var i:int;
			
			// remove subMenu
			if (_acceptInviteButton)
			{
				_acceptInviteButton.destroy();
				_acceptInviteButton.removeEventListener(TouchEvent.TOUCH, onTouchAcceptInvite);
				removeChild(_acceptInviteButton);
				_acceptInviteButton = null;
			}
			if (_rejectInviteButton)
			{
				_rejectInviteButton.destroy();
				_rejectInviteButton.removeEventListener(TouchEvent.TOUCH, onTouchRejectInvite);
				removeChild(_rejectInviteButton);
				_rejectInviteButton = null;
			}
			if (_enterGameButton)
			{
				_enterGameButton.destroy();
				_enterGameButton.removeEventListener(TouchEvent.TOUCH, onTouchEnterGame);
				removeChild(_enterGameButton);
				_enterGameButton = null;
			}
			if (_forfeitGameButton)
			{
				_forfeitGameButton.destroy();
				_forfeitGameButton.removeEventListener(TouchEvent.TOUCH, onTouchForfeitGame);
				removeChild(_forfeitGameButton);
				_forfeitGameButton = null;
			}
			if (_removeGameButton)
			{
				_removeGameButton.destroy();
				_removeGameButton.removeEventListener(TouchEvent.TOUCH, onTouchRemoveGame);
				removeChild(_removeGameButton);
				_removeGameButton = null;
			}
			
			if(_surrenderScreen)
				removeSurrenderScreen();
			
			if(_removeScreen)
				removeRemoveScreen();
			
			if (_subBackground)
			{
				_subBackground.dispose();
				removeChild(_subBackground);
				_subBackground = null;
			}
			
			if (_header)
			{
				_header.dispose();
				removeChild(_header);
				_header = null;
			}
			
			if (_background)
			{
				_background.dispose();
				removeChild(_background);
				_background = null;
			}
			if (_mapName)
			{
				_mapName.dispose();
				removeChild(_mapName);
				_mapName = null;
			}
			if (_statusText)
			{
				_statusText.dispose();
				removeChild(_statusText);
				_statusText = null;
			}

			for (i=0; i<_playerNicknamesTextfields.length; i++)
			{
				removeChild(_playerNicknamesTextfields[i]);
				_playerNicknamesTextfields[i].dispose();
			}
			for (i=0; i<_playerColorImages.length; i++)
			{
				removeChild(_playerColorImages[i]);
				_playerColorImages[i].dispose();
			}
			
			if (_previewHolder)
			{
				_previewHolder.dispose();
				removeChild(_previewHolder);
				_previewHolder = null;
			}
			for (i=(_previewCellHolder.length - 1); i>=0; i--)
				_previewCellHolder[i].dispose();
			_previewCellHolder = [];
		}
		
		public function get sortValue():int { return _sortValue; }
	}
}