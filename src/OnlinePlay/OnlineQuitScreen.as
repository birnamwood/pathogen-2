package OnlinePlay
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	import flash.text.ReturnKeyLabel;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	
	public class OnlineQuitScreen extends BW_UI
	{
		//Parent Class
		private var _gameManager:OnlineGameManager;
		
		//Buttons
		private var _returnToGameButton:Utils.BW_Button
		private var _surrenderButton:Utils.BW_Button
		private var _backToGamesListButton:Utils.BW_Button
			
		public function OnlineQuitScreen(onlineGameManager:OnlineGameManager)
		{
			super();
			
			_gameManager = onlineGameManager; 
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			// add background and header
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			addChild(transBackground);
			transBackground.alpha = 0.75;
			
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 5;
			background.height = PathogenTest.hTenPercent * 4.25;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = 0.5;
			
			var header:Scale9Image = new Scale9Image(textures);
			addChild(header);
			header.width = PathogenTest.wTenPercent * 5;
			header.height = PathogenTest.hTenPercent;
			header.pivotX = header.width/2;
			header.y = -background.height/2;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
			var title:TextField = new TextField(header.width, header.height, "Quit", "Questrial", fontSize, Color.WHITE);
			title.pivotX = title.width/2;
			addChild(title);
			title.y = header.y;
			
			// quit screen buttons
			_returnToGameButton = new Utils.BW_Button(2.5, 0.75, "Return to Game", 32);
			_returnToGameButton.y = header.y + (_returnToGameButton.height * 2.25);
			addChild(_returnToGameButton);
			_returnToGameButton.addEventListener(TouchEvent.TOUCH, onTouchReturnToGame);
			
			_surrenderButton = new Utils.BW_Button(2.5, 0.75, "Surrender", 32);
			_surrenderButton.y = _returnToGameButton.y + (_surrenderButton.height * 1.25);
			addChild(_surrenderButton);
			
			if (_gameManager.isItMyTurn() && _gameManager.areAllPlayersJoined())
				_surrenderButton.addEventListener(TouchEvent.TOUCH, onTouchSurrender);
			else
				_surrenderButton.alpha = 0.5;
			
			if (_gameManager.isGameOver())
			{
				_surrenderButton.removeEventListener(TouchEvent.TOUCH, onTouchSurrender);
				_surrenderButton.alpha = 0.5;
			}
			
			_backToGamesListButton = new Utils.BW_Button(2.5, 0.75, "Back to Game List", 32);
			_backToGamesListButton.y = _surrenderButton.y + (_backToGamesListButton.height * 1.25);
			addChild(_backToGamesListButton);
			_backToGamesListButton.addEventListener(TouchEvent.TOUCH, onTouchBackToGamesList);
		}
		private function onTouchReturnToGame($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				removeListeners();
				startFadeOut();
			}
		}
		private function onTouchSurrender($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_gameManager.addSurrenderScreen();
			}
		}
		private function onTouchBackToGamesList($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				removeListeners();
				_gameManager.backToGamesList();
				startFadeOut()
			}
		}
		private function removeListeners():void
		{
			_returnToGameButton.removeEventListener(TouchEvent.TOUCH, onTouchReturnToGame);
			_surrenderButton.removeEventListener(TouchEvent.TOUCH, onTouchSurrender);
			_backToGamesListButton.removeEventListener(TouchEvent.TOUCH, onTouchBackToGamesList);
		}
		public override function death():void
		{
			_returnToGameButton.destroy();
			_returnToGameButton.dispose();
			removeChild(_returnToGameButton);
			_returnToGameButton = null;
			
			_surrenderButton.destroy();
			_surrenderButton.dispose();
			removeChild(_surrenderButton);
			_surrenderButton = null;
			
			_backToGamesListButton.destroy();
			_backToGamesListButton.dispose();
			removeChild(_backToGamesListButton);
			_backToGamesListButton = null;
			
			_gameManager.removeQuitScreen();
			_gameManager = null;
		}
	}
}