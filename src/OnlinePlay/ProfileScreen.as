package OnlinePlay
{
	import FloxConnect.EntityManager;
	import FloxConnect.GameEntity;
	import FloxConnect.InviteEntity;
	import FloxConnect.PlayerEntity;
	
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.display.Scale9Image;
	import feathers.events.FeathersEventType;
	import feathers.layout.VerticalLayout;
	import feathers.textures.Scale9Textures;
	
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.VertexBuffer3D;
	import flash.geom.Rectangle;
	
	import flashx.textLayout.elements.OverflowPolicy;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class ProfileScreen extends BW_UI
	{
		//Parent Class
		private var _onlinePlay:OnlinePlay;
		
		//Game List Vars
		private var _slider:ScrollContainer;
		private var _gamePreviews:Vector.<OnlineGamePreviewSmall>;
		private var _invitePreviews:Vector.<OnlineGamePreviewSmall>;
		private var _sliding:Boolean;
		
		//Background
		private var scale9Num:Number = 20/PathogenTest.scaleFactor;
		private const texture:Texture = Main.assets.getTexture("scale9_White");
		private const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
		private var leftSideBackground:Scale9Image
		private var playerBG:Scale9Image;
		
		//Player
		private var player:PlayerEntity
		private var playerLeague:String;
		
		//Buttons
		private var _casualButton:BW_Button;
		private var _rankedButton:BW_Button;
		
		//Editable Fields
		private var victories:TextField;
		private var losses:TextField;
		private var ties:TextField;
		private var cellsCreated:TextField;
		private var cellsDestroyed:TextField;
		private var playerNicknameText:TextField;
		private var playerLeagueText:TextField;
		
		//Icons
		private var playerIcon:Image;
		private var leagueIcon:Image;
		
		private var currentPage:String;
		
		private var _joinedLeagueScreen:JoinedLeagueScreen;
		
		public function ProfileScreen(onlinePlay:OnlinePlay)
		{
			super();
			
			_onlinePlay = onlinePlay;
			player = _onlinePlay.main.entityManager.currentPlayerEntity;
			
			setupProfile();
			setupGameList();
		}
		private function setupProfile():void
		{
			leftSideBackground = new Scale9Image(textures);
			leftSideBackground.width = PathogenTest.wTenPercent * 4.5;
			leftSideBackground.height = PathogenTest.hTenPercent * 7;
			leftSideBackground.pivotX = leftSideBackground.width / 2;
			leftSideBackground.pivotY = leftSideBackground.height / 2;
			leftSideBackground.color = 0x222245;
			//leftSideBackground.alpha = .5;
			leftSideBackground.x = PathogenTest.wTenPercent * 2.5;
			leftSideBackground.y = PathogenTest.stageCenter.y - (PathogenTest.hTenPercent * 0.5);
			addChild(leftSideBackground);
			
			_casualButton = new BW_Button(1.5, .75, "Custom", 30);
			addChildAt(_casualButton, 0);
			_casualButton.toggle = true;
			_casualButton.x = leftSideBackground.x - leftSideBackground.width/2 + (_casualButton.width/2);
			_casualButton.y = leftSideBackground.y - leftSideBackground.height/2 - (_casualButton.height * .3);
			_casualButton.addEventListener(TouchEvent.TOUCH, onTouchCasual);
			
			_rankedButton = new BW_Button(1.5, .75, "Ranked", 30);
			addChildAt(_rankedButton, 0);
			_rankedButton.toggle = true;
			_rankedButton.toggleDown();
			_rankedButton.x = _casualButton.x + (_casualButton.width/2) + (_rankedButton.width/2) + (_rankedButton.width * .1)
			_rankedButton.y = _casualButton.y;
			_rankedButton.addEventListener(TouchEvent.TOUCH, onTouchRanked);
			
			playerBG = new Scale9Image(textures);
			playerBG.width = PathogenTest.wTenPercent * 4.1;
			playerBG.height = PathogenTest.hTenPercent * 1.65;
			playerBG.pivotX = playerBG.width / 2;
			playerBG.pivotY = playerBG.height / 2;
			playerBG.color = Color.BLACK
			playerBG.x = leftSideBackground.x;
			playerBG.y = leftSideBackground.y - (playerBG.height * 1.5);
			addChild(playerBG);
			playerBG.alpha = 0.5;
			
			var playerIconBG:Scale9Image = new Scale9Image(textures);
			playerIconBG.width = PathogenTest.wTenPercent * 1.0;
			playerIconBG.height = PathogenTest.hTenPercent * 1.3;
			playerIconBG.pivotX = playerIconBG.width/2;
			playerIconBG.pivotY = playerIconBG.height / 2;
			playerIconBG.color = 0x222245;
			playerIconBG.x = playerBG.x - (playerBG.width * 0.35);
			playerIconBG.y = playerBG.y;
			addChild(playerIconBG);
			
			playerIcon = new Image(Main.assets.getTexture("UserIcon"));
			playerIcon.pivotX = playerIcon.width / 2;
			playerIcon.pivotY = playerIcon.height / 2;
			addChild(playerIcon);
			playerIcon.x = playerIconBG.x;
			playerIcon.y = playerIconBG.y;
			
			var playerRank:int = player.ranks[0] + 1;
			var leagueIconString:String = "UserIcon";
				
			switch(_onlinePlay.main.entityManager.getLeagueName(player.league2Player))
			{
				case "Bronze":
					playerLeague = "Bronze League";
					if (playerRank > 0 )
						playerLeague += " | Rank: " + playerRank;
					
					//Determine which Badge to show
					if(playerRank <= 10)
						leagueIconString = "Bronze03";
					else if (playerRank > 10 && playerRank <= 25)
						leagueIconString = "Bronze02";
					else
						leagueIconString = "Bronze01";
					
					break;
				
				case "Silver":
					playerLeague = "Silver League";
					if (playerRank > 0 )
						playerLeague += " | Rank: " + playerRank;
					
					//Determine which Badge to show
					if(playerRank <= 10)
						leagueIconString = "Silver03";
					else if (playerRank > 10 && playerRank <= 25)
						leagueIconString = "Silver02";
					else
						leagueIconString = "Silver01";
					
					break;
				
				case "Gold":
					playerLeague = "Gold League";
					if (playerRank > 0 )
						playerLeague += " | Rank: " + playerRank;
					
					//Determine which Badge to show
					if(playerRank <= 10)
						leagueIconString = "Gold03";
					else if (playerRank > 10 && playerRank <= 25)
						leagueIconString = "Gold02";
					else
						leagueIconString = "Gold01";
					
					break;
					
				default:
					if(player.getNumberOfGamesUntilLeague(EntityManager.GAME_TYPE_2_PLAYER) == 1)
						playerLeague = player.getNumberOfGamesUntilLeague(EntityManager.GAME_TYPE_2_PLAYER).toString() +  " Placement Match Remaining";
					else
						playerLeague = player.getNumberOfGamesUntilLeague(EntityManager.GAME_TYPE_2_PLAYER).toString() +  " Placement Matches Remaining";
					break;
			}
			
			leagueIcon = new Image(Main.assets.getTexture(leagueIconString));
			leagueIcon.pivotX = leagueIcon.width / 2;
			leagueIcon.pivotY = leagueIcon.height / 2;
			addChild(leagueIcon);
			leagueIcon.x = playerIconBG.x;
			leagueIcon.y = playerIconBG.y;
			leagueIcon.visible = false;
			
			if(leagueIconString != "UserIcon")
			{
				leagueIcon.scaleX = .6;
				leagueIcon.scaleY = .6;
			}
			
			var playerNickname:String = Main.sOnlineNickname;
			var playerNickFontSize:int = 48;
			if (playerNickname.length > 10 && playerNickname.length <= 15)
				playerNickFontSize = 38;
			else if (playerNickname.length > 15 && playerNickname.length <= 20)
				playerNickFontSize = 30;
			else if (playerNickname.length > 20)
			{
				playerNickname = playerNickname.substr(0, 20);
				playerNickFontSize = 30;
			}
			
			playerNicknameText = new TextField(PathogenTest.wTenPercent * 2.75, PathogenTest.hTenPercent * 0.7, playerNickname, "Questrial",PathogenTest.HD_Multiplyer * playerNickFontSize / PathogenTest.scaleFactor, Color.WHITE);
			addChild(playerNicknameText);
			playerNicknameText.pivotX = playerNicknameText.width / 2;
			playerNicknameText.pivotY = playerNicknameText.height / 2;
			playerNicknameText.x = playerBG.x + (PathogenTest.wTenPercent * 0.55);
			playerNicknameText.y = playerBG.y;
			
			playerNickFontSize = 24;
			playerLeagueText = new TextField(playerNicknameText.width, PathogenTest.hTenPercent * 0.8, playerLeague, "Questrial",PathogenTest.HD_Multiplyer * playerNickFontSize / PathogenTest.scaleFactor, Color.WHITE);
			addChild(playerLeagueText);
			playerLeagueText.pivotX = playerLeagueText.width / 2;
			playerLeagueText.pivotY = playerLeagueText.height / 2;
			playerLeagueText.x = playerNicknameText.x;
			playerLeagueText.y = playerNicknameText.y + playerNicknameText.height/2 + playerLeagueText.height/2;
			playerLeagueText.visible = false;
			playerLeagueText.hAlign = HAlign.LEFT;
			playerLeagueText.vAlign = VAlign.TOP;
			//playerLeagueText.border = true;
			
			// stats
			victories = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent, player.customWins.toString(), "Dekar",PathogenTest.HD_Multiplyer * 80 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(victories);
			victories.pivotX = victories.width / 2;
			victories.pivotY = victories.height / 2;
			victories.x = leftSideBackground.x - PathogenTest.wTenPercent * 1.25;
			victories.y = leftSideBackground.y - (PathogenTest.hTenPercent * 0.8);
			
			var victoriesText:TextField = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent * 0.7, "victories", "Questrial",PathogenTest.HD_Multiplyer * 48 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(victoriesText);
			victoriesText.pivotX = victoriesText.width / 2;
			victoriesText.pivotY = victoriesText.height / 2;
			victoriesText.x = leftSideBackground.x - PathogenTest.wTenPercent * 1.25;
			victoriesText.y = leftSideBackground.y;
			
			losses = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent, player.customLosses.toString(), "Dekar",PathogenTest.HD_Multiplyer * 60 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(losses);
			losses.pivotX = losses.width / 2;
			losses.pivotY = losses.height / 2;
			losses.x = leftSideBackground.x + PathogenTest.wTenPercent * 0.4;
			losses.y = leftSideBackground.y - (PathogenTest.hTenPercent * 0.8);
			
			var lossesText:TextField = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent * 0.7, "losses", "Questrial",PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(lossesText);
			lossesText.pivotX = lossesText.width / 2;
			lossesText.pivotY = lossesText.height / 2;
			lossesText.x = leftSideBackground.x + PathogenTest.wTenPercent * 0.4;
			lossesText.y = leftSideBackground.y;
			
			ties = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent, player.customTies.toString(), "Dekar",PathogenTest.HD_Multiplyer * 60 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(ties);
			ties.pivotX = ties.width / 2;
			ties.pivotY = ties.height / 2;
			ties.x = leftSideBackground.x + PathogenTest.wTenPercent * 1.6;
			ties.y = leftSideBackground.y - (PathogenTest.hTenPercent * 0.8);
			
			var tiesText:TextField = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent * 0.7, "ties", "Questrial",PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(tiesText);
			tiesText.pivotX = tiesText.width / 2;
			tiesText.pivotY = tiesText.height / 2;
			tiesText.x = leftSideBackground.x + PathogenTest.wTenPercent * 1.6;
			tiesText.y = leftSideBackground.y;
			
			var cellsCreatedBG:Scale9Image = new Scale9Image(textures);
			cellsCreatedBG.width = PathogenTest.wTenPercent * 1.8;
			cellsCreatedBG.height = PathogenTest.hTenPercent * 1.65;
			cellsCreatedBG.pivotX = cellsCreatedBG.width / 2;
			cellsCreatedBG.pivotY = cellsCreatedBG.height / 2;
			cellsCreatedBG.color = Color.BLACK;
			cellsCreatedBG.x = leftSideBackground.x - PathogenTest.wTenPercent * 1.1;
			cellsCreatedBG.y = leftSideBackground.y + PathogenTest.hTenPercent * 1.4;
			addChild(cellsCreatedBG);
			cellsCreatedBG.alpha = 0.5;
			
			var cell:Image = new Image(Main.assets.getTexture("Green_Stage01"));
			cell.pivotX = cell.width / 2;
			cell.pivotY = cell.height / 2;
			cell.scaleX = 1.75;
			cell.scaleY = 1.75;
			cell.x = cellsCreatedBG.x;
			cell.y = cellsCreatedBG.y;
			cell.alpha = 0.4;
			addChild(cell);
			
			cellsCreated = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent, player.numCellsCreatedCustom.toString(), "Dekar",PathogenTest.HD_Multiplyer * 45 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(cellsCreated);
			cellsCreated.pivotX = ties.width / 2;
			cellsCreated.pivotY = ties.height / 2;
			cellsCreated.x = cellsCreatedBG.x;
			cellsCreated.y = cellsCreatedBG.y - PathogenTest.hTenPercent * 0.25;
			
			var cellsCreatedText:TextField = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent, "cells created", "Questrial",PathogenTest.HD_Multiplyer * 25 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(cellsCreatedText);
			cellsCreatedText.pivotX = cellsCreatedText.width / 2;
			cellsCreatedText.pivotY = cellsCreatedText.height / 2;
			cellsCreatedText.x = cellsCreatedBG.x;
			cellsCreatedText.y = cellsCreatedBG.y + PathogenTest.hTenPercent * 0.25;
			
			// cells destroyed
			var cellsDestroyedBG:Scale9Image = new Scale9Image(textures);
			cellsDestroyedBG.width = PathogenTest.wTenPercent * 1.8;
			cellsDestroyedBG.height = PathogenTest.hTenPercent * 1.65;
			cellsDestroyedBG.pivotX = cellsDestroyedBG.width / 2;
			cellsDestroyedBG.pivotY = cellsDestroyedBG.height / 2;
			cellsDestroyedBG.color = Color.BLACK;
			cellsDestroyedBG.x = leftSideBackground.x + PathogenTest.wTenPercent * 1.1;
			cellsDestroyedBG.y = leftSideBackground.y + PathogenTest.hTenPercent * 1.4;
			addChild(cellsDestroyedBG);
			cellsDestroyedBG.alpha = 0.5;
			
			var virus:Image = new Image(Main.assets.getTexture("Green_Virus"));
			virus.pivotX = virus.width / 2;
			virus.pivotY = virus.height / 2;
			virus.scaleX = 1.6;
			virus.scaleY = 1.6;
			virus.x = cellsDestroyedBG.x;
			virus.y = cellsDestroyedBG.y;
			virus.alpha = 0.4;
			addChild(virus);
			
			cellsDestroyed = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent, player.numCellsDestroyedCustom.toString(), "Dekar",PathogenTest.HD_Multiplyer * 45 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(cellsDestroyed);
			cellsDestroyed.pivotX = ties.width / 2;
			cellsDestroyed.pivotY = ties.height / 2;
			cellsDestroyed.x = cellsDestroyedBG.x;
			cellsDestroyed.y = cellsDestroyedBG.y - PathogenTest.hTenPercent * 0.25;
			
			var cellsDestroyedText:TextField = new TextField(PathogenTest.wTenPercent * 2.0, PathogenTest.hTenPercent * 0.7, "cells destroyed", "Questrial",PathogenTest.HD_Multiplyer * 25 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(cellsDestroyedText);
			cellsDestroyedText.pivotX = cellsDestroyedText.width / 2;
			cellsDestroyedText.pivotY = cellsDestroyedText.height / 2;
			cellsDestroyedText.x = cellsDestroyedBG.x;
			cellsDestroyedText.y = cellsDestroyedBG.y + PathogenTest.hTenPercent * 0.25;
			
			// nemesis
			var nemesisNickname:String = player.getNemesisNickname();
			if (nemesisNickname != "")
			{
				var nemesisString:String = "Nemesis: " + nemesisNickname;
				var nemesisText:TextField = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent * 1.0, nemesisString, "Dekar",PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
				addChild(nemesisText);
				nemesisText.pivotX = nemesisText.width / 2;
				nemesisText.pivotY = nemesisText.height / 2;
				nemesisText.x = leftSideBackground.x;
				nemesisText.y = cellsDestroyedBG.y + PathogenTest.hTenPercent * 1.5;
			}
			
			if(Main.saveDataObject.data.lastProfilePage != null)
			{
				currentPage = Main.saveDataObject.data.lastProfilePage;
				
				if(currentPage == "Ranked")
					onTouchRanked(null);
				else
					onTouchCasual(null);
			}
			else
				onTouchCasual(null);	
		}
		
		private function createJoinedLeagueScreen(promoted:Boolean):void
		{
			_joinedLeagueScreen = new JoinedLeagueScreen(this, promoted);
			addChild(_joinedLeagueScreen);
			
			//_onlinePlay.main.entityManager.generateCurrentLadder();
		}
		
		public function removeJoinedLeagueScreen():void
		{
			if(_joinedLeagueScreen)
			{
				_joinedLeagueScreen.dispose();
				removeChild(_joinedLeagueScreen);
				_joinedLeagueScreen = null;
			}
		}
		
		private function setupGameList():void
		{
			var i:int;
			
			// right side
			var rightSideBackground:Scale9Image = new Scale9Image(textures);
			rightSideBackground.width = PathogenTest.wTenPercent * 4.5;
			rightSideBackground.height = PathogenTest.hTenPercent * 7;
			rightSideBackground.pivotX = leftSideBackground.width/2;
			rightSideBackground.pivotY = leftSideBackground.height/2;
			rightSideBackground.color = 0x222245;
			rightSideBackground.alpha = .5;
			rightSideBackground.x = PathogenTest.wTenPercent * 7.5;
			rightSideBackground.y = leftSideBackground.y;
			addChild(rightSideBackground);
			
			// games list
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingTop = PathogenTest.hTenPercent * 0.4;
			//layout.paddingTop = PathogenTest.hTenPercent * 1.0;
			layout.paddingLeft = PathogenTest.wTenPercent * 2.5;
			layout.gap = PathogenTest.wTenPercent * 0.5;
			
			_slider = new ScrollContainer();
			_slider.layout = layout;
			_slider.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_slider.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			_slider.scrollerProperties.snapScrollPositionsToPixels = true;
			_slider.width = PathogenTest.wTenPercent * 5.0;
			_slider.height = PathogenTest.hTenPercent * 6.5;
			_slider.x = PathogenTest.wTenPercent * 5.0;
			_slider.y = PathogenTest.hTenPercent * 1.4;
			addChild(_slider);
			
			var onlineGamePreview:OnlineGamePreviewSmall;
			// invites
			_invitePreviews = new Vector.<OnlineGamePreviewSmall>();
			var invites:Vector.<InviteEntity> = _onlinePlay.main.entityManager.invites;
			
			// your turn text
			if (invites.length > 0)
			{
				var invitesText:TextField = new TextField(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent * 0.7, "Invites", "Questrial",PathogenTest.HD_Multiplyer * 48 / PathogenTest.scaleFactor, Color.WHITE);
				invitesText.pivotX = invitesText.width / 2;
				invitesText.pivotY = invitesText.height / 2;
				_slider.addChild(invitesText);
			}
			
			for (i=0; i<invites.length; i++)
			{
				//Main.updateDebugText(games[i].id + " " + games[i].status + " fp = " + _onlinePlay.main.entityManager.amIFirstPlayer(games[i].id));
				var invitedGame:GameEntity = _onlinePlay.main.entityManager.findInvitedGameFromID(invites[i].gameID);
				onlineGamePreview = new OnlineGamePreviewSmall(_onlinePlay, invitedGame.mapID, invitedGame.status, invitedGame.id, invitedGame.playerNicknames, invitedGame.colors, true, false, null, null);
				_invitePreviews.push(onlineGamePreview);
				//_slider.addChild(onlineGamePreview);
			}
			for (i=0; i<_invitePreviews.length; i++)
				_slider.addChild(_invitePreviews[i]);
			
			// games
			_gamePreviews = new Vector.<OnlineGamePreviewSmall>();
			var games:Vector.<GameEntity> = _onlinePlay.main.entityManager.games;
			
			// your turn text
			var gamesToShow:int = 0;
			for (i=0; i<games.length; i++)
			{
				//Main.updateDebugText(games[i].id + " " + games[i].status + " fp = " + _onlinePlay.main.entityManager.amIFirstPlayer(games[i].id));
				
				if ((games[i].status == EntityManager.GAME_STATUS_BEGUN && _onlinePlay.main.entityManager.isItMyTurn(games[i].id)) ||
					(games[i].status == EntityManager.GAME_STATUS_WAITING && _onlinePlay.main.entityManager.amIFirstPlayer(games[i].id) && games[i].turnLocations.length == 0))
				{
					gamesToShow++;
				}
			}
			//if (gamesToShow > 0)
			{
				var yourTurn:TextField = new TextField(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent * 0.7, "Your Turn", "Questrial",PathogenTest.HD_Multiplyer * 48 / PathogenTest.scaleFactor, Color.WHITE);
				yourTurn.pivotX = yourTurn.width / 2;
				yourTurn.pivotY = yourTurn.height / 2;
				_slider.addChild(yourTurn);
			}
			
			for (i=0; i<games.length; i++)
			{
				//Main.updateDebugText(games[i].id + " " + games[i].status + " fp = " + _onlinePlay.main.entityManager.amIFirstPlayer(games[i].id));
				
				if ((games[i].status == EntityManager.GAME_STATUS_BEGUN && _onlinePlay.main.entityManager.isItMyTurn(games[i].id)) ||
					(games[i].status == EntityManager.GAME_STATUS_WAITING && _onlinePlay.main.entityManager.amIFirstPlayer(games[i].id) && games[i].turnLocations.length == 0))
				{
					onlineGamePreview = new OnlineGamePreviewSmall(_onlinePlay, games[i].mapID, games[i].status, games[i].id, games[i].playerNicknames, games[i].colors, false, games[i].ranked, games[i].playerLeagues, games[i].playerRanks);
					_gamePreviews.push(onlineGamePreview);
					//_slider.addChild(onlineGamePreview);
				}
			}
			
			_gamePreviews.sort(gameSort);
			
			for (i=0; i<_gamePreviews.length; i++)
				_slider.addChild(_gamePreviews[i]);
			
			// slider events
			_slider.addEventListener(FeathersEventType.SCROLL_COMPLETE, onScrollComplete);
			_slider.addEventListener(Event.SCROLL, onScroll);
			Starling.juggler.delayCall(stopSliding, 0.25);
			
			// check for new league
			if (_onlinePlay.main.entityManager.checkForLeague())
				createJoinedLeagueScreen(false);
		}
		private function onTouchCasual($e:TouchEvent):void
		{
			if($e)
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				var open:Boolean = true;
			
			if (touch || open)
			{
				cellsDestroyed.text = player.numCellsDestroyedCustom.toString();
				cellsCreated.text = player.numCellsCreatedCustom.toString();
				ties.text = player.customTies.toString();
				losses.text = player.customLosses.toString();
				victories.text = player.customWins.toString();
				
				playerNicknameText.hAlign = HAlign.CENTER;
				
				playerNicknameText.y = playerBG.y;
				playerLeagueText.visible = false;
				
				playerIcon.visible = true;
				leagueIcon.visible = false;
				
				_casualButton.toggleUp();
				_rankedButton.toggleDown();
				
				currentPage = "Custom";
				Main.saveDataObject.data.lastProfilePage = currentPage;
				Main.saveDataObject.flush();
			}
		}
		private function onTouchRanked($e:TouchEvent):void
		{
			if($e)
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				var open:Boolean = true;
			
			if (touch || open)
			{
				cellsDestroyed.text = player.numCellsDestroyedRanked.toString();
				cellsCreated.text = player.numCellsCreatedRanked.toString();
				ties.text = player.ties[0].toString();
				losses.text = player.losses[0].toString();
				victories.text = player.wins[0].toString();
				
				playerNicknameText.hAlign = HAlign.LEFT;
				playerNicknameText.y = playerBG.y - PathogenTest.wTenPercent * .15;
				
				playerLeagueText.y = playerNicknameText.y + playerNicknameText.height/2 + playerLeagueText.height/2;
				playerLeagueText.visible = true;
				
				playerIcon.visible = false;
				leagueIcon.visible = true;
				
				_casualButton.toggleDown();
				_rankedButton.toggleUp();
				
				currentPage = "Ranked";
				Main.saveDataObject.data.lastProfilePage = currentPage;
				Main.saveDataObject.flush();
			}
		}
		public function onScrollComplete(event:Event):void
		{
			_sliding = false;
		}
		public function onScroll(event:Event):void
		{
			_sliding = true;
		}
		private function stopSliding():void
		{
			_sliding = false;
		}
		
		private function gameSort(game1:OnlineGamePreviewSmall, game2:OnlineGamePreviewSmall):int
		{
			// sort ranked games before unranked games
			if (game1.ranked)
			{
				if (game2.ranked)
					return 0;
				else
					return 1;
			}
			else
			{
				if (game2.ranked)
					return -1;
				else
					return 0;
			}
		}
		
		public override function death():void
		{
			_onlinePlay = null;
			var i:int;
			
			if (_casualButton)
			{
				_casualButton.removeEventListener(TouchEvent.TOUCH, onTouchCasual);
				_casualButton.destroy();
				removeChild(_casualButton);
				_casualButton = null;
			}
			
			if (_rankedButton)
			{
				_rankedButton.removeEventListener(TouchEvent.TOUCH, onTouchRanked);
				_rankedButton.destroy();
				removeChild(_rankedButton);
				_rankedButton = null;
			}
			
			if (_gamePreviews)
			{
				for (i=(_gamePreviews.length - 1); i>=0; i--)
				{
					_gamePreviews[i].garbageCollection();
					_slider.removeChild(_gamePreviews[i]);
					_gamePreviews.pop();
				}
				_gamePreviews = null;
			}
			
			if (_invitePreviews)
			{
				for (i=(_invitePreviews.length - 1); i>=0; i--)
				{
					_invitePreviews[i].garbageCollection();
					_slider.removeChild(_invitePreviews[i]);
					_invitePreviews.pop();
				}
				_invitePreviews = null;
			}
			
			if (_slider)
			{
				_slider.dispose();
				removeChild(_slider);
				_slider = null;
			}
		}
		
		public function get sliding():Boolean { return _sliding; }
		public function get onlinePlay():OnlinePlay { return _onlinePlay; }
	}
}