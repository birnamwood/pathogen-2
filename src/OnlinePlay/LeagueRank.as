package OnlinePlay
{
	public class LeagueRank
	{
		private var _league:String;
		private var _division:int;
		private var _status:Boolean;
		
		public function LeagueRank(league:String, division:int)
		{
			_league = league;
			_division = division;
			_status = false;
		}
		
		public function get league():String { return _league; }
		public function get division():int { return _division; }
		public function get status():Boolean { return _status; }
		
		public function set league(league:String):void { _league = league; }
		public function set division(division:int):void { _division = division; }
		public function set status(status:Boolean):void { _status = status; }
	}
}