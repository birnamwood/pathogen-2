package OnlinePlay
{
	import UI.Main;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class OnlinePlayerPreview extends Sprite
	{
		private var _onlineGameSetup:OnlineGameSetup;
		private var _playerID:String;
		private var _playerNickname:TextField;
		
		public function OnlinePlayerPreview(onlineGameSetup:OnlineGameSetup, playerID:String, playerNicknameString:String)
		{
			super();
			
			_onlineGameSetup = onlineGameSetup;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const blacktexture:Texture = Main.assets.getTexture("scale9_black");
			const blacktextures:Scale9Textures = new Scale9Textures(blacktexture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var background:Scale9Image = new Scale9Image(blacktextures);
			background.width = PathogenTest.wTenPercent * 5;
			background.height = PathogenTest.hTenPercent * 4;
			background.pivotX = background.width / 2;
			background.pivotY = background.height / 2;
			background.alpha = .5;
			addChild(background);
			
			_playerID = playerID;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 48 / PathogenTest.scaleFactor;
			_playerNickname = new TextField(background.width, PathogenTest.wTenPercent, playerNicknameString, "Questrial", fontSize, Color.WHITE);
			_playerNickname.pivotX = _playerNickname.width / 2;
			_playerNickname.pivotY = _playerNickname.height / 2;
			addChild(_playerNickname);
			
			if (_onlineGameSetup)
			{
				alpha = 0;
				addEventListener(Event.ENTER_FRAME, fadeIn);
				addEventListener(TouchEvent.TOUCH, onTouch);
			}
		}
		
		private function fadeIn(event:Event):void
		{
			alpha += Main.FADE_SPEED;
			
			if(alpha >= 1)
			{
				removeEventListener(Event.ENTER_FRAME, fadeIn);
			}
		}

		public function onTouch(event:TouchEvent):void
		{
			if(_onlineGameSetup.sliding == false)
			{
				var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
				if (touch)
					_onlineGameSetup.selectPlayer(this);
			}
		}
		
		public function getPlayerID():String { return _playerID; }
		public function setPlayerID(playerID:String):void { _playerID = playerID; }
	}
}