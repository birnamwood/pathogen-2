package OnlinePlay
{
	import UI.Main;
	
	import Utils.BW_Button;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class OnlinePlayerPicker extends Sprite
	{
		//Parent Class
		private var gameSetup:OnlineGameSetup;
		
		private var _playerHolder:Sprite;
		
		private var deactivated:Number = .4;
		private var removeBT:BW_Button;
		private var _playerButton:BW_Button;
		
		public var chosenColor:int = 0;
		public var whatPlayer:int;
		private var _playerName:TextField;
		
		public var ai:Boolean = false;
		public var active:Boolean = false;
		
		private var _playerID:String;
		
		public function OnlinePlayerPicker(whatPlayer:int, gameSetup:OnlineGameSetup)
		{
			this.whatPlayer = whatPlayer;
			this.gameSetup = gameSetup;
			
			super();
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var background:Scale9Image = new Scale9Image(textures);
			background.width = PathogenTest.wTenPercent * 3;
			background.height = PathogenTest.hTenPercent * 3;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.color = 0x00001a;
			background.alpha = .5;
			addChild(background);
			
			_playerHolder = new Sprite();
			addChild(_playerHolder);
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
			_playerName = new TextField(background.width, PathogenTest.hTenPercent/1.5, "Player " + (whatPlayer + 1), "Questrial", fontSize, Color.WHITE);
			_playerHolder.addChild(_playerName);
			
			_playerName.pivotX = _playerName.width/2;
			_playerName.pivotY = _playerName.height/2;
			
			if(whatPlayer == 0)
			{
				chosenColor = 0;
				active = true;
			}
			if(whatPlayer == 1)
				chosenColor = 1;
			
			if(whatPlayer > 0)
			{
				_playerHolder.visible = false;
				
				_playerButton = new BW_Button(2.8, 2.8, "Invite Player " + (whatPlayer + 1) , 28);
				addChild(_playerButton);
				_playerButton.addEventListener(TouchEvent.TOUCH, onTouchInviteFriend);
			}
			
			if(whatPlayer > 1)
			{
				var comingSoonOverlay:Scale9Image = new Scale9Image(textures);
				comingSoonOverlay.width = PathogenTest.wTenPercent * 3;
				comingSoonOverlay.height = PathogenTest.hTenPercent * 3;
				comingSoonOverlay.pivotX = comingSoonOverlay.width/2;
				comingSoonOverlay.pivotY = comingSoonOverlay.height/2;
				comingSoonOverlay.color = Color.BLACK;
				comingSoonOverlay.alpha = .5;
				addChild(comingSoonOverlay);
				
				var comingSoonText:TextField = new TextField(comingSoonOverlay.width, comingSoonOverlay.height, "coming soon", "Dekar", PathogenTest.HD_Multiplyer * 24/ PathogenTest.scaleFactor, Color.WHITE);
				comingSoonText.pivotX = comingSoonText.width/2;
				comingSoonText.pivotY = comingSoonText.height/2;
				addChild(comingSoonText);
				comingSoonText.y = PathogenTest.hTenPercent * .4;
			}
		}
		private function onTouchInviteFriend($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				gameSetup.addOnlineFriendList(this)
			}
		}
		public function updatePlayer(playerName:String, playerID:String):void
		{
			_playerButton.removeEventListener(TouchEvent.TOUCH, onTouchInviteFriend);
			removeChild(_playerButton);
			_playerHolder.visible = true;
			_playerName.text = playerName;
			
			_playerID = playerID;
			active = true;
			gameSetup.updatePlayerIndicators();
		}
		public function garbageCollection():void
		{
			if(_playerButton)
			{
				_playerButton.destroy();
				_playerButton = null;
			}
			
			gameSetup = null;
		}
		
		public function get playerName():String {return _playerName.text};
		public function get playerID():String {return _playerID};
		
		public function set playerName(playerName:String):void {_playerName.text = playerName};
		
	}
}