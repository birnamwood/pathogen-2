package OnlinePlay
{
	import FloxConnect.InviteEntity;
	
	import GameRecords.PlayerRecord;
	
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_Selector;
	import Utils.BW_UI;
	
	import com.sticksports.nativeExtensions.gameCenter.GCPlayer;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.display.Scale9Image;
	import feathers.events.FeathersEventType;
	import feathers.layout.VerticalLayout;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class OnlineFriendList extends BW_UI
	{
		//Parent Classes
		private var _onlinePlayerPicker:OnlinePlayerPicker;
		private var _onlineGameSetup:OnlineGameSetup;
		
		//buttons
		private var _backButton:BW_Button;
		private var _inviteButton:BW_Button;
		
		private var _selectedOnlineFriend:PlayerRecord;
		//private var _selector:BW_Selector;
		private var _iconArray:Array = [];
		private var _container:ScrollContainer;
		
		private var _sliding:Boolean;
		
		public function OnlineFriendList(onlinePlayerPicker:OnlinePlayerPicker, onlineGameSetup:OnlineGameSetup)
		{
			super();
			
			_onlinePlayerPicker = onlinePlayerPicker;
			_onlineGameSetup = onlineGameSetup;
			
			//Create a transparent background.
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			addChild(transBackground);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			transBackground.alpha = .75;
			
			//Creating background so that content stands out.
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 5;
			background.height = PathogenTest.hTenPercent * 7;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			background.color = 0x222245;
			
			//Create Back button.
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_backButton = new BW_Button(.75,1, "", 0);
			else
				_backButton = new BW_Button(.5,.75, "", 0);
			
			_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(_backButton);
			_backButton.x = - background.width/2 + (_backButton.width * .7);
			_backButton.y = background.height/2 - (_backButton.height * .7);
			_backButton.addEventListener(TouchEvent.TOUCH, onTouchQuit);
			
			//Create Invite button
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_inviteButton = new BW_Button(1.5,1.0, "Invite", 24);
			else
				_inviteButton = new BW_Button(1.5,.75, "Invite", 24);
				
			addChild(_inviteButton);
			_inviteButton.y = _backButton.y;
			_inviteButton.addEventListener(TouchEvent.TOUCH, onTouchInvite);
			_inviteButton.alpha = .5;
			
			var title:TextField = new TextField(background.width, PathogenTest.hTenPercent, "Friends", "Dekar", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(title);
			title.pivotX = title.width/2;
			title.y = -background.height/2;
			
			var layout:VerticalLayout = new VerticalLayout();
			
			//Create container for scrollable objects.
			_container = new ScrollContainer();
			_container.layout = layout;
			_container.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_container.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			_container.scrollerProperties.snapScrollPositionsToPixels = true;
			_container.height = PathogenTest.hTenPercent * 5;
			_container.width = PathogenTest.wTenPercent * 5;
			addChild(_container);
			
			_container.x = -background.width/2;
			_container.y = title.y + title.height;
			
			layout.paddingLeft = PathogenTest.wTenPercent * 2.5;
			layout.paddingTop = PathogenTest.hTenPercent;
			layout.gap = PathogenTest.wTenPercent/2;
			
			//Main.updateDebugText("Player has " + onlineGameSetup.onlinePlay.main.entityManager.players.length + " friends");
			
			for (var i:int = 0; i < onlineGameSetup.onlinePlay.main.entityManager.players.length; i++)
			{
				if (onlineGameSetup.onlinePlay.main.entityManager.players[i].hasPathogen)
				{
					var onlinePlayerIcon:OnlinePlayerIcon = new OnlinePlayerIcon(this, onlineGameSetup.onlinePlay.main.entityManager.players[i]);
					_container.addChild(onlinePlayerIcon);
					_iconArray.push(onlinePlayerIcon);
				}
			}
			
			//_selector = new BW_Selector(4.1,1.6);
			//_container.addChild(_selector);
			//_selector.visible = false;
			
			// slider events
			_container.addEventListener(FeathersEventType.SCROLL_COMPLETE, onScrollComplete);
			_container.addEventListener(Event.SCROLL, onScroll);
			Starling.juggler.delayCall(stopSliding, 0.25);
		}
		
		// custom slider functions
		public function onScrollComplete(event:Event):void
		{
			_sliding = false;
		}
		public function onScroll(event:Event):void
		{
			_sliding = true;
		}
		private function stopSliding():void
		{
			_sliding = false;
		}
		
		private function onTouchQuit($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				startFadeOut();
			}
		}
		private function onTouchInvite($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && _inviteButton.alpha == 1)
			{
				_onlinePlayerPicker.updatePlayer(_selectedOnlineFriend.nickname, _selectedOnlineFriend.gamerID);
				startFadeOut();
			}
		}
		public function selectFriend(onlinePlayerIcon:OnlinePlayerIcon):void
		{
			/*
			if (!_sliding)
			{
				Main.updateDebugText("selector");
				
				if (onlinePlayerIcon.friend != null)
				{
					if (onlinePlayerIcon.friend.nickname != "")
					{
						Main.updateDebugText("selected " + onlinePlayerIcon.friend.nickname);
						
						_selectedOnlineFriend = onlinePlayerIcon.friend;
						
						_selector.visible = true;
						_selector.x = onlinePlayerIcon.x;
						_selector.y = onlinePlayerIcon.y;
						
						_inviteButton.alpha = 1;
					}
				}
			}
			*/
			
			// set selector to be only visible for clicked player
			for (var i:int=0; i<_iconArray.length; i++)
			{
				_iconArray[i].setSelectorVisibility(false);
			}
			onlinePlayerIcon.setSelectorVisibility(true);
			
			_selectedOnlineFriend = onlinePlayerIcon.friend;
			_inviteButton.alpha = 1;
		}
		public override function death():void
		{
			if (_iconArray)
			{
				//Purge all icons
				for each (var playerIcon:OnlinePlayerIcon in _iconArray)
				{
					playerIcon.garbageCollection();
					playerIcon.dispose();
					removeChild(playerIcon)
					playerIcon = null;
				}
				
				_iconArray = [];
			}
			
			if (_inviteButton)
			{
				_inviteButton.destroy();
				_inviteButton.dispose();
				removeChild(_inviteButton)
				_inviteButton = null;
			}
			
			if (_backButton)
			{
				_backButton.destroy();
				_backButton.dispose();
				removeChild(_backButton)
				_backButton = null;
			}
			
			/*
			if (_selector)
			{
				_selector.removeEventListener(FeathersEventType.SCROLL_COMPLETE, onScrollComplete);
				_selector.removeEventListener(Event.SCROLL, onScroll);
				_selector.dispose();
				_container.removeChild(_selector);
				_selector = null;
			}
			*/
			
			_onlineGameSetup.removeOnlineFriendList();
			_onlinePlayerPicker = null;
		}
		
		public function get sliding():Boolean { return _sliding; }
	}
}