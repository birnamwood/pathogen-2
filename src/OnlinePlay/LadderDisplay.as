package OnlinePlay
{
	import UI.Main;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.deg2rad;
	
	public class LadderDisplay extends Sprite
	{
		public function LadderDisplay(rank:int, playerName:String, state:int, pointValue:int, wins:int, losses:int)
		{
			super();
			
			var background:Quad = new Quad(PathogenTest.wTenPercent * 8, PathogenTest.hTenPercent * .5, 0x222245);
			addChild(background);
			
			if(rank % 2 == 0)
				background.color = 0x00001a;
			
			if(playerName == Main.sOnlineNickname)
				background.color = 0x4D4D75;
			
			var fontName:String = "Dekar";
			var fontSize:int = PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor;
			
			//What rank is this
			var rankText:TextField = new TextField(PathogenTest.wTenPercent * 1.75, background.height, (rank + 1).toString(), fontName, fontSize, Color.WHITE);
			addChild(rankText);
			rankText.x = PathogenTest.wTenPercent * .4;
			rankText.hAlign = HAlign.LEFT;
			//rankText.border = true;
			
			//What player is currently at this rank
			var playerText:TextField = new TextField(PathogenTest.wTenPercent * 3.5, background.height, playerName.toString(), fontName, fontSize, Color.WHITE);
			addChild(playerText);
			playerText.x = rankText.x + rankText.width;
			playerText.hAlign = HAlign.LEFT;
			//playerText.border = true;
			
			//Is this player moving up | down on this ladder
			//var state:Image;
			
			if(state >= 0)
			{
				var arrow:Image = new Image(Main.assets.getTexture("Pathogen_BTN_Back"));
				addChild(arrow);
				arrow.pivotX = arrow.width/2;
				arrow.pivotY = arrow.height/2;
				arrow.x = arrow.width * .75;
				arrow.y = arrow.height/2;
				
				if(state == 1)//Moving up
				{
					arrow.rotation = deg2rad(90);
					arrow.color = 0x52CAAF;
				}
				else//Moving down
				{
					arrow.rotation = deg2rad(-90);
					arrow.color = 0xE34081;
				}
			}
			
			//What this players actual point value is
			var pointText:TextField = new TextField(PathogenTest.wTenPercent * .5, background.height, pointValue.toString(), fontName, fontSize, Color.WHITE);
			addChild(pointText);
			pointText.x = playerText.x + playerText.width;
			//pointText.border = true;
			
			//This players current wins
			var winText:TextField = new TextField(PathogenTest.wTenPercent * 1, background.height, wins.toString(), fontName, fontSize, Color.WHITE);
			addChild(winText);
			winText.x = pointText.x + pointText.width;
			
			//This players current losses
			var lossesText:TextField = new TextField(PathogenTest.wTenPercent * .5, background.height, losses.toString(), fontName, fontSize, Color.WHITE);
			addChild(lossesText);
			lossesText.x = winText.x + winText.width;
		}
	}
}