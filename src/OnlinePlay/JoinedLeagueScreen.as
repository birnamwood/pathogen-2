package OnlinePlay
{
	import FloxConnect.PlayerEntity;
	
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_Window;
	
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class JoinedLeagueScreen extends BW_Window
	{
		//Classes
		private var _profileScreen:ProfileScreen;
		
		//Buttons
		private var _okButton:BW_Button;
		
		private var player:PlayerEntity
		private var _currentLeague:String = "Ladder";
		private var _leagueName:String = "League";
		private var _leagueIconString:String;
		
		private var _promoted:Boolean;
		
		public function JoinedLeagueScreen(profileScreen:ProfileScreen, promoted:Boolean)
		{
			super();
			
			_profileScreen = profileScreen;
			_promoted = promoted;
			
			determineLeague();
			
			createbackground(5, 6.5);
			createHeader(_currentLeague);
			
			init();
		}
		private function determineLeague():void
		{
			player = _profileScreen.onlinePlay.main.entityManager.currentPlayerEntity;
			
			var playerRank:int = player.ranks[0]
			var leagueIconString:String = "UserIcon";
			
			switch(_profileScreen.onlinePlay.main.entityManager.getLeagueName(player.league2Player))
			{
				case "Bronze":
					_currentLeague = "Joined Bronze League!";
					_leagueName = "Bronze League";
					
					//Determine which Badge to show
					if(playerRank <= 10)
						_leagueIconString = "Bronze03";
					else if (playerRank > 10 && playerRank <= 25)
						_leagueIconString = "Bronze02";
					else
						_leagueIconString = "Bronze01";
					
					break;
				
				case "Silver":
					_currentLeague = "Joined Silver League!";
					_leagueName = "Silver League";
					
					//Determine which Badge to show
					if(playerRank <= 10)
						_leagueIconString = "Silver03";
					else if (playerRank > 10 && playerRank <= 25)
						_leagueIconString = "Silver02";
					else
						_leagueIconString = "Silver01";
					
					break;
				
				case "Gold":
					_currentLeague = "Joined Gold League!";
					_leagueName = "Gold League";
					
					//Determine which Badge to show
					if(playerRank <= 10)
						_leagueIconString = "Gold03";
					else if (playerRank > 10 && playerRank <= 25)
						_leagueIconString = "Gold02";
					else
						_leagueIconString = "Gold01";
					
					break;
				
				default:
					//Player has not yet ranked!
					break;
			}
		}
		private function init():void
		{
			var leagueIcon:Image = new Image(Main.assets.getTexture(_leagueIconString));
			leagueIcon.pivotY = leagueIcon.height/2;
			leagueIcon.pivotX = leagueIcon.width / 2;
			addChild(leagueIcon);
			leagueIcon.x = _background.x;
			leagueIcon.y = _background.y - (PathogenTest.hTenPercent * .7);
			
			var leagueText:String;
			
			if(_promoted)
				leagueText = "Congratulations you have joined " + _leagueName + "!"
			else
				leagueText = "Congratulations you have been promoted to " + _leagueName + "!"
			
			var textX:int = _background.width;
			if (textX > Main.TEXTURE_LIMIT)
				textX = Main.TEXTURE_LIMIT;
			var text:TextField = new TextField(textX, PathogenTest.hTenPercent * 2, leagueText , "Dekar", PathogenTest.HD_Multiplyer * 42 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(text);
			text.pivotX = text.width/2;
			text.x = _background.x;
			text.y = leagueIcon.y + leagueIcon.height * .4;
			
			// create ok button
			_okButton = new BW_Button(1.0, 1.0, "OK", 36);
			addChild(_okButton);
			_okButton.x = _background.x;
			_okButton.y =  _background.y + _background.height/2 - (_okButton.height * .75);
			_okButton.addEventListener(TouchEvent.TOUCH, onTouchOk);
		}
		private function onTouchOk(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this,TouchPhase.ENDED);
			if (touch)
			{
				startFadeOut();
			}
		}
		override public function death():void
		{
			if(_okButton)
			{
				_okButton.dispose();
				_okButton.destroy();
				removeChild(_okButton);
				_okButton = null;
			}
			
			_profileScreen.removeJoinedLeagueScreen();
			_profileScreen = null;
		}
	}
}