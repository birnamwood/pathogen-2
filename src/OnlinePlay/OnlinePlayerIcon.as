package OnlinePlay
{
	import GameRecords.PlayerRecord;
	
	import UI.Main;
	
	import Utils.BW_Selector;
	
	import com.sticksports.nativeExtensions.gameCenter.GCPlayer;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	
	public class OnlinePlayerIcon extends Sprite
	{
		private var _onlineFriendList:OnlineFriendList
		private var _friend:PlayerRecord;
		private var _selector:BW_Selector;
		
		public function OnlinePlayerIcon(onlineFriendList:OnlineFriendList, friend:PlayerRecord)
		{
			super();
			
			_onlineFriendList = onlineFriendList;
			_friend = friend;
			
			//Creating screen background
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var playerBG:Quad = new Quad(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent * 1.5, Color.BLACK);
			playerBG.pivotX = playerBG.width / 2;
			playerBG.pivotY = playerBG.height / 2;
			playerBG.alpha = 0.5;
			addChild(playerBG);
			
			var playerIconBG:Scale9Image = new Scale9Image(textures);
			playerIconBG.width = PathogenTest.wTenPercent * 1.0;
			playerIconBG.height = PathogenTest.hTenPercent * 1.3;
			playerIconBG.color = 0x222245;
			playerIconBG.pivotX = playerIconBG.width / 2;
			playerIconBG.pivotY = playerIconBG.height / 2;
			addChild(playerIconBG);
			playerIconBG.x = -playerBG.width/2 + (playerIconBG.width/2 * 1.2)
			
			var playerIcon:Image = new Image(Main.assets.getTexture("UserIcon"));
			playerIcon.pivotX = playerIcon.width / 2;
			playerIcon.pivotY = playerIcon.height / 2;
			addChild(playerIcon);
			playerIcon.x = playerIconBG.x;
			playerIcon.y = playerIconBG.y;

			var playerNicknameString:String = friend.nickname;
			var playerNickFontSize:int = 48;
			if (playerNicknameString.length > 10 && playerNicknameString.length <= 15)
				playerNickFontSize = 38;
			else if (playerNicknameString.length > 15 && playerNicknameString.length <= 20)
				playerNickFontSize = 30;
			else if (playerNicknameString.length > 20)
			{
				playerNicknameString = playerNicknameString.substr(0, 20);
				playerNickFontSize = 30;
			}
			var playerNickname:TextField = new TextField(PathogenTest.wTenPercent * 2.5, PathogenTest.hTenPercent * 0.7, playerNicknameString, "Dekar", PathogenTest.HD_Multiplyer * playerNickFontSize / PathogenTest.scaleFactor, Color.WHITE);
			addChild(playerNickname);
			playerNickname.pivotY = playerNickname.height / 2;
			playerNickname.hAlign = HAlign.LEFT;
			playerNickname.x = playerIconBG.x + (playerIconBG.width * .8);
			
			addEventListener(TouchEvent.TOUCH, onTouch);
			
			_selector = new BW_Selector(4.1,1.6);
			addChild(_selector);
			_selector.visible = false;
		}
		private function onTouch($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && !_onlineFriendList.sliding)
			{
				_onlineFriendList.selectFriend(this);
			}
		}
		
		public function setSelectorVisibility(visible:Boolean):void
		{
			_selector.visible = visible;
		}
		
		public function garbageCollection():void
		{
			if (_selector)
			{
				_selector.dispose();
				removeChild(_selector);
				_selector = null;
			}
			
			removeEventListener(TouchEvent.TOUCH, onTouch);
			_onlineFriendList = null;
		}

		public function get friend():PlayerRecord { return _friend};
	}
}