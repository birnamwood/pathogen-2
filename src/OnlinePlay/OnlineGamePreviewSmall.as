package OnlinePlay
{
	import Core.Player;
	
	import FloxConnect.EntityManager;
	
	import Maps.Map;
	import Maps.MapInfo;
	import Maps.MapPreviewCell;
	
	import UI.Loader;
	import UI.Main;
	
	import Utils.BW_Button;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	
	public class OnlineGamePreviewSmall extends Sprite
	{
		private const LEAGUE_BADGE_SCALE:Number = 0.25;
		private const PLAYER_ICON_SCALE:Number = 0.375;
		private const NICKNAME_MAX_LENGTH:int = 16;
		
		private var _onlinePlay:OnlinePlay;
		private var _gameID:String;
		private var _mapIndex:int;
		private var _playerNicknames:Array;
		private var _playerColors:Array;
		private var _background:Scale9Image;
		private var _mapName:TextField;
		private var _status:int;
		private var _statusText:TextField;
		private var _otherPlayerText:TextField;
		private var _sortValue:int;
		private var _isInvite:Boolean;
		private var _subBackground:Scale9Image;
		private var _acceptInviteButton:BW_Button;
		private var _rejectInviteButton:BW_Button;
		private var _ranked:Boolean;
		
		private var green:Number = 0x52CAAF;
		private var red:Number = 0xE34081;
		private var blue:Number = 0x5A69CE;
		private var yellow:Number = 0xE0AF38;
		
		public function OnlineGamePreviewSmall(onlinePlay:OnlinePlay, mapIndex:int, status:int, gameID:String, playerNicknames:Array, playerColors:Array, isInvite:Boolean, ranked:Boolean, playerLeagues:Array, playerRanks:Array)
		{
			super();
			
			_onlinePlay = onlinePlay;
			_gameID = gameID;
			_playerNicknames = playerNicknames;
			_playerColors = playerColors;
			_isInvite = isInvite;
			_ranked = ranked;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			var texture:Texture = Main.assets.getTexture("scale9_White");
			var textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			_background = new Scale9Image(textures);
			_background.width = PathogenTest.stageWidth * 0.425;
			_background.height = PathogenTest.stageHeight * 0.2;
			_background.pivotX = _background.width / 2;
			_background.pivotY = _background.height / 2;
			addChild(_background);
			_background.color = Color.BLACK;
			_background.alpha = .5;
			
			// map name bg
			var mapNameBG:Scale9Image = new Scale9Image(textures);
			mapNameBG.color = 0x222245;
			mapNameBG.alpha = 0.9;
			mapNameBG.width = _background.width * 0.975;
			mapNameBG.height = PathogenTest.hTenPercent * 0.6;
			mapNameBG.pivotX = mapNameBG.width / 2;
			mapNameBG.pivotY = mapNameBG.height / 2;
			mapNameBG.y = _background.y - (_background.height / 2) + (mapNameBG.height / 2) + (PathogenTest.hTenPercent * 0.1);
			addChild(mapNameBG);
			
			_mapIndex = mapIndex;
			var map:Map = _onlinePlay.main.mapManager.getMap(_mapIndex);
			var mapNameText:String = map.name;
			if (ranked)
				mapNameText = "Ranked - " + mapNameText;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor;
			_mapName = new TextField(_background.width, PathogenTest.hTenPercent * 0.6, mapNameText, "Questrial", fontSize, Color.WHITE);
			//_mapName.hAlign = HAlign.LEFT;
			_mapName.pivotX = _mapName.width / 2;
			_mapName.pivotY = _mapName.height / 2;
			//_mapName.x = -(PathogenTest.wTenPercent * 0.9);
			//_mapName.y = -(PathogenTest.hTenPercent * 0.5);
			_mapName.y = mapNameBG.y;
			addChild(_mapName);
			//trace("creating preview - status = " + _status);
			
			// player 1
			var playerColor:Scale9Image = new Scale9Image(textures);
			playerColor.width = _background.width * 0.485;
			playerColor.height = PathogenTest.hTenPercent * 1.1;
			playerColor.pivotX = playerColor.width / 2;
			playerColor.pivotY = playerColor.height / 2;
			if (_playerColors[0] == 0)
				playerColor.color = green;
			else if (_playerColors[0] == 1)
				playerColor.color = red;
			else if (_playerColors[0] == 2)
				playerColor.color = blue;
			else if (_playerColors[0] == 3)
				playerColor.color = yellow;
			playerColor.x = _background.x - (_background.width / 4);
			playerColor.y = _background.y + PathogenTest.hTenPercent * 0.35;
			addChild(playerColor);
			
			var playerBGBG:Scale9Image = new Scale9Image(textures);
			playerBGBG.width = playerColor.width * 0.96;
			playerBGBG.height = playerColor.height * 0.94;
			playerBGBG.pivotX = playerBGBG.width / 2;
			playerBGBG.pivotY = playerBGBG.height / 2;
			playerBGBG.color = Color.BLACK;
			playerBGBG.x = playerColor.x;
			playerBGBG.y = playerColor.y;
			addChild(playerBGBG);
			
			var playerBG:Scale9Image = new Scale9Image(textures);
			playerBG.width = playerColor.width * 0.96;
			playerBG.height = playerColor.height * 0.94;
			playerBG.pivotX = playerBG.width / 2;
			playerBG.pivotY = playerBG.height / 2;
			playerBG.color = 0x222245;
			playerBG.alpha = 0.9;
			playerBG.x = playerColor.x;
			playerBG.y = playerColor.y;
			addChild(playerBG);

			var leagueIconString:String;
			var league:Boolean = false;
			var playerIcon:Image;
			if (!ranked)
			{
				playerIcon = new Image(Main.assets.getTexture("UserIcon"));
				playerIcon.scaleX = PLAYER_ICON_SCALE;
				playerIcon.scaleY = PLAYER_ICON_SCALE;
			}
			else
			{
				switch(_onlinePlay.main.entityManager.getLeagueName(playerLeagues[0]))
				{
					case "Bronze":
						//Determine which Badge to show
						if(playerRanks[0] <= 10)
							leagueIconString = "Bronze03";
						else if (playerRanks[0] > 10 && playerRanks[0] <= 25)
							leagueIconString = "Bronze02";
						else
							leagueIconString = "Bronze01";
						league = true;
						break;
					
					case "Silver":
						//Determine which Badge to show
						if(playerRanks[0] <= 10)
							leagueIconString = "Silver03";
						else if (playerRanks[0] > 10 && playerRanks[0] <= 25)
							leagueIconString = "Silver02";
						else
							leagueIconString = "Silver01";
						league = true;
						break;
					
					case "Gold":
						//Determine which Badge to show
						if(playerRanks[0] <= 10)
							leagueIconString = "Gold03";
						else if (playerRanks[0] > 10 && playerRanks[0] <= 25)
							leagueIconString = "Gold02";
						else
							leagueIconString = "Gold01";
						league = true;
						break;
					default:
						leagueIconString = "UserIcon";
						break;
				}
				playerIcon = new Image(Main.assets.getTexture(leagueIconString));
				if (league)
				{
					playerIcon.scaleX = LEAGUE_BADGE_SCALE;
					playerIcon.scaleY = LEAGUE_BADGE_SCALE;
				}
				else
				{
					playerIcon.scaleX = PLAYER_ICON_SCALE;
					playerIcon.scaleY = PLAYER_ICON_SCALE;
				}
			}
			playerIcon.pivotX = playerIcon.width / 2;
			playerIcon.pivotY = playerIcon.height / 2;
			addChild(playerIcon);
			if (league)
			{
				playerIcon.x = playerBG.x - PathogenTest.wTenPercent * 0.8;
				playerIcon.y = playerBG.y - PathogenTest.hTenPercent * 0.2;
			}
			else
			{
				playerIcon.x = playerBG.x - PathogenTest.wTenPercent * 0.75;
				playerIcon.y = playerBG.y - PathogenTest.hTenPercent * 0.15;
			}
			
			fontSize = PathogenTest.HD_Multiplyer * 22 / PathogenTest.scaleFactor;
			var playerNickStr:String = _playerNicknames[0];
			if (playerNickStr.length > NICKNAME_MAX_LENGTH)
				playerNickStr = playerNickStr.substr(0, NICKNAME_MAX_LENGTH);
			var playerText:TextField = new TextField(_background.width  * .6, playerBG.height, playerNickStr, "Questrial", fontSize, Color.WHITE);
			addChild(playerText);
			playerText.pivotY = playerText.height / 2;
			playerText.x = playerIcon.x + playerIcon.width;
			playerText.y = playerBG.y;
			playerText.hAlign = HAlign.LEFT;
			
			if (playerNicknames.length > 1)
			{
				// player 2
				var playerColor_02:Scale9Image = new Scale9Image(textures);
				playerColor_02.width = _background.width * 0.485;
				playerColor_02.height = PathogenTest.hTenPercent * 1.1;
				playerColor_02.pivotX = playerColor.width / 2;
				playerColor_02.pivotY = playerColor.height / 2;
				if (_playerColors[1] == 0)
					playerColor_02.color = green;
				else if (_playerColors[1] == 1)
					playerColor_02.color = red;
				else if (_playerColors[1] == 2)
					playerColor_02.color = blue;
				else if (_playerColors[1] == 3)
					playerColor_02.color = yellow;
				playerColor_02.x = _background.x + (_background.width / 4);
				playerColor_02.y = _background.y + PathogenTest.hTenPercent * 0.35;
				addChild(playerColor_02);
				
				var playerBGBG_02:Scale9Image = new Scale9Image(textures);
				playerBGBG_02.width = playerColor_02.width * 0.96;
				playerBGBG_02.height = playerColor_02.height * 0.94;
				playerBGBG_02.pivotX = playerBGBG_02.width / 2;
				playerBGBG_02.pivotY = playerBGBG_02.height / 2;
				playerBGBG_02.color = Color.BLACK;
				playerBGBG_02.x = playerColor_02.x;
				playerBGBG_02.y = playerColor_02.y;
				addChild(playerBGBG_02);
				
				var playerBG_02:Scale9Image = new Scale9Image(textures);
				playerBG_02.width = playerColor_02.width * 0.96;
				playerBG_02.height = playerColor_02.height * 0.94;
				playerBG_02.pivotX = playerBG_02.width / 2;
				playerBG_02.pivotY = playerBG_02.height / 2;
				playerBG_02.color = 0x222245;
				playerBG_02.alpha = 0.9;
				playerBG_02.x = playerColor_02.x;
				playerBG_02.y = playerColor_02.y;
				addChild(playerBG_02);
				
				var playerIcon_02:Image;
				if (!ranked)
				{
					playerIcon_02 = new Image(Main.assets.getTexture("UserIcon"));
					playerIcon_02.scaleX = PLAYER_ICON_SCALE;
					playerIcon_02.scaleY = PLAYER_ICON_SCALE;
				}
				else
				{
					switch(_onlinePlay.main.entityManager.getLeagueName(playerLeagues[1]))
					{
						case "Bronze":
							//Determine which Badge to show
							if(playerRanks[1] <= 10)
								leagueIconString = "Bronze03";
							else if (playerRanks[1] > 10 && playerRanks[1] <= 25)
								leagueIconString = "Bronze02";
							else
								leagueIconString = "Bronze01";
							league = true;
							break;
						
						case "Silver":
							//Determine which Badge to show
							if(playerRanks[1] <= 10)
								leagueIconString = "Silver03";
							else if (playerRanks[1] > 10 && playerRanks[1] <= 25)
								leagueIconString = "Silver02";
							else
								leagueIconString = "Silver01";
							league = true;
							break;
						
						case "Gold":
							//Determine which Badge to show
							if(playerRanks[1] <= 10)
								leagueIconString = "Gold03";
							else if (playerRanks[1] > 10 && playerRanks[1] <= 25)
								leagueIconString = "Gold02";
							else
								leagueIconString = "Gold01";
							league = true;
							break;
						default:
							leagueIconString = "UserIcon";
							break;
					}
					playerIcon_02 = new Image(Main.assets.getTexture(leagueIconString));
					if (league)
					{
						playerIcon_02.scaleX = LEAGUE_BADGE_SCALE;
						playerIcon_02.scaleY = LEAGUE_BADGE_SCALE;
					}
					else
					{
						playerIcon_02.scaleX = PLAYER_ICON_SCALE;
						playerIcon_02.scaleY = PLAYER_ICON_SCALE;
					}
				}
				playerIcon_02.pivotX = playerIcon_02.width / 2;
				playerIcon_02.pivotY = playerIcon_02.height / 2;
				addChild(playerIcon_02);
				if (league)
				{
					playerIcon_02.x = playerBG_02.x - PathogenTest.wTenPercent * 0.8;
					playerIcon_02.y = playerBG_02.y - PathogenTest.hTenPercent * 0.2;
				}
				else
				{
					playerIcon_02.x = playerBG_02.x - PathogenTest.wTenPercent * 0.75;
					playerIcon_02.y = playerBG_02.y - PathogenTest.hTenPercent * 0.15;
				}
				
				fontSize = PathogenTest.HD_Multiplyer * 22 / PathogenTest.scaleFactor;
				playerNickStr = _playerNicknames[1];
				if (playerNickStr.length > NICKNAME_MAX_LENGTH)
					playerNickStr = playerNickStr.substr(0, NICKNAME_MAX_LENGTH);
				var playerText_02:TextField = new TextField(_background.width  * .6, playerBG_02.height, playerNickStr, "Questrial", fontSize, Color.WHITE);
				addChild(playerText_02);
				playerText_02.pivotY = playerText_02.height / 2;
				playerText_02.x = playerIcon_02.x + playerIcon_02.width;
				playerText_02.y = playerBG_02.y;
				playerText_02.hAlign = HAlign.LEFT;
			}
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function onTouch(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.BEGAN)
			//if (touch)
				//_background.color = BW_Button.BACKGROUND_DOWN;
			
			touch = event.getTouch(this, TouchPhase.MOVED);
			//if (touch && _onlinePlay.profileScreen.sliding)
				//_background.color = BW_Button.BACKGROUND_UP;
			
			touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				//_background.color = BW_Button.BACKGROUND_UP;
					
				if (!_onlinePlay.profileScreen.sliding)
				{
					if (!_isInvite)
					{
						_onlinePlay.main.createGameLoader();
						
						//_onlinePlay.main.entityManager.setCurrentGameIndex(_gameID);
						//_onlinePlay.loadGame(_gameID);
						Starling.juggler.delayCall(enterGame, 0.5);
					}
					else
					{
						// create a transparent background
						var scale9Num:Number = 20/PathogenTest.scaleFactor;
						const texture:Texture = Main.assets.getTexture("scale9_White");
						const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
						
						_subBackground = new Scale9Image(textures);
						addChild(_subBackground);
						_subBackground.width = _background.width;
						_subBackground.height = _background.height;
						_subBackground.pivotX = _subBackground.width/2;
						_subBackground.pivotY = _subBackground.height/2;
						_subBackground.alpha = .75;
						_subBackground.color = Color.BLACK;
						
						_acceptInviteButton = new BW_Button(1.5, 0.75, "Accept", 36);
						_acceptInviteButton.x = -_acceptInviteButton.width * 0.75;
						addChild(_acceptInviteButton);
						_acceptInviteButton.addEventListener(TouchEvent.TOUCH, onTouchAcceptInvite);
						
						_rejectInviteButton = new BW_Button(1.5, 0.75, "Reject", 36);
						_rejectInviteButton.x = _rejectInviteButton.width * 0.75;
						addChild(_rejectInviteButton);
						_rejectInviteButton.addEventListener(TouchEvent.TOUCH, onTouchRejectInvite);
					}
				}
			}
		}
		
		private function enterGame():void
		{
			_onlinePlay.main.entityManager.setCurrentGameIndex(_gameID);
			_onlinePlay.loadGame(_gameID);
		}
		
		private function onTouchAcceptInvite(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this);
			if(touch.phase == TouchPhase.ENDED)
			{
				_onlinePlay.main.entityManager.pushInviteGameToActiveList(_gameID);
			}
		}
		
		private function onTouchRejectInvite(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this);
			if(touch.phase == TouchPhase.ENDED)
			{
				_onlinePlay.main.entityManager.rejectInvite(_gameID);
				_onlinePlay.main.returnToOnlineGamesList();
			}
		}
		
		public function garbageCollection():void
		{
			_playerNicknames = [];
			_playerColors = [];
			
			if (_background)
			{
				_background.dispose();
				removeChild(_background);
				_background = null;
			}
			if (_mapName)
			{
				_mapName.dispose();
				removeChild(_mapName);
				_mapName = null;
			}
			if (_statusText)
			{
				_statusText.dispose();
				removeChild(_statusText);
				_statusText = null;
			}
			if (_otherPlayerText)
			{
				_otherPlayerText.dispose();
				removeChild(_otherPlayerText);
				_otherPlayerText = null;
			}
			if (_subBackground)
			{
				_subBackground.dispose();
				removeChild(_subBackground);
				_subBackground = null;
			}
			if (_acceptInviteButton)
			{
				_acceptInviteButton.destroy();
				_acceptInviteButton.removeEventListener(TouchEvent.TOUCH, onTouchAcceptInvite);
				removeChild(_acceptInviteButton);
				_acceptInviteButton = null;
			}
			if (_rejectInviteButton)
			{
				_rejectInviteButton.destroy();
				_rejectInviteButton.removeEventListener(TouchEvent.TOUCH, onTouchRejectInvite);
				removeChild(_rejectInviteButton);
				_rejectInviteButton = null;
			}
		}
		
		public function get sortValue():int { return _sortValue; }
		public function get ranked():Boolean { return _ranked; }
	}
}