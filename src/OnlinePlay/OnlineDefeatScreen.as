package OnlinePlay
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import com.sticksports.nativeExtensions.flurry.Flurry;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	
	public class OnlineDefeatScreen extends BW_UI
	{
		private var _gameManager:OnlineGameManager;
		
		private var victoryCell:Sprite
		private var victoryParticles:PDParticleSystem
		
		private var viewStats:BW_Button;
		
		private var quitting:Boolean;
		
		public function OnlineDefeatScreen(gameManager:OnlineGameManager, playerRankDataArray:Array)
		{
			super();
			
			_gameManager = gameManager;
			
			//Creating Background
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			addChild(transBackground);
			transBackground.alpha = .75;
			
			var playerPercentArray:Array = [];
			var player:OnlinePlayer;
			
			for each (player in _gameManager.playerArray)
			{
				playerPercentArray.push(player.playerPercent);
			}
			
			var highestPercent:int = Math.max.apply(null, playerPercentArray)
			//trace("Highest %: " + highestPercent);
			
			var winningTeam:String;
			var numOfWinners:int = 0;
			
			var surrender:Boolean = false;
			var surrenderingPlayer:String = gameManager.getMain().entityManager.whoSurrenderedGame();

			if (surrenderingPlayer != "")
			{
				surrender = true;
			}
			
			if (surrender)
			{
				for (var i:int=0; i<playerRankDataArray.length; i++)
				{
					if (playerRankDataArray[i].id != surrenderingPlayer)
					{
						winningTeam = _gameManager.playerArray[i].teamName;
						numOfWinners++;
					}
				}
				
				if (numOfWinners > 1)
					winningTeam = "Tie";
			}
			else
			{
				for each (player in _gameManager.playerArray)
				{
					if (player.playerPercent >= highestPercent)
					{
						winningTeam = player.teamName;
						//trace(winningTeam + ": " + player.playerPercent + "%");
						
						numOfWinners++;
					}
				}
				
				if (numOfWinners > 1)
					winningTeam = "Tie";
			}
			
			//trace("winning team = " + winningTeam);
			
			victoryCell = new Sprite();
			victoryCell.pivotX = victoryCell.width/2;
			victoryCell.pivotY = victoryCell.height/2;
			addChild(victoryCell);
			
			var quarterSlice_01:Image;
			var quarterSlice_02:Image;
			var quarterSlice_03:Image;
			var quarterSlice_04:Image
			
			switch(winningTeam)
			{
				case "Green":
					quarterSlice_01 = new Image(Main.assets.getTexture("GreenVictory_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("GreenVictory_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("GreenVictory_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("GreenVictory_Slice"));	
					break;
				case "Red":
					quarterSlice_01 = new Image(Main.assets.getTexture("RedVictory_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("RedVictory_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("RedVictory_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("RedVictory_Slice"));	
					break;
				case "Blue":
					quarterSlice_01 = new Image(Main.assets.getTexture("BlueVictory_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("BlueVictory_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("BlueVictory_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("BlueVictory_Slice"));	
					break;
				case "Yellow":
					quarterSlice_01 = new Image(Main.assets.getTexture("YellowVictory_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("YellowVictory_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("YellowVictory_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("YellowVictory_Slice"));	
					break;
				case "Tie":
					quarterSlice_01 = new Image(Main.assets.getTexture("Tie_Slice"));
					quarterSlice_02 = new Image(Main.assets.getTexture("Tie_Slice"));
					quarterSlice_03 = new Image(Main.assets.getTexture("Tie_Slice"));
					quarterSlice_04 = new Image(Main.assets.getTexture("Tie_Slice"));	
					break;
				default:
					break;
			}
			
			victoryCell.addChild(quarterSlice_01);
			victoryCell.addChild(quarterSlice_02);
			victoryCell.addChild(quarterSlice_03);
			victoryCell.addChild(quarterSlice_04);
			
			quarterSlice_01.pivotX = quarterSlice_01.width/2;
			quarterSlice_01.pivotY = quarterSlice_01.height/2;
			
			quarterSlice_02.pivotX = quarterSlice_02.width/2;
			quarterSlice_02.pivotY = quarterSlice_02.height/2;
			
			quarterSlice_03.pivotX = quarterSlice_03.width/2;
			quarterSlice_03.pivotY = quarterSlice_03.height/2;
			
			quarterSlice_04.pivotX = quarterSlice_04.width/2;
			quarterSlice_04.pivotY = quarterSlice_04.height/2;
			
			quarterSlice_02.rotation = deg2rad(90);
			quarterSlice_02.x = quarterSlice_01.x + quarterSlice_01.width;
			
			quarterSlice_03.rotation = deg2rad(180);
			quarterSlice_03.x = quarterSlice_02.x;
			quarterSlice_03.y = quarterSlice_01.y + quarterSlice_01.height;
			
			quarterSlice_04.rotation = deg2rad(270);
			quarterSlice_04.x = quarterSlice_01.x
			quarterSlice_04.y = quarterSlice_03.y;
			
			victoryCell.x = -victoryCell.width/4;
			victoryCell.y = -victoryCell.height/4;
			
			// add particles
			var texture:Texture = Main.assets.getTexture("Particle");
			var config:XML = XML(new Main.ParticlePEX());
			victoryParticles = new PDParticleSystem(config, texture);
			addChild(victoryParticles);
			Starling.juggler.add(victoryParticles);
			victoryParticles.scaleX = PathogenTest.HD_Multiplyer * 1 / PathogenTest.scaleFactor;
			victoryParticles.scaleY = PathogenTest.HD_Multiplyer * 1 / PathogenTest.scaleFactor;
			victoryParticles.start();
			
			//Adding Victory Text
			var fontSize:int = PathogenTest.HD_Multiplyer * 72 / PathogenTest.scaleFactor;
			var textX:int = PathogenTest.HD_Multiplyer * 400 / PathogenTest.scaleFactor;
			var textY:int = PathogenTest.HD_Multiplyer * 200 / PathogenTest.scaleFactor;
			var textfield:TextField = new TextField(textX, textY, "DEFEAT", "Questrial", fontSize, Color.WHITE);
			textfield.pivotX = textfield.width/2;
			textfield.pivotY = textfield.height/2;
			addChild(textfield);
			textfield.y = -(victoryCell.height/2) - (PathogenTest.hTenPercent * .5);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				viewStats = new Utils.BW_Button(2, 1.0, "View Stats", 32);
			else
				viewStats = new Utils.BW_Button(2, 0.75, "View Stats", 32);
			addChild(viewStats);
			viewStats.y = (victoryCell.height/2) + (PathogenTest.hTenPercent * .5);
			viewStats.addEventListener(TouchEvent.TOUCH, onTouchStatsScreen);
			
			Main.sSoundController.playLossSound();
		}
		public function onTouchStatsScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				viewStats.removeEventListener(TouchEvent.TOUCH, onTouchStatsScreen);
				
				_gameManager.addPostGameScreen();
				startFadeOut();
			}
		}
		public function setViewStatsButtonVisible(isVisible:Boolean):void
		{
			if (viewStats)
			{
				viewStats.visible == isVisible;
			}
		}
		public override function death():void
		{
			if(victoryCell)
			{
				victoryCell.dispose();
				removeChild(victoryCell);
				victoryCell = null;
			}
			
			if(viewStats)
			{
				viewStats.destroy();
				viewStats.dispose();
				removeChild(viewStats);
				viewStats = null;
			}
			
			if(victoryParticles)
			{
				victoryParticles.stop(true);
				victoryParticles.removeFromParent(true);
				Starling.juggler.remove(victoryParticles);
				victoryParticles = null;
			}
			//_gameManager.removeDefeatScreen();
			
			if(_gameManager)
			{
				_gameManager.removeDefeatScreen();
				
				if(quitting)
					_gameManager.Quit(null);
				
				_gameManager = null;
			}
		}
	}
}