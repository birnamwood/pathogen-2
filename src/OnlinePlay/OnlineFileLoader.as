package OnlinePlay
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	public class OnlineFileLoader
	{
		private var _onlineGameManager:OnlineGameManager;
		private var _data:Array;
		
		public function OnlineFileLoader(onlineGameManager:OnlineGameManager)
		{
			_onlineGameManager = onlineGameManager;
			
			var myTextLoader:URLLoader = new URLLoader();
			myTextLoader.addEventListener(Event.COMPLETE, onLoaded);
			myTextLoader.load(new URLRequest("onlineMoves.txt"));
		}
		private function onLoaded(e:Event):void
		{
			// trace("onLoaded file");
			_data = e.target.data.split(/\n/);
			_onlineGameManager.showData();
		}
		
		public function get data():Array { return _data; }
	}
}