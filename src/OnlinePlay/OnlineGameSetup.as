package OnlinePlay
{
	import Campaign.DifficultySelector;
	
	import Maps.MapInfo;
	import Maps.MapManager;
	import Maps.MapPreview;
	
	import Setup.PlayerIndicator;
	
	import UI.Loader;
	import UI.Main;
	import UI.MainMenu;
	
	import Utils.BW_Button;
	import Utils.BW_Selector;
	import Utils.BW_UI;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.display.Scale3Image;
	import feathers.display.Scale9Image;
	import feathers.events.FeathersEventType;
	import feathers.layout.HorizontalLayout;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	
	public class OnlineGameSetup extends BW_UI
	{
		private const HEADING_TEXT_COLOR:uint = 0xAAAAAA;
		
		//Classes
		private var _onlinePlay:OnlinePlay;
		
		//Buttons
		private var _backButton:BW_Button;
		private var _playersButton:BW_Button;
		private var _mapsButton:BW_Button;
		private var _modesButton:BW_Button;
		private var _summeryButton:BW_Button;
		
		//Progression Buttons
		private var _nextButton:BW_Button;
		private var _nextButtonSelector:BW_Selector;
		private var _previousButton:BW_Button;
		
		//Top Bar Information
		private var _playerIndicators:Array = [];
		private var _mapSizeTextfield:TextField;
		private var _mapNameTextfield:TextField;
		
		//Background
		private var screenBackground:Quad
		private var _tab:Image;
		
		//Player Screen
		private var _playerHolder:Sprite;
		private var playerPickerArray:Array = [];
		private var _onlineFriendList:OnlineFriendList;
		
		//Map Screen
		private var _mapHolder:Sprite;
		private var sizePicker:OnlineSizePicker;
		public var sliding:Boolean = false;
		private var container:ScrollContainer;
		private var chosenMapArray:Array;
		private var chosenMapNameArray:Array;
		public var chosenMap:MapPreview;
		public var mapSize:int = -1;
		private var _mapBackground:Quad
		private var _mapSelector:BW_Selector;
		private var _mapPreviewArray:Array = [];
		private var _mapPreviewTutorialArray:Array = [];
		//private var _mapLoadingText:TextField;
		private var _loader:Loader;
		
		//Defaults
		private var defaultMapName:String = "Rectangle";
		private var defaultMapData:String = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,";
		
		// arrays of Maps
		private var smallMapDataArray:Array;
		private var smallMapNameArray:Array;
		private var mediumDataArray:Array;
		private var mediumMapNameArray:Array;
		private var largeDataArray:Array;
		private var largeMapNameArray:Array;
		
		//Summery Screen
		private var _summeryHolder:Sprite;
		private var _startButton:BW_Button
		private var ready:Boolean;
		private var _startGameButtonSelector:BW_Selector;
		private var _summeryMapPreview:MapPreview;
		
		//Modes
		public var modes:Array = [];
		
		//Save
		private var sharedDataObject:SharedObject;
		
		private var startIndex:int;
		
		//iPhone Vars
		private var _buttonHeight:Number;
		
		public function OnlineGameSetup(onlinePlay:OnlinePlay)
		{
			super();
			
			_onlinePlay = onlinePlay;
			sharedDataObject = Main.saveDataObject;
			
			loadSystemMaps();
			
			var defaultMap:MapPreview = new MapPreview(defaultMapName, defaultMapData, 0, null, this, _onlinePlay.main.mapManager.maps[0]);
			chosenMap = defaultMap;
			
			//Creating UI Bands
			var bottomUIBand:Quad;
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				bottomUIBand  = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1.8, Color.BLACK);
			else
				bottomUIBand  = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1.5, Color.BLACK);
			
			bottomUIBand.pivotX = bottomUIBand.width/2;
			bottomUIBand.pivotY = bottomUIBand.height;
			addChild(bottomUIBand);
			bottomUIBand.x = PathogenTest.stageCenter.x;
			bottomUIBand.y = PathogenTest.stageHeight;
			bottomUIBand.alpha = .5;
			
			var topUIBand:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1, Color.BLACK);
			topUIBand.pivotX = topUIBand.width/2;
			addChild(topUIBand);
			topUIBand.x = PathogenTest.stageCenter.x;
			topUIBand.alpha = .5;
			
			//Title Text
			var title:TextField = new TextField(PathogenTest.wTenPercent * 2.5, PathogenTest.hTenPercent * .6, "Game Setup", "Questrial", PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(title);
			title.hAlign = HAlign.LEFT;
			title.x = PathogenTest.wTenPercent * .1;
			title.y = PathogenTest.hTenPercent * .2;
			
			//Player Title text
			var playerText:TextField = new TextField(PathogenTest.wTenPercent, PathogenTest.hTenPercent, "Players:", "Questrial", PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
			addChild(playerText);
			playerText.hAlign = HAlign.LEFT;
			playerText.y = title.y;
			playerText.x = title.x + title.width;
			
			if(PathogenTest.device != PathogenTest.PC)
				playerText.y = PathogenTest.hTenPercent * .2;
			
			//Create player indicators that live on the TOP UI.
			var startingPos:Point = new Point(playerText.x + playerText.width, playerText.y + playerText.height/2)
			
			for (var i:int = 0; i < 4; i++)
			{
				var playerIndicator:PlayerIndicator = new PlayerIndicator();
				addChild(playerIndicator);
				playerIndicator.x = startingPos.x + ((playerIndicator.width * i) * 1.1);
				playerIndicator.y = startingPos.y;
				_playerIndicators.push(playerIndicator);
			}
			
			//Creat top map information
			var mapSizeTextfield:TextField = new TextField(PathogenTest.wTenPercent * .9, PathogenTest.hTenPercent, "Map Size:", "Questrial", PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
			addChild(mapSizeTextfield);
			mapSizeTextfield.hAlign = HAlign.LEFT;
			mapSizeTextfield.y = playerText.y;
			mapSizeTextfield.x = playerIndicator.x + (playerIndicator.width * 1.4);
			
			_mapSizeTextfield = new TextField(PathogenTest.wTenPercent * 1, PathogenTest.hTenPercent, "", "Questrial",PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(_mapSizeTextfield);
			_mapSizeTextfield.hAlign = HAlign.LEFT;
			_mapSizeTextfield.y = mapSizeTextfield.y;
			_mapSizeTextfield.x = mapSizeTextfield.x + mapSizeTextfield.width;
			
			var mapNameTextfield:TextField = new TextField(PathogenTest.wTenPercent * 1.1, PathogenTest.hTenPercent, "Map Name:", "Questrial", PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
			addChild(mapNameTextfield);
			mapNameTextfield.hAlign = HAlign.LEFT;
			mapNameTextfield.y = playerText.y;
			mapNameTextfield.x = PathogenTest.wTenPercent * 7.2;
			
			_mapNameTextfield = new TextField(PathogenTest.wTenPercent * 1.3, PathogenTest.hTenPercent, "", "Questrial", 20 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(_mapNameTextfield);
			_mapNameTextfield.hAlign = HAlign.LEFT;
			_mapNameTextfield.y = mapSizeTextfield.y;
			_mapNameTextfield.x = mapNameTextfield.x + mapNameTextfield.width;
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_buttonHeight = 1;
			else
				_buttonHeight = 0.75;
			
			//Creating Tabs
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_backButton = new BW_Button(0.75, 1, "", 0);
			else
				_backButton = new BW_Button(0.5, 0.75, "", 0);
			_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_Back"), Main.assets.getTexture("Pathogen_BTN_Back_Down"));
			addChild(_backButton);
			_backButton.x = PathogenTest.wTenPercent * .5;
			_backButton.y = bottomUIBand.y - bottomUIBand.height/2;
			_backButton.addEventListener(TouchEvent.TOUCH, quitSetup);
			
			_playersButton = new Utils.BW_Button(2, _buttonHeight, "Players", 32);
			addChild(_playersButton);
			_playersButton.x = PathogenTest.wTenPercent * 2.75;
			_playersButton.y = _backButton.y;
			_playersButton.addEventListener(TouchEvent.TOUCH, openPlayerScreen);
			
			_mapsButton = new Utils.BW_Button(2, _buttonHeight, "Maps", 32);
			addChild(_mapsButton);
			_mapsButton.x = PathogenTest.stageCenter.x;
			_mapsButton.y = _backButton.y;
			_mapsButton.addEventListener(TouchEvent.TOUCH, openMapScreen);
			
			_summeryButton = new Utils.BW_Button(2, _buttonHeight, "Summary", 32);
			addChild(_summeryButton);
			_summeryButton.x = PathogenTest.wTenPercent * 7.5;
			_summeryButton.y = _backButton.y;
			_summeryButton.addEventListener(TouchEvent.TOUCH, openSummeryScreen);
			
			//Creating screen background
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			//Creating screen background
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				screenBackground = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 7.2, 0x222245);
			else
				screenBackground = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 7.5, 0x222245);
			
			screenBackground.pivotX = screenBackground.width/2;
			screenBackground.color = 0x222245;
			screenBackground.alpha = .5;
			screenBackground.x = PathogenTest.stageCenter.x;
			screenBackground.y = topUIBand.height;
			addChild(screenBackground);
			
			//Create "tab" indicator
			_tab = new Image(Main.assets.getTexture("Indicator"))
			_tab.pivotX = _tab.width/2;;
			_tab.x = _playersButton.x;
			_tab.y = screenBackground.y + screenBackground.height + (_tab.height * .2);
			addChildAt(_tab, 1);
			
			//Create next/back buttons
			_nextButton = new BW_Button(1.25,_buttonHeight, "", 24);
			_nextButton.addImage(Main.assets.getTexture("BTN_Next"), Main.assets.getTexture("BTN_Next_Down"));
			addChild(_nextButton);
			_nextButton.x = PathogenTest.wTenPercent * 9.1;
			_nextButton.y = PathogenTest.stageCenter.y - screenBackground.height/2 + (_nextButton.height * .8);
			_nextButton.visible = false;
			
			// highlight next button
			_nextButtonSelector = new BW_Selector(1.3, _buttonHeight + .05);
			addChild(_nextButtonSelector);
			setChildIndex(_nextButton, numChildren-1);
			_nextButtonSelector.x = _nextButton.x;
			_nextButtonSelector.y = _nextButton.y;
			_nextButtonSelector.visible = false;
			
			_previousButton = new BW_Button(1.25,_buttonHeight, "", 24);
			_previousButton.addImage(Main.assets.getTexture("BTN_Back"), Main.assets.getTexture("BTN_Back_Down"));
			addChild(_previousButton);
			_previousButton.x = PathogenTest.wTenPercent * .9;
			_previousButton.y = PathogenTest.stageCenter.y - screenBackground.height/2 + (_nextButton.height * .8);
			_previousButton.visible = false;
			
			openPlayerScreen(null);
		}
		private function openPlayerScreen($e:TouchEvent):void
		{
			if($e)
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				var open:Boolean = true;
			
			if (touch || open)
			{
				if(_playerHolder == null)
				{
					_playerHolder = new Sprite();
					addChild(_playerHolder);
					
					//Locations for where they player pickers go.
					var playerOnePickerLocation:Point = new Point(PathogenTest.stageCenter.x - (PathogenTest.wTenPercent * 1.6), PathogenTest.stageCenter.y - PathogenTest.hTenPercent* 2.6)
					var playerTwoPickerLocation:Point = new Point(PathogenTest.stageCenter.x + (PathogenTest.wTenPercent * 1.6), PathogenTest.stageCenter.y - PathogenTest.hTenPercent * 2.6)
					var playerThreePickerLocation:Point = new Point(PathogenTest.stageCenter.x + (PathogenTest.wTenPercent * 1.6), PathogenTest.stageCenter.y + PathogenTest.hTenPercent* .8);
					var playerFourPickerLocation:Point = new Point(PathogenTest.stageCenter.x - (PathogenTest.wTenPercent * 1.6), PathogenTest.stageCenter.y + PathogenTest.hTenPercent* .8);
					
					var playerLocationArray_01:Array = [playerOnePickerLocation,playerTwoPickerLocation, playerThreePickerLocation, playerFourPickerLocation];
					
					var maxNumOfPlayers:int = 4;
					
					//Create all potential players.
					for (var i:int = 0; i < maxNumOfPlayers; i++)
					{
						var playerPicker:OnlinePlayerPicker = new OnlinePlayerPicker(i, this);
						_playerHolder.addChild(playerPicker);
						playerPickerArray.push(playerPicker);
						playerPicker.x = playerLocationArray_01[i].x;
						playerPicker.y = playerLocationArray_01[i].y;
						
						if(i == 0)
							playerPicker.playerName = Main.sOnlineNickname;
					}
					
					_playerHolder.y = PathogenTest.hTenPercent * .6;
				}
				
				//Hide all other tabs except this one.
				if(_playerHolder)
					_playerHolder.visible = true;
				if(_mapHolder)
					_mapHolder.visible = false;
				if(_summeryHolder)
					_summeryHolder.visible = false;
				
				_tab.x = _playersButton.x;
				
				_nextButton.visible = true;
				_previousButton.visible = false;
				
				//Remove Potential other listeners
				_nextButton.removeEventListener(TouchEvent.TOUCH, openSummeryScreen);
				
				_nextButton.addEventListener(TouchEvent.TOUCH, openMapScreen);
				
				_playerIndicators[0].changeColor(0);
				
				var active:int = 0;
				//Updates the quick-player indicatiors on the TOP UI to reflect the choices made.
				for (i = 0; i < _playerIndicators.length; i++)
				{
					var currentPlayerPicker:OnlinePlayerPicker = playerPickerArray[i];
					
					if(currentPlayerPicker.active)
						active++;
				}
				if (active >= 2)
					_nextButtonSelector.visible = true;
				else
					_nextButtonSelector.visible = false
			}
		}
		public function addOnlineFriendList(onlinePlayerPicker:OnlinePlayerPicker):void
		{
			_onlineFriendList = new OnlineFriendList(onlinePlayerPicker, this);
			addChild(_onlineFriendList);
			_onlineFriendList.x = PathogenTest.stageCenter.x;
			_onlineFriendList.y = PathogenTest.stageCenter.y;
		}
		public function removeOnlineFriendList():void
		{
			_onlineFriendList.dispose();
			removeChild(_onlineFriendList);
			_onlineFriendList = null;
		}
		public function updatePlayerIndicators():void
		{
			_playerIndicators[1].changeColor(1);
			
			var active:int = 0;
			//Updates the quick-player indicatiors on the TOP UI to reflect the choices made.
			for (var i:int=0; i<_playerIndicators.length; i++)
			{
				var currentPlayerPicker:OnlinePlayerPicker = playerPickerArray[i];
				
				if(currentPlayerPicker.active)
					active++;
			}
			if (active >= 2)
				_nextButtonSelector.visible = true;
			else
				_nextButtonSelector.visible = false
		}
		private function openMapScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(_mapHolder == null)
				{
					//Create the map screen if not already created.
					_mapHolder = new Sprite();
					addChild(_mapHolder);
					
					sizePicker = new OnlineSizePicker(this);
					_mapHolder.addChild(sizePicker);
					sizePicker.x = PathogenTest.stageCenter.x;
					sizePicker.y = PathogenTest.stageCenter.y - screenBackground.height/2 + (sizePicker.height * .8);				
					selectedSmallMap($e);
				}
				
				//Hide all other tabs except this one.
				if(_mapHolder)
					_mapHolder.visible = true;
				if(_playerHolder)
					_playerHolder.visible = false;
				if(_summeryHolder)
					_summeryHolder.visible = false;
				
				_tab.x = _mapsButton.x;
				
				_nextButton.visible = true;
				_previousButton.visible = true;
				
				//Remove Potential other listeners
				_nextButton.removeEventListener(TouchEvent.TOUCH, openMapScreen);
				_previousButton.removeEventListener(TouchEvent.TOUCH, openMapScreen);
				
				_nextButton.addEventListener(TouchEvent.TOUCH, openSummeryScreen);
				_previousButton.addEventListener(TouchEvent.TOUCH, openPlayerScreen);
				
				if (_mapSelector)
				{
					if (_mapSelector.visible)
						_nextButtonSelector.visible = true;
					else
						_nextButtonSelector.visible = false;
				}
				else
					_nextButtonSelector.visible = false;
			}
		}
		private function addLoader():void
		{
			_loader = new Loader("Loading Maps", 24);
			addChild(_loader);
			_loader.x = PathogenTest.stageCenter.x;
			_loader.y = PathogenTest.stageCenter.y;
		}
		public function selectedSmallMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && mapSize != 0)
			{
				//Player has selected a small map.
				sizePicker.whichPicker("Small Map");
				mapSize = 0;
				chosenMapArray = smallMapDataArray;
				chosenMapNameArray = smallMapNameArray;
				_mapSizeTextfield.text = "Small";
				startIndex = MapManager.START_SMALL_INDEX;
				
				//_mapLoadingText.visible = true;
				
				addLoader();
				
				Starling.juggler.delayCall(addMaps, 0.05);
			}
		}
		public function selectedMediumMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && mapSize != 1)
			{
				//Player has selected a medium Map
				sizePicker.whichPicker("Medium Map");
				mapSize = 1;
				chosenMapArray = mediumDataArray;
				chosenMapNameArray = mediumMapNameArray;
				_mapSizeTextfield.text = "Medium";
				startIndex = MapManager.START_MED_INDEX;
				
				addLoader();
				//_mapLoadingText.visible = true;
				
				Starling.juggler.delayCall(addMaps, 0.05);
			}
		}
		public function selectedLargeMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && mapSize != 2)
			{
				//Player has selected a large Map
				sizePicker.whichPicker("Large Map");
				mapSize = 2;
				chosenMapArray = largeDataArray;
				chosenMapNameArray = largeMapNameArray;
				_mapSizeTextfield.text = "Large";
				startIndex = MapManager.START_LARGE_INDEX;
				
				addLoader();
				//_mapLoadingText.visible = true;
				
				Starling.juggler.delayCall(addMaps, 0.05);
			}
		}
		private function addMaps():void
		{
			//setChildIndex(_mapLoadingText, numChildren-1);
			
			//Add the scrollable map container object.
			var layout:HorizontalLayout = new HorizontalLayout();
			
			if(container != null)
			{
				for each(var map_Preview:MapPreview in _mapPreviewArray)
				{
					map_Preview.garbageCollection();
					map_Preview.dispose();
					container.removeChild(map_Preview);
					map_Preview = null;
				}
				
				_mapHolder.removeChild(container);
				container.dispose();
				container = null;
			}
			
			
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20, 20, 20, 20));
			
			if(_mapBackground != null)
				_mapHolder.removeChild(_mapBackground);
			
			//Add a black background so they show up better.
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_mapBackground = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 5.2, 0x000000);
			else
				_mapBackground = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 5.5, 0x000000);
			
			_mapBackground.pivotX = _mapBackground.width/2;
			_mapBackground.pivotY = _mapBackground.height/2;
			_mapBackground.alpha = .5;
			_mapBackground.x = PathogenTest.stageCenter.x;
			_mapBackground.y = PathogenTest.stageCenter.y + PathogenTest.hTenPercent * .45;
			_mapHolder.addChild(_mapBackground);
			
			//Create container for scrollable objects.
			container = new ScrollContainer();
			container.layout = layout;
			container.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			container.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			container.scrollerProperties.snapScrollPositionsToPixels = true;
			_mapHolder.addChild(container);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				container.visible = false;
			
			container.y = PathogenTest.stageCenter.y * 0.5;
			
			container.width = PathogenTest.stageWidth;
			container.height = _mapBackground.height;
			layout.gap = PathogenTest.wTenPercent/2;
			
			_mapPreviewArray = [];
			_mapPreviewTutorialArray = [];
			
			//Create all map previews
			for (var i:int = 0; i < 6; i++)
			{
				var mapPreview:MapPreview = new MapPreview(chosenMapNameArray[i], chosenMapArray[i], mapSize, null, this, _onlinePlay.main.mapManager.maps[startIndex]);
				mapPreview.scaleX = .7;
				mapPreview.scaleY = .7;
				container.addChild(mapPreview);
				_mapPreviewArray.push(mapPreview);
				startIndex++;
			}
			
			layout.paddingTop = mapPreview.height * .6;
			layout.paddingLeft = mapPreview.width;
			
			if(_mapSelector != null)
				_mapHolder.removeChild(_mapSelector);
			
			_mapSelector = new BW_Selector(4,5);
			container.addChild(_mapSelector);
			_mapSelector.visible = false;
			
			const texture_02:Texture = Main.assets.getTexture("scale9_outline");
			const textures_02:Scale9Textures = new Scale9Textures(texture_02, new Rectangle(20, 20, 20, 20));
			
			container.addEventListener(FeathersEventType.SCROLL_COMPLETE, scrollComplete);
			container.addEventListener(Event.SCROLL, scrolling);
			Starling.juggler.delayCall(stopSliding, 0.25);
			
			setChildIndex(_backButton, numChildren-1);
			
			//_mapLoadingText.visible = false;
			_loader.startFadeOut();
			
			Starling.juggler.delayCall(stopSliding, .5);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				Starling.juggler.delayCall(revealContainer, .5);
		}
		private function scrollComplete($e:Event):void
		{
			sliding = false;
		}
		private function scrolling($e:Event):void
		{
			sliding = true;
		}
		private function stopSliding():void
		{
			sliding = false;
		}
		private function revealContainer():void
		{
			container.visible = true;
		}
		public function selectLevel(whatPreview:MapPreview):void
		{
			_mapSelector.visible = true;
			_nextButtonSelector.visible = true;
			
			//Player has selected a map from the list.
			_mapSelector.x = whatPreview.x;
			_mapSelector.y = whatPreview.y;
			
			_mapNameTextfield.text = whatPreview.mapName;
			
			chosenMap = whatPreview;
		}
		private function loadSystemMaps():void
		{
			var i:int;
			
			// small maps
			smallMapDataArray = [];
			smallMapNameArray = [];
			for (i=0; i<MapInfo.smallMapDataArray.length; i++)
			{
				smallMapDataArray.push(MapInfo.smallMapDataArray[i]);
				smallMapNameArray.push(MapInfo.smallMapNameArray[i]);
			}
			
			// medium maps
			mediumDataArray = [];
			mediumMapNameArray = [];
			for (i=0; i<MapInfo.mediumDataArray.length; i++)
			{
				mediumDataArray.push(MapInfo.mediumDataArray[i]);
				mediumMapNameArray.push(MapInfo.mediumMapNameArray[i]);
			}
			
			// large maps
			largeDataArray = [];
			largeMapNameArray = [];
			for (i=0; i<MapInfo.largeMapNameArray.length; i++)
			{
				largeDataArray.push(MapInfo.largeDataArray[i]);
				largeMapNameArray.push(MapInfo.largeMapNameArray[i]);
			}
		}
		private function quitSetup($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if(alpha < 1.0) return; 
			
			if (touch)
			{
				//Leave game setup & return to main
				_backButton.removeEventListener(TouchEvent.TOUCH, quitSetup);
				
				for each (var mapPreview:MapPreview in container)
				{
					mapPreview.removeTouchEvents();
				}
				
				_onlinePlay.showUI();
				
				startFadeOut();
			}
		}
		public function getNextUnusedColor(inPlayer:int):int
		{
			//Get the next available color.
			var greenTaken:Boolean = false;
			var redTaken:Boolean = false;
			var blueTaken:Boolean = false;
			var yellowTaken:Boolean = false;
			
			for(var i:int = 0; i<playerPickerArray.length; i++)
			{
				if (i != inPlayer)
				{
					if (playerPickerArray[i].active)
					{
						if (playerPickerArray[i].chosenColor == 0)
							greenTaken = true;
						else if (playerPickerArray[i].chosenColor == 1)
							redTaken = true;
						else if (playerPickerArray[i].chosenColor == 2)
							blueTaken = true;
						else if (playerPickerArray[i].chosenColor == 3)
							yellowTaken = true;
					}
				}
			}
			
			var nextColor:int = -1;
			if (!greenTaken)
				nextColor = 0;
			else if (!redTaken)
				nextColor = 1;
			else if (!blueTaken)
				nextColor = 2;
			else if (!yellowTaken)
				nextColor = 3;
			return nextColor;
		}
		public function iPhonePlayerHolderReposition(isVisible:Boolean):void
		{
			if(isVisible)
			{
				_playerHolder.getChildAt(0).y += PathogenTest.hTenPercent * .2;
				_playerHolder.getChildAt(1).y += PathogenTest.hTenPercent * .2;
				
				_playerHolder.getChildAt(2).y -= PathogenTest.hTenPercent * .3;
				_playerHolder.getChildAt(3).y -= PathogenTest.hTenPercent * .3;
				
				return;
			}
			
			_playerHolder.getChildAt(0).y -= PathogenTest.hTenPercent * .2;
			_playerHolder.getChildAt(1).y -= PathogenTest.hTenPercent * .2;
			
			_playerHolder.getChildAt(2).y += PathogenTest.hTenPercent * .3;
			_playerHolder.getChildAt(3).y += PathogenTest.hTenPercent * .3;		
		}
		public function beginLevel($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_startButton.removeEventListener(TouchEvent.TOUCH, beginLevel);
				
				//Player has clicked START GAME Button. Collect the nessessary information, close Game Setup & launch the game.
				var playerLocations:Array = [];
				var playerColors:Array = [];
				var playerAI:Array = [];
				var playerIDs:Array = [];
				
				for each (var playerPicker:OnlinePlayerPicker in playerPickerArray)
				{
					if(playerPicker.active)
					{
						playerLocations.push(playerPicker.whatPlayer);
						playerIDs.push(playerPicker.playerID);
					}
				}
				if(playerLocations.length == 0)
				{
					playerColors.push(0);
					playerLocations.push(0);
				}
				
				//Main.updateDebugText("There are " + playerIDs.length + " players in this game");
				//Main.updateDebugText("Player 1: " + playerIDs[1]);
				
				_onlinePlay.startNewCustomGame(playerIDs[1], chosenMap.map);
				
				startFadeOut();
			}
		}
		public function checkReady():void
		{
			//Check to see if a player is active.
			var playerSelected:Boolean;
			var mapSelected:Boolean;
			
			var playerPicker:OnlinePlayerPicker = playerPickerArray[1];
			
			if(playerPicker.active)
				playerSelected = true;
			
			//If there is a chosen map
			if(chosenMap != null)
				mapSelected = true;
			
			//If both are good, change ready to true.
			if(mapSelected && playerSelected)
				ready = true;
			
			if(ready)
			{
				_startButton.alpha = 1;
				_startButton.addEventListener(TouchEvent.TOUCH, beginLevel);
				_startGameButtonSelector.visible = true;
			}
		}
		private function openSummeryScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(_summeryHolder != null)
				{
					_summeryMapPreview.garbageCollection();
					_summeryMapPreview.dispose();
					_summeryMapPreview.removeChild(_summeryMapPreview);
					_summeryMapPreview = null;
					
					_summeryHolder.dispose();
					removeChild(_summeryHolder);
					_summeryHolder = null;
				}
				
				_summeryHolder = new Sprite();
				addChild(_summeryHolder);
				
				const texture:Texture = Main.assets.getTexture("scale9_White");
				const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20, 20, 20, 20));
				
				//Add a black background so they show up better.
				var transBackground:Scale9Image = new Scale9Image(textures);
				transBackground.width = PathogenTest.wTenPercent * 6.6;
				transBackground.height = PathogenTest.hTenPercent * 6.6;
				transBackground.pivotX = transBackground.width/2;
				transBackground.pivotY = transBackground.height/2;
				transBackground.color = 0x000000;
				transBackground.alpha = .5;
				transBackground.x = PathogenTest.stageCenter.x + PathogenTest.wTenPercent * .1;
				transBackground.y = PathogenTest.stageCenter.y - (PathogenTest.hTenPercent * 1.4);
				_summeryHolder.addChild(transBackground);
				
				//Create a large preview of the map they are going to play on.
				_summeryMapPreview = new MapPreview(chosenMap.mapName, chosenMap.originalData, chosenMap.mapSize, null, this);
				_summeryMapPreview.scaleX = .7;
				_summeryMapPreview.scaleY = .7;
				_summeryMapPreview.x = PathogenTest.wTenPercent * 4;
				_summeryMapPreview.y = PathogenTest.hTenPercent * 3;
				_summeryHolder.addChild(_summeryMapPreview);
				
				var playerTitle:TextField = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent * .7, "Players", "Dekar", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
				_summeryHolder.addChild(playerTitle);
				playerTitle.x = _summeryMapPreview.x + (_summeryMapPreview.width * .6);
				playerTitle.y = PathogenTest.hTenPercent * .45 + (i * playerTitle.height);
				playerTitle.hAlign = HAlign.LEFT;
				
				//For every active player, list their name & color.
				for (var i:int = 0; i < playerPickerArray.length; i++)
				{
					var playerPicker:OnlinePlayerPicker = playerPickerArray[i];
					
					if(playerPicker.active)
					{
						var playerString:String = "";
						playerString = playerPicker.playerName;
						var playerTextField:TextField = new TextField(PathogenTest.wTenPercent * 1.5, PathogenTest.hTenPercent/2, playerString, "Dekar", PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
						_summeryHolder.addChild(playerTextField);
						playerTextField.hAlign = HAlign.LEFT;
						playerTextField.x = playerTitle.x;
						playerTextField.y = playerTitle.y + playerTitle.height + (i * playerTextField.height);
						
						var colorIndicatior:PlayerIndicator = new PlayerIndicator();
						_summeryHolder.addChild(colorIndicatior);
						colorIndicatior.changeColor(playerPicker.chosenColor);
						colorIndicatior.x = playerTextField.x + playerTextField.width;
						colorIndicatior.y = playerTextField.y + colorIndicatior.height/2;
						
					}
				}
				
				//Add additional details about the game.
				var gameLengthTitle:TextField  = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent * .5, "Game Length", "Dekar", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
				_summeryHolder.addChild(gameLengthTitle);
				gameLengthTitle.x = playerTitle.x;
				gameLengthTitle.y = PathogenTest.hTenPercent * 3.3;
				gameLengthTitle.hAlign = HAlign.LEFT
				
				var gameLength:TextField  = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent/2, "Unlimited", "Dekar", PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
				_summeryHolder.addChild(gameLength);
				gameLength.x = playerTitle.x;
				gameLength.y = gameLengthTitle.y + gameLengthTitle.height;
				gameLength.hAlign = HAlign.LEFT;
				
				var turnLengthTitle:TextField  = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent * .5, "Turn Length", "Dekar", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
				_summeryHolder.addChild(turnLengthTitle);
				turnLengthTitle.x = playerTitle.x;
				turnLengthTitle.y = gameLength.y + gameLength.height;
				turnLengthTitle.hAlign = HAlign.LEFT;
				
				var turnLength:TextField  = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent/2, "Unlimited", "Dekar", PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
				_summeryHolder.addChild(turnLength);
				turnLength.x = playerTitle.x;
				turnLength.y = turnLengthTitle.y + turnLengthTitle.height;
				turnLength.hAlign = HAlign.LEFT;
				
				//Update text
				if(sharedDataObject.data.GameLength != null)
				{
					var gameRounds:int = sharedDataObject.data.GameLength;
					
					if(gameRounds == 100)
						gameLength.text = "Unlimited";
					else
						gameLength.text = gameRounds.toString() + " Rounds";
				}
				else
					gameLength.text = "Unlimited";
				
				//Update text
				if(sharedDataObject.data.TurnLength != null)
				{
					var turnNumber:int = sharedDataObject.data.TurnLength;
					
					if(turnNumber == 30)
						turnLength.text = "Unlimited";
					else
						turnLength.text = turnNumber.toString() + " Sec's";
				}
				else
					turnLength.text = "Unlimited";
				
				//Create start button.
				_startButton = new BW_Button(3,1, "START GAME", 34);
				_summeryHolder.addChild(_startButton);
				_startButton.x = _summeryMapPreview.width * 1.3;
				_startButton.y = _summeryMapPreview.y + (_summeryMapPreview.height * .65);
				_startButton.alpha = .5;
				
				// highlight start button
				if(_startGameButtonSelector != null)
					_summeryHolder.removeChild(_startGameButtonSelector);
				_startGameButtonSelector = new BW_Selector(3.05, 1.05);
				_summeryHolder.addChild(_startGameButtonSelector);
				_summeryHolder.setChildIndex(_startButton, numChildren-1);
				_startGameButtonSelector.x = _startButton.x;
				_startGameButtonSelector.y = _startButton.y;
				_startGameButtonSelector.visible = false;
				
				_summeryHolder.y = PathogenTest.hTenPercent * 1.2;
				
				//Hide all other screens.
				if(_playerHolder)
					_playerHolder.visible = false;
				if(_mapHolder)
					_mapHolder.visible = false;
				
				_tab.x = _summeryButton.x;
				_nextButton.visible = false;
				_nextButtonSelector.visible = false;
				_previousButton.visible = true;
				
				_previousButton.removeEventListener(TouchEvent.TOUCH, openPlayerScreen);
				_previousButton.addEventListener(TouchEvent.TOUCH, openMapScreen);
				
				checkReady();
			}
		}
		
		override public function death():void
		{
			//Garbage Collection
			for each (var playerPicker:OnlinePlayerPicker in playerPickerArray)
			{
				playerPicker.garbageCollection();
				playerPicker.dispose();
				removeChild(playerPicker);
				playerPicker = null;
			}
			
			playerPickerArray = [];
			_playerHolder.dispose();
			removeChild(_playerHolder);
			_playerHolder = null;
			
			if(_mapHolder)
			{
				sizePicker.garbageCollection();
				sizePicker.dispose();
				_mapHolder.removeChild(sizePicker);
				sizePicker = null;
				
				for each(var mapPreview:MapPreview in _mapPreviewArray)
				{
					mapPreview.garbageCollection();
					mapPreview.dispose();
					container.removeChild(mapPreview);
					mapPreview = null;
				}
				
				_mapPreviewArray = [];
				
				if(_mapSelector)
				{
					_mapSelector.destroy();
					_mapSelector.dispose();
					container.removeChild(_mapSelector);
					_mapSelector = null;
				}
				
				container.dispose();
				_mapHolder.removeChild(container);
				container = null;
				
				_mapHolder.dispose();
				removeChild(_mapHolder);
				_mapHolder = null;
			}
			if(_summeryHolder)
			{
				_summeryMapPreview.garbageCollection();
				_summeryMapPreview.dispose();
				_summeryMapPreview.removeChild(_summeryMapPreview);
				_summeryMapPreview = null;
				
				_summeryHolder.dispose();
				removeChild(_summeryHolder);
				_summeryHolder = null;
			}
			
			if(_nextButtonSelector)
			{
				_nextButtonSelector.dispose();
				removeChild(_nextButtonSelector);
				_nextButtonSelector = null;
			}
			
			if(_startGameButtonSelector)
			{
				_startGameButtonSelector.dispose();
				removeChild(_startGameButtonSelector);
				_startGameButtonSelector = null;
			}
			
			//bottom UI Buttons.
			if (_backButton)
			{
				_backButton.destroy();
				removeChild(_backButton);
				_backButton = null;
			}
			
			if (_playersButton)
			{
				_playersButton.destroy();
				removeChild(_playersButton);
				_playersButton = null;
			}
			
			if (_mapsButton)
			{
				_mapsButton.destroy();
				removeChild(_mapsButton);
				_mapsButton = null;
			}
			
			if (_summeryButton)
			{
				_summeryButton.destroy();
				removeChild(_summeryButton);
				_summeryButton = null;
			}
			
			if(_loader)
			{
				_loader.dispose();
				removeChild(_loader);
				_loader = null;
			}
			
			chosenMap = null;
			
			_onlinePlay.removeOnlineGameSetup();
			_onlinePlay = null;
		}
		
		public function get onlinePlay():OnlinePlay {return _onlinePlay};
	}
}