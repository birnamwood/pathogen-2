package OnlinePlay
{
	import UI.Main;
	
	import com.milkmangames.nativeextensions.*;
	import com.milkmangames.nativeextensions.events.*;
	
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	public class PushNotification
	{
		private const GCM_PROJECT_NUMBER:String="941700649764";
		private var _main:Main;
		public static var sEnabled:Boolean;
		public static var sTokenID:String;
		public static var sServerAddress:String;
		public static var sAndroidServerAddress:String;
		
		public function PushNotification(main:Main)
		{
			//Main.debugText.text = "PushNotification constructor";
			
			_main = main;
			sEnabled = false;
			sTokenID = "";
			
			if (EasyPush.isSupported() && EasyPush.areNotificationsAvailable())
			{
				sEnabled = true;
				//Main.updateDebugText("EasyPush is supported");
			}
			
			if (sEnabled)
			{
				EasyPush.initManual(GCM_PROJECT_NUMBER, true);

				//EasyPush.initAirship("zAL62lyhST2uqgn71R5Rfw", "HGZjom5CRBqnTIPFmXaF-w", "", true, true, true);
				
				//Main.updateDebugText("initialized");
				
				EasyPush.manual.addEventListener(PNMEvent.ALERT_DISMISSED, onAlertDismissed);
				EasyPush.manual.addEventListener(PNMEvent.FOREGROUND_NOTIFICATION, onNotification);
				EasyPush.manual.addEventListener(PNMEvent.RESUMED_FROM_NOTIFICATION, onNotificationFromResume);
				EasyPush.manual.addEventListener(PNMEvent.TOKEN_REGISTERED, onTokenRegistered);
				EasyPush.manual.addEventListener(PNMEvent.TOKEN_REGISTRATION_FAILED, onRegFailed);	
				
				//sServerAddress = "http://72.15.31.115/pathogen_APNS.php";
				
				//Main.updateDebugText("event listeners added");
				
				//EasyPush.airship.addEventListener(PNAEvent.ALERT_DISMISSED, onAlertDismissed);
				//EasyPush.airship.addEventListener(PNAEvent.FOREGROUND_NOTIFICATION, onNotification);
				//EasyPush.airship.addEventListener(PNAEvent.RESUMED_FROM_NOTIFICATION, onNotification);
			}
		}
		
		private function onAlertDismissed(event:PNMEvent):void
		{
			//trace("Alert dismissed, payload=" + event.rawPayload + "," + event.badgeValue + "," + event.title);
			//Main.updateDebugText("Alert dismissed, payload=" + event.rawPayload + "," + event.badgeValue + "," + event.title);
		}
		
		private function onNotification(event:PNMEvent):void
		{
			//trace("new notification, type=" + event.type + "," + event.rawPayload + "," + event.badgeValue + "," + event.title);
			//Main.updateDebugText("new notification, type=" + event.type + "," + event.rawPayload + "," + event.badgeValue + "," + event.title);
			//_main.pushForceOnlineGameRefresh();
		}
		
		private function onNotificationFromResume(event:PNMEvent):void
		{
			//Main.updateDebugText("resume notification, type=" + event.type + "," + event.rawPayload + "," + event.badgeValue + "," + event.title);
		}
		
		private function onTokenRegistered(event:PNEvent):void
		{
			//trace("token registered:" + event.token);
			//Main.updateDebugText("token registered: " + event.token);
			sTokenID = event.token.toString();
			
			/*
			if (!Main.isIOS())
			{
				sendPush(sTokenID, "This is a test push");
			}
			*/
		}
		
		private function onRegFailed(event:PNEvent):void
		{
			//trace("reg failed: " + event.errorId + "=" + event.errorMsg);
			//Main.updateDebugText("reg failed: " + event.errorId + "=" + event.errorMsg);
		}
		
		public static function sendPush(deviceToken:String, message:String):void
		{
			if (sEnabled)
			{
				//Main.updateDebugText("send push to " + deviceToken + " message = " + message);
				
				var urlreq:URLRequest;
				// the device that the push notification is going to is Android
				if (deviceToken.length == 162)
				{
					urlreq = new URLRequest (sAndroidServerAddress);
					
					//Main.updateDebugText("using server " + sAndroidServerAddress);
				}
				// otherwise it is going to an iOS device
				else
				{
					urlreq = new URLRequest (sServerAddress);
					
					//Main.updateDebugText("using server " + sServerAddress);
				}
				urlreq.method = URLRequestMethod.POST; 
				
				var urlvars:URLVariables = new URLVariables(); 
				urlvars.deviceToken = deviceToken;
				urlvars.message = message;
				urlreq.data = urlvars;          
				
				var loader:URLLoader = new URLLoader (urlreq); 
				loader.dataFormat = URLLoaderDataFormat.VARIABLES; 
				loader.load(urlreq);
			}
		}
		
		/*
		private function onAlertDismissed(event:PNAEvent):void
		{
		trace("Alert dismissed, payload=" + event.rawPayload + "," + event.badgeValue + "," + event.title);" +
		}
		*/
	}
}