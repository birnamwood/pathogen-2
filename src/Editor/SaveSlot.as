package Editor
{
	import UI.Main;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class SaveSlot extends Sprite
	{
		//Parent Class
		public var mapSlotScreen:MapSlotScreen;
		
		public var location:int;
		public var mapName:String;
		public var mapData:String;	
		
		public function SaveSlot(savedData:String, mapSlotScreen:MapSlotScreen, location:int)
		{
			this.mapSlotScreen = mapSlotScreen;
			this.location = location + 1;
			
			var arrayOfSavedData:Array = savedData.split(":");
			mapName = arrayOfSavedData[0];
			mapData = arrayOfSavedData[1];
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 2;
			background.height = PathogenTest.hTenPercent * .8;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			
			var fontSize:int = 24 / PathogenTest.scaleFactor;
			var textField:TextField = new TextField(background.width, background.height, mapName, "Dekar", fontSize, Color.WHITE);
			textField.pivotX = textField.width/2;
			textField.pivotY = textField.height/2;
			addChild(textField);
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		private function onTouch($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				mapSlotScreen.selectedSaveSlot(this);
			}
		}
		public function death():void
		{
			removeEventListener(TouchEvent.TOUCH, onTouch);
			mapSlotScreen = null;
		}
	}
}