package Editor
{
	import UI.Achievements;
	import UI.Main;
	import UI.fSprite;
	
	import Utils.BW_Button;
	import Utils.BW_SocialButton;
	import Utils.BW_UI;
	
	import feathers.controls.TextInput;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.FileReference;
	import flash.net.SharedObject;
	import flash.utils.ByteArray;
	
	import starling.display.Button;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import UI.AchievementPopUp;
	
	public class MapEditor extends BW_UI
	{
		//Classes
		private var main:Main;
		private var mapSlotScreen:MapSlotScreen;
		private var _saveScreen:SaveScreen;
		private var _overwriteScreen:OverwriteScreen;
		private var _mapCreatedScreen:MapCreatedScreen;
		
		// scale constants
		private const SMALL_SCALE:Number = 1.0;
		private const MEDIUM_SCALE:Number = 0.8;
		private const LARGE_SCALE:Number = 0.675;
		private const LARGE_SCALE_IPHONE:Number = 0.65;
		
		// rows and columns
		private const SMALL_ROWS:int = 16;
		private const SMALL_COLS:int = 14;
		private const MED_ROWS:int = 20;
		private const MED_COLS:int = 18;
		private const LARGE_ROWS:int = 24;
		private const LARGE_COLS:int = 22;
		
		private const NUM_SAVE_SLOTS : int = 6; //per map size
		
		// map info
		private var smallRectangleString:String = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,"
		private var mediumRectangleString:String = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,";
		private var largeRectangleString:String = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,";
		//private var largeRectangleString:String = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,10,9,0,9,3,9,0,0,0,0,9,3,9,0,9,10,3,3,4,4,3,10,9,0,0,0,9,0,0,0,0,0,0,9,0,0,0,9,10,3,4,4,10,9,21,21,21,21,0,1,0,1,1,0,1,0,21,21,21,21,9,10,4,4,9,0,21,21,21,21,0,0,0,0,0,0,0,0,21,21,21,21,0,9,4,4,0,0,21,21,21,21,0,0,0,0,0,0,0,0,21,21,21,21,0,0,4,4,9,0,21,21,21,21,0,1,0,1,1,0,1,0,21,21,21,21,0,9,4,4,3,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,3,4,4,9,22,22,22,22,22,12,12,12,12,12,12,12,12,22,22,22,22,22,9,4,4,0,22,2,22,2,22,12,12,12,12,12,12,12,12,22,2,22,2,22,0,4,4,0,22,22,22,22,22,12,12,12,12,12,12,12,12,22,22,22,22,22,0,4,4,0,22,2,22,2,22,12,12,12,12,12,12,12,12,22,2,22,2,22,0,4,4,0,22,2,22,2,22,12,12,12,12,12,12,12,12,22,2,22,2,22,0,4,4,0,22,22,22,22,22,12,12,12,12,12,12,12,12,22,22,22,22,22,0,4,4,0,22,2,22,2,22,12,12,12,12,12,12,12,12,22,2,22,2,22,0,4,4,9,22,22,22,22,22,12,12,12,12,12,12,12,12,22,22,22,22,22,9,4,4,3,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,3,4,4,9,0,21,21,21,21,0,1,0,1,1,0,1,0,21,21,21,21,0,9,4,4,0,0,21,21,21,21,0,0,0,0,0,0,0,0,21,21,21,21,0,0,4,4,9,0,21,21,21,21,0,0,0,0,0,0,0,0,21,21,21,21,0,9,4,4,10,9,21,21,21,21,0,1,0,1,1,0,1,0,21,21,21,21,9,10,4,4,3,10,9,0,0,0,9,0,0,0,0,0,0,9,0,0,0,9,9,3,4,4,3,3,10,9,0,9,3,9,0,0,0,0,9,3,9,0,9,10,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4";
		
		private var defaultMap:Array = [];
		private var currentMap:Array = [];
		private var mapButtons:Array = [];
		private var mapName:TextField;
		
		// board
		private var gameBoard:Array = [];
		private var rows:int = 0;
		private var cols:int = 0;
		private var cellGap:int;
		private var cellCounter:int = 0;
		private var cellChanged:Boolean = false;
		private var startingPos:Point;
		private var cellWidth:int;
		private var cellHeight:int;
		private var cellScale:Number;
		private var _clipSprite:Sprite;
		
		// size
		private var potentialSize:int = 0;
		public var currentSize:int = 0;
		
		// general buttons
		private var okButton:BW_Button;
		//private var backButton:Button;
		
		// map buttons
		private var mapButtonBG:Sprite;
		private var smallMapButton:BW_Button;
		private var mediumMapButton:BW_Button;
		private var largeMapButton:BW_Button;
		private var changingMaps:Boolean = false;
		
		// save
		private var saveDataObject:SharedObject;
		private var _saveLocation:int = 0;
		private var saveButton:BW_Button;
		private var devSaveButton:BW_Button;
		
		// private var saveMapName:String;
		private var smallSlot:int = 0;
		private var mediumSlot:int = 0;
		private var largeSlot:int = 0;
		
		// name map screen
		private var newMapName:TextInput;
		private var saveScreenVisible:Boolean = false;
		
		// load
		private var loadButton:BW_Button;
		
		// ok screen
		private var yesOverwrite:BW_Button;
		private var noOverwrite:BW_Button;
		
		// other buttons
		private var resetButton:BW_Button;
		private var playButton:Button;
		
		// change map
		private var changeMapHolder:fSprite;
		private var changeMapYesButton:BW_Button;
		private var changeMapNoButton:BW_Button;
		
		// sidebar buttons
		private var selectorCellWidth:int;
		private var selectorCellHeight:int;
		private var cellSelector:MovieClip;
		// blank, wall & gray cells
		private var blankButton:Button;
		private var gray1Button:Button;
		private var gray2Button:Button;
		private var gray3Button:Button;
		// green cells
		private var green1Button:Button;
		private var green2Button:Button;
		private var green3Button:Button;
		private var green4Button:Button;
		// red cells
		private var red1Button:Button;
		private var red2Button:Button;
		private var red3Button:Button;
		private var red4Button:Button;
		// blue cells
		private var blue1Button:Button;
		private var blue2Button:Button;
		private var blue3Button:Button;
		private var blue4Button:Button;
		// yellow cells
		private var yellow1Button:Button;
		private var yellow2Button:Button;
		private var yellow3Button:Button;
		private var yellow4Button:Button;
		// special cells
		private var captureButton:Button;
		private var erosionButton:Button;
		
		//Holders
		private var utilityHolder:Sprite;
		private var yellowCellHolder:Sprite;
		private var blueCellHolder:Sprite;
		private var redCellHolder:Sprite;
		private var greenCellHolder:Sprite;
		
		// touch
		private var selection:int = 0;
		private var eraseSelection:int = -1;
		
		// font
		//private var textEditorFactory:flash.text.StageText;
		
		private var backBt:BW_Button;
		private var gameBoardString:String;
		
		private var _boardHolder:Sprite;
		
		// zooming
		private var _holderInitialPosition:Point;
		private var _holderInitialScaleX:Number;
		private var _holderInitialScaleY:Number;
		private var _lastHolderScaleX:Number;
		private var _topBoundary:Number;
		private var _bottomBoundary:Number;
		private var _leftBoundary:Number;
		private var _rightBoundary:Number;
		private var _leftQuad:Quad;
		private var _rightQuad:Quad;
		private var _topQuad:Quad;
		private var _bottomQuad:Quad;
		private var _holderCenterQuad:Quad;
		private var _startLeftBoundary:Number;
		private var _startRightBoundary:Number;
		private var _startTopBoundary:Number;
		private var _startBottomBoundary:Number;
		private var _startLeftQuad:Quad;
		private var _startRightQuad:Quad;
		private var _startTopQuad:Quad;
		private var _startBottomQuad:Quad;
		private var _zoomPanDelay:int;
		private const ZOOM_PAN_DELAY_TIME:int = 15;
		private var _zoomButton:BW_SocialButton;
		
		private var _currentMapName:String;
		
		public function MapEditor(main:Main)
		{
			super();
			
			this.main = main;
			saveDataObject = Main.saveDataObject;
			
			currentSize = 0;
			setupEditorSelections();
			drawBoard();
			
			addEventListener(TouchEvent.TOUCH, touchEnd);
			
			selection = 1;
			
			CalculateSaveSpot();
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				backBt = new BW_Button(.75,1.0, "", 0);
			else
				backBt = new BW_Button(.5,.75, "", 0);
			
			
			backBt.addImage(Main.assets.getTexture("Pathogen_BTN_Back"), Main.assets.getTexture("Pathogen_BTN_Back_Down"));
			addChild(backBt);
			backBt.x = PathogenTest.wTenPercent * .5;
			backBt.y = PathogenTest.hTenPercent * 9.3;
			backBt.addEventListener(TouchEvent.TOUCH, quitMapEditor);
			
			addEventListener(Event.ENTER_FRAME, update);
		}
		private function setupEditorSelections():void
		{
			var fontSize:int = PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor;
			mapName = new TextField(PathogenTest.stageWidth / 2, PathogenTest.wTenPercent, "New Map", "Dekar", fontSize, Color.WHITE);
			mapName.pivotY = mapName.height/2;
			addChild(mapName);
			mapName.x = PathogenTest.wTenPercent * .2;
			mapName.y = PathogenTest.hTenPercent * .5;
			mapName.hAlign = HAlign.LEFT;
			
			// create save/load buttons
			saveButton = new BW_Button(1, .4, "Save", 28);
			addChild(saveButton);
			saveButton.x = PathogenTest.wTenPercent * .8;
			saveButton.y = PathogenTest.hTenPercent * 1.2;
			saveButton.addEventListener(TouchEvent.TOUCH, addSaveScreen);
			
			loadButton = new BW_Button(1, .4, "Load", 28);
			addChild(loadButton);
			loadButton.x = saveButton.x + saveButton.width + PathogenTest.wTenPercent * .1;
			loadButton.y = saveButton.y;
			loadButton.addEventListener(TouchEvent.TOUCH, addloadScreen);
			
			resetButton = new BW_Button(1, .4, "Reset", 28);
			addChild(resetButton);
			resetButton.x = saveButton.x + saveButton.width/2;
			resetButton.y = PathogenTest.hTenPercent * 1.8;
			resetButton.addEventListener(TouchEvent.TOUCH, reset);
			
/*			devSaveButton = new BW_Button(1, .4, "Dev Save", 28);
			addChild(devSaveButton);
			devSaveButton.x = saveButton.x + saveButton.width/2;
			devSaveButton.y = resetButton.y + resetButton.height;
			devSaveButton.addEventListener(TouchEvent.TOUCH, devSave);*/
			
			//Add Map Choices
			smallMapButton = new Utils.BW_Button(.75, .75, "Small", 16);
			smallMapButton.toggle = true;
			addChild(smallMapButton);
			
			mediumMapButton = new Utils.BW_Button(.75, .75, "Medium", 16);
			mediumMapButton.toggle = true;
			addChild(mediumMapButton);
			
			largeMapButton = new Utils.BW_Button(.75, .75, "Large", 16);
			largeMapButton.toggle = true;
			addChild(largeMapButton);
			
			smallMapButton.x = saveButton.x - (saveButton.width * .2);
			smallMapButton.y = resetButton.y + (resetButton.height * 2);
			mediumMapButton.x = resetButton.x + PathogenTest.wTenPercent * .1;
			mediumMapButton.y = smallMapButton.y;
			largeMapButton.x = loadButton.x + PathogenTest.wTenPercent * .1 +(loadButton.width * .2);
			largeMapButton.y = smallMapButton.y;
			
			smallMapButton.addEventListener(TouchEvent.TOUCH, selectSmallMap);
			mediumMapButton.addEventListener(TouchEvent.TOUCH, selectMediumMap);
			largeMapButton.addEventListener(TouchEvent.TOUCH, selectLargeMap);
			
			smallMapButton.toggleDown();
			
			// green cell selectors
			var buttonGap:Number = PathogenTest.wTenPercent * .03;
			
			greenCellHolder = new Sprite();
			addChild(greenCellHolder);
			greenCellHolder.x = PathogenTest.wTenPercent * .8;
			greenCellHolder.y = PathogenTest.hTenPercent * 4.15;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var greenCellBackground:Scale9Image = new Scale9Image(textures);
			greenCellHolder.addChild(greenCellBackground);
			greenCellBackground.width = PathogenTest.wTenPercent * 1.2;
			greenCellBackground.height = PathogenTest.hTenPercent * 1.6;
			greenCellBackground.pivotX = greenCellBackground.width/2;
			greenCellBackground.pivotY = greenCellBackground.height/2;
			
			green1Button = new Button(Main.assets.getTexture("Green_Stage01"));
			green1Button.pivotX = green1Button.width/2;
			green1Button.pivotY = green1Button.height/2;
			selectorCellWidth = green1Button.width;
			selectorCellHeight = green1Button.height;
			greenCellHolder.addChild(green1Button);
			green1Button.x = -selectorCellWidth/2 - buttonGap;
			green1Button.y = -selectorCellHeight/2 - buttonGap;
			green1Button.addEventListener(TouchEvent.TOUCH, green1ButtonSelect);
			
			green2Button = new Button(Main.assets.getTexture("Green_Stage02"));
			green2Button.pivotX = green2Button.width/2;
			green2Button.pivotY = green2Button.height/2;
			greenCellHolder.addChild(green2Button);
			green2Button.x = selectorCellWidth/2 + buttonGap;
			green2Button.y = -selectorCellHeight/2 - buttonGap;
			green2Button.addEventListener(TouchEvent.TOUCH, green2ButtonSelect);
			
			green3Button = new Button(Main.assets.getTexture("Green_Stage03"));
			green3Button.pivotX = green3Button.width/2;
			green3Button.pivotY = green3Button.height/2;
			greenCellHolder.addChild(green3Button);
			green3Button.x = -selectorCellWidth/2 - buttonGap;
			green3Button.y = selectorCellHeight/2 + buttonGap;
			green3Button.addEventListener(TouchEvent.TOUCH, green3ButtonSelect);
			
			green4Button = new Button(Main.assets.getTexture("Green_Stage04"));
			green4Button.pivotX = green4Button.width/2;
			green4Button.pivotY = green4Button.height/2;
			greenCellHolder.addChild(green4Button);
			green4Button.x = selectorCellWidth/2 + buttonGap;
			green4Button.y = selectorCellHeight/2 + buttonGap;
			green4Button.addEventListener(TouchEvent.TOUCH, green4ButtonSelect);
			
			// red cell selectors
			
			redCellHolder = new Sprite();
			addChild(redCellHolder);
			redCellHolder.x = greenCellHolder.x + (greenCellHolder.width * 1.1);
			redCellHolder.y = greenCellHolder.y;
				
			var redCellBackground:Scale9Image = new Scale9Image(textures);
			redCellHolder.addChild(redCellBackground);
			redCellBackground.width = PathogenTest.wTenPercent * 1.2;
			redCellBackground.height = PathogenTest.hTenPercent * 1.6;
			redCellBackground.pivotX = redCellBackground.width/2;
			redCellBackground.pivotY = redCellBackground.height/2;
			
			// red cell selectors
			red1Button = new Button(Main.assets.getTexture("Red_Stage01"));
			red1Button.pivotX = red1Button.width/2;
			red1Button.pivotY = red1Button.height/2;
			redCellHolder.addChild(red1Button);
			red1Button.x = -selectorCellWidth/2 - buttonGap;
			red1Button.y = -selectorCellHeight/2 - buttonGap;
			red1Button.addEventListener(TouchEvent.TOUCH, red1ButtonSelect);
			
			red2Button = new Button(Main.assets.getTexture("Red_Stage02"));
			red2Button.pivotX = red2Button.width/2;
			red2Button.pivotY = red2Button.height/2;
			redCellHolder.addChild(red2Button);
			red2Button.x = selectorCellWidth/2 + buttonGap;
			red2Button.y = -selectorCellHeight/2 - buttonGap;
			red2Button.addEventListener(TouchEvent.TOUCH, red2ButtonSelect);
			
			red3Button = new Button(Main.assets.getTexture("Red_Stage03"));
			red3Button.pivotX = red3Button.width/2;
			red3Button.pivotY = red3Button.height/2;
			redCellHolder.addChild(red3Button);
			red3Button.x = -selectorCellWidth/2 - buttonGap;
			red3Button.y = selectorCellHeight/2 + buttonGap;
			red3Button.addEventListener(TouchEvent.TOUCH, red3ButtonSelect);
			
			red4Button = new Button(Main.assets.getTexture("Red_Stage04"));
			red4Button.pivotX = red4Button.width/2;
			red4Button.pivotY = red4Button.height/2;
			redCellHolder.addChild(red4Button);
			red4Button.x = selectorCellWidth/2 + buttonGap;
			red4Button.y = selectorCellHeight/2 + buttonGap;
			red4Button.addEventListener(TouchEvent.TOUCH, red4ButtonSelect);
			
			// blue cell selectors
			blueCellHolder = new Sprite();
			addChild(blueCellHolder);
			blueCellHolder.x = greenCellHolder.x;
			blueCellHolder.y = greenCellHolder.y + (greenCellHolder.height * 1.1);
			
			var blueCellBackground:Scale9Image = new Scale9Image(textures);
			blueCellHolder.addChild(blueCellBackground);
			blueCellBackground.width = PathogenTest.wTenPercent * 1.2;
			blueCellBackground.height = PathogenTest.hTenPercent * 1.6;
			blueCellBackground.pivotX = blueCellBackground.width/2;
			blueCellBackground.pivotY = blueCellBackground.height/2;
			
			blue1Button = new Button(Main.assets.getTexture("Blue_Stage01"));
			blue1Button.pivotX = blue1Button.width/2;
			blue1Button.pivotY = blue1Button.height/2;
			blueCellHolder.addChild(blue1Button);
			blue1Button.x = -selectorCellWidth/2 - buttonGap;
			blue1Button.y = -selectorCellHeight/2 - buttonGap;
			blue1Button.addEventListener(TouchEvent.TOUCH, blue1ButtonSelect);
			
			blue2Button = new Button(Main.assets.getTexture("Blue_Stage02"));
			blue2Button.pivotX = blue2Button.width/2;
			blue2Button.pivotY = blue2Button.height/2;
			blueCellHolder.addChild(blue2Button);
			blue2Button.x = selectorCellWidth/2 + buttonGap;
			blue2Button.y = -selectorCellHeight/2 - buttonGap;
			blue2Button.addEventListener(TouchEvent.TOUCH, blue2ButtonSelect);
			
			blue3Button = new Button(Main.assets.getTexture("Blue_Stage03"));
			blue3Button.pivotX = blue3Button.width/2;
			blue3Button.pivotY = blue3Button.height/2;
			blueCellHolder.addChild(blue3Button);
			blue3Button.x = -selectorCellWidth/2 - buttonGap;
			blue3Button.y = selectorCellHeight/2 + buttonGap;
			blue3Button.addEventListener(TouchEvent.TOUCH, blue3ButtonSelect);
			
			blue4Button = new Button(Main.assets.getTexture("Blue_Stage04"));
			blue4Button.pivotX = blue2Button.width/2;
			blue4Button.pivotY = blue2Button.height/2;
			blueCellHolder.addChild(blue4Button);
			blue4Button.x = selectorCellWidth/2 + buttonGap;
			blue4Button.y = selectorCellHeight/2 + buttonGap;
			blue4Button.addEventListener(TouchEvent.TOUCH, blue4ButtonSelect);
			
			// yellow cell selectors
			yellowCellHolder = new Sprite();
			addChild(yellowCellHolder);
			yellowCellHolder.x = redCellHolder.x;
			yellowCellHolder.y = blueCellHolder.y;
			
			var yellowCellBackground:Scale9Image = new Scale9Image(textures);
			yellowCellHolder.addChild(yellowCellBackground);
			yellowCellBackground.width = PathogenTest.wTenPercent * 1.2;
			yellowCellBackground.height = PathogenTest.hTenPercent * 1.6;
			yellowCellBackground.pivotX = yellowCellBackground.width/2;
			yellowCellBackground.pivotY = yellowCellBackground.height/2;
			
			yellow1Button = new Button(Main.assets.getTexture("Yellow_Stage01"));
			yellow1Button.pivotX = yellow1Button.width/2;
			yellow1Button.pivotY = yellow1Button.height/2;
			yellowCellHolder.addChild(yellow1Button);
			yellow1Button.x = -selectorCellWidth/2 - buttonGap;
			yellow1Button.y = -selectorCellHeight/2 - buttonGap;
			yellow1Button.addEventListener(TouchEvent.TOUCH, yellow1ButtonSelect);
			
			yellow2Button = new Button(Main.assets.getTexture("Yellow_Stage02"));
			yellow2Button.pivotX = yellow2Button.width/2;
			yellow2Button.pivotY = yellow2Button.height/2;
			yellowCellHolder.addChild(yellow2Button);
			yellow2Button.x = selectorCellWidth/2 + buttonGap;
			yellow2Button.y = -selectorCellHeight/2 - buttonGap;
			yellow2Button.addEventListener(TouchEvent.TOUCH, yellow2ButtonSelect);
			
			yellow3Button = new Button(Main.assets.getTexture("Yellow_Stage03"));
			yellow3Button.pivotX = yellow3Button.width/2;
			yellow3Button.pivotY = yellow3Button.height/2;
			yellowCellHolder.addChild(yellow3Button);
			yellow3Button.x = -selectorCellWidth/2 - buttonGap;
			yellow3Button.y = selectorCellHeight/2 + buttonGap;
			yellow3Button.addEventListener(TouchEvent.TOUCH, yellow3ButtonSelect);
			
			yellow4Button = new Button(Main.assets.getTexture("Yellow_Stage04"));
			yellow4Button.pivotX = yellow4Button.width/2;
			yellow4Button.pivotY = yellow4Button.height/2;
			yellowCellHolder.addChild(yellow4Button);
			yellow4Button.x = selectorCellWidth/2 + buttonGap;
			yellow4Button.y = selectorCellHeight/2 + buttonGap;
			yellow4Button.addEventListener(TouchEvent.TOUCH, yellow4ButtonSelect);
			
			// gray cell selectors
			utilityHolder = new Sprite();
			addChild(utilityHolder);
			utilityHolder.x = greenCellHolder.x + (greenCellHolder.width/2);
			utilityHolder.y = blueCellHolder.y + (blueCellHolder.height * 1.1);
			
			var utilityBackground:Scale9Image = new Scale9Image(textures);
			utilityHolder.addChild(utilityBackground);
			utilityBackground.width = PathogenTest.wTenPercent * 1.8;
			utilityBackground.height = PathogenTest.hTenPercent * 1.6;
			utilityBackground.pivotX = utilityBackground.width/2;
			utilityBackground.pivotY = utilityBackground.height/2;
			
			gray1Button = new Button(Main.assets.getTexture("Grey_Stage01"));
			gray1Button.pivotX = gray1Button.width/2;
			gray1Button.pivotY = gray1Button.height/2;
			utilityHolder.addChild(gray1Button);
			gray1Button.x = -selectorCellWidth - buttonGap;
			gray1Button.y = -selectorCellHeight/2;
			gray1Button.addEventListener(TouchEvent.TOUCH, gray1ButtonSelect);
			
			gray2Button = new Button(Main.assets.getTexture("Grey_Stage02"));
			gray2Button.pivotX = gray2Button.width/2;
			gray2Button.pivotY = gray2Button.height/2;
			utilityHolder.addChild(gray2Button);
			//gray2Button.x = selectorCellWidth;
			gray2Button.y = gray1Button.y
			gray2Button.addEventListener(TouchEvent.TOUCH, gray2ButtonSelect);
			
			gray3Button = new Button(Main.assets.getTexture("Grey_Stage04"));
			gray3Button.pivotX = gray3Button.width/2;
			gray3Button.pivotY = gray3Button.height/2;
			utilityHolder.addChild(gray3Button);
			gray3Button.x = selectorCellWidth + buttonGap;
			gray3Button.y = gray1Button.y
			gray3Button.addEventListener(TouchEvent.TOUCH, gray3ButtonSelect);
			
			// capture and erosion
			captureButton = new Button(Main.assets.getTexture("Capture_Zone"));
			captureButton.pivotX = captureButton.width/2;
			captureButton.pivotY = captureButton.height/2;
			utilityHolder.addChild(captureButton);
			captureButton.x = -selectorCellWidth/2;
			captureButton.y = selectorCellHeight/2;
			captureButton.addEventListener(TouchEvent.TOUCH, captureButtonSelect);
			
			erosionButton = new Button(Main.assets.getTexture("Erosion_Zone"));
			erosionButton.pivotX = erosionButton.width/2;
			erosionButton.pivotY = erosionButton.height/2;
			utilityHolder.addChild(erosionButton);
			erosionButton.x = selectorCellWidth/2;
			erosionButton.y = selectorCellHeight/2;
			erosionButton.addEventListener(TouchEvent.TOUCH, erosionButtonSelect);
			
			cellSelector = new MovieClip(Main.assets.getTextures("Selector"));
			cellSelector.pivotX = cellSelector.width/2;
			cellSelector.pivotY = cellSelector.height/2;
			cellSelector.x = utilityHolder.x + gray1Button.x;
			cellSelector.y = utilityHolder.y + gray1Button.y;
			addChild(cellSelector);
		}
		private function addSaveScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(_saveScreen == null)
				{
					_saveScreen = new SaveScreen(this);
					addChild(_saveScreen);
				}
			}
		}
		public function removeSaveScreen():void
		{
			if(_saveScreen)
			{
				_saveScreen.dispose();
				removeChild(_saveScreen);
				_saveScreen = null;
			}
		}
		private function CalculateSaveSpot():void 
		{
			//Small Maps
			if (saveDataObject.data.userSmall_06Map != null)
			{
				smallSlot = 6;
			}
			else if (saveDataObject.data.userSmall_05Map != null)
			{
				smallSlot = 5;
			}
			else if (saveDataObject.data.userSmall_04Map != null)
			{
				smallSlot = 4;
			}
			else if (saveDataObject.data.userSmall_03Map != null)
			{
				smallSlot = 3;
			}
			else if (saveDataObject.data.userSmall_02Map != null)
			{
				smallSlot = 2;
			}
			else if (saveDataObject.data.userSmall_01Map != null)
			{
				smallSlot = 1;
			}
			//Medium Maps
			if (saveDataObject.data.userMedium_06Map != null)
			{
				mediumSlot = 6;
			}
			else if (saveDataObject.data.userMedium_05Map != null)
			{
				mediumSlot = 5;
			}
			else if (saveDataObject.data.userMedium_04Map != null)
			{
				mediumSlot = 4;
			}
			else if (saveDataObject.data.userMedium_03Map != null)
			{
				mediumSlot = 3;
			}
			else if (saveDataObject.data.userMedium_02Map != null)
			{
				mediumSlot = 2;
			}
			else if (saveDataObject.data.userMedium_01Map != null)
			{
				mediumSlot = 1;
			}
			//Large Maps
			if (saveDataObject.data.userLarge_06Map != null)
			{
				largeSlot = 6;
			}
			else if (saveDataObject.data.userLarge_05Map != null)
			{
				largeSlot = 5;
			}
			else if (saveDataObject.data.userLarge_04Map != null)
			{
				largeSlot = 4;
			}
			else if (saveDataObject.data.userLarge_03Map != null)
			{
				largeSlot = 3;
			}
			else if (saveDataObject.data.userLarge_02Map != null)
			{
				largeSlot = 2;
			}
			else if (saveDataObject.data.userLarge_01Map != null)
			{
				largeSlot = 1;
			}
		}
		public function saveMap(mapName:String):void
		{	
			_currentMapName = mapName;
			
			generateSaveGameData(mapName)
			
			if(!checkMapName(mapName))
				checkSlot();
			else
				addMapOverwriteScreen();
			
			checkAchievements();
		}
		private function generateSaveGameData(mapName:String):void
		{
			gameBoardString = mapName;
			
			gameBoardString += ":";
			
			for each(var i:EditorCell in gameBoard)
			{
				gameBoardString += i.getState().toString() + ",";
			}
		}
		private function checkAchievements():void
		{
			if(Main.achievements.progress_Create_Map_1 != Achievements.COMPLETE)
				Main.achievements.updateAchievementProgress(Achievements.CREATE_MAP_1);
		}
		private function checkSlot():void
		{
			var full:Boolean = false;
			
			switch(currentSize)
			{
				case 0:
				{
					if(smallSlot < NUM_SAVE_SLOTS)
					{
						smallSlot++;
						_saveLocation = smallSlot + currentSize * NUM_SAVE_SLOTS;
					}
					else
						full = true;
					break;
				}
				case 1:
				{
					if (mediumSlot < NUM_SAVE_SLOTS)
					{
						mediumSlot++;
						_saveLocation = mediumSlot + currentSize * NUM_SAVE_SLOTS;
					}
					else
						full = true;
					break;
				}
				case 2:
				{
					if (largeSlot < NUM_SAVE_SLOTS)
					{
						largeSlot++;
						_saveLocation = largeSlot + currentSize * NUM_SAVE_SLOTS;
					}
					else
						full = true;
					break;
				}
					
				default:
					break;
			}
			
			// trace("Real Save Location: " + _saveLocation);
			if(!full)
				assignSaveLocation()
			else
			{
				mapSlotScreen = new MapSlotScreen(this, "Save");
				addChild(mapSlotScreen);
				mapSlotScreen.x = PathogenTest.stageCenter.x;
				mapSlotScreen.y = PathogenTest.stageCenter.y;
			}
		}
		public function overwriteLocation(saveSlotlocation:int):void
		{
			_saveLocation = saveSlotlocation + currentSize * NUM_SAVE_SLOTS;
			assignSaveLocation();
		}
		public function assignSaveLocation():void
		{
			trace("Assign Save Location at location: " + _saveLocation);
			
			switch(_saveLocation)
			{
				case 1:
					saveDataObject.data.userSmall_01Map = gameBoardString;
					break;
				case 2:
					saveDataObject.data.userSmall_02Map = gameBoardString;
					break;
				case 3:
					saveDataObject.data.userSmall_03Map = gameBoardString;
					break;
				case 4:
					saveDataObject.data.userSmall_04Map = gameBoardString;
					break;
				case 5:
					saveDataObject.data.userSmall_05Map = gameBoardString;
					break;
				case 6:
					saveDataObject.data.userSmall_06Map = gameBoardString;
					break;
				case 7:
					saveDataObject.data.userMedium_01Map = gameBoardString;
					break;
				case 8:
					saveDataObject.data.userMedium_02Map = gameBoardString;
					break;
				case 9:
					saveDataObject.data.userMedium_03Map = gameBoardString;
					break;
				case 10:
					saveDataObject.data.userMedium_04Map = gameBoardString;
					break;
				case 11:
					saveDataObject.data.userMedium_05Map = gameBoardString;
					break;
				case 12:
					saveDataObject.data.userMedium_06Map = gameBoardString;
					break;
				case 13:
					saveDataObject.data.userLarge_01Map = gameBoardString;
					break;
				case 14:
					saveDataObject.data.userLarge_02Map = gameBoardString;
					break;
				case 15:
					saveDataObject.data.userLarge_03Map = gameBoardString;
					break;
				case 16:
					saveDataObject.data.userLarge_04Map = gameBoardString;
					break;
				case 17:
					saveDataObject.data.userLarge_05Map = gameBoardString;
					break;
				case 18:
					saveDataObject.data.userLarge_06Map = gameBoardString;
					break;
				default:
					break;
			}
			
			saveDataObject.flush();
			mapName.text = _currentMapName;
			addMapCreatedScreen();
		}
		private function checkMapName(mapName:String):Boolean
		{
			//Check a save maps name
			
			var currentMapName:String;
			var arrayOfSavedData:Array;
			var saveData:String;
			var matching:Boolean = false;
			var i:int;
			
			//Look at different places depending on the current map size.
			switch(currentSize)
			{
				case 0:
				{
					//loop through all known small save slot locations
					for (i = 0; i < smallSlot; i++)
					{
						switch(i)
						{
							case 0:
								if(saveDataObject.data.userSmall_01Map != null)
									saveData = saveDataObject.data.userSmall_01Map;
								break;
							case 1:
								if(saveDataObject.data.userSmall_02Map != null)
									saveData = saveDataObject.data.userSmall_02Map;
								break;
							case 2:
								if(saveDataObject.data.userSmall_03Map != null)
									saveData = saveDataObject.data.userSmall_03Map;
								break;
							case 3:
								if(saveDataObject.data.userSmall_04Map != null)
									saveData = saveDataObject.data.userSmall_04Map;
								break;
							case 4:
								if(saveDataObject.data.userSmall_05Map != null)
									saveData = saveDataObject.data.userSmall_05Map;
								break;
							case 5:
								if(saveDataObject.data.userSmall_06Map != null)
									saveData = saveDataObject.data.userSmall_06Map;
								break;
						}
						
						//If a location has data in it, check its name
						if(saveData)
						{
							arrayOfSavedData = saveData.split(":");
							mapName = arrayOfSavedData[0];
							
							if(currentMapName == mapName)
							{
								matching = true;
								_saveLocation = i + 1 + currentSize * NUM_SAVE_SLOTS;
								break;
							}
								
						}
						
					}
					break;
				}
					
				case 1:
				{
					//loop through all known medium save slot locations
					for (i = 0; i < mediumSlot; i++)
					{
						switch(i)
						{
							case 0:
								if(saveDataObject.data.userMedium_01Map != null)
									saveData = saveDataObject.data.userMedium_01Map;
								break;
							case 1:
								if(saveDataObject.data.userMedium_02Map != null)
									saveData = saveDataObject.data.userMedium_02Map;
								break;
							case 2:
								if(saveDataObject.data.userMedium_03Map != null)
									saveData = saveDataObject.data.userMedium_03Map;
								break;
							case 3:
								if(saveDataObject.data.userMedium_04Map != null)
									saveData = saveDataObject.data.userMedium_04Map;
								break;
							case 4:
								if(saveDataObject.data.userMedium_05Map != null)
									saveData = saveDataObject.data.userMedium_05Map;
								break;
							case 5:
								if(saveDataObject.data.userMedium_06Map != null)
									saveData = saveDataObject.data.userMedium_06Map;
								break;
						}
						
						//If a location has data in it, check its name
						if(saveData)
						{
							arrayOfSavedData = saveData.split(":");
							mapName = arrayOfSavedData[0];
							
							if(currentMapName == mapName)
							{
								matching = true;
								_saveLocation = i + 1 + currentSize * NUM_SAVE_SLOTS;
								break;
							}
							
						}
						
					}
					break;
				}
					
				case 2:
				{
					//loop through all known large save slot locations
					for (i = 0; i < largeSlot; i++)
					{
						switch(i)
						{
							case 0:
								if(saveDataObject.data.userLarge_01Map != null)
									saveData = saveDataObject.data.userLarge_01Map;
								break;
							case 1:
								if(saveDataObject.data.userLarge_02Map != null)
									saveData = saveDataObject.data.userLarge_02Map;
								break;
							case 2:
								if(saveDataObject.data.userLarge_03Map != null)
									saveData = saveDataObject.data.userLarge_03Map;
								break;
							case 3:
								if(saveDataObject.data.userLarge_04Map != null)
									saveData = saveDataObject.data.userLarge_04Map;
								break;
							case 4:
								if(saveDataObject.data.userLarge_05Map != null)
									saveData = saveDataObject.data.userLarge_05Map;
								break;
							case 5:
								if(saveDataObject.data.userLarge_06Map != null)
									saveData = saveDataObject.data.userLarge_06Map;
								break;
						}
						
						//If a location has data in it, check its name
						if(saveData)
						{
							arrayOfSavedData = saveData.split(":");
							mapName = arrayOfSavedData[0];
							
							if(currentMapName == mapName)
							{
								matching = true;
								_saveLocation = i + 1 + currentSize * NUM_SAVE_SLOTS;
								break;
							}
							
						}
						
					}
					break;
				}
			}
			
			return matching;
		}
		public function addMapOverwriteScreen():void
		{	
			if(_overwriteScreen == null)
			{
				_overwriteScreen = new OverwriteScreen(this);
				addChild(_overwriteScreen);
			}
		}
		public function removeMapOverwriteScreen():void
		{
			if(_overwriteScreen)
			{
				_overwriteScreen.dispose();
				removeChild(_overwriteScreen);
				_overwriteScreen = null;
			}
		}
		public function overwriteMap():void
		{
			trace("Overwrite Map");
			
			var gameBoardString:String = _currentMapName;
			
			gameBoardString += ":";
			
			for each(var i:EditorCell in gameBoard)
			{
				gameBoardString += i.getState().toString() + ",";
			}
			
			assignSaveLocation();
		}
		private function addMapCreatedScreen():void
		{
			if(_mapCreatedScreen == null)
			{
				_mapCreatedScreen = new MapCreatedScreen(this);
				addChild(_mapCreatedScreen);
			}
		}
		public function removeMapCreatedScreen():void
		{
			if(_mapCreatedScreen)
			{
				_mapCreatedScreen.dispose();
				removeChild(_mapCreatedScreen);
				_mapCreatedScreen = null;
			}
		}
		private function addloadScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(mapSlotScreen == null)
				{
					trace("Adding in Loading Screen");
					
					mapSlotScreen = new MapSlotScreen(this, "Load");
					addChild(mapSlotScreen);
					mapSlotScreen.x = PathogenTest.stageCenter.x;
					mapSlotScreen.y = PathogenTest.stageCenter.y;
				}
			}
		}
		public function removeLoadScreen():void
		{
			if(mapSlotScreen)
			{
				mapSlotScreen.dispose();
				removeChild(mapSlotScreen);
				mapSlotScreen = null;
			}
		}
		public function load($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(mapSlotScreen.chosenSaveSlot)
				{
					//trace("Load");
					potentialSize = mapSlotScreen.chosenSize;
					currentMap = mapSlotScreen.chosenSaveSlot.mapData.split(",");
					mapName.text =  mapSlotScreen.chosenSaveSlot.mapName;
					
					_saveLocation = 1;
					mapSlotScreen.startFadeOut();
					changeMapSize($e);
				}
			}
		}
		private function gray1ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 1;
				cellSelector.x = utilityHolder.x + gray1Button.x;
				cellSelector.y = utilityHolder.y + gray1Button.y;
			}
		}
		
		private function gray2ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 2;
				cellSelector.x = utilityHolder.x + gray2Button.x;
				cellSelector.y = utilityHolder.y + gray2Button.y;
			}
		}
		
		private function gray3ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 3;
				cellSelector.x = utilityHolder.x + gray3Button.x;
				cellSelector.y = utilityHolder.y + gray3Button.y;
			}
		}
		
		private function blankButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 0;
				cellSelector.x = blankButton.x;
				cellSelector.y = blankButton.y;
			}
		}
		
		private function green1ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 5;
				cellSelector.x = greenCellHolder.x + green1Button.x;
				cellSelector.y = greenCellHolder.y + green1Button.y;
			}
		}
		
		private function green2ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 6;
				cellSelector.x = greenCellHolder.x + green2Button.x;
				cellSelector.y = greenCellHolder.y + green2Button.y;
			}
		}
		
		private function green3ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 7;
				cellSelector.x = greenCellHolder.x + green3Button.x;
				cellSelector.y = greenCellHolder.y + green3Button.y;
			}
		}
		
		private function green4ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 8;
				cellSelector.x = greenCellHolder.x + green4Button.x;
				cellSelector.y = greenCellHolder.y + green4Button.y;
			}
		}
		
		private function red1ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 9;
				cellSelector.x = redCellHolder.x + red1Button.x;
				cellSelector.y = redCellHolder.y + red1Button.y;
			}
		}
		
		private function red2ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 10;
				cellSelector.x = redCellHolder.x + red2Button.x;
				cellSelector.y = redCellHolder.y + red2Button.y;
			}
		}
		
		private function red3ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 11;
				cellSelector.x = redCellHolder.x + red3Button.x;
				cellSelector.y = redCellHolder.y + red3Button.y;
			}
		}
		
		private function red4ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 12;
				cellSelector.x = redCellHolder.x + red4Button.x;
				cellSelector.y = redCellHolder.y + red4Button.y;
			}
		}
		
		private function blue1ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 13;
				cellSelector.x = blueCellHolder.x + blue1Button.x;
				cellSelector.y = blueCellHolder.y + blue1Button.y;
			}
		}
		
		private function blue2ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 14;
				cellSelector.x = blueCellHolder.x + blue2Button.x;
				cellSelector.y = blueCellHolder.y + blue2Button.y;
			}
		}
		
		private function blue3ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 15;
				cellSelector.x = blueCellHolder.x + blue3Button.x;
				cellSelector.y = blueCellHolder.y + blue3Button.y;
			}
		}
		
		private function blue4ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 16;
				cellSelector.x = blueCellHolder.x + blue4Button.x;
				cellSelector.y = blueCellHolder.y + blue4Button.y;
			}
		}
		
		private function yellow1ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 17;
				cellSelector.x = yellowCellHolder.x + yellow1Button.x;
				cellSelector.y = yellowCellHolder.y + yellow1Button.y;
			}
		}
		
		private function yellow2ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 18;
				cellSelector.x = yellowCellHolder.x + yellow2Button.x;
				cellSelector.y = yellowCellHolder.y + yellow2Button.y;
			}
		}
		
		private function yellow3ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 19;
				cellSelector.x = yellowCellHolder.x + yellow3Button.x;
				cellSelector.y = yellowCellHolder.y + yellow3Button.y;
			}
		}
		
		private function yellow4ButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 20;
				cellSelector.x = yellowCellHolder.x + yellow4Button.x;
				cellSelector.y = yellowCellHolder.y + yellow4Button.y;
			}
		}
		
		private function captureButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 21;
				cellSelector.x = utilityHolder.x + captureButton.x;
				cellSelector.y = utilityHolder.y + captureButton.y;
			}
		}
		
		private function erosionButtonSelect($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				selection = 22;
				cellSelector.x = utilityHolder.x + erosionButton.x;
				cellSelector.y = utilityHolder.y + erosionButton.y;
			}
		}
		
		private function touchEnd($e:TouchEvent):void
		{
			if (!isZoomed())
			{
				var touchEnd:Touch = $e.getTouch(this, TouchPhase.ENDED);
				if (touchEnd && _zoomPanDelay >= ZOOM_PAN_DELAY_TIME)
				{
					clearChangedCells();
					eraseSelection = -1;
				}
			}
		}
		private function drawBoard():void
		{
			if(_clipSprite)
			{
				if(_boardHolder)
				{
					_boardHolder.dispose();
					_clipSprite.removeChild(_boardHolder);
					_boardHolder = null;
				}
				
				_clipSprite.dispose();
				removeChild(_clipSprite);
				_clipSprite = null;
			}
			else
			{
				if(_boardHolder)
				{
					_boardHolder.dispose();
					removeChild(_boardHolder);
					_boardHolder = null;
				}
			}	
			
			//Small Map
			if (currentSize == 0)
			{
				defaultMap = smallRectangleString.split(",");
				defaultMap.pop()
				
				cols = SMALL_COLS;
				rows = SMALL_ROWS;
				
				cellScale = SMALL_SCALE
			}
			//Medium Map
			if (currentSize == 1)
			{
				defaultMap = mediumRectangleString.split(",");
				defaultMap.pop()
				
				cols = MED_COLS;
				rows = MED_ROWS;
				
				cellScale = MEDIUM_SCALE;
			}
			//Large Map
			if (currentSize == 2)
			{
				defaultMap = largeRectangleString.split(",");
				defaultMap.pop()
				
				cols = LARGE_COLS;
				rows = LARGE_ROWS;
				
				if (Main.isPhone())
					cellScale = LARGE_SCALE_IPHONE;
				else
					cellScale = LARGE_SCALE;
			}
			
			if (_saveLocation == 0)
				currentMap = defaultMap;
			
			_boardHolder = new Sprite();
			
			if (Main.isPhone())
			{
				//New Clip Rect
				_clipSprite = new Sprite();
				var clipRectWidth:Number = PathogenTest.wTenPercent * 7;
				var clipRectHeight:Number = PathogenTest.hTenPercent * 9;
				var rect:Rectangle = new Rectangle(PathogenTest.wTenPercent * 2.8, PathogenTest.hTenPercent * 0.5, clipRectWidth, clipRectHeight)
				_clipSprite.clipRect = rect;
				addChild(_clipSprite);
				_clipSprite.addChild(_boardHolder);
			}
			else
				addChild(_boardHolder);
			
			//Create a blank board
			for (var i:int = 0; i < rows; i++)
			{
				for (var j:int = 0; j < cols; j++)
				{
					var cell:EditorCell = new EditorCell(this, cellCounter);
					cell.init();
					cell.resetVals();
					cell.setMapEditor(this);
					_boardHolder.addChild(cell);

					cell.changeColor(currentMap[cellCounter]);
					
					cellGap = cell.width * cellScale;

					cell.x = cellGap * i;
					cell.y = cellGap * j;
					
					cell.scaleX = cellScale;
					cell.scaleY = cellScale;
					
					gameBoard.push(cell);
					
					cellCounter++;
				}
			}
			
			cellWidth = cell.width * cellScale;
			cellHeight = cell.height * cellScale;
			
			_boardHolder.pivotX = _boardHolder.width/2;
			_boardHolder.pivotY = _boardHolder.height/2;
			
			_boardHolder.y = PathogenTest.stageCenter.y;
			_boardHolder.x = PathogenTest.wTenPercent * 6.3;
			
			_holderInitialPosition = new Point(_boardHolder.x, _boardHolder.y);
			
			if (Main.isPhone())
			{
				_boardHolder.addEventListener(TouchEvent.TOUCH, onTouchLocation);
				
				// set the boundaries
				_topBoundary = _boardHolder.y - _boardHolder.height * 0.5;
				_bottomBoundary = _boardHolder.y + _boardHolder.height * 0.5;
				_leftBoundary = _boardHolder.x - _boardHolder.width * 0.5;
				_rightBoundary = _boardHolder.x + _boardHolder.width * 0.5;
				
				// set the initial boundaries
				_startTopBoundary = _topBoundary;
				_startBottomBoundary = _bottomBoundary;
				_startLeftBoundary = _leftBoundary;
				_startRightBoundary = _rightBoundary;
				
				// create zoom button
				if (Main.isPhone())
					_zoomButton = new BW_SocialButton(1.5, 1.0, "Zoom", "Zoom", 24, Main.assets.getTexture("ZoomIn_Off"), Main.assets.getTexture("ZoomOut_off"));
				else
					_zoomButton = new BW_SocialButton(1.5, 0.6, "Zoom", "Zoom", 24, Main.assets.getTexture("ZoomIn_Off"), Main.assets.getTexture("ZoomOut_off"));
				
				
				_zoomButton.toggle = true;
				_zoomButton.x = PathogenTest.stageCenter.x;
				_zoomButton.y = PathogenTest.hTenPercent * 9.35;
				addChild(_zoomButton);
				_zoomButton.addEventListener(TouchEvent.TOUCH, onTouchZoom);
				_zoomButton.visible = true;
			}
			else
				_zoomPanDelay = ZOOM_PAN_DELAY_TIME;
			
			cellCounter = 0;
			cellChanged = false;
			clearChangedCells();
		}
		private function reset($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				switch(currentSize)
				{
					case 0:
						selectSmallMap($e);
						break;
					case 1:
						selectMediumMap($e);
						break;
					case 2:
						selectLargeMap($e);
						break;
					default:
						break;
				}
			}
		}
		private function selectSmallMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if (!saveScreenVisible)
				{
					if (changingMaps)
						removeChangeMapScreen($e);
					
					potentialSize = 0;
					currentMap = smallRectangleString.split(",");
					currentMap.pop();
					if (cellChanged)
						addChangeMapScreen();
					else
						changeMapSize(null);
				}
			}
		}
		
		private function selectMediumMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if (!saveScreenVisible)
				{
					if (changingMaps)
						removeChangeMapScreen($e);
					
					potentialSize = 1;
					currentMap = mediumRectangleString.split(",");
					currentMap.pop();
					if (cellChanged)
						addChangeMapScreen();
					else
						changeMapSize(null);
				}
			}
		}
		
		private function selectLargeMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if (!saveScreenVisible)
				{
					if (changingMaps)
						removeChangeMapScreen($e);
					
					potentialSize = 2;
					currentMap = largeRectangleString.split(",");
					currentMap.pop();
					if (cellChanged)
						addChangeMapScreen();
					else
						changeMapSize(null);
				}
			}
		}
		
		public function changeMapSize($e:TouchEvent):void
		{
			var touch:Touch;
			if ($e != null)
				touch = $e.getTouch(this, TouchPhase.ENDED);

			if (touch || $e == null)
			{
				if (changingMaps)
					removeChangeMapScreen($e);
				removeMap();
				
				smallMapButton.toggleUp();
				mediumMapButton.toggleUp();
				largeMapButton.toggleUp();
				
				if (potentialSize == 0)
				{
					smallMapButton.alpha = 1.0;
					currentSize = 0;
					smallMapButton.toggleDown();
				}
				if (potentialSize == 1)
				{
					mediumMapButton.alpha = 1.0;
					currentSize = 1;
					mediumMapButton.toggleDown();
				}
				if (potentialSize == 2)
				{
					largeMapButton.alpha = 1.0;
					currentSize = 2;
					largeMapButton.toggleDown();
				}
				
				drawBoard();
			}
		}
		
		private function removeMap():void
		{
			for each (var cell:EditorCell in gameBoard)
			{
				cell.removeTouchEvent();
				cell.dispose();
				removeChild(cell);
				cell = null;
			}
			gameBoard = [];
		}
		
		private function addChangeMapScreen():void
		{
			changingMaps = true;
			
			changeMapHolder = new fSprite();
			addChild(changeMapHolder);
			changeMapHolder.pivotX = changeMapHolder.width/2;
			changeMapHolder.pivotY = changeMapHolder.height/2;
			changeMapHolder.x = PathogenTest.stageCenter.x;
			changeMapHolder.y = PathogenTest.stageCenter.y;
			
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			changeMapHolder.addChild(transBackground);
			transBackground.alpha = .75;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var background:Scale9Image = new Scale9Image(textures);
			changeMapHolder.addChild(background);
			background.width = PathogenTest.wTenPercent * 4.5;
			background.height = PathogenTest.hTenPercent * 3.5;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			
			var header:Scale9Image = new Scale9Image(textures);
			changeMapHolder.addChild(header);
			header.width = background.width
			header.height = PathogenTest.hTenPercent;
			header.pivotX = header.width/2;
			header.y = -background.height/2;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
			var title:TextField = new TextField(header.width, header.height, "Change Map Size", "Questrial", fontSize, Color.WHITE);
			title.pivotX = title.width/2;
			changeMapHolder.addChild(title);
			title.y = header.y;
			
			fontSize = PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor;
			var textX:int = 500 / PathogenTest.scaleFactor;
			var textY:int = 200 / PathogenTest.scaleFactor;
			var textfield:TextField = new starling.text.TextField(textX, textY, "You will lose all your work. Proceed?", "Dekar", fontSize, Color.WHITE);
			textfield.pivotX = textfield.width/2;
			textfield.pivotY = textfield.height/2;
			changeMapHolder.addChild(textfield);
			
			//Create buttons
			changeMapYesButton = new Utils.BW_Button(2, .75, "Yes", 32);
			changeMapHolder.addChild(changeMapYesButton);
			changeMapYesButton.x = -background.width/2 + changeMapYesButton.width/2 + PathogenTest.wTenPercent * .2;
			changeMapYesButton.y = background.height/2 - changeMapYesButton.height;
			changeMapYesButton.addEventListener(TouchEvent.TOUCH, changeMapSize);
			
			changeMapNoButton = new Utils.BW_Button(2, .75, "No", 32);
			changeMapHolder.addChild(changeMapNoButton);
			changeMapNoButton.x = background.width/2 - changeMapNoButton.width/2 - PathogenTest.wTenPercent * .2;
			changeMapNoButton.y = background.height/2 - changeMapNoButton.height;
			changeMapNoButton.addEventListener(TouchEvent.TOUCH, removeChangeMapScreen);
		
			changeMapHolder.alpha = 0;
			changeMapHolder.startfadeIn();
		}
		private function removeChangeMapScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if (touch)
			{
				changeMapHolder.startfadeOut();
				changingMaps = false;
			}
		}
		public function clearChangedCells():void
		{
			for each (var cell:EditorCell in gameBoard)
			{
				cell.setChanged(false);
			}
		}
		public function touchMoved(x:Number, y:Number, cell:EditorCell):void
		{
			var touchPoint:Point = new Point(x - (_boardHolder.x - _boardHolder.width/2), y - (_boardHolder.y - _boardHolder.height/2));
			var xIndex:int = (touchPoint.x * cellScale) / cellWidth;
			var yIndex:int = (touchPoint.y * cellScale) / cellHeight;
			var touchIndex:int = (xIndex * cols) + yIndex;
			if(touchIndex <= gameBoard.length-1)
			{
				if(gameBoard[touchIndex].state != 4)
				{		
					if (!gameBoard[touchIndex].getChanged())
					{
						if (eraseSelection == -1)
							gameBoard[touchIndex].changeColor(selection);
						else
						{
							if (gameBoard[touchIndex].getState() == eraseSelection)
								gameBoard[touchIndex].changeColor(0);
						}
					}
				}
			}
		}
		public function setCellChanged(inCellChanged:Boolean):void
		{
			cellChanged = inCellChanged;
		}
		public function getSelection():int
		{
			return selection;
		}
		public function setEraseSelection(inEraseSelection:int):void
		{
			eraseSelection = inEraseSelection;
		}
		public function quitMapEditor($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if(alpha < 1.0) return;
			
			if (touch)
			{
				backBt.removeEventListener(TouchEvent.TOUCH, quitMapEditor);
				
				main.openMainMenu();
				startFadeOut();
			}
		}
		private function devSave($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				var gameBoardString:String = "";
				
				for each(var i:EditorCell in gameBoard)
				{
					gameBoardString += i.state.toString() + ",";
				}
				
				var xml:String = gameBoardString;
				
				var ba:ByteArray = new ByteArray();
				ba.writeUTFBytes(xml);
				
				var fr:FileReference = new FileReference();
				
				fr.save(xml, "OverflowMap.xml");
			}
		}
		
		private function onTouchLocation($e:TouchEvent):void
		{
			var touches:Vector.<Touch>
			
			touches = $e.getTouches(this, TouchPhase.MOVED);
			
			if (touches.length == 1 && _boardHolder.scaleX > 1.0)
			{
				// one finger touching -> move
				var delta:Point = touches[0].getMovement(parent);
				
				// if the holder is not going off screen, move it left or right
				if (delta.x < 0.0)
				{
					if (_rightBoundary > _startRightBoundary)
					{
						_boardHolder.x += delta.x;
						if ((_boardHolder.x + (_boardHolder.width / 2)) < _startRightBoundary)
							_boardHolder.x = _startRightBoundary - (_boardHolder.width / 2);
					}
				}
				else if (delta.x > 0.0)
				{
					if (_leftBoundary < _startLeftBoundary)
					{
						_boardHolder.x += delta.x;
						if ((_boardHolder.x - (_boardHolder.width / 2)) > _startLeftBoundary)
							_boardHolder.x = _startLeftBoundary + (_boardHolder.width / 2);
					}
				}
				
				// if the holder is not going off screen, move it up or down
				if (delta.y < 0.0)
				{
					if (_bottomBoundary > _startBottomBoundary)
					{
						_boardHolder.y += delta.y;
						if ((_boardHolder.y + (_boardHolder.height / 2)) < _startBottomBoundary)
							_boardHolder.y = _startBottomBoundary - (_boardHolder.height / 2);
					}
				}
				else if (delta.y > 0.0)
				{
					if (_topBoundary < _startTopBoundary)
					{
						_boardHolder.y += delta.y;
						if ((_boardHolder.y - (_boardHolder.height / 2)) > _startTopBoundary)
							_boardHolder.y = _startTopBoundary + (_boardHolder.height / 2);
					}
				}
				
				_topBoundary = _boardHolder.y - (_boardHolder.height / 2);
				_bottomBoundary = _boardHolder.y + (_boardHolder.height / 2);
				_leftBoundary = _boardHolder.x - (_boardHolder.width / 2);
				_rightBoundary = _boardHolder.x + (_boardHolder.width / 2);
				
				_zoomPanDelay = 0;
			}
			else if (touches.length == 2 && _zoomPanDelay >= ZOOM_PAN_DELAY_TIME)
			{
				// two fingers touching -> scale
				var touchA:Touch = touches[0];
				var touchB:Touch = touches[1];
				
				var currentPosA:Point  = touchA.getLocation(this);
				var previousPosA:Point = touchA.getPreviousLocation(this);
				var currentPosB:Point  = touchB.getLocation(this);
				var previousPosB:Point = touchB.getPreviousLocation(this);
				
				var currentVector:Point  = currentPosA.subtract(currentPosB);
				var previousVector:Point = previousPosA.subtract(previousPosB);
				
				// scale
				var sizeDiff:Number = currentVector.length / previousVector.length;
				
				if(sizeDiff > 1.0 && _boardHolder.scaleX <= 2.0)
				{
					_boardHolder.scaleX = 2.0;
					_boardHolder.scaleY = 2.0;
				}
				else if (sizeDiff < 1.0 && _boardHolder.scaleX >= 1.0)
				{
					_boardHolder.scaleX = 1.0;
					_boardHolder.scaleY = 1.0;
				}
				
				// check if scaling smaller than smallest, and if it is, adjust to the smallest size
				if (_boardHolder.scaleX < _holderInitialScaleX)
				{
					_boardHolder.scaleX = _holderInitialScaleX;
				}
				if (_boardHolder.scaleY < _holderInitialScaleY)
				{
					_boardHolder.scaleY = _holderInitialScaleY;
				}
				
				// check if scaling is the initial, and if so, position at initial position
				if (_boardHolder.scaleX == _holderInitialScaleX)
				{
					_boardHolder.x = _holderInitialPosition.x;
					_boardHolder.y = _holderInitialPosition.y;
				}
				
				if ((_boardHolder.x + (_boardHolder.width / 2)) < _startRightBoundary)
					_boardHolder.x = _startRightBoundary - (_boardHolder.width / 2);
				if ((_boardHolder.x - (_boardHolder.width / 2)) > _startLeftBoundary)
					_boardHolder.x = _startLeftBoundary + (_boardHolder.width / 2);
				if ((_boardHolder.y + (_boardHolder.height / 2)) < _startBottomBoundary)
					_boardHolder.y = _startBottomBoundary - (_boardHolder.height / 2);
				if ((_boardHolder.y - (_boardHolder.height / 2)) > _startTopBoundary)
					_boardHolder.y = _startTopBoundary + (_boardHolder.height / 2);
				
				_topBoundary = _boardHolder.y - (_boardHolder.height / 2);
				_bottomBoundary = _boardHolder.y + (_boardHolder.height / 2);
				_leftBoundary = _boardHolder.x - (_boardHolder.width / 2);
				_rightBoundary = _boardHolder.x + (_boardHolder.width / 2);
				
				_zoomPanDelay = 0;
			}
		}
		
		private function onTouchZoom(event:TouchEvent):void
		{
			if(event)
			{
				var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
				
				if (touch && _zoomPanDelay >= ZOOM_PAN_DELAY_TIME)
				{
					if(_boardHolder.scaleX < 2.0)
					{
						_boardHolder.scaleX = 2.0;
						_boardHolder.scaleY = 2.0;
					}
					else
					{
						_boardHolder.scaleX = 1.0;
						_boardHolder.scaleY = 1.0;
					}
					
					// check if scaling smaller than smallest, and if it is, adjust to the smallest size
					if (_boardHolder.scaleX < _holderInitialScaleX)
					{
						_boardHolder.scaleX = _holderInitialScaleX;
					}
					if (_boardHolder.scaleY < _holderInitialScaleY)
					{
						_boardHolder.scaleY = _holderInitialScaleY;
					}
					
					// check if scaling is the initial, and if so, position at initial position
					if (_boardHolder.scaleX == _holderInitialScaleX)
					{
						_boardHolder.x = _holderInitialPosition.x;
						_boardHolder.y = _holderInitialPosition.y;
					}
					
					if ((_boardHolder.x + (_boardHolder.width / 2)) < _startRightBoundary)
						_boardHolder.x = _startRightBoundary - (_boardHolder.width / 2);
					if ((_boardHolder.x - (_boardHolder.width / 2)) > _startLeftBoundary)
						_boardHolder.x = _startLeftBoundary + (_boardHolder.width / 2);
					if ((_boardHolder.y + (_boardHolder.height / 2)) < _startBottomBoundary)
						_boardHolder.y = _startBottomBoundary - (_boardHolder.height / 2);
					if ((_boardHolder.y - (_boardHolder.height / 2)) > _startTopBoundary)
						_boardHolder.y = _startTopBoundary + (_boardHolder.height / 2);
					
					_topBoundary = _boardHolder.y - (_boardHolder.height / 2);
					_bottomBoundary = _boardHolder.y + (_boardHolder.height / 2);
					_leftBoundary = _boardHolder.x - (_boardHolder.width / 2);
					_rightBoundary = _boardHolder.x + (_boardHolder.width / 2);
					
					_zoomPanDelay = 0;
				}
			}
		}
		
		private function update(event:Event):void
		{
			if (_zoomPanDelay < ZOOM_PAN_DELAY_TIME)
				_zoomPanDelay++;
		}
		public function addAchievementPopUp(achievementNumber:int):void
		{
			var achievementPopUp:AchievementPopUp = new AchievementPopUp();
			addChild(achievementPopUp);
			achievementPopUp.init(achievementNumber);
			achievementPopUp.x = PathogenTest.stageCenter.x;
			achievementPopUp.pivotX = achievementPopUp.width/2;
			achievementPopUp.y = PathogenTest.stageHeight;
			achievementPopUp.mapEditor = this;
			achievementPopUp.start();
		}
		public function removeAchievementPopUp(achivementPopUp:AchievementPopUp):void
		{
			achivementPopUp.dispose();
			removeChild(achivementPopUp);
			achivementPopUp = null;
		}
		
		override public function death():void
		{
			saveButton.removeEventListener(TouchEvent.TOUCH, addSaveScreen);
			saveButton.destroy();
			saveButton = null;
			
			loadButton.removeEventListener(TouchEvent.TOUCH, addloadScreen);
			loadButton.destroy();
			loadButton = null;
			
			if(devSaveButton)
			{
				devSaveButton.removeEventListener(TouchEvent.TOUCH, devSave);
				devSaveButton.dispose();
				removeChild(devSaveButton);
				devSaveButton = null;
			}
			
			for (var i:int = 0; i < gameBoard.length; i++)
			{
				var editorCell:EditorCell = gameBoard[i];
				
				editorCell.dispose();
				editorCell.cleanUp();
				_boardHolder.removeChild(editorCell);
				editorCell = null;
			}
			
			gameBoard = [];
			
			if(_boardHolder)
			{
				_boardHolder.dispose();
				removeChild(_boardHolder);
				_boardHolder = null;
			}
			
			resetButton.removeEventListener(TouchEvent.TOUCH, reset);
			resetButton.destroy();
			resetButton = null;
			
			smallMapButton.removeEventListener(TouchEvent.TOUCH, selectSmallMap);
			smallMapButton.dispose();
			removeChild(smallMapButton);
			smallMapButton = null;
			
			mediumMapButton.removeEventListener(TouchEvent.TOUCH, selectMediumMap);
			mediumMapButton.dispose();
			removeChild(mediumMapButton);
			mediumMapButton = null;
			
			largeMapButton.removeEventListener(TouchEvent.TOUCH, selectLargeMap);
			largeMapButton.dispose();
			removeChild(largeMapButton);
			largeMapButton = null;
			
			green1Button.removeEventListener(TouchEvent.TOUCH, green1ButtonSelect);
			green1Button.dispose();
			removeChild(green1Button);
			green1Button = null;
			
			green2Button.removeEventListener(TouchEvent.TOUCH, green2ButtonSelect);
			green2Button.dispose();
			removeChild(green2Button);
			green2Button = null;
			
			green3Button.removeEventListener(TouchEvent.TOUCH, green3ButtonSelect);
			green3Button.dispose();
			removeChild(green3Button);
			green3Button = null;
			
			green4Button.removeEventListener(TouchEvent.TOUCH, green4ButtonSelect);
			green4Button.dispose();
			removeChild(green4Button);
			green4Button = null;
			
			greenCellHolder.dispose();
			removeChild(greenCellHolder);
			greenCellHolder = null;
			
			// red cell selectors
			red1Button.removeEventListener(TouchEvent.TOUCH, red1ButtonSelect);
			red1Button.dispose();
			removeChild(red1Button);
			red1Button = null;
			
			red2Button.removeEventListener(TouchEvent.TOUCH, red2ButtonSelect);
			red2Button.dispose();
			removeChild(red2Button);
			red2Button = null;
			
			red3Button.removeEventListener(TouchEvent.TOUCH, red3ButtonSelect);
			red3Button.dispose();
			removeChild(red3Button);
			red3Button = null;
			
			red4Button.removeEventListener(TouchEvent.TOUCH, red4ButtonSelect);
			red4Button.dispose();
			removeChild(red4Button);
			red4Button = null;
			
			redCellHolder.dispose();
			removeChild(redCellHolder);
			redCellHolder = null;
			
			// blue cell selectors
			blue1Button.removeEventListener(TouchEvent.TOUCH, blue1ButtonSelect);
			blue1Button.dispose();
			removeChild(blue1Button);
			blue1Button = null;
			
			blue2Button.removeEventListener(TouchEvent.TOUCH, blue2ButtonSelect);
			blue2Button.dispose();
			removeChild(blue2Button);
			blue2Button = null;
			
			blue3Button.removeEventListener(TouchEvent.TOUCH, blue3ButtonSelect);
			blue3Button.dispose();
			removeChild(blue3Button);
			blue3Button = null;
			
			blue4Button.removeEventListener(TouchEvent.TOUCH, blue4ButtonSelect);
			blue4Button.dispose();
			removeChild(blue4Button);
			blue4Button = null;
			
			blueCellHolder.dispose();
			removeChild(blueCellHolder);
			blueCellHolder = null;
			
			// yellow cell selectors
			yellow1Button.removeEventListener(TouchEvent.TOUCH, yellow1ButtonSelect);
			yellow1Button.dispose();
			removeChild(yellow1Button);
			yellow1Button = null;
			
			yellow2Button.removeEventListener(TouchEvent.TOUCH, yellow2ButtonSelect);
			yellow2Button.dispose();
			removeChild(yellow2Button);
			yellow2Button = null;
			
			yellow3Button.removeEventListener(TouchEvent.TOUCH, yellow3ButtonSelect);
			yellow3Button.dispose();
			removeChild(yellow3Button);
			yellow3Button = null;
			
			yellow4Button.removeEventListener(TouchEvent.TOUCH, yellow4ButtonSelect);
			yellow4Button.dispose();
			removeChild(yellow4Button);
			yellow4Button = null;
			
			yellowCellHolder.dispose();
			removeChild(yellowCellHolder);
			yellowCellHolder = null;
			
			// gray cell selectors
			gray1Button.removeEventListener(TouchEvent.TOUCH, gray1ButtonSelect);
			gray1Button.dispose();
			removeChild(gray1Button);
			gray1Button = null;
			
			gray2Button.removeEventListener(TouchEvent.TOUCH, gray2ButtonSelect);
			gray2Button.dispose();
			removeChild(gray2Button);
			gray2Button = null;
			
			gray3Button.removeEventListener(TouchEvent.TOUCH, gray3ButtonSelect);
			gray3Button.dispose();
			removeChild(gray3Button);
			gray3Button = null;
			
			// capture and erosion
			captureButton.removeEventListener(TouchEvent.TOUCH, captureButtonSelect);
			captureButton.dispose();
			removeChild(captureButton);
			captureButton = null;
			
			erosionButton.removeEventListener(TouchEvent.TOUCH, erosionButtonSelect);
			erosionButton.dispose();
			removeChild(erosionButton);
			erosionButton = null;
			
			utilityHolder.dispose();
			removeChild(utilityHolder);
			utilityHolder = null;
			
			cellSelector.dispose();
			removeChild(cellSelector);
			cellSelector = null;
			
			backBt.dispose();
			removeChild(backBt);
			backBt = null;
			
			if (_zoomButton)
			{
				_zoomButton.removeEventListener(TouchEvent.TOUCH, onTouchZoom);
				_zoomButton.dispose();
				removeChild(_zoomButton);
				_zoomButton = null;
			}
			
			removeEventListener(Event.ENTER_FRAME, update);
			
			gameBoard = [];
			
			main.removeMapEditor();
		}
		
		public function isZoomed():Boolean
		{
			if (_boardHolder.scaleX > 1.0)
				return true;
			return false;
		}
		
		public function recentlyPanned():Boolean
		{
			if (_zoomPanDelay < ZOOM_PAN_DELAY_TIME)
				return true;
			return false;
		}
		
		public function get saveLocation():int {return _saveLocation};
		public function set saveLocation(saveLocation:int):void { _saveLocation = saveLocation};
	}
}