package Editor
{
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import UI.Main;
	
	public class EditorCell extends Sprite
	{
		public var state:int = 0;
		private var selectedEditorTileType:int = 1;
		private var changed:Boolean = false;
		private var display:MovieClip;
		private var mapEditor:MapEditor;
		private var index:int;
		private var originalScaleX:int;
		private var originalScaleY:int;
		private var originalWidth:int;
		private var originalHeight:int;
		private var originalState:int;
		
		private var smallScaleX:int;
		private var smallScaleY:int;
		private var smallWidth:int;
		private var smallHeight:int;
		
		public function EditorCell(inEditor:MapEditor, inIndex:int)
		{
			super();

			mapEditor = inEditor;
			index = inIndex;
		}
		public function init():void
		{
			display = new MovieClip(Main.assets.getTextures("Grid"));
			addChild(display);
			originalScaleX = scaleX;
			originalScaleY = scaleY;
			originalWidth = width;
			originalHeight = height;
			originalState = state;
		}
		public function changeColor(id:int):void
		{
			if (!changed || mapEditor.isZoomed())
			{
				state = id;
				
				switch(state)
				{
					case 0:
						display.texture = Main.assets.getTexture("Grid");
						break;
					case 1:
						display.texture = Main.assets.getTexture("Grey_Stage01");
						break;
					case 2:
						display.texture = Main.assets.getTexture("Grey_Stage02");
						break;
					case 3:
						display.texture = Main.assets.getTexture("Grey_Stage04");
						break;
					case 4:
						visible = false;
						break;
					case 5:
						display.texture = Main.assets.getTexture("Green_Stage01");
						break;
					case 6:
						display.texture = Main.assets.getTexture("Green_Stage02");
						break;
					case 7:
						display.texture = Main.assets.getTexture("Green_Stage03");
						break;
					case 8:
						display.texture = Main.assets.getTexture("Green_Stage04");
						break;
					case 9:
						display.texture = Main.assets.getTexture("Red_Stage01");
						break;
					case 10:
						display.texture = Main.assets.getTexture("Red_Stage02");
						break;
					case 11:
						display.texture = Main.assets.getTexture("Red_Stage03");
						break;
					case 12:
						display.texture = Main.assets.getTexture("Red_Stage04");
						break;
					case 13:
						display.texture = Main.assets.getTexture("Blue_Stage01");
						break;
					case 14:
						display.texture = Main.assets.getTexture("Blue_Stage02");
						break;
					case 15:
						display.texture = Main.assets.getTexture("Blue_Stage03");
						break;
					case 16:
						display.texture = Main.assets.getTexture("Blue_Stage04");
						break;
					case 17:
						display.texture = Main.assets.getTexture("Yellow_Stage01");
						break;
					case 18:
						display.texture = Main.assets.getTexture("Yellow_Stage02");
						break;
					case 19:
						display.texture = Main.assets.getTexture("Yellow_Stage03");
						break;
					case 20:
						display.texture = Main.assets.getTexture("Yellow_Stage04");
						break;
					case 21:
						//Capture Zone
						display.texture = Main.assets.getTexture("Capture_Zone");
						break;
					case 22:
						//Erosion Zone
						display.texture = Main.assets.getTexture("Erosion_Zone");
						break;
					default:
						break;					
				}
				
				if (!mapEditor.isZoomed())
				{
					changed = true;
					mapEditor.setCellChanged(true);
				}
			}
		}
		
		private function touchCell($e:TouchEvent):void
		{
			if (!mapEditor.isZoomed())
			{
				var touchBegan:Touch = $e.getTouch(this, TouchPhase.BEGAN);
			
				if (touchBegan && !changed)
				{
					if(state != 4)
					{
						if (mapEditor.getSelection() == state)
						{
							mapEditor.setEraseSelection(state);
							changeColor(0);
						}
						else
							changeColor(mapEditor.getSelection());
					}
				}
				
				var touchMoved:Touch = $e.getTouch(this, TouchPhase.MOVED);
				if (touchMoved)
				{
					mapEditor.touchMoved(touchMoved.globalX, touchMoved.globalY, this);
				}
			}
			else	
			{
				var touchEnd:Touch = $e.getTouch(this, TouchPhase.ENDED);
				
				if (touchEnd && !mapEditor.recentlyPanned())
				{
					if(state != 4)
					{
						if (mapEditor.getSelection() == state)
							changeColor(0);
						else
							changeColor(mapEditor.getSelection());
					}
				}
			}
		}
		
		public function resetVals():void
		{
			scaleX = originalScaleX;
			scaleY = originalScaleY;
			width = originalWidth;
			height = originalHeight;
			if (display)
				display.texture = Main.assets.getTexture("Grid");
			addEventListener(TouchEvent.TOUCH, touchCell);
			state = 0;
			visible = true;
		}
		public function removeTouchEvent():void
		{
			removeEventListener(TouchEvent.TOUCH, touchCell);
		}
		
		public function getChanged():Boolean
		{
			return changed;
		}
		
		public function setChanged(inChanged:Boolean):void
		{
			changed = inChanged;
		}
		
		public function getState():int
		{
			return state;
		}
		public function setState(inState:int):void
		{
			state = inState;
		}
		public function setMapEditor(inMapEditor:MapEditor):void
		{
			mapEditor = inMapEditor;
		}
		public function cleanUp():void
		{
			display.dispose();
			display = null;
		}
	}
}