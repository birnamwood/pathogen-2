package Editor
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_Window;
	
	import feathers.controls.TextInput;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class MapCreatedScreen extends BW_Window
	{
		//Classes
		private var _mapEditor:MapEditor;
		
		//UI
		private var _okButton:BW_Button;
		
		public function MapCreatedScreen(mapEditor:MapEditor)
		{
			super();
			
			_mapEditor = mapEditor;
			
			createbackground(4.5,4);
			
			init();
		}
		private function init():void
		{
			var textfield:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Your map has been saved", "Dekar", PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor, Color.WHITE);
			textfield.pivotX = textfield.width/2;
			textfield.pivotY = textfield.height/2;
			textfield.x = _background.x;
			textfield.y = _background.y - PathogenTest.hTenPercent;
			addChild(textfield);
			
			_okButton = new Utils.BW_Button(1.3, .8, "Ok", 22);
			addChild(_okButton);
			_okButton.x = _background.x;
			_okButton.y = _background.y + _background.height/2 - _okButton.height;
			_okButton.addEventListener(TouchEvent.TOUCH, onTouchOk);
		}
		private function onTouchOk($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{	
				_okButton.removeEventListener(TouchEvent.TOUCH, onTouchOk);
				startFadeOut();
			}
		}
		public override function death():void
		{
			if(_okButton)
			{
				_okButton.dispose();
				_okButton.destroy();
				removeChild(_okButton);
				_okButton = null;
			}
			
			_mapEditor.removeMapCreatedScreen();
			_mapEditor = null;
		}
	}
}