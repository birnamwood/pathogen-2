package Editor
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_Window;
	
	import feathers.controls.TextInput;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class SaveScreen extends BW_Window
	{
		//Classes
		private var _mapEditor:MapEditor;
		
		//UI
		private var _backButton:BW_Button;
		private var _okButton:BW_Button;
		
		//Vars
		private var _mapName:TextInput;
		
		public function SaveScreen(mapEditor:MapEditor)
		{
			super();
			
			_mapEditor = mapEditor;
			
			createbackground(4.5,4);
			createHeader("Name Your Map");
			
			init();
		}
		private function init():void
		{
			var textInputBackground:Quad = new Quad(PathogenTest.hTenPercent * 3, PathogenTest.hTenPercent * 0.65, Color.BLACK);
			textInputBackground.pivotX = textInputBackground.width/2;
			textInputBackground.pivotY = textInputBackground.height/2;
			addChild(textInputBackground);
			textInputBackground.alpha = .5;
			textInputBackground.x = _background.x;
			textInputBackground.y = _background.y;
			
			_mapName = new TextInput();
			_mapName.width = PathogenTest.hTenPercent * 3;
			if (Main.isPhone() && Main.isAndroid())
				_mapName.height = PathogenTest.hTenPercent * 0.85;
			else
				_mapName.height = PathogenTest.hTenPercent * 0.65;
			_mapName.pivotX = _mapName.width / 2;
			_mapName.pivotY = _mapName.height / 2;
			_mapName.textEditorProperties.fontFamily = "Dekar";
			_mapName.textEditorProperties.fontSize = PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor;
			_mapName.textEditorProperties.color = Color.WHITE;
			_mapName.text = "Your Map";
			_mapName.setFocus();
			_mapName.selectRange(_mapName.text.length);
			_mapName.paddingTop = 5;
			_mapName.paddingLeft = 5;
			addChild(_mapName);
			_mapName.x = textInputBackground.x;
			if (Main.isPhone() && Main.isAndroid())
				_mapName.y = textInputBackground.y - PathogenTest.hTenPercent * 0.5;
			else
				_mapName.y = textInputBackground.y;
			
			//Back Button
			if (Main.isPhone())
				_backButton = new BW_Button(.75,1.0, "", 0);
			else
				_backButton = new BW_Button(.5,.7, "", 0);
			
			_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(_backButton);
			_backButton.x = _background.x - _background.width/2 + _backButton.width;
			_backButton.y = _background.y + _background.height/2 - _backButton.height;
			_backButton.addEventListener(TouchEvent.TOUCH, onTouchQuit);
			
			//Ok Button
			if (Main.isPhone())
				_okButton = new Utils.BW_Button(1.5, 1, "Ok", 22);
			else
				_okButton = new Utils.BW_Button(1.3, .8, "Ok", 22);
			
			addChild(_okButton);
			_okButton.x = _background.x;
			_okButton.y = _background.y + _background.height/2 - _okButton.height;
			_okButton.addEventListener(TouchEvent.TOUCH, onTouchOk);
		}
		private function onTouchOk($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_mapEditor.saveMap(_mapName.text);
				
				_okButton.removeEventListener(TouchEvent.TOUCH, onTouchOk)
				_backButton.removeEventListener(TouchEvent.TOUCH, onTouchQuit);
				
				startFadeOut();
			}
		}
		private function onTouchQuit($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_okButton.removeEventListener(TouchEvent.TOUCH, onTouchOk)
				_backButton.removeEventListener(TouchEvent.TOUCH, onTouchQuit);
				
				startFadeOut();
			}
		}
		public override function death():void
		{
			if(_okButton)
			{
				_okButton.dispose();
				_okButton.destroy();
				removeChild(_okButton);
				_okButton = null;
			}
			
			if(_backButton)
			{
				_backButton.dispose();
				_backButton.destroy();
				removeChild(_backButton);
				_backButton = null;
			}
			
			_mapEditor.removeSaveScreen();
			_mapEditor = null;
		}
	}
}