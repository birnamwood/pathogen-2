package Editor
{
	import Maps.MapPreview;
	
	import UI.Main;
	import UI.fSprite;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;

	public class MapSlotScreen extends BW_UI
	{
		//Classes
		private var mapEditor:MapEditor;
		private var _overwriteScreen:OverwriteScreen;
		
		//UI
		private var _smallMapButton:BW_Button;
		private var _mediumMapButton:BW_Button;
		private var _largeMapButton:BW_Button;
		private var _okButton:BW_Button;
		private var _backButton:BW_Button;
		private var saveSlotSelector:Scale9Image;
		
		private var slotHolder:Sprite;
		private var holder:Sprite;
		
		public var chosenSaveSlot:SaveSlot;
		public var chosenSize:int = 0;
		private var chosenArray:Array;
		private var _currentSlotArray:Array = [];
		
		private var smallMapArray:Array = [];
		private var mediumMapArray:Array = [];
		private var largeMapArray:Array = [];
		
		private var whatFunction:String;
		
		//Save
		private var saveDataObject:SharedObject;
		private var yesOverwrite:BW_Button;
		private var noOverwrite:BW_Button;
		
		private var mapPreview:MapPreview;
		
		public function MapSlotScreen(mapEditor:MapEditor, whatFunction:String)
		{
			super();
			
			this.mapEditor = mapEditor;
			this.whatFunction = whatFunction;
			saveDataObject = Main.saveDataObject;
			
			holder = new Sprite();
			holder.pivotX = holder.width/2;
			holder.pivotY = holder.height/2;
			addChild(holder);
			
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			holder.addChild(transBackground);
			transBackground.alpha = .75;
			
			// create bg
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var background:Scale9Image = new Scale9Image(textures);
			holder.addChild(background);
			background.width = PathogenTest.wTenPercent * 8;
			background.height = PathogenTest.hTenPercent * 7;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			
			var header:Scale9Image = new Scale9Image(textures);
			holder.addChild(header);
			header.width = background.width
			header.height = PathogenTest.hTenPercent;
			header.pivotX = header.width/2;
			header.y = -background.height/2;
			
			var previewBackground:Scale9Image = new Scale9Image(textures);
			holder.addChild(previewBackground);
			previewBackground.width = PathogenTest.wTenPercent * 3;
			previewBackground.height = PathogenTest.hTenPercent * 4.1;
			previewBackground.pivotX = previewBackground.width/2;
			previewBackground.pivotY = previewBackground.height/2;
			previewBackground.x = PathogenTest.wTenPercent * 2.2;
			previewBackground.y = -PathogenTest.hTenPercent *.1
			
			var fontSize:int = 50 / PathogenTest.scaleFactor;
			var title:TextField = new TextField(header.width,header.height, whatFunction + " Map", "Dekar", fontSize, Color.WHITE);
			title.pivotX = title.width/2;
			holder.addChild(title);
			title.y = header.y;
			
			// create map size selectors
			_smallMapButton = new Utils.BW_Button(1.3, .8, "View your Small Maps", 22);
			holder.addChild(_smallMapButton);
			_smallMapButton.x = -PathogenTest.wTenPercent * 3;
			_smallMapButton.y = -PathogenTest.hTenPercent * 1.75;
			
			var gap:Number = _smallMapButton.width/8;
			
			_mediumMapButton = new Utils.BW_Button(1.3, .8, "View your Medium Maps", 22);
			holder.addChild(_mediumMapButton);
			_mediumMapButton.x = _smallMapButton.x + _smallMapButton.width + gap;
			_mediumMapButton.y = _smallMapButton.y;
			
			_largeMapButton = new Utils.BW_Button(1.3, .8, "View your Large Maps", 22);
			holder.addChild(_largeMapButton);
			_largeMapButton.x = _mediumMapButton.x + gap + _mediumMapButton.width;
			_largeMapButton.y = _smallMapButton.y;
			
			if(whatFunction == "Load")
			{
				_smallMapButton.addEventListener(TouchEvent.TOUCH, onTouchSmallMap)
				_mediumMapButton.addEventListener(TouchEvent.TOUCH, onTouchMediumMap);
				_largeMapButton.addEventListener(TouchEvent.TOUCH, onTouchLargeMap);
			}
			
			// ok Button
			if (Main.isPhone())
				_okButton = new Utils.BW_Button(1.5, 1.0, "Ok", 22);
			else
				_okButton = new Utils.BW_Button(1.5, 1.0, "Ok", 22);
			
			holder.addChild(_okButton);
			_okButton.y = background.height/2 - _okButton.height;
			
			if(whatFunction == "Load")
				_okButton.addEventListener(TouchEvent.TOUCH, mapEditor.load);
			else
				_okButton.addEventListener(TouchEvent.TOUCH, addMapOverwriteScreen);
			
			// back Button
			if (Main.isPhone())
				_backButton = new BW_Button(.75,1.0, "", 0);
			else
				_backButton = new BW_Button(.5,.7, "", 0);
			
			_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			holder.addChild(_backButton);
			_backButton.x = -background.width/2 + _backButton.width;
			_backButton.y = background.height/2 - _backButton.height;
			_backButton.addEventListener(TouchEvent.TOUCH, onTouchQuit);
			
			checkSaveSlots();
			
			//Default
			chosenSize = mapEditor.currentSize;
			reset();
		}
		private function onTouchSmallMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				chosenSize = 0;
				reset()
			}
		}
		private function onTouchMediumMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				chosenSize = 1;
				reset()
			}
		}
		private function onTouchLargeMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				chosenSize = 2;
				reset()
			}
		}
		private function reset():void
		{
			_smallMapButton.alpha = .5;
			_mediumMapButton.alpha = .5;
			_largeMapButton.alpha = .5;
			
			switch(chosenSize)
			{
				case 0:
					_smallMapButton.alpha = 1;
					chosenArray = smallMapArray;
					break;
				case 1:
					_mediumMapButton.alpha = 1;
					chosenArray = mediumMapArray;
					break;
				case 2:
					_largeMapButton.alpha = 1;
					chosenArray = largeMapArray;
					break;
				default:
					break;
			}
			
			setup();
		}
		private function setup():void
		{
			trace("Setup");
			
			if(slotHolder)
			{
				for each (var slot:SaveSlot in _currentSlotArray)
				{
					slot.death();
					slot.dispose();
					slotHolder.removeChild(slot);
					slot = null;
				}
				
				slotHolder.dispose();
				removeChild(slotHolder);
				slotHolder = null;
				
				_currentSlotArray = [];
			}
				
			
			if(saveSlotSelector)
			{
				removeChild(saveSlotSelector);
				saveSlotSelector = null;
			}
			
			slotHolder = new Sprite();
			addChild(slotHolder);
			
			var topY:Number = -PathogenTest.hTenPercent * .5;
			var midY:Number = topY + PathogenTest.hTenPercent * 1;
			var bottomY:Number = midY + PathogenTest.hTenPercent * 1;
			var left:Number = -PathogenTest.wTenPercent * 2.65;
			var right:Number = -PathogenTest.wTenPercent * .5;
			
			var slotLocations:Array = [new Point(left, topY), new Point(right, topY), new Point(left, midY), new Point(right, midY), new Point(left, bottomY), new Point(right, bottomY)] 
			
			var maxSlots:int = 6;
			var currentSlot:int = 0;
			
			for each (var map:String in chosenArray)
			{
				if(map != null)
				{
					var saveSlot:SaveSlot = new SaveSlot(map, this, currentSlot);
					slotHolder.addChild(saveSlot);
					saveSlot.x = slotLocations[currentSlot].x;
					saveSlot.y = slotLocations[currentSlot].y;
					_currentSlotArray.push(saveSlot);
				}
				
				currentSlot++;
			}
		}
		private function checkSaveSlots():void
		{
			//Small Maps
			if (saveDataObject.data.userSmall_01Map != null)
				smallMapArray.push(saveDataObject.data.userSmall_01Map);
			if (saveDataObject.data.userSmall_02Map != null)
				smallMapArray.push(saveDataObject.data.userSmall_02Map);
			if (saveDataObject.data.userSmall_03Map != null)
				smallMapArray.push(saveDataObject.data.userSmall_03Map);
			if (saveDataObject.data.userSmall_04Map != null)
				smallMapArray.push(saveDataObject.data.userSmall_04Map);
			if (saveDataObject.data.userSmall_05Map != null)
				smallMapArray.push(saveDataObject.data.userSmall_05Map);
			if (saveDataObject.data.userSmall_06Map != null)
				smallMapArray.push(saveDataObject.data.userSmall_06Map);
			//Medium Maps
			if (saveDataObject.data.userMedium_06Map != null)
				mediumMapArray.push(saveDataObject.data.userMedium_06Map);
			if (saveDataObject.data.userMedium_05Map != null)
				mediumMapArray.push(saveDataObject.data.userMedium_05Map);
			if (saveDataObject.data.userMedium_04Map != null)
				mediumMapArray.push(saveDataObject.data.userMedium_04Map);
			if (saveDataObject.data.userMedium_03Map != null)
				mediumMapArray.push(saveDataObject.data.userMedium_03Map);
			if (saveDataObject.data.userMedium_02Map != null)
				mediumMapArray.push(saveDataObject.data.userMedium_02Map);
			if (saveDataObject.data.userMedium_01Map != null)
				mediumMapArray.push(saveDataObject.data.userMedium_01Map);
			//Large Maps
			if (saveDataObject.data.userLarge_06Map != null)
				largeMapArray.push(saveDataObject.data.userLarge_06Map);
			if (saveDataObject.data.userLarge_05Map != null)
				largeMapArray.push(saveDataObject.data.userLarge_05Map);
			if (saveDataObject.data.userLarge_04Map != null)
				largeMapArray.push(saveDataObject.data.userLarge_04Map);
			if (saveDataObject.data.userLarge_03Map != null)
				largeMapArray.push(saveDataObject.data.userLarge_03Map);
			if (saveDataObject.data.userLarge_02Map != null)
				largeMapArray.push(saveDataObject.data.userLarge_02Map);
			if (saveDataObject.data.userLarge_01Map != null)
				largeMapArray.push(saveDataObject.data.userLarge_01Map);
		}
		public function selectedSaveSlot(whichSaveSlot:SaveSlot):void
		{
			if(saveSlotSelector == null)
			{
				var scale9Num:Number = 20/PathogenTest.scaleFactor;
				const texture:Texture = Main.assets.getTexture("scale9_outline");
				const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
				
				saveSlotSelector = new Scale9Image(textures);
				addChild(saveSlotSelector);
				saveSlotSelector.width = PathogenTest.wTenPercent * 2;
				saveSlotSelector.height = PathogenTest.hTenPercent * .8;
				saveSlotSelector.pivotX = saveSlotSelector.width/2;
				saveSlotSelector.pivotY = saveSlotSelector.height/2;
			}
			
			saveSlotSelector.x = whichSaveSlot.x;
			saveSlotSelector.y = whichSaveSlot.y;
			setChildIndex(saveSlotSelector, numChildren -1);
			
			chosenSaveSlot = whichSaveSlot;
			mapEditor.saveLocation = chosenSaveSlot.location;
			
			addPreview();
		}
		public function addPreview():void
		{
			if(mapPreview)
			{
				mapPreview.dispose();
				removeChild(mapPreview);
				mapPreview = null;
			}
			
			mapPreview = new MapPreview(chosenSaveSlot.mapName, chosenSaveSlot.mapData, chosenSize, null);
			addChild(mapPreview);
			mapPreview.scaleX = .5;
			mapPreview.scaleY = .5;
			mapPreview.x = PathogenTest.wTenPercent * 2.2;
			mapPreview.y = -PathogenTest.hTenPercent *.1;
		}
		private function addMapOverwriteScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{		
				mapEditor.addMapOverwriteScreen();
			}
		}
		public function onTouchQuit($e:TouchEvent):void
		{	
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				startFadeOut();
			}
		}
		public override function death():void
		{
			//Remove UI
			if(_smallMapButton)
			{
				_smallMapButton.dispose();
				_smallMapButton.destroy();
				removeChild(_smallMapButton);
				_smallMapButton = null;
			}
			
			if(_mediumMapButton)
			{
				_mediumMapButton.dispose();
				_mediumMapButton.destroy();
				removeChild(_mediumMapButton);
				_mediumMapButton = null;
			}
			
			if(_largeMapButton)
			{
				_largeMapButton.dispose();
				_largeMapButton.destroy();
				removeChild(_largeMapButton);
				_largeMapButton = null;
			}
			
			if(_okButton)
			{
				_okButton.dispose();
				_okButton.destroy();
				removeChild(_okButton);
				_okButton = null;
			}
			
			if(_backButton)
			{
				_backButton.dispose();
				_backButton.destroy();
				removeChild(_backButton);
				_backButton = null;
			}
			
			if(mapPreview)
			{
				mapPreview.garbageCollection();
				mapPreview.dispose();
				removeChild(mapPreview);
				mapPreview = null;
			}
			
			//Remove Generated Buttons
			if(slotHolder)
			{
				for each (var slot:SaveSlot in _currentSlotArray)
				{
					slot.dispose();
					slotHolder.removeChild(slot);
					slot = null;
				}
				
				slotHolder.dispose();
				removeChild(slotHolder);
				slotHolder = null;
			}
			
			mapEditor.removeLoadScreen();
			mapEditor = null;
		}
	}
}