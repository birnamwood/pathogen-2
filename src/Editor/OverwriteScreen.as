package Editor
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_Window;
	
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class OverwriteScreen extends BW_Window
	{
		//Classes
		private var _mapEditor:MapEditor;
		
		//UI
		private var _yesButton:BW_Button;
		private var _noButton:BW_Button;
		
		public function OverwriteScreen(mapEditor:MapEditor)
		{
			super();
			
			_mapEditor = mapEditor;
			
			createbackground(4.5,4);
			createHeader("Overwrite?");
			
			init();
		}
		private function init():void
		{
			var textfield:TextField = new TextField(_background.width,PathogenTest.hTenPercent * 2, "A map by this name aready exists, overwrite it?", "Dekar", PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor, Color.WHITE);
			textfield.pivotX = textfield.width/2;
			textfield.pivotY = textfield.height/2;
			addChild(textfield);
			
			textfield.x = _background.x;
			textfield.y = _background.y;
			
			//Create buttons
			_yesButton = new Utils.BW_Button(2, .75, "Yes", 32);
			addChild(_yesButton);
			_yesButton.x = _background.x - _background.width/2 + _yesButton.width/2 + PathogenTest.wTenPercent * .2;
			_yesButton.y = _background.y + _background.height/2 - _yesButton.height;
			_yesButton.addEventListener(TouchEvent.TOUCH, onTouchYes);
			
			_noButton = new Utils.BW_Button(2, .75, "No", 32);
			addChild(_noButton);
			_noButton.x = _background.x + _background.width/2 - _noButton.width/2 - PathogenTest.wTenPercent * .2;
			_noButton.y = _yesButton.y;
			_noButton.addEventListener(TouchEvent.TOUCH, onTouchNo);
		}
		private function onTouchYes($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{	
				_mapEditor.overwriteMap();
				
				_yesButton.removeEventListener(TouchEvent.TOUCH, onTouchYes);
				_noButton.removeEventListener(TouchEvent.TOUCH, onTouchNo)
				startFadeOut();
			}
		}
		private function onTouchNo($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{	
				_yesButton.removeEventListener(TouchEvent.TOUCH, onTouchYes);
				_noButton.removeEventListener(TouchEvent.TOUCH, onTouchNo);
				
				startFadeOut();
			}
		}
		public override function death():void
		{
			//Remove Buttons
			if(_yesButton)
			{
				_yesButton.dispose();
				_yesButton.destroy();
				removeChild(_yesButton);
				_yesButton = null;
			}
			
			if(_noButton)
			{
				_noButton.dispose();
				_noButton.destroy();
				removeChild(_noButton);
				_noButton = null;
			}
			
			_mapEditor.removeMapOverwriteScreen();
			_mapEditor.removeLoadScreen();
			_mapEditor = null;
		}
	}
}