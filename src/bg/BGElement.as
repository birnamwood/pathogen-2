package bg
{
	import flash.geom.Point;
	
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class BGElement extends Sprite
	{
		public var wanderingDistanceX:int;
		public var wanderingDistanceY:int;
		private var destination:Point;
		private var minimumParticleSpeed:Number = .05;
		private var maximumParticleSpeed:Number = .2;
		public var particleSpeed:Number;
		private var NumOfInputs:int;
		//In Seconds
		public var minimumLengthOfLife:int = 10;
		public var maximumLengthofLife:int = 10;
		
		public var fadingOut:Boolean;
		private var fadingIn:Boolean = true;
		private var fadingSpeed:Number = .01;
		private var lifetime:Number;
		
		private var particleScale:Number;
		private var minimumScale:Number = .3;
		private var maximumScale:Number = .7;
		
		private var particleAlpha:Number;
		private var minimumAlpha:Number = .3;
		private var maximumAlpha:Number = .6;
		
		private var randomX:int;
		private var randomY:int;
		
		private var frameCount:int = 0;
		private var frameThreshold:int = 60;
		private var secondCounter:int = 0;
		
		public var asymetricalShape:Boolean = false;
		public var display:MovieClip
		
		public function BGElement()
		{
			super();
			
			wanderingDistanceX = PathogenTest.stageWidth;
			wanderingDistanceY = PathogenTest.stageHeight;
			
			setup();
		}
		
		private function setup():void
		{
			lifetime = minimumLengthOfLife + Math.round(Math.random() * maximumLengthofLife);
			particleScale = minimumScale + (Math.random() * maximumScale);
			particleAlpha = minimumAlpha + (Math.random() * maximumAlpha);
			
			scaleX = particleScale;
			scaleY = particleScale;
			
			particleSpeed = minimumParticleSpeed + (Math.random() * maximumParticleSpeed);
			
			alpha = 0;
			fadingIn = true;
			
			randomX = Math.round(Math.random() * wanderingDistanceX);
			randomY = Math.round(Math.random() * wanderingDistanceY);
			
			x = randomX;
			y = randomY;
				
			pickADestination()
		}
		private function pickADestination():void
		{
			//Pick a random X and Y
			randomX = Math.round(Math.random() * wanderingDistanceX);
			randomY = Math.round(Math.random() * wanderingDistanceY);
			
			destination = new Point(randomX, randomY)
				
			addEventListener(Event.ENTER_FRAME, wander);
		}
		private function wander($e:Event):void
		{
			frameCount++
			
			if (frameCount == frameThreshold)
			{
				frameCount = 0;
				secondCounter++;
			}
			if (secondCounter == lifetime)
			{
				secondCounter = 0;
				fadingOut = true;
			}
			
			var diff:Point = destination.subtract(new Point(this.x, this.y));
			
			var dist:Number = diff.length;
			//Go to Point
			if (dist < 2)
			{
				//When reached find a new point
				pickADestination()
			}
			else // If we are not there yet. Keep moving.
			{ 
				diff.normalize(1);
				x += diff.x * particleSpeed;
				y += diff.y * particleSpeed;
				
				if(asymetricalShape)
				{
					var angleRadian:Number = Math.atan2(destination.y - y, destination.x - x);
					var angleDegree:Number = angleRadian * 180 / Math.PI;
					rotation = angleDegree;
				}
				
				if (fadingOut)
				{
					alpha -= fadingSpeed;
					
					if (alpha <= 0)
					{
						fadingOut = false;
						setup()
					}
				}
				
				if (fadingIn)
				{
					alpha += fadingSpeed;
					
					if (alpha >= particleAlpha)
					{
						fadingIn = false;
					}
				}
			}
		}
	}
}