package bg
{
	import UI.Main;
	
	import starling.display.Sprite;
	
	public class BackgroundManager extends Sprite
	{
		private var particleCount:int = 50;
		private var darkCircleCount:int = 2;
		private var lightCircleCount:int = 2;
		private var parameciumCount:int = 0;
		private var simpleCreatureCount:int = 20;
		
		public function BackgroundManager()
		{	
			super();
			
			var counter:int;
			
			if (!Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPAD_2 || PathogenTest.device == PathogenTest.PC || PathogenTest.device == PathogenTest.iPAD_HD)
			{
				//Create light Circles.
				for(counter = 0; counter <lightCircleCount; counter++)
				{
					var lightCircle:LightCircle = new LightCircle();
					addChild(lightCircle);
				}
				
				//Create Dark circles
				for(counter = 0; counter < darkCircleCount; counter++)
				{
					var darkCircle:DarkCircle = new DarkCircle();
					addChild(darkCircle);
				}
			}
			
			//Create Particles
			for(counter = 0; counter < particleCount; counter++)
			{
				var particle:Particle = new Particle();
				addChild(particle);
			}
			
			if (!Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPAD_2 || PathogenTest.device == PathogenTest.PC || PathogenTest.device == PathogenTest.iPAD_HD)
				addNewBackground();
			
			touchable = false;
		}
		public function addNewBackground():void
		{
			//Create a new background and shift its layering to the back
			var backgroundShift:BackgroundShift = new BackgroundShift(this);
			addChild(backgroundShift);
			setChildIndex(backgroundShift, 0);
		}
	}
}