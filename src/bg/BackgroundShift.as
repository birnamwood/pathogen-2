package bg
{
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class BackgroundShift extends Sprite 
	{
		//Static Vars
		private static const RECT_WIDTH:Number = PathogenTest.stageWidth;
		private static const RECT_HEIGHT:Number = PathogenTest.stageHeight;
		
		private static const FADE_IN:int = 0;
		private static const LIVING:int = 1;
		private static const FADE_OUT:int = 3;
		
		private static const FADE_SPEED:Number = .005;
		
		private var _display:Quad;
		
		//Counters for time.
		private var _frameCount:int = 0;
		private var _frameRate:int = 24;
		private var _secondCount:int = 0;
		private var _LifeThreshold:int;
		
		//Randomize the life expectancy of each background.
		private var _minimumLifeExpectancy:int = 10;
		private var _maximumLifeExpectancy:int = 15;
		
		private var _status:int;
		
		//parentClass
		private var _backgroundManager:BackgroundManager;
		
		//A supplied array of known Analogous Colors that work well together.
		private var _colorArray:Array = [0x000033, 0x0A0A26, 0x000021, 0x070032, 0x0D0926, 0x050021, 0x000932, 0x090E25, 0x000620];
		
		public function BackgroundShift(backgroundManager:BackgroundManager)
		{
			_backgroundManager = backgroundManager;
			
			//Calculate the potential range and assign _lifeThreshold
			var range:int = _maximumLifeExpectancy - _minimumLifeExpectancy;
			_LifeThreshold = _minimumLifeExpectancy + Math.round(Math.random() * range);
			
			createRect();
			
			//Set first status
			alpha = 0;
			_status = FADE_IN;
			addEventListener(Event.ENTER_FRAME, update);
		}
		private function createRect():void
		{
			var randomColor:int = Math.round(Math.random() * (_colorArray.length - 1));
			
			var choosenColor:int = _colorArray[randomColor]
			
			//Create the Background image with a pure color.
			_display = new Quad(RECT_WIDTH, RECT_HEIGHT, choosenColor);
			addChild(_display);
		}
		public function update($e:Event):void
		{
			//While in the living state, the background ages
			if (_status == LIVING)
			{
				_frameCount++;
				
				if (_frameCount == _frameRate)
				{
					_secondCount++;
					_frameCount = 0;
					
					//When background reachs life threshold it begins to FADE_OUT.
					if (_secondCount == _LifeThreshold)
					{
						_backgroundManager.addNewBackground();
						_status = FADE_OUT;
					}
				}
			}
			
			//Fade the background in
			if (_status == FADE_IN)
			{
				alpha += FADE_SPEED;
				
				if (alpha >= 1)
				{
					_status = LIVING;
				}
			}
			
			//Fade the backgound out
			if (_status == FADE_OUT)
			{
				alpha -= FADE_SPEED;
				
				if (alpha <= 0)
				{
					//when completely gone, kill object.
					death();
				}
			}
		}
		private function death():void
		{
			//remove from stage & kill all event listeners.
			removeEventListener(Event.ENTER_FRAME, update);
			removeChild(_display);
			this.parent.removeChild(this);
		}
	}
	
}