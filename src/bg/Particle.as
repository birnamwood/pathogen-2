package bg
{
	import starling.display.MovieClip;
	import UI.Main;
	
	public class Particle extends BGElement
	{
		public function Particle()
		{
			super();
			
			display= new MovieClip(Main.assets.getTextures("Particle"));
			addChild(display);
		}
	}
}