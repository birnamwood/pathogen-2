package bg
{
	import starling.display.MovieClip;
	import UI.Main;
	
	public class DarkCircle extends BGElement
	{
		public function DarkCircle()
		{
			super();
			
			minimumLengthOfLife = 5;
			maximumLengthofLife = 30;
			
			//if(PathogenTest.device == PathogenTest.iPAD_HD)
			if (PathogenTest.HD_Multiplyer > 1.0)
				display = new MovieClip(Main.assets.getTextures("HD_DarkCircle"));
			else
				display = new MovieClip(Main.assets.getTextures("DarkCircle"));
			
			addChild(display);
			
			display.scaleX = 3;
			display.scaleY = 3;
		}
	}
}