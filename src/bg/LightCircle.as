package bg
{
	import starling.display.MovieClip;
	import UI.Main;
	
	public class LightCircle extends BGElement
	{
		public function LightCircle()
		{
			super();
			
			minimumLengthOfLife = 5;
			maximumLengthofLife = 30;
			
			//if(PathogenTest.device == PathogenTest.iPAD_HD)
			if (PathogenTest.HD_Multiplyer > 1.0)
				display = new MovieClip(Main.assets.getTextures("HD_LightCircle"));
			else
				display = new MovieClip(Main.assets.getTextures("LightCircle"));
			
			addChild(display);
			
			display.scaleX = 3;
			display.scaleY = 3;
		}
	}
}