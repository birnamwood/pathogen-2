package Tutorial
{
	import UI.Main;
	
	import Utils.BW_Selector;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class TutorialCellSelector extends Sprite
	{
		//Parent Class
		private var _tutorialManager:TutorialManager;
		
		//Display Objects
		private var _whiteFlash:BW_Selector;
		private var _display:Image;
		
		//Vars
		private var _type:int
		
		public function TutorialCellSelector(type:int, tutorialManager:TutorialManager)
		{
			super();
			
			_type = type;
			_tutorialManager = tutorialManager;
			
			switch(_type)
			{
				case 0:
					_display = new Image(Main.assets.getTexture("Green_Stage01"));
					break;
				case 1:
					_display = new Image(Main.assets.getTexture("Green_Stage02"));
					break;
				case 2:
					_display = new Image(Main.assets.getTexture("Green_Stage03"));
					break;
			}
			
			addChild(_display);
			_display.pivotX = _display.width/2;
			_display.pivotY = _display.height/2;
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		private function onTouch($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if(touch)
			{
				_tutorialManager.selectCell(_type);
				removeWhiteFlash();
			}
		}
		public function fadeIN():void
		{
			_display.alpha = 0;
			_display.scaleX = 0;
			_display.scaleY = 0;
			
			//Animate it in
			var tween:Tween = new Tween(_display, .5);
			tween.scaleTo(1);
			tween.fadeTo(1);
			Starling.juggler.add(tween);
			tween.onComplete = fadeINComplete;
		}
		public function fadeOUT():void
		{
			
		}
		private function fadeINComplete():void
		{
			createWhiteFlash();
		}
		private function createWhiteFlash():void
		{
			var whiteCircle:Image;
			whiteCircle = new Image(Main.assets.getTexture("White_Circle"));
			_whiteFlash = new BW_Selector(0,0,whiteCircle, false, .8, 1);
			addChild(_whiteFlash);
		}
		private function removeWhiteFlash():void
		{
			if(_whiteFlash)
			{
				_whiteFlash.destroy();
				_whiteFlash.dispose();
				removeChild(_whiteFlash);
				_whiteFlash = null;
			}
		}
	}
}