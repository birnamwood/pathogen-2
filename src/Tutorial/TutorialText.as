package Tutorial
{
	import UI.Main;
	
	import Utils.BW_UI;
	
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class TutorialText extends BW_UI
	{
		public function TutorialText(text:String, fontSize:Number = 32)
		{
			super(true,Main.FADE_SPEED * 2);
			
			var instructions:TextField = new TextField(PathogenTest.stageWidth, PathogenTest.hTenPercent * 2, text, "Dekar", fontSize/PathogenTest.scaleFactor, Color.WHITE);
			addChild(instructions);
			instructions.pivotX = instructions.width/2;
			instructions.pivotY = instructions.height/2;
		}
	}
}