package Tutorial
{
	import Core.LevelTitle;
	import Core.TutorialCell;
	
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_Selector;
	import Utils.BW_UI;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class TutorialManager extends BW_UI
	{
		//parent class
		private var _main:Main;
		
		//Consts
		private const BOARD_WIDTH:int = 9;
		private const BOARD_HEIGHT:int = 9;
		
		//Steps
		private const STEP_01:String = "step_01";
		private const STEP_02:String = "step_02";
		private const STEP_03:String = "step_03";
		private const STEP_04:String = "step_04";
		private const STEP_05:String = "step_05";
		private const STEP_06:String = "step_06";
		private const STEP_07:String = "step_07";
		private const STEP_08:String = "step_08";
		private const STEP_10:String = "step_10";
		private const STEP_11:String = "step_11";
		private const STEP_12:String = "step_12";
		private const STEP_13:String = "step_13";
		
		//Game Vars
		private var _gameBoard:Array = [];
		private var _cellHolder:Sprite;
		private var _cellSize:int;
		private var _moveUnderway:Boolean;
		
		//Player/move Inforamtion
		private var _playerTeam:int;
		private var _cellType:int;
		
		//UI elements
		private var _title:LevelTitle;
		private var _tileSelector:BW_Selector;
		private var _whiteFlash:BW_Selector;
		
		//Buttons
		private var _nextButton:BW_Button;
		private var _resetButton:BW_Button;
		
		//Cell Selector
		private var _Cell_Level_01:TutorialCellSelector;
		private var _Cell_Level_02:TutorialCellSelector;
		private var _Cell_Level_03:TutorialCellSelector;
		
		//Instructions
		private var _step01_Instructions:TutorialText;
		private var _step02_Instructions:TutorialText;
		private var _step03_Instructions:TutorialText;
		
		private var _currentStep:String;
		
		public function TutorialManager(main:Main)
		{
			_main = main;
			
			super();
			
			var background:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			addChild(background);
			background.alpha = .5;
			
			createBoard();
			
			_currentStep = STEP_01;
			addTitle("Spread");
		}
		private function createBoard():void
		{
			_cellHolder = new Sprite();
			addChild(_cellHolder);
			
			_cellSize = 0;
			var cellCount:int = 0;
			//Create the board
			for (var i:int = 0; i<BOARD_WIDTH; i++)
			{
				for (var j:int = 0; j<BOARD_HEIGHT; j++)
				{
					var newCell:TutorialCell = new TutorialCell(cellCount, null, this);
					
					newCell.scaleX = 1.0;
					newCell.scaleY = 1.0;
					newCell.ownerTeam = -1;
					
					if (_cellSize == 0)
						_cellSize = newCell.width;
					
					var tileGap:Number = PathogenTest.wTenPercent * 0.5 * newCell.scaleX;
					
					newCell.x = tileGap * i;
					newCell.y = tileGap * j;
					
					newCell.state = 0;
					
					if( i == 0 || i == BOARD_WIDTH -1 || j == 0 || j == BOARD_HEIGHT-1)
					{
						newCell.state = 10;
						newCell.visible = false;
					}
					
					newCell.reset();
					newCell.visible = false;
					
					_cellHolder.addChild(newCell);
					_gameBoard.push(newCell);
					
					cellCount++;
				}
			}
			
			//Assign neighboring cells to all cells.
			for each(var cell:TutorialCell in _gameBoard)
			{
				cell.adjacentTopCell = _gameBoard[cell.location - 1];
				cell.adjacentBottomCell = _gameBoard[cell.location + 1];
				cell.adjacentLeftCell = _gameBoard[cell.location - BOARD_HEIGHT];
				cell.adjacentRightCell = _gameBoard[cell.location + BOARD_HEIGHT];
			}
			
			//Center the cells on the screen
			_cellHolder.pivotX = _cellHolder.width / 2;
			_cellHolder.pivotY = _cellHolder.height / 2;
			
			_cellHolder.x = PathogenTest.stageCenter.x + (newCell.height/2);
			_cellHolder.y = PathogenTest.stageCenter.y + (newCell.height/2);
			
			//Create Tile Selector icon
			var selectorDisplay:Image = new Image(Main.assets.getTexture("Selector"));
			
			_tileSelector = new BW_Selector(0,0, selectorDisplay);
			addChild(_tileSelector);
			_tileSelector.visible = false;
		}
		public function makeMove(cell:TutorialCell):void
		{
			//Allow the player to make a move
			_moveUnderway = true;
			cell.changeState(_cellType, _playerTeam);
			addEventListener(Event.ENTER_FRAME, update);
		}
		public function shiftStack(cell:TutorialCell):void
		{
			//Push the currently animating cell to the top of the stack
			_cellHolder.setChildIndex(cell, _cellHolder.numChildren -1);
		}
		private function update($e:Event):void
		{
			if(_moveUnderway)
			{
				//Assume all moves are done
				var complete:Boolean = true;
				
				for (var i:int = 0; i < _gameBoard.length; i++)
				{
					var cell:TutorialCell = _gameBoard[i];
					
					//If a cell is still animating, all moves are not done
					if(cell.flipping)
						complete = false;
				}
				
				if(complete)
				{
					removeEventListener(Event.ENTER_FRAME, update);
					_moveUnderway = false;
					resetTiles();
					
					//Determine next step
					switch(_currentStep)
					{
						case STEP_01:
							stepTwo();
							break;
							
						case STEP_02:
							stepThree();
							break
						
						case STEP_03:
							stepFour();
							break;
							
						case STEP_05:
							stepSix();
							break;
						
						case STEP_08:
							stepNine();
							break;
						
						case STEP_10:
							stepEleven();
							break;
						
						case STEP_12:
							stepThirteen();
							break;
					}
				}
			}
		}
		private function resetTiles():void
		{
			//All tiles are told its a new turn, so they may once again start their cascade
			for each (var cell:TutorialCell in _gameBoard)
			{
				if(cell.hasAlreadySpilledOver)
					cell.hasAlreadySpilledOver = false;
				
				cell.reactionNumber = 0;
			}
		}
		public function selectCell(type:int):void
		{
			_tileSelector.visible = true;
			
			switch(type)
			{
				case 0:
					_tileSelector.x = _Cell_Level_01.x;
					_tileSelector.y = _Cell_Level_01.y;
					_cellType = 0;
					break;
				
				case 1:
					_tileSelector.x = _Cell_Level_02.x;
					_tileSelector.y = _Cell_Level_02.y;
					_cellType = 1;
					break;
				
				case 0:
					_tileSelector.x = _Cell_Level_03.x;
					_tileSelector.y = _Cell_Level_03.y;
					_cellType = 2;
					break;
			}
			
			//Indicate the place to place a cell
			switch(_currentStep)
			{
				case STEP_02:
					_gameBoard[40].createWhiteFlash();
					break;
				case STEP_03:
					_gameBoard[40].createWhiteFlash();
					break;
			}
		}
		private function addTitle(text:String):void
		{
			_title = new LevelTitle(null, text, this);
			addChild(_title);
		}
		public function removeTitle():void
		{
			_title.dispose();
			removeChild(_title);
			_title = null;
			
			switch(_currentStep)
			{
				case STEP_01:
					stepOne();
					break;
				
				case STEP_04:
					stepFive();
					break
				
				case STEP_07:
					stepEight();
					break;
				
				case STEP_11:
					stepTwelve();
					break;
			}
			
		}
		private function stepOne():void
		{
			//Activate the center cell.
			var centerCell:TutorialCell = _gameBoard[40];
			centerCell.fadeIN(1);
			centerCell.adjacentBottomCell.fadeIN(.5);
			centerCell.adjacentLeftCell.fadeIN(.5);
			centerCell.adjacentRightCell.fadeIN(.5);
			centerCell.adjacentTopCell.fadeIN(.5);
			centerCell.addTouchEvent();
			centerCell.createWhiteFlash();
			
			//Create instructions
			_step01_Instructions = new TutorialText("Tap");
			addChild(_step01_Instructions);
			_step01_Instructions.x = PathogenTest.stageCenter.x;
			_step01_Instructions.y = PathogenTest.hTenPercent * 7;
		}
		private function stepTwo():void
		{
			//Increment Step
			_currentStep = STEP_02;
			//Remove old instructions
			_step01_Instructions.startFadeOut();
			
			//Create the Cell Selector 1
			_Cell_Level_01 = new TutorialCellSelector(0, this);
			addChild(_Cell_Level_01);
			_Cell_Level_01.x = PathogenTest.wTenPercent * 3;
			_Cell_Level_01.y = PathogenTest.stageCenter.y - (_Cell_Level_01.height/2);
			_Cell_Level_01.fadeIN();
			
			//Create new instructions
			_step02_Instructions = new TutorialText("Match to Evolve");
			addChild(_step02_Instructions);
			_step02_Instructions.x = PathogenTest.stageCenter.x;
			_step02_Instructions.y = PathogenTest.hTenPercent * 7;
			
			//Create new instructions
			_step03_Instructions = new TutorialText("Tap to select", 24);
			addChild(_step03_Instructions);
			_step03_Instructions.x = PathogenTest.stageCenter.x;
			_step03_Instructions.y = PathogenTest.hTenPercent * 7.5;
		}
		private function stepThree():void
		{
			//Increment Step
			_currentStep = STEP_03;
			
			//Create the Cell Selector 1
			_Cell_Level_02 = new TutorialCellSelector(1, this);
			addChild(_Cell_Level_02);
			_Cell_Level_02.x = _Cell_Level_01.x;
			_Cell_Level_02.y = _Cell_Level_01.y + (_Cell_Level_01.height * 1.2)
			_Cell_Level_02.fadeIN();
			
			//Fade in additional parts of the board
			_gameBoard[31].fadeOUT(1);
			_gameBoard[31].adjacentBottomCell.fadeIN(.5);
			_gameBoard[31].adjacentLeftCell.fadeIN(.5);
			_gameBoard[31].adjacentRightCell.fadeIN(.5);
			_gameBoard[31].adjacentTopCell.fadeIN(.5);
			
			_gameBoard[39].fadeOUT(1);
			_gameBoard[39].adjacentBottomCell.fadeIN(.5);
			_gameBoard[39].adjacentLeftCell.fadeIN(.5);
			_gameBoard[39].adjacentRightCell.fadeIN(.5);
			_gameBoard[39].adjacentTopCell.fadeIN(.5);
			
			_gameBoard[41].fadeOUT(1);
			_gameBoard[41].adjacentBottomCell.fadeIN(.5);
			_gameBoard[41].adjacentLeftCell.fadeIN(.5);
			_gameBoard[41].adjacentRightCell.fadeIN(.5);
			_gameBoard[41].adjacentTopCell.fadeIN(.5);
			
			_gameBoard[49].fadeOUT(1);
			_gameBoard[49].adjacentBottomCell.fadeIN(.5);
			_gameBoard[49].adjacentLeftCell.fadeIN(.5);
			_gameBoard[49].adjacentRightCell.fadeIN(.5);
			_gameBoard[49].adjacentTopCell.fadeIN(.5);
		}
		private function stepFour():void
		{
			//Increment Step
			_currentStep = STEP_04;
			//Remove old instructions
			_step02_Instructions.startFadeOut();
			_step03_Instructions.startFadeOut();
			
			for each (var cell:TutorialCell in _gameBoard)
			{
				if(cell.state != 10)
				{
					cell.state = 0;
					cell.reset();
					cell.addTouchEvent();
					cell.fadeIN(1);
				}	
			}
			
			addTitle("Try");
		}
		private function stepFive():void
		{
			//Increment Step
			_currentStep = STEP_05
		}
		private function stepSix():void
		{
			//Increment Step
			_currentStep = STEP_06;
			
			_nextButton = new BW_Button(1,.75, "Next", 32);
			addChild(_nextButton);
			_nextButton.x = PathogenTest.wTenPercent * 6;
			_nextButton.y = PathogenTest.hTenPercent * 8;
			_nextButton.addEventListener(TouchEvent.TOUCH, stepSeven);
			_nextButton.alpha = 0;
			
			var tween_01:Tween = new Tween(_nextButton, .5);
			tween_01.fadeTo(1);
			Starling.juggler.add(tween_01);
			
			_resetButton = new BW_Button(1,.75, "Reset", 32);
			addChild(_resetButton);
			_resetButton.x = PathogenTest.wTenPercent * 4;
			_resetButton.y = _nextButton.y
			_resetButton.addEventListener(TouchEvent.TOUCH, resetMap);
			_resetButton.alpha = 0;
			
			var tween_02:Tween = new Tween(_resetButton, .5);
			tween_02.fadeTo(1);
			Starling.juggler.add(tween_02);
		}
		private function stepSeven($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if(touch)
			{
				//Increment Step
				_currentStep = STEP_07;
				
				hideAllCells();
				
				//Remove buttons
				_nextButton.removeEventListener(TouchEvent.TOUCH, stepSeven);
				_nextButton.destroy();
				_nextButton.dispose();
				removeChild(_nextButton);
				_nextButton = null;
				
				_resetButton.removeEventListener(TouchEvent.TOUCH, resetMap);
				_resetButton.destroy();
				_resetButton.dispose();
				removeChild(_resetButton);
				_resetButton = null;
				
				addTitle("Conquer");
			}
		}
		private function stepEight():void
		{
			//Increment Step
			_currentStep = STEP_08;
			
			resetMap(null);
			
			//Activate the center cell.
			var centerCell:TutorialCell = _gameBoard[40];
			centerCell.state = 1;
			centerCell.ownerTeam = 1;
			centerCell.reset();
			
			centerCell.fadeIN(1);
			centerCell.adjacentBottomCell.fadeIN(.5);
			centerCell.adjacentLeftCell.fadeIN(.5);
			centerCell.adjacentRightCell.fadeIN(.5);
			centerCell.adjacentTopCell.fadeIN(.5);
			centerCell.addTouchEvent();
			centerCell.createWhiteFlash();
			
			//Create new instructions
			_step02_Instructions = new TutorialText("Override to Conquer");
			addChild(_step02_Instructions);
			_step02_Instructions.x = PathogenTest.stageCenter.x;
			_step02_Instructions.y = PathogenTest.hTenPercent * 7;
			
			//Create new instructions
			_step03_Instructions = new TutorialText("Tap to select", 24);
			addChild(_step03_Instructions);
			_step03_Instructions.x = PathogenTest.stageCenter.x;
			_step03_Instructions.y = PathogenTest.hTenPercent * 7.5;
		}
		private function stepNine():void
		{
			_step01_Instructions.startFadeOut();
			
			hideAllCells();
			Starling.juggler.delayCall(stepTen, 1);
		}
		private function stepTen():void
		{
			//Increment Step
			_currentStep = STEP_10;
			
			resetMap(null);
			
			//Activate the center cell.
			var centerCell:TutorialCell = _gameBoard[40];
			centerCell.state = 2;
			centerCell.ownerTeam = 1;
			centerCell.reset();
			
			centerCell.fadeIN(1);
			centerCell.addTouchEvent();
			centerCell.createWhiteFlash();
			
			//Fade in additional parts of the board
			_gameBoard[31].fadeIN(1);
			_gameBoard[31].state = 1;
			_gameBoard[31].ownerTeam = 1;
			_gameBoard[31].reset();
			
			_gameBoard[31].adjacentBottomCell.fadeIN(.5);
			_gameBoard[31].adjacentLeftCell.fadeIN(.5);
			_gameBoard[31].adjacentRightCell.fadeIN(.5);
			_gameBoard[31].adjacentTopCell.fadeIN(.5);
			
			_gameBoard[39].fadeIN(1);
			_gameBoard[39].state = 1;
			_gameBoard[39].ownerTeam = 1;
			_gameBoard[39].reset();
			
			_gameBoard[39].adjacentBottomCell.fadeIN(.5);
			_gameBoard[39].adjacentLeftCell.fadeIN(.5);
			_gameBoard[39].adjacentRightCell.fadeIN(.5);
			_gameBoard[39].adjacentTopCell.fadeIN(.5);
			
			_gameBoard[41].fadeIN(1);
			_gameBoard[41].state = 1;
			_gameBoard[41].ownerTeam = 1;
			_gameBoard[41].reset();
			
			_gameBoard[41].adjacentBottomCell.fadeIN(.5);
			_gameBoard[41].adjacentLeftCell.fadeIN(.5);
			_gameBoard[41].adjacentRightCell.fadeIN(.5);
			_gameBoard[41].adjacentTopCell.fadeIN(.5);
			
			_gameBoard[49].fadeIN(1);
			_gameBoard[49].state = 1;
			_gameBoard[49].ownerTeam = 1;
			_gameBoard[49].reset();
			
			_gameBoard[49].adjacentBottomCell.fadeIN(.5);
			_gameBoard[49].adjacentLeftCell.fadeIN(.5);
			_gameBoard[49].adjacentRightCell.fadeIN(.5);
			_gameBoard[49].adjacentTopCell.fadeIN(.5);
			
			//Create instructions
			_step02_Instructions = new TutorialText("Tap");
			addChild(_step02_Instructions);
			_step02_Instructions.x = PathogenTest.stageCenter.x;
			_step02_Instructions.y = PathogenTest.hTenPercent * 7;
			
			//Create the Cell Selector 2
			_Cell_Level_03 = new TutorialCellSelector(2, this);
			addChild(_Cell_Level_03);
			_Cell_Level_03.x = _Cell_Level_01.x;
			_Cell_Level_03.y = _Cell_Level_02.y + (_Cell_Level_02.height * 1.2);
			_Cell_Level_03.fadeIN();
		}
		private function stepEleven():void
		{
			//Increment Step
			_currentStep = STEP_11
			
			//Remove old instructions
			_step02_Instructions.startFadeOut();
			
			hideAllCells();
			addTitle("Try");
		}
		private function stepTwelve():void
		{
			//Increment Step
			_currentStep = STEP_12
			
			for each (var cell:TutorialCell in _gameBoard)
			{
				if(cell.state != 10)
				{
					cell.state = 0;
					cell.reset();
					cell.addTouchEvent();
				}	
			}
			
			revealAllCells();
		}
		private function stepThirteen():void
		{
			//Increment Step
			_currentStep = STEP_13;
			
			_nextButton = new BW_Button(1,.75, "Next", 32);
			addChild(_nextButton);
			_nextButton.x = PathogenTest.wTenPercent * 6;
			_nextButton.y = PathogenTest.hTenPercent * 8;
			//_nextButton.addEventListener(TouchEvent.TOUCH, stepSeven);
			_nextButton.alpha = 0;
			
			var tween_01:Tween = new Tween(_nextButton, .5);
			tween_01.fadeTo(1);
			Starling.juggler.add(tween_01);
			
			_resetButton = new BW_Button(1,.75, "Reset", 32);
			addChild(_resetButton);
			_resetButton.x = PathogenTest.wTenPercent * 4;
			_resetButton.y = _nextButton.y
			_resetButton.addEventListener(TouchEvent.TOUCH, resetMap);
			_resetButton.alpha = 0;
			
			var tween_02:Tween = new Tween(_resetButton, .5);
			tween_02.fadeTo(1);
			Starling.juggler.add(tween_02);
		}
		
		private function hideAllCells():void
		{
			for each (var cell:TutorialCell in _gameBoard)
			{
				cell.fadeOUT(0);
			}
		}
		private function revealAllCells():void
		{
			for each (var cell:TutorialCell in _gameBoard)
			{
				cell.fadeIN(1);
			}
		}
		private function resetMap($e:TouchEvent):void
		{
			if($e)
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				var open:Boolean = true;
			
			if(touch || open)
			{
				for each (var cell:TutorialCell in _gameBoard)
				{
					if(cell.state != 10)
					{
						cell.state = 0;
						cell.reset();
					}	
				}
			}
		}
		
		public function get cellType():int {return _cellType};
		public function get playerTeam():int {return _playerTeam};
		public function get moveUnderway():Boolean {return _moveUnderway};
	}
}