package Utils
{
	public class GeneralFunctions
	{
		public static function dateArraySortFunction(date1:Date, date2:Date):int
		{
			if (date1.valueOf() < date2.valueOf())
				return -1;
			else if (date1.valueOf() > date2.valueOf())
				return 1;
			else
				return 0;
		}
		
		public static function roundToPrecision(numberVal:Number, precision:int = 0):Number
		{
			var decimalPlaces:Number = Math.pow(10, precision);
			return Math.round(decimalPlaces * numberVal) / decimalPlaces;
		}
	}
}