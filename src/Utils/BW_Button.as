package Utils
{
	import UI.Main;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class BW_Button extends Sprite
	{
		//Toggle States
		public static const DOWN:int = 0;
		public static const UP:int = 1;
		
		//Colors
		private var TEXT_DOWN:int;
		public static var BACKGROUND_DOWN:int = 0x00001a;
		public static var BACKGROUND_UP:int = 0x222245;
		
		private var _backGround:Scale9Image;
		private var _textField:TextField;
		
		private var _downTexture:Texture;
		private var _upTexture:Texture;
		
		private var _imageUp:Image;
		private var _imageDown:Image;
		
		public var _toggle:Boolean;
		private var _state:int = UP;
		
		public function BW_Button(buttonWidth:Number, buttonHeight:Number, text:String, fontSize:int, textDown:int = 0x888888)
		{
			TEXT_DOWN = textDown;
			
			super();
			
			createBackground(buttonWidth, buttonHeight);
			addTextField(text, fontSize);
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		private function createBackground(buttonWidth:Number, buttonHeight:Number):void
		{
			//Create the button background
			var texture:Texture = Main.assets.getTexture("scale9_White");
			var textures:Scale9Textures = new Scale9Textures(texture, new Rectangle( PathogenTest.HD_Multiplyer * 20/PathogenTest.scaleFactor,  PathogenTest.HD_Multiplyer * 20/PathogenTest.scaleFactor,  PathogenTest.HD_Multiplyer * 20/PathogenTest.scaleFactor, PathogenTest.HD_Multiplyer * 20/PathogenTest.scaleFactor));
			_backGround = new Scale9Image(textures);
			_backGround.width = PathogenTest.wTenPercent * buttonWidth;
			_backGround.height = PathogenTest.hTenPercent * buttonHeight;
			_backGround.pivotX = _backGround.width/2;
			_backGround.pivotY = _backGround.height/2;
			addChild(_backGround);
			
			_backGround.color = BACKGROUND_UP;
			
			if(buttonWidth == 0)
				_backGround.alpha = 0;
		}
		private function addTextField(text:String, fontSize:int):void
		{
			//if(PathogenTest.device == PathogenTest.iPAD_HD)
				//fontSize = fontSize * 2;
			fontSize *= PathogenTest.HD_Multiplyer;
			
			//Add the text field
			_textField = new TextField(_backGround.width, _backGround.height, text, "Dekar", fontSize / PathogenTest.scaleFactor, Color.WHITE);
			_textField.pivotX = _textField.width/2;
			_textField.pivotY = _textField.height/2;
			addChild(_textField);	
		}
		public function addImage(upTexture:Texture, downTexture:Texture):void
		{
			//Create an Image on the button
			
			if(upTexture)
			{
				_imageUp = new Image(upTexture);
				_imageUp.pivotX = _imageUp.width / 2;
				_imageUp.pivotY = _imageUp.height / 2;
				addChild(_imageUp);
			}
			
			if(downTexture)
			{
				_imageDown = new Image(downTexture);
				_imageDown.visible = false;
				_imageDown.pivotX = _imageDown.width / 2;
				_imageDown.pivotY = _imageDown.height / 2;
				addChild(_imageDown);
			}
			_textField.visible = false;
		}
		private function onTouch($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this);
			if (touch)
			{
				//Toggle State active
				if(_toggle)
				{
					if(touch.phase == TouchPhase.ENDED)
					{
						if(_state == UP)
						{
							if (_imageUp)
							{
								_imageUp.visible = false;
								_imageDown.visible = true;
							}
							else
								_textField.color = TEXT_DOWN;

							
							_backGround.color = BACKGROUND_DOWN;
							_state = DOWN;
						}
						else
						{
							if (_imageUp)
							{
								_imageUp.visible = true;
								_imageDown.visible = false;
							}
							else
								_textField.color = Color.WHITE;
							
							_backGround.color = BACKGROUND_UP;
							_state = UP;
						}
					}
				}
				
				//Normal button state
				if(!_toggle)
				{
					if(touch.phase == TouchPhase.BEGAN)
					{
						if (_imageUp)
						{
							_imageUp.visible = false;
							_imageDown.visible = true;
							_backGround.color = BACKGROUND_DOWN;
						}
						else
						{
							_backGround.color = BACKGROUND_DOWN;
							_textField.color = TEXT_DOWN;
						}
					}
					
					if(touch.phase == TouchPhase.ENDED)
					{
						if (_imageUp)
						{
							_imageUp.visible = true;
							_imageDown.visible = false;
							_backGround.color = BACKGROUND_UP;
						}
						else
						{
							_backGround.color = BACKGROUND_UP;
							_textField.color = Color.WHITE;	
						}
					}
				}
			}
		}
		public function toggleUp():void
		{
			//manually set toggle up
			_backGround.color = BACKGROUND_UP;
			_textField.color = Color.WHITE;
			_state = UP;
			
			if(_imageUp)
			{
				_imageUp.visible = true;
				_imageDown.visible = false;
			}
		}
		public function toggleDown():void
		{
			//Manually set toggle down
			_backGround.color = BACKGROUND_DOWN;
			_textField.color = TEXT_DOWN;
			_state = DOWN;
			
			if(_imageUp)
			{
				_imageUp.visible = false;
				_imageDown.visible = true;
			}
		}
		public function changeText(newText:String):void
		{
			//Change the visible text
			_textField.text = newText;
		}
		public function destroy():void
		{
			removeEventListener(TouchEvent.TOUCH, onTouch);
			
			if (_backGround)
				_backGround.dispose();
			
			if (_textField)
				_textField.dispose();
			
			if (_imageUp)
			{
				_imageUp.texture.dispose();
				_imageUp.dispose();
			}
			
			if (_imageDown)
			{
				_imageDown.texture.dispose();
				_imageDown.dispose();
			}
			
			this.removeFromParent(true);
		}
		
		public function get toggle():Boolean { return _toggle};
		public function get state():int {return _state};
		
		public function set toggle(toggle:Boolean):void { _toggle = toggle};
		public function set backgroundDOWN_COLOR(backgroundDOWN_COLOR:int):void {BACKGROUND_DOWN = backgroundDOWN_COLOR};
		
	}
}