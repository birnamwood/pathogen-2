package Utils
{
	import UI.Main;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	
	public class BW_SocialButton extends Sprite
	{
		//Toggle States
		public static const DOWN:int = 1;
		public static const UP:int = 0;
		
		//Toggle States
		public static const ACTIVE:int = 0;
		public static const INACTIVE:int = 1;
		
		//Colors;
		public static const BACKGROUND_DOWN:int = 0x00001a;
		public static const BACKGROUND_UP:int = 0x222245;
		public static const TEXT_DOWN:int = 0x888888;
		
		private var _backGround:Scale9Image;
		private var _textField:TextField;
		private var _upText:String;
		private var _downText:String;
		
		private var _downTexture:Texture;
		private var _upTexture:Texture;
		
		private var _imageUp:Image;
		private var _imageDown:Image;
		
		public var _toggle:Boolean;
		private var _state:int = ACTIVE;
		
		public function BW_SocialButton(buttonWidth:Number, buttonHeight:Number, text:String, textDown:String, fontSize:int, upTexture:Texture, downTexture:Texture)
		{
			super();
			
			_upText = text;
			_downText = textDown;
			
			createBackground(buttonWidth, buttonHeight);
			addImage(upTexture, downTexture);
			addTextField(text, fontSize);
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		private function createBackground(buttonWidth:Number, buttonHeight:Number):void
		{
			//Create the button background
			var texture:Texture = Main.assets.getTexture("scale9_White");
			var textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20/PathogenTest.scaleFactor, 20/PathogenTest.scaleFactor, 20/PathogenTest.scaleFactor, 20/PathogenTest.scaleFactor));
			_backGround = new Scale9Image(textures);
			_backGround.width = PathogenTest.wTenPercent * buttonWidth;
			_backGround.height = PathogenTest.hTenPercent * buttonHeight;
			_backGround.pivotX = _backGround.width/2;
			_backGround.pivotY = _backGround.height/2;
			addChild(_backGround);
			
			_backGround.color = BACKGROUND_UP;
			
			if(buttonWidth == 0)
				_backGround.alpha = 0;
		}
		private function addImage(upTexture:Texture, downTexture:Texture):void
		{
			//Create an Image on the button
			_imageUp = new Image(upTexture);
			_imageUp.pivotX = _imageUp.width / 2;
			_imageUp.pivotY = _imageUp.height / 2;
			addChild(_imageUp);
			_imageUp.x = -_backGround.width * .25;
			
			_imageDown = new Image(downTexture);
			_imageDown.visible = false;
			_imageDown.pivotX = _imageUp.width / 2;
			_imageDown.pivotY = _imageUp.height / 2;
			addChild(_imageDown);
			_imageDown.x = _imageUp.x;
		}
		private function addTextField(text:String, fontSize:int):void
		{
			//Add the text field
			_textField = new TextField(_backGround.width, _backGround.height, text, "Dekar", PathogenTest.HD_Multiplyer * fontSize / PathogenTest.scaleFactor, Color.WHITE);
			_textField.pivotY = _textField.height/2;
			_textField.hAlign = HAlign.LEFT
			addChild(_textField);
			_textField.x = _imageUp.x + _imageUp.width * .6;
		}
		private function onTouch($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this);
			if (touch)
			{
				if(_toggle)
				{
					if(touch.phase == TouchPhase.ENDED)
					{
						if(_state == UP)
						{
							_imageUp.visible = false;
							_imageDown.visible = true;
							_state = DOWN;
						}
						else
						{
							_imageUp.visible = true;
							_imageDown.visible = false;
							_state = UP;
						}
					}
				}
				
				if(!_toggle)
				{
					if(touch.phase == TouchPhase.BEGAN)
					{
						_imageUp.visible = false;
						_imageDown.visible = true;
						_backGround.color = BACKGROUND_DOWN;
						_textField.color = TEXT_DOWN;
					}
					
					if(touch.phase == TouchPhase.ENDED)
					{
						_imageUp.visible = true;
						_imageDown.visible = false;
						_backGround.color = BACKGROUND_UP;
						_textField.color = Color.WHITE;
						_textField.text = _downText;
						_state = INACTIVE;
					}
				}
			}
		}
		public function resetButton():void
		{
			_textField.text = _upText;
			_state = ACTIVE;
		}
		public function destroy():void
		{
			removeEventListener(TouchEvent.TOUCH, onTouch);
			
			if (_backGround)
				_backGround.dispose();
			
			if (_textField)
				_textField.dispose();
			
			if (_imageUp)
			{
				_imageUp.texture.dispose();
				_imageUp.dispose();
			}
			
			if (_imageDown)
			{
				_imageDown.texture.dispose();
				_imageDown.dispose();
			}
			
			this.removeFromParent(true);
		}
		
		public function set toggle(toggle:Boolean):void { _toggle = toggle};
	}
}