package Utils
{
	import UI.Main;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class BW_Window extends BW_UI
	{
		protected var _background:Scale9Image;
		protected var _header:Scale9Image;
		
		//Scale Information
		private var scale9Num:Number = 20/PathogenTest.scaleFactor;
		private const texture:Texture = Main.assets.getTexture("scale9_White");
		private const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
		
		public function BW_Window()
		{
			super();
			
			createTransparent_background();
		}
		private function createTransparent_background():void
		{
			// create a transparent _background to obscure all other screen elements
			var trans_background:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			addChild(trans_background);
			trans_background.alpha = .75;
		}
		public function createbackground(backgroundWidth:Number, backgroundHeight:Number):void
		{
			//Creating _background so that content stands out.
			_background = new Scale9Image(textures);
			addChild(_background);
			_background.width = PathogenTest.wTenPercent * backgroundWidth;
			_background.height = PathogenTest.hTenPercent * backgroundHeight;
			_background.pivotX = _background.width/2;
			_background.pivotY = _background.height/2;
			_background.alpha = .5;
			_background.color = 0x222245;
			_background.x = PathogenTest.stageCenter.x;
			_background.y = PathogenTest.stageCenter.y;
		}
		public function createHeader(headerTitle:String):void
		{
			//Creating Header & Title
			_header = new Scale9Image(textures);
			addChild(_header);
			_header.width = _background.width;
			_header.height = PathogenTest.hTenPercent;
			_header.pivotX = _header.width/2;
			_header.x = _background.x;
			_header.y = _background.y -_background.height/2;
			_header.color = 0x222245;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
			var title:TextField = new TextField(_header.width, _header.height, headerTitle, "Questrial", fontSize, Color.WHITE);
			title.pivotX = title.width/2;
			addChild(title);
			title.x = _header.x;
			title.y = _header.y;
		}
	}
}