package Utils
{
	import Core.CountDownIndicator;
	
	import UI.Main;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.events.OutputProgressEvent;
	import flash.geom.Rectangle;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	public class BW_Selector extends Sprite
	{
		private var _fadetime:Number;
		private var _image:Image;
		private var _scale9:Scale9Image;
		private var _selectorFade:Tween;
		private var _outline:Boolean;
		private var _maxAlphaValue:Number;
		private var _currentCount:int = 0;
		private var _flashThreshold:int;
		
		public function BW_Selector(selectorWidth:Number, selectorHeight:Number, image:Image = null, outline:Boolean = true, maxAlphaValue:Number = 1.0, fadeTime:Number = 2.0, flashThreshold:int = 0)
		{
			super();
			
			_outline = outline;
			_maxAlphaValue = maxAlphaValue;
			_fadetime = fadeTime;
			_flashThreshold = flashThreshold;
			
			if(image)
			{
				_image = image;
				addChild(image);
				_image.pivotX = _image.width/2;
				_image.pivotY = _image.height/2;
				_image.alpha = 0;
			}
			else
				createScale9(selectorWidth, selectorHeight);
			
			if(fadeTime > 0)	
				fadeIn();
		}
		private function createScale9(selectorWidth:Number, selectorHeight:Number):void
		{
			var texture:Texture;
			
			if(_outline)
				texture = Main.assets.getTexture("scale9_outline");
			else
				texture = Main.assets.getTexture("scale9_White");
			
			
			var textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20 / PathogenTest.scaleFactor, 20/ PathogenTest.scaleFactor, 20/ PathogenTest.scaleFactor, 20/ PathogenTest.scaleFactor));
			
			_scale9 = new Scale9Image(textures);
			_scale9.width = PathogenTest.wTenPercent * selectorWidth;
			_scale9.height = PathogenTest.hTenPercent * selectorHeight;
			_scale9.pivotX = _scale9.width/2;
			_scale9.pivotY = _scale9.height/2;
			addChild(_scale9);
			_scale9.alpha = 0;
		}
		public function startFlash():void
		{
			_currentCount = 0;
			fadeIn();
		}
		public function stopFlash():void
		{
			Starling.juggler.remove(_selectorFade);
		}
		private function fadeIn():void
		{
			if(_flashThreshold == 0)
			{
				if(_image)
					_selectorFade = new Tween(_image, _fadetime);
				else
					_selectorFade = new Tween(_scale9, _fadetime);
				
				_selectorFade.fadeTo(_maxAlphaValue);
				_selectorFade.onComplete = fadeOut;
				Starling.juggler.add(_selectorFade);
			}
			else
			{
				if(_currentCount < _flashThreshold)
				{
					if(_image)
						_selectorFade = new Tween(_image, _fadetime);
					else
						_selectorFade = new Tween(_scale9, _fadetime);
					
					_selectorFade.fadeTo(_maxAlphaValue);
					_selectorFade.onComplete = fadeOut;
					Starling.juggler.add(_selectorFade);
				}
			}
			
		}
		private function fadeOut():void
		{
			_currentCount++;
			
			if(_image)
				_selectorFade = new Tween(_image, _fadetime);
			else
				_selectorFade = new Tween(_scale9, _fadetime);
			
			_selectorFade.fadeTo(0.0);
			_selectorFade.onComplete = fadeIn;
			Starling.juggler.add(_selectorFade);
		}
		public function destroy():void
		{
			Starling.juggler.remove(_selectorFade);
			_selectorFade = null;
			
			if(_image)
			{
				_image.dispose();
				removeChild(_image)
				_image = null;
			}
			else
			{
				_scale9.dispose();
				removeChild(_scale9);
				_scale9 = null;
			}
		}
	}
}