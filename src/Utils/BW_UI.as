package Utils
{
	import UI.Main;
	
	import feathers.system.DeviceCapabilities;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class BW_UI extends starling.display.Sprite
	{
		//Static consts
		protected static const FADE_IN:int = 0;
		protected static const ALIVE:int = 1;
		protected static const FADE_OUT:int = 2;
		
		protected var state:int;
		private var _fadeSpeed:Number
		
		public function BW_UI(fadeIn:Boolean = true, fadeSpeed:Number = 0)
		{
			super();
			
			if(fadeSpeed == 0)
				_fadeSpeed = Main.FADE_SPEED;
			else
				_fadeSpeed = fadeSpeed;
			
			if (!Main.isPhone() || fadeIn == false)
			//if(PathogenTest.device == PathogenTest.iPAD_HD || PathogenTest.device == PathogenTest.iPAD_2 || PathogenTest.device == PathogenTest.PC || fadeIn == false)
			{
				state = FADE_IN;
				alpha = 0;
				addEventListener(Event.ENTER_FRAME, update);
			}
		}
		public function startFadeOut():void
		{
			if (!Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPAD_HD || PathogenTest.device == PathogenTest.iPAD_2 || PathogenTest.device == PathogenTest.PC)
			{
				state = FADE_OUT;
				addEventListener(Event.ENTER_FRAME, update);
			}
			else
			{
				death();
			}
		}
		private function update($e:Event):void
		{
			if(state == FADE_IN)
			{
				alpha += _fadeSpeed;
				
				if(alpha >= 1)
				{
					removeEventListener(Event.ENTER_FRAME, update);
					state = ALIVE;
				}
			}
			
			if(state == FADE_OUT)
			{
				alpha -= _fadeSpeed;
				
				if(alpha <= 0)
				{
					removeEventListener(Event.ENTER_FRAME, update);
					death();
				}
			}
		}
		public function death():void
		{
			
		}
	}
}