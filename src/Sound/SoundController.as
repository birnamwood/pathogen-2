package Sound
{
	import UI.Main;
	
	import flash.net.SharedObject;
	
	import starling.extensions.SoundManager;
	
	public class SoundController
	{
		private static const MAX_MUSIC_VOLUME:Number = 0.2;
		private static const MAX_SOUND_VOLUME:Number = 0.6;
		
		public static var sSoundManager:SoundManager;
		public static var sCurrentSong:String;
		public static var sMusicVolume:Number;
		public static var sSoundVolume:Number;
		
		private static var currentReactionCount:int = 0;
		private static var currentSongNumber:int = 0;
		private static var state:int;
		
		private static var UP:int = 0;
		private static var DOWN:int = 1;
		
		private static var sharedDataObject:SharedObject;
		public static var selectedScale:String;
		
		private var _inGameSongIndex:int;
		private var _inGameSongs:Array;
		
		private var _mute:Boolean;
		
		public function SoundController()
		{
			sharedDataObject = Main.saveDataObject;
			
			sSoundManager = SoundManager.getInstance();
			
			//Music
			
			if(!Main.isAndroid())
				sSoundManager.addSound("pathogen_in_game_1", Main.assets.getSound("pathogen_in_game_1"));
			
			//sSoundManager.addSound("pathogen_in_game_1", Main.assets.getSound("pathogen_in_game_1"));
			
			sSoundManager.addSound("pathogen_menu", Main.assets.getSound("pathogen_menu"));
			sSoundManager.addSound("pathogen_in_game_2", Main.assets.getSound("pathogen_in_game_2"));
			
			_inGameSongs = [];
			
			if(!Main.isAndroid())
				_inGameSongs.push("pathogen_in_game_1");
			
			_inGameSongs.push("pathogen_in_game_2");
			_inGameSongIndex = -1;
			
			// load sounds into sound manager
			sCurrentSong = "pathogen_menu";
			sSoundManager.playSound(sCurrentSong, 1, 9999);  // play music looping
			if (sharedDataObject.data.MusicVolume != null)
				sMusicVolume = sharedDataObject.data.MusicVolume;
			else
				sMusicVolume = MAX_MUSIC_VOLUME;
			sSoundManager.setVolume(sCurrentSong, sMusicVolume);
			
			//SFX
			sSoundManager.addSound("C_1", Main.assets.getSound("C_1"));
			sSoundManager.addSound("C_2", Main.assets.getSound("C_2"));
			sSoundManager.addSound("C_3", Main.assets.getSound("C_3"));
			sSoundManager.addSound("C_4", Main.assets.getSound("C_4"));
			sSoundManager.addSound("C_5", Main.assets.getSound("C_5"));
			sSoundManager.addSound("C_6", Main.assets.getSound("C_6"));
			sSoundManager.addSound("C_7", Main.assets.getSound("C_7"));
			sSoundManager.addSound("C_8", Main.assets.getSound("C_8"));
			
			sSoundManager.addSound("D_1", Main.assets.getSound("D_1"));
			sSoundManager.addSound("D_2", Main.assets.getSound("D_2"));
			sSoundManager.addSound("D_3", Main.assets.getSound("D_3"));
			sSoundManager.addSound("D_4", Main.assets.getSound("D_4"));
			sSoundManager.addSound("D_5", Main.assets.getSound("D_5"));
			sSoundManager.addSound("D_6", Main.assets.getSound("D_6"));
			sSoundManager.addSound("D_7", Main.assets.getSound("D_7"));
			sSoundManager.addSound("D_8", Main.assets.getSound("D_8"));
			
			sSoundManager.addSound("E_1", Main.assets.getSound("E_1"));
			sSoundManager.addSound("E_2", Main.assets.getSound("E_2"));
			sSoundManager.addSound("E_3", Main.assets.getSound("E_3"));
			sSoundManager.addSound("E_4", Main.assets.getSound("E_4"));
			sSoundManager.addSound("E_5", Main.assets.getSound("E_5"));
			sSoundManager.addSound("E_6", Main.assets.getSound("E_6"));
			sSoundManager.addSound("E_7", Main.assets.getSound("E_7"));
			sSoundManager.addSound("E_8", Main.assets.getSound("E_8"));
			
			sSoundManager.addSound("Lose", Main.assets.getSound("Lose"));
			sSoundManager.addSound("Win", Main.assets.getSound("Win"));
			
			if (sharedDataObject.data.SoundVolume != null)
				sSoundVolume = sharedDataObject.data.SoundVolume
			else
				sSoundVolume = MAX_SOUND_VOLUME;
			
			state = UP;
		}
		public function transitionToInGameMusic():void
		{
			//Main.updateDebugText("Transition to in Game Music, sCurrent Song: " + sCurrentSong);
			
			//trace(sSoundManager.soundIsPlaying(sCurrentSong));
			
			if(Main.isIOS())
			{
				if(sSoundManager.soundIsPlaying(sCurrentSong))
				{
					//trace("Current Song: " + sCurrentSong + "inGame Song Index: " + _inGameSongIndex);
					
					if (_inGameSongIndex == -1)
						_inGameSongIndex = Math.round(Math.random() * (_inGameSongs.length - 1));
					else
					{
						_inGameSongIndex++;
						if ((_inGameSongIndex + 1) > _inGameSongs.length)
							_inGameSongIndex = 0;
					}
					sSoundManager.crossFade(sCurrentSong, _inGameSongs[_inGameSongIndex], 1, sMusicVolume, 9999);
					sCurrentSong = _inGameSongs[_inGameSongIndex];
				}
			}
			
			if(Main.isAndroid())
			{
				if(sSoundManager.soundIsPlaying(sCurrentSong))
				{
					if (_inGameSongIndex == -1)
						_inGameSongIndex = 0;
					
					sSoundManager.crossFade(sCurrentSong, "pathogen_in_game_2", 1, sMusicVolume, 9999);
					sCurrentSong = "pathogen_in_game_2";
				}
			}
		}
		public function transitionToMenuMusic():void
		{
			//Main.updateDebugText("transitionToMenuMusic - " + sSoundManager.soundIsPlaying(sCurrentSong));
			if(sSoundManager.soundIsPlaying(sCurrentSong))
			{
				//Main.updateDebugText("changing to menu song");
				sSoundManager.crossFade(sCurrentSong, "pathogen_menu", 1, sMusicVolume, 9999);
				sCurrentSong = "pathogen_menu";
			}
		}
		private static function saveSettings():void
		{
			sharedDataObject.data.MusicVolume = sMusicVolume;
			sharedDataObject.data.SoundVolume = sSoundVolume;
			sharedDataObject.flush();
		}
		public static function playCellSelector1():void
		{
			sSoundManager.playSound("C_1", sSoundVolume);
		}
		public static function playCellSelector2():void
		{
			sSoundManager.playSound("C_1", sSoundVolume);
		}
		public static function playCellSelector3():void
		{
			sSoundManager.playSound("D_1", sSoundVolume);
		}
		public static function playCellSelector4():void
		{
			sSoundManager.playSound("E_1", sSoundVolume);
		}
		public function playWinSound():void
		{
			sSoundManager.playSound("Win", sSoundVolume);
		}
		public function playLossSound():void
		{
			sSoundManager.playSound("Lose", sSoundVolume);
		}
		public static function selectScaleSound(cellState:int):void
		{
			switch(cellState)
			{
				case 0:
					selectedScale = "E_";
					break;
				case 1:
					selectedScale = "C_";
					break;
				case 2:
					selectedScale = "C_";
					break;
				case 3:
					selectedScale = "D_";
					break;
				case 4:
					selectedScale = "E_";
					break;
			}
		}
		
		public static function playSpreadSound(reactionNumber:int):void
		{
			if(currentReactionCount < reactionNumber)
			{
				if(state == UP)
				{
					if(currentSongNumber < 8)
						currentSongNumber++;
					else
						state = DOWN;
				}
				
				if(state == DOWN)
				{
					if(currentSongNumber > 1)
						currentSongNumber--;
					else
					{
						state = UP;
						currentSongNumber = 2;
					}
						
				}
				
				var currentSong:String = selectedScale + currentSongNumber.toString();
				
				sSoundManager.playSound(currentSong, sSoundVolume);
				currentReactionCount++;
			}
		}
		public static function randomNote():void
		{
			currentSongNumber = 1 + Math.round(Math.random() * 7);
		}
		public static function resetSoundCounters():void
		{
			currentReactionCount = 0;
			currentSongNumber = 0;
			state = UP;
		}
		public static function pauseAudio():void
		{
			if(sSoundManager)
				sSoundManager.stopAllSounds();
		}
		public static function unPauseAudio():void
		{
			if(sSoundManager)
				sSoundManager.playSound(sCurrentSong, sSoundVolume, 9999);
		}
		public static function setMusicVolume(value:Number):void
		{
			sMusicVolume = (value / 100) * MAX_MUSIC_VOLUME;
			sSoundManager.setVolume(sCurrentSong, sMusicVolume);
			saveSettings();
		}
		
		public static function setSoundVolume(value:Number):void
		{
			sSoundVolume = (value / 100) * MAX_SOUND_VOLUME;
			saveSettings();
		}
		
		public static function getMusicVolume():int
		{
			//trace("returning " + (sMusicVolume / MAX_MUSIC_VOLUME) * 100);
			return (sMusicVolume / MAX_MUSIC_VOLUME) * 100;
		}
		
		public static function getSoundVolume():int
		{
			return (sSoundVolume / MAX_SOUND_VOLUME) * 100;
		}
	}
}