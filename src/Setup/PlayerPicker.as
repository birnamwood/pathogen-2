package Setup
{
	import Setup.GameSetup;
	
	import UI.Main;
	
	import Utils.BW_Button;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class PlayerPicker extends Sprite
	{
		
		private var _playerHolder:Sprite;
		
		//Color Selection
		private var green:Image;
		private var red:Image;
		private var blue:Image;
		private var yellow:Image;
		
		private var deactivated:Number = .4;
		private var removeBT:BW_Button;
		private var addAIBt:BW_Button;
		private var _playerButton:BW_Button;
		
		public var chosenColor:int = 0;
		public var whatPlayer:int;
		private var playerName:TextField;
		
		public var ai:Boolean = false;
		public var active:Boolean = false;
		
		//Parent Class
		private var gameSetup:GameSetup;
		
		public function PlayerPicker(whatPlayer:int, gameSetup:GameSetup)
		{
			this.whatPlayer = whatPlayer;
			this.gameSetup = gameSetup;
			
			super();
			
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20/PathogenTest.scaleFactor, 20/PathogenTest.scaleFactor, 20/PathogenTest.scaleFactor, 20/PathogenTest.scaleFactor));
			
			var background:Scale9Image = new Scale9Image(textures);
			background.width = PathogenTest.wTenPercent * 3;
			background.height = PathogenTest.hTenPercent * 3;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.color = 0x00001a;
			background.alpha = .5;
			addChild(background);
			
			_playerHolder = new Sprite();
			addChild(_playerHolder);
			
			green = new Image(Main.assets.getTexture("Green_Stage01"));
			red = new Image(Main.assets.getTexture("Red_Stage01"));
			blue = new Image(Main.assets.getTexture("Blue_Stage01"));
			yellow = new Image(Main.assets.getTexture("Yellow_Stage01"));
			
			green.pivotX = green.width/2;
			green.pivotY = green.height/2;
			red.pivotX = red.width/2;
			red.pivotY = red.height/2;
			blue.pivotX = blue.width/2;
			blue.pivotY = blue.height/2;
			yellow.pivotX = yellow.width/2;
			yellow.pivotY = yellow.height/2;
			
			green.scaleX = 1.1;
			green.scaleY = 1.1;
			red.scaleX = 1.1;
			red.scaleY = 1.1;
			blue.scaleX = 1.1;
			blue.scaleY = 1.1;
			yellow.scaleX = 1.1;
			yellow.scaleY = 1.1;
			
			green.x = -green.width * 1.8;
			red.x = -red.width * .6
			blue.x = blue.width * .6;
			yellow.x = yellow.width * 1.8;
			
			green.y = -green.height/2;
			red.y = green.y;
			blue.y = green.y;
			yellow.y = green.y;
			
			resetAlpha();
			
			_playerHolder.addChild(green);
			_playerHolder.addChild(red);
			_playerHolder.addChild(blue);
			_playerHolder.addChild(yellow);
			
			green.addEventListener(TouchEvent.TOUCH, selectedGreen);
			red.addEventListener(TouchEvent.TOUCH, selectedRed);
			blue.addEventListener(TouchEvent.TOUCH, selectedBlue);
			yellow.addEventListener(TouchEvent.TOUCH, selectedYellow);
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 25 / PathogenTest.scaleFactor;
			playerName = new TextField(PathogenTest.wTenPercent  * 1.2, PathogenTest.hTenPercent/1.5, "Player " + (whatPlayer + 1), "Dekar", fontSize, Color.WHITE);
			_playerHolder.addChild(playerName);
			
			playerName.pivotX = playerName.width/2;
			playerName.pivotY = playerName.height/2;
			
			playerName.y = green.y - playerName.height;
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				addAIBt = new BW_Button(1, 1, "Add AI", 18);
				_playerHolder.addChild(addAIBt);
				addAIBt.y =  blue.y + blue.height/2 + (addAIBt.height * .8);
				addAIBt.x = -addAIBt.width * .6
				addAIBt.addEventListener(TouchEvent.TOUCH, selectedAI);
				
				removeBT = new BW_Button(1, 1, "Remove Player", 18);
				_playerHolder.addChild(removeBT);
				removeBT.y = addAIBt.y;
				removeBT.x = removeBT.width * .6
				removeBT.addEventListener(TouchEvent.TOUCH, removedPlayer);
			}
			else
			{
				addAIBt = new BW_Button(1.5, .4, "Add AI", 18);
				_playerHolder.addChild(addAIBt);
				addAIBt.y =  blue.y + blue.height/2 + addAIBt.height;
				addAIBt.addEventListener(TouchEvent.TOUCH, selectedAI);
				
				removeBT = new BW_Button(1.5, .4, "Remove Player", 18);
				_playerHolder.addChild(removeBT);
				removeBT.y = addAIBt.y + removeBT.height * 1.2;
				removeBT.addEventListener(TouchEvent.TOUCH, removedPlayer);	
			}
			
			
			
			_playerHolder.visible = false;
			
			_playerButton = new BW_Button(2.8, 2.8, "Add Player " + (whatPlayer + 1) , 28);
			addChild(_playerButton);
			_playerButton.addEventListener(TouchEvent.TOUCH, onTouchRemovePlayerButton);
		}
		private function onTouchRemovePlayerButton($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_playerButton.removeEventListener(TouchEvent.TOUCH, onTouchRemovePlayerButton);
				removeChild(_playerButton);
				_playerHolder.visible = true;
				getNextAvailableColor();
			}
		}
		private function selectedGreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				resetAlpha();
				green.alpha = 1;
				chosenColor = 0;
				removeBT.visible = true;
				active = true;
				gameSetup.updatePlayerIndicators();
			}
		}
		private function selectedRed($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				resetAlpha();
				red.alpha = 1;
				chosenColor = 1;
				removeBT.visible = true;
				active = true;
				gameSetup.updatePlayerIndicators();
			}
		}
		private function selectedBlue($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				resetAlpha();
				blue.alpha = 1;
				chosenColor = 2;
				removeBT.visible = true;
				active = true;
				gameSetup.updatePlayerIndicators();
			}
		}
		private function selectedYellow($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				resetAlpha();
				yellow.alpha = 1;
				chosenColor = 3;
				removeBT.visible = true;
				active = true;
				gameSetup.updatePlayerIndicators();
			}
		}
		private function selectedAI($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if (!ai)
				{
					ai = true;
					gameSetup.addOrRemoveDifficultySelector()
					
					addAIBt.removeEventListener(TouchEvent.TOUCH, selectedAI);
					playerName.text = "AI " + (whatPlayer + 1); 
					active = true;
					addAIBt.changeText("Remove AI");
					addAIBt.addEventListener(TouchEvent.TOUCH, removeAI);
										
					getNextAvailableColor();
				}
				else
					ai = false;
			}
		}
		private function getNextAvailableColor():void
		{
			if (green.alpha == deactivated && red.alpha == deactivated && blue.alpha == deactivated && yellow.alpha == deactivated)
			{
				var nextColor:int = gameSetup.getNextUnusedColor(whatPlayer);
				if (nextColor == -1 || nextColor == 0)
				{
					resetAlpha();
					green.alpha = 1;
					chosenColor = 0;
				}
				else if (nextColor == 1)
				{
					resetAlpha();
					red.alpha = 1;
					chosenColor = 1;
				}
				else if (nextColor == 2)
				{
					resetAlpha();
					blue.alpha = 1;
					chosenColor = 2;
				}
				else if (nextColor == 3)
				{
					resetAlpha();
					yellow.alpha = 1;
					chosenColor = 3;
				}
			}
			
			removeBT.visible = true;
			active = true;
			gameSetup.updatePlayerIndicators();
		}
		private function removeAI($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				
				ai = false;
				gameSetup.addOrRemoveDifficultySelector()
				
				addAIBt.removeEventListener(TouchEvent.TOUCH, removeAI);
				playerName.text = "Player " + (whatPlayer + 1)
				
				addAIBt.changeText("Add AI");
				addAIBt.addEventListener(TouchEvent.TOUCH, selectedAI);
			}
		}
		private function removedPlayer($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				resetAlpha()
				active = false;
				if (ai)
				{
					//ai = false;
					removeAI($e);
				}
				
				playerName.text = "Player " + (whatPlayer + 1);
				
				_playerHolder.visible = false;
				
				_playerButton = new BW_Button(2.8, 2.8, "Add Player " + (whatPlayer + 1), 28);
				addChild(_playerButton);
				_playerButton.addEventListener(TouchEvent.TOUCH, onTouchRemovePlayerButton);
				
				gameSetup.updatePlayerIndicators();
			}
		}
		private function resetAlpha():void
		{
			green.alpha = deactivated;
			red.alpha = deactivated;
			blue.alpha = deactivated;
			yellow.alpha = deactivated;
		}
		public function garbageCollection():void
		{
			_playerButton.destroy();
			_playerButton = null;
			
			addAIBt.destroy();
			addAIBt = null;
			
			removeBT.destroy();
			removeBT = null;
			
			gameSetup = null;
		}
	}
}