package Setup
{
	import UI.Main;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class PlayerIndicator extends Sprite
	{
		private var _display:Image;
		
		public function PlayerIndicator()
		{
			super();
			
			_display = new Image(Main.assets.getTexture("PlayerCounter"));
			addDisplay();
		}
		private function addDisplay():void
		{
			_display.pivotX = _display.width/2;
			_display.pivotY = _display.height/2;
			addChild(_display)
			_display.scaleX = .7;
			_display.scaleY = .7;
		}
		public function changeColor(chosenColor:int):void
		{
			removeChild(_display);
			
			if(chosenColor == -1)
				_display = new Image(Main.assets.getTexture("PlayerCounter"));
			if(chosenColor == 0)
				_display = new Image(Main.assets.getTexture("Green_Stage01"));
			if(chosenColor == 1)
				_display = new Image(Main.assets.getTexture("Red_Stage01"));
			if(chosenColor == 2)
				_display = new Image(Main.assets.getTexture("Blue_Stage01"));
			if(chosenColor == 3)
				_display = new Image(Main.assets.getTexture("Yellow_Stage01"));
			
			addDisplay();
		}
	}
}