package Setup
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import feathers.controls.Slider;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class ModePicker extends BW_UI
	{
		//Classes
		private var gameSetup:GameSetup;
		
		//Textfields
		private var _gameLengthTitle:TextField;
		private var _gameLengthTextfield:TextField;
		private var _turnLengthTitle:TextField;
		private var _turnLengthTextfield:TextField;
		
		//Sliders
		private var _gameLengthSlider:Slider;
		private var _turnLengthSlider:Slider;
		
		//Save
		private var sharedDataObject:SharedObject;
		
		//Buttons
		private var _backButton:BW_Button;
		private var _infectionCheckbox:BW_Button;
		
		public function ModePicker(gameSetup:GameSetup)
		{
			super();
			
			this.gameSetup = gameSetup;
			sharedDataObject = Main.saveDataObject;
			
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			addChild(transBackground);
			transBackground.alpha = 0.75;
			
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20, 20, 20, 20));
			
			//Creating background so that content stands out.
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 5;
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				background.height = PathogenTest.hTenPercent * 6.75;
			else
				background.height = PathogenTest.hTenPercent * 6.0;
			
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			background.color = 0x222245;
			
			//Game Length
			_gameLengthTitle = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent/2, "Game Length", "Questrial",PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			_gameLengthTitle.pivotX = _gameLengthTitle.width/2;
			addChild(_gameLengthTitle);
			_gameLengthTitle.y = background.y - (background.height * .4);
			
			_gameLengthTextfield = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent/2, "Unlimited Rounds", "Dekar",PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
			_gameLengthTextfield.pivotX = _gameLengthTextfield.width/2;
			addChild(_gameLengthTextfield);
			_gameLengthTextfield.y = _gameLengthTitle.y + _gameLengthTitle.height;
				
			_gameLengthSlider = new Slider();
			_gameLengthSlider.minimum = 10;
			_gameLengthSlider.maximum = 100;
			_gameLengthSlider.value = _gameLengthSlider.maximum;
			_gameLengthSlider.step = 10
			_gameLengthSlider.direction = Slider.DIRECTION_HORIZONTAL;
			_gameLengthSlider.liveDragging = true;
			_gameLengthSlider.thumbProperties.defaultSkin = new Image(Main.assets.getTexture("SliderKnob"));
			_gameLengthSlider.thumbProperties.width = PathogenTest.HD_Multiplyer * 30;
			_gameLengthSlider.minimumTrackProperties.defaultSkin = new Quad(PathogenTest.wTenPercent * 3,PathogenTest.hTenPercent/3, 0x222245);
			_gameLengthSlider.pivotX = _gameLengthSlider.minimumTrackProperties.defaultSkin.width/2;
			_gameLengthSlider.pivotY = _gameLengthSlider.minimumTrackProperties.defaultSkin.height/2;
			addChild(_gameLengthSlider);
			_gameLengthSlider.y = _gameLengthTextfield.y + (PathogenTest.hTenPercent * .8);
			_gameLengthSlider.addEventListener(Event.CHANGE, changeGameLengthSlider);
			
			//Turn Length
			_turnLengthTitle = new TextField(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent/2, "Turn Length", "Questrial",PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			_turnLengthTitle.pivotX = _turnLengthTitle.width/2;
			addChild(_turnLengthTitle);
			_turnLengthTitle.y = _gameLengthSlider.y + (PathogenTest.hTenPercent * .8);
			
			_turnLengthTextfield = new TextField(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent/2, "Unlimited", "Dekar",PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
			_turnLengthTextfield.pivotX = _turnLengthTextfield.width/2;
			addChild(_turnLengthTextfield);
			_turnLengthTextfield.y = _turnLengthTitle.y + _turnLengthTitle.height;
			
			_turnLengthSlider = new Slider();
			_turnLengthSlider.minimum = 10;
			_turnLengthSlider.maximum = 30;
			_turnLengthSlider.value = _turnLengthSlider.maximum;
			_turnLengthSlider.step = 5;
			_turnLengthSlider.direction = Slider.DIRECTION_HORIZONTAL;
			_turnLengthSlider.liveDragging = true;
			_turnLengthSlider.thumbProperties.defaultSkin = new Image(Main.assets.getTexture("SliderKnob"));
			_turnLengthSlider.thumbProperties.width = PathogenTest.HD_Multiplyer * 30;
			_turnLengthSlider.minimumTrackProperties.defaultSkin = new Quad(PathogenTest.wTenPercent * 3,PathogenTest.hTenPercent/3, 0x222245);
			_turnLengthSlider.pivotX = _turnLengthSlider.minimumTrackProperties.defaultSkin.width/2;
			_turnLengthSlider.pivotY = _turnLengthSlider.minimumTrackProperties.defaultSkin.height/2;
			addChild(_turnLengthSlider);
			_turnLengthSlider.y = _turnLengthTextfield.y + (PathogenTest.hTenPercent * .8)
			_turnLengthSlider.addEventListener(Event.CHANGE, changeTurnLengthSlider);
			
			if(sharedDataObject.data.GameLength != null)
				_gameLengthSlider.value = sharedDataObject.data.GameLength;
			if(sharedDataObject.data.TurnLength != null)
				_turnLengthSlider.value = sharedDataObject.data.TurnLength;
			
			var infectionTitle:TextField = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent/2, "Infection", "Questrial",PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			infectionTitle.pivotX = infectionTitle.width/2;
			addChild(infectionTitle);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				infectionTitle.y = _turnLengthSlider.y + (infectionTitle.height * 3.0);
			else
				infectionTitle.y = _turnLengthSlider.y + (infectionTitle.height * 2.0);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_infectionCheckbox = new BW_Button(0.75, 1.0, "", 0);
			else
				_infectionCheckbox = new BW_Button(0.5, 0.7, "", 0);
			
			_infectionCheckbox.addImage(Main.assets.getTexture("CheckBoxUp"), Main.assets.getTexture("CheckBoxDown"));
			_infectionCheckbox.toggle = true;
			addChild(_infectionCheckbox);
			_infectionCheckbox.x = infectionTitle.x + PathogenTest.wTenPercent * 1.3;
			_infectionCheckbox.y = infectionTitle.y + infectionTitle.height/2;
			_infectionCheckbox.addEventListener(TouchEvent.TOUCH, onTouchToggleInfection);
			
			//Create Back button.
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_backButton = new BW_Button(0.75, 1.0, "", 0);
			else
				_backButton = new BW_Button(0.5, 0.75, "", 0);
				
			_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(_backButton);
			_backButton.x = - background.width/2 + (_backButton.width * .7);
			_backButton.y = background.height/2 - (_backButton.height * .7);
			_backButton.addEventListener(TouchEvent.TOUCH, quitOptions);
		}
		private function changeGameLengthSlider($e:Event):void
		{
			//Updating Game Length Information
			if(_gameLengthSlider.value != _gameLengthSlider.maximum)
				_gameLengthTextfield.text = _gameLengthSlider.value.toString() + " Rounds";
			else
				_gameLengthTextfield.text = "Unlimited Rounds"
			
			//Save choice out
			sharedDataObject.data.GameLength = _gameLengthSlider.value;
			sharedDataObject.flush();
		}
		private function changeTurnLengthSlider($e:Event):void
		{
			//Updating Turn Length Information
			if(_turnLengthSlider.value != _turnLengthSlider.maximum)
				_turnLengthTextfield.text = _turnLengthSlider.value.toString() + " Sec's";
			else
				_turnLengthTextfield.text = "Unlimited";
					
			//Save choice out
			sharedDataObject.data.TurnLength = _turnLengthSlider.value;
			sharedDataObject.flush();
		}
		private function onTouchToggleInfection($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				gameSetup.addRemoveInfection();
			}
		}
		private function quitOptions($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				gameSetup.updateGameOptionsText();
				startFadeOut();
			}
		}
		override public function death():void
		{
			_backButton.destroy();
			_backButton = null;
			
			gameSetup.removeModeScreen();
			gameSetup = null;
		}
	}
}