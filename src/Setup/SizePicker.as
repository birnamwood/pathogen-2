package Setup
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_Selector;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class SizePicker extends Sprite
	{
		//Classes
		private var gameSetup:GameSetup;
		
		private var sizeHolder:Sprite;
		
		//Map Choices
		private var _smallMapBT:BW_Button;
		private var _mediumMapBT:BW_Button;
		private var _largeMapBt:BW_Button;
		
		private var destination:Point;
		
		private var textField:TextField;
		
		private var _mapSizeSelector:BW_Selector;
		
		//iPhone Vars
		private var _buttonHeight:Number;
		
		public function SizePicker(gameSetup:GameSetup)
		{
			super();
			
			this.gameSetup = gameSetup;
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_buttonHeight = 1;
			else
				_buttonHeight = .75;
			
			//Add Map Choices
			_smallMapBT = new Utils.BW_Button(2, _buttonHeight, "View Small Maps", 24);
			_smallMapBT.toggle = true;
			addChild(_smallMapBT);
			_smallMapBT.x = -PathogenTest.wTenPercent * 2.2;
			_smallMapBT.addEventListener(TouchEvent.TOUCH, gameSetup.selectedSmallMap);
			
			_mediumMapBT = new Utils.BW_Button(2, _buttonHeight, "View Medium Maps", 24);
			_mediumMapBT.toggle = true;
			addChild(_mediumMapBT);
			_mediumMapBT.addEventListener(TouchEvent.TOUCH, gameSetup.selectedMediumMap);
			
			_largeMapBt = new Utils.BW_Button(2, _buttonHeight, "View Large Maps", 24);
			_largeMapBt.toggle = true;
			addChild(_largeMapBt);
			_largeMapBt.x = PathogenTest.wTenPercent * 2.2;
			_largeMapBt.addEventListener(TouchEvent.TOUCH, gameSetup.selectedLargeMap);
			
			const texture_02:Texture = Main.assets.getTexture("scale9_outline");
			const textures_02:Scale9Textures = new Scale9Textures(texture_02, new Rectangle(20, 20, 20, 20));
			
			_mapSizeSelector = new BW_Selector(2, _buttonHeight);
			_mapSizeSelector.x = _smallMapBT.x;
			_mapSizeSelector.y = _smallMapBT.y;
			addChild(_mapSizeSelector);
			_mapSizeSelector.visible = false;
			
			_smallMapBT.toggleDown();
		}
		public function garbageCollection():void
		{
			_smallMapBT.destroy()
			_smallMapBT = null;
			
			_mediumMapBT.destroy();
			_mediumMapBT = null;
			
			_largeMapBt.destroy();
			_largeMapBt = null;
			
			_mapSizeSelector.destroy();
			_mapSizeSelector.dispose();
			removeChild(_mapSizeSelector);
			_mapSizeSelector = null;
			
			gameSetup = null;
		}
		public function whichPicker(whatButton:String):void
		{
			_mapSizeSelector.visible = true;
			
			if(whatButton == "Small Map")
			{
				_mediumMapBT.toggleUp();
				_largeMapBt.toggleUp();
				
				_mapSizeSelector.x = _smallMapBT.x;
				_mapSizeSelector.y = _smallMapBT.y;
			}
			if(whatButton == "Medium Map")
			{
				_smallMapBT.toggleUp();
				_largeMapBt.toggleUp();
				
				_mapSizeSelector.x = _mediumMapBT.x;
				_mapSizeSelector.y = _mediumMapBT.y;
			}
			if(whatButton == "Large Map")
			{
				_smallMapBT.toggleUp();
				_mediumMapBT.toggleUp();
				
				_mapSizeSelector.x = _largeMapBt.x;
				_mapSizeSelector.y = _largeMapBt.y;
			}
		}
	}
}