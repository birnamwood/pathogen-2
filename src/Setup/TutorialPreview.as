package Setup
{
	import flash.geom.Rectangle;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import UI.Main;
	
	public class TutorialPreview extends Sprite
	{
		public function TutorialPreview(whichTutorial:String)
		{
			super();
			
			const texture:Texture = Main.assets.getTexture("scale9_black");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20, 20, 20, 20));
			
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 3;
			background.height = PathogenTest.hTenPercent * 7;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor;
			var title:TextField = new TextField(background.width, PathogenTest.hTenPercent, whichTutorial, "Dekar", fontSize, Color.WHITE);
			title.pivotX = title.width/2;
			addChild(title);
			title.y = -background.height/2
			
			fontSize = PathogenTest.HD_Multiplyer * 27 / PathogenTest.scaleFactor;
			var bodyText:TextField = new TextField(background.width * .9, background.height * .8, "", "Dekar", fontSize, Color.WHITE);
			
			switch(whichTutorial)
			{
				case "Capture Zone":
					bodyText.text = "Capture Zones are special locations on the board. Every ten rounds all active capture zones on the map are claimed. When claimed all cells within those zones are evolved to their highest state. \n \nCapture Zones can be claimed early by holding all points within a zone."
					break;
				
				case "Erosion Zone":
					bodyText.text = "Erosion Zones are hidden zones on the board. Every round a random zone will decay until it erodes away entirly, revealing new areas or pathways. \n \n A board with multiple erosion zones will select a new active zone after each decay."
					break;
					
				default:
					break;
			}
			
			bodyText.pivotX = bodyText.width/2;
			addChild(bodyText);
			bodyText.y = title.y + title.height;
			bodyText.hAlign = HAlign.LEFT;
		}
	}
}