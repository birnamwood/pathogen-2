package Setup
{
	import Campaign.DifficultySelector;
	
	import Maps.MapInfo;
	import Maps.MapPreview;
	
	import UI.Loader;
	import UI.Main;
	import UI.MainMenu;
	
	import Utils.BW_Button;
	import Utils.BW_Selector;
	import Utils.BW_UI;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.display.Scale3Image;
	import feathers.display.Scale9Image;
	import feathers.events.FeathersEventType;
	import feathers.layout.HorizontalLayout;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	
	public class GameSetup extends BW_UI
	{
		private const HEADING_TEXT_COLOR:uint = 0xAAAAAA;
		
		//Classes
		private var _main:Main;
		
		//Buttons
		private var _backButton:BW_Button;
		private var _playersButton:BW_Button;
		private var _mapsButton:BW_Button;
		private var _advancedOptions:BW_Button;
		private var _summeryButton:BW_Button;
		
		//Progression Buttons
		private var _nextButton:BW_Button;
		private var _nextButtonSelector:BW_Selector;
		private var _previousButton:BW_Button;
		
		//Top Bar Information
		private var _playerIndicators:Array = [];
		private var _mapSizeTextfield:TextField;
		private var _mapNameTextfield:TextField;
		
		//Background
		private var screenBackground:Quad
		private var _tab:Image;
		
		//Player Screen
		private var _playerHolder:Sprite;
		private var playerPickerArray:Array = [];
		private var _difficultySelector:DifficultySelector;
		
		//Map Screen
		private var _mapHolder:Sprite;
		private var sizePicker:SizePicker;
		public var sliding:Boolean = false;
		private var container:ScrollContainer;
		private var chosenMapArray:Array;
		private var chosenMapNameArray:Array;
		public var chosenMap:MapPreview;
		public var mapSize:int = -1;
		private var _mapBackground:Quad
		private var _mapSelector:BW_Selector;
		private var _mapPreviewArray:Array = [];
		private var _mapPreviewTutorialArray:Array = [];
		private var _loader:Loader;
		
		//Defaults
		private var defaultMapName:String = "Rectangle";
		private var defaultMapData:String = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,";
		
		// arrays of Maps
		private var smallMapDataArray:Array;
		private var smallMapNameArray:Array;
		private var mediumDataArray:Array;
		private var mediumMapNameArray:Array;
		private var largeDataArray:Array;
		private var largeMapNameArray:Array;
		
		//Summery Screen
		private var _summeryHolder:Sprite;
		private var _startButton:BW_Button
		private var ready:Boolean;
		private var _startGameButtonSelector:BW_Selector;
		private var _summeryMapPreview:MapPreview;
		
		//Modes
		public var modes:Array = [];
		public var modePicker:ModePicker;
		private var _gameLength:TextField;
		private var _turnLength:TextField;
		
		//Save
		private var sharedDataObject:SharedObject;
		
		//iPhone Vars
		private var _buttonHeight:Number;
		
		private var _onPlayers:Boolean;
		private var _onMaps:Boolean;
		private var _onSummary:Boolean;
		
		public function GameSetup(main:Main)
		{
			super();
						
			_main = main;
			sharedDataObject = Main.saveDataObject;
			
			loadSystemMaps();
			loadUserCreatedMaps();
			
			var defaultMap:MapPreview = new MapPreview(defaultMapName, defaultMapData, 0, this);
			chosenMap = defaultMap;
			 
			//Creating UI Bands
			var bottomUIBand:Quad;
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				bottomUIBand  = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1.8, Color.BLACK);
			else
				bottomUIBand  = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1.5, Color.BLACK);
			
			bottomUIBand.pivotX = bottomUIBand.width/2;
			bottomUIBand.pivotY = bottomUIBand.height;
			addChild(bottomUIBand);
			bottomUIBand.x = PathogenTest.stageCenter.x;
			bottomUIBand.y = PathogenTest.stageHeight;
			bottomUIBand.alpha = .5;
			
			var topUIBand:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1, Color.BLACK);
			topUIBand.pivotX = topUIBand.width/2;
			addChild(topUIBand);
			topUIBand.x = PathogenTest.stageCenter.x;
			topUIBand.alpha = .5;
			
			//Title Text
			var title:TextField = new TextField(PathogenTest.wTenPercent * 2.5, PathogenTest.hTenPercent * .6, "Game Setup", "Questrial", PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(title);
			title.hAlign = HAlign.LEFT;
			title.x = PathogenTest.wTenPercent * .1;
			title.y = PathogenTest.hTenPercent * .2;
			
			//Player Title text
			var playerText:TextField = new TextField(PathogenTest.wTenPercent * 1, PathogenTest.hTenPercent, "Players:", "Questrial", PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
			addChild(playerText);
			playerText.hAlign = HAlign.LEFT;
			playerText.y = title.y - PathogenTest.hTenPercent * .1;
			playerText.x = title.x + title.width;
			
			//Create player indicators that live on the TOP UI.
			var startingPos:Point = new Point(playerText.x + playerText.width, playerText.y + playerText.height/2)
				
			for (var i:int = 0; i < 4; i++)
			{
				var playerIndicator:PlayerIndicator = new PlayerIndicator();
				addChild(playerIndicator);
				playerIndicator.x = startingPos.x + ((playerIndicator.width * i) * 1.1);
				playerIndicator.y = startingPos.y;
				_playerIndicators.push(playerIndicator);
			}
			
			//Creat top map information
			var mapSizeTextfield:TextField = new TextField(PathogenTest.wTenPercent * .9, PathogenTest.hTenPercent, "Map Size:", "Questrial",PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
			addChild(mapSizeTextfield);
			mapSizeTextfield.hAlign = HAlign.LEFT;
			mapSizeTextfield.y = title.y - PathogenTest.hTenPercent * .1;
			mapSizeTextfield.x = playerIndicator.x + (playerIndicator.width * 1.4);
			
			_mapSizeTextfield = new TextField(PathogenTest.wTenPercent * 1, PathogenTest.hTenPercent, "", "Questrial",PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(_mapSizeTextfield);
			_mapSizeTextfield.hAlign = HAlign.LEFT;
			_mapSizeTextfield.y = mapSizeTextfield.y;
			_mapSizeTextfield.x = mapSizeTextfield.x + mapSizeTextfield.width;
			
			var mapNameTextfield:TextField = new TextField(PathogenTest.wTenPercent * 1.1, PathogenTest.hTenPercent, "Map Name:", "Questrial",PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
			addChild(mapNameTextfield);
			mapNameTextfield.hAlign = HAlign.LEFT;
			mapNameTextfield.y = mapSizeTextfield.y;
			mapNameTextfield.x = PathogenTest.wTenPercent * 7.2;
			
			//if(PathogenTest.device != PathogenTest.PC)
				//mapNameTextfield.y = mapSizeTextfield.y - PathogenTest.hTenPercent * .1;
			
			_mapNameTextfield = new TextField(PathogenTest.wTenPercent * 1.3, PathogenTest.hTenPercent, "", "Questrial",PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(_mapNameTextfield);
			_mapNameTextfield.hAlign = HAlign.LEFT;
			_mapNameTextfield.y = mapSizeTextfield.y;
			_mapNameTextfield.x = mapNameTextfield.x + mapNameTextfield.width;
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_buttonHeight = 1;
			else
				_buttonHeight = 0.75;
			
			//Creating Tabs
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_backButton = new BW_Button(0.75, 1, "", 0);
			else
				_backButton = new BW_Button(0.5, 0.75, "", 0);
			_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_Back"), Main.assets.getTexture("Pathogen_BTN_Back_Down"));
			addChild(_backButton);
			_backButton.x = PathogenTest.wTenPercent * .5;
			_backButton.y = bottomUIBand.y - bottomUIBand.height/2;
			_backButton.addEventListener(TouchEvent.TOUCH, quitSetup);
			
			_playersButton = new Utils.BW_Button(2, _buttonHeight, "Players", 32);
			addChild(_playersButton);
			_playersButton.x = PathogenTest.wTenPercent * 2.5;
			_playersButton.y = _backButton.y;
			_playersButton.addEventListener(TouchEvent.TOUCH, openPlayerScreen);
			
			_mapsButton = new Utils.BW_Button(2, _buttonHeight, "Maps", 32);
			addChild(_mapsButton);
			_mapsButton.x = PathogenTest.stageCenter.x;
			_mapsButton.y = _backButton.y;
			_mapsButton.addEventListener(TouchEvent.TOUCH, openMapScreen);
			
			_summeryButton = new Utils.BW_Button(2, _buttonHeight, "Summary", 32);
			addChild(_summeryButton);
			_summeryButton.x = PathogenTest.wTenPercent * 7.5;
			_summeryButton.y = _backButton.y;
			_summeryButton.addEventListener(TouchEvent.TOUCH, openSummeryScreen);
			
			//Creating screen background
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				screenBackground = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 7.2, 0x222245);
			else
				screenBackground = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 7.5, 0x222245);
			
			screenBackground.pivotX = screenBackground.width/2;
			screenBackground.color = 0x222245;
			screenBackground.alpha = .5;
			screenBackground.x = PathogenTest.stageCenter.x;
			screenBackground.y = topUIBand.height;
			addChild(screenBackground);
			
			//Create "tab" indicator
			_tab = new Image(Main.assets.getTexture("Indicator"))
			_tab.pivotX = _tab.width/2;
			_tab.x = _playersButton.x;
			_tab.y = screenBackground.y + screenBackground.height + (_tab.height * .2);
			addChildAt(_tab, 1);
			
			//Create next/back buttons
			_nextButton = new BW_Button(1.25,_buttonHeight, "", 24);
			_nextButton.addImage(Main.assets.getTexture("BTN_Next"), Main.assets.getTexture("BTN_Next_Down"));
			addChild(_nextButton);
			_nextButton.x = PathogenTest.wTenPercent * 9.1;
			_nextButton.y = PathogenTest.stageCenter.y - screenBackground.height/2 + (_nextButton.height * .8);
			_nextButton.visible = false;
			
			// highlight next button
			_nextButtonSelector = new BW_Selector(1.3, _buttonHeight + .05);
			addChild(_nextButtonSelector);
			setChildIndex(_nextButton, numChildren-1);
			_nextButtonSelector.x = _nextButton.x;
			_nextButtonSelector.y = _nextButton.y;
			_nextButtonSelector.visible = false;
			
			_previousButton = new BW_Button(1.25,_buttonHeight, "", 24);
			_previousButton.addImage(Main.assets.getTexture("BTN_Back"), Main.assets.getTexture("BTN_Back_Down"));
			addChild(_previousButton);
			_previousButton.x = PathogenTest.wTenPercent * .9;
			_previousButton.y = PathogenTest.stageCenter.y - screenBackground.height/2 + (_nextButton.height * .8);
			_previousButton.visible = false;
			
			openPlayerScreen(null);
		}
		private function openPlayerScreen($e:TouchEvent):void
		{
			if($e)
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			else
				var open:Boolean = true;
			
			if (touch || open)
			{
				_onPlayers = true;
				_onMaps = false;
				_onSummary = false;
				
				if(_playerHolder == null)
				{
					var i:int;
					
					_playerHolder = new Sprite();
					addChild(_playerHolder);
					
					//Locations for where they player pickers go.
					var playerOnePickerLocation:Point = new Point(PathogenTest.stageCenter.x - (PathogenTest.wTenPercent * 1.6), PathogenTest.stageCenter.y - PathogenTest.hTenPercent* 2.6)
					var playerTwoPickerLocation:Point = new Point(PathogenTest.stageCenter.x + (PathogenTest.wTenPercent * 1.6), PathogenTest.stageCenter.y - PathogenTest.hTenPercent * 2.6)
					var playerThreePickerLocation:Point = new Point(PathogenTest.stageCenter.x + (PathogenTest.wTenPercent * 1.6), PathogenTest.stageCenter.y + PathogenTest.hTenPercent* .8);
					var playerFourPickerLocation:Point = new Point(PathogenTest.stageCenter.x - (PathogenTest.wTenPercent * 1.6), PathogenTest.stageCenter.y + PathogenTest.hTenPercent* .8);
					
					var playerLocationArray_01:Array = [playerOnePickerLocation,playerTwoPickerLocation, playerThreePickerLocation, playerFourPickerLocation];
					
					var maxNumOfPlayers:int = 4;
					
					//Create all potential players.
					for (i = 0; i < maxNumOfPlayers; i++)
					{
						var playerPicker:PlayerPicker = new PlayerPicker(i, this);
						_playerHolder.addChild(playerPicker);
						playerPickerArray.push(playerPicker);
						playerPicker.x = playerLocationArray_01[i].x;
						playerPicker.y = playerLocationArray_01[i].y;
					}
					
					_playerHolder.y = PathogenTest.hTenPercent * .6;
					
					// add difficulty selector
					_difficultySelector = new DifficultySelector(true);
					_playerHolder.addChild(_difficultySelector);
					_difficultySelector.x = PathogenTest.stageCenter.x;
					
					if (Main.isPhone())
					//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					{
						_difficultySelector.x = PathogenTest.wTenPercent * 1;
						_difficultySelector.y = PathogenTest.stageCenter.y - _difficultySelector.height/2;
					}
					else
						_difficultySelector.y = playerPicker.y + playerPicker.height/2 + (_difficultySelector.height * .6);
				}
				
				//Hide all other tabs except this one.
				if(_playerHolder)
					_playerHolder.visible = true;
				if(_mapHolder)
					_mapHolder.visible = false;
				if(modePicker)
					modePicker.visible = false;
				if(_summeryHolder)
					_summeryHolder.visible = false;
				
				_tab.x = _playersButton.x;
				
				_nextButton.visible = true;
				_previousButton.visible = false;
				
				//Remove Potential other listeners
				_nextButton.removeEventListener(TouchEvent.TOUCH, openSummeryScreen);
				
				_nextButton.addEventListener(TouchEvent.TOUCH, openMapScreen);
				
				var anyActive:Boolean = false;
				//Updates the quick-player indicatiors on the TOP UI to reflect the choices made.
				for (i = 0; i < _playerIndicators.length; i++)
				{
					var currentPlayerPicker:PlayerPicker = playerPickerArray[i];
					
					if(currentPlayerPicker.active)
						anyActive = true;
				}
				if (anyActive)
					_nextButtonSelector.visible = true;
				else
					_nextButtonSelector.visible = false;
			}
		}
		public function updatePlayerIndicators():void
		{
			var anyActive:Boolean = false;
			//Updates the quick-player indicatiors on the TOP UI to reflect the choices made.
			for (var i:int = 0; i < _playerIndicators.length; i++)
			{
				var currentPlayerPicker:PlayerPicker = playerPickerArray[i];
				var currentIndicator:PlayerIndicator = _playerIndicators[i];
				
				if(currentPlayerPicker.active)
				{
					currentIndicator.changeColor(currentPlayerPicker.chosenColor);
					anyActive = true;
				}
				else
					currentIndicator.changeColor(-1);
			}
			if (anyActive)
				_nextButtonSelector.visible = true;
			else
				_nextButtonSelector.visible = false;
		}
		private function openMapScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_onPlayers = false;
				_onMaps = true;
				_onSummary = false;
				
				if(_mapHolder == null)
				{
					//Create the map screen if not already created.
					_mapHolder = new Sprite();
					addChild(_mapHolder);
					
					sizePicker = new SizePicker(this);
					_mapHolder.addChild(sizePicker);
					sizePicker.x = PathogenTest.stageCenter.x;
					sizePicker.y = PathogenTest.stageCenter.y - screenBackground.height/2 + (sizePicker.height * .8);				
					selectedSmallMap($e);
				}
				
				//Hide all other tabs except this one.
				if(_mapHolder)
					_mapHolder.visible = true;
				if(_playerHolder)
					_playerHolder.visible = false;
				if(modePicker)
					modePicker.visible = false;
				if(_summeryHolder)
					_summeryHolder.visible = false;
				
				_tab.x = _mapsButton.x;
				
				_nextButton.visible = true;
				_nextButtonSelector.visible = false;
				_previousButton.visible = true;
				
				//Remove Potential other listeners
				_nextButton.removeEventListener(TouchEvent.TOUCH, openMapScreen);
				_previousButton.removeEventListener(TouchEvent.TOUCH, openMapScreen);
				
				_nextButton.addEventListener(TouchEvent.TOUCH, openSummeryScreen);
				_previousButton.addEventListener(TouchEvent.TOUCH, openPlayerScreen);
				
				if (_mapSelector)
				{
					if (_mapSelector.visible)
						_nextButtonSelector.visible = true;
					else
						_nextButtonSelector.visible = false;
				}
				else
					_nextButtonSelector.visible = false;
			}
		}
		private function addLoader():void
		{
			_loader = new Loader("Loading Maps", 24);
			addChild(_loader);
			_loader.x = PathogenTest.stageCenter.x;
			_loader.y = PathogenTest.stageCenter.y;
		}
		public function selectedSmallMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && mapSize != 0)
			{
				//Player has selected a small map.
				sizePicker.whichPicker("Small Map");
				mapSize = 0;
				chosenMapArray = smallMapDataArray;
				chosenMapNameArray = smallMapNameArray;
				_mapSizeTextfield.text = "Small";
				
				addLoader();
				
				Starling.juggler.delayCall(addMaps, 0.05);
			}
		}
		public function selectedMediumMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && mapSize != 1)
			{
				//Player has selected a medium Map
				sizePicker.whichPicker("Medium Map");
				mapSize = 1;
				chosenMapArray = mediumDataArray;
				chosenMapNameArray = mediumMapNameArray;
				_mapSizeTextfield.text = "Medium";
				
				addLoader();
				
				Starling.juggler.delayCall(addMaps, 0.05);
			}
		}
		public function selectedLargeMap($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && mapSize != 2)
			{
				//Player has selected a large Map
				sizePicker.whichPicker("Large Map");
				mapSize = 2;
				chosenMapArray = largeDataArray;
				chosenMapNameArray = largeMapNameArray;
				_mapSizeTextfield.text = "Large";
				
				addLoader();
				
				Starling.juggler.delayCall(addMaps, 0.05);
			}
		}
		private function addMaps():void
		{
			
			//Add the scrollable map container object.
			var layout:HorizontalLayout = new HorizontalLayout();
			
			if(container != null)
			{
				for each(var map_Preview:MapPreview in _mapPreviewArray)
				{
					map_Preview.garbageCollection();
					map_Preview.dispose();
					container.removeChild(map_Preview);
					map_Preview = null;
				}
				
				for each (var map_Preview_Tutorial:TutorialPreview in _mapPreviewTutorialArray)
				{
					map_Preview_Tutorial.dispose();
					container.removeChild(map_Preview_Tutorial);
					map_Preview_Tutorial = null; 
				}
				
				_mapHolder.removeChild(container);
				container.dispose();
				container = null;
			}
				
			
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(20, 20, 20, 20));
			
			if(_mapBackground != null)
				_mapHolder.removeChild(_mapBackground);
			
			//Add a black background so they show up better.
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_mapBackground = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 5.2, 0x000000);
			else
				_mapBackground = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 5.5, 0x000000);
			
			_mapBackground.pivotX = _mapBackground.width/2;
			_mapBackground.pivotY = _mapBackground.height/2;
			_mapBackground.alpha = .5;
			_mapBackground.x = PathogenTest.stageCenter.x;
			_mapBackground.y = PathogenTest.stageCenter.y + PathogenTest.hTenPercent * .45;
			_mapHolder.addChild(_mapBackground);
			
			//Create container for scrollable objects.
			container = new ScrollContainer();
			container.layout = layout;
			container.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			container.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			container.scrollerProperties.snapScrollPositionsToPixels = true;
			_mapHolder.addChild(container);
			
			container.visible = false;
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				container.y = PathogenTest.stageCenter.y * 0.6;
			else
				container.y = PathogenTest.stageCenter.y * 0.5;
			
			
			container.width = PathogenTest.stageWidth;
			container.height = _mapBackground.height;
			layout.gap = PathogenTest.wTenPercent/2;
			
			_mapPreviewArray = [];
			_mapPreviewTutorialArray = [];
			
			//Create all map previews
			for (var i:int = 0; i < chosenMapArray.length; i++)
			{
				var mapPreview:MapPreview = new MapPreview(chosenMapNameArray[i], chosenMapArray[i], mapSize, this);
				
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				{
					mapPreview.scaleX = .6;
					mapPreview.scaleY = .6;
				}
				else
				{
					mapPreview.scaleX = .7;
					mapPreview.scaleY = .7;
				}
				
				if(mapPreview.mapName != "Rectangle" && Main._lite && Main._locked)
					mapPreview.alpha = .25;
				
				container.addChild(mapPreview);
				_mapPreviewArray.push(mapPreview);
				
				if(i == 5)
				{
					//At this point add in the capture Zone tutorial message
					var captureZoneTutorialPreview:TutorialPreview = new TutorialPreview("Capture Zone");
					captureZoneTutorialPreview.pivotX = captureZoneTutorialPreview.width/2;
					captureZoneTutorialPreview.scaleX = .7;
					captureZoneTutorialPreview.scaleY = .7;
					container.addChild(captureZoneTutorialPreview);
					_mapPreviewTutorialArray.push(captureZoneTutorialPreview);
				}
				
				if(i == 7)
				{
					//At this point add in the Erosion Zone tutorial message
					var erosionZoneTutorialPreview:TutorialPreview = new TutorialPreview("Erosion Zone");
					erosionZoneTutorialPreview.pivotX = erosionZoneTutorialPreview.width/2;
					erosionZoneTutorialPreview.scaleX = .7;
					erosionZoneTutorialPreview.scaleY = .7;
					container.addChild(erosionZoneTutorialPreview);
					_mapPreviewTutorialArray.push(erosionZoneTutorialPreview);
				}
			}
			
			layout.paddingTop = mapPreview.height * .6;
			layout.paddingLeft = mapPreview.width;
			
			if(_mapSelector != null)
				container.removeChild(_mapSelector);
			
			_mapSelector = new BW_Selector(4,5);
			container.addChild(_mapSelector);
			_mapSelector.visible = false;
			
			const texture_02:Texture = Main.assets.getTexture("scale9_outline");
			const textures_02:Scale9Textures = new Scale9Textures(texture_02, new Rectangle(20, 20, 20, 20));
			
			container.addEventListener(FeathersEventType.SCROLL_COMPLETE, scrollComplete);
			container.addEventListener(Event.SCROLL, scrolling);
			Starling.juggler.delayCall(stopSliding, 0.25);
			
			setChildIndex(_backButton, numChildren-1);
			
			_loader.startFadeOut();
			
			Starling.juggler.delayCall(stopSliding, .5);
			
			Starling.juggler.delayCall(revealContainer, .5);
		}
		private function scrollComplete($e:Event):void
		{
			sliding = false;
		}
		private function scrolling($e:Event):void
		{
			sliding = true;
		}
		private function stopSliding():void
		{
			sliding = false;
		}
		private function revealContainer():void
		{
			container.visible = true;
		}
		public function selectLevel(whatPreview:MapPreview):void
		{
			if (_onMaps)
			{
				_mapSelector.visible = true;
				_nextButtonSelector.visible = true;
			}
			
			//Player has selected a map from the list.
			_mapSelector.x = whatPreview.x;
			_mapSelector.y = whatPreview.y;
			
			_mapNameTextfield.text = whatPreview.mapName;
			
			chosenMap = whatPreview;
		}
		private function loadSystemMaps():void
		{
			var i:int;
			
			// small maps
			smallMapDataArray = [];
			smallMapNameArray = [];
			for (i=0; i<MapInfo.smallMapDataArray.length; i++)
			{
				smallMapDataArray.push(MapInfo.smallMapDataArray[i]);
				smallMapNameArray.push(MapInfo.smallMapNameArray[i]);
			}
			
			// medium maps
			mediumDataArray = [];
			mediumMapNameArray = [];
			for (i=0; i<MapInfo.mediumDataArray.length; i++)
			{
				mediumDataArray.push(MapInfo.mediumDataArray[i]);
				mediumMapNameArray.push(MapInfo.mediumMapNameArray[i]);
			}
			
			// large maps
			largeDataArray = [];
			largeMapNameArray = [];
			for (i=0; i<MapInfo.largeMapNameArray.length; i++)
			{
				largeDataArray.push(MapInfo.largeDataArray[i]);
				largeMapNameArray.push(MapInfo.largeMapNameArray[i]);
			}
		}
		private function loadUserCreatedMaps():void
		{
			var totalNumOfSmallSaveSlots:int = 6;
			var totalNumOfMediumSaveSlots:int = 6;
			var totalNumOfLargeSaveSlots:int = 6;
			
			var mapString:String;
			
			var saveMapArray:Array;
			var saveMapName:String;
			var saveMapData:String;
			
			var i:int;
			
			for (i= 0; i < totalNumOfSmallSaveSlots; i++)
			{
				switch(i)
				{
					//Small Maps
					case 0:
						if (sharedDataObject.data.userSmall_01Map != null)
							mapString = sharedDataObject.data.userSmall_01Map;
						break;
					case 1:
						if (sharedDataObject.data.userSmall_02Map != null)
							mapString = sharedDataObject.data.userSmall_02Map;
						break;
					case 2:
						if (sharedDataObject.data.userSmall_03Map != null)
							mapString = sharedDataObject.data.userSmall_03Map;
						break;
					case 3:
						if (sharedDataObject.data.userSmall_04Map != null)
							mapString = sharedDataObject.data.userSmall_04Map;
						break;
					case 4:
						if (sharedDataObject.data.userSmall_05Map != null)
							mapString = sharedDataObject.data.userSmall_05Map;
						break;
					case 5:
						if (sharedDataObject.data.userSmall_06Map != null)
							mapString = sharedDataObject.data.userSmall_06Map;
						break;
				}
				
				if (mapString != null)
				{
					//If there is data, push it into the correct array.
					saveMapArray = mapString.split(":");
					
					saveMapName = saveMapArray[0];
					saveMapData = saveMapArray[1];
					
					smallMapNameArray.push(saveMapName);
					smallMapDataArray.push(saveMapData);
					
					mapString = null;
				}
			}
			for (i = 0; i < totalNumOfMediumSaveSlots; i++)
			{
				switch(i)
				{
					//Medium Maps
					case 0:
						if (sharedDataObject.data.userMedium_01Map != null)
							mapString = sharedDataObject.data.userMedium_01Map;
						break;
					case 1:
						if (sharedDataObject.data.userMedium_02Map != null)
							mapString = sharedDataObject.data.userMedium_02Map;
						break;
					case 2:
						if (sharedDataObject.data.userMedium_03Map != null)
							mapString = sharedDataObject.data.userMedium_03Map;
						break;
					case 3:
						if (sharedDataObject.data.userMedium_04Map != null)
							mapString = sharedDataObject.data.userMedium_04Map;
						break;
					case 4:
						if (sharedDataObject.data.userMedium_05Map != null)
							mapString = sharedDataObject.data.userMedium_05Map;
						break;
					case 5:
						if (sharedDataObject.data.userMedium_06Map != null)
							mapString = sharedDataObject.data.userMedium_06Map;
						break;
				}
				
				if (mapString != null)
				{
					// trace("Have found a user created map");
					
					//If there is data, push it into the correct array.
					saveMapArray = mapString.split(":");
					
					saveMapName = saveMapArray[0];
					saveMapData = saveMapArray[1];
					
					// trace("Map Name: " + saveMapName);
					// trace("Map Data: " + saveMapData);
					// trace("-------------------------");
					
					mediumMapNameArray.push(saveMapName);
					mediumDataArray.push(saveMapData);
					
					mapString = null;
				}
			}
			
			for ( i = 0; i < totalNumOfLargeSaveSlots; i++)
			{
				switch(i)
				{
					//Large Maps
					case 0:
						if (sharedDataObject.data.userLarge_01Map != null)
							mapString = sharedDataObject.data.userLarge_01Map;
						break;
					case 1:
						if (sharedDataObject.data.userLarge_02Map != null)
							mapString = sharedDataObject.data.userLarge_02Map;
						break;
					case 2:
						if (sharedDataObject.data.userLarge_03Map != null)
							mapString = sharedDataObject.data.userLarge_03Map;
						break;
					case 3:
						if (sharedDataObject.data.userLarge_04Map != null)
							mapString = sharedDataObject.data.userLarge_04Map;
						break;
					case 4:
						if (sharedDataObject.data.userLarge_05Map != null)
							mapString = sharedDataObject.data.userLarge_05Map;
						break;
					case 5:
						if (sharedDataObject.data.userLarge_06Map != null)
							mapString = sharedDataObject.data.userLarge_06Map;
						break;
				}
				
				if (mapString != null)
				{
					//If there is data, push it into the correct array.
					saveMapArray = mapString.split(":");
					
					saveMapName = saveMapArray[0];
					saveMapData = saveMapArray[1];
					
					largeMapNameArray.push(saveMapName);
					largeDataArray.push(saveMapData);
					
					mapString = null;
				}
			}
		}
		private function openModeScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(Main._lite && Main._locked)
				{
					_main.openContentLockedScreen();
				}
				else
				{
					//Create the mode information if it hasn't been created.
					
					if(modePicker == null)
					{
						modePicker = new ModePicker(this);
						modePicker.x = PathogenTest.stageCenter.x;
						modePicker.y = PathogenTest.stageCenter.y;
						addChild(modePicker);
					}
				}
			}
		}
		public function removeModeScreen():void
		{
			if(modePicker)
			{
				modePicker.dispose();
				removeChild(modePicker)
				modePicker = null;
			}
		}
		private function quitSetup($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if(alpha < 1.0) return; 
			
			if (touch)
			{
				//Leave game setup & return to _main
				_backButton.removeEventListener(TouchEvent.TOUCH, quitSetup);
				
				for each (var mapPreview:MapPreview in container)
				{
					mapPreview.removeTouchEvents();
				}
				
				_main.openMainMenu();
				startFadeOut();
			}
		}
		public function getNextUnusedColor(inPlayer:int):int
		{
			//Get the next available color.
			var greenTaken:Boolean = false;
			var redTaken:Boolean = false;
			var blueTaken:Boolean = false;
			var yellowTaken:Boolean = false;
			
			for(var i:int = 0; i<playerPickerArray.length; i++)
			{
				if (i != inPlayer)
				{
					if (playerPickerArray[i].active)
					{
						if (playerPickerArray[i].chosenColor == 0)
							greenTaken = true;
						else if (playerPickerArray[i].chosenColor == 1)
							redTaken = true;
						else if (playerPickerArray[i].chosenColor == 2)
							blueTaken = true;
						else if (playerPickerArray[i].chosenColor == 3)
							yellowTaken = true;
					}
				}
			}
			
			var nextColor:int = -1;
			if (!greenTaken)
				nextColor = 0;
			else if (!redTaken)
				nextColor = 1;
			else if (!blueTaken)
				nextColor = 2;
			else if (!yellowTaken)
				nextColor = 3;
			return nextColor;
		}
		public function addOrRemoveDifficultySelector():void
		{
			var makeVisible:Boolean = false;
			
			//Check to see if there are any AI players in party
			for(var i:int = 0; i<playerPickerArray.length; i++)
			{
				if (playerPickerArray[i].ai)
					makeVisible = true;		
			}
			
			//Make the AI difficulty selector active.
			if (makeVisible)
			{
				if (_difficultySelector.status == DifficultySelector.INVISIBLE)
				{
					_difficultySelector.fadeIN();
					
					if (!Main.isPhone())
					//if (!(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5))
						_playerHolder.y = 0;
				}
				else if (_difficultySelector.alpha < 1.0)
				{
					_difficultySelector.makeInvisible();
					_difficultySelector.fadeIN();
					if (!Main.isPhone())
					//if (!(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5))
						_playerHolder.y = 0;
				}
			}
			//Hide the AI difficulty selector.
			else
			{
				if (_difficultySelector.status == DifficultySelector.VISIBLE)
				{
					_difficultySelector.fadeOUT();
					
					if (!Main.isPhone())
					//if (!(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5))
						_playerHolder.y = PathogenTest.hTenPercent * .6;
				}
				else if (_difficultySelector.alpha > 0.0)
				{
					_difficultySelector.makeVisible();
					_difficultySelector.fadeOUT();
					if (!Main.isPhone())
					//if (!(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5))
						_playerHolder.y = PathogenTest.hTenPercent * .6;
				}
			}
		}
		public function iPhonePlayerHolderReposition(isVisible:Boolean):void
		{
			if(isVisible)
			{
				_playerHolder.getChildAt(0).y += PathogenTest.hTenPercent * .2;
				_playerHolder.getChildAt(1).y += PathogenTest.hTenPercent * .2;
				
				_playerHolder.getChildAt(2).y -= PathogenTest.hTenPercent * .3;
				_playerHolder.getChildAt(3).y -= PathogenTest.hTenPercent * .3;
				
				return;
			}
			
			_playerHolder.getChildAt(0).y -= PathogenTest.hTenPercent * .2;
			_playerHolder.getChildAt(1).y -= PathogenTest.hTenPercent * .2;
			
			_playerHolder.getChildAt(2).y += PathogenTest.hTenPercent * .3;
			_playerHolder.getChildAt(3).y += PathogenTest.hTenPercent * .3;		
		}
		public function beginLevel($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				//Player has clicked START GAME Button. Collect the nessessary information, close Game Setup & launch the game.
				var playerLocations:Array = [];
				var playerColors:Array = [];
				var playerAI:Array = [];
				
				for each (var playerPicker:PlayerPicker in playerPickerArray)
				{
					if(playerPicker.active)
					{
						playerLocations.push(playerPicker.whatPlayer);
						playerColors.push(playerPicker.chosenColor);
						playerAI.push(playerPicker.ai);
					}
				}
				if(playerLocations.length == 0)
				{
					playerColors.push(0);
					playerLocations.push(0);
				}
				
				_main.initGameManager(playerColors, playerLocations, playerAI, chosenMap.mapData, chosenMap.mapName, chosenMap.mapSize, modes);
				startFadeOut();
			}
		}
		public function addRemoveInfection():void
		{
			if(modes.length == 0)
				modes.push("Infection");
			else
				modes = [];
		}
		public function checkReady():void
		{
			//Check to see if a player is active.
			var playerSelected:Boolean;
			var mapSelected:Boolean;
			
			for (var i:int = 0; i < playerPickerArray.length; i++)
			{
				var playerPicker:PlayerPicker = playerPickerArray[i];
				
				if(playerPicker.active)
					playerSelected = true;
			}
			
			//If there is a chosen map
			if(chosenMap != null)
				mapSelected = true;
			
			//If both are good, change ready to true.
			if(mapSelected && playerSelected)
				ready = true;
			
			if(ready)
			{
				_startButton.alpha = 1;
				_startButton.addEventListener(TouchEvent.TOUCH, beginLevel);
				_startGameButtonSelector.visible = true;
			}
		}
		private function openSummeryScreen($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_onPlayers = false;
				_onMaps = false;
				_onSummary = true;

				if(_summeryHolder != null)
				{
					_summeryMapPreview.garbageCollection();
					_summeryMapPreview.dispose();
					_summeryMapPreview.removeChild(_summeryMapPreview);
					_summeryMapPreview = null;
					
					_summeryHolder.dispose();
					removeChild(_summeryHolder);
					_summeryHolder = null;
				}
				
				_summeryHolder = new Sprite();
				addChild(_summeryHolder);
				
				var scale9Num:Number = 20/PathogenTest.scaleFactor;
				const texture:Texture = Main.assets.getTexture("scale9_White");
				const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
				
				//Add a black background so they show up better.
				var transBackground:Scale9Image = new Scale9Image(textures);
				transBackground.width = PathogenTest.wTenPercent * 6.6;
				transBackground.height = PathogenTest.hTenPercent * 6.6;
				transBackground.pivotX = transBackground.width/2;
				transBackground.pivotY = transBackground.height/2;
				transBackground.color = 0x000000;
				transBackground.alpha = .5;
				transBackground.x = PathogenTest.stageCenter.x + PathogenTest.wTenPercent * .1;
				transBackground.y = PathogenTest.stageCenter.y - (PathogenTest.hTenPercent * 1.4);
				_summeryHolder.addChild(transBackground);
				
				//Create a large preview of the map they are going to play on.
				_summeryMapPreview = new MapPreview(chosenMap.mapName, chosenMap.originalData, chosenMap.mapSize, this);
				_summeryMapPreview.scaleX = .7;
				_summeryMapPreview.scaleY = .7;
				_summeryMapPreview.x = PathogenTest.wTenPercent * 4;
				_summeryMapPreview.y = PathogenTest.hTenPercent * 3;
				_summeryHolder.addChild(_summeryMapPreview);
				
				if (_mapNameTextfield)
					_mapNameTextfield.text = _summeryMapPreview.mapName;
				
				var playerTitle:TextField = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent * .7, "Players", "Dekar", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
				_summeryHolder.addChild(playerTitle);
				playerTitle.x = _summeryMapPreview.x + (_summeryMapPreview.width * .6);
				playerTitle.y = PathogenTest.hTenPercent * .45 + (i * playerTitle.height);
				playerTitle.hAlign = HAlign.LEFT;
				
				//For every active player, list their name & color.
				for (var i:int = 0; i < playerPickerArray.length; i++)
				{
					var playerPicker:PlayerPicker = playerPickerArray[i];
					
					if(playerPicker.active)
					{
						var playerString:String = "";
						if (playerPickerArray[i].ai)
							playerString = "AI " + (i+1).toString() + ":";
						else
							playerString = "Player " + (i+1).toString() + ":";
						var playerTextField:TextField = new TextField(PathogenTest.wTenPercent * 1, PathogenTest.hTenPercent/2, playerString, "Dekar",PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
						_summeryHolder.addChild(playerTextField);
						playerTextField.hAlign = HAlign.LEFT;
						playerTextField.x = playerTitle.x;
						playerTextField.y = playerTitle.y + playerTitle.height + (i * playerTextField.height);
						
						var colorIndicatior:PlayerIndicator = new PlayerIndicator();
						_summeryHolder.addChild(colorIndicatior);
						colorIndicatior.changeColor(playerPicker.chosenColor);
						colorIndicatior.x = playerTextField.x + playerTextField.width;
						colorIndicatior.y = playerTextField.y + colorIndicatior.height/2;
						
					}
				}
				
				//Add additional details about the game.
				var gameLengthTitle:TextField  = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent * .5, "Game Length", "Dekar",PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
				_summeryHolder.addChild(gameLengthTitle);
				gameLengthTitle.x = playerTitle.x;
				gameLengthTitle.y = PathogenTest.hTenPercent * 3.3;
				gameLengthTitle.hAlign = HAlign.LEFT
				
				_gameLength  = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent/2, "Unlimited", "Dekar",PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
				_summeryHolder.addChild(_gameLength);
				_gameLength.x = playerTitle.x;
				_gameLength.y = gameLengthTitle.y + gameLengthTitle.height;
				_gameLength.hAlign = HAlign.LEFT;
				
				var turnLengthTitle:TextField  = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent * .5, "Turn Length", "Dekar",PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, HEADING_TEXT_COLOR);
				_summeryHolder.addChild(turnLengthTitle);
				turnLengthTitle.x = playerTitle.x;
				turnLengthTitle.y = _gameLength.y + _gameLength.height;
				turnLengthTitle.hAlign = HAlign.LEFT;
				
				_turnLength  = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent/2, "Unlimited", "Dekar",PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
				_summeryHolder.addChild(_turnLength);
				_turnLength.x = playerTitle.x;
				_turnLength.y = turnLengthTitle.y + turnLengthTitle.height;
				_turnLength.hAlign = HAlign.LEFT;
				
				updateGameOptionsText();
				
				//Create start button.
				_startButton = new BW_Button(3,1, "START GAME", 34);
				_summeryHolder.addChild(_startButton);
				_startButton.x = _summeryMapPreview.width * 1;
				_startButton.y = _summeryMapPreview.y + (_summeryMapPreview.height * .65);
				_startButton.alpha = .5;
				
				_advancedOptions = new BW_Button(2,1, "Adv. Options", 24);
				_summeryHolder.addChild(_advancedOptions);
				_advancedOptions.x = _summeryMapPreview.width * 1.8;
				_advancedOptions.y = _summeryMapPreview.y + (_summeryMapPreview.height * .65);
				_advancedOptions.addEventListener(TouchEvent.TOUCH, openModeScreen);
				
				if(Main._lite && Main._locked)
				{
					_advancedOptions.alpha = .5;
				}
				
				// highlight start button
				if(_startGameButtonSelector != null)
					_summeryHolder.removeChild(_startGameButtonSelector);
				_startGameButtonSelector = new BW_Selector(3.05, 1.05);
				_summeryHolder.addChild(_startGameButtonSelector);
				_summeryHolder.setChildIndex(_startButton, numChildren-1);
				_startGameButtonSelector.x = _startButton.x;
				_startGameButtonSelector.y = _startButton.y;
				_startGameButtonSelector.visible = false;
				
				_summeryHolder.y = PathogenTest.hTenPercent * 1.2;
				
				//Hide all other screens.
				if(_playerHolder)
					_playerHolder.visible = false;
				if(_mapHolder)
					_mapHolder.visible = false;
				if(modePicker)
					modePicker.visible = false;
				
				_tab.x = _summeryButton.x;
				_nextButton.visible = false;
				_nextButtonSelector.visible = false;
				_previousButton.visible = true;
				
				_previousButton.removeEventListener(TouchEvent.TOUCH, openPlayerScreen);
				_previousButton.addEventListener(TouchEvent.TOUCH, openMapScreen);
				
				checkReady();
			}
		}
		
		public function updateGameOptionsText():void
		{
			//Update text
			if(sharedDataObject.data.GameLength != null)
			{
				var gameRounds:int = sharedDataObject.data.GameLength;
				
				if(gameRounds == 100)
					_gameLength.text = "Unlimited";
				else
					_gameLength.text = gameRounds.toString() + " Rounds";
			}
			else
				_gameLength.text = "Unlimited";
			
			//Update text
			if(sharedDataObject.data.TurnLength != null)
			{
				var turnNumber:int = sharedDataObject.data.TurnLength;
				
				if(turnNumber == 30)
					_turnLength.text = "Unlimited";
				else
					_turnLength.text = turnNumber.toString() + " Sec's";
			}
			else
				_turnLength.text = "Unlimited";
		}
		public function refreshGameSetup():void
		{
			if(_advancedOptions)
				_advancedOptions.alpha = 1;
			
			if(_mapPreviewArray.length > 0)
			{
				for each (var mapPreview:MapPreview in _mapPreviewArray)
				{
					mapPreview.alpha = 1;
				}
			}
		}
		override public function death():void
		{
			//Garbage Collection
			for each (var playerPicker:PlayerPicker in playerPickerArray)
			{
				playerPicker.garbageCollection();
				playerPicker.dispose();
				removeChild(playerPicker);
				playerPicker = null;
			}
			
			playerPickerArray = [];
			_playerHolder.dispose();
			removeChild(_playerHolder);
			_playerHolder = null;
			
			if(_mapHolder)
			{
				sizePicker.garbageCollection();
				sizePicker.dispose();
				_mapHolder.removeChild(sizePicker);
				sizePicker = null;
				
				for each(var mapPreview:MapPreview in _mapPreviewArray)
				{
					mapPreview.garbageCollection();
					mapPreview.dispose();
					container.removeChild(mapPreview);
					mapPreview = null;
				}
				
				_mapPreviewArray = [];
				
				for each (var map_Preview_Tutorial:TutorialPreview in _mapPreviewTutorialArray)
				{
					map_Preview_Tutorial.dispose();
					container.removeChild(map_Preview_Tutorial);
					map_Preview_Tutorial = null; 
				}
				
				_mapPreviewTutorialArray = [];
				
				if(_mapSelector)
				{
					_mapSelector.destroy();
					_mapSelector.dispose();
					container.removeChild(_mapSelector);
					_mapSelector = null;
				}
				
				container.layout = null;
				container.dispose();
				_mapHolder.removeChild(container);
				container = null;
				
				_mapHolder.dispose();
				removeChild(_mapHolder);
				_mapHolder = null;
			}
			if (_difficultySelector)
			{
				_difficultySelector.death();
				_difficultySelector.dispose();
				removeChild(_difficultySelector);
				_difficultySelector = null;
			}
			if(_summeryHolder)
			{
				_summeryMapPreview.garbageCollection();
				_summeryMapPreview.dispose();
				_summeryMapPreview.removeChild(_summeryMapPreview);
				_summeryMapPreview = null;
				
				_summeryHolder.dispose();
				removeChild(_summeryHolder);
				_summeryHolder = null;
			}
			
			if(_advancedOptions)
			{
				_advancedOptions.destroy();
				_advancedOptions.dispose();
				removeChild(_advancedOptions);
				_advancedOptions = null;
			}
			
			//bottom UI Buttons.
			if (_backButton)
			{
				_backButton.destroy();
				removeChild(_backButton);
				_backButton = null;
			}
			
			if (_playersButton)
			{
				_playersButton.destroy();
				removeChild(_playersButton);
				_playersButton = null;
			}
			
			if (_mapsButton)
			{
				_mapsButton.destroy();
				removeChild(_mapsButton);
				_mapsButton = null;
			}
			
			if (_summeryButton)
			{
				_summeryButton.destroy();
				removeChild(_summeryButton);
				_summeryButton = null;
			}
			
			if(_difficultySelector)
			{
				_difficultySelector.death();
				_difficultySelector.dispose();
				removeChild(_difficultySelector);
				_difficultySelector = null;
			}
			
			if(_nextButtonSelector)
			{
				_nextButtonSelector.dispose();
				removeChild(_nextButtonSelector);
				_nextButtonSelector = null;
			}
			
			if(_startGameButtonSelector)
			{
				_startGameButtonSelector.dispose();
				removeChild(_startGameButtonSelector);
				_startGameButtonSelector = null;
			}
			if(_loader)
			{
				_loader.dispose();
				removeChild(_loader);
				_loader = null;
			}
			if(chosenMap)
			{
				chosenMap.garbageCollection();
				chosenMap.dispose();
				chosenMap = null;
			}
			
			_main.removeGameSetup();
			_main = null;
		}
		
		public function  get difficultySelector():DifficultySelector { return _difficultySelector };
		public function  get main():Main {return _main};
	}
}