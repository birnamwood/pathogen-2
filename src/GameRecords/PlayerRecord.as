package GameRecords
{
	public class PlayerRecord
	{
		private var _gamerID:String;
		private var _nickname:String;
		private var _hasPathogen:Boolean;
		
		public function PlayerRecord(gamerID:String, nickname:String)
		{
			_gamerID = gamerID;
			_nickname = nickname;
		}
		
		public function get gamerID():String { return _gamerID; }
		public function get nickname():String { return _nickname; }
		public function get hasPathogen():Boolean { return _hasPathogen; }
		
		public function set gamerID(gamerID:String):void { _gamerID = gamerID; }
		public function set nickname(nickname:String):void { _nickname = nickname; }
		public function set hasPathogen(hasPathogen:Boolean):void { _hasPathogen = hasPathogen; }
	}
}