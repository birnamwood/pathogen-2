package UI
{
	import Campaign.Campaign;
	
	import Core.GameManager;
	import Core.VictoryScreen;
	
	import Editor.MapEditor;
	
	import OnlinePlay.OnlineGameManager;
	import OnlinePlay.OnlineVictoryScreen;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class AchievementPopUp extends Sprite
	{
		//Scale Information
		private var scale9Num:Number = 20/PathogenTest.scaleFactor;
		private const texture:Texture = Main.assets.getTexture("scale9_White");
		private const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
		
		//Display
		protected var _background:Scale9Image;
		private var _title:TextField;
		private var _text:TextField;
		private var _image:Image;
		private var _achieveMentNumber:int;
		
		//Classes
		private var _campaign:Campaign;
		private var _gameManager:GameManager;
		private var _onlineGameManager:OnlineGameManager;
		private var _mapEditor:MapEditor;
		private var _victoryScreen:VictoryScreen;
		private var _onlineVictoryScreen:OnlineVictoryScreen;
		
		public function AchievementPopUp()
		{
			super();
		}
		public function init(achievementNumber:int):void
		{
			_achieveMentNumber = achievementNumber;
			
			createDisplay();
		}
		private function createDisplay():void
		{
			//Creating _background so that content stands out.
			_background = new Scale9Image(textures);
			addChild(_background);
			_background.width = PathogenTest.wTenPercent * 6;
			_background.height = PathogenTest.hTenPercent * 2;
			//_background.color = 0xffffff;
			_background.color = 0x222245;
			
			_image = new Image(Main.assets.getTexture("Icon"));
			addChild(_image);
			_image.pivotX = _image.width/2;
			_image.pivotY = _image.height/2;
			_image.x = _image.width * .75;
			_image.y = _background.height/2;
			
			var titleFontSize:Number = PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor;
			var textFontSize:Number = PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor;
			
			_title = new TextField(_background.width * .7, PathogenTest.hTenPercent * .6, "Title", "Questrial", titleFontSize, Color.WHITE);
			addChild(_title);
			_title.hAlign = HAlign.LEFT;
			_title.vAlign = VAlign.TOP;
			_title.x = _image.x + (_image.width * .6)
			_title.y = _image.y - _image.height/2;
			
			_text = new TextField(_background.width * .7, PathogenTest.hTenPercent, "This is Description Text", "Dekar", textFontSize, Color.WHITE);
			addChild(_text);
			_text.hAlign = HAlign.LEFT;
			_text.vAlign = VAlign.TOP;
			_text.x = _title.x;
			_text.y = _title.y + _title.height;
			
			textUpdate();
		}
		private function textUpdate():void
		{
			var NOT_COMPLETE:Number = .5;
			var COMPLETED:int = 1;
			
			var title:String;
			var description:String;
			var alphaLevel:Number = NOT_COMPLETE;
			
			
			
			switch(_achieveMentNumber)
			{
				case 0:
				{
					title = "Knowledge is Power";
					description = "Complete the Tutorial";
					if(Main.achievements.progress_Complete_Tutorial == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 1:
				{
					title = "Outmanned";
					description = "Complete all Outmanned Levels on Easy.";
					if(Main.achievements.progress_5_1_EASY == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 2:
				{
					title = "Outgunned";
					description = "Complete all Outmanned Levels on Medium.";
					if(Main.achievements.progress_5_1_MEDIUM == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 3:
				{
					title = "Outclassed";
					description = "Complete all Outmanned Levels on Hard.";
					if(Main.achievements.progress_5_1_HARD == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 4:
				{
					title = "Captured that Zone";
					description = "Complete all Capture Zone levels on Easy.";
					if(Main.achievements.progress_5_2_EASY == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 5:
				{
					title = "Cap'ed";
					description = "Complete all Capture Zone levels on Medium.";
					if(Main.achievements.progress_5_2_MEDIUM == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 6:
				{
					title = "King of the Hill";
					description = "Complete all Capture Zone levels on Hard.";
					if(Main.achievements.progress_5_2_HARD == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 7:
				{
					title = "Infectious";
					description = "Complete all Infection Levels on Easy.";
					if(Main.achievements.progress_6_1_EASY == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 8:
				{
					title = "Cancerous";
					description = "Complete all Infection Levels on Medium.";
					if(Main.achievements.progress_6_1_MEDIUM == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 9:
				{
					title = "Pandemic";
					description = "Complete all Infection Levels on Hard.";
					if(Main.achievements.progress_6_1_HARD == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 10:
				{
					title = "Erosive";
					description = "Complete all Erosion Zone Levels on Easy.";
					if(Main.achievements.progress_6_2_EASY == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 11:
				{
					title = "Corrosive";
					description = "Complete all Erosion Zone Levels on Medium.";
					if(Main.achievements.progress_6_2_MEDIUM == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 12:
				{
					title = "Landslide";
					description = "Complete all Erosion Zone Levels on Hard.";
					if(Main.achievements.progress_6_2_HARD == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 13:
				{
					title = "Underdog";
					description = "Complete all Underdog Levels on Easy.";
					if(Main.achievements.progress_7_1_EASY == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 14:
				{
					title = "Dark Horse";
					description = "Complete all Underdog Levels on Medium.";
					if(Main.achievements.progress_7_1_MEDIUM == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 15:
				{
					title = "David vs. Goliath";
					description = "Complete all Underdog Levels on Hard.";
					if(Main.achievements.progress_7_1_HARD == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 16:
				{
					title = "Flash";
					description = "Complete all Lightning Levels on Easy.";
					if(Main.achievements.progress_7_2_EASY == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 17:
				{
					title = "Bolt";
					description = "Complete all Lightning Levels on Medium.";
					if(Main.achievements.progress_7_2_MEDIUM == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 18:
				{
					title = "Storm";
					description = "Complete all Lightning Levels on Hard.";
					if(Main.achievements.progress_7_2_HARD == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 19:
				{
					title = "Virus";
					description = "Complete the final level Level on Easy.";
					if(Main.achievements.progress_8_EASY == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 20:
				{
					title = "Biological Agent";
					description = "Complete the final level Level on Medium.";
					if(Main.achievements.progress_8_MEDIUM == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 21:
				{
					title = "Pathogen";
					description = "Complete the final level Level on Hard.";
					if(Main.achievements.progress_8_HARD == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 22:
				{
					title = "Subject Zero";
					description = "Start an online game.";
					if(Main.achievements.progress_Play_Online_Custom_1 == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 23:
				{
					title = "Intern";
					description = "Create a map in the editor.";
					if(Main.achievements.progress_Create_Map_1 == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 24:
				{
					title = "Equal & Opposite";
					description = "End a game in a Tie.";
					if(Main.achievements.progress_Tie_Game == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 25:
				{
					title = "Apex Predator";
					description = "Win a game through Domination.";
					if(Main.achievements.progress_Domination_Game == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 26:
				{
					title = "Bacterial Culture";
					description = "Create 100 Cells.";
					if(Main.achievements.progress_Create_100_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 27:
				{
					title = "Colony";
					description = "Create 1,000 Cells.";
					if(Main.achievements.progress_Create_1000_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 28:
				{
					title = "Minor Case";
					description = "Create 5,000 Cells.";
					if(Main.achievements.progress_Create_5000_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 29:
				{
					title = "Contagious";
					description = "Create 25,000 Cells.";
					if(Main.achievements.progress_Create_25000_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 30:
				{
					title = "Disease";
					description = "Create 50,000 Cells.";
					if(Main.achievements.progress_Create_50000_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 31:
				{
					title = "Epidemic";
					description = "Create 100,000 Cells.";
					if(Main.achievements.progress_Create_100000_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 32:
				{
					title = "Harmful";
					description = "Destroy 100 Cells.";
					if(Main.achievements.progress_Destroy_100_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 33:
				{
					title = "Damaging";
					description = "Destroy 500 Cells.";
					if(Main.achievements.progress_Destroy_500_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 34:
				{
					title = "Destructive";
					description = "Destroy 1,000 Cells.";
					if(Main.achievements.progress_Destroy_1000_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 35:
				{
					title = "Virulent";
					description = "Destroy 2,500 Cells.";
					if(Main.achievements.progress_Destroy_2500_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
					
				case 36:
				{
					title = "Deadly";
					description = "Destroy 5,000 Cells.";
					if(Main.achievements.progress_Destroy_5000_Cells == Achievements.COMPLETE)
						alphaLevel = COMPLETED;
					break;
				}
			}
			
			if(alphaLevel == NOT_COMPLETE)
			{
				var quad:Quad = new Quad(_image.width, _image.height, 0x322469);
				addChild(quad);
				quad.pivotX = quad.width/2;
				quad.pivotY = quad.height/2;
				quad.x = _image.x;
				quad.y = _image.y;
				_image.visible = false;
			}
			
			_title.text = title;
			_text.text = description;
			alpha = alphaLevel;
		}
		public function start():void
		{
			var tween:Tween = new Tween(this, 2, Transitions.EASE_OUT);
			tween.moveTo(PathogenTest.stageCenter.x, PathogenTest.stageHeight-height);
			Starling.juggler.add(tween);
			tween.onComplete = pause;
		}
		private function pause():void
		{
			Starling.juggler.delayCall(fadeOut, 2);	
		}
		private function fadeOut():void
		{
			var tween:Tween = new Tween(this, 1);
			tween.fadeTo(0)
			Starling.juggler.add(tween);
			tween.onComplete = death;
		}
		private function death():void
		{
			if(_campaign)
			{
				_campaign.removeAchievementPopUp(this);
				_campaign = null;
			}
			
			if(_gameManager)
			{
				_gameManager.removeAchievementPopUp(this);
				_gameManager = null;
			}
			
			if(_onlineGameManager)
			{
				_onlineGameManager.removeAchievementPopUp(this);
				_onlineGameManager = null;
			}
			
			if(_mapEditor)
			{
				_mapEditor.removeAchievementPopUp(this);
				_mapEditor = null;
			}
			
			if(_victoryScreen)
			{
				_victoryScreen.removeAchievementPopUp(this);
				_victoryScreen = null;
			}
			
			if(_onlineVictoryScreen)
			{
				_onlineVictoryScreen.removeAchievementPopUp(this);
				_onlineVictoryScreen = null;
			}
		}
		
		//Classes
		public function get campaign():Campaign {return _campaign};
		public function get gameManager():GameManager {return _gameManager};
		public function get onlineGameManager():OnlineGameManager {return _onlineGameManager};
		public function get mapEditor():MapEditor {return _mapEditor};
		public function get victoryScreen():VictoryScreen {return _victoryScreen};
		public function get onlineVictoryScreen():OnlineVictoryScreen {return _onlineVictoryScreen};
		
		public function set campaign(campaign:Campaign):void {_campaign = campaign};
		public function set gameManager(gameManager:GameManager):void {_gameManager = gameManager};
		public function set onlineGameManager(onlinegameManager:OnlineGameManager):void {_onlineGameManager = onlinegameManager};
		public function set mapEditor(mapEditor:MapEditor):void {_mapEditor = mapEditor};
		public function set victoryScreen(victoryScreen:VictoryScreen):void {_victoryScreen = victoryScreen};
		public function set onlineVictoryScreen(onlineVictoryScreen:OnlineVictoryScreen):void {_onlineVictoryScreen = onlineVictoryScreen};
	}
}