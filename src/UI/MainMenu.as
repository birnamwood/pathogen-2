package UI
{
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import com.milkmangames.nativeextensions.GoogleGames;
	import com.milkmangames.nativeextensions.ios.StoreKit;
	import com.milkmangames.nativeextensions.ios.StoreKitProduct;
	import com.milkmangames.nativeextensions.ios.events.StoreKitErrorEvent;
	import com.milkmangames.nativeextensions.ios.events.StoreKitEvent;
	import com.sticksports.nativeExtensions.flurry.Flurry;
	import com.sticksports.nativeExtensions.gameCenter.GameCenter;
	
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.utils.Color;
	
	public class MainMenu extends BW_UI
	{
		// rate this app vars
		private static const APPLE_APP_ID:String= "123456789";
		private static const PLAY_APP_ID:String= "air.com.domain.mygame";
		private static const PLAY_STORE_BASE_URI:String= "market://details?id=";
		private static const PLAY_REVIEW:String= "&reviewId=0";
		private static const APP_STORE_BASE_URI:String= "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?onlyLatestVersion=false&type=Purple+Software&id=";
		
		//Parent Class
		private var _main:Main;
		
		private var _UIBand:Quad;
		
		//Primary buttons
		private var _SingleplayerButton:BW_Button;
		private var _MultiplayerButton:BW_Button;
		private var _mapButton:BW_Button;
		private var _unlockButton:Image;
		
		private var _multiplayerButtonHolder:Sprite;
		private var _singlePlayerButtonHolder:Sprite;
		
		//Secondary
		private var _gameCenterButton:Button;
		private var _optionsButton:BW_Button;
		private var _rateButton:BW_Button;
		private var _achievementsButton:Button;
		
		//Mulitplayer Buttons
		private var _localPlay:BW_Button;
		private var _onlinePlay:BW_Button;
		
		//Singleplayer Buttons
		private var _TutorialButton:BW_Button;
		private var _CampaignButton:BW_Button;
		
		private var _titleScreen:Title;
		
		private var _onlineUnavailable:OnlineUnavailable;
		
		//iPhone Vars
		private var _buttonHeight:Number;
		
		public function MainMenu(main:Main)
		{
			super();
			
			_main = main;
			
			//initStore();
			
			_titleScreen = new Title(new Point(PathogenTest.stageCenter.x, PathogenTest.stageCenter.y));
			addChild(_titleScreen);
			
			if(PathogenTest.stageHeight < 750)
			{
				_titleScreen.scaleX = .8;
				_titleScreen.scaleY = .8;
			}
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_UIBand = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1.8, Color.BLACK);
			else
				_UIBand = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1.5, Color.BLACK);
			
			_UIBand.pivotX = _UIBand.width/2;
			_UIBand.pivotY = _UIBand.height;
			addChild(_UIBand);
			_UIBand.x = PathogenTest.stageCenter.x;
			_UIBand.y = PathogenTest.stageHeight;
			_UIBand.alpha = .5;
			
			/*_SingleplayerButton = new Utils.BW_Button(2, .75, "Singleplayer", 32);
			addChild(_SingleplayerButton);
			_SingleplayerButton.x = PathogenTest.wTenPercent * 2.5;
			_SingleplayerButton.y = PathogenTest.stageHeight - (PathogenTest.hTenPercent * .8);
			_SingleplayerButton.addEventListener(TouchEvent.TOUCH, openSingleplayer);*/
			
			if (Main.isPhone())
				_buttonHeight = 1;
			else
				_buttonHeight = .75;
			
			_CampaignButton = new BW_Button(2, _buttonHeight, "Single Player", 32);
			addChild(_CampaignButton);
			_CampaignButton.x = PathogenTest.wTenPercent * 2.5;
			_CampaignButton.y = _UIBand.y - _UIBand.height/2;
			_CampaignButton.addEventListener(TouchEvent.TOUCH, openCampaign);
			
			_MultiplayerButton = new Utils.BW_Button(2, _buttonHeight, "Multiplayer", 32);
			addChild(_MultiplayerButton);
			_MultiplayerButton.x = PathogenTest.wTenPercent * 5;
			_MultiplayerButton.y = _CampaignButton.y;
			_MultiplayerButton.addEventListener(TouchEvent.TOUCH, openMultiplayer);
			
			_mapButton = new Utils.BW_Button(2, _buttonHeight, "Editor", 32);
			addChild(_mapButton);
			_mapButton.x = PathogenTest.wTenPercent * 7.5;
			_mapButton.y = _CampaignButton.y
			_mapButton.addEventListener(TouchEvent.TOUCH, openMaps);
			if(Main._lite && Main._locked)
				_mapButton.alpha = .5;
			
			if(Main._lite && Main._locked)
			{
				//Create Unlock Button
				_unlockButton = new Image(Main.assets.getTexture("UnlockCorner"));
				addChild(_unlockButton);
				_unlockButton.pivotX = _unlockButton.width;
				_unlockButton.x = PathogenTest.stageWidth;
				_unlockButton.addEventListener(TouchEvent.TOUCH, onTouchUnlock);
			}

			if (!Main.isPhone() && Main.isIOS())
			//if(PathogenTest.device != PathogenTest.iPHONE_4 && PathogenTest.device != PathogenTest.iPHONE_5)
			{
				_gameCenterButton = new Button(Main.assets.getTexture("Pathogen_BTN_GameCenter"));
				addChild(_gameCenterButton);
				_gameCenterButton.pivotX = _gameCenterButton.width/2;
				_gameCenterButton.pivotY = _gameCenterButton.height/2;
				_gameCenterButton.x = PathogenTest.wTenPercent * .5;
				_gameCenterButton.y = _CampaignButton.y;
				_gameCenterButton.scaleX = .95;
				_gameCenterButton.scaleY = .95;
				_gameCenterButton.addEventListener(TouchEvent.TOUCH, openGameCenter);
			}
			
			//Create Options Button
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_optionsButton = new BW_Button(.75,1, "", 0);
			else
				_optionsButton = new BW_Button(.5,.7, "", 0);
			
			_optionsButton.addImage(Main.assets.getTexture("Pathogen_BTN_Options"), Main.assets.getTexture("Pathogen_BTN_Down"));
			addChild(_optionsButton);
			_optionsButton.x = PathogenTest.wTenPercent * 9.5;
			_optionsButton.y = _CampaignButton.y;
			_optionsButton.addEventListener(TouchEvent.TOUCH, openMainOptions);
			
			if (Main.isAndroid())
			{
				_achievementsButton = new Button(Main.assets.getTexture("Pathogen_BTN_GoogleGames"));
				addChild(_achievementsButton);
				_achievementsButton.pivotX = _achievementsButton.width/2;
				_achievementsButton.pivotY = _achievementsButton.height/2;
				_achievementsButton.x = PathogenTest.wTenPercent * .5;
				_achievementsButton.y = _CampaignButton.y;
				_achievementsButton.scaleX = .95;
				_achievementsButton.scaleY = .95;
				_achievementsButton.addEventListener(TouchEvent.TOUCH, onTouchAchievements);
			}
			
			if(Flurry.isSupported)
				Flurry.logEvent("Main Menu");
		}
		private function onTouchUnlock($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this,TouchPhase.ENDED);
			if (touch)
			{
				_main.openContentLockedScreen();
			}
		}
		private function openSingleplayer($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(_singlePlayerButtonHolder == null)
				{
					//Open the singleplayer tab
					_singlePlayerButtonHolder = new Sprite();
					addChild(_singlePlayerButtonHolder);
					
					var popUpBand:Quad = new Quad(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent * 2, Color.BLACK);
					popUpBand.pivotX = popUpBand.width/2;
					popUpBand.pivotY = popUpBand.height;
					_singlePlayerButtonHolder.addChild(popUpBand);
					popUpBand.x = _SingleplayerButton.x;
					popUpBand.y = _UIBand.y - _UIBand.height;
					popUpBand.alpha = .5;
					
					_CampaignButton = new BW_Button(2, .75, "Campaign", 32);
					_singlePlayerButtonHolder.addChild(_CampaignButton);
					_CampaignButton.x = _SingleplayerButton.x;
					_CampaignButton.y = _SingleplayerButton.y - (PathogenTest.hTenPercent * 1);
					_CampaignButton.addEventListener(TouchEvent.TOUCH, openCampaign);
					
					_TutorialButton = new BW_Button(2, .75, "Tutorial", 32);
					_singlePlayerButtonHolder.addChild(_TutorialButton);
					_TutorialButton.x = _SingleplayerButton.x;
					_TutorialButton.y = _SingleplayerButton.y - (PathogenTest.hTenPercent * 2);
					_TutorialButton.addEventListener(TouchEvent.TOUCH, openTutorial);
					
					removeMultiplayer();
				}
				else
					removeSingleplayer();
			}
		}
		private function removeSingleplayer():void
		{
			if(_singlePlayerButtonHolder)
			{
				if(_TutorialButton)
				{
					_TutorialButton.destroy();
					_TutorialButton.dispose();
					_singlePlayerButtonHolder.removeChild(_TutorialButton);
					_TutorialButton = null;
				}
				if(_CampaignButton)
				{
					_CampaignButton.destroy();
					_CampaignButton.dispose();
					_singlePlayerButtonHolder.removeChild(_CampaignButton);
					_CampaignButton = null;
				}
				
				_singlePlayerButtonHolder.dispose();
				removeChild(_singlePlayerButtonHolder);
				_singlePlayerButtonHolder = null;
			}
		}
		private function openCampaign($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				removeListeners();
				_main.openCampaign();
				startFadeOut();
			}
		}
		private function openTutorial($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				removeListeners();
				_main.openTutorial();
				startFadeOut();
			}
		}
		private function openMultiplayer($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if (Main.isKindle() || Main.isNook())
					openLocalPlay($e);
				else
				{
					if(_multiplayerButtonHolder == null)
					{
						//Open the multiplayer tab
						_multiplayerButtonHolder = new Sprite();
						addChild(_multiplayerButtonHolder);
						
						var popUpBand:Quad;
						
						if (Main.isPhone())
						//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
							popUpBand = new Quad(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent * 2.7, Color.BLACK);
						else
							popUpBand = new Quad(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent * 2, Color.BLACK);
						
						popUpBand.pivotX = popUpBand.width/2;
						popUpBand.pivotY = popUpBand.height;
						_multiplayerButtonHolder.addChild(popUpBand);
						popUpBand.x = _MultiplayerButton.x;
						popUpBand.y = _UIBand.y - _UIBand.height;
						popUpBand.alpha = .5;
						
						_onlinePlay = new Utils.BW_Button(2, _buttonHeight, "Online Play", 32);
						_multiplayerButtonHolder.addChild(_onlinePlay);
						_onlinePlay.x = _MultiplayerButton.x;
						_onlinePlay.y = _MultiplayerButton.y  - (_onlinePlay.height * 1.4);
						_onlinePlay.addEventListener(TouchEvent.TOUCH, openOnlinePlay);
						if(Main._lite && Main._locked)
							_onlinePlay.alpha = .5;
						
						_localPlay = new Utils.BW_Button(2, _buttonHeight, "Local Play", 32);
						_multiplayerButtonHolder.addChild(_localPlay);
						_localPlay.x = _MultiplayerButton.x;
						_localPlay.y = _onlinePlay.y - (_localPlay.height * 1.4);
						_localPlay.addEventListener(TouchEvent.TOUCH, openLocalPlay);
						
						removeSingleplayer();
					}
					else
						removeMultiplayer();
				}
			}
		}
		private function removeMultiplayer():void
		{
			if(_multiplayerButtonHolder)
			{
				if(_localPlay)
				{
					_localPlay.destroy();
					_localPlay.dispose();
					_multiplayerButtonHolder.removeChild(_localPlay);
					_localPlay = null;
				}
				if(_onlinePlay)
				{
					_onlinePlay.destroy();
					_onlinePlay.dispose();
					_multiplayerButtonHolder.removeChild(_onlinePlay);
					_onlinePlay = null;
				}
				
				_multiplayerButtonHolder.dispose();
				removeChild(_multiplayerButtonHolder);
				_multiplayerButtonHolder = null;
			}
		}
		private function openMaps($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				if(Main._lite && Main._locked)
					_main.openContentLockedScreen();
				else
				{
					removeListeners();
					_main.openMapEditor()
					startFadeOut();
				}
			}
		}
		//Multiplayer Buttons Pressed
		private function openLocalPlay($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				removeListeners();
				_main.openLocalPlay()
				startFadeOut();
			}
		}
		private function openOnlinePlay($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				if (Main.isKindle())
				{
					addOnlineUnavailable()
				}
				else
				{
					if(Main._lite && Main._locked)
						_main.openContentLockedScreen();
					else
					{
						//Main.updateDebugText("openOnlinePlay");
						
						if (!Main.sInternet)
							_main.checkInternetConnectivity(true);
						else
							launchOnlinePlay();
					}
				}
			}
		}
		public function launchOnlinePlay():void
		{
			Main.updateDebugText("launch online play");
			
			var onlinePlayReady:Boolean = false;
			if (_main.entityManager)
			{
				Main.updateDebugText("found entity manager");
				
				if (_main.entityManager.currentPlayer && Main.sInternet)
				{
					if (PathogenTest.device != PathogenTest.PC && !PathogenTest.IGF_Build)
					{
						Main.updateDebugText("Current player set and internet is good");
						
						if(Main.isIOS())
						{
							Main.updateDebugText("isIOS - GameCenter autheticated = " + GameCenter.isAuthenticated);
							
							if (GameCenter.isAuthenticated || PathogenTest.IGF_Build)
								onlinePlayReady = true;
							else
								_main.setupGameCenter();
						}
						else if(Main.isAndroid())
						{
							//Main.updateDebugText("is Android");
							
							if (Main.sFloxAltAuthentication)
							{
								if (Main.sOnlineID != null && Main.sOnlineNickname != null)
									onlinePlayReady = true;
							}
							else
							{
								if(GoogleGames.games.isSignedIn())
									onlinePlayReady = true;
								else
									_main.setupGooglePlay();
							}
						}
					}
					else 
						onlinePlayReady = true;
				}
			}
			
			//Main.updateDebugText("onlineplayready = " + onlinePlayReady);
			
			if (!onlinePlayReady)
			{
				//Main.updateDebugText("online play not ready");
				
				//_onlineUnavailable = new OnlineUnavailable(this);
				//addChild(_onlineUnavailable);
				
				addOnlineUnavailable();
				
				//_main.openOnlinePlay()
				//startFadeOut();
			}
			else
			{
				_main.openOnlinePlay()
				startFadeOut();
			}
		}
		public function addOnlineUnavailable():void
		{
			_onlineUnavailable = new OnlineUnavailable(this);
			addChild(_onlineUnavailable);
			_onlineUnavailable.x = PathogenTest.stageCenter.x;
			_onlineUnavailable.y = PathogenTest.stageCenter.y;
		}
		
		private function openMainOptions($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				_main.openMainOptions();
			}
		}
		private function onTouchAchievements($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				_main.addAchievementScreen();
			}
		}
		private function openGameCenter($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				_main.openGameCenter();
			}
		}
		private function removeListeners():void
		{
			//_SingleplayerButton.removeEventListener(TouchEvent.TOUCH, openSingleplayer);
			_MultiplayerButton.removeEventListener(TouchEvent.TOUCH, openMultiplayer);
			_mapButton.removeEventListener(TouchEvent.TOUCH, openMaps);
			
			if(_gameCenterButton)
				_gameCenterButton.removeEventListener(TouchEvent.TOUCH, openGameCenter);
			
			if(_onlinePlay)
			{
				_localPlay.removeEventListener(TouchEvent.TOUCH, openLocalPlay);
				_onlinePlay.addEventListener(TouchEvent.TOUCH, openOnlinePlay);
			}
			
			if(_TutorialButton)
			{
				_CampaignButton.removeEventListener(TouchEvent.TOUCH, openCampaign);
				_TutorialButton.removeEventListener(TouchEvent.TOUCH, openTutorial);
			}
			
			if(_unlockButton)
				_unlockButton.addEventListener(TouchEvent.TOUCH, onTouchUnlock);
		}
		public function removeOnlineUnavailable():void
		{
			_onlineUnavailable.dispose();
			removeChild(_onlineUnavailable);
			_onlineUnavailable = null;
		}
		public function refreshMainMenu():void
		{
			if(_mapButton)
				_mapButton.alpha = 1;
			
			if(_onlinePlay)
				_onlinePlay.alpha = 1;
			
			if(_unlockButton)
				_unlockButton.visible = false;
		}
		override public function death():void
		{
			//Main.updateDebugText("Main Menu death");
			
			_titleScreen.destroy();
			_titleScreen.dispose();
			removeChild(_titleScreen);
			_titleScreen = null;
			
			/*_SingleplayerButton.destroy();
			_SingleplayerButton.dispose();
			removeChild(_SingleplayerButton);
			_SingleplayerButton = null;*/
			
			_CampaignButton.destroy();
			_CampaignButton.dispose();
			removeChild(_CampaignButton);
			_CampaignButton = null;
			
			_MultiplayerButton.destroy();
			_MultiplayerButton.dispose();
			removeChild(_MultiplayerButton);
			_MultiplayerButton = null;
			
			if(_gameCenterButton)
			{
				_gameCenterButton.dispose();
				removeChild(_gameCenterButton);
				_gameCenterButton = null;
			}
			
			_mapButton.destroy();
			_mapButton.dispose();
			removeChild(_mapButton);
			_mapButton = null;
			
			removeMultiplayer();
			removeSingleplayer();
			
			_optionsButton.destroy();
			_optionsButton.dispose();
			removeChild(_optionsButton);
			_optionsButton = null;
			
			if (_achievementsButton)
			{
				_achievementsButton.dispose();
				removeChild(_achievementsButton);
				_achievementsButton = null;
			}
			
			_main.closeMainMenu();
			_main = null;
			
			if(_unlockButton)
			{
				_unlockButton.dispose();
				removeChild(_unlockButton);
				_unlockButton = null;
			}
		}
		
		public function get main():Main {return _main};
	}
}