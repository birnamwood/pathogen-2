package UI
{
	import Utils.BW_UI;
	
	import bg.BackgroundManager;
	
	import flash.geom.Point;
	
	import flashx.textLayout.elements.OverflowPolicy;
	import flashx.textLayout.operations.MoveChildrenOperation;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;

	public class Title extends Sprite
	{
		//Static Vars
		private static const FADE_IN:int = 0;
		private static const LIVING:int = 1;
		private static const FADE_OUT:int = 3;
		
		private static const FADE_SPEED:Number = .02;
		
		//Parent Class
		private var _main:Main;
		
		//Movement Vars
		public var _wanderingDistanceX:int = 50;
		public var _wanderingDistanceY:int = 50;
		private var _destination:Point;
		public var _movementSpeed:Number = .1;
		private var _randomX:int;
		private var _randomY:int;
		private var _location:Point;
		
		private var _display:Sprite;
		
		private var _letterArray:Array;
		private var _symbol:Image;
		
		public function Title(location:Point)
		{
			super();
			
			_location = location;
			x = _location.x;
			y = _location.y;
			
			_display = new Sprite();
			addChild(_display);
			
			var cell:Image;
			
			if(PathogenTest.HD_Multiplyer == 2)
				cell = new Image(Main.assets.getTexture("HD_Pathogen_Bubble"));
			else
				cell = new Image(Main.assets.getTexture("Pathogen_Bubble"));
			
			cell.pivotX = cell.width/2;
			cell.pivotY = cell.height/2;
			_display.addChild(cell);
			
			//Create Letters
			var p:Image = new Image(Main.assets.getTexture("Title_P"));
			var a:Image = new Image(Main.assets.getTexture("Title_A"));
			var t:Image = new Image(Main.assets.getTexture("Title_T"));
			var h:Image = new Image(Main.assets.getTexture("Title_H"));
			_symbol = new Image(Main.assets.getTexture("Title_Symbol"));
			var g:Image = new Image(Main.assets.getTexture("Title_G"));
			var e:Image = new Image(Main.assets.getTexture("Title_E"));
			var n:Image = new Image(Main.assets.getTexture("Title_n"));
			
			_letterArray = [p,a,t,h,_symbol,g,e,n];
			
			var letterHolder:Sprite = new Sprite();
			_display.addChild(letterHolder);
			
			for (var i:int = 0; i < _letterArray.length; i++)
			{
				var letter:Image = _letterArray[i];
				letterHolder.addChild(letter);
				letter.alpha = 0;
				
				var gap:Number = PathogenTest.wTenPercent * .1;
				
				if(i!=0)
					letter.x = _letterArray[i-1].x + _letterArray[i-1].width + gap;
				
				if(i == 1)
					letter.x -= gap*2;
				
				if(i == 2)
					letter.x -= gap*2;
				
				if(i == 4)
					letter.y = -letter.height/4;
			}
			
			_letterArray.splice(4,1);
			
			Starling.juggler.delayCall(pickLetter, .4);
			
			letterHolder.pivotX = letterHolder.width/2;
			letterHolder.pivotY = letterHolder.height/2;
			
			letterHolder.y = PathogenTest.hTenPercent * .25;
			
			pickADestination();
			addEventListener(Event.ENTER_FRAME, update);
			
			touchable = false;
		}
		private function pickLetter():void
		{
			var random:int = Math.round(Math.random() * (_letterArray.length -1));
			var randomLetter:Image = _letterArray[random];
			_letterArray.splice(random,1);
			
			var tween:Tween = new Tween(randomLetter, .6);
			tween.fadeTo(1);
			Starling.juggler.add(tween);
			
			if(_letterArray.length > 0)
				Starling.juggler.delayCall(pickLetter, .4);
			else
				Starling.juggler.delayCall(addSymbol, .6);
		}
		private function addSymbol():void
		{
			var finalTween:Tween = new Tween(_symbol, .6);
			finalTween.fadeTo(1);
			finalTween.onComplete = animationsComplete;
			Starling.juggler.add(finalTween);
		}
		private function animationsComplete():void
		{
			flatten();
		}
		private function pickADestination():void
		{
			//Pick a random X and Y
			_randomX = (_location.x - (_wanderingDistanceX /2)) + Math.round(Math.random() * _wanderingDistanceX);
			_randomY = (_location.y - (_wanderingDistanceY / 2)) + Math.round(Math.random() * _wanderingDistanceY);
			
			_destination = new Point(_randomX, _randomY)
		}
		public function update($e:Event):void
		{
			//Go to Point
			var diff:Point = _destination.subtract(new Point(this.x, this.y));
			var dist:Number = diff.length;
			
			//When reached find a new point
			if (dist < 2)
				pickADestination()
			else // If we are not there yet. Keep moving.
			{ 
				diff.normalize(1);
				x += diff.x * _movementSpeed;
				y += diff.y * _movementSpeed;
			}
		}
		public function destroy():void
		{
			//remove from stage & kill all event listeners.
			removeEventListener(Event.ENTER_FRAME, update);
			
			for each (var image:Image in _letterArray)
			{
				image.texture.dispose();
				image.dispose();
				removeChild(image)
				image = null;
			}
			
			_letterArray = [];
			
			_symbol.dispose();
			removeChild(_symbol);
			_symbol = null;
			
			removeChild(_display);
			removeFromParent(true);
		}
	}
}