package UI
{
	import Campaign.Campaign;
	
	import Core.Cell;
	import Core.GameManager;
	
	import Editor.EditorCell;
	import Editor.MapEditor;
	
	import FloxConnect.EntityManager;
	import FloxConnect.ErrorEntity;
	import FloxConnect.GameEntity;
	import FloxConnect.PlayerEntity;
	
	import Maps.Map;
	import Maps.MapManager;
	
	import OnlinePlay.OnlineCell;
	import OnlinePlay.OnlineGameManager;
	import OnlinePlay.OnlinePlay;
	import OnlinePlay.PushNotification;
	
	import Setup.GameSetup;
	
	import Sound.SoundController;
	
	import Tutorial.TutorialManager;
	
	import air.net.URLMonitor;
	
	import bg.BackgroundManager;
	
	import com.fiksu.Fiksu;
	import com.gamua.flox.AuthenticationType;
	import com.gamua.flox.Flox;
	import com.gamua.flox.Player;
	import com.milkmangames.nativeextensions.GoViral;
	import com.milkmangames.nativeextensions.GoogleGames;
	import com.milkmangames.nativeextensions.RateBox;
	import com.milkmangames.nativeextensions.events.GoogleGamesEvent;
	import com.milkmangames.nativeextensions.events.RateBoxEvent;
	import com.milkmangames.nativeextensions.ios.StoreKit;
	import com.sticksports.nativeExtensions.SilentSwitch;
	import com.sticksports.nativeExtensions.flurry.Flurry;
	import com.sticksports.nativeExtensions.gameCenter.GCPlayer;
	import com.sticksports.nativeExtensions.gameCenter.GameCenter;
	import com.sticksports.nativeExtensions.gameCenter.signals.GCSignal0;
	
	import feathers.controls.ScrollText;
	import feathers.system.DeviceCapabilities;
	import feathers.textures.Scale9Textures;
	
	import flash.events.StatusEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.text.TextFormat;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.AssetManager;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	import starling.utils.deg2rad;
	import com.hasoffers.nativeExtensions.MobileAppTracker;
	
	public class Main extends Sprite
	{
		public static const TEXTURE_LIMIT:int = 2048;
		public static const ANIMATION_SPEED_NORMAL:int = 24;
		public static const ANIMATION_SPEED_FAST:int = 48;
		public static const FADE_SPEED:Number = 0.06;
		private static const FACEBOOK_KEY:String = "659590164065414";
		public static var animationFramerate:int = 24;
		public static var sAnimationsOn:Boolean;
		public static var sEmail:String;
		public static var mobileAppTracker:MobileAppTracker;
		private const GOOGLE_SIGNIN_ATTEMPTS:int = 3;
		
		//Classes
		public var backgroundManager:BackgroundManager;
		public var gameSetup:GameSetup;
		public var gameManager:GameManager;
		public var mapEditor:MapEditor;
		public var _tutorial:TutorialManager;
		public static var campaign:Campaign
		public static var achievements:Achievements;
		public static var sSoundController:SoundController;
		private static var sAssets:AssetManager;
		private var _usernameCreationScreen:UsernameCreationScreen;
		private var _achievementScreen:AchievementScreen;
		
		// online play
		private var _onlinePlay:OnlinePlay;
		private var _onlineGameManager:OnlineGameManager;
		private var _mapManager:MapManager;
		private var _entityManager:EntityManager;
		
		// static variables used for online play
		public static var sOnlineID:String;
		public static var sVersion:String;
		public static var sOnlineNickname:String;
		public static var sGCFriends:Array = [];
		
		//Save
		public static var saveDataObject:SharedObject;
		
		//Cells
		public static var cellArray:Array = [];
		public static var mapCellArray:Array = [];
		
		[Embed(source="../../Resources/Fonts/Dekar.ttf", embedAsCFF="false", fontFamily="Dekar")]
		private static const Dekar:Class;
		
		[Embed(source="../../Resources/Fonts/Questrial-Regular.ttf", embedAsCFF="false", fontFamily="Questrial")]
		private static const Questrial:Class
		
		[Embed(source="../../Resources/particle.pex", mimeType="application/octet-stream")]
		public static const ParticlePEX:Class;
		
		[Embed(source="../../Resources/Standard/Loader.png")]
		public static const LoaderTexture:Class;
		
		[Embed(source="../../Resources/Standard/Loader.xml", mimeType="application/octet-stream")]
		public static const LoaderXML:Class;
		
		[Embed(source="../../Resources/HD/HD_Loader.png")]
		public static const HDLoaderTexture:Class;
		
		[Embed(source="../../Resources/HD/HD_Loader.xml", mimeType="application/octet-stream")]
		public static const HDLoaderXML:Class;
		
		// loading
		private var loader:Loader;
		private var totalNumOfTiles:int;
		public static var loaderTextureAtlas:TextureAtlas;
		
		private var _onlineGameIDs:Array;
		private var _onlineRankedGameIDs:Array;
		
		//Main Menu
		private var mainMenu:MainMenu;
		private var mainOptions:MainOptions;
		private var _credits:Credits;
		public static var debugText:ScrollText;
		private var _gameblyrSplash:Image;
		private var _BWSplash:Image;
		private var splashTween:Tween;
		
		// check internet connectivity
		private var _serviceMonitor:URLMonitor;
		public static var sInternet:Boolean;
		
		public static var sPushNotification:PushNotification;
		
		private var _inviteNoLongerExists:InviteNoLongerExists;
		
		//Tips
		private var tip_01:String = "Did You know that you can alter the animations speeds? Check it out in the options menu.";
		//private var tip_02:String = "Having trouble with understanding the spread of a move? Turn on practice mode to see a move’s effects before you make it.";
		private var tip_03:String = "Would you rather play without any animations? You’re in luck, check out the options menu to turn them off.";
		private var tip_04:String = "Do you like the Infection mode? Play it with your friends in the Adv. Options menu on the summary screen.";
		private var tip_05:String = "Ever want to build your own levels? There’s an editor for that. Select it off the main menu to start making your own right now.";
		private var tip_06:String = "Do you have suggestions on how to make Pathogen better? Let us know, drop us a line in the forums @ www.pathogen.co/forum";
		private var tip_07:String = "Are you hard core? Want to play without the Undo Button? If so, you can turn it off in the in-game options menu.";
		private var tip_08:String = "Ever say to yourself, “If only I hadn’t made that move…” Well, at the end of any game you can not only watch the replay, but change it!";
		private var tip_09:String = "Did you know: Pathogen was created in Flash Builder using Actionscript 3.";
		private var tip_10:String = "Did you know: You can practice against the AI outside the campaign? Tap on multiplayer & select Local to start a skirmish match.";
		
		private var tipArray:Array = [tip_01, tip_03, tip_04, tip_05, tip_06, tip_07, tip_08, tip_09, tip_10];
		private var _tipText:TextField;
		
		//Lite Version
		public static var _lite:Boolean = false;
		public static var _locked:Boolean = false;
		public static var sTestGooglePush:Boolean = false;
		private var _contentLockedScreen:ContentLock;
		
		private var _enjoyTheGameScreen:EnjoyTheGame;
		
		private var _gameLoader:Loader;
		private var _loadedOnlinePlay:Boolean = false;
		private var _lastOnlinePlayTab:String = "";
		
		// Android login vars
		private var _googleSigninAttempts:int = 0;
		public static var sFloxAltAuthentication:Boolean = true;
		private var _welcomeBackScreen:WelcomeBackScreen;
		
		public function Main()
		{
			super();
		}
		public function startGame(assets:AssetManager):void
		{
			//Creating Static Debug Text.
			debugText = new ScrollText();
			debugText.width = PathogenTest.wTenPercent * 5;
			debugText.height = PathogenTest.hTenPercent * 7;
			debugText.scaleX = PathogenTest.HD_Multiplyer * PathogenTest.scaleFactor;
			debugText.scaleY = PathogenTest.HD_Multiplyer * PathogenTest.scaleFactor;
			debugText.textFormat = new TextFormat("Verdana", 18, Color.WHITE);
			debugText.y = 100;
			addChild(debugText);
			debugText.visible = false;
			updateDebugText("Device: " + PathogenTest.device);
			updateDebugText("res: " + PathogenTest.stageWidth + " by " + PathogenTest.stageHeight);
			updateDebugText("scale: " + PathogenTest.scaleFactor);
			
			// check internet connectivity
			sInternet = false;
			checkInternetConnectivity(false);
			
			// Fiksu
			if (_lite)
			{
				if (Fiksu.isSupported())
				{
					Fiksu.create();
					//updateDebugText("Fiksu is supported!");
				}
			}
			
			// connect with Flox
			// testing
			//Flox.init("DoOlXiwIsftUSE1D", "QuCK1rVNEWJUQQw7", "0.9");
			
			// live
			Flox.init("dUsI47Y641f42n1v", "VO6P1Ewkl7zWWAcC", "0.9");
			
			// create Loader texture atlas
			var texture:Texture;
			var xml:XML;
			
			if (PathogenTest.HD_Multiplyer > 1.0)
			//if(PathogenTest.device == PathogenTest.iPAD_HD)
			{
				texture = Texture.fromBitmap(new HDLoaderTexture());
				xml = XML(new HDLoaderXML());
			}
			else
			{
				texture = Texture.fromBitmap(new LoaderTexture());
				xml = XML(new LoaderXML());
			}
			
			loaderTextureAtlas = new TextureAtlas(texture, xml);
			
			//Load in-game assets.
			sAssets = assets;
			
			loader = new Loader("0%", 38);
			addChild(loader);
			loader.x = PathogenTest.stageCenter.x;
			loader.y = PathogenTest.stageCenter.y;
			
			var randomTip:int = Math.round(Math.random() * (tipArray.length - 1))
			var tipText:String = tipArray[randomTip];
			
			_tipText = new TextField(PathogenTest.wTenPercent * 6, PathogenTest.hTenPercent * 2, tipText, "Questrial",  PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(_tipText);
			_tipText.pivotX = _tipText.width/2;
			_tipText.pivotY = _tipText.height/2;
			_tipText.x = PathogenTest.stageCenter.x;
			_tipText.y = PathogenTest.hTenPercent * 8;
			
			assets.loadQueue(function(ratio:Number):void
			{
				loader.updateText(Math.round(ratio * 100) + "%");
				
				// a progress bar should always show the 100% for a while,
				// so we show the main menu only after a short delay. 
				
				if (ratio == 1)
					Starling.juggler.delayCall(function():void
					{
						initBackground();
					}, 0.15);
			});
			
			// get save data
			saveDataObject = SharedObject.getLocal("Pathogen_Save_Data");
			
			if(_lite)
			{
				if(saveDataObject.data.liteStatus != null)
					Main._locked = saveDataObject.data.liteStatus;
			}
			
			// increment application opening variable
			var appOpens:int = saveDataObject.data.AppOpens;
			saveDataObject.data.AppOpens = appOpens + 1;
			saveDataObject.flush();
			
			achievements = new Achievements();
			
			// set animation speed
			if (saveDataObject.data.AnimationSpeed)
			{
				if (saveDataObject.data.AnimationSpeed != 0)
					animationFramerate = saveDataObject.data.AnimationSpeed;
			}
			sAnimationsOn = true;
			if (saveDataObject.data.AnimationsOn != null)
			{
				if (saveDataObject.data.AnimationsOn == true)
					sAnimationsOn = true;
				else
					sAnimationsOn = false;
			}
		}
		
		public function checkInternetConnectivity(relaunchOnlinePlay:Boolean):void
		{
			_serviceMonitor = new URLMonitor(new URLRequest('http://www.flox.cc/api'));
			if (!relaunchOnlinePlay)
				_serviceMonitor.addEventListener(StatusEvent.STATUS, announceStatus);
			else
				_serviceMonitor.addEventListener(StatusEvent.STATUS, announceStatusAndRelaunchOnlinePlay);
			_serviceMonitor.start();
		}
		
		private function announceStatus(statusEvent:StatusEvent):void
		{
			if (statusEvent.code == "Service.available")
				sInternet = true;
			_serviceMonitor.stop();
		}
		
		private function announceStatusAndRelaunchOnlinePlay(statusEvent:StatusEvent):void
		{
			if (statusEvent.code == "Service.available")
			{
				sInternet = true;
				mainMenu.launchOnlinePlay();
			}
			else
				mainMenu.addOnlineUnavailable();
			_serviceMonitor.stop();
		}
		
		private function initBackground():void
		{
			//Init Sound
			sSoundController = new SoundController();
			_mapManager = new MapManager();

			if (isIOS())
				SilentSwitch.apply();
			
			// create push notification
			//sPushNotification = new PushNotification();
			//sPushNotification2 = new PushNotification2();
			
			//Create Ambient Background
			backgroundManager = new BackgroundManager();
			addChild(backgroundManager);
			backgroundManager.touchable = false;
			
			loader.startFadeOut();
			
			removeChild(loader);
			loader = null;
			
			removeChild(_tipText);
			_tipText = null;
			
			//Starling.juggler.delayCall(addBWGSplashScreen, 1);
			init();
		}
		private function addBWGSplashScreen():void
		{
			//Add BW Splash
			_BWSplash = new Image(Main.assets.getTexture("Splash_BWG"));
			_BWSplash.pivotX = _BWSplash.width/2;
			_BWSplash.pivotY = _BWSplash.height/2;
			addChild(_BWSplash);
			_BWSplash.x = PathogenTest.stageCenter.x;
			_BWSplash.y = PathogenTest.stageCenter.y;
			_BWSplash.alpha = 0;
			
			//Add tween that makes it fade in.
			splashTween = new Tween(_BWSplash, 1);
			splashTween.fadeTo(1);
			splashTween.onComplete = addBWGSplashScreenComplete;
			Starling.juggler.add(splashTween);
		}
		private function addBWGSplashScreenComplete():void
		{
			//Have it stay onscreen for a moment before disappearing.
			Starling.juggler.delayCall(removeBWGSplashScreen, 2);
		}
		private function removeBWGSplashScreen():void
		{
			//Fade out splash screen.
			splashTween = new Tween(_BWSplash, 1);
			splashTween.fadeTo(0);
			splashTween.onComplete = addGamebylrSpashScreen;
			Starling.juggler.add(splashTween);
		}
		private function addGamebylrSpashScreen():void
		{
			//Remove BW screen.
			_BWSplash.dispose();
			removeChild(_BWSplash);
			_BWSplash = null;
			
			//Add BW Splash
			_gameblyrSplash = new Image(Main.assets.getTexture("Splash_Gambylr"));
			_gameblyrSplash.pivotX = _gameblyrSplash.width/2;
			_gameblyrSplash.pivotY = _gameblyrSplash.height/2;
			addChild(_gameblyrSplash);
			_gameblyrSplash.x = PathogenTest.stageCenter.x;
			_gameblyrSplash.y = PathogenTest.stageCenter.y;
			_gameblyrSplash.alpha = 0;
			
			//Add tween that makes it fade in.
			splashTween = new Tween(_gameblyrSplash, 1);
			splashTween.fadeTo(1);
			splashTween.onComplete = addGamebylrSpashScreenComplete;
			Starling.juggler.add(splashTween);
		}
		private function addGamebylrSpashScreenComplete():void
		{
			Starling.juggler.delayCall(removeGamebylrSplashScreen, 2);
		}
		private function removeGamebylrSplashScreen():void
		{
			//Fade out splash screen.
			splashTween = new Tween(_gameblyrSplash, 1);
			splashTween.fadeTo(0);
			splashTween.onComplete = delayBeforePathogen;
			Starling.juggler.add(splashTween);
		}
		private function delayBeforePathogen():void
		{
			Starling.juggler.delayCall(init, .5);
		}
		private function init():void
		{	
			setupPushNotifications();
			
			if(_gameblyrSplash)
			{
				//Remove Gameblyr screen.
				_gameblyrSplash.dispose();
				removeChild(_gameblyrSplash);
				_gameblyrSplash = null;	
			}
			
			openMainMenu();
			
			//updateDebugText("opened main menu");
			
			if (isIOS())
				setupGameCenter();
			if(isAndroid() && !isKindle())
			{
				if (sFloxAltAuthentication)
				{
					if (saveDataObject.data.sOnlineID != null)
						sOnlineID = saveDataObject.data.sOnlineID;
					if (saveDataObject.data.sOnlineNickname != null)
						sOnlineNickname = saveDataObject.data.sOnlineNickname;
					//if (saveDataObject.data.sEmail != null)
						//sEmail = saveDataObject.data.sEmail;

					if (sOnlineNickname == null)
						addUserNameCreationScreen();
					else
					{
						_welcomeBackScreen = new WelcomeBackScreen(this);
						addChild(_welcomeBackScreen);
						floxLogin();
					}
				}
				else
					setupGooglePlay();
			}
			
			if(PathogenTest.device == PathogenTest.PC || isKindle())
				floxLogin();
			
			socialMediaSetup();
			setupFlurry();
			setupStore();
			setupMAT();
			
			//entityManager.createInitialEntities();
			
			//Main.updateDebugText("checking for ratebox");
			
			// check for rate box
			if (RateBox.isSupported())
			{
				if (saveDataObject.data.AppOpens == 3)
				{
					_enjoyTheGameScreen = new EnjoyTheGame(this);
					addChild(_enjoyTheGameScreen);
				}
				else if (saveDataObject.data.AppOpens < 3)
					RateBox.rateBox.resetConditions();
			}
			
			//Main.updateDebugText("finished init");
		}
		
		/* iOS FUNCTIONS */
		public function setupGameCenter():void
		{
			//updateDebugText("Game Center is Supported: " + GameCenter.isSupported);
			
			//If this device is supported have the user authenticate with Game Center.
			//if(PathogenTest.device != PathogenTest.PC && GameCenter.isSupported && !PathogenTest.IGF_Build)
			if(GameCenter.isSupported && !PathogenTest.IGF_Build)
			{
				//updateDebugText("Authenticating User");
				
				GameCenter.localPlayerAuthenticated.add(localPlayerAuthenticated);
				GameCenter.localPlayerNotAuthenticated.add(localPlayerNotAuthenticated);
				GameCenter.authenticateLocalPlayer();
			}
		}
		private function localPlayerAuthenticated() : void
		{
			//updateDebugText("Local Player has been authenticated");

			GameCenter.localPlayerAuthenticated.remove(localPlayerAuthenticated);
			GameCenter.localPlayerFriendsLoadComplete.add(localPlayerFriendsLoaded);
			GameCenter.getLocalPlayerFriends();
		}
		private function localPlayerNotAuthenticated():void
		{
			//updateDebugText("Local Player has NOT been authenticated");
		}
		private function setupStore():void
		{
			//updateDebugText("initStore");
			
			if(StoreKit.isSupported())
			{
				//updateDebugText("Store kit is supported, Beginning Inilization");
				StoreKit.create();
			}
		}
		private function localPlayerFriendsLoaded(friends:Array):void
		{
			GameCenter.localPlayerFriendsLoadComplete.remove(localPlayerFriendsLoaded);
			
			//updateDebugText("localPlayerFriendsLoadComplete");
			
			if (friends.length == 0)
			{
				//updateDebugText("player has no friends");
			}
			else
			{
				sGCFriends = [];
				sGCFriends = friends;
				//updateDebugText("Player has " + friends.length + " Friends");
			}
			
			//updateDebugText("About to login to Flox");
			
			floxLogin();
		}
		public function reloadGameCenterFriends():void
		{
			if (GameCenter.isSupported)
			{
				// call GameCenter functions to reload friends
				GameCenter.localPlayerFriendsLoadComplete.add(onReloadLocalPlayerFriends);
				GameCenter.getLocalPlayerFriends();
			}
		}
		private function onReloadLocalPlayerFriends(friends:Array):void
		{
			// remove event
			GameCenter.localPlayerFriendsLoadComplete.remove(onReloadLocalPlayerFriends);
			
			// get the friends list into a static
			if (friends.length > 0)
			{
				sGCFriends = [];
				sGCFriends = friends;
				//updateDebugText("Player has " + friends.length + " Friends");
			}
			
			// insert friends into online play data
			if (_entityManager)
			{
				_entityManager.reloadPlayerFriends();
			}
		}
		public function openGameCenter():void
		{
			//updateDebugText("Opening Game Center: Game Center is Supported: " + GameCenter.isSupported);
			
			if(PathogenTest.device != PathogenTest.PC && GameCenter.isSupported)
			{
				//updateDebugText("Game Center Authenicated: " + GameCenter.isAuthenticated);
				
				if(GameCenter.isAuthenticated)
				{
					GameCenter.showStandardAchievements();
					GameCenter.gameCenterViewRemoved.add( GameCenterClosed );
				}
				else
					setupGameCenter();
			}	
		}
		private function GameCenterClosed():void
		{
			GameCenter.gameCenterViewRemoved.remove( GameCenterClosed );
		}
		public function setupGooglePlay():void
		{
			_googleSigninAttempts = 0;
			
			//updateDebugText("Setup Google Play");
			
			//updateDebugText("Google Play is Supported: " + GoogleGames.isSupported());
			
			//updateDebugText("Google Play is supported: " + AirGooglePlayGames.isSupported);
			
			/*
			if (AirGooglePlayGames.isSupported)
			{
				AirGooglePlayGames.getInstance().addEventListener(AirGooglePlayGamesEvent.ON_SIGN_IN_SUCCESS, onGoogleSignInSuccess);
				AirGooglePlayGames.getInstance().addEventListener(AirGooglePlayGamesEvent.ON_SIGN_IN_FAIL, onGoogleSignInFail);
				AirGooglePlayGames.getInstance().startAtLaunch();
			}
			*/
			
			if(GoogleGames.isSupported())
			{
				//updateDebugText("Creating Google Play Instance");
				GoogleGames.create();
				GoogleGames.games.addEventListener(GoogleGamesEvent.SIGN_IN_SUCCEEDED, onSignedIn);
				GoogleGames.games.addEventListener(GoogleGamesEvent.SIGN_IN_FAILED, onGamesError);
				//updateDebugText("Attempting to sign in...");
				GoogleGames.games.signIn();
			}
			
		}
		/*
		private function onGoogleSignInSuccess($e:AirGooglePlayGamesEvent):void
		{
			AirGooglePlayGames.getInstance().removeEventListener(AirGooglePlayGamesEvent.ON_SIGN_IN_SUCCESS, onGoogleSignInSuccess);
			AirGooglePlayGames.getInstance().removeEventListener(AirGooglePlayGamesEvent.ON_SIGN_IN_FAIL, onGoogleSignInFail);
			
			updateDebugText("Andriod User has been authenticated");
			updateDebugText("PlayerName: " + AirGooglePlayGames.getInstance().getActivePlayerName());
			
			floxLogin();
		}
		private function onGoogleSignInFail(e:AirGooglePlayGamesEvent):void
		{
			updateDebugText("Andriod User has NOT been authenticated");
			
			AirGooglePlayGames.getInstance().removeEventListener(AirGooglePlayGamesEvent.ON_SIGN_IN_SUCCESS, onGoogleSignInSuccess);
			AirGooglePlayGames.getInstance().removeEventListener(AirGooglePlayGamesEvent.ON_SIGN_IN_FAIL, onGoogleSignInFail);
		}
		*/
		
		private function onSignedIn($e:GoogleGamesEvent):void
		{
			GoogleGames.games.removeEventListener(GoogleGamesEvent.SIGN_IN_SUCCEEDED, onSignedIn);
			GoogleGames.games.removeEventListener(GoogleGamesEvent.SIGN_IN_FAILED, onGamesError);
			
			//updateDebugText("Andriod User has been authenticated");
			//updateDebugText("PlayerID: " + GoogleGames.games.getCurrentPlayerId());
			//updateDebugText("PlayerName: " + GoogleGames.games.getCurrentPlayerName());
			
			//if(saveDataObject.data.AndriodUserName == null)
				//addUserNameCreationScreen();
			//else
				//floxLogin();
			
			floxLogin();
		}
		private function onGamesError(e:GoogleGamesEvent):void
		{
			_googleSigninAttempts++;
			
			//updateDebugText("Andriod User has NOT been authenticated");
			
			GoogleGames.games.removeEventListener(GoogleGamesEvent.SIGN_IN_SUCCEEDED, onSignedIn);
			GoogleGames.games.removeEventListener(GoogleGamesEvent.SIGN_IN_FAILED, onGamesError);
			
			if (_googleSigninAttempts < GOOGLE_SIGNIN_ATTEMPTS)
			{
				GoogleGames.games.addEventListener(GoogleGamesEvent.SIGN_IN_SUCCEEDED, onSignedIn);
				GoogleGames.games.addEventListener(GoogleGamesEvent.SIGN_IN_FAILED, onGamesError);
				//updateDebugText("Attempting to sign in again...");
				GoogleGames.games.signIn();
			}
		}
		
		private function addUserNameCreationScreen():void
		{
			if(_usernameCreationScreen == null)
			{
				_usernameCreationScreen = new UsernameCreationScreen(this);
				addChild(_usernameCreationScreen);
			}
		}
		public function removeUserNameCreationScreen():void
		{
			if(_usernameCreationScreen)
			{
				_usernameCreationScreen.dispose();
				removeChild(_usernameCreationScreen);
				_usernameCreationScreen = null;
			}
		}
		
		/* FLOX FUNCTIONS */
		private function floxLogin():void
		{
			//updateDebugText("Flox Login");
			
			_entityManager = new EntityManager(this);
			addChild(_entityManager);
			
			if(PathogenTest.device == PathogenTest.PC || PathogenTest.IGF_Build || isKindle())
			{
				sOnlineID = "0123456789";
				sOnlineNickname = "monkeyboy2014yes";
				
				//sOnlineID = "1234567890";
				//sOnlineNickname = "player1";
			}
			else if (isIOS())
			{
				//debugText.text = "iOS Device";
				
				sOnlineID = GameCenter.localPlayer.id.substr(2);
				sOnlineNickname = GameCenter.localPlayer.alias;
				
				if(Flurry.isSupported)
					Flurry.setUserId(sOnlineNickname);
			}
			else if (isAndroid())
			{
				//debugText.text = "Andriod Device";
				
				if(isKindle())
				{
					sOnlineID = "012345012345";
					sOnlineNickname = "KindleUser";
				}
				else
				{
					if (!sFloxAltAuthentication)
					{
						sOnlineID = GoogleGames.games.getCurrentPlayerId();
						sOnlineNickname = GoogleGames.games.getCurrentPlayerName();
					}
				}
					
				if(Flurry.isSupported)
					Flurry.setUserId(sOnlineNickname);
			}
			
			if (_entityManager)
			{
				if (_entityManager.currentPlayer == null)
					Player.logout();
			}
			
			//updateDebugText("Flox Logging In Now");
			Player.login();
			
			if(Player.current.authType == AuthenticationType.GUEST)
				Player.loginWithKey(sOnlineID, onLoginComplete, onLoginError);
			else
				Player.current.refresh(onRefreshPlayerComplete, onRefreshPlayerError);
		}
		private function onLoginComplete():void
		{
			//updateDebugText("Completed Login");
			entityManager.setCurrentPlayer();
		}
		private function onLoginError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onLoginError";
			errorEntity.error = error + " sOnlineID = " + sOnlineID;
			errorEntity.saveQueued();
			*/
		}
		private function onRefreshPlayerComplete():void
		{
			entityManager.setCurrentPlayer();
		}
		private function onRefreshPlayerError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onRefreshPlayerError";
			errorEntity.error = error + " sOnlineID = " + sOnlineID;
			errorEntity.saveQueued();
			*/
		}
		private function waitForEntityManager(event:Event):void
		{
			if (entityManager)
			{
				removeEventListener(Event.ENTER_FRAME, waitForEntityManager);
				entityManager.setPushNotificationServerAddress();
				entityManager.setPlayerAPNSTokenID();
			}
		}
		
		/* DATA GATHERING FUNCTIONS */
		private function setupFlurry():void
		{
			if(Flurry.isSupported)
			{
				Flurry.setAppVersion("2.6.9");
				Flurry.flurryAgentVersion;
				Flurry.sessionContinueSeconds;
				Flurry.secureTransportEnabled = true;
				Flurry.startSession("RNF9RXM9HCKDRXQ48XT3"); //Live Analyitcs
				//Flurry.startSession("HV323W69D2J3MX75ZYH3"); //Debug
			}
		}
		private function setupMAT():void
		{
/*			if(PathogenTest.device != PathogenTest.PC)
			{
				mobileAppTracker = MobileAppTracker.instance;
				
				var advertiserID:String = "14434";
				var conversionKey:String  = "1ca903f52512649eaa378bc312d37a57";
				
				mobileAppTracker.init(advertiserID, conversionKey);
				
				if(saveDataObject.data.CampaignSaveData != null)
				mobileAppTracker.trackUpdate();
				else
				mobileAppTracker.trackInstall();
				
				mobileAppTracker.trackAction("open");
			}*/
		}
		private function socialMediaSetup():void
		{
			//Main.updateDebugText("Social Media is supported: " + GoViral.isSupported());
			
			if(GoViral.isSupported())
			{
				GoViral.create();
				GoViral.goViral.initFacebook(FACEBOOK_KEY, "");
				GoViral.goViral.publishInstall();
			}
		}
		private function setupPushNotifications():void
		{
			// create push notification
			sPushNotification = new PushNotification(this);
			addEventListener(Event.ENTER_FRAME, waitForEntityManager);
		}
		public static function updateDebugText(text:String):void
		{
			if (debugText.visible)
				debugText.text += "\n" + text;
		}
		
		/* GENERAL MAIN MENU FUNCTIONS */
		public function openMainMenu():void
		{
			mainMenu = new MainMenu(this);
			addChild(mainMenu);
		}
		
		public function closeMainMenu():void
		{
			mainMenu.dispose();
			removeChild(mainMenu);
			mainMenu = null;
		}
		public function openLocalPlay():void
		{
			if(gameSetup == null)
			{
				gameSetup = new GameSetup(this);
				addChild(gameSetup);
			}
		}
		public function openOnlinePlay():void
		{
			//updateDebugText("openOnlinePlay");
					
			// set up the online board
			if(_onlinePlay == null)
			{
				_onlinePlay = new OnlinePlay(this);
				addChild(_onlinePlay);
			}
		}
		public function openTutorial():void
		{
			_tutorial = new TutorialManager(this);
			addChild(_tutorial);
		}
		public function removeTutorial():void
		{
			_tutorial.dispose();
			removeChild(_tutorial);
			_tutorial = null;
		}
		public function openCampaign():void
		{
			if(campaign == null)
			{
				campaign = new Campaign(this, gameManager, saveDataObject);
				addChild(campaign);
				achievements.campaign = campaign;
			}
		}
		public function removeCampaign():void
		{
			if(campaign)
			{
				campaign.dispose();
				removeChild(campaign);
				campaign = null;
				achievements.campaign = null;
			}
		}
		public function openMapEditor():void
		{
			if(Flurry.isSupported)
				Flurry.startTimedEvent("Editor");
			
			mapEditor = new MapEditor(this);
			addChild(mapEditor);
			achievements.mapEditor = mapEditor;
		}
		public function initGameManager(playerColors:Array, playerLocations:Array, playerAI:Array, mapData:Array, mapName:String, mapSize:int, modes:Array):void
		{
			if(gameManager == null)
			{
				gameManager = new GameManager(this, null, playerColors, playerLocations, playerAI, mapData, mapName, mapSize, modes);
				addChild(gameManager);
				achievements.gameManager = gameManager;
			}
		}
		public function initOnlineGameManager(playerColors:Array, playerLocations:Array, map:Map, gameID:String):void
		{
			if (_onlineGameManager == null)
			{
				_onlineGameManager = new OnlineGameManager(this, playerColors, playerLocations, map, gameID);
				addChild(_onlineGameManager);
				achievements.onlineGameManager = _onlineGameManager;
			}
		}
		public function removeGameSetup():void
		{
			if(gameSetup)
			{
				gameSetup.dispose();
				removeChild(gameSetup)
				gameSetup = null;
			}
		}
		public function removeGameManager():void
		{
			if(gameManager)
			{
				gameManager.dispose();
				removeChild(gameManager)
				gameManager = null;
				achievements.gameManager = null;
				openMainMenu();
			}
			
			sSoundController.transitionToMenuMusic();
		}
		public function removeOnlinePlay(startOnlineGame:Boolean):void
		{
			if(_onlinePlay)
			{
				_onlinePlay.dispose();
				removeChild(_onlinePlay);
				_onlinePlay = null;
			}
			
			if (!startOnlineGame)
			{
				if(Flurry.isSupported)
					Flurry.endTimedEvent("Online");
				
				openMainMenu();	
			}
				
		}
		public function removeOnlineGameManager():void
		{
			_onlineGameManager.dispose();
			removeChild(_onlineGameManager);
			_onlineGameManager = null;
			achievements.onlineGameManager = null;
			openMainMenu();
		}
		public function openContentLockedScreen():void
		{
			if(_contentLockedScreen == null)
			{
				_contentLockedScreen = new ContentLock(this);
				addChild(_contentLockedScreen);
			}
		}
		public function removeContentLockedScreen():void
		{
			_contentLockedScreen.dispose();
			removeChild(_contentLockedScreen);
			_contentLockedScreen = null;
			
			if(mainMenu && _locked == false)
				mainMenu.refreshMainMenu();
			
			if(gameSetup && _locked == false)
				gameSetup.refreshGameSetup();
		}
		public function returnToOnlineGamesList():void
		{
			//updateDebugText("Return to Online Games List");
			
			// remove online game manager
			if (_onlineGameManager)
			{
				//_onlineGameManager.garbageCollection();
				_onlineGameManager.dispose();
				removeChild(_onlineGameManager);
				_onlineGameManager = null;
			}

			var tab:String = "";
			if (_onlinePlay)
			{
				tab = _onlinePlay.openingTab;
				
				_onlinePlay.dispose();
				removeChild(_onlinePlay);
				_onlinePlay = null;
			}
			else
				tab = _lastOnlinePlayTab;
			
			if (tab == "")
				tab = "Custom";
			
			// create onlineplay
			_onlinePlay = new OnlinePlay(this, tab);
			addChild(_onlinePlay);
		}
		public function removeMapEditor():void
		{
			if(Flurry.isSupported)
				Flurry.endTimedEvent("Editor");
			
			//Removes all map editor cells.
			for (var i:int=(mapCellArray.length - 1); i >= 0; i--)
			{
				mapCellArray[i].cleanUp();
				mapCellArray[i].dispose();
				mapCellArray[i] = null;
			}
			mapCellArray = null;
			
			mapEditor.dispose();
			removeChild(mapEditor);
			mapEditor = null;
			achievements.mapEditor = null;
		}
		public function openMainOptions():void
		{
			mainOptions = new MainOptions(this);
			addChild(mainOptions);
			mainOptions.x = PathogenTest.stageCenter.x;
			mainOptions.y = PathogenTest.stageCenter.y;
		}
		public function removeMainOptions():void
		{
			mainOptions.dispose();
			removeChild(mainOptions);
			mainOptions = null;
		}
		public function openCredits():void
		{
			_credits = new Credits(this);
			addChild(_credits);
			_credits.x = PathogenTest.stageCenter.x;
			_credits.y = PathogenTest.stageCenter.y;
		}
		public function removeCredits():void
		{
			_credits.dispose();
			removeChild(_credits);
			_credits = null;
		}
		public function addAchievementScreen():void
		{
			if(_achievementScreen == null)
			{
				_achievementScreen = new AchievementScreen();
				addChild(_achievementScreen);
				_achievementScreen.init(this);
			}
		}
		public function leaveAchievementScreen():void
		{
			if(_achievementScreen)
			{
				_achievementScreen.dispose();
				removeChild(_achievementScreen);
				_achievementScreen = null;
			}
		}
		public function launchGameInOnlinePlay(gameID:String):void
		{
			onlinePlay.loadGame(gameID);
		}
		public function recheckInternetConnectivity():void
		{
			//updateDebugText("recheckInternetConnectivity");
			sInternet = false;
			checkInternetConnectivity(false);
		}
		
		/* RATEBOX FUNCTIONS */
		public function removeEnjoyTheGame(yesOrNo:Boolean):void
		{
			if (yesOrNo)
			{
				if (RateBox.isSupported())
				{
					//updateDebugText("popping up ratebox");
					//RateBox.create("600038811", "Rate This App", "If you like Pathogen, please rate it!", "Rate Now", "Ask Me Later","Don't ask again", 5, 0, 5, 3);
					RateBox.create("600038811", "Rate This App", "If you like Pathogen, please rate it!", "Rate Now", "Ask Me Later","Don't ask again", 0, 0, 0, 0);
					RateBox.rateBox.onLaunch();
					RateBox.rateBox.addEventListener(RateBoxEvent.LATER_SELECTED, onChoseRateLater);
				}
			}
			
			_enjoyTheGameScreen.dispose();
			removeChild(_enjoyTheGameScreen);
			_enjoyTheGameScreen = null;
		}
		private function onChoseRateLater(event:RateBoxEvent):void
		{
			saveDataObject.data.AppOpens = 0;
			saveDataObject.flush();
		}
		
		/* LOADER FUNCTIONS */
		public function createGameLoader():void
		{
			// display Loader
			_gameLoader = new Loader("Loading Game Data", 38, false);
			addChild(_gameLoader);
			_gameLoader.x = PathogenTest.stageCenter.x;
			_gameLoader.y = PathogenTest.stageCenter.y;
		}
		public function removeGameLoader():void
		{
			if (_gameLoader)
				_gameLoader.startFadeOut();
		}
		
		/* GAME INVITATION FUNCTIONS */
		public function inviteNoLongerExists():void
		{
			//updateDebugText("Return to Online Games List");
			
			// remove online game manager
			if (_onlineGameManager)
			{
				_onlineGameManager.dispose();
				removeChild(_onlineGameManager);
				_onlineGameManager = null;
			}
			
			if (_onlinePlay)
			{
				_onlinePlay.dispose();
				removeChild(_onlinePlay);
				_onlinePlay = null;
			}
			
			_inviteNoLongerExists = new InviteNoLongerExists(this);
			addChild(_inviteNoLongerExists);
		}
		public function removeInviteNoLongerExists():void
		{
			if (_inviteNoLongerExists)
			{
				_inviteNoLongerExists.dispose();
				removeChild(_inviteNoLongerExists);
				_inviteNoLongerExists = null;
			}
			
			// create onlineplay
			_onlinePlay = new OnlinePlay(this, "Custom");
			addChild(_onlinePlay);
		}
		public function saveOnlineInfo(nickname:String):void
		{
			sOnlineID = PushNotification.sTokenID;
			saveDataObject.data.sOnlineID = sOnlineID;
			sOnlineNickname = nickname;
			saveDataObject.data.sOnlineNickname = sOnlineNickname;
			saveDataObject.flush();
			
			floxLogin();
		}
		public function removeWelcomeBackScreen():void
		{
			_welcomeBackScreen.dispose();
			removeChild(_welcomeBackScreen);
			_welcomeBackScreen = null;
		}
		
		/* DEVICE CHECK FUNCTIONS */
		public static function isPhone():Boolean
		{
			if (PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5 || PathogenTest.stageWidth < 1024)
				return true;
			return false;
		}
		public static function isIOS():Boolean
		{
			if (PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5 || PathogenTest.device == PathogenTest.iPAD_2 || PathogenTest.device == PathogenTest.iPAD_HD)
				return true;
			return false;
		}
		public static function isAndroid():Boolean
		{
			if (PathogenTest.device == PathogenTest.GOOGLE_NEXUS_4 || PathogenTest.device == PathogenTest.LG_OPTIMUS || PathogenTest.device == PathogenTest.MOTOROLA_MOTO_X || PathogenTest.device == PathogenTest.MOTOROLA_DROID_RAZR || PathogenTest.device == PathogenTest.MOTOROLA_DROID_M || PathogenTest.device == PathogenTest.ANDROID)
				return true;
			return false;
		}
		public static function isKindle():Boolean
		{
			if (PathogenTest.KINDLE == true)
				return true;
			return false;
		}
		public static function isNook():Boolean
		{
			if (PathogenTest.device.substr(0, 6) == "NOOK")
				return true;
			return false;
		}
		
		// getters
		public static function get assets():AssetManager { return sAssets; }
		public function get onlineGameManager():OnlineGameManager { return _onlineGameManager; }
		public function get mapManager():MapManager { return _mapManager; }
		public function get entityManager():EntityManager { return _entityManager; }
		public function get onlinePlay():OnlinePlay { return _onlinePlay; }
		public function get onlineGameIDs():Array { return _onlineGameIDs; }
		public function get onlineRankedGameIDs():Array { return _onlineRankedGameIDs; }
		public function get internet():Boolean {return sInternet};
		public function get loadedOnlinePlay():Boolean { return _loadedOnlinePlay; }
		
		public function set onlineGameIDs(onlineGameIDs:Array):void { _onlineGameIDs = onlineGameIDs; }
		public function set onlineRankedGameIDs(onelineRankedGameIDs:Array):void { _onlineRankedGameIDs = onlineRankedGameIDs; }
		public function set loadedOnlinePlay(loadedOnlinePlay:Boolean):void { _loadedOnlinePlay = loadedOnlinePlay; }
		public function set lastOnlinePlayTab(lastOnlinePlayTab:String):void { _lastOnlinePlayTab = lastOnlinePlayTab; }
	}
}