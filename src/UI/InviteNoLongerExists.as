package UI
{
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class InviteNoLongerExists extends BW_UI
	{
		//Classes
		private var _main:Main;
		private var _okButton:BW_Button;
		
		public function InviteNoLongerExists(main:Main)
		{
			super();
			
			_main = main;
			
			// create a transparent background
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			addChild(transBackground);
			transBackground.alpha = .75;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var screenHolder:Sprite = new Sprite();
			addChild(screenHolder);
			screenHolder.x = PathogenTest.stageCenter.x;
			screenHolder.y = PathogenTest.stageCenter.y;
			
			//Creating background so that content stands out.
			var background:Scale9Image = new Scale9Image(textures);
			screenHolder.addChild(background);
			background.width = PathogenTest.wTenPercent * 5;
			background.height = PathogenTest.hTenPercent * 4.5;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			background.color = 0x222245;
			
			// create text
			var fontSize:int = 36 / PathogenTest.scaleFactor;
			var text:TextField = new TextField(background.width, PathogenTest.hTenPercent * 1.5, "Sorry that game has been removed", "Questrial", fontSize, Color.WHITE);
			text.pivotX = text.width / 2;
			text.pivotY = text.height / 2;
			text.y -= text.height;
			screenHolder.addChild(text);
			
			// create ok button
			_okButton = new BW_Button(1.0, 1.0, "OK", 36);
			screenHolder.addChild(_okButton);
			_okButton.y += _okButton.height;
			_okButton.addEventListener(TouchEvent.TOUCH, onTouchOK);
		}
		
		private function onTouchOK(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this,TouchPhase.ENDED);
			if (touch)
			{
				startFadeOut();
			}
		}
		override public function death():void
		{
			if (_okButton)
			{
				_okButton.dispose();
				removeChild(_okButton);
				_okButton = null;
			}
			
			_main.removeInviteNoLongerExists();
		}
	}
}