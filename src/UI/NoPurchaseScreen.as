package UI
{
	import Utils.BW_Button;
	import Utils.BW_Window;
	
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class NoPurchaseScreen extends BW_Window
	{
		//Parent Class
		private var _contentLock:ContentLock;
		
		//Buttons
		private var _okButton:BW_Button;
		
		public function NoPurchaseScreen(contentLock:ContentLock)
		{
			super();
			
			_contentLock = contentLock;
			
			createbackground(5, 6);
			
			// create text
			var fontSize:int = 36 / PathogenTest.scaleFactor;
			var text:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "No Purchase Record found.", "Questrial", fontSize, Color.WHITE);
			text.pivotX = text.width / 2;
			text.pivotY = text.height / 2;
			text.x = _background.x;
			text.y = _background.y;
			addChild(text);
			
			// create ok button
			_okButton = new BW_Button(1.0, 1.0, "OK", 36);
			addChild(_okButton);
			_okButton.x = _background.x;
			_okButton.y = _background.y + _background.height/2 - (_okButton.height * 1.2);
			_okButton.addEventListener(TouchEvent.TOUCH, onTouchOK);
		}
		private function onTouchOK(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this,TouchPhase.ENDED);
			if (touch)
			{
				startFadeOut();
			}
		}
		override public function death():void
		{
			if (_okButton)
			{
				_okButton.dispose();
				removeChild(_okButton);
				_okButton = null;
			}
			
			_contentLock.removeNoPurchaseScreen();
			_contentLock = null;
		}
	}
}