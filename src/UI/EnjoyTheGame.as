package UI
{
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class EnjoyTheGame extends BW_UI
	{
		private var _main:Main;
		private var _yesButton:BW_Button;
		private var _noButton:BW_Button;
		private var _answer:Boolean;
			
		public function EnjoyTheGame(main:Main)
		{
			super();
			
			_main = main;
			_answer = true;
			
			// create a transparent background
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			addChild(transBackground);
			transBackground.alpha = .75;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var screenHolder:Sprite = new Sprite();
			addChild(screenHolder);
			screenHolder.x = PathogenTest.stageCenter.x;
			screenHolder.y = PathogenTest.stageCenter.y;
			
			//Creating background so that content stands out.
			var background:Scale9Image = new Scale9Image(textures);
			screenHolder.addChild(background);
			background.width = PathogenTest.wTenPercent * 5;
			background.height = PathogenTest.hTenPercent * 4.5;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			background.color = 0x222245;
			
			// create text
			var fontSize:int = PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor;
			var textX:int = transBackground.width;
			if (textX > Main.TEXTURE_LIMIT)
				textX = Main.TEXTURE_LIMIT;
			var text:TextField = new TextField(textX, PathogenTest.hTenPercent * 2, "Are you enjoying the game?", "Questrial", fontSize, Color.WHITE);
			text.pivotX = text.width / 2;
			text.pivotY = text.height / 2;
			text.y -= text.height / 2;
			screenHolder.addChild(text);
			
			var buttonHeight:Number = 0.75;
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				buttonHeight = 1.0;
				
			_yesButton = new Utils.BW_Button(2, buttonHeight, "Yes", 32);
			screenHolder.addChild(_yesButton);
			_yesButton.x -= _yesButton.width * 0.6;
			_yesButton.y += _yesButton.height;
			_yesButton.addEventListener(TouchEvent.TOUCH, onTouchYes);
			
			_noButton = new Utils.BW_Button(2, buttonHeight, "No", 32);
			screenHolder.addChild(_noButton);
			_noButton.x += _noButton.width * 0.6;
			_noButton.y += _noButton.height;
			_noButton.addEventListener(TouchEvent.TOUCH, onTouchNo);
		}
		
		private function onTouchYes(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_yesButton.removeEventListener(TouchEvent.TOUCH, onTouchYes);
				_answer = true;
				startFadeOut();
			}
		}
		
		private function onTouchNo(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_noButton.removeEventListener(TouchEvent.TOUCH, onTouchNo);
				_answer = false;
				startFadeOut();
			}
		}
		
		override public function death():void
		{
			if (_yesButton)
			{
				_yesButton.dispose();
				_yesButton.destroy();
				removeChild(_yesButton);
				_yesButton = null;
			}
			
			if (_noButton)
			{
				_noButton.dispose();
				_noButton.destroy();
				removeChild(_noButton);
				_noButton = null;
			}
			
			_main.removeEnjoyTheGame(_answer);
			_main = null;
		}
	}
}