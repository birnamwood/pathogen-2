package UI
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_Window;
	
	import feathers.controls.TextInput;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class UsernameCreationScreen extends BW_Window
	{
		//Classes
		private var _main:Main;
		
		//UI
		private var _okButton:BW_Button;
		
		//Vars
		private var _userName:TextInput;
		//private var _email:TextInput;
		
		public function UsernameCreationScreen(main:Main)
		{
			super();
			
			_main = main;
			
			createbackground(6.5, 4.5);
			createHeader("Welcome!")
			init();
		}
		private function init():void
		{
			var text:TextField = new TextField(_background.width, PathogenTest.hTenPercent * .5, "Please create a nickname", "Dekar", PathogenTest.HD_Multiplyer * 34 / PathogenTest.scaleFactor, Color.WHITE);
			addChild(text);
			text.pivotX = text.width/2;
			text.pivotY = text.height/2;
			text.x = _header.x;
			text.y = _header.y + _header.height + (text.height * 0.75);
			
			var textInputBackground:Quad = new Quad(_background.width * .6, PathogenTest.hTenPercent * 0.65, Color.BLACK);
			textInputBackground.pivotX = textInputBackground.width/2;
			textInputBackground.pivotY = textInputBackground.height/2;
			addChild(textInputBackground);
			textInputBackground.alpha = .5;
			textInputBackground.x = _background.x;
			textInputBackground.y = _background.y;
			
			/*
			_email = new TextInput();
			_email.width = PathogenTest.hTenPercent * 3;
			_email.height = PathogenTest.hTenPercent * 0.65;
			_email.pivotX = _email.width / 2;
			_email.pivotY = _email.height / 2;
			_email.textEditorProperties.fontFamily = "Dekar";
			_email.textEditorProperties.fontSize = PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor;
			_email.textEditorProperties.color = Color.WHITE;
			_email.text = "email";
			_email.setFocus();
			_email.selectRange(_email.text.length);
			_email.paddingTop = 5;
			_email.paddingLeft = 5;
			addChild(_email);
			_email.x = textInputBackground.x;
			_email.y = textInputBackground.y;
			*/
			
			_userName = new TextInput();
			_userName.width = textInputBackground.width;
			if (Main.isPhone() && Main.isAndroid())
				_userName.height = PathogenTest.hTenPercent * 0.85;
			else
				_userName.height = PathogenTest.hTenPercent * 0.65;
			_userName.pivotX = _userName.width / 2;
			_userName.pivotY = _userName.height / 2;
			_userName.textEditorProperties.fontFamily = "Dekar";
			_userName.textEditorProperties.fontSize = PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor;
			_userName.textEditorProperties.color = Color.WHITE;
			_userName.text = "enter a nickname";
			_userName.setFocus();
			_userName.selectRange(_userName.text.length);
			_userName.paddingTop = 5;
			_userName.paddingLeft = 5;
			addChild(_userName);
			_userName.x = textInputBackground.x;
			_userName.y = text.y + text.height + (_userName.height * 0.6);
			
			//Ok Button
			if (Main.isPhone())
				_okButton = new Utils.BW_Button(1.5, 1, "Ok", 22);
			else
				_okButton = new Utils.BW_Button(1.3, .8, "Ok", 22);
			
			addChild(_okButton);
			_okButton.x = _background.x;
			//_okButton.y = _background.y + _background.height/2 - _okButton.height;
			_okButton.y = _userName.y + _userName.height + (_okButton.height * 0.6);
			_okButton.addEventListener(TouchEvent.TOUCH, onTouchOk);
		}
		private function onTouchOk($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				//_sharedData.data.AndriodUserName = _userName.text;
				//_sharedData.flush();
				
				_main.saveOnlineInfo(_userName.text);
				
				_okButton.removeEventListener(TouchEvent.TOUCH, onTouchOk)
				
				startFadeOut();
			}
		}
		private function onTouchQuit($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				_okButton.removeEventListener(TouchEvent.TOUCH, onTouchOk)
				
				startFadeOut();
			}
		}
		public override function death():void
		{
			if(_okButton)
			{
				_okButton.dispose();
				_okButton.destroy();
				removeChild(_okButton);
				_okButton = null;
			}
			
			_main.removeUserNameCreationScreen();
			_main = null;
		}
	}
}