package UI
{
	import Utils.BW_Button;
	import Utils.BW_Window;
	
	import com.milkmangames.nativeextensions.ios.StoreKit;
	import com.milkmangames.nativeextensions.ios.StoreKitProduct;
	import com.milkmangames.nativeextensions.ios.events.StoreKitErrorEvent;
	import com.milkmangames.nativeextensions.ios.events.StoreKitEvent;
	import com.sticksports.nativeExtensions.gameCenter.GameCenter;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	
	public class ContentLock extends BW_Window
	{
		//Parent Classes
		private var _main:Main;
		
		//Buttons
		private var _quitButton:BW_Button;
		private var _unlockButton:BW_Button;
		private var _restoreButton:BW_Button;
		
		//Addtional Screens
		private var _NoPurchaseScreen:NoPurchaseScreen;
		
		public function ContentLock(main:Main)
		{
			super();
			
			_main = main;
			
			if (Main.isPhone())
				createbackground(6, 6);
			else
				createbackground(6, 4.9);
			
			
			createHeader("Unlock Premium Game");
			initStore();
			init();
		}
		private function initStore():void
		{
			Main.updateDebugText("Store Kit is Supported: " + StoreKit.isSupported());
			
			//If this library is supported
			if(StoreKit.isSupported())
			{
				Main.updateDebugText("Store Kit is Available: " + StoreKit.storeKit.isStoreKitAvailable());
				
				//Make sure Parental controls are not disabling this
				if(StoreKit.storeKit.isStoreKitAvailable())
				{
					Main.updateDebugText("Store Kit is Available, adding event listeners");
					
					// add listeners here
					StoreKit.storeKit.addEventListener(StoreKitEvent.PRODUCT_DETAILS_LOADED, productLoadedSuccess);
					StoreKit.storeKit.addEventListener(StoreKitEvent.PURCHASE_SUCCEEDED,onPurchaseSuccess);
					StoreKit.storeKit.addEventListener(StoreKitEvent.PURCHASE_CANCELLED,onPurchaseUserCancelled);
					StoreKit.storeKit.addEventListener(StoreKitEvent.TRANSACTIONS_RESTORED, onTransactionsRestored);
					
					// adding error events. always listen for these to avoid your program failing.
					StoreKit.storeKit.addEventListener(StoreKitErrorEvent.PRODUCT_DETAILS_FAILED,onProductDetailsFailed);
					StoreKit.storeKit.addEventListener(StoreKitErrorEvent.PURCHASE_FAILED,onPurchaseFailed);
					StoreKit.storeKit.addEventListener(StoreKitErrorEvent.TRANSACTION_RESTORE_FAILED, onTransactionRestoreFailed);
					
					var inventoryVector:Vector.<String> = new Vector.<String>();
					inventoryVector.push("Full_Unlock");
					
					Main.updateDebugText("Loading Product Details");
					StoreKit.storeKit.loadProductDetails(inventoryVector);
					
				}
				else
				{
					Main.updateDebugText("Store Kit is NOT Available");
				}
			}
		}
		private function init():void
		{
			// create text
			var fontSize:int = PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor;
			var text:TextField = new TextField(_background.width, PathogenTest.hTenPercent * 2.5, "Unlock the premium game to gain access to: " + "\n" + " additional campaign levels, expanded local multiplayer, map editor, and online multiplayer!", "Questrial", fontSize, Color.WHITE);
			text.pivotX = text.width / 2;
			text.pivotY = text.height / 2;
			text.y = _background.y - PathogenTest.hTenPercent * .6;
			text.x = _background.x;
			addChild(text);
			
			//Create Unlock Button
			_unlockButton = new BW_Button(2.5, 1.0, "Unlock Premium Game", 24);
			addChild(_unlockButton);
			_unlockButton.y = text.y + (text.height/2 * 1.2);
			_unlockButton.x = _background.x - (_unlockButton.width * .6);
			_unlockButton.addEventListener(TouchEvent.TOUCH, onTouchUnlock);
			
			//Create Restore Button
			_restoreButton = new BW_Button(2.5, 1.0, "Restore Purchase", 24);
			addChild(_restoreButton);
			_restoreButton.y = _unlockButton.y;
			_restoreButton.x = _background.x + (_restoreButton.width * .6);
			_restoreButton.addEventListener(TouchEvent.TOUCH, onTouchRestore);
			
			// create Quit button
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_quitButton = new BW_Button(.75,1, "", 0);
			else
				_quitButton = new BW_Button(.5,.75, "", 0);
			
			_quitButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(_quitButton);
			_quitButton.x =  _background.x - _background.width/2 + (_quitButton.width * .75);
			_quitButton.y = _background.y + _background.height/2 - (_quitButton.width * .75);
			_quitButton.addEventListener(TouchEvent.TOUCH, onTouchQuit);
		}
		private function onTouchUnlock($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this,TouchPhase.ENDED);
			if (touch)
			{
				Main.updateDebugText("Start Purchase of Non-Consumable: Full Game Unlock");
				
				if(StoreKit.isSupported())
					StoreKit.storeKit.purchaseProduct("Full_Unlock");
			}
		}
		private function onTouchRestore($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this,TouchPhase.ENDED);
			if (touch)
			{	
				Main.updateDebugText("Requesting to Restore Transactions");
				
				if(StoreKit.isSupported())
					StoreKit.storeKit.restoreTransactions();
			}
		}
		protected function onTransactionRestoreFailed(event:StoreKitErrorEvent):void
		{
			Main.updateDebugText("Transaction Restore Failed");
		}
		
		protected function onPurchaseFailed(event:StoreKitErrorEvent):void
		{
			Main.updateDebugText("Transaction Purchase Failed");
		}
		
		protected function onProductDetailsFailed(event:StoreKitErrorEvent):void
		{
			Main.updateDebugText("Product Details Failed");
		}
		protected function onTransactionsRestored(event:StoreKitEvent):void
		{
			Main.updateDebugText("Transaction Restored Success! Receipt: " + event.receipt);
			
			if(Main._locked)
			{
				Main.updateDebugText("No Products to be restored");
				openNoPurchaseScreen();
			}
		}
		private function openNoPurchaseScreen():void
		{
			_NoPurchaseScreen = new NoPurchaseScreen(this);
			addChild(_NoPurchaseScreen);
		}
		public function removeNoPurchaseScreen():void
		{
			_NoPurchaseScreen.dispose();
			removeChild(_NoPurchaseScreen);
			_NoPurchaseScreen = null;
		}
		protected function onPurchaseUserCancelled(event:StoreKitEvent):void
		{
			Main.updateDebugText("User has Cancelled Purchase");
		}
		protected function onPurchaseSuccess($e:StoreKitEvent):void
		{
			Main.updateDebugText("Successful purchase of "+ $e.productId);
			
			//Change Locked Status
			Main._locked = false;
			Main.saveDataObject.data.liteStatus = Main._locked;
			Main.saveDataObject.flush();
			
			//Main.mobileAppTracker.trackAction("Premium Unlock", 2.99, "USD"); 
			
			removeListeners();
			startFadeOut();
		}
		private function productLoadedSuccess($e:StoreKitEvent):void
		{
			Main.updateDebugText("Product was successfully loaded");
			Main.updateDebugText("Loaded "+ $e.validProducts.length + " Products.");
		}
		private function onTouchQuit(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this,TouchPhase.ENDED);
			if (touch)
			{
				_quitButton.removeEventListener(TouchEvent.TOUCH, onTouchQuit);
				startFadeOut();
			}
		}
		private function removeListeners():void
		{
			_restoreButton.removeEventListener(TouchEvent.TOUCH, onTouchRestore);
			_quitButton.removeEventListener(TouchEvent.TOUCH, onTouchQuit);
			_unlockButton.removeEventListener(TouchEvent.TOUCH, onTouchUnlock);
		}
		override public function death():void
		{
			if(_unlockButton)
			{
				_unlockButton.dispose();
				_unlockButton.destroy();
				removeChild(_unlockButton);
				_unlockButton = null;
			}
			
			if(_quitButton)
			{
				_quitButton.dispose();
				_quitButton.destroy();
				removeChild(_quitButton);
				_quitButton = null;
			}
			
			if(_restoreButton)
			{
				_restoreButton.dispose();
				_restoreButton.destroy();
				removeChild(_restoreButton);
				_restoreButton = null;
			}
			
			if(StoreKit.isSupported())
			{
				if(StoreKit.storeKit.isStoreKitAvailable())
				{
					// add listeners here
					StoreKit.storeKit.removeEventListener(StoreKitEvent.PRODUCT_DETAILS_LOADED, productLoadedSuccess);
					StoreKit.storeKit.removeEventListener(StoreKitEvent.PURCHASE_SUCCEEDED,onPurchaseSuccess);
					StoreKit.storeKit.removeEventListener(StoreKitEvent.PURCHASE_CANCELLED,onPurchaseUserCancelled);
					StoreKit.storeKit.removeEventListener(StoreKitEvent.TRANSACTIONS_RESTORED, onTransactionsRestored);
					
					// adding error events. always listen for these to avoid your program failing.
					StoreKit.storeKit.removeEventListener(StoreKitErrorEvent.PRODUCT_DETAILS_FAILED,onProductDetailsFailed);
					StoreKit.storeKit.removeEventListener(StoreKitErrorEvent.PURCHASE_FAILED,onPurchaseFailed);
					StoreKit.storeKit.removeEventListener(StoreKitErrorEvent.TRANSACTION_RESTORE_FAILED, onTransactionRestoreFailed);
				}
			}
			
			if(_main)
				_main.removeContentLockedScreen();
			
			_main = null;
		}
	}
}