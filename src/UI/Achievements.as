package UI
{
	import Campaign.Campaign;
	
	import Core.GameManager;
	import Core.VictoryScreen;
	
	import Editor.MapEditor;
	
	import OnlinePlay.OnlineGameManager;
	import OnlinePlay.OnlineVictoryScreen;
	
	import com.milkmangames.nativeextensions.GoogleGames;
	import com.sticksports.nativeExtensions.gameCenter.GameCenter;
	
	import flash.display3D.IndexBuffer3D;
	import flash.net.SharedObject;
	
	import starling.display.Sprite;
	
	public class Achievements extends Sprite
	{
		//Classes
		private var _gameManager:GameManager;
		private var _mapEditor:MapEditor;
		private var _campaign:Campaign;
		private var _onlineGameManager:OnlineGameManager;
		private var _victoryScreen:VictoryScreen;
		private var _onlineVictoryScreen:OnlineVictoryScreen;
		
		//Static vars
		public static const COMPLETE:int = 100;
		public static const CAMPAIGN_PROGRESS:int = 20;
		
		public static const CREATING:String = "Creating";
		public static const CREATING_ANDROID:String = "Creating Android";
		public static const DESTROYING:String = "Destroying";
		public static const DESTROYING_ANDROID:String = "Destroying Android";
		
		//Level 5.1 - Outmanned
		public static const CAMPAIGN_5_1_EASY:String = "Completed_5.1_EASY";
		public static const CAMPAIGN_5_1_MEDIUM:String = "Completed_5.1_MEDIUM";
		public static const CAMPAIGN_5_1_HARD:String = "Completed_5.1_HARD";
		
		public static const CAMPAIGN_5_1_EASY_ANDROID:String = "CgkIpKbujbQbEAIQAw";
		public static const CAMPAIGN_5_1_MEDIUM_ANDROID:String = "CgkIpKbujbQbEAIQBA";
		public static const CAMPAIGN_5_1_HARD_ANDROID:String = "CgkIpKbujbQbEAIQBQ";
		
		private var _progress_5_1_EASY:int = 0;
		private var _progress_5_1_MEDIUM:int = 0;
		private var _progress_5_1_HARD:int = 0;
		
		//Level 5.2 - Capture Zone
		public static const CAMPAIGN_5_2_EASY:String = "Completed_5.2_EASY";
		public static const CAMPAIGN_5_2_MEDIUM:String = "Completed_5.2_MEDIUM";
		public static const CAMPAIGN_5_2_HARD:String = "Completed_5.2_HARD";
		
		public static const CAMPAIGN_5_2_EASY_ANDROID:String = "CgkIpKbujbQbEAIQBg";
		public static const CAMPAIGN_5_2_MEDIUM_ANDROID:String = "CgkIpKbujbQbEAIQCQ";
		public static const CAMPAIGN_5_2_HARD_ANDROID:String = "CgkIpKbujbQbEAIQCg";
		
		private var _progress_5_2_EASY:int = 0;
		private var _progress_5_2_MEDIUM:int = 0;
		private var _progress_5_2_HARD:int = 0;
		
		//Level 6.1 - Infection
		public static const CAMPAIGN_6_1_EASY:String = "Completed_6.1_EASY";
		public static const CAMPAIGN_6_1_MEDIUM:String = "Completed_6.1_MEDIUM";
		public static const CAMPAIGN_6_1_HARD:String = "Completed_6.1_HARD";
		
		public static const CAMPAIGN_6_1_EASY_ANDROID:String = "CgkIpKbujbQbEAIQCw";
		public static const CAMPAIGN_6_1_MEDIUM_ANDROID:String = "CgkIpKbujbQbEAIQDA";
		public static const CAMPAIGN_6_1_HARD_ANDROID:String = "CgkIpKbujbQbEAIQDQ";
		
		private var _progress_6_1_EASY:int = 0;
		private var _progress_6_1_MEDIUM:int = 0;
		private var _progress_6_1_HARD:int = 0;
		
		//Level 6.2 - Erosion Zone
		public static const CAMPAIGN_6_2_EASY:String = "Completed_6.2_EASY";
		public static const CAMPAIGN_6_2_MEDIUM:String = "Completed_6.2_MEDIUM";
		public static const CAMPAIGN_6_2_HARD:String = "Completed_6.2_HARD";
		
		public static const CAMPAIGN_6_2_EASY_ANDROID:String = "CgkIpKbujbQbEAIQDg";
		public static const CAMPAIGN_6_2_MEDIUM_ANDROID:String = "CgkIpKbujbQbEAIQDw";
		public static const CAMPAIGN_6_2_HARD_ANDROID:String = "CgkIpKbujbQbEAIQEA";
		
		private var _progress_6_2_EASY:int = 0;
		private var _progress_6_2_MEDIUM:int = 0;
		private var _progress_6_2_HARD:int = 0;
		
		//Level 7.1 - Underdog
		public static const CAMPAIGN_7_1_EASY:String = "Completed_7.1_EASY";
		public static const CAMPAIGN_7_1_MEDIUM:String = "Completed_7.1_MEDIUM";
		public static const CAMPAIGN_7_1_HARD:String = "Completed_7.1_HARD";
		
		public static const CAMPAIGN_7_1_EASY_ANDROID:String = "CgkIpKbujbQbEAIQEQ";
		public static const CAMPAIGN_7_1_MEDIUM_ANDROID:String = "CgkIpKbujbQbEAIQEg";
		public static const CAMPAIGN_7_1_HARD_ANDROID:String = "CgkIpKbujbQbEAIQEw";
		
		private var _progress_7_1_EASY:int = 0;
		private var _progress_7_1_MEDIUM:int = 0;
		private var _progress_7_1_HARD:int = 0;
		
		//Level 7.2 - Lightning
		public static const CAMPAIGN_7_2_EASY:String = "Completed_7.2_EASY";
		public static const CAMPAIGN_7_2_MEDIUM:String = "Completed_7.2_MEDIUM";
		public static const CAMPAIGN_7_2_HARD:String = "Completed_7.2_HARD";
		
		public static const CAMPAIGN_7_2_EASY_ANDROID:String = "CgkIpKbujbQbEAIQFA";
		public static const CAMPAIGN_7_2_MEDIUM_ANDROID:String = "CgkIpKbujbQbEAIQFQ";
		public static const CAMPAIGN_7_2_HARD_ANDROID:String = "CgkIpKbujbQbEAIQFg";
		
		private var _progress_7_2_EASY:int = 0;
		private var _progress_7_2_MEDIUM:int = 0;
		private var _progress_7_2_HARD:int = 0;
		
		//Level 8 - Final
		public static const CAMPAIGN_8_EASY:String = "Completed_8_EASY";
		public static const CAMPAIGN_8_MEDIUM:String = "Completed_8_MEDIUM";
		public static const CAMPAIGN_8_HARD:String = "Completed_8_HARD";
		
		public static const CAMPAIGN_8_EASY_ANDROID:String = "CgkIpKbujbQbEAIQFw";
		public static const CAMPAIGN_8_MEDIUM_ANDROID:String = "CgkIpKbujbQbEAIQGA";
		public static const CAMPAIGN_8_HARD_ANDROID:String = "CgkIpKbujbQbEAIQGQ";
		
		private var _progress_8_EASY:int = 0;
		private var _progress_8_MEDIUM:int = 0;
		private var _progress_8_HARD:int = 0;
		
		//Map Editor
		
		//Creating Maps
		public static const CREATE_MAP_1:String = "Create_Map_1";
		public static const CREATE_MAP_1_ANDROID:String = "CgkIpKbujbQbEAIQGw";
		private var _progress_Create_Map_1:int = 0;
		
		//Gameplay
		
		public static const COMPLETE_TUTORIAL:String = "1.0";
		public static const COMPLETE_TUTORIAL_ANDROID:String = "CgkIpKbujbQbEAIQAg";
		private var _progress_Complete_Tutorial:int = 0;
		
		//Game Result
		public static const TIE_GAME:String = "Tie_Game";
		public static const TIE_GAME_ANDROID:String = "CgkIpKbujbQbEAIQHA";
		private var _progress_Tie_Game:int = 0;
		
		public static const DOMINATION_GAME:String = "Domination_Game";
		public static const DOMINATION_GAME_ANDROID:String = "CgkIpKbujbQbEAIQHQ";
		private var _progress_Domination_Game:int = 0;
		
		//Game Statistics
		
		//Cell Creation
		private var _count_Create_Cells:int = 0;
		
		public static const CREATE_CELLS_100:String = "Create_Cells_100";
		public static const CREATE_CELLS_100_ANDROID:String = "CgkIpKbujbQbEAIQHg";
		private var _progress_Create_Cells_100:int = 0;
		private var _Threshold_Create_Cells_100:int = 100;
		
		public static const CREATE_CELLS_1000:String = "Create_Cells_1000";
		public static const CREATE_CELLS_1000_ANDROID:String = "CgkIpKbujbQbEAIQIA";
		private var _progress_Create_Cells_1000:int = 0;
		private var _Threshold_Create_Cells_1000:int = 1000;
		
		public static const CREATE_CELLS_5000:String = "Create_Cells_5000";
		public static const CREATE_CELLS_5000_ANDROID:String = "CgkIpKbujbQbEAIQIQ";
		private var _progress_Create_Cells_5000:int = 0;
		private var _Threshold_Create_Cells_5000:int = 5000;
		
		public static const CREATE_CELLS_25000:String = "Create_Cells_25000";
		public static const CREATE_CELLS_25000_ANDROID:String = "CgkIpKbujbQbEAIQIg";
		private var _progress_Create_Cells_25000:int = 0;
		private var _Threshold_Create_Cells_25000:int = 25000;
		
		public static const CREATE_CELLS_50000:String = "Create_Cells_50000";
		public static const CREATE_CELLS_50000_ANDROID:String = "CgkIpKbujbQbEAIQIw";
		private var _progress_Create_Cells_50000:int = 0;
		private var _Threshold_Create_Cells_50000:int = 50000;
		
		public static const CREATE_CELLS_100000:String = "Create_Cells_100000";
		public static const CREATE_CELLS_100000_ANDROID:String = "CgkIpKbujbQbEAIQJA";
		private var _progress_Create_Cells_100000:int = 0;
		private var _Threshold_Create_Cells_100000:int = 100000;
		
		//Cell Destruction
		private var _count_Destroy_Cells:int = 0;
		
		public static const DESTROY_CELLS_100:String = "Destroy_Cells_100";
		public static const DESTROY_CELLS_100_ANDROID:String = "CgkIpKbujbQbEAIQHw";
		private var _progress_Destroy_Cells_100:int = 0;
		private var _Threshold_Destroy_Cells_100:int = 100;
		
		public static const DESTROY_CELLS_500:String = "Destroy_Cells_500";
		public static const DESTROY_CELLS_500_ANDROID:String = "CgkIpKbujbQbEAIQJQ";
		private var _progress_Destroy_Cells_500:int = 0;
		private var _Threshold_Destroy_Cells_500:int = 500;
		
		public static const DESTROY_CELLS_1000:String = "Destroy_Cells_1000";
		public static const DESTROY_CELLS_1000_ANDROID:String = "CgkIpKbujbQbEAIQJg";
		private var _progress_Destroy_Cells_1000:int = 0;
		private var _Threshold_Destroy_Cells_1000:int = 1000;
		
		public static const DESTROY_CELLS_2500:String = "Destroy_Cells_2500";
		public static const DESTROY_CELLS_2500_ANDROID:String = "CgkIpKbujbQbEAIQJw";
		private var _progress_Destroy_Cells_2500:int = 0;
		private var _Threshold_Destroy_Cells_2500:int = 2500;
		
		public static const DESTROY_CELLS_5000:String = "Destroy_Cells_5000";
		public static const DESTROY_CELLS_5000_ANDROID:String = "CgkIpKbujbQbEAIQKA";
		private var _progress_Destroy_Cells_5000:int = 0;
		private var _Threshold_Destroy_Cells_5000:int = 5000;
		
		// online
		
		// creating one online game
		public static const PLAY_ONLINE_CUSTOM_1:String = "Play_Online_Custom_1";
		public static const PLAY_ONLINE_CUSTOM_1_ANDROID:String = "CgkIpKbujbQbEAIQGg";
		private var _progress_Play_Online_Custom_1:int = 0;
		
		//Save
		private var sharedData:SharedObject;
		
		public function Achievements()
		{
			super();
			
			sharedData = Main.saveDataObject;
			
			//Load the saved progress of these Achievements
			
			//Level 5.1
			if (sharedData.data.progress_5_1_EASY != null)
				_progress_5_1_EASY = sharedData.data.progress_5_1_EASY;
			if (sharedData.data.progress_5_1_MEDIUM != null)
				_progress_5_1_MEDIUM = sharedData.data.progress_5_1_MEDIUM;
			if (sharedData.data.progress_5_1_HARD != null)
				_progress_5_1_HARD = sharedData.data.progress_5_1_HARD;
			
			//Level 5.2
			if (sharedData.data.progress_5_2_EASY != null)
				_progress_5_2_EASY = sharedData.data.progress_5_2_EASY;
			if (sharedData.data.progress_5_2_MEDIUM != null)
				_progress_5_2_MEDIUM = sharedData.data.progress_5_2_MEDIUM;
			if (sharedData.data.progress_5_2_HARD != null)
				_progress_5_2_HARD = sharedData.data.progress_5_2_HARD;
			
			//Level 6.1
			if (sharedData.data.progress_6_1_EASY != null)
				_progress_6_1_EASY = sharedData.data.progress_6_1_EASY;
			if (sharedData.data.progress_6_1_MEDIUM != null)
				_progress_6_1_MEDIUM = sharedData.data.progress_6_1_MEDIUM;
			if (sharedData.data.progress_6_1_HARD != null)
				_progress_6_1_HARD = sharedData.data.progress_6_1_HARD;
			
			//Level 6.2
			if (sharedData.data.progress_6_2_EASY != null)
				_progress_6_2_EASY = sharedData.data.progress_6_2_EASY;
			if (sharedData.data.progress_6_2_MEDIUM != null)
				_progress_6_2_MEDIUM = sharedData.data.progress_6_2_MEDIUM;
			if (sharedData.data.progress_6_1_HARD != null)
				_progress_6_2_HARD = sharedData.data.progress_6_2_HARD;
			
			//Level 7.1
			if (sharedData.data.progress_7_1_EASY != null)
				_progress_7_1_EASY = sharedData.data.progress_7_1_EASY;
			if (sharedData.data.progress_7_1_MEDIUM != null)
				_progress_7_1_MEDIUM = sharedData.data.progress_7_1_MEDIUM;
			if (sharedData.data.progress_7_1_HARD != null)
				_progress_7_1_HARD = sharedData.data.progress_7_1_HARD;
			
			//Level 7.2
			if (sharedData.data.progress_7_2_EASY != null)
				_progress_7_2_EASY = sharedData.data.progress_7_2_EASY;
			if (sharedData.data.progress_7_2_MEDIUM != null)
				_progress_7_2_MEDIUM = sharedData.data.progress_7_2_MEDIUM;
			if (sharedData.data.progress_7_2_HARD != null)
				_progress_7_2_HARD = sharedData.data.progress_7_2_HARD;
			
			//Level 7.2
			if (sharedData.data.progress_8_EASY != null)
				_progress_8_EASY = sharedData.data.progress_8_EASY;
			if (sharedData.data.progress_8_MEDIUM != null)
				_progress_8_MEDIUM = sharedData.data.progress_8_MEDIUM;
			if (sharedData.data.progress_8_HARD != null)
				_progress_8_HARD = sharedData.data.progress_8_HARD;
			
			//Map Editor
			
			//Create 1 Map
			if (sharedData.data.progress_Create_Map_1 != null)
				_progress_Create_Map_1 = sharedData.data.progress_Create_Map_1;
			
			//Gameplay
			
			//Game Results
			if (sharedData.data.progress_Tie_Game != null)
				_progress_Tie_Game = sharedData.data.progress_Tie_Game;
			
			if (sharedData.data.progress_Domination_Game != null)
				_progress_Domination_Game = sharedData.data.progress_Domination_Game;
			
			//Game Statistics
			
			if (sharedData.data.progress_Complete_Tutorial != null)
				_progress_Complete_Tutorial= sharedData.data.progress_Complete_Tutorial;
			
			//Creating Cells
			if (sharedData.data.count_Create_Cells != null)
				_count_Create_Cells = sharedData.data.count_Create_Cells;
			
			if (sharedData.data.progress_Create_Cells_100 != null)
				_progress_Create_Cells_100 = sharedData.data.progress_Create_Cells_100;
			
			if (sharedData.data.progress_Create_Cells_1000 != null)
				_progress_Create_Cells_1000 = sharedData.data.progress_Create_Cells_1000;
			
			if (sharedData.data.progress_Create_Cells_5000 != null)
				_progress_Create_Cells_5000 = sharedData.data.progress_Create_Cells_5000;
			
			if (sharedData.data.progress_Create_Cells_25000 != null)
				_progress_Create_Cells_25000 = sharedData.data.progress_Create_Cells_25000;
			
			if (sharedData.data.progress_Create_Cells_50000 != null)
				_progress_Create_Cells_50000 = sharedData.data.progress_Create_Cells_50000;
			
			if (sharedData.data.progress_Create_Cells_100000 != null)
				_progress_Create_Cells_100000 = sharedData.data.progress_Create_Cells_100000;
			
			//Destroying Cells
			if (sharedData.data.count_Destroy_Cells != null)
				_count_Destroy_Cells = sharedData.data.count_Destroy_Cells;
			
			if (sharedData.data.progress_Destroy_Cells_100 != null)
				_progress_Destroy_Cells_100 = sharedData.data.progress_Destroy_Cells_100;
			
			if (sharedData.data.progress_Destroy_Cells_500 != null)
				_progress_Destroy_Cells_500 = sharedData.data.progress_Destroy_Cells_500;
			
			if (sharedData.data.progress_Destroy_Cells_1000 != null)
				_progress_Destroy_Cells_1000 = sharedData.data.progress_Destroy_Cells_1000;
			
			if (sharedData.data.progress_Destroy_Cells_2500 != null)
				_progress_Destroy_Cells_2500 = sharedData.data.progress_Destroy_Cells_2500;
			
			if (sharedData.data._progress_Destroy_Cells_5000 != null)
				_progress_Destroy_Cells_5000 = sharedData.data._progress_Destroy_Cells_5000;
			
			// online
			
			// create 1 online game
			if (sharedData.data.progress_Play_Online_Custom_1 != null)
				_progress_Play_Online_Custom_1 = sharedData.data.progress_Play_Online_Custom_1;
			
		}
		public function updateAchievementProgress(achievement:String):void
		{
			//Main.updateDebugText("Updating Achievement Progress of: " + achievement);
			
			var progress:int = 0;
			var remainder:int;
			var achievementNumber:int;
			
			switch(achievement)
			{
				// iOS
				case CAMPAIGN_5_1_EASY:
					_progress_5_1_EASY = COMPLETE;
					progress = _progress_5_1_EASY;
					sharedData.data.progress_5_1_EASY = _progress_5_1_EASY;
					achievementNumber = 1;
					break;
				
				case CAMPAIGN_5_1_MEDIUM:
					_progress_5_1_MEDIUM = COMPLETE;
					progress = _progress_5_1_MEDIUM;
					sharedData.data.progress_5_1_MEDIUM = _progress_5_1_MEDIUM;
					achievementNumber = 2;
					break;
				
				case CAMPAIGN_5_1_HARD:
					_progress_5_1_HARD = COMPLETE;
					progress = _progress_5_1_HARD;
					sharedData.data.progress_5_1_HARD = _progress_5_1_HARD
					achievementNumber = 3;
					break;
				
				case CAMPAIGN_5_2_EASY:
					_progress_5_2_EASY = COMPLETE;
					progress = _progress_5_2_EASY;
					sharedData.data.progress_5_2_EASY = _progress_5_2_EASY;
					achievementNumber = 4;
					break;
				
				case CAMPAIGN_5_2_MEDIUM:
					_progress_5_2_MEDIUM = COMPLETE;
					progress = _progress_5_2_MEDIUM;
					sharedData.data.progress_5_2_MEDIUM = _progress_5_2_MEDIUM;
					achievementNumber = 5;
					break;
				
				case CAMPAIGN_5_2_HARD:
					_progress_5_2_HARD = COMPLETE;
					progress = _progress_5_2_HARD;
					sharedData.data.progress_5_2_HARD = _progress_5_2_HARD;
					achievementNumber = 6;
					break;
				
				case CAMPAIGN_6_1_EASY:
					_progress_6_1_EASY = COMPLETE;
					progress = _progress_6_1_EASY;
					sharedData.data.progress_6_1_EASY = _progress_6_1_EASY;
					achievementNumber = 7;
					break;
				
				case CAMPAIGN_6_1_MEDIUM:
					_progress_6_1_MEDIUM = COMPLETE;
					progress = _progress_6_1_MEDIUM;
					sharedData.data.progress_6_1_MEDIUM = _progress_6_1_MEDIUM;
					achievementNumber = 8;
					break;
				
				case CAMPAIGN_6_1_HARD:
					_progress_6_1_HARD = COMPLETE;
					progress = _progress_6_1_HARD;
					sharedData.data.progress_6_1_HARD = _progress_6_1_HARD;
					achievementNumber = 9;
					break;
				
				case CAMPAIGN_6_2_EASY:
					_progress_6_2_EASY = COMPLETE;
					progress = _progress_6_2_EASY;
					sharedData.data.progress_6_2_EASY = _progress_6_2_EASY;
					achievementNumber = 10;
					break;
				
				case CAMPAIGN_6_2_MEDIUM:
					_progress_6_2_MEDIUM = COMPLETE;
					progress = _progress_6_2_MEDIUM;
					sharedData.data.progress_6_2_MEDIUM = _progress_6_2_MEDIUM;
					achievementNumber = 11;
					break;
				
				case CAMPAIGN_6_2_HARD:
					_progress_6_2_HARD = COMPLETE;
					progress = _progress_6_2_HARD;
					sharedData.data.progress_6_2_HARD = _progress_6_2_HARD;
					achievementNumber = 12;
					break;
				
				case CAMPAIGN_7_1_EASY:
					_progress_7_1_EASY = COMPLETE;
					progress = _progress_7_1_EASY;
					sharedData.data.progress_7_1_EASY = _progress_7_1_EASY;
					achievementNumber = 13;
					break;
				
				case CAMPAIGN_7_1_MEDIUM:
					_progress_7_1_MEDIUM = COMPLETE;
					progress = _progress_7_1_MEDIUM;
					sharedData.data.progress_6_1_MEDIUM = _progress_7_1_MEDIUM;
					achievementNumber = 14;
					break;
				
				case CAMPAIGN_7_1_HARD:
					_progress_7_1_HARD = COMPLETE;
					progress = _progress_7_1_HARD;
					sharedData.data.progress_7_1_HARD = _progress_7_1_HARD;
					achievementNumber = 15;
					break;
				
				case CAMPAIGN_7_2_EASY:
					_progress_7_2_EASY = COMPLETE;
					progress = _progress_7_2_EASY;
					sharedData.data.progress_7_2_EASY = _progress_7_2_EASY;
					achievementNumber = 16;
					break;
				
				case CAMPAIGN_7_2_MEDIUM:
					_progress_7_2_MEDIUM = COMPLETE;
					progress = _progress_7_2_MEDIUM;
					sharedData.data.progress_7_2_MEDIUM = _progress_7_2_MEDIUM;
					achievementNumber = 17;
					break;
				
				case CAMPAIGN_7_2_HARD:
					_progress_7_2_HARD = COMPLETE;
					progress = _progress_7_2_HARD;
					sharedData.data.progress_7_2_HARD = _progress_7_2_HARD;
					achievementNumber = 18;
					break;
				
				case CAMPAIGN_8_EASY:
					_progress_8_EASY = COMPLETE;
					progress = _progress_8_EASY;
					sharedData.data.progress_8_EASY = _progress_8_EASY;
					achievementNumber = 19;
					break;
				
				case CAMPAIGN_8_MEDIUM:
					_progress_8_MEDIUM = COMPLETE;
					progress = _progress_8_MEDIUM;
					sharedData.data.progress_8_MEDIUM = _progress_8_MEDIUM;
					achievementNumber = 20;
					break;
				
				case CAMPAIGN_8_HARD:
					_progress_8_HARD = COMPLETE;
					progress = _progress_8_HARD;
					sharedData.data.progress_8_HARD = _progress_8_HARD;
					achievementNumber = 21;
					break;
				
				case CREATE_MAP_1:
					_progress_Create_Map_1 = COMPLETE;
					progress = _progress_Create_Map_1;
					sharedData.data.progress_Create_Map_1 = _progress_Create_Map_1;
					achievementNumber = 23;
					break;
				
				case TIE_GAME:
					_progress_Tie_Game = COMPLETE;
					progress = _progress_Tie_Game;
					sharedData.data.progress_Tie_Game = _progress_Tie_Game;
					achievementNumber = 24;
					break;
				
				case DOMINATION_GAME:
					_progress_Domination_Game = COMPLETE;
					progress = _progress_Domination_Game;
					sharedData.data.progress_Domination_Game = _progress_Domination_Game;
					achievementNumber = 25;
					break;
					
				case CREATE_CELLS_100:
					_progress_Create_Cells_100 = COMPLETE;
					progress = _progress_Create_Cells_100;
					sharedData.data.progress_Create_Cells_100 = _progress_Create_Cells_100;
					achievementNumber = 26;
					break;
				
				case CREATE_CELLS_1000:
					_progress_Create_Cells_1000 = COMPLETE;
					progress = _progress_Create_Cells_1000;
					sharedData.data.progress_Create_Cells_1000 = _progress_Create_Cells_1000;
					achievementNumber = 27;
					break;
				
				case CREATE_CELLS_5000:
					_progress_Create_Cells_5000 = COMPLETE;
					progress = _progress_Create_Cells_5000;
					sharedData.data.progress_Create_Cells_5000 = _progress_Create_Cells_5000;
					achievementNumber = 28;
					break;
				
				case CREATE_CELLS_25000:
					_progress_Create_Cells_25000 = COMPLETE;
					progress = _progress_Create_Cells_25000;
					sharedData.data.progress_Create_Cells_25000 = _progress_Create_Cells_25000;
					achievementNumber = 29;
					break;
				
				case CREATE_CELLS_50000:
					_progress_Create_Cells_50000 = COMPLETE;
					progress = _progress_Create_Cells_50000;
					sharedData.data.progress_Create_Cells_50000 = _progress_Create_Cells_50000;
					achievementNumber = 30;
					break;
				
				case CREATE_CELLS_100000:
					_progress_Create_Cells_100000 = COMPLETE;
					progress = _progress_Create_Cells_100000;
					sharedData.data.progress_Create_Cells_100000 = _progress_Create_Cells_100000;
					achievementNumber = 31;
					break;
				
				case DESTROY_CELLS_100:
					_progress_Destroy_Cells_100 = COMPLETE;
					progress = _progress_Destroy_Cells_100;
					sharedData.data.progress_Destroy_Cells_100 = _progress_Destroy_Cells_100;
					achievementNumber = 32;
					break;
				
				case DESTROY_CELLS_500:
					_progress_Destroy_Cells_500 = COMPLETE;
					progress = _progress_Destroy_Cells_500;
					sharedData.data.progress_Destroy_Cells_500 = _progress_Destroy_Cells_500;
					achievementNumber = 33;
					break;
				
				case DESTROY_CELLS_1000:
					_progress_Destroy_Cells_1000 = COMPLETE;
					progress = _progress_Destroy_Cells_1000;
					sharedData.data.progress_Destroy_Cells_1000 = _progress_Destroy_Cells_1000;
					achievementNumber = 34;
					break;
				
				case DESTROY_CELLS_2500:
					_progress_Destroy_Cells_2500 = COMPLETE;
					progress = _progress_Destroy_Cells_2500;
					sharedData.data.progress_Destroy_Cells_2500 = _progress_Destroy_Cells_2500;
					achievementNumber = 35;
					break;
				
				case DESTROY_CELLS_5000:
					_progress_Destroy_Cells_5000 = COMPLETE;
					progress = _progress_Destroy_Cells_5000;
					sharedData.data.progress_Destroy_Cells_5000 = _progress_Destroy_Cells_5000;
					achievementNumber = 36;
					break;
				
				case PLAY_ONLINE_CUSTOM_1:
					_progress_Play_Online_Custom_1 = COMPLETE;
					progress = _progress_Play_Online_Custom_1;
					sharedData.data.progress_Play_Online_Custom_1 = _progress_Play_Online_Custom_1
					achievementNumber = 22;
					break;
				
				case COMPLETE_TUTORIAL:
					_progress_Complete_Tutorial = COMPLETE;
					progress = _progress_Complete_Tutorial;
					sharedData.data.progress_Complete_Tutorial = _progress_Complete_Tutorial;
					achievementNumber = 0;
					break;
			}
			
			//Save out to memory
			sharedData.flush();
			
			if(Main._lite)
			{
				//achievement = "Lite_" + achievement;
				
				//Main.updateDebugText("Logging Achievement: " + achievement);
				if(Main.isIOS())
				{
					if (GameCenter.isSupported && progress != 0 && GameCenter.isAuthenticated)
						GameCenter.reportAchievement(achievement,progress,true);
				}
			}
			else
			{
				//Main.updateDebugText("Logging Achievement: " + achievement);
				if (Main.isIOS())
				{
					if(GameCenter.isSupported && progress != 0 && GameCenter.isAuthenticated)
						GameCenter.reportAchievement(achievement,progress,true);
				}
				
				//Display Popup
				if(Main.isAndroid())
				{
					if(progress != 0)
					{
						if(_victoryScreen)
							_victoryScreen.addAchievementPopUp(achievementNumber);
						else if(_onlineVictoryScreen)
							_onlineVictoryScreen.addAchievementPopUp(achievementNumber);
						else if(_gameManager)
							_gameManager.addAchievementPopUp(achievementNumber);
						else if(_campaign)
							_campaign.addAchievementPopUp(achievementNumber);
						else if(_mapEditor)
							_mapEditor.addAchievementPopUp(achievementNumber);
						else if(_onlineGameManager)
							_onlineGameManager.addAchievementPopUp(achievementNumber);
					}
				}
			}
		}
		public function updatePlayerStatistics(statisticType:String, howMuch:int):void
		{
			//Main.updateDebugText("update with " + statisticType);
			
			switch(statisticType)
			{
				case CREATING:
				{
					_count_Create_Cells += howMuch;
					
					if(_progress_Create_Cells_100 != COMPLETE && _count_Create_Cells >= _Threshold_Create_Cells_100)
						updateAchievementProgress(CREATE_CELLS_100);
					
					if(_progress_Create_Cells_1000 != COMPLETE && _count_Create_Cells >= _Threshold_Create_Cells_1000)
						updateAchievementProgress(CREATE_CELLS_1000);
					
					if(_progress_Create_Cells_5000 != COMPLETE && _count_Create_Cells >= _Threshold_Create_Cells_5000)
						updateAchievementProgress(CREATE_CELLS_5000);
					
					if(_progress_Create_Cells_25000 != COMPLETE && _count_Create_Cells >= _Threshold_Create_Cells_25000)
						updateAchievementProgress(CREATE_CELLS_25000);
					
					if(_progress_Create_Cells_50000 != COMPLETE && _count_Create_Cells >= _Threshold_Create_Cells_50000)
						updateAchievementProgress(CREATE_CELLS_50000);
					
					if(_progress_Create_Cells_100000 != COMPLETE && _count_Create_Cells >= _Threshold_Create_Cells_100000)
						updateAchievementProgress(CREATE_CELLS_100000);
					
					break;
				}
					
				case DESTROYING:
				{
					_count_Destroy_Cells += howMuch;
					
					if(_progress_Destroy_Cells_100 != COMPLETE && _count_Destroy_Cells >= _Threshold_Destroy_Cells_100)
						updateAchievementProgress(DESTROY_CELLS_100);
					
					if(_progress_Destroy_Cells_500 != COMPLETE && _count_Destroy_Cells >= _Threshold_Destroy_Cells_500)
						updateAchievementProgress(DESTROY_CELLS_500);
					
					if(_progress_Destroy_Cells_1000 != COMPLETE && _count_Destroy_Cells >= _Threshold_Destroy_Cells_1000)
						updateAchievementProgress(DESTROY_CELLS_1000);
					
					if(_progress_Destroy_Cells_2500 != COMPLETE && _count_Destroy_Cells >= _Threshold_Destroy_Cells_2500)
						updateAchievementProgress(DESTROY_CELLS_2500);
					
					if(_progress_Destroy_Cells_5000 != COMPLETE && _count_Destroy_Cells >= _Threshold_Destroy_Cells_5000)
						updateAchievementProgress(DESTROY_CELLS_5000);
					
					break;
				}	
			}
		}
		
		public function get progress_5_1_EASY():int {return _progress_5_1_EASY};
		public function get progress_5_1_MEDIUM():int {return _progress_5_1_MEDIUM};
		public function get progress_5_1_HARD():int {return _progress_5_1_HARD};
		
		public function get progress_5_2_EASY():int {return _progress_5_2_EASY};
		public function get progress_5_2_MEDIUM():int {return _progress_5_2_MEDIUM};
		public function get progress_5_2_HARD():int {return _progress_5_2_HARD};
		
		public function get progress_6_1_EASY():int {return _progress_6_1_EASY};
		public function get progress_6_1_MEDIUM():int {return _progress_6_1_MEDIUM};
		public function get progress_6_1_HARD():int {return _progress_6_1_HARD};
		
		public function get progress_6_2_EASY():int {return _progress_6_2_EASY};
		public function get progress_6_2_MEDIUM():int {return _progress_6_2_MEDIUM};
		public function get progress_6_2_HARD():int {return _progress_6_2_HARD};
		
		public function get progress_7_1_EASY():int {return _progress_7_1_EASY};
		public function get progress_7_1_MEDIUM():int {return _progress_7_1_MEDIUM};
		public function get progress_7_1_HARD():int {return _progress_7_1_HARD};
		
		public function get progress_7_2_EASY():int {return _progress_7_2_EASY};
		public function get progress_7_2_MEDIUM():int {return _progress_7_2_MEDIUM};
		public function get progress_7_2_HARD():int {return _progress_7_2_HARD};
		
		public function get progress_8_EASY():int {return _progress_8_EASY};
		public function get progress_8_MEDIUM():int {return _progress_8_MEDIUM};
		public function get progress_8_HARD():int {return _progress_8_HARD};
		
		public function get progress_Create_Map_1():int {return _progress_Create_Map_1};
		
		public function get progress_Tie_Game():int {return _progress_Tie_Game};
		public function get progress_Domination_Game():int {return _progress_Domination_Game};
		
		public function get progress_Play_Online_Custom_1():int {return _progress_Play_Online_Custom_1};
		public function get progress_Complete_Tutorial():int { return _progress_Complete_Tutorial};
	
		public function get progress_Create_100_Cells():int {return _progress_Create_Cells_100};
		public function get progress_Create_1000_Cells():int {return _progress_Create_Cells_1000};
		public function get progress_Create_5000_Cells():int {return _progress_Create_Cells_5000};
		public function get progress_Create_25000_Cells():int {return _progress_Create_Cells_25000};
		public function get progress_Create_50000_Cells():int {return _progress_Create_Cells_50000};
		public function get progress_Create_100000_Cells():int {return _progress_Create_Cells_100000};
		
		public function get progress_Destroy_100_Cells():int {return _progress_Destroy_Cells_100};
		public function get progress_Destroy_500_Cells():int {return _progress_Destroy_Cells_500};
		public function get progress_Destroy_1000_Cells():int {return _progress_Destroy_Cells_1000};
		public function get progress_Destroy_2500_Cells():int {return _progress_Destroy_Cells_2500};
		public function get progress_Destroy_5000_Cells():int {return _progress_Destroy_Cells_5000};
		
		//Classes
		public function get campaign():Campaign {return _campaign};
		public function get gameManager():GameManager {return _gameManager};
		public function get onlineGameManager():OnlineGameManager {return _onlineGameManager};
		public function get mapEditor():MapEditor {return _mapEditor};
		public function get victoryScreen():VictoryScreen {return _victoryScreen};
		public function get onlineVictoryScreen():OnlineVictoryScreen {return _onlineVictoryScreen};
		
		public function set campaign(campaign:Campaign):void {_campaign = campaign};
		public function set gameManager(gameManager:GameManager):void {_gameManager = gameManager};
		public function set onlineGameManager(onlinegameManager:OnlineGameManager):void {_onlineGameManager = onlinegameManager};
		public function set mapEditor(mapEditor:MapEditor):void {_mapEditor = mapEditor};
		public function set victoryScreen(victoryScreen:VictoryScreen):void {_victoryScreen = victoryScreen};
		public function set onlineVictoryScreen(onlineVictoryScreen:OnlineVictoryScreen):void {_onlineVictoryScreen = onlineVictoryScreen};
		
	}
}