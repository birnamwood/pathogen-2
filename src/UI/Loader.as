package UI
{
	import Utils.BW_UI;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class Loader extends BW_UI
	{
		private var _loadingText:TextField;
		private var _loader:MovieClip
		private var _loaderImg:Image;
		
		public function Loader(loaderText:String, textFontSize:int, moving:Boolean=true)
		{
			super();
			
			if (moving)
			{
				// display a sub-texture
				_loader = new MovieClip(Main.loaderTextureAtlas.getTextures("Loader_"), Main.animationFramerate);
				addChild(_loader);
				_loader.pivotX = _loader.width/2;
				_loader.pivotY = _loader.height/2;
				_loader.scaleX = 1 / PathogenTest.scaleFactor;
				_loader.scaleY = 1 / PathogenTest.scaleFactor;
				Starling.juggler.add(_loader);
			}
			else
			{
				_loaderImg = new Image(Main.loaderTextureAtlas.getTexture("Loader_025"));
				addChild(_loaderImg);
				_loaderImg.pivotX = _loaderImg.width / 2;
				_loaderImg.pivotY = _loaderImg.height / 2;
				_loaderImg.scaleX = 1 / PathogenTest.scaleFactor;
				_loaderImg.scaleY = 1 / PathogenTest.scaleFactor;
			}
			
			// set up the loading text
			var fontSize:int = PathogenTest.HD_Multiplyer * textFontSize / PathogenTest.scaleFactor;
			_loadingText = new TextField(PathogenTest.stageWidth / 2, PathogenTest.hTenPercent, loaderText, "Questrial", fontSize, Color.WHITE);
			_loadingText.pivotX = _loadingText.width / 2;
			_loadingText.pivotY = _loadingText.height / 2;
			
			if (moving)
				_loadingText.y = _loader.y + (_loader.height * .9);
			else
				_loadingText.y = _loaderImg.y + (_loaderImg.height * .9);
			addChild(_loadingText);
		}
		public function updateText(text:String):void
		{
			_loadingText.text = text;
			_loadingText.pivotX = _loadingText.width / 2;
			_loadingText.pivotY = _loadingText.height / 2;
		}
		public override function death():void
		{
			if (_loader)
			{
				Starling.juggler.remove(_loader);
				_loader.dispose();
				removeChild(_loader);
				_loader = null;
			}
			
			if (_loaderImg)
			{
				_loaderImg.dispose();
				removeChild(_loaderImg);
				_loaderImg = null
			}
			
			if (_loadingText)
			{
				_loadingText.dispose();
				removeChild(_loadingText);
				_loadingText = null;
			}
		}
	}
}