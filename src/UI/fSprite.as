package UI
{
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class fSprite extends Sprite
	{
		public function fSprite()
		{
			super();
		}
		public function startfadeIn():void
		{
			removeEventListener(Event.ENTER_FRAME, fadeOut);
			addEventListener(Event.ENTER_FRAME, fadeIn);
		}
		private function fadeIn($e:Event):void
		{
			alpha += Main.FADE_SPEED;
			
			if(alpha >= 1)
			{
				removeEventListener(Event.ENTER_FRAME, fadeIn);
			}
		}
		public function startfadeOut():void
		{
			removeEventListener(Event.ENTER_FRAME, fadeIn);
			addEventListener(Event.ENTER_FRAME, fadeOut);
		}
		private function fadeOut($e:Event):void
		{
			alpha -= Main.FADE_SPEED;
			
			if(alpha <= 0)
			{
				removeEventListener(Event.ENTER_FRAME, fadeOut);
				this.dispose();
				this.parent.removeChild(this);
			}
		}
	}
}