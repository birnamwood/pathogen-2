package UI
{
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import com.sticksports.nativeExtensions.gameCenter.GameCenter;
	import com.milkmangames.nativeextensions.GoogleGames;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class OnlineUnavailable extends BW_UI
	{
		//Classes
		private var _mainMenu:MainMenu;
		private var _okButton:BW_Button;
		
		public function OnlineUnavailable(mainMenu:MainMenu)
		{
			super();
			
			_mainMenu = mainMenu;
			
			// create a transparent background
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			addChild(transBackground);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			transBackground.alpha = .75;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			//Creating background so that content stands out.
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 5;
			background.height = PathogenTest.hTenPercent * 4.5;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			background.color = 0x222245;
			
			// create text
			var fontSize:int = PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor;
			var textX:int = transBackground.width;
			if (textX > Main.TEXTURE_LIMIT)
				textX = Main.TEXTURE_LIMIT;
			var text:TextField = new TextField(textX, PathogenTest.hTenPercent * 2, "Online is currently unavailable", "Questrial", fontSize, Color.WHITE);
			text.pivotX = text.width / 2;
			text.pivotY = text.height / 2;
			text.y -= text.height / 2;
			addChild(text);
			
			//Main.updateDebugText("onlineunavailable");
			
			if (Main.isKindle())
			//if(PathogenTest.device == PathogenTest.KINDLE_FIRE || PathogenTest.device == PathogenTest.KINDLE_FIRE_HD || PathogenTest.device == PathogenTest.KINDLE_FIRE_HDX || PathogenTest.device == PathogenTest.KINDLE_FIRE_HDX_8inch)
			{
				text.text = "Online is currently unavailable";
			}
			else
			{
				if(!mainMenu.main.internet)
					text.text = "Please connect to the internet";
				else
				{
					if (Main.isIOS())
					{
						if(!GameCenter.isAuthenticated)
							text.text = "Please connect to Game " + "\n" + "Center to play online.";
					}
					else if (Main.isAndroid())
					{
						//Main.updateDebugText("here");
						text.text = "Online is currently unavailable";
						
						//if (!GoogleGames.games.isSignedIn())
							//text.text = "Please connect to Google\nGames to play online";
					}
				}
			}
			
			// create ok button
			_okButton = new BW_Button(1.0, 1.0, "OK", 36);
			addChild(_okButton);
			_okButton.y += _okButton.height;
			_okButton.addEventListener(TouchEvent.TOUCH, onTouchOK);
			
			Main.updateDebugText("finished onlineunavailable");
		}
		
		private function onTouchOK(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this,TouchPhase.ENDED);
			if (touch)
			{
				_okButton.removeEventListener(TouchEvent.TOUCH, onTouchOK);
				startFadeOut();
			}
		}
		override public function death():void
		{
			_okButton.dispose();
			_okButton.destroy();
			removeChild(_okButton);
			_okButton = null;
			
			_mainMenu.removeOnlineUnavailable();
			_mainMenu = null;
		}
	}
}