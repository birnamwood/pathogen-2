package UI
{
	import Utils.BW_Window;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class WelcomeBackScreen extends BW_Window
	{
		private const FADE_COUNT:int = 90;
		private var _nickname:TextField;
		private var _fadeCounter:int;
		private var _main:Main;
		
		public function WelcomeBackScreen(main:Main)
		{
			super();

			_main = main;
			createbackground(4.5, 3);
			createHeader("Welcome Back")
			init();
		}
		private function init():void
		{
			var textInputBackground:Quad = new Quad(PathogenTest.hTenPercent * 3, PathogenTest.hTenPercent * 0.65, Color.BLACK);
			textInputBackground.pivotX = textInputBackground.width/2;
			textInputBackground.pivotY = textInputBackground.height/2;
			addChild(textInputBackground);
			textInputBackground.alpha = .5;
			textInputBackground.x = _background.x;
			textInputBackground.y = _background.y;
			
			_nickname = new TextField(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent, Main.sOnlineNickname, "Dekar", PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor, Color.WHITE);
			_nickname.pivotX = _nickname.width / 2;
			_nickname.pivotY = _nickname.height / 2;
			_nickname.x = textInputBackground.x;
			_nickname.y = textInputBackground.y;
			addChild(_nickname);
			
			_fadeCounter = 0;
			addEventListener(Event.ENTER_FRAME, update);
		}
		private function update(event:Event):void
		{
			_fadeCounter += 1;
			if (_fadeCounter >= FADE_COUNT)
			{
				removeEventListener(Event.ENTER_FRAME, update);
				startFadeOut();
			}
		}
		public override function death():void
		{
			removeEventListener(Event.ENTER_FRAME, update);
			
			if(_nickname)
			{
				_nickname.dispose();
				removeChild(_nickname);
				_nickname = null;
			}
			
			_main.removeWelcomeBackScreen();
		}
	}
}