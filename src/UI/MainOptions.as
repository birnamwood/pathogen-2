package UI
{
	import Sound.SoundController;
	
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import feathers.controls.Radio;
	import feathers.controls.Slider;
	import feathers.core.ToggleGroup;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class MainOptions extends BW_UI
	{
		//Classes
		private var _main:Main;
		
		//Textfields
		private var _MusicVolumeTitle:TextField;
		private var _MusicVolumeTextfield:TextField;
		private var _soundEffectsTitle:TextField;
		private var _soundEffectsTextfield:TextField;
		private var _animationSpeedTitle:TextField;
		
		//Sliders
		private var _musicSlider:Slider;
		private var _soundEffectsSlider:Slider;

		// buttons
		private var _animationSpeedButton:BW_Button;

		//Quit
		private var _backButton:BW_Button;
		private var _creditsButton:BW_Button;
		
		//checkboxes
		private var _animationCheckbox:BW_Button;
		
		public function MainOptions(main:Main)
		{
			super();
			
			_main = main;
			
			//Create a transparent background.
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			addChild(transBackground);
			transBackground.alpha = .75;
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			//Creating background so that content stands out.
			var background:Scale9Image = new Scale9Image(textures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 5;
			background.height = PathogenTest.hTenPercent * 8.0;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			background.color = 0x222245;
			
			//Music Volume
			_MusicVolumeTitle = new TextField(background.width, PathogenTest.hTenPercent/2, "Music Volume", "Questrial", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			_MusicVolumeTitle.pivotX = _MusicVolumeTitle.width/2;
			addChild(_MusicVolumeTitle);
			_MusicVolumeTitle.y = background.y - (background.height * .4);
			
			_MusicVolumeTextfield = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent/2, SoundController.getMusicVolume().toString() + "%", "Dekar", PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
			_MusicVolumeTextfield.pivotX = _MusicVolumeTextfield.width/2;
			addChild(_MusicVolumeTextfield);
			_MusicVolumeTextfield.y = _MusicVolumeTitle.y + _MusicVolumeTitle.height;
			
			//Create Slider
			_musicSlider = new Slider();
			_musicSlider.minimum = 0;
			_musicSlider.maximum = 100;
			_musicSlider.value = SoundController.getMusicVolume();
			_musicSlider.step = 10
			_musicSlider.direction = Slider.DIRECTION_HORIZONTAL;
			_musicSlider.liveDragging = true;
			_musicSlider.thumbProperties.defaultSkin = new Image(Main.assets.getTexture("SliderKnob"));
			_musicSlider.thumbProperties.width = PathogenTest.HD_Multiplyer * 30;
			_musicSlider.minimumTrackProperties.defaultSkin = new Quad(PathogenTest.wTenPercent * 3,PathogenTest.hTenPercent/3, 0x222245);
			_musicSlider.pivotX = _musicSlider.minimumTrackProperties.defaultSkin.width/2;
			_musicSlider.pivotY = _musicSlider.minimumTrackProperties.defaultSkin.height/2;
			addChild(_musicSlider);
			_musicSlider.y = _MusicVolumeTextfield.y + (PathogenTest.hTenPercent * .8)
			_musicSlider.addEventListener(Event.CHANGE, changeMusicSlider);
			
			//Sound Effect Volume
			_soundEffectsTitle = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent/2, "Sound Effects Volume", "Questrial", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			_soundEffectsTitle.pivotX = _soundEffectsTitle.width/2;
			addChild(_soundEffectsTitle);
			_soundEffectsTitle.y = _musicSlider.y + (PathogenTest.hTenPercent * .8);
			
			_soundEffectsTextfield = new TextField(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent/2,  SoundController.getSoundVolume().toString() + "%", "Dekar", PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
			_soundEffectsTextfield.pivotX = _soundEffectsTextfield.width/2;
			addChild(_soundEffectsTextfield);
			_soundEffectsTextfield.y = _soundEffectsTitle.y + _soundEffectsTitle.height;
			
			//Create Slider
			_soundEffectsSlider = new Slider();
			_soundEffectsSlider.minimum = 0;
			_soundEffectsSlider.maximum = 100;
			_soundEffectsSlider.value = SoundController.getSoundVolume();
			_soundEffectsSlider.step = 5;
			_soundEffectsSlider.direction = Slider.DIRECTION_HORIZONTAL;
			_soundEffectsSlider.liveDragging = true;
			_soundEffectsSlider.thumbProperties.defaultSkin = new Image(Main.assets.getTexture("SliderKnob"));
			_soundEffectsSlider.thumbProperties.width =  PathogenTest.HD_Multiplyer * 30;
			_soundEffectsSlider.minimumTrackProperties.defaultSkin = new Quad(PathogenTest.wTenPercent * 3,PathogenTest.hTenPercent/3, 0x222245);
			_soundEffectsSlider.pivotX = _soundEffectsSlider.minimumTrackProperties.defaultSkin.width/2;
			_soundEffectsSlider.pivotY = _soundEffectsSlider.minimumTrackProperties.defaultSkin.height/2;
			addChild(_soundEffectsSlider);
			_soundEffectsSlider.y = _soundEffectsTextfield.y + (PathogenTest.hTenPercent * .8)
			_soundEffectsSlider.addEventListener(Event.CHANGE, changeSoundEffectsSlider);
			
			// animation speed text
			_animationSpeedTitle = new TextField(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent/2, "Animations", "Questrial", PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor, Color.WHITE);
			_animationSpeedTitle.pivotX = _animationSpeedTitle.width/2;
			addChild(_animationSpeedTitle);
			_animationSpeedTitle.y = _soundEffectsSlider.y + (PathogenTest.hTenPercent * .8);
			
			var animationOnText:TextField = new TextField(PathogenTest.wTenPercent * 0.5, PathogenTest.hTenPercent * 0.5, "On", "Dekar", PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor, Color.WHITE);
			animationOnText.pivotX = animationOnText.width/2;
			animationOnText.pivotY = animationOnText.height/2;
			addChild(animationOnText);
			animationOnText.x -= PathogenTest.wTenPercent * 1.75;
			animationOnText.y = _animationSpeedTitle.y + (PathogenTest.hTenPercent * 1.0);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_animationCheckbox = new BW_Button(.75,1, "", 0);
			else
				_animationCheckbox = new BW_Button(.5,.7, "", 0);
			
			_animationCheckbox.addImage(Main.assets.getTexture("CheckBoxUp"), Main.assets.getTexture("CheckBoxDown"));
			_animationCheckbox.toggle = true;
			addChild(_animationCheckbox);
			_animationCheckbox.x = animationOnText.x + PathogenTest.wTenPercent * 0.75;
			_animationCheckbox.y = animationOnText.y;
			_animationCheckbox.addEventListener(TouchEvent.TOUCH, onCheckAnimations);
			if (Main.sAnimationsOn)
				_animationCheckbox.toggleDown();
			
			var animationSpeedText:TextField = new TextField(PathogenTest.wTenPercent * 1.5, PathogenTest.hTenPercent * 0.5, "Speed", "Dekar", PathogenTest.HD_Multiplyer * 38 / PathogenTest.scaleFactor, Color.WHITE);
			animationSpeedText.pivotX = animationSpeedText.width/2;
			animationSpeedText.pivotY = animationSpeedText.height/2;
			addChild(animationSpeedText);
			animationSpeedText.x = _animationCheckbox.x + PathogenTest.wTenPercent * 1.25;
			animationSpeedText.y = animationOnText.y;
			
			var animationSpeedString:String;
			if (Main.animationFramerate == Main.ANIMATION_SPEED_NORMAL)
				animationSpeedString = "Normal";
			else
				animationSpeedString = "Fast";
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_animationSpeedButton = new BW_Button(1.0, 1, animationSpeedString, 24);
			else
				_animationSpeedButton = new BW_Button(1.0, 0.75, animationSpeedString, 24);
				
			addChild(_animationSpeedButton);
			_animationSpeedButton.x = animationSpeedText.x + (PathogenTest.wTenPercent * 1.25);
			_animationSpeedButton.y = _animationCheckbox.y;
			
			if (!Main.sAnimationsOn)
				_animationSpeedButton.alpha = 0.5;
			else
				_animationSpeedButton.addEventListener(TouchEvent.TOUCH, onChangeAnimationSpeed);
			
			//Create Back button.
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_backButton = new BW_Button(.75,1, "", 0);
			else
				_backButton = new BW_Button(.5,.75, "", 0);
			
			_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(_backButton);
			_backButton.x = - background.width/2 + (_backButton.width * .7);
			_backButton.y = background.height/2 - (_backButton.height * .7);
			_backButton.addEventListener(TouchEvent.TOUCH, quitOptions);
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_creditsButton = new BW_Button(1, 1, "Credits", 24);
			else
				_creditsButton = new BW_Button(1,.75, "Credits", 24);
			
			
			addChild(_creditsButton);
			_creditsButton.y = _backButton.y
			_creditsButton.addEventListener(TouchEvent.TOUCH, onTouchCredits);
			
		}
		private function changeMusicSlider($e:Event):void
		{
			//Updating music volumne information
			_MusicVolumeTextfield.text = _musicSlider.value.toString() + "%";
			SoundController.setMusicVolume(_musicSlider.value);
		}
		private function changeSoundEffectsSlider($e:Event):void
		{
			//Updating sound Effects Volume information.
			_soundEffectsTextfield.text = _soundEffectsSlider.value.toString()  + "%";
			SoundController.setSoundVolume(_soundEffectsSlider.value);
		}
		private function onCheckAnimations(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				if (Main.sAnimationsOn)
				{
					Main.sAnimationsOn = false;
					Main.saveDataObject.data.AnimationsOn = false;
					_animationSpeedButton.removeEventListener(TouchEvent.TOUCH, onChangeAnimationSpeed);
					_animationSpeedButton.alpha = 0.5;
				}
				else
				{
					Main.sAnimationsOn = true;
					Main.saveDataObject.data.AnimationsOn = true;
					_animationSpeedButton.addEventListener(TouchEvent.TOUCH, onChangeAnimationSpeed);
					_animationSpeedButton.alpha = 1.0;
				}
			}
		}
		private function onChangeAnimationSpeed(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				if (Main.animationFramerate == Main.ANIMATION_SPEED_NORMAL)
				{
					Main.animationFramerate = Main.ANIMATION_SPEED_FAST;
					Main.saveDataObject.data.AnimationSpeed = Main.ANIMATION_SPEED_FAST;
					_animationSpeedButton.changeText("Fast");
				}
				else
				{
					Main.animationFramerate = Main.ANIMATION_SPEED_NORMAL;
					Main.saveDataObject.data.AnimationSpeed = Main.ANIMATION_SPEED_NORMAL;
					_animationSpeedButton.changeText("Normal");
				}
			}
		}
		private function quitOptions($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				//leave options & return to Main Menu.
				removelisteners();
				startFadeOut();
			}
		}
		private function onTouchCredits($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				removelisteners();
				startFadeOut();
				_main.openCredits();
			}
		}
		private function removelisteners():void
		{
			_backButton.removeEventListener(TouchEvent.TOUCH, quitOptions);
			_creditsButton.removeEventListener(TouchEvent.TOUCH, onTouchCredits);
		}
		override public function death():void
		{
			_backButton.removeEventListener(TouchEvent.TOUCH, quitOptions);
			_backButton.destroy();
			_backButton.dispose();
			removeChild(_backButton);
			_backButton = null;
			
			_creditsButton.removeEventListener(TouchEvent.TOUCH, onTouchCredits);
			_creditsButton.destroy();
			_creditsButton.dispose();
			removeChild(_creditsButton);
			_creditsButton = null;
			
			_animationSpeedButton.removeEventListener(TouchEvent.TOUCH, onChangeAnimationSpeed);
			_animationSpeedButton.destroy();
			_animationSpeedButton.dispose();
			removeChild(_animationSpeedButton);
			_animationSpeedButton = null;
			
			_animationCheckbox.removeEventListener(TouchEvent.TOUCH, onCheckAnimations);
			_animationCheckbox.destroy();
			_animationCheckbox.dispose();
			removeChild(_animationCheckbox);
			_animationCheckbox = null;
			
			_soundEffectsSlider.removeEventListener(Event.CHANGE, changeSoundEffectsSlider);
			_soundEffectsSlider.dispose();
			removeChild(_soundEffectsSlider);
			_soundEffectsSlider = null;
			
			_musicSlider.removeEventListener(Event.CHANGE, changeMusicSlider);
			_musicSlider.dispose();
			removeChild(_musicSlider);
			_musicSlider = null;
			
			_main.removeMainOptions();
		}
	}
}