package UI
{
	import Utils.BW_Button;
	import Utils.BW_Window;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class AchievementScreen extends BW_Window
	{
		//Classes
		private var _main:Main;
		
		//Buttons
		private var _quitButton:BW_Button;
		
		private var _container:ScrollContainer;
		
		public function AchievementScreen()
		{
			super();
		}
		public function init(main:Main):void
		{
			_main = main;
			
			createbackground(8,8);
			createHeader("Achievements");
			createButtons();
			createAchievements();
		}
		private function createButtons():void
		{
			// create Quit button
			if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_quitButton = new BW_Button(.75,1, "", 0);
			else
				_quitButton = new BW_Button(.5,.75, "", 0);
			
			_quitButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(_quitButton);
			_quitButton.x =  _background.x - _background.width/2 + (_quitButton.width * .75);
			_quitButton.y = _background.y + _background.height/2 - (_quitButton.width * .75);
			_quitButton.addEventListener(TouchEvent.TOUCH, onTouchQuit);
		}
		private function createAchievements():void
		{
			var layout:VerticalLayout = new VerticalLayout();
			_container = new ScrollContainer();
			_container.layout = layout;
			_container.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_container.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			_container.scrollerProperties.snapScrollPositionsToPixels = true;
			addChild(_container);
			_container.height = _background.height - PathogenTest.hTenPercent;
			_container.width = _background.width;
			
			layout.gap = PathogenTest.hTenPercent * .25;
			layout.paddingTop = PathogenTest.hTenPercent;
			
			for (var i:int = 0; i < 36; i++)
			{
				var achievementPopUp:AchievementPopUp = new AchievementPopUp();
				_container.addChild(achievementPopUp);
				achievementPopUp.init(i);
			}
			
			_container.pivotX = _container.width/2;
			_container.x = PathogenTest.stageCenter.x + PathogenTest.wTenPercent;
			_container.y = PathogenTest.hTenPercent * 2;	
		}
		private function onTouchQuit(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this,TouchPhase.ENDED);
			if (touch)
			{
				_quitButton.removeEventListener(TouchEvent.TOUCH, onTouchQuit);
				startFadeOut();
			}
		}
		public override function death():void
		{
			if(_container)
			{
				_container.dispose();
				removeChild(_container);
				_container = null;
			}
			
			if(_quitButton)
			{
				_quitButton.destroy();
				_quitButton.dispose();
				removeChild(_quitButton);
				_quitButton = null;
			}
			
			_main.leaveAchievementScreen();
			_main = null;
		}
	}
}