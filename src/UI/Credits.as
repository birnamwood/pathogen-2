package UI
{
	import Campaign.Campaign;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import com.sticksports.nativeExtensions.flurry.Flurry;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.display.Scale9Image;
	import feathers.layout.VerticalLayout;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	import flash.media.CameraUI;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class Credits extends BW_UI
	{
		//Parent Class
		private var _main:Main;
		private var _campaign:Campaign;
		
		//Buttons
		private var _backButton:BW_Button;
		
		private var _textHolder:Sprite;
		private var _background:Scale9Image
		private var _container:ScrollContainer;
		
		public function Credits(main:Main, campaign:Campaign = null)
		{
			super();
			
			_main = main;
			_campaign = campaign;
			
			//Create a transparent background.
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			addChild(transBackground);
			transBackground.alpha = .9;
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			const texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num))
			
			//Creating background so that content stands out.
			_background = new Scale9Image(textures);
			addChild(_background);
			_background.width = PathogenTest.wTenPercent * 5;
			_background.height = PathogenTest.hTenPercent * 7;
			_background.pivotX = _background.width/2;
			_background.pivotY = _background.height/2;
			_background.alpha = .5;
			_background.color = 0x222245;
			
			//Create Back button.
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
				_backButton = new BW_Button(.75,1.0, "", 0);
			else
				_backButton = new BW_Button(.5,.75, "", 0);
			
			
			_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
			addChild(_backButton);
			_backButton.x = - _background.width/2 + (_backButton.width * .7);
			_backButton.y = _background.height/2 - (_backButton.height * .7);
			_backButton.addEventListener(TouchEvent.TOUCH, quitOptions);
			
			addCreditsText();
			
			if(Flurry.isSupported)
				Flurry.logEvent("Credits");
		}
		private function addCreditsText():void
		{
			var layout:VerticalLayout = new VerticalLayout();
			_container = new ScrollContainer();
			_container.layout = layout;
			_container.scrollerProperties.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_container.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			_container.scrollerProperties.snapScrollPositionsToPixels = true;
			addChild(_container);
			_container.height = PathogenTest.hTenPercent * 6;
			_container.width = _background.width;
			_container.x = -_background.width/2;
			_container.y = -_background.height/2;
			
			layout.paddingTop = PathogenTest.hTenPercent;
			layout.paddingLeft = _background.width/2;
			
			_textHolder = new Sprite();
			_container.addChild(_textHolder);
			
			var titleFontSize:Number = PathogenTest.HD_Multiplyer * 36 / PathogenTest.scaleFactor;
			var textFontSize:Number = PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor;
			
			var thankYouTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Thank You for Playing!", "Questrial", PathogenTest.HD_Multiplyer * 50 / PathogenTest.scaleFactor, Color.WHITE);
			thankYouTitle.pivotX = thankYouTitle.width/2;
			thankYouTitle.pivotY = thankYouTitle.height/2;
			_textHolder.addChild(thankYouTitle);
			
			var createdByTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Created By", "Questrial", titleFontSize, Color.WHITE);
			createdByTitle.pivotX = createdByTitle.width/2;
			createdByTitle.pivotY = createdByTitle.height/2;
			_textHolder.addChild(createdByTitle);
			createdByTitle.y = thankYouTitle.y + (thankYouTitle.height * 1.2);
			
			var createdByText:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Zach Bohn", "Dekar", textFontSize, Color.WHITE);
			createdByText.pivotX = createdByTitle.width/2;
			_textHolder.addChild(createdByText);
			createdByText.y = createdByTitle.y;
			
			var zachBohnTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Design/Code/Art Direction", "Questrial", titleFontSize, Color.WHITE);
			zachBohnTitle.pivotY = zachBohnTitle.height/2;
			zachBohnTitle.pivotX = zachBohnTitle.width/2;
			_textHolder.addChild(zachBohnTitle);
			zachBohnTitle.y = createdByText.y + (createdByText.height * 1.2);
			
			var zachBohnText:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Zach Bohn", "Dekar", textFontSize, Color.WHITE);
			zachBohnText.pivotX = zachBohnText.width/2;
			_textHolder.addChild(zachBohnText);
			zachBohnText.y = zachBohnTitle.y;
			
			var mattBrandTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Code", "Questrial", titleFontSize, Color.WHITE);
			mattBrandTitle.pivotY = mattBrandTitle.height/2;
			mattBrandTitle.pivotX = mattBrandTitle.width/2;
			_textHolder.addChild(mattBrandTitle);
			mattBrandTitle.y = zachBohnText.y + (zachBohnText.height * 1.2);
			
			var mattBrandText:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Matt Brand", "Dekar", textFontSize, Color.WHITE);
			mattBrandText.pivotX = mattBrandText.width/2;
			_textHolder.addChild(mattBrandText);
			mattBrandText.y = mattBrandTitle.y;
			
			var margueriteDibbleTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Art Direction/Business", "Questrial", titleFontSize, Color.WHITE);
			margueriteDibbleTitle.pivotY = margueriteDibbleTitle.height/2;
			margueriteDibbleTitle.pivotX = margueriteDibbleTitle.width/2;
			_textHolder.addChild(margueriteDibbleTitle);
			margueriteDibbleTitle.y = mattBrandText.y + (mattBrandText.height * 1.2);
			
			var margueriteDibbleText:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Marguerite Dibble", "Dekar", textFontSize, Color.WHITE);
			margueriteDibbleText.pivotX = margueriteDibbleText.width/2;
			_textHolder.addChild(margueriteDibbleText);
			margueriteDibbleText.y = margueriteDibbleTitle.y;
			
			var mikeHopkeTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Social Media", "Questrial", titleFontSize, Color.WHITE);
			mikeHopkeTitle.pivotY = mikeHopkeTitle.height/2;
			mikeHopkeTitle.pivotX = mikeHopkeTitle.width/2;
			_textHolder.addChild(mikeHopkeTitle);
			mikeHopkeTitle.y = margueriteDibbleText.y + (margueriteDibbleText.height * 1.2);
			
			var mikeHopkeText:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Mike Hopke", "Dekar", textFontSize, Color.WHITE);
			mikeHopkeText.pivotX = mikeHopkeText.width/2;
			_textHolder.addChild(mikeHopkeText);
			mikeHopkeText.y = mikeHopkeTitle.y;
			
			var sibylCunninghamTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Art", "Questrial", titleFontSize, Color.WHITE);
			sibylCunninghamTitle.pivotY = sibylCunninghamTitle.height/2;
			sibylCunninghamTitle.pivotX = sibylCunninghamTitle.width/2;
			_textHolder.addChild(sibylCunninghamTitle);
			sibylCunninghamTitle.y = mikeHopkeText.y + (mikeHopkeText.height * 1.2);
			
			var sibylCunninghamText:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Sibyl Cunningham", "Dekar", textFontSize, Color.WHITE);
			sibylCunninghamText.pivotX = sibylCunninghamText.width/2;
			_textHolder.addChild(sibylCunninghamText);
			sibylCunninghamText.y = sibylCunninghamTitle.y;
			
			var soundTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Sound", "Questrial", titleFontSize, Color.WHITE);
			soundTitle.pivotY = soundTitle.height/2;
			soundTitle.pivotX = soundTitle.width/2;
			_textHolder.addChild(soundTitle);
			soundTitle.y = sibylCunninghamText.y + (sibylCunninghamText.height * 1.2);
			
			var soundText:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Gavin Harrison" + "\n" + "Eric Sample", "Dekar", textFontSize, Color.WHITE);
			soundText.pivotX = soundText.width/2;
			_textHolder.addChild(soundText);
			soundText.y = soundTitle.y;
				
			var publishedByTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Published By", "Questrial", titleFontSize, Color.WHITE);
			publishedByTitle.pivotY = publishedByTitle.height/2;
			publishedByTitle.pivotX = publishedByTitle.width/2;
			_textHolder.addChild(publishedByTitle);
			publishedByTitle.y = soundText.y + (soundText.height * 1.2);
			
			var publishedByText:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Gameblyr", "Dekar", textFontSize, Color.WHITE);
			publishedByText.pivotX = publishedByText.width/2;
			_textHolder.addChild(publishedByText);
			publishedByText.y = publishedByTitle.y;
			
			var specialThanksTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Special Thanks", "Questrial", titleFontSize, Color.WHITE);
			specialThanksTitle.pivotY = specialThanksTitle.height/2;
			specialThanksTitle.pivotX = specialThanksTitle.width/2;
			_textHolder.addChild(specialThanksTitle);
			specialThanksTitle.y = publishedByText.y + (publishedByText.height * 1.2);
			
			var specialThanksText:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Greg Bemis, Jonathan Ferguson and the EGD professors at Champlain College", "Dekar", textFontSize, Color.WHITE);
			specialThanksText.pivotX = specialThanksText.width/2;
			_textHolder.addChild(specialThanksText);
			specialThanksText.y = specialThanksTitle.y;
			
			var specialThanksText_02:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Dillion Handy, Hunter Grey and the Testers of Champlain", "Dekar", textFontSize, Color.WHITE);
			specialThanksText_02.pivotX = specialThanksText_02.width/2;
			_textHolder.addChild(specialThanksText_02);
			specialThanksText_02.y = specialThanksText.y + (specialThanksText.height * 1.2)
				
			var specialThanksText_03:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Mom & Dad for always being there, in the ups and the downs - Zach", "Dekar", textFontSize, Color.WHITE);
			specialThanksText_03.pivotX = specialThanksText_02.width/2;
			_textHolder.addChild(specialThanksText_03);
			specialThanksText_03.y = specialThanksText_02.y + (specialThanksText_02.height * 1.2)
				
			var createdWithTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Created With", "Questrial", titleFontSize, Color.WHITE);
			createdWithTitle.pivotY = createdWithTitle.height/2;
			createdWithTitle.pivotX = createdWithTitle.width/2;
			_textHolder.addChild(createdWithTitle);
			createdWithTitle.y = specialThanksText_03.y + (specialThanksText_03.height * 1.5);
			
			var createdWithText:TextField = new TextField(_background.width, PathogenTest.hTenPercent * 2, "Flash CS6" + "\n" + "Flash Builder 4.6" + "\n" + "Starling" + "\n" + "Photoshop" + "\n" + "Flox", "Dekar", textFontSize, Color.WHITE);
			createdWithText.pivotX = createdWithText.width/2;
			_textHolder.addChild(createdWithText);
			createdWithText.y = createdWithTitle.y;
			
			var DevelopmentTitle:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "Development Time", "Questrial", titleFontSize, Color.WHITE);
			DevelopmentTitle.pivotY = DevelopmentTitle.height/2;
			DevelopmentTitle.pivotX = DevelopmentTitle.width/2;
			_textHolder.addChild(DevelopmentTitle);
			DevelopmentTitle.y = createdWithText.y + (createdWithText.height * 1.2);
			
			var developmentText:TextField = new TextField(_background.width, PathogenTest.hTenPercent, "12 Months", "Dekar", textFontSize, Color.WHITE);
			developmentText.pivotX = developmentText.width/2;
			_textHolder.addChild(developmentText);
			developmentText.y = DevelopmentTitle.y;
			
			var BWlogo:Image = new Image(Main.assets.getTexture("Splash_BWG"));
			BWlogo.pivotY = BWlogo.height/2;
			BWlogo.pivotX = BWlogo.width/2;
			_textHolder.addChild(BWlogo);
			BWlogo.scaleX = .5;
			BWlogo.scaleY = .5;
			BWlogo.x = -PathogenTest.wTenPercent;
			BWlogo.y = developmentText.y + (developmentText.height * 1.6);
			
			var gamebylrLogo:Image = new Image(Main.assets.getTexture("Splash_Gambylr"));
			gamebylrLogo.pivotY = gamebylrLogo.height/2;
			gamebylrLogo.pivotX = gamebylrLogo.width/2;
			gamebylrLogo.scaleX = .5;
			gamebylrLogo.scaleY = .5;
			_textHolder.addChild(gamebylrLogo);
			gamebylrLogo.x = PathogenTest.wTenPercent;
			gamebylrLogo.y = BWlogo.y;
			
		}
		private function quitOptions($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch && alpha == 1)
			{
				_backButton.removeEventListener(TouchEvent.TOUCH, quitOptions);
				//leave options & return to Main Menu.
				startFadeOut();
			}
		}
		public override function death():void
		{
			_backButton.destroy();
			_backButton.dispose();
			removeChild(_backButton);
			_backButton = null;
			
			_container.dispose();
			removeChild(_container);
			_container = null;
			
			if(_main)
			{
				_main.removeCredits();
				_main = null;
			}
			
			if(_campaign)
			{
				_campaign.removeCredits();
				_campaign = null;
			}
		}
	}
}