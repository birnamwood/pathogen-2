package Campaign
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import feathers.controls.Header;
	
	import flash.display3D.IndexBuffer3D;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	
	public class DifficultySelector extends BW_UI
	{
		private const FADE_SPEED:Number = 0.02;
		public static const EASY:int = 1;
		public static const MEDIUM:int = 2;
		public static const HARD:int = 3;
		
		public static const VISIBLE:int = 0;
		public static const INVISIBLE:int = 1;
		
		private var textField:TextField
		private var difficultyBaseString:String;
		
		//Display
		private var easyBt:BW_Button;
		private var mediumBt:BW_Button;
		private var hardBt:BW_Button;
		
		private var _status:int;
		
		public var difficulty:int = 1;
		
		private var _tween:Tween;
		
		public function DifficultySelector(localPlay:Boolean = false)
		{
			super(false);
			
			easyBt = new BW_Button(.5,.8, "", 0);
			easyBt.addImage(Main.assets.getTexture("Green_Stage01"), Main.assets.getTexture("Green_Stage01"));
			addChild(easyBt);
			easyBt.toggle = true;
			//easyBt.backgroundDOWN_COLOR = 0xffffff;
			easyBt.x = -easyBt.width * 1.5;
			easyBt.addEventListener(TouchEvent.TOUCH, selectEasy);
			
			mediumBt = new BW_Button(.5,.8, "", 0);
			mediumBt.addImage(Main.assets.getTexture("Yellow_Stage01"), Main.assets.getTexture("Yellow_Stage01"));
			addChild(mediumBt);
			mediumBt.toggle = true;
			//easyBt.backgroundDOWN_COLOR = 0xffffff;
			mediumBt.addEventListener(TouchEvent.TOUCH, selectMedium);
			
			hardBt = new BW_Button(.5,.8, "", 0);
			hardBt.addImage(Main.assets.getTexture("Red_Stage01"), Main.assets.getTexture("Red_Stage01"));
			addChild(hardBt);
			hardBt.toggle = true;
			//easyBt.backgroundDOWN_COLOR = 0xffffff;
			hardBt.x = easyBt.width * 1.5;
			hardBt.addEventListener(TouchEvent.TOUCH, selectHard);
			
			//if ((PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5) && localPlay)
			if (Main.isPhone() && localPlay)
			{
				easyBt.x = 0;
				mediumBt.y = easyBt.y + (easyBt.height * 1.5);
				hardBt.x = 0;
				hardBt.y = mediumBt.y + (mediumBt.height * 1.5);
			}
			
/*			easyBt = new Image(Main.assets.getTexture("Green_Stage01"));
			easyBt.pivotX = easyBt.width/2;
			easyBt.pivotY = easyBt.height/2;
			easyBt.x = -easyBt.width * 1.2;
			easyBt.addEventListener(TouchEvent.TOUCH, selectEasy);
			
			mediumBt = new Image(Main.assets.getTexture("Yellow_Stage01"));
			mediumBt.pivotX = mediumBt.width/2;
			mediumBt.pivotY = mediumBt.height/2;
			mediumBt.addEventListener(TouchEvent.TOUCH, selectMedium);
			
			hardBt = new Image(Main.assets.getTexture("Red_Stage01"));
			hardBt.pivotX = hardBt.width/2;
			hardBt.pivotY = hardBt.height/2;
			hardBt.x = hardBt.width * 1.2;
			hardBt.addEventListener(TouchEvent.TOUCH, selectHard);*/
			
			if (Main.isPhone())
			//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
			{
				easyBt.scaleX = 1.2;
				easyBt.scaleY = 1.2;
				mediumBt.scaleX = 1.2;
				mediumBt.scaleY = 1.2;
				hardBt.scaleX = 1.2;
				hardBt.scaleY = 1.2;
			}
			
			difficultyBaseString = "Campaign Difficulty: ";
			if (localPlay)
				difficultyBaseString = "AI Difficulty: ";
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 20 / PathogenTest.scaleFactor;
			var textX:int = PathogenTest.HD_Multiplyer * 300 / PathogenTest.scaleFactor;
			var textY:int = PathogenTest.HD_Multiplyer * 70 / PathogenTest.scaleFactor;
			textField = new TextField(textX, textY, difficultyBaseString, "Dekar", fontSize, Color.WHITE);
			textField.pivotX = textField.width/2;
			textField.pivotY = textField.height/2;
			addChild(textField);
			textField.y = -easyBt.height * .7;
			
			addChild(easyBt);
			addChild(mediumBt);
			addChild(hardBt);
			
			visible = false;
			_status = INVISIBLE;
			
			reset();
		}
		public function selectEasy($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				difficulty = EASY;
				reset();
			}
		}
		public function selectMedium($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				difficulty = MEDIUM;
				reset();
			}
		}
		public function selectHard($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				difficulty = HARD;
				reset();
			}
		}
		public function reset():void
		{
			easyBt.alpha = .2;
			mediumBt.alpha = .2;
			hardBt.alpha = .2;
			
			easyBt.toggleDown();
			mediumBt.toggleDown();
			hardBt.toggleDown();
			
			var difficultyText:String = difficultyBaseString;
			
			switch(difficulty)
			{
				case 1:
					easyBt.alpha = 1;
					difficultyText += "Easy";
					easyBt.toggleUp();
					break;
				case 2:
					mediumBt.alpha = 1;
					difficultyText += "Medium";
					mediumBt.toggleUp();
					break;
				case 3:
					hardBt.alpha = 1;
					difficultyText += "Hard";
					hardBt.toggleUp();
					break;
					
				default:
					break;
			}
			
			textField.text = difficultyText;
		}
		public function fadeIN():void
		{
			alpha = 0;
			visible = true;
			
			_tween = new Tween(this, 1);
			_tween.fadeTo(1);
			_tween.onComplete = fadeINComplete;
			Starling.juggler.add(_tween);
		}
		private function fadeINComplete():void
		{
			_status = VISIBLE;
		}
		public function makeInvisible():void
		{
			Starling.juggler.remove(_tween);
			alpha = 0.0;
			visible = false;
			_status = INVISIBLE;
		}
		public function fadeOUT():void
		{	
			_tween = new Tween(this, 1);
			_tween.fadeTo(0);
			_tween.onComplete = fadeOUTComplete;
			Starling.juggler.add(_tween);
		}
		private function fadeOUTComplete():void
		{
			visible = false;
			_status = INVISIBLE;
		}
		public function makeVisible():void
		{
			Starling.juggler.remove(_tween);
			alpha = 1.0;
			visible = true;
			_status = VISIBLE;
		}
		public override function death():void
		{
			easyBt.removeEventListener(TouchEvent.TOUCH, selectEasy);
			easyBt.dispose();
			removeChild(easyBt);
			easyBt = null;
			
			mediumBt.removeEventListener(TouchEvent.TOUCH, selectMedium);
			mediumBt.dispose();
			removeChild(mediumBt);
			mediumBt = null;
			
			hardBt.removeEventListener(TouchEvent.TOUCH, selectHard);
			hardBt.dispose();
			removeChild(hardBt);
			hardBt = null;
		}
		
		public function get status():int {return _status}; 
	}
}