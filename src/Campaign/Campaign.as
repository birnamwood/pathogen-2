package Campaign
{
	import Core.GameManager;
	
	import UI.AchievementPopUp;
	import UI.Achievements;
	import UI.Credits;
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_UI;
	
	import com.sticksports.nativeExtensions.flurry.Flurry;
	import com.sticksports.nativeExtensions.gameCenter.GameCenter;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.events.FeathersEventType;
	import feathers.layout.VerticalLayout;
	
	import flash.net.SharedObject;
	
	import mx.core.mx_internal;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.utils.Color;
	
	public class Campaign extends BW_UI
	{
		//Static Vars
		private const SCROLL_MOVE_AMOUNT:Number = 5.0;
		private const SCROLL_MOVE_FAST_AMOUNT:Number = 15.0;
		private const SCROLL_CLOSE_ENOUGH:Number = 3.0;
		private const SCROLL_SLOW_DOWN_CLOSE_AMOUNT:Number = 600.0;
		private const SCROLL_LEVEL_4_THRESHOLD:Number = 1950.0;
		
		//Classes
		private var _main:Main;
		private var gameManager:GameManager;
		private var _credits:Credits;
		
		//Campaign Vars
		public var currentCampaignLevel:Level;
		private var listOfCoreLevels:Array = [0,1,2,3,4,5,6,7,8,9,10,11];
		private var listOfCompletedLevels:Array = [];
		private var currentLevel:int;
		private var visibleLevelHolder:Sprite;
		
		//UI
		private var backBt:BW_Button;
		private var campaignArray:Array = [];
		private var sliderArray:Array = [];
		public var difficultySelector:DifficultySelector;
		private var UIBand:Quad;
		
		//Scroll
		public var sliding:Boolean = false;
		private var scrollToPos:Number;
		
		//Save
		private var sharedData:SharedObject;
		private var _saveXML:XML;
		
		private var _levelBackBt:BW_Button;
		private var _zoomedInLevel:Level;
		
		//Debug
		private var allLevels:Boolean = false;
		
		private var _levelScreen:LevelScreen;
		
		public function Campaign(_main:Main, gameManager:GameManager, sharedData:SharedObject)
		{
			super();
			
			this._main = _main;
			this.sharedData = sharedData;
			
			//Main.updateDebugText("Device: " + PathogenTest.device);
			//Main.updateDebugText("KINDLE: " + PathogenTest.KINDLE);
			//Main.updateDebugText("KINDLE_TYPE: " + PathogenTest.KINDLE_TYPE);
			
			if(allLevels)
				listOfCompletedLevels = listOfCoreLevels;
			
			setup();
			loadProgress();
			
			if (Main.isPhone())
				UIBand = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1.8, Color.BLACK);
			else
				UIBand = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 1.5, Color.BLACK);
			
			UIBand.pivotX = UIBand.width/2;
			UIBand.pivotY = UIBand.height;
			addChild(UIBand);
			UIBand.x = PathogenTest.stageCenter.x;
			UIBand.y = PathogenTest.stageHeight;
			UIBand.alpha = .5;
			
			//Adding back button
			if (Main.isPhone())
				backBt = new BW_Button(.75,1.0, "", 0);
			else
				backBt = new BW_Button(.5,.75, "", 0);
				
			backBt.addImage(Main.assets.getTexture("Pathogen_BTN_Back"), Main.assets.getTexture("Pathogen_BTN_Back_Down"));
			addChild(backBt);
			backBt.x = PathogenTest.wTenPercent * 0.55;
			backBt.y = UIBand.y - UIBand.height/2;
			backBt.addEventListener(TouchEvent.TOUCH, quitCampaign);
			
			//Adding Difficulty Selecter
			difficultySelector = new DifficultySelector();
			addChild(difficultySelector);
			difficultySelector.x = PathogenTest.wTenPercent * 8.25;
			difficultySelector.y = backBt.y;
			difficultySelector.fadeIN();
			
			if(Flurry.isSupported)
				Flurry.startTimedEvent("Campaign");
		}
		private function setup():void
		{
			//All visible sprites will be added to this, restricts scrolling to levels that have been unlocked.
			
			visibleLevelHolder = new Sprite();
			addChild(visibleLevelHolder);
			
			//Create all levels
			for (var i:int = 0; i < listOfCoreLevels.length; i++)
			{
				var level:Level = new Level(this,listOfCoreLevels[i]);
				addChild(level);
				level.scaleX = .75;
				level.scaleY = .75;
				
				campaignArray.push(level);
				
				//Each level is laid out a little differently.
				switch(i)
				{
					case 0:
					{
						level.x = PathogenTest.wTenPercent * 1;
						level.y = PathogenTest.hTenPercent * 3;
						break;
					}
					case 1:
					{
						level.x = PathogenTest.wTenPercent * 1;
						level.y = PathogenTest.hTenPercent * 6;
						break;
					}
					case 3:
					{
						level.x = PathogenTest.wTenPercent * 2.5;
						level.y = PathogenTest.hTenPercent * 3;
						break;
					}
					case 2:
					{
						level.x = PathogenTest.wTenPercent * 2.5;
						level.y = PathogenTest.hTenPercent * 6;
						break;
					}
					case 4:
					{
						level.x = PathogenTest.wTenPercent * 3.5;
						level.y = PathogenTest.hTenPercent * 4.5;
						break;
					}
					case 5:
					{
						level.x = PathogenTest.wTenPercent * 5.2;
						level.y = PathogenTest.hTenPercent * 2;
						break;
					}
					case 6:
					{
						level.x = PathogenTest.wTenPercent * 5.2;
						level.y = PathogenTest.hTenPercent * 7;
						break;
					}
					case 7:
					{
						level.x = PathogenTest.wTenPercent * 7.2;
						level.y = PathogenTest.hTenPercent * 2;
						break;
					}
					case 8:
					{
						level.x = PathogenTest.wTenPercent * 7.2;
						level.y = PathogenTest.hTenPercent * 7;
						break;
					}
					case 9:
					{
						level.x = PathogenTest.wTenPercent * 9.2;
						level.y = PathogenTest.hTenPercent * 2;
						break;
					}
					case 10:
					{
						level.x = PathogenTest.wTenPercent * 9.2;
						level.y = PathogenTest.hTenPercent * 7;
						break;
					}
					case 11:
					{
						level.x = PathogenTest.wTenPercent * 6;
						level.y = PathogenTest.hTenPercent * 4.5;
						break;
					}
				}
				
				//Hide all but the first level.
				visibleLevelHolder.addChild(level);
				
				if( i > 0)
					level.alpha = .2;
			}
		}
		public function loadProgress():void
		{
			//Load player progress if not in debug
			if (sharedData.data.CampaignSaveData != null)
			{
				_saveXML = sharedData.data.CampaignSaveData;
				var progressString:String = _saveXML.Progress.*
				listOfCompletedLevels = progressString.split(",");
			}
			
			//trace(_saveXML);
			//trace("----------------------------");
			
			var currentNum:int = 0;
			
			//Extract save data and load it.
			for each(var levelNumber:int in listOfCompletedLevels)
			{
				//trace("Completed Level: " + levelNumber);
				var level:Level = campaignArray[listOfCompletedLevels[currentNum]];
				
				if (sharedData.data.CampaignSaveData != null)
				{
					for (var i:int = 0; i < _saveXML.level.length(); i++)
					{
						if(_saveXML.level[i].levelNumber.* == level.levelNumber)
						{
							var difficultyString:String = _saveXML.level[i].arrayOfCompletedDifficulties.*;
							var CompletedDifficulties:Array = difficultyString.split(",");
							
							//Check is see what difficulties were completed
							for each (var difficulty:int in CompletedDifficulties)
							{
								level.checkDifficulty(difficulty);
							}
							
							//trace("looking at Level: " + level.levelNumber);
							
							if(level.numOfSublevels > 0)
							{
								var numOfSubLevelsComplete:int = 0;
								
								//trace("Level " + level.levelNumber + " has " + level.numOfSublevels);
								
								for (var j:int = 0; j < level.numOfSublevels; j++)
								{
									if(_saveXML.level[i].sublevel[j])
									{
										var subLevelDifficultyString:String = _saveXML.level[i].sublevel[j].subLevelArrayOfCompletedDifficulties.*;
										var completedSubLevelDifficulties:Array = subLevelDifficultyString.split(",");
										var sublevel:SubLevel = level.sublevels[j];
										
										//trace("Looking at Level " + level.levelNumber + " at sub level " + sublevel.levelNumber);
										
										if(subLevelDifficultyString.length > 0)
										{
											numOfSubLevelsComplete++;
											
											for each (var subLeveldifficulty:int in completedSubLevelDifficulties)
											{
												sublevel.checkDifficulty(subLeveldifficulty);
												level.checkDifficulty(subLeveldifficulty);
											}
											
											level.furthestSubLevel = sublevel.levelNumber;
											level.chosenSubLevel = sublevel;
											level.unlockNextSubLevel();
											
											//trace("Looking at Level " + level.levelNumber + " at sub level " + sublevel.levelNumber + " has been completed with the following difficulties: " + subLevelDifficultyString);
										}
									}
									
									//trace("Level: " + level.levelNumber + " is now checking difficulty");
									
								}
								
								level.updateProgressText(numOfSubLevelsComplete);
							}
							
							//trace("------------------");
						}
					}
				}
				
				//Set the current level to the latest level completed
				currentCampaignLevel = level;
				incrementLevel();
				
				currentNum++;
			}
		}
		public function checkSubLevelProgression(level:Level):void
		{
			var j:int;
			
			if(_saveXML)
			{
				for (j = 0; j < level.numOfSublevels; j++)
				{
					if(_saveXML.level[level.levelNumber].sublevel[j])
					{
						
						if(_saveXML.level[level.levelNumber].sublevel[j].levelNumber.* == level.sublevels[j].levelNumber)
						{
							var subLevelDifficultyString:String = _saveXML.level[level.levelNumber].sublevel[j].subLevelArrayOfCompletedDifficulties.*;
							var completedSubLevelDifficulties:Array = subLevelDifficultyString.split(",");
							
							if(subLevelDifficultyString.length > 0)
							{
								for each (var subLeveldifficulty:int in completedSubLevelDifficulties)
								{
									level.unlockNextSubLevel();
									level.sublevels[j].checkDifficulty(subLeveldifficulty);
								}
							}
						}
					}
				}
			}
			
			if(allLevels)
			{
				for (j = 0; j < level.numOfSublevels; j++)
				{
					level.sublevels[j].checkDifficulty(difficultySelector.difficulty);
					level.unlockNextSubLevel();
				}
			}
				
		}
		public function addLevelScreen(level:Level):void
		{
			if(_levelScreen == null)
			{
				_levelScreen = new LevelScreen(this, level);
				addChild(_levelScreen);
				_levelScreen.x = PathogenTest.stageCenter.x;
				_levelScreen.y = PathogenTest.stageCenter.y;
			}
		}
		public function removeLevelScreen():void
		{
			if(_levelScreen)
			{
				removeChild(_levelScreen);
				_levelScreen = null;
			}
		}
		public function beginLevel(thisLevel:Level):void
		{
			//Start a campaign level.
			currentCampaignLevel =  thisLevel;
			
			Main.updateDebugText("hide Campaign");
			
			hideCampaign();
			
			Main.updateDebugText("Create a game");
			
			if(gameManager == null)
			{
				gameManager = new GameManager(null, this, thisLevel.playerColors, thisLevel.playerLocations, thisLevel.playerAI, thisLevel.mapData, thisLevel.mapName, thisLevel.mapSize, thisLevel.levelModes);
				addChild(gameManager);
			}
		}
		public function removeGameManager():void
		{
			Main.updateDebugText("Remove a campaign game");
			
			//Player is retrying the same level.
			if (gameManager.retry)
			{
				gameManager.dispose();
				removeChild(gameManager)
				gameManager = null;
				
				beginLevel(currentCampaignLevel);
			}
			else
			{
				Main.updateDebugText("Flurry Supported: " + Flurry.isSupported);
				
				if(Flurry.isSupported)
				{
					if(currentCampaignLevel.numOfSublevels > 0)
						Flurry.logEvent("Campaign Level Complete", { LevelName: currentCampaignLevel.mapName, LevelNumber: currentCampaignLevel.currentSubLevel, Difficulty : difficultySelector.difficulty, Outcome: gameManager.success});
					else
						Flurry.logEvent("Campaign Level Complete", { LevelName: currentCampaignLevel.mapName, LevelNumber : currentCampaignLevel.levelNumber, Difficulty : difficultySelector.difficulty, Outcome: gameManager.success});
				}
				
				//Player successfully completed the level
				if(gameManager.success)
				{
					Main.updateDebugText("GameManager Success: " + gameManager.success);
					
					var level:int;
					var unlocked:Boolean = true;
					
					for each (level in listOfCompletedLevels)
					{
						if (level == currentCampaignLevel.levelNumber)
							unlocked = false;
						
					}
					if (unlocked)
					{
						incrementLevel();
						listOfCompletedLevels.push(currentCampaignLevel.levelNumber);
					}
					
					currentCampaignLevel.checkDifficulty(difficultySelector.difficulty);
					
					//If the level was a sub-level, unlock the next one.
					if(currentCampaignLevel.chosenSubLevel)
					{
						currentCampaignLevel.chosenSubLevel.checkDifficulty(difficultySelector.difficulty);
						currentCampaignLevel.checkDifficulty(difficultySelector.difficulty);
						
						if(currentCampaignLevel.furthestSubLevel == currentCampaignLevel.chosenSubLevel.levelNumber)
							currentCampaignLevel.unlockNextSubLevel()
						
					}
					
					Main.updateDebugText("check Achievements");
					
					checkAchievements();
					
					Main.updateDebugText("Create Campaign Save Data");
					
					createCampaignSaveData();
					
					if(currentCampaignLevel.levelNumber == 11 && unlocked)
						addCredits();
						
				}
				
				Main.updateDebugText("Remove Game Manager");
				
				if(gameManager)
				{
					gameManager.dispose();
					removeChild(gameManager)
					gameManager = null;
				}
				
				Main.updateDebugText("Reveal Campaign");
				
				revealCampaign();
				Main.sSoundController.transitionToMenuMusic();
			}
		}
		private function incrementLevel():void
		{
			//Unlock the next level in the series.
			if(currentCampaignLevel.levelNumber != listOfCoreLevels.length -1)
			{
				var highestLevel:int = -1;
				
				for each (var levelNum:int in currentCampaignLevel.nextLevels)
				{
					for each (var level:Level in campaignArray)
					{
						if(level.levelNumber == levelNum)
						{
							level.alpha = 1;
							
							if (level.levelNumber > highestLevel)
								highestLevel = level.levelNumber;
						}
					}
				}
			}
		}
		private function checkAchievements():void
		{
			//Main.updateDebugText("---------------------");
			//Main.updateDebugText("Checking Achievements for level: " + currentCampaignLevel.levelNumber);
			
			var allSubLevelsComplete:Boolean = true;
			var sublevel:SubLevel;
			
			switch(currentCampaignLevel.levelNumber)
			{
				case 4:
				{
					if(Main.achievements.progress_Complete_Tutorial != Achievements.COMPLETE)
						Main.achievements.updateAchievementProgress(Achievements.COMPLETE_TUTORIAL);
					break;
				}
				
				case 5:
				{
					switch(difficultySelector.difficulty)
					{
						case  DifficultySelector.EASY:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.easy)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_5_1_EASY != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_5_1_EASY);
							break;
						}
						case  DifficultySelector.MEDIUM:
						{
							//Main.updateDebugText("Checking Medium Difficulty for level " + currentCampaignLevel.levelNumber);
							
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								//Main.updateDebugText("Checking Sublevel: " + sublevel.levelNumber + " Medium Difficulty completed: " + sublevel.medium);
								
								if(!sublevel.medium)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_5_1_MEDIUM != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_5_1_MEDIUM);
							break;
						}
						case  DifficultySelector.HARD:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.hard)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_5_1_HARD != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_5_1_HARD);
								
							break;
						}
					}
					break;
				}
				case 6:
				{
					switch(difficultySelector.difficulty)
					{
						case  DifficultySelector.EASY:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.easy)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_5_2_EASY != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_5_2_EASY);
								
							break;
						}
						case  DifficultySelector.MEDIUM:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.medium)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_5_2_MEDIUM != Achievements.COMPLETE)
							{
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_5_2_MEDIUM);
							}
								
							break;
						}
						case  DifficultySelector.HARD:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.hard)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_5_2_HARD != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_5_2_HARD);
								
							break;
						}
					}
					break;
				}
				case 7:
				{
					switch(difficultySelector.difficulty)
					{
						case  DifficultySelector.EASY:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.easy)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_6_1_EASY != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_6_1_EASY);
								
							break;
						}
						case  DifficultySelector.MEDIUM:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.medium)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_6_1_MEDIUM != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_6_1_MEDIUM);
								
							break;
						}
						case  DifficultySelector.HARD:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.hard)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_6_1_HARD != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_6_1_HARD);
								
							break;
						}
					}
					break;
				}
				case 8:
				{
					switch(difficultySelector.difficulty)
					{
						case  DifficultySelector.EASY:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.easy)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_6_2_EASY != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_6_2_EASY);
							
							break;
						}
						case  DifficultySelector.MEDIUM:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.medium)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_6_2_MEDIUM != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_6_2_MEDIUM);
								
							break;
						}
						case  DifficultySelector.HARD:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.hard)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_6_2_HARD != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_6_2_HARD);
								
							break;
						}
					}
					break;
				}
				case 9:
				{
					switch(difficultySelector.difficulty)
					{
						case  DifficultySelector.EASY:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.easy)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_7_1_EASY != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_7_1_EASY);
								
							break;
						}
						case  DifficultySelector.MEDIUM:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.medium)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_7_1_MEDIUM != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_7_1_MEDIUM);
								
							break;
						}
						case  DifficultySelector.HARD:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.hard)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_7_1_HARD != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_7_1_HARD);
								
							break;
						}
					}
					break;
				}
				case 10:
				{
					switch(difficultySelector.difficulty)
					{
						case  DifficultySelector.EASY:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.easy)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_7_2_EASY != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_7_2_EASY);
								
							break;
						}
						case  DifficultySelector.MEDIUM:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.medium)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_7_2_MEDIUM != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_7_2_MEDIUM);
								
							break;
						}
						case  DifficultySelector.HARD:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.hard)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_7_2_HARD != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_7_2_HARD);
							
							break;
						}
					}
					break;
				}
				case 11:
				{
					switch(difficultySelector.difficulty)
					{
						case  DifficultySelector.EASY:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.easy)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_8_EASY != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_8_EASY);
								
							break;
						}
						case  DifficultySelector.MEDIUM:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.medium)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_8_MEDIUM != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_8_MEDIUM);
								
							break;
						}
						case  DifficultySelector.HARD:
						{
							for each (sublevel in currentCampaignLevel.sublevels)
							{
								if(!sublevel.hard)
									allSubLevelsComplete = false;
							}
							
							if(allSubLevelsComplete && Main.achievements.progress_8_HARD != Achievements.COMPLETE)
								Main.achievements.updateAchievementProgress(Achievements.CAMPAIGN_8_HARD);
								
							break;
						}
					}
					break;
				}
			}
		}
		private function hideCampaign():void
		{
			//Hides all campaign UI. Called when entering a campaign level
			backBt.visible = false;
			difficultySelector.visible = false;
			UIBand.visible = false;
			visibleLevelHolder.visible = false;
		}
		private function revealCampaign():void
		{
			//Reveals all campaign UI. Called when exiting a campaign level
			difficultySelector.visible = true;
			UIBand.visible = true;
			visibleLevelHolder.visible = true;
			backBt.visible = true;
		}
		private function quitCampaign($e:TouchEvent):void
		{
			//Leave the campaign
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if (touch && alpha == 1)
			{
				backBt.removeEventListener(TouchEvent.TOUCH, quitCampaign);
				
				for each(var level:Level in campaignArray)
				{
					level.removeEventListeners();
				}
				
				_main.openMainMenu();
				startFadeOut();
			}
		}
		private function createCampaignSaveData():void
		{
			//Erase Old data
			//trace("Erase Old Data");
			//trace("-------------------");
			sharedData.data.CampaignSaveData = null;
			
			//Create Save data XML entry
			_saveXML = new XML( <saveData />);
			var Progress:XML = new XML( <Progress> {listOfCompletedLevels} </Progress>);
			_saveXML.appendChild(Progress);
			
			//Save out the state of each level
			for each (var templevel:Level in campaignArray)
			{
				var level:XML =  new XML( <level />);
				var levelNumber:XML =  new XML( <levelNumber> {templevel.levelNumber} </levelNumber>);
				level.appendChild(levelNumber);
				var arrayOfCompletedDifficulties:XML =  new XML( <arrayOfCompletedDifficulties> {templevel.arrayOfDifficulties} </arrayOfCompletedDifficulties>);
				level.appendChild(arrayOfCompletedDifficulties);
				
				//Save out the state of each sub-level
				for each (var tempSubLevel:SubLevel in templevel.sublevels)
				{
					var sublevel:XML =  new XML( <sublevel />);
					var sublevelNumber:XML =  new XML( <levelNumber> {tempSubLevel.levelNumber} </levelNumber>);
					sublevel.appendChild(sublevelNumber);
					var subLevelArrayOfCompletedDifficulties:XML =  new XML( <subLevelArrayOfCompletedDifficulties> {tempSubLevel.arrayOfDifficulties} </subLevelArrayOfCompletedDifficulties>);
					sublevel.appendChild(subLevelArrayOfCompletedDifficulties);
					level.appendChild(sublevel);
				}
				
				_saveXML.appendChild(level);
			}
			
			//trace(_saveXML);
			
			sharedData.data.CampaignSaveData = _saveXML;
			sharedData.flush();
		}
		private function addCredits():void
		{
			_credits = new Credits(null, this);
			addChild(_credits);
			_credits.x = PathogenTest.stageCenter.x;
			_credits.y = PathogenTest.stageCenter.y;
		}
		public function removeCredits():void
		{
			_credits.dispose();
			removeChild(_credits);
			_credits = null;
		}
		public function addAchievementPopUp(achievementNumber:int):void
		{
			var achievementPopUp:AchievementPopUp = new AchievementPopUp();
			addChild(achievementPopUp);
			achievementPopUp.init(achievementNumber);
			achievementPopUp.x = PathogenTest.stageCenter.x;
			achievementPopUp.pivotX = achievementPopUp.width/2;
			achievementPopUp.y = PathogenTest.stageHeight;
			achievementPopUp.campaign = this;
			achievementPopUp.start();
		}
		public function removeAchievementPopUp(achivementPopUp:AchievementPopUp):void
		{
			achivementPopUp.dispose();
			removeChild(achivementPopUp);
			achivementPopUp = null;
		}
		override public function death():void
		{
			for each(var level:Level in campaignArray)
			{
				level.removeTouchListeners();
				level.dispose();
				removeChild(level);
				level = null;
			}
			
			campaignArray = [];
			currentCampaignLevel = null;
			
			visibleLevelHolder.dispose();
			removeChild(visibleLevelHolder);
			visibleLevelHolder = null;
			
			backBt.removeEventListener(TouchEvent.TOUCH, quitCampaign);
			backBt.dispose();
			removeChild(backBt);
			backBt = null;
			
			difficultySelector.death();
			difficultySelector.dispose();
			removeChild(difficultySelector);
			difficultySelector = null;
			
			if(Flurry.isSupported)
				Flurry.endTimedEvent("Campaign");
			
			_main.removeCampaign();
			_main = null;
		}
		
		public function get saveXML():XML {return _saveXML};
		public function get main():Main {return _main};
		
		public function set zoomedInLevel(zoomedInLevel:Level):void { _zoomedInLevel = zoomedInLevel; }
	}
}