package Campaign
{
	import UI.Main;
	
	import Utils.BW_Button;
	
	import flash.geom.Point;
	import flash.net.SharedObject;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	
	public class Level extends Sprite
	{
		//Classes
		private var campaign:Campaign; 
		
		//Level Vars
		public var levelNumber:int;
		public var unlocked:Boolean = false;
		
		//UI
		private var cell:Image;
		private var mask:Quad;
		private var animationSpeed:Number = 1;
		private var progress:TextField
		
		//Game Vars
		public var numOfPlayers:int;
		public var playerColors:Array;
		public var playerLocations:Array;
		public var playerAI:Array;
		public var mapData:Array;
		public var mapName:String;
		public var mapSize:int;
		private var mapString:String;
		public var levelModes:Array;
		public var gameLength:int;
		public var turnLength:int;
		private var _worldDescription:String = "";
		public var _worldName:String;
		
		//SubLevel Ints
		public var nextLevels:Array = [];
		public var sublevels:Array = [];
		public var numOfSublevels:int;
		public var currentSubLevel:int = 0;
		public var chosenSubLevel:SubLevel;
		public var furthestSubLevel:int = 0;
		
		//Zoom
		public var zoomedIn:Boolean;
		private var destination:Point;
		private var speed:Number = 5;
		private var oldLocation:Point;
		private var subLevelHolder:Sprite;
		
		//Difficulty
		private var easyDifficulty:Image;
		private var mediumDifficulty:Image;
		private var hardDifficulty:Image;
		public var easy:Boolean;
		public var medium:Boolean;
		public var hard:Boolean;
		public var arrayOfDifficulties:Array = [];
		
		//Save
		private var sharedDataObject:SharedObject;
		
		public function Level(campaign:Campaign, levelNumber:int)
		{
			super();
			
			this.campaign = campaign;
			this.levelNumber = levelNumber;
			sharedDataObject = Main.saveDataObject;
			
			//Default Settings
			cell = new Image(Main.assets.getTexture("Cell_Small"));
			cell.pivotX = cell.width/2;
			cell.pivotY = cell.height/2;
			addChild(cell);
			
			//Create level number
			var fontSize:int = PathogenTest.HD_Multiplyer * 54 / PathogenTest.scaleFactor;
			var levelDisplay:TextField = new TextField(cell.width, cell.height, (levelNumber + 1).toString(), "Questrial", fontSize, Color.WHITE);
			levelDisplay.pivotX = levelDisplay.width/2;
			levelDisplay.pivotY = levelDisplay.height/2;
			addChild(levelDisplay);
			
			//Generic defualt game settings, overwritten by individual levels if need be.
			numOfPlayers = 2;
			playerColors = [0, 1];
			playerLocations = [0,1];
			playerAI = [false,true];
			mapSize = 0;
			levelModes = [];
			gameLength = 100;
			turnLength = 30;
			
			//All levels are a little unique.
			switch(levelNumber)
			{
				case 0:
				{
					mapName = "Getting Started";
					_worldDescription = "Getting Started: Learn how to place those first few cells.";
					
					mapString = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,3,3,3,3,3,3,3,3," +
						"3,3,3,3,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,3,3,3,0,0,0,0,0,0,3,3,3,4,4,3,3,3,0,0,0,0,0,0,3,3,3,4,4,3,3,3,0,0,0,0,0" +
						",0,3,3,3,4,4,3,3,3,0,0,0,0,0,0,3,3,3,4,4,3,3,3,0,0,0,0,0,0,3,3,3,4,4,3,3,3,0,0,0,0,0,0,3,3,3,4,4,3,3,3,3,3,3,3,3" +
						",3,3,3,3,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4," +
						"4,4,4,4,4,4,";
					
					levelModes = ["Tutorial_01"];
					nextLevels = [1];
					break;
				}
				case 1:
				{
					mapName = "Evolution";
					_worldDescription = "Evolution: Learn about B Cells & how they spread."
					
					mapString = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,3,3,2,1,0,0,0,1," +
						"2,3,3,3,4,4,3,3,1,0,5,5,5,0,1,3,3,3,4,4,3,3,0,5,6,6,6,5,0,3,3,3,4,4,3,3,0,0,5,5,5,0,0,3,3,3,4,4,3,3,0,0,0,0,0," +
						"0,0,3,3,3,4,4,3,3,0,0,0,0,0,0,0,3,3,3,4,4,3,3,0,0,9,9,9,0,0,3,3,3,4,4,3,3,0,9,10,10,10,9,0,3,3,3,4,4,3,3,1,0,9" +
						",9,9,0,1,3,3,3,4,4,3,3,2,1,0,0,0,1,2,3,3,3,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4" +
						",4,4,4,4,4,4,4,4,4,";
					
					levelModes = ["Tutorial_02"];
					nextLevels = [2];
					break;
				}
				case 2:
				{
					mapName = "Locked Down";
					_worldDescription = "Locked Down: Learn how to take and hold territory."
					
					mapString = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,3,3,2,1,0,0,0,0,1,2,3,3,4,4,3,2,1,0,0,6,6,0,0" +
						",1,2,3,4,4,3,1,0,0,0,0,0,0,0,0,1,3,4,4,3,0,0,0,0,7,7,0,0,0,0,3,4,4,3,0,0,0,0,7,7,0,0,0,0,3,4,4,3,9,0,7,7,7,7,7," +
						"7,0,5,3,4,4,3,9,0,11,11,11,11,11,11,0,5,3,4,4,3,0,0,0,0,11,11,0,0,0,0,3,4,4,3,0,0,0,0,11,11,0,0,0,0,3,4,4,3,1,0" +
						",0,0,0,0,0,0,0,1,3,4,4,3,2,1,0,0,10,10,0,0,1,2,3,4,4,3,3,2,1,0,0,0,0,1,2,3,3,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4," +
						"4,4,4,4,4,4,4,4,4,4,4,4,";
					
					levelModes = ["Tutorial_03"];
					nextLevels = [3];
					break;
				}       
				case 3:
				{
					mapName = "Conquer";
					_worldDescription = "Conquer: Learn how to take over enemy cells."
					
					mapString = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,3,0,0,0,0,0,0,0,0,0,0,3,4,4,3,0,10,0,9,0,0,5," +
						"0,6,0,3,4,4,3,0,10,0,9,0,0,5,0,6,0,3,4,4,3,0,10,0,9,0,0,5,0,6,0,3,4,4,3,0,10,0,9,0,0,5,0,6,0,3,4,4,3,0,10,0,9,0" +
						",0,5,0,6,0,3,4,4,3,0,10,0,9,0,0,5,0,6,0,3,4,4,3,0,10,0,9,0,0,5,0,6,0,3,4,4,3,0,10,0,9,0,0,5,0,6,0,3,4,4,3,0,10," +
						"0,9,0,0,5,0,6,0,3,4,4,3,0,10,0,9,0,0,5,0,6,0,3,4,4,3,0,0,0,0,0,0,0,0,0,0,3,4,4,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4," +
						"4,4,4,4,4,4,4,4,4,4,4,";
					
					levelModes = ["Tutorial_04"];
					nextLevels = [4];
					break;
				}
				case 4:
				{
					mapName = "Nuclear Option";
					_worldDescription = "Nuclear Option: Learn how to destroy cells."
					
					mapString = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,1,2,2,1,0,0,0,0,4,4,0,0,0,0,0,1,1,0,0,0,0,0,4,4,0,0,7,0,0,0,0,0,0,7,0,0,4,4,0,0,0,0,7,7,7,7,0,0,0,0,4,4,1,0,0,7,7,7,7,7,7,0,0,1,4,4,2,1,0,7,7,7,7,7,7,0,1,2,4,4,3,2,1,7,7,7,7,7,7,1,2,3,4,4,3,2,1,11,11,11,11,11,11,1,2,3,4,4,2,1,0,11,11,11,11,11,11,0,1,2,4,4,1,0,0,11,11,11,11,11,11,0,0,1,4,4,0,0,0,0,11,11,11,11,0,0,0,0,4,4,0,0,7,0,0,0,0,0,0,7,0,0,4,4,0,0,0,0,0,1,1,0,0,0,0,0,4,4,0,0,0,0,1,2,2,1,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,";
					
					levelModes = ["Tutorial_05"];
					nextLevels = [5,6];
					break;
				}
				case 5:
				{
					mapName = "Outmanned";
					_worldDescription = "Outmanned: Plan carefully, the enemy already has cells covering parts of the board.";
					numOfSublevels = 5;
					nextLevels = [7];
					break;
				}
				case 6:
				{
					mapName = "Capture Zone";
					_worldDescription = "Capture Zone: Think King of the Hill and act quickly because in 10 rounds everything will change."
					numOfSublevels = 5;
					nextLevels = [8];
					break;
				}
				case 7:
				{
					mapName = "Infection";
					_worldDescription = "Infection: What happens when replication gets out of control."
					numOfSublevels = 5;
					nextLevels = [9];
					break;
				}
				case 8:
				{
					mapName = "Erosion Zone";
					_worldDescription = "Erosion Zone: Nothing is as it first appears."
					numOfSublevels = 5;
					nextLevels = [10];
					break;
				}
				case 9:
				{
					mapName = "Underdog";
					_worldDescription = "Underdog: May the strongest win."
					numOfSublevels = 5;
					nextLevels = [11];
					playerAI = [false,true,true];
					break;
				}
				case 10:
				{
					mapName = "Lightning";
					_worldDescription = "Lightning: Act quickly, the game will end after a set period of time."
					numOfSublevels = 5;
					nextLevels = [11];
					break;
				}
				case 11:
				{
					mapName = "Final";
					_worldDescription = "Final: The final challenge with a little bit of everything."
					
					mapString = "4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,10,9,0,9,3,9,0,0,0,0,9,3,9,0,9,10,3,3,4,4,3,10,9,0,0,0,9,0,0,0,0,0,0,9,0,0,0,9,10,3,4,4,10,9,21,21,21,21,0,1,0,1,1,0,1,0,21,21,21,21,9,10,4,4,9,0,21,21,21,21,0,0,0,0,0,0,0,0,21,21,21,21,0,9,4,4,0,0,21,21,21,21,0,0,0,0,0,0,0,0,21,21,21,21,0,0,4,4,9,0,21,21,21,21,0,1,0,1,1,0,1,0,21,21,21,21,0,9,4,4,3,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,3,4,4,9,22,22,22,22,22,12,12,12,12,12,12,12,12,22,22,22,22,22,9,4,4,0,22,2,22,2,22,12,12,12,12,12,12,12,12,22,2,22,2,22,0,4,4,0,22,22,22,22,22,12,12,12,12,12,12,12,12,22,22,22,22,22,0,4,4,0,22,2,22,2,22,12,12,12,12,12,12,12,12,22,2,22,2,22,0,4,4,0,22,2,22,2,22,12,12,12,12,12,12,12,12,22,2,22,2,22,0,4,4,0,22,22,22,22,22,12,12,12,12,12,12,12,12,22,22,22,22,22,0,4,4,0,22,2,22,2,22,12,12,12,12,12,12,12,12,22,2,22,2,22,0,4,4,9,22,22,22,22,22,12,12,12,12,12,12,12,12,22,22,22,22,22,9,4,4,3,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,3,4,4,9,0,21,21,21,21,0,1,0,1,1,0,1,0,21,21,21,21,0,9,4,4,0,0,21,21,21,21,0,0,0,0,0,0,0,0,21,21,21,21,0,0,4,4,9,0,21,21,21,21,0,0,0,0,0,0,0,0,21,21,21,21,0,9,4,4,10,9,21,21,21,21,0,1,0,1,1,0,1,0,21,21,21,21,9,10,4,4,3,10,9,0,0,0,9,0,0,0,0,0,0,9,0,0,0,9,9,3,4,4,3,3,10,9,0,9,3,9,0,0,0,0,9,3,9,0,9,10,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4";
					
					nextLevels = [];
					mapSize = 2;
					break;
				}
					
				default:
					break;
			}
			
			_worldName = mapName;
			
			if(mapString != null)
			{
				mapData = mapString.split(",");
				mapData.pop();
			}
			
			//If a level has sub-levels.
			if(numOfSublevels)
			{
				for (var i:int = 0; i < numOfSublevels; i++)
				{
					var sublevel:SubLevel = new SubLevel(i, this);
					sublevels.push(sublevel);
				}
				
				//Unlock the first sub-level
				sublevels[0].unlocked = true;
				selectedSubLevel(sublevels[0]);
				
				//If a level has sub-levels, show 1/5 completion rate.
				fontSize = PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor;
				progress = new TextField(cell.width, cell.height, currentSubLevel + "/" + numOfSublevels, "Questrial", fontSize, Color.WHITE);
				progress.pivotX = progress.width/2;
				//progress.pivotY = title.height/2;
				addChild(progress);
				//progress.y = cell.height * .15;
				progress.y = -cell.height * .75;
			}
			
			if(levelNumber > 4)
			{
				//Non-tutorial levels have multiple difficulty levels.
				easyDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell"));
				easyDifficulty.pivotX = easyDifficulty.width/2;
				easyDifficulty.pivotY = easyDifficulty.height/2;
				addChild(easyDifficulty);
				easyDifficulty.x = -cell.width * .3;
				easyDifficulty.y = cell.height * .35;
				
				mediumDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell"));
				mediumDifficulty.pivotX = mediumDifficulty.width/2;
				mediumDifficulty.pivotY = mediumDifficulty.height/2;
				addChild(mediumDifficulty);
				mediumDifficulty.y = cell.height * .45;
				
				hardDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell"));
				hardDifficulty.pivotX = hardDifficulty.width/2;
				hardDifficulty.pivotY = hardDifficulty.height/2;
				addChild(hardDifficulty);
				hardDifficulty.x = cell.width * .3;
				hardDifficulty.y = easyDifficulty.y;
			}
			
			addEventListener(TouchEvent.TOUCH, onTouch);
			
			addLine();
			
			flatten();
		}
		private function addLine():void
		{
			var quad:Quad;
			var quad_02:Quad;
			var quad_03:Quad;
			
			switch(levelNumber)
			{
				case 1:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2, Color.WHITE);
							break;
						
						case PathogenTest.iPHONE_5:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_2:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.25, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_HD:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.25, Color.WHITE);
							break;
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.78, Color.WHITE);
							
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.3, Color.WHITE);
							
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.3, Color.WHITE)
									
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.9, Color.WHITE);
							
							break;
						}
						
						case PathogenTest.NOOK_HD_PLUS:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2, Color.WHITE);
							break;
						
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.25, Color.WHITE);
							break;
						
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.18, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.1, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.5, Color.WHITE);
							break;
						
						default:
							break;
					}
					
					if(quad)
					{
						addChild(quad);
						quad.pivotX = quad.width/2;
						quad.pivotY = quad.height;
						quad.y = -cell.height * .45;
					}
					break;
				}
				case 2:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1, Color.WHITE);
							quad.x = -cell.width * .88;
							break;
						
						case PathogenTest.iPHONE_5:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.54, Color.WHITE);
							quad.x = -cell.width * 1.12;
							break;
						
						case PathogenTest.iPAD_2:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .90, Color.WHITE);
							quad.x = -cell.width * .875;
							break;
						
						case PathogenTest.iPAD_HD:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .90, Color.WHITE);
							quad.x = -cell.width * .875;
							break;
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.2, Color.WHITE);
								quad.x = -cell.width * .885;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.5, Color.WHITE);
								quad.x = -cell.width * 1.225;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.5, Color.WHITE);
								quad.x = -cell.width * 1.225;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.08, Color.WHITE);
								quad.x = -cell.width * .875;
							}
							
							break;
						}
						
						case PathogenTest.NOOK_HD_PLUS:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1, Color.WHITE);
							quad.x = -cell.width * .88;
							break;
						
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.57, Color.WHITE);
							quad.x = -cell.width * 1.22;
							break;
						
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .55, Color.WHITE);
							quad.x = -cell.width * .6;
							break;
						
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.7, Color.WHITE);
							quad.x = -cell.width * 1.22;
							break;
						
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.05, Color.WHITE);
							quad.x = -cell.width * .8;
							break;
						
						default:
							break;
					}
					
					if(quad)
					{
						quad.pivotX = quad.width/2;
						quad.pivotY = quad.height;
						addChild(quad);
						quad.rotation = deg2rad(90);
					}
					break;
				}
				case 3:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2, Color.WHITE);
							break;
						
						case PathogenTest.iPHONE_5:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_2:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.25, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_HD:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.25, Color.WHITE);
							break;
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.78, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.3, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.3, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.9, Color.WHITE);
							
							break;
						}
						
						case PathogenTest.NOOK_HD_PLUS:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2, Color.WHITE);
							break;
						
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.25, Color.WHITE);
							break;
						
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.18, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.1, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.5, Color.WHITE);
							break;
						
						default:
							break;
					}
					
					if(quad)
					{
						quad.pivotX = quad.width/2;
						addChild(quad);
						quad.y = cell.height * .43;
					}
					break;
				}
				case 4:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .87, Color.WHITE);
							quad.x = -cell.width * .58;
							quad.y = -cell.height * .58;
							break;
						
						case PathogenTest.iPHONE_5:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.1, Color.WHITE);
							quad.x = -cell.width * .67;
							quad.y = -cell.height * .67;
							break;
						
						case PathogenTest.iPAD_2:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .95, Color.WHITE);
							quad.x = -cell.width * .64;
							quad.y = -cell.height * .64;
							break;
						
						case PathogenTest.iPAD_HD:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .95, Color.WHITE);
							quad.x = -cell.width * .64;
							quad.y = -cell.height * .64;
							break;
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .85, Color.WHITE);
								quad.x = -cell.width * .54;
								quad.y = -cell.height * .54;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.25, Color.WHITE);
								quad.x = -cell.width * .77;
								quad.y = -cell.height * .77;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.25, Color.WHITE);
								quad.x = -cell.width * .77;
								quad.y = -cell.height * .77;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .85, Color.WHITE);
								quad.x = -cell.width * .56;
								quad.y = -cell.height * .56;
							}
							
							break;
						}
						
						case PathogenTest.NOOK_HD_PLUS:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .87, Color.WHITE);
							quad.x = -cell.width * .58;
							quad.y = -cell.height * .58;
							break;
						
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.26, Color.WHITE);
							quad.x = -cell.width * .75;
							quad.y = -cell.height * .75;
							break;
						
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .2, Color.WHITE);
							quad.x = -cell.width * .36;
							quad.y = -cell.height * .36;
							break;
						
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.26, Color.WHITE);
							quad.x = -cell.width * .73;
							quad.y = -cell.height * .73;
							break;
						
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * .6, Color.WHITE);
							quad.x = -cell.width * .46;
							quad.y = -cell.height * .46;
							break;
						
						default:
							break;
					}
					
					if(quad)
					{
						quad.pivotX = quad.width/2;
						addChild(quad);
						quad.rotation = deg2rad(-45);
					}
					break;
				}
				case 5:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.49, Color.WHITE);
							quad.y = cell.height * .8;
							quad.x = -cell.width * .78;
							break;
						
						case PathogenTest.iPHONE_5:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.1, Color.WHITE);
							quad.y = cell.height * .89;
							quad.x = -cell.width * .87;
							break;
						
						case PathogenTest.iPAD_2:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.45, Color.WHITE);
							quad.y = cell.height * .845;
							quad.x = -cell.width * .825;
							break;
						
						case PathogenTest.iPAD_HD:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.45, Color.WHITE);
							quad.y = cell.height * .845;
							quad.x = -cell.width * .825;
							break;
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.58, Color.WHITE);
								quad.y = cell.height * .75;
								quad.x = -cell.width * .75;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3, Color.WHITE);
								quad.y = cell.height * .955;
								quad.x = -cell.width * .935;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3, Color.WHITE);
								quad.y = cell.height * .955;
								quad.x = -cell.width * .935;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.5, Color.WHITE);
								quad.y = cell.height * .775;
								quad.x = -cell.width * .755;
							}
							break;
						}
						
						case PathogenTest.NOOK_HD_PLUS:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.49, Color.WHITE);
							quad.y = cell.height * .8;
							quad.x = -cell.width * .78;
							break;
						
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.08, Color.WHITE);
							quad.y = cell.height * .93;
							quad.x = -cell.width * .93;
							break;
						
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.83, Color.WHITE);
							quad.y = cell.height * .61;
							quad.x = -cell.width * .59;
							break;
						
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.3, Color.WHITE);
							quad.y = cell.height * .91;
							quad.x = -cell.width * .91;
							break;
						
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.45, Color.WHITE);
							quad.y = cell.height * .7;
							quad.x = -cell.width * .7;
							break;
						
						default:
							break;
					}
					
					if(quad)
					{
						quad.pivotX = quad.width/2;
						quad.pivotY = quad.height/2;
						addChild(quad);
						quad.rotation = deg2rad(45);
					}
					break;
				}
				case 6:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.8, Color.WHITE);
							quad.y = -cell.height * .77;
							quad.x = -cell.width * .75;
							break;
						
						case PathogenTest.iPHONE_5:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.34, Color.WHITE);
							quad.y = -cell.height * .855;
							quad.x = -cell.width * .835;
							break;
						
						case PathogenTest.iPAD_2:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.78, Color.WHITE);
							quad.y = -cell.height * .815;
							quad.x = -cell.width * .795;
							break;
						
						case PathogenTest.iPAD_HD:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.78, Color.WHITE);
							quad.y = -cell.height * .815;
							quad.x = -cell.width * .795;
							break;
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.925, Color.WHITE);
								quad.y = -cell.height * .73;
								quad.x = -cell.width * .71;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.275, Color.WHITE);
								quad.y = -cell.height * .91;
								quad.x = -cell.width * .89;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.275, Color.WHITE);
								quad.y = -cell.height * .91;
								quad.x = -cell.width * .89;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.8, Color.WHITE);
								quad.y = -cell.height * .735;
								quad.x = -cell.width * .715;
							}
							break;
						}
						
						case PathogenTest.NOOK_HD_PLUS:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.8, Color.WHITE);
							quad.y = -cell.height * .77;
							quad.x = -cell.width * .75;
							break;
						
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.35, Color.WHITE);
							quad.y = -cell.height * .905;
							quad.x = -cell.width * .885;
							break;
						
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.2, Color.WHITE);
							quad.y = -cell.height * .57;
							quad.x = -cell.width * .55;
							break;
						
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.49, Color.WHITE);
							quad.y = -cell.height * .9;
							quad.x = -cell.width * .88;
							break;
						
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.83, Color.WHITE);
							quad.y = -cell.height * .67;
							quad.x = -cell.width * .65;
							break;
						
						default:
							break;
					}
					
					if(quad)
					{
						quad.pivotX = quad.width/2;
						quad.pivotY = quad.height/2;
						addChild(quad);
						quad.rotation = deg2rad(-45);
					}
					break;
				}
				case 7:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.05, Color.WHITE);
							break;
						
						case PathogenTest.iPHONE_5:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.75, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_2:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.80, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_HD:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.80, Color.WHITE);
							break;
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.3, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.6, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.6, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.19, Color.WHITE);
							break;
						}
						
						case PathogenTest.NOOK_HD_PLUS:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.05, Color.WHITE);
							break;
						
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.7, Color.WHITE);
							break;
						
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.64, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.9, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.25, Color.WHITE);
							break;
						
						default:
							break;
					}
					
					if(quad)
					{
						quad.pivotX = quad.width/2;
						addChild(quad);
						quad.rotation = deg2rad(90);
						quad.x = -cell.width * .43;
					}
					break;
				}
				case 8:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.05, Color.WHITE);
							break;
						
						case PathogenTest.iPHONE_5:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.75, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_2:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.80, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_HD:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.80, Color.WHITE);
							break;
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.3, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.6, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.6, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.19, Color.WHITE);
							break;
						}
						
						case PathogenTest.NOOK_HD_PLUS:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.05, Color.WHITE);
							break;
						
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.7, Color.WHITE);
							break;
						
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.64, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.9, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.25, Color.WHITE);
							break;
						
						default:
							break;
					}
					
					if(quad)
					{
						quad.pivotX = quad.width/2;
						addChild(quad);
						quad.rotation = deg2rad(90);
						quad.x = -cell.width * .43;
					}
					break;
				}
				case 9:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.05, Color.WHITE);
							break;
						
						case PathogenTest.iPHONE_5:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.75, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_2:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.80, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_HD:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.80, Color.WHITE);
							break;
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.3, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.6, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.6, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.19, Color.WHITE);
							break;
						}
						
						case PathogenTest.NOOK_HD_PLUS:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.05, Color.WHITE);
							break;
						
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.7, Color.WHITE);
							break;
						
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.64, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.9, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.25, Color.WHITE);
							break;
						
						default:
							break;
					}
					
					if(quad)
					{
						quad.pivotX = quad.width/2;
						addChild(quad);
						quad.rotation = deg2rad(90);
						quad.x = -cell.width * .43;
					}
					break;
				}
				case 10:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.05, Color.WHITE);
							break;
						
						case PathogenTest.iPHONE_5:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.75, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_2:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.80, Color.WHITE);
							break;
						
						case PathogenTest.iPAD_HD:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.80, Color.WHITE);
							break;
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.3, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.6, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.6, Color.WHITE);
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.19, Color.WHITE);
							break;
						}
						
						case PathogenTest.NOOK_HD_PLUS:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.05, Color.WHITE);
							break;
						
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.7, Color.WHITE);
							break;
						
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.64, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.9, Color.WHITE);
							break;
						
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.25, Color.WHITE);
							break;
						
						default:
							break;
					}
					
					if(quad)
					{
						quad.pivotX = quad.width/2;
						addChild(quad);
						quad.rotation = deg2rad(90);
						quad.x = -cell.width * .43;
					}
					break;
				}
				case 11:
				{
					switch(PathogenTest.device)
					{
						case PathogenTest.iPHONE_4:
						{
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.675, Color.WHITE);
							quad.x = cell.width * 1.2;
							
							quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 4.05, Color.WHITE);
							quad_02.y = -cell.height * .62;
							quad_02.x = cell.width * 1.82;
							
							quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 4, Color.WHITE);
							quad_03.y = cell.height * .63;
							quad_03.x = cell.width * 1.83;
							break;
						}
							
						case PathogenTest.iPHONE_5:
						{
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.92, Color.WHITE);
							quad.x = cell.width * 1.72;
							
							quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 4.05, Color.WHITE);
							quad_02.y = -cell.height * .62;
							quad_02.x = cell.width * 2.35;
							
							quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 4, Color.WHITE);
							quad_03.y = cell.height * .63;
							quad_03.x = cell.width * 2.35;
							break;
						}
						
						case PathogenTest.iPAD_2:
						{
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.55, Color.WHITE);
							quad.x = cell.width * 1.2;
							
							quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.5, Color.WHITE);
							quad_02.y = -cell.height * .617;
							quad_02.x = cell.width * 1.817;
							
							quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.76, Color.WHITE);
							quad_03.y = cell.height * .665;
							quad_03.x = cell.width * 1.865;	
							break;
						}
						
						case PathogenTest.iPAD_HD:
						{
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.55, Color.WHITE);
							quad.x = cell.width * 1.2;
							
							quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.5, Color.WHITE);
							quad_02.y = -cell.height * .617;
							quad_02.x = cell.width * 1.817;
							
							quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.76, Color.WHITE);
							quad_03.y = cell.height * .665;
							quad_03.x = cell.width * 1.865;	
							break;
						}
						
						case PathogenTest.ANDROID:
						{
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.15, Color.WHITE);
								quad.x = cell.width * 1.635;
								
								quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.05, Color.WHITE);
								quad_02.y = -cell.height * .43;
								quad_02.x = cell.width * 2.05;
								
								quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.4, Color.WHITE);
								quad_03.y = cell.height * .47;
								quad_03.x = cell.width * 2.1;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HD)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.7, Color.WHITE);
								quad.x = cell.width * 1.82;
								quad.y = cell.height * .05
								
								quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.7, Color.WHITE);
								quad_02.y = -cell.height * .617;
								quad_02.x = cell.width * 2.5;
								
								quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.79, Color.WHITE);
								quad_03.y = cell.height * .72;
								quad_03.x = cell.width * 2.5;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX_8inch)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.7, Color.WHITE);
								quad.x = cell.width * 1.82;
								quad.y = cell.height * .05
								
								quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.7, Color.WHITE);
								quad_02.y = -cell.height * .617;
								quad_02.x = cell.width * 2.5;
								
								quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.79, Color.WHITE);
								quad_03.y = cell.height * .72;
								quad_03.x = cell.width * 2.5;
							}
							if(PathogenTest.KINDLE_TYPE == PathogenTest.KINDLE_FIRE_HDX)
							{
								quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.9, Color.WHITE);
								quad.x = cell.width * 1.22;
								
								quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 4.15, Color.WHITE);
								quad_02.y = -cell.height * .59;
								quad_02.x = cell.width * 1.8;
								
								quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 4.1, Color.WHITE);
								quad_03.y = cell.height * .61;
								quad_03.x = cell.width * 1.81;
							}
							break;
						}
						case PathogenTest.NOOK_HD_PLUS:
						{
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 1.675, Color.WHITE);
							quad.x = cell.width * 1.2;
							
							quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 4.05, Color.WHITE);
							quad_02.y = -cell.height * .62;
							quad_02.x = cell.width * 1.82;
							
							quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 4, Color.WHITE);
							quad_03.y = cell.height * .63;
							quad_03.x = cell.width * 1.83;
							break;
						}
							
						case PathogenTest.GOOGLE_NEXUS_4 && PathogenTest.sPixelsPerInch == 318:
						{
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.05, Color.WHITE);
							quad.x = cell.width * 1.95;
							
							quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.5, Color.WHITE);
							quad_02.y = -cell.height * .617;
							quad_02.x = cell.width * 2.55;
							
							quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.76, Color.WHITE);
							quad_03.y = cell.height * .665;
							quad_03.x = cell.width * 2.6;	
							break;
						}
							
						case PathogenTest.LG_OPTIMUS && PathogenTest.sPixelsPerInch == 235:
						{
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.6, Color.WHITE);
							quad.x = cell.width * 1.225;
							
							quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.8, Color.WHITE);
							quad_02.y = -cell.height * .30;
							quad_02.x = cell.width * 1.51;
							
							quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.2, Color.WHITE);
							quad_03.y = cell.height * .36;
							quad_03.x = cell.width * 1.57;
							break;
						}
							
						case PathogenTest.MOTOROLA_MOTO_X && PathogenTest.sPixelsPerInch == 316:
						{
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.25, Color.WHITE);
							quad.x = cell.width * 1.95;
							
							quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.58, Color.WHITE);
							quad_02.y = -cell.height * .58;
							quad_02.x = cell.width * 2.53;
							
							quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.86, Color.WHITE);
							quad_03.y = cell.height * .636;
							quad_03.x = cell.width * 2.57;	
							break;
						}
							
						case PathogenTest.MOTOROLA_DROID_RAZR && PathogenTest.sPixelsPerInch == 256:
						{
							quad = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 2.85, Color.WHITE);
							quad.x = cell.width * 1.42;
							
							quad_02 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.2, Color.WHITE);
							quad_02.y = -cell.height * .4;
							quad_02.x = cell.width * 1.83;
							
							quad_03 = new Quad(PathogenTest.wTenPercent * .03, PathogenTest.hTenPercent * 3.6, Color.WHITE);
							quad_03.y = cell.height * .44;
							quad_03.x = cell.width * 1.87;	
							break;
						}
							
						default:
							break;
					}
					
					if(quad)
					{
						quad.pivotX = quad.width/2;
						addChild(quad);
						quad.rotation = deg2rad(90);
					}
					
					if(quad_02)
					{
						quad_02.pivotX = quad_02.width/2;
						quad_02.pivotY = quad_02.height/2;
						addChild(quad_02);
						quad_02.rotation = deg2rad(45);
					}
					
					if(quad_03)
					{
						quad_03.pivotX = quad_03.width/2;
						quad_03.pivotY = quad_03.height/2;
						addChild(quad_03);
						quad_03.rotation = deg2rad(-45);
					}
					
					break;
				}
			}
		}
		public function onTouch($e:TouchEvent):void
		{
			if($e != null)
				var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if (touch || $e == null)
			{
				if(Main._lite && Main._locked && levelNumber > 6)
				{
					campaign.main.openContentLockedScreen();
				}
				else
				{
					if(alpha == 1)
						campaign.addLevelScreen(this);
					
					sharedDataObject.data.GameLength = gameLength;
					sharedDataObject.flush();
					
					sharedDataObject.data.TurnLength = turnLength;
					sharedDataObject.flush();
				}
			}
		}
		public function selectedSubLevel(whatSubLevel:SubLevel):void
		{
			//Play a particular sublevel.
			chosenSubLevel = whatSubLevel;
			numOfPlayers = chosenSubLevel.numOfPlayers;
			playerColors = chosenSubLevel.playerColors;
			playerLocations = chosenSubLevel.playerLocations;
			playerAI = chosenSubLevel.playerAI;
			mapData = chosenSubLevel.mapString.split(",");
			mapData.pop();
			mapName = chosenSubLevel.mapName;
			mapSize = chosenSubLevel.mapSize;
			levelModes = chosenSubLevel.levelModes;
			gameLength = chosenSubLevel.gameLength;
			turnLength = chosenSubLevel.turnLength;
			
			sharedDataObject.data.GameLength = gameLength;
			sharedDataObject.flush();
			
			sharedDataObject.data.TurnLength = turnLength;
			sharedDataObject.flush();
		}
		public function unlockNextSubLevel():void
		{
			unflatten();
			
			furthestSubLevel++;
			
			progress.text = furthestSubLevel + "/" + numOfSublevels;
			
			//Unlock the next sub level in the list.
			if(furthestSubLevel < numOfSublevels)
			{
				sublevels[furthestSubLevel].unlocked = true;
				
				progress.text = furthestSubLevel + "/" + numOfSublevels;
			}
		}
		public function updateProgressText(numOfCompletedSubLevels:int):void
		{
			unflatten()
			progress.text = numOfCompletedSubLevels + "/" + numOfSublevels;
			flatten();
		}
		public function checkDifficulty(whatDifficulty:int):void
		{
			//trace("Level: " + levelNumber + " Check Difficulty: " + whatDifficulty);
			
			var subLevel:SubLevel;
			
			if(levelNumber == 11)
				unflatten();
			
			//Check to see if a difficulty was completed.
			switch(whatDifficulty)
			{
				case 1:
				{
					//Only check if it has not already been accomplished
					if(!easy)
					{
						var allCompletedEasy:Boolean = true;
						
						if(numOfSublevels > 0)
						{
							//In order for a non-tutorial level to be marked complete, all children sub-levels must have been beaten in that difficulty
							for each (subLevel in sublevels)
							{
								if(!subLevel.easy)
									allCompletedEasy = false;
							}
						}
						
						if(allCompletedEasy)
						{
							easy = true;
							arrayOfDifficulties.push(whatDifficulty);
							
							if(levelNumber > 4)
							{
								removeChild(easyDifficulty);
								
								easyDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell_Easy"));
								easyDifficulty.pivotX = easyDifficulty.width/2;
								easyDifficulty.pivotY = easyDifficulty.height/2;
								addChild(easyDifficulty);
								easyDifficulty.x = -cell.width * .3;
								easyDifficulty.y = cell.height * .35;
							}
						}
					}
					break;
				}
				case 2:
				{
					//Only check if it has not already been accomplished
					if(!medium)
					{
						var allCompletedMedium:Boolean = true;
						
						if(numOfSublevels > 0)
						{
							//In order for a non-tutorial level to be marked complete, all children sub-levels must have been beaten in that difficulty
							for each (subLevel in sublevels)
							{
								if(!subLevel.medium)
									allCompletedMedium = false;
							}
						}
						
						if(allCompletedMedium)
						{
							medium = true;
							arrayOfDifficulties.push(whatDifficulty);
							
							if(levelNumber > 4)
							{
								removeChild(mediumDifficulty);
								
								mediumDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell_Med"));
								addChild(mediumDifficulty);
								mediumDifficulty.pivotX = mediumDifficulty.width/2;
								mediumDifficulty.pivotY = mediumDifficulty.height/2;                                            
								addChild(mediumDifficulty);
								mediumDifficulty.y = cell.height * .45;
							}
						}
					}
					break;
				}
				case 3:
				{
					//Only check if it has not already been accomplished
					if(!hard)
					{
						var allCompletedHard:Boolean = true;
						
						if(numOfSublevels > 0)
						{
							//In order for a non-tutorial level to be marked complete, all children sub-levels must have been beaten in that difficulty
							for each (subLevel in sublevels)
							{
								if(!subLevel.hard)
								{
									allCompletedHard = false;
								}
							}
						}
						
						if(allCompletedHard)
						{
							hard = true;
							arrayOfDifficulties.push(whatDifficulty);
							
							if(levelNumber > 4)
							{
								removeChild(hardDifficulty);
								
								hardDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell_Hard"));
								hardDifficulty.pivotX = hardDifficulty.width/2;
								hardDifficulty.pivotY = hardDifficulty.height/2;
								addChild(hardDifficulty);
								hardDifficulty.x = cell.width * .3;
								hardDifficulty.y = easyDifficulty.y;
							}
						}
					}
					break;
				}
					
				default:
					break;
			}
			
			if(levelNumber == 11)
				flatten();
		}
		public function removeTouchListeners():void
		{
			for each (var subLevel:SubLevel in sublevels)
			{
				subLevel.garbageCollection();
				subLevel.dispose();
				subLevel = null;
			}
			
			sublevels = [];
			
			if(subLevelHolder)
			{
				subLevelHolder.dispose();
				removeChild(subLevelHolder);
				subLevelHolder = null;
			}
				
			campaign = null;
		}
		
		public function get levelName():String {return mapName};
		public function get worldDescription():String {return _worldDescription};
	}
}