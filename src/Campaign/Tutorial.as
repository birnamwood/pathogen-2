package Campaign
{
	import Core.GameManager;
	
	import UI.Main;
	
	import Utils.BW_UI;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class Tutorial extends BW_UI
	{
		//Classes
		private var gameManager:GameManager;
		
		//Display Objects
		private var tutorialScreen:Sprite;
		private var tutorialMessage:TextField;
		public var currentPlace:int = 0;
		private var _currentTextArray:Array;
		private var _complete:Boolean;
		
		//Text
		private var tutorialLevel_01Text:Array = ["Tap to place an A-Cell", "Two of the same A-Cells can be stacked to evolve", "The game is over when all spaces are filled.", "The player with the most territory wins.", "You can undo your move at any time.", "You can track your progress above.", "Complete this map to progress"];
		private var tutorialLevel_02Text:Array = ["A B-Cell has been unlocked! Tap it now.", "Two of the same B-Cells can be stacked to evolve.", "B-Cells must charge to become active.", "B-Cells take 4 turns to charge", "Complete this map to progress"];
		private var tutorialLevel_03Text:Array = ["A C-Cell has been unlocked! Tap it now.", "Two of the same C-Cells can be stacked to evolve.", "C-Cells will capture all adjacent C-Cells, even your enemies, evolving them into Wall Cells.", "Wall cells cannot be taken or destroyed by an enemy.", "C-Cells take 8 turns to charge.", "Complete this map to progress"];
		private var tutorialLevel_04Text:Array = ["You can conquer enemy cells by placing higher level cells on lower level cells.", "B-Cells can conquer enemy A-Cells", "C-Cells can conquer enemy B-Cells", "Complete this map to progress"];
		private var tutorialLevel_05Text:Array = ["The Virus has been unlocked! Tap it now.", "The Virus destroys all adjacent cells of the same type, even your own.", "Viruses take 10 turns to charge.", "Complete this map to progress"];
		 
		private var tutorialCaptureZoneText:Array = ["Capture Zones are special locations that exist for 10 rounds", "After 10 rounds, all cells within the zones will transform", "Zones can be claimed early if owned entirely by 1 player"];
		private var tutorialErosionZoneText:Array = ["Erosion maps have special locations that will decay over time", "After a unknown period of time they will crumble, opening new areas"];
		private var tutorialInfectionText:Array = ["In this map type cells replicate at a much higher rate", "At the end of every round all A & B cells will duplicate into all adjacent locations"];
		
		public function Tutorial(gameManager:GameManager, whichTextArray:String)
		{
			super();
			
			this.gameManager = gameManager;
			
			var background:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.hTenPercent * 2, Color.BLACK);
			addChild(background);
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .75;
			
			//Main.updateDebugText("in tutorial");
			
			var textX:int = background.width;
			if (textX > Main.TEXTURE_LIMIT)
				textX = Main.TEXTURE_LIMIT;
			//tutorialMessage = new TextField(background.width, background.height, "", "Dekar",PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor, Color.WHITE);
			//tutorialMessage = new TextField(background.width * 0.8, background.height, "", "Dekar",PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor, Color.WHITE);
			tutorialMessage = new TextField(textX, background.height, "", "Dekar",PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor, Color.WHITE);
			tutorialMessage.pivotX = tutorialMessage.width/2;
			tutorialMessage.pivotY = tutorialMessage.height/2;
			addChild(tutorialMessage);
			
			//Main.updateDebugText("created textfield");
			
			var clickToRemoveText:TextField = new TextField(PathogenTest.wTenPercent * 2, PathogenTest.hTenPercent * .6, "Tap to remove", "Dekar",PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
			clickToRemoveText.pivotX = clickToRemoveText.width;
			clickToRemoveText.pivotY = clickToRemoveText.height;
			addChild(clickToRemoveText);
			clickToRemoveText.x = background.width/2;
			clickToRemoveText.y = background.height/2;
			
			switch(whichTextArray)
			{
				case "Tutorial_01":
					_currentTextArray = tutorialLevel_01Text;
					break;
				
				case "Tutorial_02":
					_currentTextArray = tutorialLevel_02Text;
					break;
				
				case "Tutorial_03":
					_currentTextArray = tutorialLevel_03Text;
					break;
				
				case "Tutorial_04":
					_currentTextArray = tutorialLevel_04Text;
					break;
				
				case "Tutorial_05":
					_currentTextArray = tutorialLevel_05Text;
					break;
				
				case "TutorialCaptureZone":
					_currentTextArray = tutorialCaptureZoneText;
					break;
				
				case "TutorialErosionZone":
					_currentTextArray = tutorialErosionZoneText;
					break;
				
				case "TutorialInfection":
					_currentTextArray = tutorialInfectionText;
					break;
			}
			
			tutorialMessage.text = _currentTextArray[currentPlace];
			
			tutorialMessage.addEventListener(TouchEvent.TOUCH, hideTutorial);
		}
		public function hideTutorial($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if(touch)
				visible = false;
		}
		public function nextPage():void
		{
			currentPlace++;
			
			tutorialMessage.text = _currentTextArray[currentPlace];
			
			if(currentPlace == _currentTextArray.length-1)
				_complete = true;
		}
		public function setPage(whatPlace:int):void
		{
			currentPlace = whatPlace;
			if(currentPlace < _currentTextArray.length - 1)
			{
				tutorialMessage.text = _currentTextArray[currentPlace];
			}
		}
		public function get complete():Boolean {return _complete};
	}
}