package Campaign
{
	import UI.Main;
	
	import Utils.BW_Button;
	import Utils.BW_Selector;
	import Utils.BW_UI;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import flashx.textLayout.elements.OverflowPolicy;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	
	public class LevelScreen extends BW_UI
	{
		//Parent Class
		private var _campaign:Campaign;
		private var _level:Level;
		
		//Display
		private var _backButton:BW_Button;
		private var _startButton:BW_Button;
		
		private var _subLevelHolder:Sprite;
		private var _subLevelSelector:BW_Selector;
		
		private var _sublevelArray:Array = [];
		
		//End State
		private var _startGame:Boolean;
		
		public function LevelScreen(campaign:Campaign, level:Level)
		{
			_campaign = campaign;
			_level = level;	
			
			//Create a transparent background across the entire screen to prevent users from interacting with anything but this screen.
			var transBackground:Quad = new Quad(PathogenTest.stageWidth, PathogenTest.stageHeight, Color.BLACK);
			transBackground.pivotX = transBackground.width/2;
			transBackground.pivotY = transBackground.height/2;
			addChild(transBackground);
			transBackground.alpha = .25;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			var texture:Texture = Main.assets.getTexture("scale9_White");
			const textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			var background:Scale9Image;
			var header:Scale9Image;
			var worldDescription:TextField;
			var title:TextField;
			
			//If the level has sublevels lay it out this way
			if(_level.numOfSublevels > 0)
			{
				//Create this screens background
				background = new Scale9Image(textures);
				addChild(background);
				background.width = PathogenTest.wTenPercent * 7;
				
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					background.height = PathogenTest.hTenPercent * 7.5;
				else
					background.height = PathogenTest.hTenPercent * 7;
				
				
				
				background.pivotX = background.width/2;
				background.pivotY = background.height/2;
				background.color = 0x00001a;
				
				//Create Header for the title.
				header = new Scale9Image(textures);
				addChild(header);
				header.width = background.width;
				header.height = PathogenTest.hTenPercent;
				header.pivotX = header.width/2;
				header.y = -background.height/2;
				header.color = 0x222245;
				
				//"World" Title.
				title = new TextField(header.width, header.height, _level._worldName, "Questrial", PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor, Color.WHITE);
				title.pivotX = title.width/2;
				addChild(title);
				title.y = header.y;
				
				//Back button.
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_backButton = new BW_Button(.75,1, "", 0);
				else
					_backButton = new BW_Button(.5,.75, "", 0);
				
				_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
				addChild(_backButton);
				_backButton.x = - background.width/2 + (_backButton.width * .7);
				_backButton.y = background.height/2 - (_backButton.height * .7);
				_backButton.addEventListener(TouchEvent.TOUCH, onTouchQuit);
					
				_subLevelHolder = new Sprite();
				addChild(_subLevelHolder);
				
				for (var i:int = 0; i < _level.sublevels.length; i++)
				{
					var subLevel:SubLevel = _level.sublevels[i];
					_subLevelHolder.addChild(subLevel);
					_sublevelArray.push(subLevel);
					subLevel.addDisplay(this);
					subLevel.x = (subLevel.width * 1.2) * i;
				}
				
				var pickTitle:TextField = new TextField(header.width, header.height, "Select a Level", "Dekar",  PathogenTest.HD_Multiplyer * 34 / PathogenTest.scaleFactor, Color.WHITE);
				addChild(pickTitle);
				pickTitle.pivotX = pickTitle.width/2;
				pickTitle.y = header.y + header.height;
				
				_subLevelHolder.pivotX = _subLevelHolder.width/2;
				_subLevelHolder.y = -PathogenTest.hTenPercent * .7;
				_subLevelHolder.x = subLevel.width/2;
				
				var divider:Quad = new Quad(PathogenTest.wTenPercent * 4, PathogenTest.hTenPercent * .05, 0x222245);
				addChild(divider);
				divider.pivotX = divider.width/2;
				divider.y = _subLevelHolder.y + (_subLevelHolder.height * .8)
					
				selectSubLevel(_sublevelArray[0]);
				
				//World Desciption
				worldDescription = new TextField(PathogenTest.wTenPercent * 3, PathogenTest.hTenPercent * 2, _level.worldDescription, "Dekar", PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
				addChild(worldDescription);
				worldDescription.pivotX = worldDescription.width/2;
				worldDescription.pivotY = worldDescription.height/2;
				worldDescription.hAlign = HAlign.LEFT;
				worldDescription.x = -PathogenTest.wTenPercent * 1.5;
				worldDescription.y = divider.y + PathogenTest.hTenPercent * 1.2;
				
				//Start Game Button
				_startButton = new BW_Button(2.5, 1.5, "Start", 54 / PathogenTest.scaleFactor);
				addChild(_startButton);
				_startButton.y = worldDescription.y;
				_startButton.addEventListener(TouchEvent.TOUCH, onTouchStart);
				_startButton.x = PathogenTest.wTenPercent * 1.5;
			}
			else
			{
				//else lay it out this way.
				
				//Create this screens background
				background = new Scale9Image(textures);
				addChild(background);
				background.width = PathogenTest.wTenPercent * 6;
				background.height = PathogenTest.hTenPercent * 4;
				background.pivotX = background.width/2;
				background.pivotY = background.height/2;
				background.color = 0x00001a;
				
				//Create Header for the title.
				header = new Scale9Image(textures);
				addChild(header);
				header.width = background.width;
				header.height = PathogenTest.hTenPercent;
				header.pivotX = header.width/2;
				header.y = -background.height/2;
				header.color = 0x222245;
				
				//"World" Title.
				title = new TextField(header.width, header.height, _level.levelName, "Questrial", PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor, Color.WHITE);
				title.pivotX = title.width/2;
				addChild(title);
				title.y = header.y;
				
				//Back button.
				if (Main.isPhone())
				//if(PathogenTest.device == PathogenTest.iPHONE_4 || PathogenTest.device == PathogenTest.iPHONE_5)
					_backButton = new BW_Button(.75,1, "", 0);
				else
					_backButton = new BW_Button(.5,.75, "", 0);
				
				_backButton.addImage(Main.assets.getTexture("Pathogen_BTN_X"), Main.assets.getTexture("Pathogen_BTN_X_Down"));
				addChild(_backButton);
				_backButton.x = - background.width/2 + (_backButton.width * .7);
				_backButton.y = background.height/2 - (_backButton.height * .7);
				_backButton.addEventListener(TouchEvent.TOUCH, onTouchQuit);
				
				//Start Game Button
				_startButton = new BW_Button(2.5, 1.5, "Start", 54 / PathogenTest.scaleFactor);
				addChild(_startButton);
				_startButton.y = background.height/2 - (_backButton.height * 1.5);
				_startButton.addEventListener(TouchEvent.TOUCH, onTouchStart);
				
				//World Desciption
				worldDescription = new TextField(background.width, PathogenTest.hTenPercent * 2, _level.worldDescription, "Dekar", PathogenTest.HD_Multiplyer * 24 / PathogenTest.scaleFactor, Color.WHITE);
				addChild(worldDescription);
				worldDescription.pivotX = worldDescription.width/2;
				worldDescription.pivotY = worldDescription.height/2;
				worldDescription.y = -PathogenTest.hTenPercent * .5;
			}
			
			if(_level.levelNumber > 4)
			{
				var easyDifficulty:Image;
				var mediumDifficulty:Image;
				var hardDifficulty:Image;
				
				if(!_level.hard)
					hardDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell"));
				else
					hardDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell_Hard"));
				
				hardDifficulty.pivotX = hardDifficulty.width/2;
				hardDifficulty.pivotY = hardDifficulty.height/2;
				addChild(hardDifficulty);
				hardDifficulty.x = header.width/2 - (hardDifficulty.width * 1.2);
				hardDifficulty.y = header.y + header.height/2;
				
				if(!_level.medium)
					mediumDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell"));
				else
					mediumDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell_Med"));
				
				mediumDifficulty.pivotX = mediumDifficulty.width/2;
				mediumDifficulty.pivotY = mediumDifficulty.height/2;
				addChild(mediumDifficulty);
				mediumDifficulty.x = hardDifficulty.x - (hardDifficulty.width * 1.5);
				mediumDifficulty.y = hardDifficulty.y;
				
				if(!_level.easy)
					easyDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell"));
				else
					easyDifficulty = new Image(Main.assets.getTexture("Pathogen_DifficultyCell_Easy"));
				
				easyDifficulty.pivotX = easyDifficulty.width/2;
				easyDifficulty.pivotY = easyDifficulty.height/2;
				addChild(easyDifficulty);
				easyDifficulty.x = mediumDifficulty.x - (mediumDifficulty.width * 1.5);
				easyDifficulty.y = hardDifficulty.y;
			}
			
			if(level.chosenSubLevel)
				selectSubLevel(level.chosenSubLevel);
		}
		public function selectSubLevel(sublevel:SubLevel):void
		{
			if(sublevel.levelNumber > 0 && Main._lite && Main._locked)
				_campaign.main.openContentLockedScreen();
			else
			{
				reset();
				
				if(_subLevelSelector == null)
				{
					var image:Image = new Image(Main.assets.getTexture("Cell_Small"));
					_subLevelSelector = new BW_Selector(PathogenTest.wTenPercent, PathogenTest.hTenPercent, image);
					_subLevelHolder.addChildAt(_subLevelSelector, 0);
					_subLevelSelector.scaleX = 1.1;
					_subLevelSelector.scaleY = 1.1;
				}
				
				var globalPos:Point = sublevel.localToGlobal(new Point(0,0));
				
				_subLevelSelector.x = sublevel.x;
				_subLevelSelector.y = sublevel.y;
				
				sublevel.scaleX = 1;
				sublevel.scaleY = 1;
			}
		}
		private function reset():void
		{
			for each (var sublevel:SubLevel in _sublevelArray)
			{
				sublevel.scaleX = .75;
				sublevel.scaleY = .75;
			}
		}
		private function onTouchQuit($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if(touch)
			{
				_backButton.removeEventListener(TouchEvent.TOUCH, onTouchQuit);
				_startButton.removeEventListener(TouchEvent.TOUCH, onTouchStart);
				startFadeOut();
			}
		}
		private function onTouchStart($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			
			if(touch)
			{
				_backButton.removeEventListener(TouchEvent.TOUCH, onTouchQuit);
				_startButton.removeEventListener(TouchEvent.TOUCH, onTouchStart);
				
				_startGame = true;
				startFadeOut();
			}
		}
		override public function death():void
		{
			_backButton.destroy();
			_backButton.dispose();
			removeChild(_backButton);
			_backButton = null;
			
			_startButton.destroy();
			_startButton.dispose();
			removeChild(_startButton);
			_startButton = null;
			
			for (var i:int = 0; i < _sublevelArray.length; i++)
			{
				var sublevel:SubLevel = _sublevelArray[i];
				sublevel.removeDisplay();
				_subLevelHolder.removeChild(sublevel);
			}
			
			if(_subLevelHolder)
			{
				_subLevelHolder.dispose();
				removeChild(_subLevelHolder);
				_subLevelHolder = null;
			}
			
			if(_subLevelSelector)
			{
				_subLevelSelector.destroy();
				_subLevelSelector.dispose();
				removeChild(_subLevelSelector);
				_subLevelSelector = null;
			}
			
			if(_startGame)
				_campaign.beginLevel(_level);
			
			_campaign.removeLevelScreen();
			
			_level = null;
			_campaign = null;
		}
	}
}