package Campaign
{
	import starling.display.Image;
	import starling.display.Sprite;
	import UI.Main;
	
	public class DottedLine extends Sprite
	{
		private var dottedLine:Image;
		
		public function DottedLine(howLong:String)
		{
			super();
			
			if(howLong == "Short")
			{
				dottedLine =  new Image(Main.assets.getTexture("Pathogen_DottedLine_Longt"));
			}
			if(howLong == "Long")
			{
				dottedLine =  new Image(Main.assets.getTexture("Pathogen_DottedLine_Short"));
			}
			
			addChild(dottedLine);
			dottedLine.visible = false;
		}
		public function setLine():void
		{
			dottedLine.visible = true;
		}
	}
}