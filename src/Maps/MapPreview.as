package Maps
{
	import OnlinePlay.OnlineGameSetup;
	
	import Setup.GameSetup;
	
	import UI.Main;
	
	import Utils.BW_UI;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class MapPreview extends Sprite
	{
		private var gameSetup:GameSetup;
		private var _onlineGameSetup:OnlineGameSetup;
		
		public var mapData:Array = [];
		public var mapName:String;
		public var mapSize:int;
		public var mode:String;
		public var originalData:String;
		
		private var previewCellHolder:Array = [];
		
		private var cellHolder:Sprite;
		
		private var background:Scale9Image;
		private var header:Scale9Image
		
		private var _map:Map;
		
		public function MapPreview(mapName:String, mapData:String, mapSize:int, gameSetup:GameSetup, onlineGameSetup:OnlineGameSetup = null, map:Map = null)
		{
			super();
			
			this.mapName = mapName;
			this.mapSize = mapSize;
			this.gameSetup = gameSetup;
			originalData = mapData;
			
			if(onlineGameSetup != null)
				_onlineGameSetup = onlineGameSetup;
			
			_map = map;
			
			var scale9Num:Number = 20/PathogenTest.scaleFactor;
			
			var blacktexture:Texture = Main.assets.getTexture("scale9_black");
			var blacktextures:Scale9Textures = new Scale9Textures(blacktexture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			background = new Scale9Image(blacktextures);
			addChild(background);
			background.width = PathogenTest.wTenPercent * 5.5;
			background.height = PathogenTest.hTenPercent * 7;
			background.pivotX = background.width/2;
			background.pivotY = background.height/2;
			background.alpha = .5;
			
			var texture:Texture = Main.assets.getTexture("scale9_White");
			var textures:Scale9Textures = new Scale9Textures(texture, new Rectangle(scale9Num, scale9Num, scale9Num, scale9Num));
			
			header = new Scale9Image(textures);
			addChild(header);
			header.width = background.width;
			header.height = PathogenTest.hTenPercent;
			header.pivotX = header.width/2;
			header.y = -background.height/2;
			header.alpha = .5;
			header.color = 0x0f2048;
			
			this.mapData = mapData.split(",");
			this.mapData.pop();
			
			var fontSize:int = PathogenTest.HD_Multiplyer * 40 / PathogenTest.scaleFactor;
			var textField:TextField = new TextField(background.width, header.height, mapName, "Questrial", fontSize, Color.WHITE);
			textField.pivotX = textField.width/2;
			textField.y = -(background.height/2);
			addChild(textField);
			
			createPreview();
			
			if(gameSetup || _onlineGameSetup)
				addEventListener(TouchEvent.TOUCH, onTouch);
		}
		public function onTouch($e:TouchEvent):void
		{
			var touch:Touch = $e.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(mapName != "Rectangle" && gameSetup && gameSetup.sliding == false && Main._lite && Main._locked)
					gameSetup.main.openContentLockedScreen();
				else
				{
					if(gameSetup && gameSetup.sliding == false)
						gameSetup.selectLevel(this);
					if(_onlineGameSetup && _onlineGameSetup.sliding == false)
						_onlineGameSetup.selectLevel(this);
				}
			}
		}
		public function removeTouchEvents():void
		{
			removeEventListener(TouchEvent.TOUCH, onTouch);
		}
		public function createPreview():void
		{
			var tileCounter:int = 0;
			var gameBoardWidth:int;
			var gameBoardHeight:int;
			var startingPos:Point = new Point(0,0);
			var tileGap:int;
			var tileScaleX:Number;
			var tileScaleY:Number;
			
			//Determine Board Length/Height
			if (mapSize == 0)
			{
				gameBoardHeight = 14;
				gameBoardWidth = 16;
				tileScaleX = 1.5;
				tileScaleY = 1.5;
			}
			if (mapSize == 1)
			{
				gameBoardHeight = 18;
				gameBoardWidth = 20;
				tileScaleX = 1;
				tileScaleY = 1;
			}
			if (mapSize == 2)
			{
				gameBoardHeight = 22;
				gameBoardWidth = 24;
				tileScaleX = .9;
				tileScaleY = .9;
			}
			
			cellHolder = new Sprite();
			addChild(cellHolder);
			
			//Create a blank board
			for (var i:int = 0; i < gameBoardWidth; i++)
			{
				for (var j:int = 0; j < gameBoardHeight; j++)
				{
					var previewCell:MapPreviewCell = new MapPreviewCell();
					cellHolder.addChild(previewCell);
					previewCellHolder.push(previewCell);
					
					previewCell.changeColor(mapData[tileCounter])
					
					tileGap = (previewCell.width * .9) * tileScaleX;
					
					previewCell.x = startingPos.x + (tileGap * i);
					previewCell.y = startingPos.y + (tileGap * j);
					
					previewCell.scaleX = tileScaleX;
					previewCell.scaleY = tileScaleY;
					
					tileCounter++;
				}
			}
			
			cellHolder.pivotX = cellHolder.width/2;
			cellHolder.pivotY = cellHolder.height/2;
			
			cellHolder.x =  -(PathogenTest.wTenPercent * .15);
			cellHolder.y = PathogenTest.hTenPercent * .2;
			
			flatten();
		}
		public function garbageCollection():void
		{
			unflatten();
			
			if(background)
			{
				background.dispose();
				removeChild(background);
				background = null;
			}
			if(header)
			{
				header.dispose();
				removeChild(header)
				header = null;
			}
			
			for each(var previewCell:MapPreviewCell in previewCellHolder)
			{
				previewCell.dispose();
				previewCell = null;
			}
			
			previewCellHolder = [];
			
			if(cellHolder)
			{
				cellHolder.removeChildren(0,-1);
				cellHolder.dispose();
				removeChild(cellHolder);
				cellHolder = null;
			}
			
			gameSetup = null;
		}
		
		public function get map():Map {return _map};
	}
}