package Maps
{
	public class Map
	{
		private var _id:int;
		private var _size:int;
		private var _name:String;
		private var _userCreated:Boolean;
		private var _layout:Array;
		
		public function Map(id:int, size:int, name:String, userCreated:Boolean, layoutString:String)
		{
			_id = id;
			_size = size;
			_name = name;
			_userCreated = userCreated;
			_layout = layoutString.split(",");
		}
		
		public function get id():int { return _id; }
		public function get size():int { return _size; }
		public function get name():String { return _name; }
		public function get userCreated():Boolean { return _userCreated; }
		public function get layout():Array { return _layout; }
		
		public function set id(id:int):void { _id = id; }
		public function set size(size:int):void { _size = size; }
		public function set name(name:String):void { _name = name; }
		public function set userCreated(userCreated:Boolean):void { _userCreated = userCreated; }
		public function set layout(layout:Array):void { _layout = layout; }
	}
}