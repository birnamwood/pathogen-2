package Maps
{
	public class MapManager
	{
		public static const SMALL:int = 0;
		public static const MEDIUM:int = 1;
		public static const LARGE:int = 2;
		public static const START_SMALL_INDEX:int = 0;
		public static const END_SMALL_INDEX:int = 5;
		public static const START_MED_INDEX:int = 6;
		public static const END_MED_INDEX:int = 11;
		public static const START_LARGE_INDEX:int = 12;
		public static const END_LARGE_INDEX:int = 17;
		
		private const MAPS_PER_SIZE:int = 6;
		
		private var _maps:Vector.<Map>;
		
		public function MapManager()
		{
			//trace("generating maps");
			
			_maps = new Vector.<Map>();
			
			var mapCounter:int = 0;
			var i:int;
			var map:Map;
			for (i=0; i<MAPS_PER_SIZE; i++)
			{
				map = new Map(mapCounter, SMALL, MapInfo.smallMapNameArray[i], false, MapInfo.smallMapDataArray[i]);
				_maps.push(map);
				mapCounter++;
			}
			for (i=0; i<MAPS_PER_SIZE; i++)
			{
				map = new Map(mapCounter, MEDIUM, MapInfo.mediumMapNameArray[i], false, MapInfo.mediumDataArray[i].split(","));
				_maps.push(map);
				mapCounter++;
			}
			for (i=0; i<MAPS_PER_SIZE; i++)
			{
				map = new Map(mapCounter, LARGE, MapInfo.largeMapNameArray[i], false, MapInfo.largeDataArray[i].split(","));
				_maps.push(map);
				mapCounter++;
			}
			
			//trace("there are " + _maps.length + " maps");
		}
		
		public function getMap(mapIndex:int):Map { return _maps[mapIndex]; }
		
		public function get maps():Vector.<Map> { return _maps; }
	}
}