package Maps
{
	import starling.display.Image;
	import starling.display.Sprite;
	import UI.Main;
	
	public class MapPreviewCell extends Sprite
	{
		private var state:int = 0;
		
		public function MapPreviewCell()
		{
			super();
		}
		public function changeColor(id:int):void
		{
			state = id;
			
			var display:Image;
			
			switch(state)
			{
				case 0:
					//Grid Space
					display = new Image(Main.assets.getTexture("Grid_Small"));
					break;
				case 1:
					//Grey Dot
					display = new Image(Main.assets.getTexture("Grey_Stage01_Small"));
					break;
				case 2:
					//Grey Circle
					display = new Image(Main.assets.getTexture("Grey_Stage02_Small"));
					break;
				case 3:
					//Grey Full
					display = new Image(Main.assets.getTexture("Grey_Stage04_Small"));
					break;
				case 4:
					visible = false;
					break;
				case 5:
					//Green Dot
					display = new Image(Main.assets.getTexture("Green_Stage01_Small"));
					break;
				case 6:
					//Green Circle
					display = new Image(Main.assets.getTexture("Green_Stage02_Small"));
					break;
				case 7:
					//Green Square
					display = new Image(Main.assets.getTexture("Green_Stage03_Small"));
					break;
				case 8:
					//Green Full
					display = new Image(Main.assets.getTexture("Green_Stage04_Small"));
					break;
				case 9:
					//Red Dot
					display = new Image(Main.assets.getTexture("Red_Stage01_Small"));
					break;
				case 10:
					//Red Circle
					display = new Image(Main.assets.getTexture("Red_Stage02_Small"));
					break;
				case 11:
					//Red Square
					display = new Image(Main.assets.getTexture("Red_Stage03_Small"));
					break;
				case 12:
					//Red Full
					display = new Image(Main.assets.getTexture("Red_Stage04_Small"));
					break;
				case 13:
					//Blue Dot
					display = new Image(Main.assets.getTexture("Blue_Stage01_Small"));
					break;
				case 14:
					//Blue Circle
					display = new Image(Main.assets.getTexture("Blue_Stage02_Small"));
					break;
				case 15:
					//Blue Square
					display = new Image(Main.assets.getTexture("Blue_Stage03_Small"));
					break;
				case 16:
					//Blue Full
					display = new Image(Main.assets.getTexture("Blue_Stage04_Small"));
					break;
				case 17:
					//Yellow Dot
					display = new Image(Main.assets.getTexture("Yellow_Stage01_Small"));
					break;
				case 18:
					//Yellow Circle
					display = new Image(Main.assets.getTexture("Yellow_Stage02_Small"));
					break;
				case 19:
					//Yellow Square
					display = new Image(Main.assets.getTexture("Yellow_Stage03_Small"));
					break;
				case 20:
					//Yellow Full
					display = new Image(Main.assets.getTexture("Yellow_Stage04_Small"));
					break;
				case 21:
					//Capture Zone
					display = new Image(Main.assets.getTexture("Capture_Zone_Small"));
					break;
				case 22:
					//Erosion Zone
					display = new Image(Main.assets.getTexture("Erosion_Zone_Small"));
					break;
				default:
					break;				
			}
			if(display != null)
				addChild(display);
				
		}
	}
}