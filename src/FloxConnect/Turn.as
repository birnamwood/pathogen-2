package FloxConnect
{
	import com.gamua.flox.Entity;
	
	public class Turn extends Entity
	{
		private var _gameID:String;
		private var _playerIndex:int;
		private var _cellType:int;
		private var _location:int;
		private var _surrender:Boolean;
		
		public function Turn()
		{
		}
		
		public function get gameID():String { return _gameID; }
		public function get playerIndex():int { return _playerIndex; }
		public function get cellType():int { return _cellType; }
		public function get location():int { return _location; }
		public function get surrender():Boolean { return _surrender; }
		
		public function set gameID(gameID:String):void { _gameID = gameID; }
		public function set playerIndex(playerIndex:int):void { _playerIndex = playerIndex; }
		public function set cellType(cellType:int):void { _cellType = cellType; }
		public function set location(location:int):void { _location = location; }
		public function set surrender(surrender:Boolean):void { _surrender = surrender; }
	}
}