package FloxConnect
{
	import com.gamua.flox.Entity;
	
	public class ErrorEntity extends Entity
	{
		private var _userID:String;
		private var _deviceType:String;
		private var _functionName:String;
		private var _error:String;
		
		public function ErrorEntity()
		{
			super();
		}
		
		public function get userID():String { return _userID; }
		public function get deviceType():String { return _deviceType; }
		public function get functionName():String { return _functionName; }
		public function get error():String { return _error; }
		
		public function set userID(userID:String):void { _userID = userID; }
		public function set deviceType(deviceType:String):void { _deviceType = deviceType; }
		public function set functionName(functionName:String):void { _functionName = functionName; }
		public function set error(error:String):void { _error = error; }
	}
}