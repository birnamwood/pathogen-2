package FloxConnect
{
	import com.gamua.flox.Entity;
	
	public class GameEntity extends Entity
	{
		private var _gameType:int;  // 0 = 2 player, 1 = 3 player, 2 = 4 player
		private var _mapID:int;
		private var _version:String;
		private var _numOfPlayers:int;
		private var _status:int;
		private var _league:String;
		private var _ranked:Boolean;
		private var _creationDate:Date;
		private var _updateDate:Date;
		private var _activePlayerIndex:int;
		private var _mode:int;
		private var _currentTurnNumber:int;
		private var _players:Array;
		private var _turnPlayers:Array;
		private var _turnLocations:Array;
		private var _turnCellTypes:Array;
		private var _turnSurrenders:Array;
		private var _seenEndGame:Array;
		private var _ranksCalculated:Boolean;
		private var _surrenders:Array;
		private var _colors:Array;
		private var _dateSortVal:Number;
		private var _playerScores:Array;
		private var _random:Boolean;
		private var _playerNicknames:Array;
		private var _playerLeagues:Array;
		private var _playerRanks:Array;
		
		public function GameEntity()
		{
			super();
			
			_players = [];
			_turnPlayers = [];
			_turnCellTypes = [];
			_turnLocations = [];
			_turnSurrenders = [];
			_seenEndGame = [];
			_surrenders = [];
			_colors = [];
			_playerScores = [];
			_playerLeagues = [];
			_playerRanks = [];_playerLeagues
		}
		
		public function takeTurn(cellType:int, location:int):void
		{
			_turnPlayers.push(_activePlayerIndex);
			_turnCellTypes.push(cellType);
			_turnLocations.push(location);
			_turnSurrenders.push(false);
			
			_currentTurnNumber++;
			
			// trace("set current turn number to " + _currentTurnNumber);
			
			_activePlayerIndex++;
			if (_activePlayerIndex > (_numOfPlayers - 1))
			{
				_activePlayerIndex = 0;
				
				if (_status == EntityManager.GAME_STATUS_WAITING)
					_status = EntityManager.GAME_STATUS_BEGUN;
			}
			
			//trace("taking turn in GameEntity. now activeplayer = " + _activePlayerIndex);
		}
		
		public function surrender(playerIndex:int):void
		{
			/*
			_turnPlayers.push(_activePlayerIndex);
			_turnCellTypes.push(-1);
			_turnLocations.push(-1);
			_turnSurrenders.push(true);
			_currentTurnNumber++;
			_activePlayerIndex++;
			*/
			
			_surrenders[playerIndex] = true;
			_status = EntityManager.GAME_STATUS_COMPLETE;
		}
		
		public function getAverageScore():int
		{
			if (_playerScores.length > 0)
			{
				var scoreTotal:int = 0;
				for (var i:int=0; i<_playerScores.length; i++)
					scoreTotal += _playerScores[i];
				return scoreTotal / _playerScores.length;
			}
			return EntityManager.NOT_FOUND;
		}
		
		public function get statusAndRankedAndRandom():String
		{
			return _status + "|" + _ranked + "|"  + _random;
		}
		
		public function get statusAndRanked():String
		{
			return _status + "|" + _ranked;
		}
		
		public function get gameType():int { return _gameType; }
		public function get mapID():int { return _mapID; }
		public function get version():String { return _version; }
		public function get numOfPlayers():int { return _numOfPlayers; }
		public function get status():int { return _status; }
		public function get league():String { return _league; }
		public function get ranked():Boolean { return _ranked; }
		public function get creationDate():Date { return _creationDate; }
		public function get updateDate():Date { return _updateDate; }
		public function get activePlayerIndex():int { return _activePlayerIndex; }
		public function get mode():int { return _mode; }
		public function get currentTurnNumber():int { return _currentTurnNumber; }
		public function get players():Array { return _players; }
		public function get turnPlayers():Array { return _turnPlayers; }
		public function get turnLocations():Array { return _turnLocations; }
		public function get turnCellTypes():Array { return _turnCellTypes; }
		public function get turnSurrenders():Array { return _turnSurrenders; }
		public function get seenEndGame():Array { return _seenEndGame; }
		public function get ranksCalculated():Boolean { return _ranksCalculated; }
		public function get surrenders():Array { return _surrenders; }
		public function get colors():Array { return _colors; }
		public function get dateSortVal():Number { return _dateSortVal; }
		public function get playerScores():Array { return _playerScores; }
		public function get random():Boolean { return _random; }
		public function get playerNicknames():Array { return _playerNicknames; }
		public function get playerLeagues():Array { return _playerLeagues; }
		public function get playerRanks():Array { return _playerRanks; }
		
		public function set gameType(gameType:int):void { _gameType = gameType; }
		public function set mapID(mapID:int):void { _mapID = mapID; }
		public function set version(version:String):void { _version = version; }
		public function set numOfPlayers(numOfPlayers:int):void { _numOfPlayers = numOfPlayers; }
		public function set status(status:int):void { _status = status; }
		public function set league(league:String):void { _league = league; }
		public function set ranked(ranked:Boolean):void { _ranked = ranked; }
		public function set creationDate(creationDate:Date):void { _creationDate = creationDate; }
		public function set updateDate(updateDate:Date):void { _updateDate = updateDate; }
		public function set activePlayerIndex(activePlayerIndex:int):void { _activePlayerIndex = activePlayerIndex; }
		public function set mode(mode:int):void { _mode = mode; }
		public function set currentTurnNumber(currentTurnNumber:int):void { _currentTurnNumber = currentTurnNumber; }
		public function set players(players:Array):void { _players = players; }
		public function set turnPlayers(turnPlayers:Array):void { _turnPlayers = turnPlayers; }
		public function set turnLocations(turnLocations:Array):void { _turnLocations = turnLocations; }
		public function set turnCellTypes(turnCellTypes:Array):void { _turnCellTypes = turnCellTypes; }
		public function set turnSurrenders(turnSurrenders:Array):void { _turnSurrenders = turnSurrenders; }
		public function set seenEndGame(seenEndGame:Array):void { _seenEndGame = seenEndGame; }
		public function set ranksCalculated(ranksCalculated:Boolean):void { _ranksCalculated = ranksCalculated; }
		public function set surrenders(surrenders:Array):void { _surrenders = surrenders; }
		public function set colors(colors:Array):void { _colors = colors; }
		public function set dateSortVal(dateSortVal:Number):void { _dateSortVal = dateSortVal; }
		public function set playerScores(playerScores:Array):void { _playerScores = playerScores; }
		public function set random(random:Boolean):void { _random = random; }
		public function set playerNicknames(playerNicknames:Array):void { _playerNicknames = playerNicknames; }
		public function set playerLeagues(playerLeagues:Array):void { _playerLeagues = playerLeagues; }
		public function set playerRanks(playerRanks:Array):void { _playerRanks = playerRanks; }
	}
}