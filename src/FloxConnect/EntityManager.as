package FloxConnect
{
	import GameRecords.PlayerRecord;
	
	import Maps.Map;
	
	import OnlinePlay.LeagueRank;
	import OnlinePlay.OnlinePlayerRankData;
	import OnlinePlay.PushNotification;
	
	import UI.Achievements;
	import UI.Main;
	
	import Utils.GeneralFunctions;
	
	import com.gamua.flox.Access;
	import com.gamua.flox.Entity;
	import com.gamua.flox.Player;
	import com.gamua.flox.Query;
	import com.gamua.flox.utils.HttpStatus;
	import com.gamua.flox.utils.cloneObject;
	import com.sticksports.nativeExtensions.gameCenter.GCPlayer;
	import com.sticksports.nativeExtensions.gameCenter.GameCenter;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class EntityManager extends Sprite
	{
		// invite statuses
		public static var INVITE_STATUS_CREATED:int = 0;
		public static var INVITE_STATUS_ACCEPTED:int = 1;
		public static var INVITE_STATUS_REJECTED:int = 2;
		
		// game statuses
		public static var GAME_STATUS_WAITING:int = 0;
		public static var GAME_STATUS_BEGUN:int = 1;
		public static var GAME_STATUS_COMPLETE:int = 2;
		public static var GAME_STATUS_REJECTED:int = 3;
		
		// game types
		public static var GAME_TYPE_2_PLAYER:int = 0;
		public static var GAME_TYPE_3_PLAYER:int = 1;
		public static var GAME_TYPE_4_PLAYER:int = 2;
		
		// leagues
		public static var LEAGUE_SILVER_TIER:int = 1500;
		public static var LEAGUE_GOLD_TIER:int = 2000;
		public static var GAMES_TO_BE_RANKED:int = 3;
		public static var NUMBER_OF_LEAGUES:int = 3;
		public static var LEAGUE_BRONZE_2_PLAYER:String = "bronze_2";
		public static var LEAGUE_SILVER_2_PLAYER:String = "silver_2";
		public static var LEAGUE_GOLD_2_PLAYER:String = "gold_2";
		public static var RANKED_GAME_PLACEMENT_SCORE_DIFFERENTIAL:int = 200;
		
		// divisions
		public static var DIVISION_MAX:int = 50;
		
		// game modes
		public static var GAME_MODE_NORMAL:int = 0;
		
		// number of game slots
		public static var INITIAL_MAX_RANKED_GAMES:int = 1;
		public static var INITIAL_MAX_CUSTOM_GAMES:int = 3;
		
		// scoring
		public static var SCORING_RATING_DISPARITY:int = 400;
		public static var SCORING_WIN:Number = 1.0;
		public static var SCORING_LOSS:Number = 0.0;
		public static var SCORING_TIE:Number = 0.5;
		
		// nemesis/dominatee
		public static var NEMESIS_COUNT:int = 3;
		public static var DOMINATEE_COUNT:int = 3;
		
		// general query error code
		public static var NOT_FOUND:int = -1;
		
		private var _main:Main;
		private var _players:Vector.<PlayerRecord>;
		private var _currentGameIndex:int;
		private var _loadingFromSaveData:Boolean;
		private var _turnCellLocation:int;
		private var _turnCellType:int;
		private var _currentPlayer:Player;
		private var _currentPlayerEntity:PlayerEntity;
		private var _playerRankDataArray:Array;
		private var _calculatingRanks:Boolean;
		private var _seekingRankedGame:Boolean;
		private var _foundRankedGameToJoin:Boolean;
		private var _seekingRandomGame:Boolean;
		private var _foundRandomGameToJoin:Boolean;
		private var _numPlayersDataTabulated:int;
		private var _takingTurn:Boolean;
		private var _leaguesToRank:Vector.<LeagueRank>;
		private var _currentGameTypeSeeking:int;
		
		private var _customGamesLoaded:int;
		private var _rankedGamesLoaded:int;
		
		// Flox data
		private var _games:Vector.<GameEntity>;  // custom games
		private var _numberOfGamesToLoad:int;
		private var _numberOfGamesLoaded:int;
		private var _numberOfIncompleteGameLoaded:int;
		private var _gamePlayers:Vector.<PlayerEntity>;  // player entities in current game - used at end game
		private var _gamePlayerDivisions:Array;
		private var _gameToJoin:GameEntity;
		
		// invites
		private var _invites:Vector.<InviteEntity>;
		private var _invitedGames:Vector.<GameEntity>;
		private var _inviteErrorCount:int;
		private var _inviteGameID:String;
		
		// surrenders
		private var _currentSurrenders:Array;
		
		private var _pushMessage:String;
		private var _gameOver:Boolean;
		
		// ladder
		private var _currentLadder:Array;
		
		// league and rank
		private var _currentLeague2Player:String;
		private var _currentRank2Player:int;
		
		private var _playerLeagues:Array;
		
		private var _debug:Boolean = false;
		
		public function EntityManager(main:Main)
		{
			super();
			
			_main = main;
			_loadingFromSaveData = false;
			_playerRankDataArray = [];
			_calculatingRanks = false;
			_seekingRankedGame = false;
			_takingTurn = false;
			_leaguesToRank = new Vector.<LeagueRank>;
			_playerLeagues = [];
			
			_players = new Vector.<PlayerRecord>();
			_games = new Vector.<GameEntity>();
			_invites = new Vector.<InviteEntity>();
			_invitedGames = new Vector.<GameEntity>();
			_gamePlayers = new Vector.<PlayerEntity>();
			
			_currentLadder = [];
			
			// check for current league and rank
			if(Main.saveDataObject.data.league2Player != null)
				_currentLeague2Player = Main.saveDataObject.data.league2Player;
			else
				_currentLeague2Player = "";
			if(Main.saveDataObject.data.rank2Player != null)
				_currentRank2Player = Main.saveDataObject.data.rank2Player;
			else
				_currentRank2Player = NOT_FOUND;
			
			var player:PlayerRecord;
			if(PathogenTest.device == PathogenTest.PC)
			{
				player = new PlayerRecord("1234567890", "player1");
				player.hasPathogen = true;
				_players.push(player);
				
				player = new PlayerRecord("player2", "player2");
				player.hasPathogen = true;
				_players.push(player);
				
				player = new PlayerRecord("1781482583", "BWGTester");
				player.hasPathogen = true;
				_players.push(player);
				
				player = new PlayerRecord("test", "marguerite2014");
				player.hasPathogen = true;
				_players.push(player);
			}
			else
			{
				// build friends list from Game Center friends
				for (var i:int=0; i<Main.sGCFriends.length; i++)
				{
					player = new PlayerRecord(Main.sGCFriends[i].id.substr(2), Main.sGCFriends[i].alias);
					_players.push(player);
				}
				checkIfPlayersHavePathogen();
			}
			
			_currentGameTypeSeeking = GAME_TYPE_2_PLAYER;
		}
		
		public function createInitialEntities():void
		{
			/*
			var invite:InviteEntity = new InviteEntity();
			invite.saveQueued();
			
			var game:GameEntity = new GameEntity();
			game.saveQueued();
			
			var gamePlayer:GamePlayerEntity = new GamePlayerEntity();
			gamePlayer.saveQueued();
			
			var league:LeagueEntity = new LeagueEntity();
			league.saveQueued();
			
			var joinRandom:JoinRandomGameEntity = new JoinRandomGameEntity();
			joinRandom.saveQueued();
			*/
			
			/*
			var server:ServerEntity = new ServerEntity();
			server.id = "2";
			server.serverAddress = "http://72.15.31.115/test/pathogen_APNS.php";
			server.publicAccess = Access.READ;
			server.saveQueued();
			*/
		}
		
		public function testQuery():void
		{
			var league:String = LEAGUE_BRONZE_2_PLAYER;
			var div:int = 0;
			var query:Query = new Query(PlayerEntity, "twoPlayer == ?", league + "|" + div);
			query.find(testQueryComplete, null);
		}
		
		private function testQueryComplete(players:Array):void
		{
			trace("found " + players.length + " players");
		}
		
		public function setCurrentPlayer():void
		{
			//Main.updateDebugText("Setting Current Player");
			
			_currentPlayer = Player.current;
			_currentPlayer.saveQueued();
			
			if (!_currentPlayerEntity)
				Entity.load(PlayerEntity, Main.sOnlineID, onCheckPlayerExistsComplete, onCheckPlayerExistsError);
		}
		
		private function onCheckPlayerExistsComplete(player:PlayerEntity):void
		{
			if (player == null)
			{
				if (_debug)
					trace("player == null in onCheckPlayerExistsComplete");
				
				var playerEntity:PlayerEntity = new PlayerEntity();
				playerEntity.id = Main.sOnlineID;
				playerEntity.nickname = Main.sOnlineNickname;
				playerEntity.publicAccess = Access.READ_WRITE;
				playerEntity.saveQueued();
				_currentPlayerEntity = playerEntity;
				//generateCurrentLadder();
				
				//Main.updateDebugText("created new player entity");
			}
			else
			{
				_currentPlayerEntity = player;
				
				if (player.nickname != Main.sOnlineNickname)
				{
					player.nickname = Main.sOnlineNickname;
					player.saveQueued();
				}
				//generateCurrentLadder();
			}
		}
		
		private function onCheckPlayerExistsError(error:String):void
		{
			//Main.updateDebugText("error in onCheckPlayerExistsError");
			
			// error on finding player entity - create it
			var playerEntity:PlayerEntity = new PlayerEntity();
			playerEntity.id = Main.sOnlineID;
			playerEntity.nickname = Main.sOnlineNickname;
			playerEntity.publicAccess = Access.READ_WRITE;
			playerEntity.saveQueued();
			_currentPlayerEntity = playerEntity;
			
			//Main.updateDebugText("created player entity with id " + _currentPlayerEntity.id);
		}
		
		public function addGame(numOfPlayers:int, invitedPlayers:Array, map:Map, ranked:Boolean, random:Boolean):void
		{
			var i:int;
			var gameType:int = NOT_FOUND;
			if (numOfPlayers == 2)
				gameType = GAME_TYPE_2_PLAYER;
			else if (numOfPlayers == 3)
				gameType = GAME_TYPE_3_PLAYER;
			else if (numOfPlayers == 4)
				gameType = GAME_TYPE_4_PLAYER;
			
			// create online entity
			var floxGame:GameEntity = new GameEntity();
			floxGame.mapID = map.id;
			floxGame.version = Main.sVersion;
			floxGame.numOfPlayers = numOfPlayers;
			floxGame.status = GAME_STATUS_WAITING;
			floxGame.ranked = ranked;
			if (floxGame.ranked)
			{
				floxGame.league = _currentPlayerEntity.league2Player;
				floxGame.playerLeagues.push(_currentPlayerEntity.league2Player);
				floxGame.playerRanks.push(_currentPlayerEntity.ranks[GAME_TYPE_2_PLAYER]);
			}
			floxGame.creationDate = new Date();
			floxGame.updateDate = new Date();
			floxGame.activePlayerIndex = 0;
			floxGame.mode = GAME_MODE_NORMAL;
			floxGame.currentTurnNumber = 0;
			floxGame.players = [Main.sOnlineID];
			floxGame.seenEndGame = [false];
			floxGame.ranksCalculated = false;
			floxGame.surrenders = [false];
			floxGame.colors.push(0);
			floxGame.playerScores.push(_currentPlayerEntity.scores[gameType]);
			floxGame.random = random;
			floxGame.playerNicknames = [Main.sOnlineNickname];
			floxGame.publicAccess = Access.READ_WRITE;
			floxGame.saveQueued();
			_games.push(floxGame);
			_currentGameIndex = _games.length - 1;
			
			// create online game player record
			var floxPlayer:GamePlayerEntity = new GamePlayerEntity();
			floxPlayer.gameID = floxGame.id;
			floxPlayer.playerID = Main.sOnlineID;
			floxPlayer.publicAccess = Access.READ_WRITE;
			floxPlayer.saveQueued();
			
			// send invites if not ranked
			if (!ranked && !random)
			{
				// send invite to other players
				var floxInvite:InviteEntity;
				for (i=0; i<invitedPlayers.length; i++)
				{
					floxInvite = new InviteEntity();
					floxInvite.gameID = floxGame.id;
					floxInvite.invitingID = Main.sOnlineNickname;
					floxInvite.invitedID = invitedPlayers[i];
					floxInvite.status = INVITE_STATUS_CREATED;
					floxInvite.dateTime = new Date();
					floxInvite.publicAccess = Access.READ_WRITE;
					floxInvite.saveQueued();
					//trace("invited " + floxInvite.invitedID);
					
					// push notification
					var inviteMessage:String = "You're invited to a game with " + Main.sOnlineNickname;
					sendPushNotificationWithPlayerLoad(invitedPlayers[i], inviteMessage);
				}
			}
		}
		
		public function gatherInvites():void
		{
			//trace("gathering invites");
			
			//Main.debugText.text = "gathering invites";
			
			// clear data
			_invites = new Vector.<InviteEntity>();
			_invitedGames = new Vector.<GameEntity>();
			_inviteErrorCount = 0;
			
			// if there is room in the list of games for invites
			if (getTotalNumberIncompleteGames() < _currentPlayerEntity.customGames)
			{
				//Main.debugText.text = "Querying for invited games";
				
				//var query:Query = new Query(InviteEntity, "invitedID == ? AND status == ?", Main.sOnlineID, INVITE_STATUS_CREATED);
				//query.find(onInviteQueryComplete, onInviteQueryError);
				
				// new query for new index structure
				var query:Query = new Query(InviteEntity, "invitedIDAndStatus == ?", Main.sOnlineID + "|" + INVITE_STATUS_CREATED);
				query.find(onInviteQueryComplete, onInviteQueryError);
			}
			else
				_main.onlinePlay.invitesGathered = true;
			
			//Main.debugText.text = "COMPLETED gathering Invites";
		}
		
		private function onInviteQueryComplete(invites:Array):void
		{
			// sort invites by date
			invites.sort(inviteDateArraySortFunction);
			
			//Main.updateDebugText("Invite Query Complete");
			
			var maxNumberOfInvitesToAccept:int = _currentPlayerEntity.customGames - getTotalNumberIncompleteGames();
			var i:int;
			
			if (_debug)
			{
				for (i=0; i<invites.length; i++)
				{
					trace("invite " + i + " date = " + invites[i].dateTime);
				}
			}
			
			//trace("max invites = " + maxNumberOfInvitesToAccept + " there are " + _games.length + " games in list");
			//trace("query complete - adding " + invites.length + " invites");
			//Main.updateDebugText("max invites = " + maxNumberOfInvitesToAccept);
			
			for (i=0; i<invites.length; i++)
			{
				// if we have reached the max slots
				if (i == maxNumberOfInvitesToAccept)
					break;
				
				// otherwise add the invite
				//trace("found invite " + invites[i].id + " status = " + invites[i].status);
				_invites.push(invites[i]);
				//trace("added invite with id " + invites[i].id + " to invite list");
			}
			
			// load the invited game data
			if (_invites.length > 0)
			{
				for (i=0; i<_invites.length; i++)
				{
					//Main.updateDebugText("loading game id = " + _invites[i].gameID);
					Entity.load(GameEntity, _invites[i].gameID, onInviteGameLoadComplete, onInviteGameLoadError);
				}
			}
			else
			{
				_main.onlinePlay.invitesGathered = true;
				//Main.updateDebugText("Invites have been gathered");
			}
			
			//Main.debugText.text = "Invites have been gathered";
		}
		
		private function onInviteQueryError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onInviteQueryError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
			
			_main.onlinePlay.invitesFailed = true;
		}
		
		private function onInviteGameLoadComplete(game:GameEntity):void
		{
			//Main.updateDebugText("onInviteGameLoadComplete");
			
			_invitedGames.push(game);
			//Main.updateDebugText("found game " + game.id);
			//Main.updateDebugText(_invitedGames.length + " / " + _invites.length);
			
			// check if # of invites match # of games
			if ((_invites.length + _inviteErrorCount) >= _invitedGames.length)
			{
				_main.onlinePlay.invitesGathered = true;
			}
		}
		
		private function onInviteGameLoadError(error:String):void
		{
			//Main.updateDebugText("onInviteGameLoadError");
			
			_inviteErrorCount++;
			
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onInviteGameLoadError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
			
			if ((_invites.length + _inviteErrorCount) >= _invitedGames.length)
				_main.onlinePlay.invitesGathered = true;
		}
		
		public function rejectInvite(gameID:String):void
		{
			_inviteGameID = gameID;
			
			// first check game
			Entity.load(GameEntity, gameID, onLoadGameToCancelInviteComplete, onLoadGameToCancelInviteError);
		}
		
		private function onLoadGameToCancelInviteComplete(game:GameEntity):void
		{
			// mark game as rejected
			game.status = GAME_STATUS_REJECTED;
			game.updateDate = new Date();
			game.saveQueued();
			
			// then reject invite
			var _inviteIndexToDelete:int = NOT_FOUND;
			for (var i:int=0; i<_invitedGames.length; i++)
			{
				if (_invites[i].gameID == game.id)
					_inviteIndexToDelete = i;
			}
			if (_inviteIndexToDelete != NOT_FOUND)
			{
				_invites[_inviteIndexToDelete].status = INVITE_STATUS_REJECTED;
				_invites[_inviteIndexToDelete].saveQueued();
			}
		}
		
		private function onLoadGameToCancelInviteError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onLoadGameToCancelInviteError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		public function gatherGamesFromPlayerID():void
		{
			// trace("gathering games from player id");
			
			//Main.updateDebugText("gathering games from player id");
			
			_games = new Vector.<GameEntity>();
			
			if (_currentPlayer)
			{
				var query:Query = new Query(GamePlayerEntity, "playerID == ?", Main.sOnlineID);
				query.find(onGamePlayerQueryComplete, onGamePlayerQueryError);
			}
			else
				addEventListener(Event.ENTER_FRAME, waitForCurrentPlayerToBeSet);
		}
		
		private function waitForCurrentPlayerToBeSet(event:Event):void
		{
			//Main.updateDebugText("Waiting for current player to be set");
			
			if (_currentPlayer)
			{
				removeEventListener(Event.ENTER_FRAME, waitForCurrentPlayerToBeSet);
				
				var query:Query = new Query(GamePlayerEntity, "playerID == ?", Main.sOnlineID);
				query.find(onGamePlayerQueryComplete, onGamePlayerQueryError);
			}
		}
		
		private function onGamePlayerQueryComplete(gamePlayers:Array):void
		{
			//trace("found " + gamePlayers.length + " games");
			
			if (gamePlayers.length > 0)
			{
				_numberOfGamesToLoad = gamePlayers.length;
				_numberOfGamesLoaded = 0;
				_numberOfIncompleteGameLoaded = 0;
				_customGamesLoaded = 0;
				_rankedGamesLoaded = 0;
				
				//if (_debug)
				//trace("found " + _numberOfGamesToLoad + " games to load");
				
				//Main.updateDebugText("found " + _numberOfGamesToLoad + " games to load");
				
				for (var i:int; i<gamePlayers.length; i++)
				{
					var gamePlayer:GamePlayerEntity = gamePlayers[i];
					
					/*
					if (_debug)
					trace("loading game " + gamePlayer.gameID);
					*/
					
					//Main.updateDebugText("loading game " + gamePlayer.gameID);
					
					Entity.load(GameEntity, gamePlayer.gameID, onLoadGameComplete, onLoadGameError);
				}
			}
			else
			{
				doneGatheringGames();
			}
		}
		
		public function gatherGamesFromSaveGameIDs(gameIDs:Array):void
		{
			if (_debug)
				trace("gathering games from save game ids");
			
			_games = new Vector.<GameEntity>();
			
			if (gameIDs.length > 0)
			{
				_loadingFromSaveData = true;
				_numberOfGamesToLoad = gameIDs.length;
				_numberOfGamesLoaded = 0;
				_numberOfIncompleteGameLoaded = 0;
				
				if (_debug)
					trace("loading " + gameIDs.length + " games");
				
				for (var i:int; i<gameIDs.length; i++)
				{
					Entity.load(GameEntity, gameIDs[i], onLoadGameComplete, onLoadGameError);
				}
			}
			else
			{
				doneGatheringGames();
			}
		}
		
		private function doneGatheringGames():void
		{
			//Main.updateDebugText("doneGatheringGames");
			//Main.debugText.text = "Completed Gathering Games, Sorting";
			
			var gameArray:Array = [];
			var i:int;
			for (i=0; i<_games.length; i++)
				gameArray.push(_games[i]);
			
			gameArray.sortOn(("dateSortVal", "status"), [Array.NUMERIC, Array.NUMERIC]);
			
			_games = new Vector.<GameEntity>();
			for (i=0; i<gameArray.length; i++)
				_games.push(gameArray[i]);
			
			_main.onlinePlay.gamesGathered = true;
			gatherInvites();
		}
		
		private function onGamePlayerQueryError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onGamePlayerQueryError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
			
			//Main.updateDebugText("Error Gathering Games");
			_main.onlinePlay.gamesFailed = true;
			
			if (error == "IO Error #2032")
			{
				//Main.debugText.text = "IOE onGamePlayerQueryError";
				_main.onlinePlay.addInternetConnectivityError();
			}
		}
		
		private function onLoadGameComplete(game:GameEntity):void
		{
			_numberOfGamesLoaded++;
			
			if (!_main.onlinePlay.gamesGathered)
			{
				var loadGame:Boolean = false;
				if (game.status == GAME_STATUS_REJECTED)
				{
					_numberOfIncompleteGameLoaded++;
				}
				else if (game.status != GAME_STATUS_COMPLETE)
				{
					if (!game.ranked)
					{
						_customGamesLoaded++;
						if (_customGamesLoaded <= _currentPlayerEntity.customGames)
							loadGame = true;
					}
					else
					{
						_rankedGamesLoaded++;
						if (_rankedGamesLoaded <= _currentPlayerEntity.rankedGames)
							loadGame = true;
					}
					
					_numberOfIncompleteGameLoaded++;
					/*
					_numberOfIncompleteGameLoaded++;
					if (_numberOfIncompleteGameLoaded <= _currentPlayerEntity.customGames)
						loadGame = true;
					*/
				}
				else
					loadGame = true;
				
				if (loadGame)
				{
					_games.push(game);
					//Main.updateDebugText("now there are " + _games.length + " games in games list. allowing " + _currentPlayerEntity.customGames);
					
					var playersString:String = "";
					for (var i:int=0; i<_games.length; i++)
					{
						playersString = "";
						
						for (var j:int=0; j<_games[i].players.length; j++)
							playersString += _games[i].players[j] + ", ";
						
						//Main.updateDebugText(_games[i].id + " players = " + playersString);
					}
					
					game.dateSortVal = game.updateDate.valueOf();
				}
			}
			
			if (_numberOfGamesLoaded >= _numberOfGamesToLoad)
				doneGatheringGames();
		}
		
		private function onLoadGameError():void
		{
			// check to see if attempt was from save data
			if (_loadingFromSaveData)
			{
				// there was an error loading games from save data. try from Flox data
				_loadingFromSaveData = false;
				_main.onlineGameIDs = [];
				gatherGamesFromPlayerID();
			}
			else
			{
				// otherwise just increment games count so we can move on
				_numberOfGamesLoaded++;
				
				if (_numberOfGamesLoaded >= _numberOfGamesToLoad)
					doneGatheringGames();
			}
		}
		
		public function getCurrentPlayerForCurrentGame():String
		{
			var activePlayerIndex:int = _games[_currentGameIndex].activePlayerIndex;
			return _games[_currentGameIndex].players[activePlayerIndex];
		}
		
		public function getMyPlayerID():String
		{
			return _currentPlayerEntity.id;
		}
		
		public function takeTurnForCurrentGame(cellType:int, location:int, surrender:Boolean, gameOver:Boolean):void
		{
			if (_debug)
				trace("takeTurnForCurrentGame - cell type: " + cellType + " loc: " + location);
			
			_takingTurn = true;
			
			var currentGame:GameEntity = _games[_currentGameIndex];
			
			if (!currentGame.ranked)
				checkAchievements();
			
			_turnCellType = cellType;
			_turnCellLocation = location;
			_gameOver = gameOver;
			currentGame.refresh(onGameRefreshBeforeTurnComplete, onGameRefreshBeforeTurnError);
		}
		
		private function onGameRefreshBeforeTurnComplete(game:GameEntity):void
		{
			_games[_currentGameIndex] = game;
			
			var currentGame:GameEntity = game;
			var lastPlayerNickname:String = _games[_currentGameIndex].playerNicknames[_games[_currentGameIndex].activePlayerIndex];
			currentGame.takeTurn(_turnCellType, _turnCellLocation);
			currentGame.updateDate = new Date();
			
			if (_debug)	
				trace("now current game active player = " + _games[_currentGameIndex].activePlayerIndex);
			
			currentGame.saveQueued();
			
			// push notification
			if (!_gameOver)
			{
				var nextPlayerID:String = _games[_currentGameIndex].players[_games[_currentGameIndex].activePlayerIndex];
				var message:String = lastPlayerNickname + " took a turn. It is now your turn.";
				sendPushNotificationWithPlayerLoad(nextPlayerID, message);
			}
			
			_takingTurn = false;
			
			// trace("set taking turn to " + _takingTurn);
		}
		
		private function onGameRefreshBeforeTurnError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onGameRefreshBeforeTurnError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
			
			_takingTurn = false;
			
			if (error == "IO Error #2032")
			{
				//Main.debugText.text = "IOE onGameRefreshBeforeTurnError";
				_main.onlineGameManager.addInternetConnectivityError();
			}
		}
		
		public function surrenderCurrentGame():void
		{
			var currentGame:GameEntity = _games[_currentGameIndex];
			currentGame.surrender(whatIsMyPlayerIndexForCurrentGame());
			currentGame.updateDate = new Date();
			currentGame.saveQueued();
		}
		
		public function surrenderGameByID(gameID:String):void
		{
			var gameIndex:int = NOT_FOUND;
			for (var i:int=0; i<_games.length; i++)
			{
				if (_games[i].id == gameID)
				{
					gameIndex = i;
					break;
				}
			}
			if (gameIndex != NOT_FOUND)
			{
				var game:GameEntity = _games[gameIndex];
				game.surrender(whatIsMyPlayerIndexForCurrentGame());
				game.updateDate = new Date();
				game.saveQueued();
			}
			else
			{
				if (_debug)
					trace("surrenderGameByID - didn't find game to surrender by ID");
			}
		}
		
		public function whoSurrenderedGame():String
		{
			var currentGame:GameEntity = _games[_currentGameIndex];
			var surrenderIndex:int = NOT_FOUND;
			for (var i:int=0; i<currentGame.surrenders.length; i++)
			{
				if (currentGame.surrenders[i])
				{
					surrenderIndex = i;
					break;
				}
			}
			if (surrenderIndex != NOT_FOUND)
				return currentGame.players[surrenderIndex];
			
			return "";
		}
		
		public function whoSurrenderedGameIndex():int
		{
			var currentGame:GameEntity = _games[_currentGameIndex];
			var surrenderIndex:int = NOT_FOUND;
			for (var i:int=0; i<currentGame.surrenders.length; i++)
			{
				if (currentGame.surrenders[i])
				{
					surrenderIndex = i;
					break;
				}
			}
			if (surrenderIndex != NOT_FOUND)
				return surrenderIndex;
			
			return NOT_FOUND;
		}
		
		public function pushInviteGameToActiveList(gameID:String):void
		{
			var gameIndex:int = findIndexOfGameWithID(gameID, _invitedGames);
			
			if (_debug)
				trace("pushInviteToActiveGameList - game id = " + gameID + " index = " + gameIndex);
			
			if (gameIndex != NOT_FOUND)
			{
				var gameToStart:GameEntity = _invitedGames[gameIndex];
				_games.push(gameToStart);
				_currentGameIndex = _games.length - 1;
				
				if (_debug)
					trace("now active game id = " + _games[_currentGameIndex]);
				
				_games[_currentGameIndex].refresh(onRefreshGameForJoinComplete, onRefreshGameForJoinError);
			}
		}
		
		private function onRefreshGameForJoinComplete(game:GameEntity):void
		{
			if (game.status == GAME_STATUS_REJECTED)
				_main.inviteNoLongerExists();
			else
			{
				var gameType:int = NOT_FOUND;
				if (game.numOfPlayers == 2)
					gameType = GAME_TYPE_2_PLAYER;
				else if (game.numOfPlayers == 3)
					gameType = GAME_TYPE_3_PLAYER;
				else if (game.numOfPlayers == 4)
					gameType = GAME_TYPE_4_PLAYER;
				
				if (_debug)
					trace("found game " + game.id);
				
				game.players.push(Main.sOnlineID);
				game.playerNicknames.push(Main.sOnlineNickname);
				
				if (game.players.length == game.numOfPlayers)
					game.status = GAME_STATUS_BEGUN;
				
				if (_debug)
					trace("pushed " + Main.sOnlineID + " now players = " + game.players);
				
				game.seenEndGame.push(false);
				game.surrenders.push(false);
				game.colors.push(game.players.length - 1);
				game.playerScores.push(_currentPlayerEntity.scores[gameType]);
				game.saveQueued();
				
				// create game player record
				var floxPlayer:GamePlayerEntity = new GamePlayerEntity();
				floxPlayer.gameID = game.id;
				floxPlayer.playerID = Main.sOnlineID;
				floxPlayer.publicAccess = Access.READ_WRITE;
				floxPlayer.saveQueued();
				
				// find invite entry
				var inviteIndex:int = NOT_FOUND;
				for (var i:int=0; i<_invites.length; i++)
				{
					if (_invites[i].gameID == game.id)
					{
						inviteIndex = i;
						break;
					}
				}
				if (inviteIndex != NOT_FOUND)
				{
					// set invite to accepted
					var invite:InviteEntity = _invites[inviteIndex];
					
					if (_debug)
						trace("changing invite " + inviteIndex + " id = " + invite.id + " gameid = " + invite.gameID);
					
					invite.status = INVITE_STATUS_ACCEPTED;
					invite.saveQueued();
				}
				
				if (_debug)
					trace("adding game " + game.id + " to saved game IDs");
				
				_main.launchGameInOnlinePlay(game.id);
			}
		}
		
		private function onRefreshGameForJoinError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onRefreshGameForJoinError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		public function getAllGameIDs():Array
		{
			var games:Array = [];
			for (var i:int=0; i<_games.length; i++)
				games.push(_games[i].id);
			return games;
		}
		
		public function getCurrentMap():int
		{
			return _games[_currentGameIndex].mapID;
		}
		
		public function getTurnPlayersForCurrentGame():Array
		{
			return _games[_currentGameIndex].turnPlayers;
		}
		
		public function getTurnCellTypesForCurrentGame():Array
		{
			return _games[_currentGameIndex].turnCellTypes;
		}
		
		public function getTurnLocationsForCurrentGame():Array
		{
			return _games[_currentGameIndex].turnLocations;
		}
		
		public function getTurnSurrendersForCurrentGame():Array
		{
			return _games[_currentGameIndex].turnSurrenders;
		}
		
		public function getPlayerTurnIndexForGame(playerID:String, gameID:String):int
		{
			var gameIndex:int = getGameFromID(gameID);
			var playerIndex:int;
			for (var i:int=0; i<_games[gameIndex].players.length; i++)
			{
				if (_games[gameIndex].players[i] == playerID)
					return i;
			}
			return NOT_FOUND;
		}
		
		public function getGameFromID(gameID:String):int
		{
			for (var i:int=0; i<_games.length; i++)
			{
				if (_games[i].id == gameID)
					return i;
			}
			return NOT_FOUND;
		}
		
		public function isItMyTurn(gameID:String):Boolean
		{
			var gameIndex:int = findIndexOfGameWithID(gameID, _games);
			var game:GameEntity = _games[gameIndex];
			
			if (game.activePlayerIndex <= game.players.length)
			{
				var currentPlayer:String = game.players[game.activePlayerIndex];
				if (currentPlayer == Main.sOnlineID)
					return true;
				return false;
			}
			else
				return false;
		}
		
		public function whatIsMyPlayerIndexForCurrentGame():int
		{
			// find player index
			var playerIndex:int = NOT_FOUND;
			var i:int;
			for (i=0; i<_games[_currentGameIndex].players.length; i++)
			{
				if (_games[_currentGameIndex].players[i] == Main.sOnlineID)
					playerIndex = i;
			}
			return playerIndex;
		}
		
		public function hasPlayerTakenATurn(gameID:String):Boolean
		{
			var gameIndex:int = findIndexOfGameWithID(gameID, _games);
			var playerIndex:int;
			var i:int;
			if (gameIndex == NOT_FOUND)
			{
				gameIndex = findIndexOfGameWithID(gameID, _invitedGames);
				
				if (gameIndex != NOT_FOUND)
				{
					// find player index
					playerIndex = NOT_FOUND;
					for (i=0; i<_invitedGames[gameIndex].players.length; i++)
					{
						if (_invitedGames[gameIndex].players[i] == Main.sOnlineID)
							playerIndex = i;
					}
					
					if (_debug)
						trace("game id = " + _invitedGames[gameIndex].id + " player index = " + playerIndex + " and current turn number = " + _invitedGames[gameIndex].currentTurnNumber);
					
					if (playerIndex != -1)
					{
						if (_invitedGames[gameIndex].currentTurnNumber > playerIndex)
							return true;
						return false;
					}
					else
						return false;
				}
			}
			else
			{
				if (gameIndex != NOT_FOUND)
				{
					// find player index
					playerIndex = NOT_FOUND;
					for (i=0; i<_games[gameIndex].players.length; i++)
					{
						if (_games[gameIndex].players[i] == Main.sOnlineID)
							playerIndex = i;
					}
					
					if (_debug)
						trace("game id = " + _games[gameIndex].id + " player index = " + playerIndex + " and current turn number = " + _games[gameIndex].currentTurnNumber);
					
					if (playerIndex != -1)
					{
						if (_games[gameIndex].currentTurnNumber > playerIndex)
							return true;
						return false;
					}
					else
						return false;
				}	
			}
			return true;
		}
		
		public function amIFirstPlayer(gameID:String):Boolean
		{
			var gameIndex:int = findIndexOfGameWithID(gameID, _games);
			
			if (gameIndex != NOT_FOUND)
			{
				if (_games[gameIndex].players[0] == Main.sOnlineID)
					return true;
			}
			return false;
		}
		
		public function getPlayerArrayFromCurrentGame():Array
		{
			return _games[_currentGameIndex].players;
		}
		
		public function getPlayerNicknameArrayFromCurrentGame():Array
		{
			return _games[_currentGameIndex].playerNicknames;
		}
		
		public function updateCurrentGameForTurns():void
		{
			_currentSurrenders = _games[_currentGameIndex].surrenders;
			Entity.load(GameEntity, _games[_currentGameIndex].id, onLoadGameForUpdateComplete, onLoadGameForUpdateError);
		}
		
		private function onLoadGameForUpdateComplete(game:GameEntity):void
		{
			var lastCurrentTurnNumber:int = _games[_currentGameIndex].currentTurnNumber;
			_games[_currentGameIndex] = game;
			
			var numOfTurns:int = _games[_currentGameIndex].currentTurnNumber - lastCurrentTurnNumber;
			
			_main.onlineGameManager.updateTurns(numOfTurns);
			_main.onlineGameManager.refreshPlayerText();
			
			var playerIndex:int;
			var i:int;
			
			// check for new surrenders
			var surrenderFound:Boolean = false;
			for (i=0; i<game.surrenders.length; i++)
			{
				if (game.surrenders[i] == true)
					surrenderFound = true;
			}
			if (surrenderFound)
			{
				if (game.surrenders != _currentSurrenders)
				{
					playerIndex = NOT_FOUND;
					
					// find out who surrendered
					for (i=0; i<game.surrenders.length; i++)
					{
						if (game.surrenders[i] != _currentSurrenders[i])
						{
							playerIndex = i;
							break;
						}
					}
					
					if (playerIndex != NOT_FOUND)
						_main.onlineGameManager.otherPlayerSurrendered(playerIndex);
				}
			}
		}
		
		private function onLoadGameForUpdateError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onLoadGameForUpdateError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
			
			if (error == "IO Error #2032")
				_main.onlineGameManager.addInternetConnectivityError();
		}
		
		public function updateCurrentGameNotForTurns():void
		{
			if (_main.onlineGameManager)
			{
				_currentSurrenders = _games[_currentGameIndex].surrenders;
				Entity.load(GameEntity, _games[_currentGameIndex].id, onLoadGameForNonTurnUpdateComplete, onLoadGameForNonTurnUpdateError);
			}
		}
		
		private function onLoadGameForNonTurnUpdateComplete(game:GameEntity):void
		{
			_games[_currentGameIndex] = game;
			_main.onlineGameManager.refreshPlayerText();
			
			var playerIndex:int;
			var i:int;
			
			// check for new surrenders
			var surrenderFound:Boolean = false;
			for (i=0; i<game.surrenders.length; i++)
			{
				if (game.surrenders[i] == true)
					surrenderFound = true;
			}
			if (surrenderFound)
			{
				if (game.surrenders != _currentSurrenders)
				{
					playerIndex = NOT_FOUND;
					
					// find out who surrendered
					for (i=0; i<game.surrenders.length; i++)
					{
						if (game.surrenders[i] != _currentSurrenders[i])
						{
							playerIndex = i;
							break;
						}
					}
					
					if (playerIndex != NOT_FOUND)
						_main.onlineGameManager.otherPlayerSurrendered(playerIndex);
				}
			}
			_main.onlineGameManager.liveUpdating = false;
		}
		
		private function onLoadGameForNonTurnUpdateError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onLoadGameForNonTurnUpdateError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
			
			if (error == "IO Error #2032")
			{
				//Main.debugText.text = "IOE onLoadGameForNonTurnUpdateError";
				_main.onlineGameManager.addInternetConnectivityError();
			}
		}
		
		public function getSubsetOfTurnCellTypesForCurrentGame(numOfTurns:int):Array
		{
			var cellTypes:Array = [];
			var currentGame:GameEntity = _games[_currentGameIndex];
			var startIndex:int = currentGame.currentTurnNumber - numOfTurns;
			var endIndex:int = currentGame.currentTurnNumber;
			
			if (_debug)
				trace("startIndex = " + startIndex + " endIndex = " + endIndex);
			
			for (var i:int=startIndex; i<endIndex; i++)
				cellTypes.push(currentGame.turnCellTypes[i]);
			
			if (_debug)
				trace("returning " + cellTypes.length + " cell types");
			
			return cellTypes;
		}
		
		public function getSubsetOfTurnLocationsForCurrentGame(numOfTurns:int):Array
		{
			var locations:Array = [];
			var currentGame:GameEntity = _games[_currentGameIndex];
			var startIndex:int = currentGame.currentTurnNumber - numOfTurns;
			var endIndex:int = currentGame.currentTurnNumber;
			for (var i:int=startIndex; i<endIndex; i++)
				locations.push(currentGame.turnLocations[i]);
			
			if (_debug)
				trace("returning " + locations.length + " locations");
			
			return locations;
		}
		
		public function getSubsetOfTurnSurrendersForCurrentGame(numOfTurns:int):Array
		{
			var surrenders:Array = [];
			var currentGame:GameEntity = _games[_currentGameIndex];
			var startIndex:int = currentGame.currentTurnNumber - numOfTurns;
			var endIndex:int = currentGame.currentTurnNumber;
			for (var i:int=startIndex; i<endIndex; i++)
				surrenders.push(currentGame.turnSurrenders[i]);
			
			if (_debug)
				trace("returning " + surrenders.length + " surrenders");
			
			return surrenders;
		}
		
		public function endGame(playerRankDataArray:Array):void
		{
			//trace("starting end game in entity manager");
			
			_playerRankDataArray = playerRankDataArray;
			
			/*
			for (var i:int=0; i<_playerRankDataArray.length; i++)
			{
			trace("set up player " + i + " as " + _playerRankDataArray[i].id + " win = " + _playerRankDataArray[i].win);
			}
			*/
			
			addEventListener(Event.ENTER_FRAME, updateWaitForTurnsToFinish);
		}
		
		private function updateWaitForTurnsToFinish(event:Event):void
		{
			if (_debug)
				trace("updating - waiting for turns to finish. taking turn = " + _takingTurn);
			
			//var errorEntity:ErrorEntity;
			var i:int;
			
			if (!_takingTurn)
			{
				removeEventListener(Event.ENTER_FRAME, updateWaitForTurnsToFinish);
				
				if (_games[_currentGameIndex] != null)
				{
					var currentGame:GameEntity = _games[_currentGameIndex];
					if (!currentGame.ranksCalculated)
					{
						if (_debug)
							trace("marking game as complete");
						
						// mark game complete and set player to have seen the 
						currentGame.status = GAME_STATUS_COMPLETE;
						var playerIndex:int = whatIsMyPlayerIndexForCurrentGame();
						currentGame.seenEndGame[playerIndex] = true;
						currentGame.ranksCalculated = true;
						currentGame.saveQueued();
						
						// ranked game - rate players
						if (currentGame.ranked)
						{
							_calculatingRanks = true;
							calculateRanksForGame();
						}
							// unranked game - just add to custom games totals
						else
						{
							if (_playerRankDataArray != null)
							{
								for (i=0; i<_playerRankDataArray.length; i++)
								{
									if (_playerRankDataArray[i].id != /* Main.sOnlineID */ _currentPlayerEntity.id)
									{
										if (_debug)
											trace("trying to load player " + _playerRankDataArray[i].id);
										
										Entity.load(PlayerEntity, _playerRankDataArray[i].id, onPlayerLoadForCustomGameComplete, onPlayerLoadForCustomGameError);
									}
								}
								if (_debug)
									trace("added to custom games total for " + _currentPlayerEntity.id);
								
								if (_currentPlayerEntity != null)
								{
									if (_debug)
										trace("calculating data for current player");
									
									var avgBoardCalc:Number = _currentPlayerEntity.customGamesPlayed * _currentPlayerEntity.avgPercentBoardOwnedCustom;
									avgBoardCalc += _playerRankDataArray[playerIndex].percentBoardOwned;
									_currentPlayerEntity.customGamesPlayed++;
									
									if (_playerRankDataArray.length > playerIndex)
									{
										if (_playerRankDataArray[playerIndex].win)
										{
											_currentPlayerEntity.customWins++;
											
											// find the dominatee
											for (i=0; i<_playerRankDataArray.length; i++)
											{
												if (_playerRankDataArray[i].lose && i != playerIndex)
												{
													if (_playerRankDataArray[i].id != "" && _playerRankDataArray[i].id != null && _playerRankDataArray[i].nickname != "" && _playerRankDataArray[i] != null)
														_currentPlayerEntity.addToDominateeCount(_playerRankDataArray[i].id, _playerRankDataArray[i].nickname);
												}
											}	
										}
										else if (_playerRankDataArray[playerIndex].lose)
										{
											_currentPlayerEntity.customLosses++;
											
											// find the nemesis
											for (i=0; i<_playerRankDataArray.length; i++)
											{
												if (_playerRankDataArray[i].win && i != playerIndex)
												{
													if (_playerRankDataArray[i].id != "" && _playerRankDataArray[i].id != null && _playerRankDataArray[i].nickname != "" && _playerRankDataArray[i] != null)
														_currentPlayerEntity.addToNemesisCount(_playerRankDataArray[i].id, _playerRankDataArray[i].nickname);
												}
											}
											
										}
										else if (_playerRankDataArray[playerIndex].tie)
											_currentPlayerEntity.customTies++;
										
										if (_debug)
											trace("setting data for player and saving current player");
										
										_currentPlayerEntity.avgPercentBoardOwnedCustom = avgBoardCalc / _currentPlayerEntity.customGamesPlayed;
										_currentPlayerEntity.numCellsCreatedCustom += _playerRankDataArray[playerIndex].numCellsCreated;
										_currentPlayerEntity.numCellsDestroyedCustom += _playerRankDataArray[playerIndex].numCellsDestroyed;
										_currentPlayerEntity.saveQueued();
									}
									else
									{
										/*
										errorEntity = new ErrorEntity();
										errorEntity.userID = Main.sOnlineID;
										errorEntity.deviceType = PathogenTest.device;
										errorEntity.functionName = "updateWaitForTurnsToFinish";
										errorEntity.error = "Player index is " + playerIndex + " but rank array length = " + _playerRankDataArray.length;
										errorEntity.saveQueued();
										*/
									}
								}
								else
								{
									/*
									errorEntity = new ErrorEntity();
									errorEntity.userID = Main.sOnlineID;
									errorEntity.deviceType = PathogenTest.device;
									errorEntity.functionName = "updateWaitForTurnsToFinish";
									errorEntity.error = "Current player entity is null";
									errorEntity.saveQueued();
									*/
								}
							}
								// if player rank data array is not found, send flox error
							else
							{
								/*
								errorEntity = new ErrorEntity();
								errorEntity.userID = Main.sOnlineID;
								errorEntity.deviceType = PathogenTest.device;
								errorEntity.functionName = "updateWaitForTurnsToFinish";
								errorEntity.error = "Player rank data array is null";
								errorEntity.saveQueued();
								*/
							}
						}
					}
					else
					{
						if (!currentGame.seenEndGame[playerIndex])
						{
							currentGame.seenEndGame[playerIndex] = true;
							currentGame.saveQueued();
						}
						
						refreshPlayerEntity();
					}
				}
					// if game is not found, send flox error
				else
				{
					/*
					errorEntity = new ErrorEntity();
					errorEntity.userID = Main.sOnlineID;
					errorEntity.deviceType = PathogenTest.device;
					errorEntity.functionName = "updateWaitForTurnsToFinish";
					errorEntity.error = "Current game is null";
					errorEntity.saveQueued();
					*/
				}
			}
		}
		
		private function onPlayerLoadForCustomGameComplete(player:PlayerEntity):void
		{
			if (_debug)
			{
				if (player != null)
					trace("in onPlayerLoadForCustomGameComplete - player id = " + player.id);
				else
					trace("in onPlayerLoadForCustomGameComplete - player is null");
			}
			
			if (player != null)
			{
				if (_debug)
					trace("added to custom games total for player " + player.id);
				
				var playerIndex:int = NOT_FOUND;
				for (var i:int=0; i<_playerRankDataArray.length; i++)
				{
					if (_debug)
						trace("checking " + player.id + " against " + _playerRankDataArray[i].id);
					
					if (_playerRankDataArray[i].id == player.id)
						playerIndex = i;
				}
				
				if (playerIndex != NOT_FOUND)
				{
					if (_debug)
						trace("found player - index " + playerIndex);
					var avgBoardCalc:Number = player.customGamesPlayed * player.avgPercentBoardOwnedCustom;
					
					if (_debug)
						trace("init avgBoardCalc = " + avgBoardCalc);
					
					avgBoardCalc += _playerRankDataArray[playerIndex].percentBoardOwned;
					
					if (_debug)
						trace("now avgBoardCalc = " + avgBoardCalc);
					
					player.customGamesPlayed++;
					
					var gameOverMessage:String;
					
					if (_playerRankDataArray[playerIndex].win)
					{
						player.customWins++;
						gameOverMessage = "You won a game against ";
						var playersBeaten:int = 0;
						
						// find the dominatee
						for (i=0; i<_playerRankDataArray.length; i++)
						{
							if (_playerRankDataArray[i].lose && i != playerIndex)
							{
								if (_playerRankDataArray[i].id != "" && _playerRankDataArray[i].id != null && _playerRankDataArray[i].nickname != "" && _playerRankDataArray[i] != null)
									player.addToDominateeCount(_playerRankDataArray[i].id, _playerRankDataArray[i].nickname);
								
								// add player nicknames to game over message
								playersBeaten++;
								if (playersBeaten > 1)
								{
									if (playersBeaten == (_games[_currentGameIndex].numOfPlayers - 1))
										gameOverMessage += " and ";
									else if (playersBeaten > 1)
										gameOverMessage += ", ";
								}
								gameOverMessage += _playerRankDataArray[i].nickname;
							}
						}
						
						if (_debug)
							trace("player " + player.id + " won");
					}
					else if (_playerRankDataArray[playerIndex].lose)
					{
						player.customLosses++;
						gameOverMessage = "You lost a game against ";
						var playersLostTo:int = 0;
						
						// find the nemesis
						for (i=0; i<_playerRankDataArray.length; i++)
						{
							if (_playerRankDataArray[i].win && i != playerIndex)
							{
								if (_playerRankDataArray[i].id != "" && _playerRankDataArray[i].id != null && _playerRankDataArray[i].nickname != "" && _playerRankDataArray[i] != null)
									player.addToNemesisCount(_playerRankDataArray[i].id, _playerRankDataArray[i].nickname);
								
								// add player nicknames to game over message
								playersLostTo++;
								if (playersLostTo > 1)
								{
									if (playersLostTo == (_games[_currentGameIndex].numOfPlayers - 1))
										gameOverMessage += " and ";
									else if (playersLostTo > 1)
										gameOverMessage += ", ";
								}
								gameOverMessage += _playerRankDataArray[i].nickname;
							}
						}
						
						if (_debug)
							trace("player " + player.id + " lost");
					}
					else if (_playerRankDataArray[playerIndex].tie)
					{
						if (_debug)
							trace("player " + player.id + " tied");
						
						player.customTies++;
						gameOverMessage = "You tied a game with ";
						var playersTiedWith:int = 0;
						
						// construct game over message
						for (i=0; i<_playerRankDataArray.length; i++)
						{
							if (_playerRankDataArray[i].tie && i != playerIndex)
							{
								// add player nicknames to game over message
								playersTiedWith++;
								if (playersTiedWith > 1)
								{
									if (playersTiedWith == (_games[_currentGameIndex].numOfPlayers - 1))
										gameOverMessage += " and ";
									else if (playersTiedWith > 1)
										gameOverMessage += ", ";
								}
								gameOverMessage += _playerRankDataArray[i].nickname;
							}
						}
					}
					
					player.avgPercentBoardOwnedCustom = avgBoardCalc / player.customGamesPlayed;
					player.numCellsCreatedCustom += _playerRankDataArray[playerIndex].numCellsCreated;
					player.numCellsDestroyedCustom += _playerRankDataArray[playerIndex].numCellsDestroyed;
					player.saveQueued();
					
					// push notification
					sendPushNotification(player.APNSTokenIDs, gameOverMessage);
				}
			}
			else
			{
				if (_debug)
					trace("found none or too many players to update for custom game!");
			}
		}
		
		private function onPlayerLoadForCustomGameError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onPlayerLoadForCustomGameError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		public function refreshPlayer():void
		{
			Player.current.refresh(onPlayerRefreshComplete, onPlayerRefreshError);
		}
		
		private function onPlayerRefreshComplete(player:Player):void
		{
		}
		
		private function onPlayerRefreshError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onPlayerRefreshError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		public function refreshPlayerEntity():void
		{
			//Main.updateDebugText("refreshPlayerEntity");
			
			_currentPlayerEntity.refresh(onPlayerEntityRefreshComplete, onRefreshPlayerEntityError);
		}
		
		private function onPlayerEntityRefreshComplete(player:PlayerEntity):void
		{
			//Main.updateDebugText("onPlayerEntityRefreshComplete");
		}
		
		private function onRefreshPlayerEntityError(error:String):void
		{
			//Main.updateDebugText("onRefreshPlayerEntityError");
			
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onRefreshPlayerEntityError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		public function isCurrentGameComplete():Boolean
		{
			if (_games[_currentGameIndex].status == GAME_STATUS_COMPLETE)
				return true;
			return false;
		}
		
		// random custom games
		public function findRandomGames():void
		{
			if (!_seekingRandomGame)
			{
				_seekingRandomGame = true;
				
				//var query:Query = new Query(GameEntity, "status == ? AND ranked == false AND random == true", GAME_STATUS_WAITING);
				//query.find(onRandomGameQueryComplete, onRandomGameQueryError);
				
				// new query for new index structure
				var query:Query = new Query(GameEntity, "statusAndRankedAndRandom == ?", GAME_STATUS_WAITING + "|false|true");
				query.find(onRandomGameQueryComplete, onRandomGameQueryError);
			}
		}
		
		private function onRandomGameQueryComplete(games:Array):void
		{
			_foundRandomGameToJoin = false;
			var gameIndex:int = NOT_FOUND;
			var smallestDiff:int = NOT_FOUND;
			
			// choose first game in list
			if (games.length > 0)
			{
				for (var i:int=0; i<games.length; i++)
				{
					var foundMyself:Boolean = false;
					for (var j:int=0; j<games[i].players.length; j++)
					{
						if (games[i].players[j] == Main.sOnlineID)
						{
							foundMyself = true;
							break;
						}
					}
					
					if (!foundMyself)
					{
						gameIndex = i;
						break;
					}
				}
			}
			
			// found the closest scored game
			if (gameIndex != NOT_FOUND)
			{
				// join game
				_gameToJoin = games[gameIndex];
				
				var joinRandomGameEntity:JoinRandomGameEntity = new JoinRandomGameEntity();
				joinRandomGameEntity.gameID = _gameToJoin.id;
				joinRandomGameEntity.playerID = Main.sOnlineID;
				joinRandomGameEntity.dateTime = new Date();
				joinRandomGameEntity.publicAccess = Access.READ_WRITE;
				joinRandomGameEntity.save(onTryToJoinGameComplete, onTryToJoinGameError);
			}
			else
				_seekingRandomGame = false;
		}
		
		private function onTryToJoinGameComplete(joinRandomGameEntity:JoinRandomGameEntity):void
		{
			var query:Query = new Query(JoinRandomGameEntity, "gameID == ?", _gameToJoin.id);
			query.find(onCheckNumberOfGameJoinersComplete, onCheckNumberOfGameJoinersError);
		}
		
		private function onTryToJoinGameError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onTryToJoinGameError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
			
			_seekingRandomGame = false;
		}
		
		private function onCheckNumberOfGameJoinersComplete(gameJoiners:Array):void
		{
			var floxPlayer:GamePlayerEntity;
			
			if (gameJoiners.length == 1)
			{
				if (gameJoiners[0].playerID == Main.sOnlineID)
				{
					if (_gameToJoin.players.length < _gameToJoin.numOfPlayers)
					{
						_games.push(_gameToJoin);
						_currentGameIndex = _games.length - 1;
						_games[_currentGameIndex].players.push(Main.sOnlineID);
						_games[_currentGameIndex].playerNicknames.push(Main.sOnlineNickname);
						_games[_currentGameIndex].seenEndGame.push(false);
						_games[_currentGameIndex].surrenders.push(false);
						_games[_currentGameIndex].colors.push(_games[_currentGameIndex].players.length - 1);
						if (_games[_currentGameIndex].players.length == _games[_currentGameIndex].numOfPlayers)
							_games[_currentGameIndex].status = GAME_STATUS_BEGUN;
						_games[_currentGameIndex].saveQueued();
						
						// create game player record
						floxPlayer = new GamePlayerEntity();
						floxPlayer.gameID = _gameToJoin.id;
						floxPlayer.playerID = Main.sOnlineID;
						floxPlayer.publicAccess = Access.READ_WRITE;
						floxPlayer.saveQueued();
						
						_foundRandomGameToJoin = true;
						_seekingRandomGame = false;
					}
					else
						_seekingRandomGame = false;
				}
				else
					_seekingRandomGame = false;
			}
			else if (gameJoiners.length > 1)
			{
				var earliestJoinerIndex:int = NOT_FOUND;
				var earliestDate:Date = new Date(0);
				var blankDate:Date = new Date(0);
				
				for (var i:int=0; i<gameJoiners.length; i++)
				{
					if (earliestDate == blankDate || gameJoiners[i].dateTime < earliestDate)
					{
						earliestDate = gameJoiners[i].dateTime;
						earliestJoinerIndex = i;
					}
				}
				if (earliestJoinerIndex != NOT_FOUND)
				{
					if (gameJoiners[earliestJoinerIndex].playerID == Main.sOnlineID)
					{
						//Main.updateDebugText("so i am joining the game");
						_games.push(_gameToJoin);
						_currentGameIndex = _games.length - 1;
						_games[_currentGameIndex].players.push(Main.sOnlineID);
						_games[_currentGameIndex].playerNicknames.push(Main.sOnlineNickname);
						_games[_currentGameIndex].seenEndGame.push(false);
						_games[_currentGameIndex].surrenders.push(false);
						_games[_currentGameIndex].colors.push(_games[_currentGameIndex].players.length - 1);
						if (_games[_currentGameIndex].players.length == _games[_currentGameIndex].numOfPlayers)
							_games[_currentGameIndex].status = GAME_STATUS_BEGUN;
						_games[_currentGameIndex].saveQueued();
						
						// create game player record
						floxPlayer = new GamePlayerEntity();
						floxPlayer.gameID = _gameToJoin.id;
						floxPlayer.playerID = Main.sOnlineID;
						floxPlayer.publicAccess = Access.READ_WRITE;
						floxPlayer.saveQueued();
						
						_foundRandomGameToJoin = true;
						_seekingRandomGame = false;
					}
					else
						_seekingRandomGame = false;
				}
				else
					_seekingRandomGame = false;
				
			}
			else
				_seekingRandomGame = false;
		}
		
		private function onCheckNumberOfGameJoinersError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onCheckNumberOfGameJoinersError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
			
			_seekingRandomGame = false;
		}
		
		private function onRandomGameQueryError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onRandomGameQueryError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		public function removeGame(gameID:String):void
		{
			// new query for new index structure
			var query:Query = new Query(GamePlayerEntity, "gameIDAndPlayerID == ?", gameID + "|" + Main.sOnlineID);
			query.find(onRemoveGameQueryComplete, onRemoveGameQueryError);
		}
		
		private function onRemoveGameQueryComplete(games:Array):void
		{
			if (games.length == 1)
			{
				var query:Query = new Query(InviteEntity, "gameID == ?", games[0].gameID);
				query.find(onRemoveGameInviteQueryComplete, onRemoveGameInviteQueryError);
				
				Entity.load(GameEntity, games[0].gameID, onLoadGameToRemoveComplete, onLoadGameToRemoveError);
				
				games[0].destroy(onDestroyGameComplete, onDestroyGameError);
			}
			else
			{
				if (_debug)
					trace("error - found not one game in onRemoveGameQueryComplete");
			}
		}
		
		private function onRemoveGameQueryError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onRemoveGameQueryError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		private function onRemoveGameInviteQueryComplete(invites:Array):void
		{
			if (invites.length == 1)
			{
				invites[0].status = INVITE_STATUS_REJECTED;
				invites[0].saveQueued();
			}
			else
			{
				if (_debug)
					trace("error - found not one invite in onRemoveGameInviteQueryComplete");
			}
		}
		
		private function onRemoveGameInviteQueryError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onRemoveGameInviteQueryError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		private function onLoadGameToRemoveComplete(game:GameEntity):void
		{
			if (game.status != GAME_STATUS_COMPLETE)
			{
				game.status = GAME_STATUS_REJECTED;
				game.saveQueued();
			}
		}
		
		private function onLoadGameToRemoveError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onLoadGameToRemoveError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		private function onDestroyGameComplete(game:GamePlayerEntity):void
		{
			_main.returnToOnlineGamesList();
		}
		
		private function onDestroyGameError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onDestroyGameError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		// ranked play
		public function isCurrentGameRanked():Boolean
		{
			return _games[_currentGameIndex].ranked;
		}
		
		public function gatherRankedGamesFromPlayerID():void
		{
			if (_debug)
				trace("gathering ranked from player ID");
			
			_games = new Vector.<GameEntity>();
			
			if (_currentPlayer)
			{
				var query:Query = new Query(GamePlayerEntity, "playerID == ?", Main.sOnlineID);
				query.find(onRankedGamePlayerQueryComplete, onGamePlayerQueryError);
			}
			else
				addEventListener(Event.ENTER_FRAME, waitForCurrentPlayerToBeSetRanked);
		}
		
		private function waitForCurrentPlayerToBeSetRanked(event:Event):void
		{
			if (_currentPlayer)
			{
				removeEventListener(Event.ENTER_FRAME, waitForCurrentPlayerToBeSetRanked);
				
				var query:Query = new Query(GamePlayerEntity, "playerID == ?", Main.sOnlineID);
				query.find(onRankedGamePlayerQueryComplete, onGamePlayerQueryError);
			}
		}
		
		private function onRankedGamePlayerQueryComplete(gamePlayers:Array):void
		{
			if (gamePlayers.length > 0)
			{
				_numberOfGamesToLoad = gamePlayers.length;
				_numberOfGamesLoaded = 0;
				_numberOfIncompleteGameLoaded = 0;
				
				//Main.updateDebugText("rank found " + _numberOfGamesToLoad + " games to load");
				
				if (_debug)
					trace("found " + _numberOfGamesToLoad + " games to load");
				
				for (var i:int; i<gamePlayers.length; i++)
				{
					var gamePlayer:GamePlayerEntity = gamePlayers[i];
					Entity.load(GameEntity, gamePlayer.gameID, onLoadRankedGameComplete, onLoadRankedGameError);
				}
			}
			else
				_main.onlinePlay.gamesGathered = true;
		}
		
		public function gatherRankedGamesFromSaveGameIDs(gameIDs:Array):void
		{
			if (_debug)
				trace("gathering ranked from save game IDs");
			
			if (gameIDs.length > 0)
			{
				_loadingFromSaveData = true;
				_numberOfGamesToLoad = gameIDs.length;
				_numberOfGamesLoaded = 0;
				_numberOfIncompleteGameLoaded = 0;
				
				for (var i:int; i<gameIDs.length; i++)
				{
					Entity.load(GameEntity, gameIDs[i], onLoadRankedGameComplete, onLoadRankedGameError);
				}
			}
			else
				_main.onlinePlay.gamesGathered = true;
		}
		
		private function onLoadRankedGameComplete(game:GameEntity):void
		{
			if (_debug)
				trace("on load ranked game complete");
			
			_numberOfGamesLoaded++;
			
			if (_debug)
				trace("number of games loaded = " + _numberOfGamesLoaded + " game ranked = " + game.ranked);
			
			if (!_main.onlinePlay.gamesGathered && game.ranked)
			{
				var loadGame:Boolean = false;
				if (game.status != GAME_STATUS_COMPLETE)
				{
					_numberOfIncompleteGameLoaded++;
					_rankedGamesLoaded++;
					
					//Main.updateDebugText("rank game load - " + _rankedGamesLoaded + " / " + currentPlayerEntity.rankedGames);
					
					if (_rankedGamesLoaded <= _currentPlayerEntity.rankedGames)
						loadGame = true;
					
					/*
					if (_numberOfIncompleteGameLoaded <= _currentPlayerEntity.rankedGames)
						loadGame = true;
					*/
				}
				else
					loadGame = true;
				
				if (loadGame)
				{
					_games.push(game);
					
					if (_debug)
						trace("now there are " + _games.length + " games in games list");
					
					var playersString:String = "";
					for (var i:int=0; i<_games.length; i++)
					{
						playersString = "";
						
						for (var j:int=0; j<_games[i].players.length; j++)
							playersString += _games[i].players[j] + ", ";
						
						/*
						if (_debug)
						trace(_games[i].id + " players = " + playersString);
						*/
					}
				}
			}
			
			if (_numberOfGamesLoaded == _numberOfGamesToLoad)
			{
				if (_debug)
					trace("all games loaded");
				
				_main.onlinePlay.gamesGathered = true;
			}
		}
		
		private function onLoadRankedGameError():void
		{
			// check to see if attempt was from save data
			if (_loadingFromSaveData)
			{
				// there was an error loading games from save data. try from Flox data
				_loadingFromSaveData = false;
				_main.onlineRankedGameIDs = [];
				gatherRankedGamesFromPlayerID();
			}
			else
			{
				// otherwise return to menu...load error
				_main.onlinePlay.gamesFailed = true;
			}
		}
		
		public function findRankedGames():void
		{
			//trace("called findRankedGame");
			if (!_seekingRankedGame)
			{
				_seekingRankedGame = true;

				// new query for new index structure
				var query:Query = new Query(GameEntity, "statusAndRanked == ?", GAME_STATUS_WAITING + "|true");
				query.find(onRankedGameQueryComplete, onRankedGameQueryError);
			}
		}
		
		private function onRankedGameQueryComplete(games:Array):void
		{
			if (_debug)
			{
				trace("called onRankedGameQueryComplete - got " + games.length + " games");
				//Main.updateDebugText("called onRankedGameQueryComplete - got " + games.length + " games");
			}
			
			_foundRankedGameToJoin = false;
			var gameIndex:int = NOT_FOUND;
			var smallestDiff:int = NOT_FOUND;
			
			// look through all games, and find the best match
			for (var i:int=0; i<games.length; i++)
			{
				/*
				// check if same league
				var sameLeague:Boolean = false;
				// bronze can be matched with no league, and vice versa
				if (_currentPlayerEntity.league2Player == "" || _currentPlayerEntity.league2Player == LEAGUE_BRONZE_2_PLAYER) 
				{
					if (games[i].league == "" || games[i].league == LEAGUE_BRONZE_2_PLAYER)
						sameLeague = true;
				}
				else
				{
					// silver has to be matched with silver, and gold with gold
					if (_currentPlayerEntity.league2Player == games[i].league)
						sameLeague = true;
				}
				
				// if a game has an open slot and is the same league
				if (games[i].players.length < games[i].numOfPlayers && sameLeague)
				{
					// calculate the avg score differential
					var diff:int = Math.abs(games[i].getAverageScore() - _currentPlayerEntity.scores[_currentGameTypeSeeking]);
					
					// if the score is not above the placement differential
					if (diff <= RANKED_GAME_PLACEMENT_SCORE_DIFFERENTIAL)
					{
						// and if it is the smallest differential
						if (smallestDiff == NOT_FOUND || diff < smallestDiff)
						{
							// grab game index
							smallestDiff = diff;
							gameIndex = i;
						}
					}
				}
				*/
				
				// if a game has an open slot
				if (games[i].players.length < games[i].numOfPlayers)
				{
					// calculate the avg score differential
					var diff:int = Math.abs(games[i].getAverageScore() - _currentPlayerEntity.scores[_currentGameTypeSeeking]);
					
					//Main.updateDebugText("game " + i + " diff = " + diff);
					
					// if the score is not above the placement differential
					if (diff <= RANKED_GAME_PLACEMENT_SCORE_DIFFERENTIAL)
					{
						// and if it is the smallest differential
						if (smallestDiff == NOT_FOUND || diff < smallestDiff)
						{
							// grab game index
							smallestDiff = diff;
							gameIndex = i;
						}
					}
				}
			}
			
			/*
			// found the closest scored game
			if (gameIndex != NOT_FOUND)
			{
				// join game
				var gameToStart:GameEntity = games[gameIndex];
				_games.push(gameToStart);
				_currentGameIndex = _games.length - 1;
				_games[_currentGameIndex].players.push(Main.sOnlineID);
				_games[_currentGameIndex].playerNicknames.push(Main.sOnlineNickname);
				_games[_currentGameIndex].seenEndGame.push(false);
				_games[_currentGameIndex].surrenders.push(false);
				_games[_currentGameIndex].colors.push(_games[_currentGameIndex].players.length - 1);
				if (_games[_currentGameIndex].players.length == _games[_currentGameIndex].numOfPlayers)
					_games[_currentGameIndex].status = GAME_STATUS_BEGUN;
				_games[_currentGameIndex].saveQueued();
				
				// create game player record
				var floxPlayer:GamePlayerEntity = new GamePlayerEntity();
				floxPlayer.gameID = gameToStart.id;
				floxPlayer.playerID = Main.sOnlineID;
				floxPlayer.publicAccess = Access.READ_WRITE;
				floxPlayer.saveQueued();
				
				_main.addGameIDToOnlineRankedGameIDs(gameToStart.id);
				
				_foundRankedGameToJoin = true;
			}
			*/
			
			if (_debug)
				trace("found closest game - game index " + i);
			
			// found the closest scored game
			if (gameIndex != NOT_FOUND)
			{
				// join game
				_gameToJoin = games[gameIndex];
				
				var joinRankedGameEntity:JoinRankedGameEntity = new JoinRankedGameEntity();
				joinRankedGameEntity.gameID = _gameToJoin.id;
				joinRankedGameEntity.playerID = Main.sOnlineID;
				joinRankedGameEntity.dateTime = new Date();
				joinRankedGameEntity.publicAccess = Access.READ_WRITE;
				joinRankedGameEntity.save(onTryToJoinRankedGameComplete, onTryToJoinRankedGameError);
			}
			else
				_seekingRankedGame = false;
		}
		
		private function onRankedGameQueryError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onRankedGameQueryError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		private function onTryToJoinRankedGameComplete(joinRankedGameEntity:JoinRankedGameEntity):void
		{
			if (_debug)
				trace("onTryToJoinRankedGameComplete");
			
			var query:Query = new Query(JoinRankedGameEntity, "gameID == ?", _gameToJoin.id);
			query.find(onCheckNumberOfRankedGameJoinersComplete, onCheckNumberOfRankedGameJoinersError);
		}
		
		private function onTryToJoinRankedGameError(error:String):void
		{
			if (_debug)
				trace("onTryToJoinRankedGameError");
			
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onTryToJoinRankedGameError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
			
			_seekingRankedGame = false;
		}
		
		private function onCheckNumberOfRankedGameJoinersComplete(gameJoiners:Array):void
		{
			if (_debug)
				trace("onCheckNumberOfRankedGameJoinersComplete - got " + gameJoiners.length + " joiners");
			
			var gameToStart:GameEntity;
			var floxPlayer:GamePlayerEntity;
			
			if (gameJoiners.length == 1)
			{
				if (gameJoiners[0].playerID == Main.sOnlineID)
				{
					if (_gameToJoin.players.length < _gameToJoin.numOfPlayers)
					{
						// join game
						gameToStart = _gameToJoin;
						_games.push(gameToStart);
						_currentGameIndex = _games.length - 1;
						_games[_currentGameIndex].players.push(Main.sOnlineID);
						_games[_currentGameIndex].playerNicknames.push(Main.sOnlineNickname);
						_games[_currentGameIndex].seenEndGame.push(false);
						_games[_currentGameIndex].surrenders.push(false);
						_games[_currentGameIndex].colors.push(_games[_currentGameIndex].players.length - 1);
						if (_games[_currentGameIndex].players.length == _games[_currentGameIndex].numOfPlayers)
							_games[_currentGameIndex].status = GAME_STATUS_BEGUN;
						_games[_currentGameIndex].playerLeagues.push(_currentPlayerEntity.league2Player);
						_games[_currentGameIndex].playerRanks.push(_currentPlayerEntity.ranks[GAME_TYPE_2_PLAYER]);
						_games[_currentGameIndex].playerScores.push(_currentPlayerEntity.scores[GAME_TYPE_2_PLAYER]);
						_games[_currentGameIndex].saveQueued();
						
						// create game player record
						floxPlayer = new GamePlayerEntity();
						floxPlayer.gameID = gameToStart.id;
						floxPlayer.playerID = Main.sOnlineID;
						floxPlayer.publicAccess = Access.READ_WRITE;
						floxPlayer.saveQueued();
						
						_foundRankedGameToJoin = true;
						_seekingRankedGame = false;
					}
					else
						_seekingRankedGame = false;
				}
				else
					_seekingRankedGame = false;
			}
			else if (gameJoiners.length > 1)
			{
				var earliestJoinerIndex:int = NOT_FOUND;
				var earliestDate:Date = new Date(0);
				var blankDate:Date = new Date(0);
				
				for (var i:int=0; i<gameJoiners.length; i++)
				{
					if (earliestDate == blankDate || gameJoiners[i].dateTime < earliestDate)
					{
						earliestDate = gameJoiners[i].dateTime;
						earliestJoinerIndex = i;
					}
				}
				if (earliestJoinerIndex != NOT_FOUND)
				{
					if (gameJoiners[earliestJoinerIndex].playerID == Main.sOnlineID)
					{
						// join game
						gameToStart = _gameToJoin;
						_games.push(gameToStart);
						_currentGameIndex = _games.length - 1;
						_games[_currentGameIndex].players.push(Main.sOnlineID);
						_games[_currentGameIndex].playerNicknames.push(Main.sOnlineNickname);
						_games[_currentGameIndex].seenEndGame.push(false);
						_games[_currentGameIndex].surrenders.push(false);
						_games[_currentGameIndex].colors.push(_games[_currentGameIndex].players.length - 1);
						if (_games[_currentGameIndex].players.length == _games[_currentGameIndex].numOfPlayers)
							_games[_currentGameIndex].status = GAME_STATUS_BEGUN;
						_games[_currentGameIndex].playerLeagues.push(_currentPlayerEntity.league2Player);
						_games[_currentGameIndex].playerRanks.push(_currentPlayerEntity.ranks[GAME_TYPE_2_PLAYER]);
						_games[_currentGameIndex].playerScores.push(_currentPlayerEntity.scores[GAME_TYPE_2_PLAYER]);
						_games[_currentGameIndex].saveQueued();
						
						// create game player record
						floxPlayer = new GamePlayerEntity();
						floxPlayer.gameID = gameToStart.id;
						floxPlayer.playerID = Main.sOnlineID;
						floxPlayer.publicAccess = Access.READ_WRITE;
						floxPlayer.saveQueued();
						
						_foundRankedGameToJoin = true;
						_seekingRankedGame = false;
					}
					else
						_seekingRankedGame = false;
				}
				else
					_seekingRankedGame = false;
				
			}
			else
				_seekingRankedGame = false;
		}
		
		private function onCheckNumberOfRankedGameJoinersError(error:String):void
		{
			if (_debug)
				trace("onCheckNumberOfRankedGameJoinersError");
			
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onCheckNumberOfRankedGameJoinersError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
			
			_seekingRankedGame = false;
		}
		
		public function saveCurrentPlayer():void
		{
			_currentPlayer.saveQueued();
			_currentPlayerEntity.saveQueued();
		}
		
		private function onLeagueQueryError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onLeagueQueryError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		public function setCurrentGameIndex(gameID:String):void
		{
			for (var i:int=0; i<_games.length; i++)
			{
				if (_games[i].id == gameID)
				{
					_currentGameIndex = i;
					break;
				}
			}
		}
		
		public function getTotalNumberOfGamesAndInvites():int
		{
			return _invites.length + _games.length;
		}
		
		public function getTotalNumberIncompleteGames():int
		{
			//Main.debugText.text = "Getting the total # of incomplete Games";
			
			var total:int = 0;
			for (var i:int=0; i<_games.length; i++)
			{
				if (_games[i].status != GAME_STATUS_COMPLETE && !_games[i].ranked)
					total++;
			}
			
			//Main.debugText.text = "COMPLETE getting the total # of incomplete Games";
			
			return total;
		}
		
		public function getTotalNumberIncompleteRankedGames():int
		{
			//Main.debugText.text = "Getting the total # of incomplete Games";
			
			var total:int = 0;
			for (var i:int=0; i<_games.length; i++)
			{
				if (_games[i].status != GAME_STATUS_COMPLETE && _games[i].ranked)
					total++;
			}
			
			//Main.debugText.text = "COMPLETE getting the total # of incomplete Games";
			
			return total;
		}
		
		private function findIndexOfGameWithID(gameID:String, gamesList:Vector.<GameEntity>):int
		{
			var gameIndex:int = NOT_FOUND;
			var i:int;
			for (i=0; i<gamesList.length; i++)
			{
				if (gamesList[i].id == gameID)
				{
					gameIndex = i;
					break;
				}
			}
			return gameIndex;
		}
		
		public function findInvitedGameFromID(gameID:String):GameEntity
		{
			for (var i:int=0; i<_invitedGames.length; i++)
			{
				if (_invitedGames[i].id == gameID)
				{
					return _invitedGames[i];
				}
			}
			return null;
		}
		
		private function calculateRanksForGame():void
		{
			// step 1 - load all players
			if (_debug)
				trace("step 1 - calculating ranks");
			
			_gamePlayers = new Vector.<PlayerEntity>();
			_gamePlayerDivisions = [];
			
			addEventListener(Event.ENTER_FRAME, rankUpdate1);
			for (var i:int=0; i<_playerRankDataArray.length; i++)
			{
				if (_debug)
					trace("from calculateRanksForGame, querying player " + _playerRankDataArray[i].id);
				
				//var query:Query = new Query(PlayerEntity, "authId == ?", _playerRankDataArray[i].id);
				//query.find(onRankPlayerLoadComplete, onRankPlayerLoadError);
				Entity.load(PlayerEntity, _playerRankDataArray[i].id, onRankPlayerLoadComplete, onRankPlayerLoadError);
			}
		}
		
		private function onRankPlayerLoadComplete(player:PlayerEntity):void
		{
			if (player != null)
			{
				_gamePlayers.push(player);
				_gamePlayerDivisions.push(NOT_FOUND);
				
				if (_debug)
					trace("added " + player.id + " to player list");
				
				var currentGame:GameEntity = _games[_currentGameIndex];
				var playerIndex:int = NOT_FOUND;
				var i:int;
				
				for (i=0; i<_playerRankDataArray.length; i++)
				{
					if (player.id == _playerRankDataArray[i].id)
					{
						playerIndex = i;
						_playerRankDataArray[i].status = 0;
						_playerRankDataArray[i].playerEntity = player;
						break;
					}
				}
				if (playerIndex != NOT_FOUND)
				{
					if (_debug)
						trace(player.id + " found player index for data array - index = " + playerIndex);
					
					var totalGames:int = 0;
					for (i=0; i<NUMBER_OF_LEAGUES; i++)
					{
						totalGames += player.wins[i];
						totalGames += player.losses[i];
						totalGames += player.ties[i];
					}
					var avgBoardCalc:Number = totalGames * player.avgPercentBoardOwnedRanked;
					
					if (_debug)
						trace("_playerRankDataArray for " + player.id + " = " + _playerRankDataArray[playerIndex]);
					
					if (_playerRankDataArray[playerIndex].win)
						player.wins[currentGame.gameType]++;
					else if (_playerRankDataArray[playerIndex].lose)
						player.losses[currentGame.gameType]++;
					else if (_playerRankDataArray[playerIndex].tie)
						player.ties[currentGame.gameType]++;
					
					avgBoardCalc += _playerRankDataArray[playerIndex].percentBoardOwned;
					totalGames++;
					player.avgPercentBoardOwnedRanked = avgBoardCalc / totalGames;
					player.numCellsCreatedRanked += _playerRankDataArray[playerIndex].numCellsCreated;
					player.numCellsDestroyedRanked += _playerRankDataArray[playerIndex].numCellsDestroyed;
					
					_numPlayersDataTabulated++;
					
					_playerRankDataArray[playerIndex].status = 1;
				}
			}
		}
		
		private function onRankPlayerLoadError(error:String):void
		{
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onRankPlayerLoadError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		private function rankUpdate1(event:Event):void
		{
			var completed:Boolean = true;
			for (var i:int=0; i<_playerRankDataArray.length; i++)
			{
				if (_playerRankDataArray[i].status <= 0)
					completed = false;
			}
			if (completed)
			{
				removeEventListener(Event.ENTER_FRAME, rankUpdate1);
				
				if (_debug)
					trace("set all data for player. " + _numPlayersDataTabulated + " have been tabulated");
				
				calculateScores();
			}
		}
		
		private function calculateScores():void
		{
			// step 2 - calculate player scores
			if (_debug)
				trace("step 2 - calculating scores");
			
			var currentGame:GameEntity = _games[_currentGameIndex];
			var i:int;
			var numPlayersScored:int = 0;
			
			for (i=0; i<_gamePlayers.length; i++)
			{
				var opponentScoreTotal:int = 0;
				var opponents:int = 0;
				var j:int;
				
				// tablulate opponent total score
				for (j=0; j<_gamePlayers.length; j++)
				{
					if (_gamePlayers[j] != _gamePlayers[i])
					{
						opponents++;
						opponentScoreTotal += _gamePlayers[j].scores[currentGame.gameType];
					}
				}
				
				if (_debug)
					trace("total opponent scores for player " + _gamePlayers[i].nickname + " = " + opponentScoreTotal + " and opponents = " + opponents);
				
				for (j=0; j<_playerRankDataArray.length; j++)
				{
					if (_gamePlayers[i] == _playerRankDataArray[j].playerEntity)
					{
						_playerRankDataArray[j].avgOpponentScore = opponentScoreTotal / opponents;
						
						if (_debug)
							trace("avg opponent score for player " + _playerRankDataArray[j].nickname + " = " + _playerRankDataArray[j].avgOpponentScore);
						
						break;
					}
				}
			}

			var oldScore:int = 0;
			var thisIsMe:Boolean = false;
			for (i=0; i<_playerRankDataArray.length; i++)
			{
				var scoreFactor:Number;

				if (_playerRankDataArray[i].playerEntity.id == _currentPlayerEntity.id)
				{
					oldScore = _playerRankDataArray[i].playerEntity.scores[currentGame.gameType];
					thisIsMe = true;
				}
				
				// set whether they won, lost or tied
				if (_playerRankDataArray[i].win)
					scoreFactor = SCORING_WIN;
				else if (_playerRankDataArray[i].lose)
					scoreFactor = SCORING_LOSS;
				else if (_playerRankDataArray[i].tie)
					scoreFactor = SCORING_TIE;
				
				if (_debug)
					trace("player " + _playerRankDataArray[i].nickname + " score factor = " + scoreFactor);
				
				_playerRankDataArray[i].playerEntity.calculateScore(currentGame.gameType, _playerRankDataArray[i].avgOpponentScore, scoreFactor);

				if (_debug)
					trace("now player score = " + _playerRankDataArray[i].playerEntity.scores[currentGame.gameType]);
				
				if (thisIsMe)
				{
					var changeInScore:int = _playerRankDataArray[i].playerEntity.scores[currentGame.gameType] - oldScore;
					_main.onlineGameManager.setVictoryScreenChangeInScore(changeInScore);
				}
				
				_playerRankDataArray[i].playerEntity.saveQueued();
				numPlayersScored++;
	
				// step 2 completed - all scores calculated. now calculated player ranks
				//if (_numPlayersScored == (currentGame.numOfPlayers - 1))
				if (numPlayersScored == currentGame.numOfPlayers)
					setPlayerLeagues();
			}
		}
		
		private function setPlayerLeagues():void
		{
			_leaguesToRank = new Vector.<LeagueRank>();
			_playerLeagues = [];
			addEventListener(Event.ENTER_FRAME, rankUpdate2);
		}
		
		private function rankUpdate2(event:Event):void
		{
			var complete:Boolean = true;
			var i:int;
			
			if (_debug)
				trace("rankUpdate2 " + _playerRankDataArray[i].nickname + " status = " + _playerRankDataArray[i].status + " setLeague = " + _playerRankDataArray[i].setLeague);
			
			for (i=0; i<_playerRankDataArray.length; i++)
			{
				if (_playerRankDataArray[i].status <= 1)
				{
					complete = false;
					
					if (_debug)
						trace("rankUpdate2 incomplete and setLeague = " + _playerRankDataArray[i].setLeague);
					
					if (!_playerRankDataArray[i].setLeague)
					{
						_playerRankDataArray[i].setLeague = true;
						
						if (_debug)
							trace("rankUpdate2 now setLeague = " + _playerRankDataArray[i].setLeague);
						
						setPlayerLeague(_playerRankDataArray[i]);
					}
					break;
				}
			}
			if (_debug)
				trace("rankUpdate2 - complete = " + complete);
			if (complete)
			{
				if (_debug)
					trace("saving leagues");
				for (i=0; i<_playerLeagues.length; i++)
				{
					trace("saving league " + _playerLeagues[i].id);
					_playerLeagues[i].saveQueued();
				}
				
				rankLeagues();
				removeEventListener(Event.ENTER_FRAME, rankUpdate2);	
			}
		}
		
		private function setPlayerLeague(playerRankData:OnlinePlayerRankData):void
		{
			var player:PlayerEntity = playerRankData.playerEntity;
			var currentGame:GameEntity = _games[_currentGameIndex];
			var currentGameType:int = currentGame.gameType;
			var leagueFound:Boolean = true;
			var movingLeagues:Boolean = false;
			var oldLeagueID:String = "";
			var oldDivision:int = NOT_FOUND;
			var leagueStr:String = "";
			var division:int = NOT_FOUND;
			var alreadyRanking:Boolean;
			var league:LeagueEntity;
			var found:Boolean;
			var i:int;
			var j:int;
			
			if (_debug)
				trace(player.nickname + " setting league for player " + player.id);

			if (player.league2Player == "")
				leagueFound = false;
			
			var leagueToRank:LeagueRank
			
			// if already in existing league, check to make sure still in same league
			if (leagueFound)
			{
				if (_debug)
					trace(player.nickname + " found league for player");
				
				if (player.scores[currentGameType] < LEAGUE_SILVER_TIER)
				{
					oldLeagueID = player.league2Player;
					oldDivision = player.division2Player;
						
					if (player.league2Player != LEAGUE_BRONZE_2_PLAYER)
					{
						movingLeagues = true;
						
						if (_debug)
							trace(player.nickname + " is moving leagues"); 
					}
				}
				else if (player.scores[currentGameType] < LEAGUE_GOLD_TIER)
				{
					oldLeagueID = player.league2Player;
					oldDivision = player.division2Player;
						
					if (player.league2Player != LEAGUE_SILVER_2_PLAYER)
					{
						movingLeagues = true;
						
						if (_debug)
							trace(player.nickname + " is moving leagues"); 
					}
				}
				else if (player.scores[currentGameType] >= LEAGUE_GOLD_TIER)
				{
					oldLeagueID = player.league2Player;
					oldDivision = player.division2Player;
						
					if (player.league2Player != LEAGUE_GOLD_2_PLAYER)
					{
						movingLeagues = true;
						
						if (_debug)
							trace(player.nickname + " is moving leagues"); 
					}
				}
				
				// add old league to the vector of leagues to rank
				alreadyRanking = false;
				if (oldLeagueID != "" && oldDivision != NOT_FOUND)
				{
					for (j=0; j<_leaguesToRank.length; j++)
					{
						if (_leaguesToRank[j].league == oldLeagueID && _leaguesToRank[j].division == oldDivision)
							alreadyRanking = true;
					}
				}
				
				if (!alreadyRanking)
				{
					leagueToRank = new LeagueRank(oldLeagueID, oldDivision);
					_leaguesToRank.push(leagueToRank);
					
					if (_debug)
						trace(player.nickname + " added old " + leagueToRank.league + " " + leagueToRank.division + " to leagues list");
				}
				
				if (!movingLeagues)
				{
					if (_debug)
						trace(player.nickname + " status set to 2");
					
					playerRankData.status = 2;
				}
			}
			
			// league not found. check to see if player is ready for league
			if (!leagueFound || movingLeagues)
			{
				if (_debug)
					trace(player.nickname + " didn't find league or moving league. player score = " + player.scores[currentGameType]);
				
				// remove a player from last league division slot
				if (movingLeagues && oldLeagueID != "" && oldDivision != NOT_FOUND)
				{
					// reset ranks
					for (j=0; j<player.ranks.length; j++)
					{
						player.ranks[j] = NOT_FOUND;
						player.lastRanks[j] = NOT_FOUND;
					}
					
					league = null;
					found = false;
					for (j=0; j<_playerLeagues.length; j++)
					{
						if (_playerLeagues[i].id == leagueID)
						{
							if (_debug)
								trace(player.nickname + " found league " + _playerLeagues[i].id + " in list of player leagues");
							
							found = true;
							league = _playerLeagues[i];
							break;
						}
					}
					
					if (!found)
					{
						// set new league and division
						Entity.load(LeagueEntity, oldLeagueID, 
							function onLeagueMoveQueryComplete(league:LeagueEntity):void
							{
								if (_debug)
									trace(player.nickname + " loaded league " + league.id + " - removing count from " + oldLeagueID + " / " + oldDivision);
								
								league.removeFromDivision(oldDivision);
								//league.saveQueued();
								_playerLeagues.push(league);
							}
							, onLeagueQueryError);
					}
					else
					{
						if (_debug)
							trace(player.nickname + " didn't load league. league = " + league.id + " - removing count from " + league.id + " / " + oldDivision);
						
						league.removeFromDivision(oldDivision);
					}
				}
				
				// set default league
				if (player.isReadyForLeague(currentGameType))
				{
					if (_debug)
						trace(player.nickname + " is ready for league. setting league");
					
					var leagueID:String;
					if (player.scores[currentGameType] < LEAGUE_SILVER_TIER)
					{
						player.league2Player = LEAGUE_BRONZE_2_PLAYER;
						leagueID = LEAGUE_BRONZE_2_PLAYER;
					}
					else if (player.scores[currentGameType] < LEAGUE_GOLD_TIER)
					{
						player.league2Player = LEAGUE_SILVER_2_PLAYER;
						leagueID = LEAGUE_SILVER_2_PLAYER;
					}
					else if (player.scores[currentGameType] >= LEAGUE_GOLD_TIER)
					{
						player.league2Player = LEAGUE_GOLD_2_PLAYER;
						leagueID = LEAGUE_GOLD_2_PLAYER;
					}
					
					if (_debug)
						trace(player.nickname + " set league to " + leagueID + " and now setting division");
					
					league = null;
					found = false;
					for (j=0; j<_playerLeagues.length; j++)
					{
						if (_playerLeagues[i].id == leagueID)
						{
							if (_debug)
								trace(player.nickname + " found league " + _playerLeagues[i].id + " in list of player leagues");
							
							found = true;
							league = _playerLeagues[i];
							break;
						}
					}
					
					alreadyRanking = false;
					var divisionIndex:int;
					if (!found)
					{
						if (_debug)
							trace(player.nickname + " loading league " + leagueID);
						
						// set new league and division
						Entity.load(LeagueEntity, leagueID, 
							function onLeagueQueryComplete(league:LeagueEntity):void
							{
								setDivisionInLeague(player, league);
								_playerLeagues.push(league);
								playerRankData.status = 2;
								
								if (_debug)
									trace(player.nickname + " status set to 2");
							}
							, onLeagueQueryError);
					}
					else
					{
						if (_debug)
							trace(player.nickname + " not loading league");
						
						setDivisionInLeague(player, league);
						playerRankData.status = 2;
						
						if (_debug)
							trace(player.nickname + " status set to 2");
					}
				}	
			}
		}
		
		private function setDivisionInLeague(player:PlayerEntity, league:LeagueEntity):void
		{
			if (_debug)
				trace(player.nickname + " setDivisionInLeague for league " + league.id);
			
			var alreadyRanking:Boolean = false;
			var divisionIndex:int = league.addToNextAvailableDivision();
			var leagueStr:String = "";
			var division:int = NOT_FOUND;
			var leagueToRank:LeagueRank;
			var i:int;
			
			if (_debug)
				trace(player.nickname + " got division " + divisionIndex);
			
			player.division2Player = divisionIndex;
			player.saveQueued();
			if (_currentPlayerEntity.id == player.id)
				_currentPlayerEntity = player;
			
			// find league/division combo string, to see if league/division needs to be ranked
			leagueStr = player.league2Player;
			division = player.division2Player;
			
			if (_debug)
				trace(player.nickname + " checking if league is already in rank list");
			
			// add new league to list of leagues to rank
			if (leagueStr != "" && division != NOT_FOUND)
			{
				for (i=0; i<_leaguesToRank.length; i++)
				{
					if (_leaguesToRank[i].league == leagueStr && _leaguesToRank[i].division == division)
					{
						alreadyRanking = true;
						
						if (_debug)
							trace(player.nickname + " league " + leagueStr + " div " + division + " is already being ranked");
					}
				}
				if (!alreadyRanking)
				{
					leagueToRank = new LeagueRank(leagueStr, division);
					_leaguesToRank.push(leagueToRank);
					
					if (_debug)
						trace(player.nickname + " added " + leagueToRank.league + " " + leagueToRank.division + " to leagues list");
				}
			}
		}
		
		private function rankLeagues():void
		{
			if (_debug)
				trace("now ranking leagues");
			
			for (var i:int=0; i<_leaguesToRank.length; i++)
			{
				if (_debug)
					trace("league " + _leaguesToRank[i].league + " division " + _leaguesToRank[i].division);
				rankLeague(_leaguesToRank[i]);
			}
			
			addEventListener(Event.ENTER_FRAME, rankUpdate3);
		}
		
		public function rankLeague(leagueToRank:LeagueRank):void
		{
			if (_debug)
				trace("in rankLeague - ranking " + leagueToRank.league + " / " + leagueToRank.division);
			
			var currentGameType:int = GAME_TYPE_2_PLAYER;
			var query:Query;
			if (currentGameType == GAME_TYPE_2_PLAYER)
			{
				if (_debug)
					trace("query on 2 player game");
				
				query = new Query(PlayerEntity, "twoPlayer == ?", leagueToRank.league + "|" + leagueToRank.division);
			}
			else if (currentGameType == GAME_TYPE_3_PLAYER)
			{
				if (_debug)
					trace("query on 3 player game");
				
				query = new Query(PlayerEntity, "threePlayer == ?", leagueToRank.league + "|" + leagueToRank.division);
			}
			else if (currentGameType == GAME_TYPE_4_PLAYER)
			{
				if (_debug)
					trace("query on 4 player game");
				
				query = new Query(PlayerEntity, "fourPlayer == ?", leagueToRank.league + "|" + leagueToRank.division);
			}
			query.limit = DIVISION_MAX;
			query.find(onPlayerQueryComplete, onPlayerQueryError);
			
			leagueToRank.status = true;
		}
		
		private function onPlayerQueryComplete(players:Array):void
		{
			if (_debug)
				trace("found " + players.length + " players in league/division");
			
			var gameType:int = GAME_TYPE_2_PLAYER;
			
			// grab the score to sort by
			var i:int;
			for (i=0; i<players.length; i++)
			{
				players[i].sortScore = players[i].scores[gameType];
			}
			
			// sory players by score and save
			players.sortOn("sortScore", Array.DESCENDING | Array.NUMERIC);
			for (i=0; i<players.length; i++)
			{
				players[i].lastRanks[gameType] = players[i].ranks[gameType];
				players[i].ranks[gameType] = i;
				players[i].saveQueued();
				if (_currentPlayerEntity.id == players[i].id)
					_currentPlayerEntity = players[i];
			}
			
			// store ladder if it is the current player's league and division
			if (players.length > 0)
			{
				if (players[0].league2Player == _currentPlayerEntity.league2Player && players[0].division2Player == _currentPlayerEntity.division2Player)
				{
					_currentLadder = [];
					_currentLadder = players;
				}
			}
			
			if (_debug)
				trace("all players re-ranked in that league");
		}
		
		private function onPlayerQueryError(error:String):void
		{
			if (_debug)
				trace("onPlayerQueryError");
			
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onPlayerQueryError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		private function rankUpdate3(event:Event):void
		{
			// check all leagues to make sure ranking is done
			var leaguesDone:Boolean = true;
			for (var i:int=0; i<_leaguesToRank.length; i++)
			{
				if (!_leaguesToRank[i].status)
				{
					leaguesDone = false;
					break;
				}
			}
			
			// once done, finish up
			if (leaguesDone)
			{
				if (_debug)
					trace("all leagues ranked. finishing up");
				
				removeEventListener(Event.ENTER_FRAME, rankUpdate3);
				_calculatingRanks = false;
			}
		}
		
		public function generateCurrentLadder():void
		{
			if (_currentPlayerEntity.league2Player != "" && _currentPlayerEntity.division2Player != NOT_FOUND)
			{
				_currentLadder = [];
				var query:Query = new Query(PlayerEntity, "twoPlayer == ?", _currentPlayerEntity.league2Player + "|" + _currentPlayerEntity.division2Player);
				query.limit = DIVISION_MAX;
				query.find(onPlayerLadderQueryComplete, onPlayerLadderQueryError);
			}
		}
		
		private function onPlayerLadderQueryComplete(players:Array):void
		{
			var gameType:int = GAME_TYPE_2_PLAYER;
			
			if (_debug)
				trace("found " + players.length + " players in league/division - game type = " + gameType);
			
			// grab the score to sort by
			var i:int;
			for (i=0; i<players.length; i++)
			{
				/*
				if (_debug)
				{
					trace(players[i].id + " " + i + " " + players[i].sortScore);
					//trace(i + " " + players[i].scores[gameType]);
				}*/
				players[i].sortScore = players[i].scores[gameType];
			}
			
			// sory players by score
			players.sortOn("sortScore", Array.DESCENDING | Array.NUMERIC);
			
			// store ladder if it is the current player's league and division
			if (players.length > 0)
			{
				if (players[0].league2Player == _currentPlayerEntity.league2Player && players[0].division2Player == _currentPlayerEntity.division2Player)
				{
					_currentLadder = [];
					_currentLadder = players;
				}
			}
		}
		
		private function onPlayerLadderQueryError(error:String):void
		{
			if (_debug)
				trace("onPlayerLadderQueryError");
			
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onPlayerLadderQueryError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		// utility functions
		private function inviteDateArraySortFunction(invite1:InviteEntity, invite2:InviteEntity):int
		{
			if (invite1.dateTime != null && invite2.dateTime != null)
			{
				if (invite1.dateTime.valueOf() < invite2.dateTime.valueOf())
					return 1;
				else if (invite1.dateTime.valueOf() > invite2.dateTime.valueOf())
					return -1;
				else
					return 0;
			}
			else
				return 0;
		}
		
		private function gameDateArraySortFunction(game1:GameEntity, game2:GameEntity):int
		{
			if (game1.updateDate != null && game2.updateDate != null)
			{
				if (game1.updateDate.valueOf() < game2.updateDate.valueOf())
					return -1;
				else if (game1.updateDate.valueOf() > game2.updateDate.valueOf())
					return 1;
				else
					return 0;
				7}
			else
				return 0;
		}
		
		public function gameStatusArraySortFunction(game1:GameEntity, game2:GameEntity):int
		{
			if (game1.status == GAME_STATUS_COMPLETE && game2.status != GAME_STATUS_COMPLETE)
				return 1;
			else if (game1.status != GAME_STATUS_COMPLETE && game2.status == GAME_STATUS_COMPLETE)
				return -1;
			else
				return 0;
		}
		
		private function playerDivisionArraySortFunction(player1:PlayerEntity, player2:PlayerEntity, leagueIndex:int):int
		{
			if (player1.ranks[leagueIndex] < player2.ranks[leagueIndex])
				return -1;
			else if (player1.ranks[leagueIndex] > player2.ranks[leagueIndex])
				return 1;
			else
				return 0;
		}
		
		public function getLeagueName(league:String):String
		{
			if (league)
			{
				if (league.length > 0)
				{
					var trimPos:int = league.indexOf("_");
					league = league.substr(0, trimPos);
					var firstChar:String = league.substr(0, 1);
					var rest:String = league.substr(1, league.length);
					league = firstChar.toUpperCase() + rest;
					return league;
				}
			}
			return "";
		}
		
		public function checkForLeague():Boolean
		{
			var newLeague:Boolean = false;
			
			// check for league pop-up
			if ((_currentPlayerEntity.league2Player != "") && (_currentPlayerEntity.league2Player != _currentLeague2Player))
			{
				// if just joined a league for the first time
				if (_currentLeague2Player == "")
				{
					newLeague = true;
				}
				else
				{
					// if moving up league
					if ((_currentLeague2Player == LEAGUE_BRONZE_2_PLAYER && _currentPlayerEntity.league2Player == LEAGUE_SILVER_2_PLAYER) ||
						(_currentLeague2Player == LEAGUE_SILVER_2_PLAYER && _currentPlayerEntity.league2Player == LEAGUE_GOLD_2_PLAYER))
					{
						newLeague = true;
					}
				}
				
				// save league and rank
				_currentLeague2Player = _currentPlayerEntity.league2Player;
				_currentRank2Player = _currentPlayerEntity.ranks[GAME_TYPE_2_PLAYER];
				
				// save data
				Main.saveDataObject.data.league2Player = _currentLeague2Player;
				Main.saveDataObject.data.rank2Player = _currentRank2Player;
				Main.saveDataObject.flush();
			}
			return newLeague;
		}
		
		// player record functions
		private function checkIfPlayersHavePathogen():void
		{
			for (var i:int=0; i<_players.length; i++)
			{
				Entity.load(PlayerEntity, _players[i].gamerID, 
					function onLoadPlayerEntityComplete (player:PlayerEntity):void
					{
						if (player != null)
							setPlayerHasPathogen(player.id, true);
						else
							setPlayerHasPathogen(player.id, false);
					},
					function onLoadPlayerEntityError (error:String):void 
					{
						/*
						var errorEntity:ErrorEntity = new ErrorEntity();
						errorEntity.userID = Main.sOnlineID;
						errorEntity.deviceType = PathogenTest.device;
						errorEntity.functionName = "onLoadPlayerEntityError";
						errorEntity.error = error;
						errorEntity.saveQueued();
						*/
					}
				);
				//_players[i].hasPathogen = true;
			}
			
			/*
			for (var i:int=0; i<_players.length; i++)
			{
			trace("player " + _players[i].gamerID + " hasPathogen = " + _players[i].hasPathogen);
			}
			*/
		}
		
		private function setPlayerHasPathogen(playerID:String, hasPathogen:Boolean):void
		{
			for (var i:int=0; i<_players.length; i++)
			{
				if (_players[i].gamerID == playerID)
				{
					_players[i].hasPathogen = hasPathogen;
					break;
				}
			}
		}
		
		public function reloadPlayerFriends():void
		{
			_players = new Vector.<PlayerRecord>();
			
			// build friends list from Game Center friends
			for (var i:int=0; i<Main.sGCFriends.length; i++)
			{
				var player:PlayerRecord = new PlayerRecord(Main.sGCFriends[i].id.substr(2), Main.sGCFriends[i].alias);
				_players.push(player);
			}
			checkIfPlayersHavePathogen();
		}
		
		// online play utility functions
		public function getCurrentGamePlayerNicknames():Array
		{
			return _games[_currentGameIndex].playerNicknames;
		}
		
		private function checkAchievements():void
		{
			//Main.updateDebugText("checkAchievements - " + Main.achievements.progress_Play_Online_Custom_1);
			
			if(Main.achievements.progress_Play_Online_Custom_1 != Achievements.COMPLETE)
			{
				Main.achievements.updateAchievementProgress(Achievements.PLAY_ONLINE_CUSTOM_1);
			}
		}
		
		public function haveAllPlayersJoinedCurrentGame():Boolean
		{
			if (_games[_currentGameIndex].players.length == _games[_currentGameIndex].numOfPlayers)
				return true;
			return false;
		}
		
		public function haveAllPlayersJoinedGame(gameID:String):Boolean
		{
			var gameIndex:int = NOT_FOUND;
			for (var i:int=0; i<_games.length; i++)
			{
				if (_games[i].id == gameID)
				{
					gameIndex = i;
					break;
				}
			}
			if (gameIndex != NOT_FOUND)
			{
				if (_games[gameIndex].players.length == _games[gameIndex].numOfPlayers)
					return true;
			}
			return false;
		}
		
		public function getcurrentGameStatus():int
		{
			return _games[_currentGameIndex].status;
		}

		// push notifications
		private function sendPushNotification(playerDeviceTokens:Array, message:String):void
		{
			//Main.updateDebugText("sendPushNotification");
			if (playerDeviceTokens != null)
			{
				for (var i:int=0; i<playerDeviceTokens.length; i++)
				{
					//Main.updateDebugText("1. push to " + playerDeviceTokens[i]);
					PushNotification.sendPush(playerDeviceTokens[i], message);
				}
			}
		}
		private function sendPushNotificationWithPlayerLoad(playerID:String, message:String):void
		{
			//Main.updateDebugText("sendPushNotificationWithPlayerLoad");
			_pushMessage = message;
			Entity.load(PlayerEntity, playerID, onLoadPlayerForPushNotificationComplete, onLoadPlayerForPushNotificationError);
		}
		
		private function onLoadPlayerForPushNotificationComplete(player:PlayerEntity):void
		{
			if (player.APNSTokenIDs != null)
			{
				for (var i:int=0; i<player.APNSTokenIDs.length; i++)
				{
					//Main.updateDebugText("2. push to " + player.APNSTokenIDs[i]);
					PushNotification.sendPush(player.APNSTokenIDs[i], _pushMessage);
				}
			}
		}
		
		private function onLoadPlayerForPushNotificationError(error:String):void
		{
			//Main.updateDebugText("onLoadPlayerForPushNotificationError");
			
			/*
			var errorEntity:ErrorEntity = new ErrorEntity();
			errorEntity.userID = Main.sOnlineID;
			errorEntity.deviceType = PathogenTest.device;
			errorEntity.functionName = "onLoadPlayerForPushNotificationError";
			errorEntity.error = error;
			errorEntity.saveQueued();
			*/
		}
		
		public function setPlayerAPNSTokenID():void
		{
			//Main.updateDebugText("setPlayerAPNSTokenID");
			addEventListener(Event.ENTER_FRAME, waitForCurrentPlayerEntity);
		}
		
		private function waitForCurrentPlayerEntity(event:Event):void
		{
			if (_currentPlayerEntity != null && PushNotification.sTokenID != "")
			{
				if (_currentPlayerEntity.addTokenID(PushNotification.sTokenID))
					_currentPlayerEntity.saveQueued();
				removeEventListener(Event.ENTER_FRAME, waitForCurrentPlayerEntity);
			}
		}
		
		public function setPushNotificationServerAddress():void
		{
			if (!Main._lite)
				Entity.load(ServerEntity, "1", onLoadServerComplete, onLoadServerError);
			else
				Entity.load(ServerEntity, "2", onLoadServerComplete, onLoadServerError);
			
			if (Main.sTestGooglePush)
				Entity.load(ServerEntity, "3", onLoadAndroidServerComplete, onLoadAndroidServerError);
			else
				Entity.load(ServerEntity, "4", onLoadAndroidServerComplete, onLoadAndroidServerError);
		}
		
		private function onLoadServerComplete(server:ServerEntity):void
		{
			PushNotification.sServerAddress = server.serverAddress;
		}
		
		private function onLoadServerError(error:String):void
		{
		}
		
		private function onLoadAndroidServerComplete(server:ServerEntity):void
		{
			PushNotification.sAndroidServerAddress = server.serverAddress;
		}
		
		private function onLoadAndroidServerError(error:String):void
		{
		}
		
		public function get players():Vector.<PlayerRecord> { return _players; }
		public function get games():Vector.<GameEntity> { return _games; }
		public function get invites():Vector.<InviteEntity> { return _invites; }
		public function get invitedGames():Vector.<GameEntity> { return _invitedGames; }
		public function get currentGameIndex():int { return _currentGameIndex; }
		public function get currentPlayer():Player { return _currentPlayer; }
		public function get currentPlayerEntity():PlayerEntity { return _currentPlayerEntity; }
		public function get calculatingRanks():Boolean { return _calculatingRanks; }
		public function get seekingRankedGame():Boolean { return _seekingRankedGame; }
		public function get seekingRandomGame():Boolean { return _seekingRandomGame; }
		public function get foundRankedGameToJoin():Boolean { return _foundRankedGameToJoin; }
		public function get foundRandomGameToJoin():Boolean { return _foundRandomGameToJoin; }
		public function get currentLadder():Array { return _currentLadder; }
		public function get currentLeague2Player():String { return _currentLeague2Player; }
		public function get currentRank2Player():int { return _currentRank2Player; }
		
		public function set currentGameIndex(currentGameIndex:int):void { _currentGameIndex = currentGameIndex; }
		public function set seekingRankedGame(seekingRankedGame:Boolean):void { _seekingRankedGame = seekingRankedGame; }
		public function set seekingRandomGame(seekingRandomGame:Boolean):void { _seekingRandomGame = seekingRandomGame; }
		public function set currentLadder(currentLadder:Array):void { _currentLadder = currentLadder; }
		public function set currentLeague2Player(currentLeague2Player:String):void { _currentLeague2Player = currentLeague2Player; }
		public function set currentRank2Player(currentRank2Player:int):void { _currentRank2Player = currentRank2Player; }
	}
}