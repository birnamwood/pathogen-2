package FloxConnect
{
	import com.gamua.flox.Entity;
	
	public class JoinRandomGameEntity extends Entity
	{
		private var _gameID:String;
		private var _playerID:String;
		private var _dateTime:Date;
		
		public function JoinRandomGameEntity()
		{
			super();
		}
		
		public function get gameID():String { return _gameID; }
		public function get playerID():String { return _playerID; }
		public function get dateTime():Date { return _dateTime; }
		
		public function set gameID(gameID:String):void { _gameID = gameID; }
		public function set playerID(playerID:String):void { _playerID = playerID; }
		public function set dateTime(dateTime:Date):void { _dateTime = dateTime; }
	}
}