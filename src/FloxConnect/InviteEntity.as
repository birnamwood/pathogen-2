package FloxConnect
{
	import com.gamua.flox.Entity;
	
	public class InviteEntity extends Entity
	{
		private var _gameID:String;
		private var _invitingID:String;
		private var _invitedID:String;
		private var _status:int;
		private var _dateTime:Date;
		
		public function InviteEntity()
		{
			super();
		}
		
		public function get invitedIDAndStatus():String
		{
			return _invitedID + "|" + _status;
		}
		
		public function get gameID():String { return _gameID; }
		public function get invitingID():String { return _invitingID; }
		public function get invitedID():String { return _invitedID; }
		public function get status():int { return _status; }
		public function get dateTime():Date { return _dateTime; }
		
		public function set gameID(gameID:String):void { _gameID = gameID; }
		public function set invitingID(invitingID:String):void { _invitingID = invitingID; }
		public function set invitedID(invitedID:String):void { _invitedID = invitedID; }
		public function set status(status:int):void { _status = status; }
		public function set dateTime(dateTime:Date):void { _dateTime = dateTime; }
	}
}