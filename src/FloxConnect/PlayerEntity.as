package FloxConnect
{
	import UI.Main;
	
	import com.gamua.flox.Entity;
	
	public class PlayerEntity extends Entity
	{
		private var _nickname:String;
		private var _ranks:Array;
		private var _lastRanks:Array;
		private var _wins:Array;
		private var _losses:Array;
		private var _ties:Array;
		private var _scores:Array;
		private var _league2Player:String
		private var _division2Player:int;
		private var _league3Player:String
		private var _division3Player:int;
		private var _league4Player:String
		private var _division4Player:int;
		private var _rankedGames:int;
		private var _numCellsCreatedRanked:int;
		private var _numCellsDestroyedRanked:int;
		private var _avgPercentBoardOwnedRanked:Number;
		private var _customGames:int;
		private var _customGamesPlayed:int;
		private var _numCellsCreatedCustom:int;
		private var _numCellsDestroyedCustom:int;
		private var _avgPercentBoardOwnedCustom:Number;
		private var _sortScore:int;
		private var _customWins:int;
		private var _customLosses:int;
		private var _customTies:int;
		private var _nemesesID:Array;
		private var _nemesesNickname:Array;
		private var _nemesesCount:Array;
		private var _dominateesID:Array;
		private var _dominateesNickname:Array;
		private var _dominateesCount:Array;
		private var _APNSTokenIDs:Array;
		
		public function PlayerEntity()
		{
			super();
			
			_wins = [0, 0, 0];
			_losses = [0, 0, 0];
			_ties = [0, 0, 0];
			_scores = [1450, 1450, 1450];
			
			_ranks = [EntityManager.NOT_FOUND, EntityManager.NOT_FOUND, EntityManager.NOT_FOUND];
			_lastRanks = [EntityManager.NOT_FOUND, EntityManager.NOT_FOUND, EntityManager.NOT_FOUND];
			_league2Player = "";
			_league3Player = "";
			_league4Player = "";
			_division2Player = EntityManager.NOT_FOUND;
			_division3Player = EntityManager.NOT_FOUND;
			_division4Player = EntityManager.NOT_FOUND;
			
			_customGames = EntityManager.INITIAL_MAX_CUSTOM_GAMES;
			_rankedGames = EntityManager.INITIAL_MAX_RANKED_GAMES;
			
			_numCellsCreatedRanked = 0;
			_numCellsDestroyedRanked = 0;
			_avgPercentBoardOwnedRanked = 0;
			
			_numCellsCreatedCustom = 0;
			_numCellsDestroyedCustom = 0;
			_avgPercentBoardOwnedCustom = 0;
			
			_customGamesPlayed = 0;
			_sortScore = 0;
			
			_nemesesID = [];
			_nemesesNickname = [];
			_nemesesCount = [];
			
			_dominateesID = [];
			_dominateesNickname = [];
			_dominateesCount = [];
			
			_APNSTokenIDs = [];
			
			// trace("creating player entity");
		}
		
		public function calculateScore(gameType:int, avgOpponentScore:int, score:Number):void
		{
			var F:int = EntityManager.SCORING_RATING_DISPARITY;
			var D:int = scores[gameType] - avgOpponentScore;
			var K:int;
			var dE:int;
			
			if (scores[gameType] < 2100 || avgOpponentScore < 2100)
				K = 32;
			else if (scores[gameType] < 2401 || avgOpponentScore < 2401)
				K = 24;
			else if (scores[gameType] > 2400 && avgOpponentScore > 2400)
				K = 16;
			
			dE = K * (score - (1 / (Math.pow(10, ((-D / F) + 1)))));
			
			scores[gameType] += dE;
		}
		
		public function isReadyForLeague(gameType:int):Boolean
		{
			if ((_wins[gameType] + _losses[gameType] + _ties[gameType]) >= EntityManager.GAMES_TO_BE_RANKED)
				return true;
			else
				return false;
		}
		
		public function getNumberOfGamesUntilLeague(gameType:int):int
		{
			var totalGames:int = _wins[gameType] + _losses[gameType] + _ties[gameType];
			
			if (totalGames <= EntityManager.GAMES_TO_BE_RANKED)
				return EntityManager.GAMES_TO_BE_RANKED - totalGames;
			return -1;
		}
		
		public function addToNemesisCount(nemesisID:String, nemesisNickname:String):void
		{
			if (nemesisID != "" && nemesisID != null && nemesisNickname != "" && nemesisNickname != null)
			{		
				var foundIndex:int = EntityManager.NOT_FOUND;
				for (var i:int=0; i<_nemesesID.length; i++)
				{
					if (_nemesesID[i] == nemesisID)
					{
						foundIndex = i;
						break;
					}
				}
				if (foundIndex != EntityManager.NOT_FOUND)
				{
					if (_nemesesCount[foundIndex])
						_nemesesCount[foundIndex]++;
				}
				else
				{
					_nemesesID.push(nemesisID);
					_nemesesNickname.push(nemesisNickname);
					_nemesesCount.push(1);
				}
			}
		}
		
		public function getNemesisID():String
		{
			var highestCount:int = 0;
			var highestIndex:int = EntityManager.NOT_FOUND;
			if (_nemesesID.length > 0)
			{
				for (var i:int=0; i<_nemesesID.length; i++)
				{
					if (_nemesesID[i] != "" && _nemesesCount[i] >= EntityManager.NEMESIS_COUNT && _nemesesCount[i] >= highestCount)
					{
						highestCount = _nemesesCount[i];
						highestIndex = i;
					}
				}
				if (highestIndex != EntityManager.NOT_FOUND && _nemesesID[highestIndex] != null)
					return _nemesesID[highestIndex];
			}
			return "";
		}
		
		public function getNemesisNickname():String
		{
			var highestCount:int = 0;
			var highestIndex:int = EntityManager.NOT_FOUND;
			if (_nemesesID.length > 0)
			{
				for (var i:int=0; i<_nemesesID.length; i++)
				{
					if (_nemesesID[i] != "" && _nemesesCount[i] >= EntityManager.NEMESIS_COUNT && _nemesesCount[i] >= highestCount)
					{
						highestCount = _nemesesCount[i];
						highestIndex = i;
					}
				}

				if (highestIndex != EntityManager.NOT_FOUND && _nemesesNickname[highestIndex] != null)
					return _nemesesNickname[highestIndex];
			}
			return "";
		}
		
		public function getNemesisWins():int
		{
			var highestCount:int = 0;
			var highestIndex:int = EntityManager.NOT_FOUND;
			var nemesisID:String;
			if (_nemesesID.length > 0)
			{
				var i:int;
				for (i=0; i<_nemesesID.length; i++)
				{
					if (_nemesesID[i] != "" && _nemesesCount[i] >= EntityManager.NEMESIS_COUNT && _nemesesCount[i] >= highestCount)
					{
						highestCount = _nemesesCount[i];
						highestIndex = i;
					}
				}
				if (highestIndex != EntityManager.NOT_FOUND)
				{
					var dominateeIndex:int = EntityManager.NOT_FOUND;
					nemesisID = _nemesesID[highestIndex];
					
					for (i=0; i<_dominateesID.length; i++)
					{
						if (_dominateesID[i] == nemesisID)
							dominateeIndex = i;
					}
					if (dominateeIndex != EntityManager.NOT_FOUND)
						return _dominateesCount[dominateeIndex];
				}
			}
			return 0;
		}
		
		public function getNemesisLosses():int
		{
			var highestCount:int = 0;
			var highestIndex:int = EntityManager.NOT_FOUND;
			if (_nemesesID.length > 0)
			{
				for (var i:int=0; i<_nemesesID.length; i++)
				{
					if (_nemesesID[i] != "" && _nemesesCount[i] >= EntityManager.NEMESIS_COUNT && _nemesesCount[i] >= highestCount)
					{
						highestCount = _nemesesCount[i];
						highestIndex = i;
					}
				}
				if (highestIndex != EntityManager.NOT_FOUND)
					return _nemesesCount[highestIndex];
			}
			return 0;
		}
		
		public function addToDominateeCount(dominateeID:String, dominateeNickname:String):void
		{
			if (dominateeID != "" && dominateeID != null && dominateeNickname != "" && dominateeNickname != null)
			{
				var foundIndex:int = EntityManager.NOT_FOUND;
				for (var i:int=0; i<_dominateesID.length; i++)
				{
					if (_dominateesID[i] == dominateeID)
					{
						foundIndex = i;
						break;
					}
				}
				if (foundIndex != EntityManager.NOT_FOUND)
				{
					// trace("foundIndex = " + foundIndex);
					
					if (_dominateesCount[foundIndex])
						_dominateesCount[foundIndex]++;
				}
				else
				{	
					_dominateesID.push(dominateeID);
					_dominateesNickname.push(dominateeNickname);
					_dominateesCount.push(1);
				}
			}
		}
		
		public function getDominateeID():String
		{
			var highestCount:int = 0;
			var highestIndex:int = EntityManager.NOT_FOUND;
			if (_dominateesID.length > 0)
			{
				for (var i:int=0; i<_dominateesID.length; i++)
				{
					if (_dominateesID[i] != "" && _dominateesCount[i] >= EntityManager.DOMINATEE_COUNT && _dominateesCount[i] >= highestCount)
					{
						highestCount = _dominateesCount[i];
						highestIndex = i;
					}
				}
				if (highestIndex != EntityManager.NOT_FOUND && _dominateesID[highestIndex] != null)
					return _dominateesID[highestIndex];
			}
			return "";
		}
		
		public function getDominateeNickname():String
		{
			var highestCount:int = 0;
			var highestIndex:int = EntityManager.NOT_FOUND;
			if (_dominateesID.length > 0)
			{
				for (var i:int=0; i<_dominateesID.length; i++)
				{
					if (_dominateesID[i] != "" && _dominateesCount[i] >= EntityManager.DOMINATEE_COUNT && _dominateesCount[i] >= highestCount)
					{
						highestCount = _dominateesCount[i];
						highestIndex = i;
					}
				}
				if (highestIndex != EntityManager.NOT_FOUND && _dominateesNickname[highestIndex] != null)
					return _dominateesNickname[highestIndex];
			}
			return "";
		}
		
		public function getDominateeWins():int
		{
			var highestCount:int = 0;
			var highestIndex:int = EntityManager.NOT_FOUND;
			if (_dominateesID.length > 0)
			{
				for (var i:int=0; i<_dominateesID.length; i++)
				{
					if (_dominateesID[i] != "" && _dominateesCount[i] >= EntityManager.DOMINATEE_COUNT && _dominateesCount[i] >= highestCount)
					{
						highestCount = _dominateesCount[i];
						highestIndex = i;
					}
				}
				if (highestIndex != EntityManager.NOT_FOUND)
					return _dominateesCount[highestIndex];
			}
			return 0;
		}
		
		public function getDominateeLosses():int
		{
			var highestCount:int = 0;
			var highestIndex:int = EntityManager.NOT_FOUND;
			var dominateeID:String;
			if (_dominateesID.length > 0)
			{
				var i:int;
				for (i=0; i<_dominateesID.length; i++)
				{
					if (_dominateesID[i] != "" && _dominateesCount[i] >= EntityManager.DOMINATEE_COUNT && _dominateesCount[i] >= highestCount)
					{
						highestCount = _dominateesCount[i];
						highestIndex = i;
					}
				}
				if (highestIndex != EntityManager.NOT_FOUND)
				{
					var nemesisIndex:int = EntityManager.NOT_FOUND;
					dominateeID = _dominateesID[highestIndex];
					
					for (i=0; i<_nemesesID.length; i++)
					{
						if (_nemesesID[i] == dominateeID)
							nemesisIndex = i;
					}
					if (nemesisIndex != EntityManager.NOT_FOUND)
						return _nemesesCount[nemesisIndex];
				}
			}
			return 0;
		}
		
		public function get twoPlayer():String
		{
			return _league2Player + "|" + _division2Player;
		}
		
		public function get threePlayer():String
		{
			return _league3Player + "|" + _division3Player;
		}
		
		public function get fourPlayer():String
		{
			return _league4Player + "|" + _division4Player;
		}
		
		public function addTokenID(tokenID:String):Boolean
		{
			//Main.updateDebugText("addTokenID " + tokenID);
			
			// format token ID
			var formattedTokenID:String = tokenID.split(" ").join("");
			formattedTokenID = formattedTokenID.split("<").join("");
			formattedTokenID = formattedTokenID.split(">").join("");
			
			var found:Boolean = false;
			for (var i:int=0; i<_APNSTokenIDs.length; i++)
			{
				if (_APNSTokenIDs[i] == formattedTokenID)
				{
					found = true;
					break;
				}
			}
			if (!found)
			{
				_APNSTokenIDs.push(formattedTokenID);
				return true;
			}
			return false;
		}

		public function get nickname():String { return _nickname; }
		public function get ranks():Array { return _ranks; }
		public function get lastRanks():Array { return _lastRanks; }
		public function get wins():Array { return _wins; }
		public function get losses():Array { return _losses; }
		public function get ties():Array { return _ties; }
		public function get scores():Array { return _scores; }
		public function get league2Player():String { return _league2Player; }
		public function get league3Player():String { return _league3Player; }
		public function get league4Player():String { return _league4Player; }
		public function get division2Player():int { return _division2Player; }
		public function get division3Player():int { return _division3Player; }
		public function get division4Player():int { return _division4Player; }
		public function get rankedGames():int { return _rankedGames; }
		public function get numCellsCreatedRanked():int { return _numCellsCreatedRanked; }
		public function get numCellsDestroyedRanked():int { return _numCellsDestroyedRanked; }
		public function get avgPercentBoardOwnedRanked():Number { return _avgPercentBoardOwnedRanked; }
		public function get customGames():int { return _customGames; }
		public function get customGamesPlayed():int { return _customGamesPlayed; }
		public function get numCellsCreatedCustom():int { return _numCellsCreatedCustom; }
		public function get numCellsDestroyedCustom():int { return _numCellsDestroyedCustom; }
		public function get avgPercentBoardOwnedCustom():Number { return _avgPercentBoardOwnedCustom; }
		public function get sortScore():int { return _sortScore; }
		public function get customWins():int { return _customWins; }
		public function get customLosses():int { return _customLosses; }
		public function get customTies():int { return _customTies; }
		public function get nemesesID():Array { return _nemesesID; }
		public function get nemesesNickname():Array { return _nemesesNickname; }
		public function get nemesesCount():Array { return _nemesesCount; }
		public function get dominateesID():Array { return _dominateesID; }
		public function get dominateesNickname():Array { return _dominateesNickname; }
		public function get dominateesCount():Array { return _dominateesCount; }
		public function get APNSTokenIDs():Array { return _APNSTokenIDs; }

		public function set nickname(nickname:String):void { _nickname = nickname; }
		public function set ranks(ranks:Array):void { _ranks = ranks; }
		public function set lastRanks(lastRank:Array):void { _lastRanks = lastRanks; }
		public function set wins(wins:Array):void { _wins = wins; }
		public function set losses(losses:Array):void { _losses = losses; }
		public function set ties(ties:Array):void { _ties = ties; }
		public function set scores(scores:Array):void { _scores = scores; }
		public function set league2Player(league2Player:String):void { _league2Player = league2Player; }
		public function set league3Player(league3Player:String):void { _league3Player = league3Player; }
		public function set league4Player(league4Player:String):void { _league4Player = league4Player; }
		public function set division2Player(division2Player:int):void { _division2Player = division2Player; }
		public function set division3Player(division3Player:int):void { _division3Player = division3Player; }
		public function set division4Player(division4Player:int):void { _division4Player = division4Player; }
		public function set rankedGames(rankedGames:int):void { _rankedGames = rankedGames; }
		public function set numCellsCreatedRanked(numCellsCreatedRanked:int):void { _numCellsCreatedRanked = numCellsCreatedRanked; }
		public function set numCellsDestroyedRanked(numCellsDestroyedRanked:int):void { _numCellsDestroyedRanked = numCellsDestroyedRanked; }
		public function set avgPercentBoardOwnedRanked(avgPercentBoardOwnedRanked:Number):void { _avgPercentBoardOwnedRanked = avgPercentBoardOwnedRanked; }
		public function set customGames(customGames:int):void { _customGames = customGames; }
		public function set customGamesPlayed(customGamesPlayed:int):void { _customGamesPlayed = customGamesPlayed; }
		public function set numCellsCreatedCustom(numCellsCreatedCustom:int):void { _numCellsCreatedCustom = numCellsCreatedCustom; }
		public function set numCellsDestroyedCustom(numCellsDestroyedCustom:int):void { _numCellsDestroyedCustom = numCellsDestroyedCustom; }
		public function set avgPercentBoardOwnedCustom(avgPercentBoardOwnedCustom:Number):void { _avgPercentBoardOwnedCustom = avgPercentBoardOwnedCustom; }
		public function set sortScore(sortScore:int):void { _sortScore = sortScore; }
		public function set customWins(customWins:int):void { _customWins = customWins; }
		public function set customLosses(customLosses:int):void { _customLosses = customLosses; }
		public function set customTies(customTies:int):void { _customTies = customTies; }
		public function set nemesesID(nemesesID:Array):void { _nemesesID = nemesesID; }
		public function set nemesesNickname(nemesesNickname:Array):void { _nemesesNickname = nemesesNickname; }
		public function set nemesesCount(nemesesCount:Array):void { _nemesesCount = nemesesCount; }
		public function set dominateesID(dominateesID:Array):void { _dominateesID = dominateesID; }
		public function set dominateesNickname(dominateesNickname:Array):void { _dominateesNickname = dominateesNickname; }
		public function set dominateesCount(dominateesCount:Array):void { _dominateesCount = dominateesCount; }
		public function set APNSTokenIDs(APNSTokenIDs:Array):void { _APNSTokenIDs = APNSTokenIDs; }
	}
}