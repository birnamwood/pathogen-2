package FloxConnect
{
	import com.gamua.flox.Entity;
	
	public class GamePlayerEntity extends Entity
	{
		private var _gameID:String;
		private var _playerID:String;
		
		public function GamePlayerEntity()
		{
			super();
		}
		
		public function get gameIDAndPlayerID():String
		{
			return _gameID + "|" + _playerID;
		}
		
		public function get gameID():String { return _gameID; }
		public function get playerID():String { return _playerID; }
		
		public function set gameID(gameID:String):void { _gameID = gameID; }
		public function set playerID(playerID:String):void { _playerID = playerID; }
	}
}