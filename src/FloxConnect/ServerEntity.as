package FloxConnect
{
	import com.gamua.flox.Entity;
	
	public class ServerEntity extends Entity
	{
		private var _serverAddress:String;
		
		public function ServerEntity()
		{
			super();
		}
		
		public function get serverAddress():String { return _serverAddress; }
		public function set serverAddress(serverAddress:String):void { _serverAddress = serverAddress; }
	}
}