package FloxConnect
{
	import com.gamua.flox.Entity;
	
	public class LeagueEntity extends Entity
	{
		private var _divisionTotals:Array;
		
		public function LeagueEntity()
		{
			super();
			
			_divisionTotals = [];
			_divisionTotals.push(0);
		}
		
		public function addToNextAvailableDivision():int
		{
			var divisionIndex:int = EntityManager.NOT_FOUND;
			
			for (var i:int=0; i<_divisionTotals.length; i++)
			{
				if (_divisionTotals[i] < EntityManager.DIVISION_MAX)
				{
					_divisionTotals[i]++;
					divisionIndex = i;
					break;
				}
			}
			if (divisionIndex == EntityManager.NOT_FOUND)
			{
				_divisionTotals.push(1);
				divisionIndex = _divisionTotals.length - 1;
			}
			
			//trace("in league entity, returning division of " + divisionIndex);
			
			return divisionIndex;
		}
		
		public function removeFromDivision(division:int):void
		{
			trace("checking for division " + division + " divisionTotals length = " + divisionTotals.length);
			
			if (divisionTotals.length > division)
			{
				divisionTotals[division]--;
				if (divisionTotals[division] < 0)
					divisionTotals[division] = 0;
			}
		}

		public function getCurrentDivision():int
		{
			return (_divisionTotals.length - 1);
		}
		
		public function get divisionTotals():Array { return _divisionTotals; }
		
		public function set divisionTotals(divisionTotals:Array):void { _divisionTotals = divisionTotals; }
	}
}